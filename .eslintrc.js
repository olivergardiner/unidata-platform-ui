/**
 * Конфиг для проверки стиля кодирования утилитой ESLint
 *
 * Причина отказа от tslint:
 * 1) Конфигурация не обладает должной гибкостью
 * 2) Microsoft планирует прекрарить использовать tslint и добиться паритета с eslint:
 *      @see https://github.com/typescript-eslint/typescript-eslint
 *      @see https://github.com/microsoft/TypeScript/issues/30553
 *      @see https://github.com/bradzacher/eslint-plugin-typescript/issues/290
 *
 * Перечень правил:
 *      Для ESLint
 *      @see https://eslint.org/docs/rules/
 *
 *      Для TypeScript
 *      @see https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin/docs/rules
 *
 *      Для react
 *      @see https://github.com/yannickcr/eslint-plugin-react
 *
 * Локальные конфигурационные файлы для анализа:
 *      @see node_modules/eslint/conf/eslint-recommended.js
 *      @see node_modules/@typescript-eslint/eslint-plugin/dist/configs/eslint-recommended.js
 *
 *      @see node_modules/@typescript-eslint/eslint-plugin/dist/configs/base.json
 *      @see node_modules/@typescript-eslint/eslint-plugin/dist/configs/all.json
 */

module.exports = {
    'parser': '@typescript-eslint/parser',

    'plugins': [
        '@typescript-eslint',
        'react',
        'jest'
    ],

    'extends': [
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:@typescript-eslint/recommended'
    ],

    /**
     * @see https://eslint.org/docs/user-guide/configuring#specifying-globals
     */
    'parserOptions': {
        'ecmaFeatures': {
            'jsx': true
        }
    },

    // определяет глобальные переменные, которые считаются предопределенными
    'env': {
        'browser': true,
        'node': true,
        'es6': true,
        'jest': true
    },

    // способы отключения правил
    // @see https://stackoverflow.com/questions/34764287/turning-off-eslint-rule-for-a-specific-file
    'overrides': [
        {
            'files': ['*.tsx'],
            'excludedFiles': [
                'src/ReactApp.tsx',
                'src/AppRoute.tsx'
            ],
            'rules': {
                '@typescript-eslint/explicit-member-accessibility': ['error', {'accessibility': 'no-public'}]
            }
        }
    ],

    /**************************************************
     *
     * НАСТРОЙКИ МОДУЛЕЙ ESLINT
     *
     **************************************************/

    'settings': {
        'react': {
            'version': 'detect' // проверяем версию реакта перед парсингом (обязательно для модуля eslint-react)
        }
    },

    /**************************************************
     *
     * ПРАВИЛА ПРОВЕРКИ
     *
     **************************************************/

    'rules': {

        /**************************************************
         *
         * ПРАВИЛА ESLINT
         *
         **************************************************/

        'indent': [
            'error',
            4,
            {
                'SwitchCase': 1
            }
        ],
        'semi': [
            'error',
            'always'
        ],
        'operator-linebreak': [
            'error',
            'after'
        ],
        'comma-style': [
            'error',
            'last'
        ],
        'max-len': [
            'error',
            {
                'code': 140,
                'ignoreComments': true,
                'ignoreRegExpLiterals': true,
                'ignoreTemplateLiterals': true
            }
        ],
        'camelcase': [
            'error',
            {
                'properties': 'always',
                'ignoreDestructuring': true,
                'allow': [
                    'multi_select'
                ]
            }
        ],
        'vars-on-top': 'error',
        'one-var': [
            'error',
            'always'
        ],

        'padding-line-between-statements': [
            'error',
            // Директивы, например «use strict»
            {'blankLine': 'always', 'prev': 'directive', 'next': '*'},
            {'blankLine': 'any', 'prev': 'directive', 'next': 'directive'},

            // блок export должен иметь отступы
            {'blankLine': 'always', 'prev': 'export', 'next': '*'},
            {'blankLine': 'always', 'prev': '*', 'next': 'export'},

            // пустая строка после блока переменных
            {'blankLine': 'always', 'prev': ['let', 'var', 'const'], 'next': '*'},
            {'blankLine': 'never', 'prev': 'let', 'next': 'let'},
            {'blankLine': 'never', 'prev': 'var', 'next': 'var'},
            {'blankLine': 'never', 'prev': 'const', 'next': 'const'},

            // многострочные блоки переменных должны иметь отступы
            {'blankLine': 'always', 'prev': ['multiline-let', 'multiline-var', 'multiline-const'], 'next': '*'},
            {'blankLine': 'always', 'prev': '*', 'next': ['multiline-let', 'multiline-var', 'multiline-const']},

            // пустая строка перед блоками
            {'blankLine': 'always', 'prev': '*', 'next': ['if', 'while', 'do', 'for', 'return', 'try', 'switch']},

            // пустая строка после блоков
            {'blankLine': 'always', 'prev': ['if', 'while', 'do', 'for', 'return', 'try', 'switch'], 'next': '*'}
        ],


        'curly': [
            'error',
            'all'
        ],
        'keyword-spacing': [
            'error',
            {
                'overrides': {
                    'else': {
                        'before': true
                    },
                    'while': {
                        'before': true
                    },
                    'catch': {
                        'before': true
                    }
                }
            }
        ],
        'space-infix-ops': 'error',
        'space-before-blocks': [
            'error',
            'always'
        ],
        'eol-last': 'error',
        'comma-spacing': [
            'error',
            {
                'before': false,
                'after': true
            }
        ],
        'space-unary-ops': [
            'error',
            {
                'words': true,
                'nonwords': false
            }
        ],
        'no-undef': 'off', // выключаем, иначе не позволяет использовать optional chaining: x?.y?.z
        'no-trailing-spaces': 'error',
        'no-mixed-spaces-and-tabs': 'error',
        'space-before-function-paren': [
            'error',
            'always'
        ],
        'func-call-spacing': ['error', 'never'],
        'key-spacing': [
            'error',
            {
                'beforeColon': false,
                'afterColon': true,
                'mode': 'strict'
            }
        ],
        'comma-dangle': [
            'error',
            'never'
        ],
        'no-multi-str': 'error',
        'no-with': 'error',
        'brace-style': [
            'error',
            '1tbs',
            {
                'allowSingleLine': true
            }
        ],
        'no-empty': [
            'error',
            {
                'allowEmptyCatch': true
            }
        ],
        'no-nested-ternary': 'error',
        'no-implicit-coercion': [
            'error',
            {
                'boolean': true,
                'string': true,
                'number': true
            }
        ],
        'no-underscore-dangle': 'error',
        'no-multiple-empty-lines': 'error',
        'space-in-parens': [
            'error',
            'never'
        ],
        'no-unused-vars': [
            'error',
            {
                'vars': 'local',
                'args': 'all'
            }
        ],
        'no-dupe-class-members': 'off', // позволяем писать overload методы

        'quote-props': ['error', 'as-needed', { 'keywords': true }],
        'quotes': ['error', 'single', {'allowTemplateLiterals': true}],

        'no-prototype-builtins': 'error',
        'array-bracket-spacing': ['error', 'never'],

        'no-extra-boolean-cast': 'off', // если программист решил что нужно приводить - ок. отключено т.к. есть ложные срабатывания

        'eqeqeq': ['error', 'always'], // требовать строгое равенство === и !==

        'object-curly-spacing': ['error', 'never'],

        /**************************************************
         *
         * ПРАВИЛА TYPESCRIPT-ESLINT
         *
         **************************************************/

        '@typescript-eslint/explicit-member-accessibility': ['error', {
            'accessibility': 'explicit',
            'overrides': {
                'constructors': 'no-public'
            }
        }], // требуется явное указания public / private / protected
        '@typescript-eslint/no-explicit-any': 'off', // Разрешаем явно указывать тип any
        '@typescript-eslint/no-parameter-properties': 'error', // Запрещено определять свойства класса параметрами конструктора constructor(public name: string) {}
        '@typescript-eslint/explicit-function-return-type': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ. Требуется обсуждение. Требует всегда явно указывать тип возвращаемого значения из функции, даже если это не нужно
        '@typescript-eslint/explicit-module-boundary-types': 'off', // новое, добавилось в v4. Обсудить
        '@typescript-eslint/ban-types': 'off', // новое, добавилось в v4. Обсудить
        '@typescript-eslint/no-inferrable-types': 'off', // Ничего страшного в том что определили переменнюу как let result: boolean = false
        '@typescript-eslint/naming-convention': [
            'error',
            {
                'selector': 'interface',
                'format': ['PascalCase'],
                'custom': {
                    'regex': '^I[A-Z]',
                    'match': true
                }
            }
        ],

        '@typescript-eslint/no-empty-interface': 'off',
        '@typescript-eslint/array-type': ['error', {'default': 'array-simple'}], // Для простых типов и ссылочных T[] для сложных вариантов Array<T>

        // В документации явно указано что для TS предпочитительно использовать ";"
        '@typescript-eslint/member-delimiter-style': ['error', {
            'multiline': {
                'delimiter': 'semi',
                'requireLast': true
            },
            'singleline': {
                'delimiter': 'semi',
                'requireLast': false
            }
        }],

        '@typescript-eslint/prefer-interface': 'off', // let t: T или let t: {a: string} - не важно

        '@typescript-eslint/no-use-before-define': ['error', {
            'functions': false, // функции поднимаются - безопасно
            'classes': true, // классы не поднимаются - не безопасно
            'variables': true,
            'typedefs': true // типы поднимаются - безопасно, но всегда удобно видеть сначала тип
        }],

        '@typescript-eslint/no-empty-function': 'off',
        '@typescript-eslint/no-this-alias': [
            'error',
            {
                'allowDestructuring': true, // Allow `const { props, state } = this`; false by default
                'allowedNames': ['self', 'me'], // Allow `const self = this`; `[]` by default
            },
        ],


        /**************************************************
         *
         * ПРАВИЛА eslint-plugin-react
         *
         **************************************************/
        'react/no-deprecated': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ. Правило отключено на момент настройки для дальнейшего  принятия решения
        'react/prop-types': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ. Правило отключено на момент настройки для дальнейшего  принятия решения
        // "react/jsx-indent": ["error", 4, {checkAttributes: true, indentLogicalExpressions: true}],
        // "react/jsx-indent": "error",


        /**************************************************
         *
         * ПРАВИЛА с которыми еще предстоит разобраться
         *
         **************************************************/

        /**
         * Временно переопределяем правила чтоб позже вернуться для принятия решения
         */
        '@typescript-eslint/no-object-literal-type-assertion': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ
        'prefer-const': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ после обновления зависимостей правило оказалось в recommended
        'prefer-spread': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ после обновления зависимостей правило оказалось в recommended

        /**
         * Для правил ниже
         * сходу решить проблему не удалось
         */
        'one-var': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ. ранее было такой стиль кодирования.

        // эти два правила как то связаны?
        'indent': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ. данное правило выключено т.к. есть проблема с jsx там производится выравнивание по правилам html
        // видимо необходимо завязаться на https://github.com/yannickcr/eslint-plugin-react
        '@typescript-eslint/indent': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ

        '@typescript-eslint/no-unused-vars': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ смотри ниже коментарий
        'no-unused-vars': 'off', // ВЕРНУТЬСЯ ПОЗЖЕ (много ложных срабатываний в том числе для JSX, для оператора import, переменные в середине arguments)

        '@typescript-eslint/no-non-null-assertion': 'off' // ВЕРНУТЬСЯ ПОЗЖЕ. Зарпщено явное указание ts что переменная не null. Уже в коде полно таких мест. Необходимо посоветоваться и принять решение. Возможно что-то пошло не так?
    }
}
