FROM nginx:1.13.6-alpine
ADD config/nginx.conf /etc/nginx/conf.d/default.conf
COPY dist/ /usr/share/nginx/html/
