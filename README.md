# Unidata react application

## Before run
It is necessary to create local config in ```config/local.yml``` with:
```yaml
APP_TYPE:
  - 'dataSteward' # data operator interface
#  - 'dataAdmin' # data admin is available in EE edition only
  - 'systemAdmin' # admin part (users, roles, etc)

serverUrl:
  'http://localhost:8080/unidata-backend/api/' # path to backend api
```

## Start dev or build
App is required node v 12.14

1) Install dependency
```bash
npm ci
```

2a) Run app in dev mode
```bash
npm run dev
```

2b) Build modules

```bash
npm run build_modules
```

2c) Build application

```bash
npm run build
```

Create war if necessary
```bash
npm run create_war
```

## LICENSE

GNU GENERAL PUBLIC LICENSE Version 3
