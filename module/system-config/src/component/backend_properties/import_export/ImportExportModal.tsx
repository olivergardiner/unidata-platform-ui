/**
 * Modal window for import export BackendProperties
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {Dialog, i18n} from '@unidata/core-app';
import {ChooseAction} from './modal/ChooseAction';
import {UploadStep} from './modal/UploadStep';
import {ImportConfirmStep} from './modal/ImportConfirmStep';
import {Modal, Wizard, WizardStep} from '@unidata/uikit';
import {ImportExportOperations} from '../../../type/ImportExportWizard';
import {ImportExportStore} from '../store/ImportExportStore';

import * as styles from './importExportModal.m.scss';
import {ExportConfirmStep} from './modal/ExportConfirmStep';

interface IProps {
    store: ImportExportStore;
    readOnly?: boolean;
    openModal: any;
    isModalOpen: boolean;
}

@observer
export class ImportExportModal extends React.Component<IProps> {
    constructor (props: IProps) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
    }

    get store () {
        return this.props.store;
    }

    get disabledOperations () {
        return this.props.readOnly ? [ImportExportOperations.Import] : [];
    }

    get steps () {
        let steps: WizardStep[];

        switch (this.store.currentOperation) {
            case ImportExportOperations.Import:
                steps = [
                    {
                        name: 'upload',
                        label: i18n.t('wizardSteps>upload'),
                        component: <UploadStep file={this.store.file}
                                               resetOperation={this.handleResetOperation}
                                               onChange={this.store.handleUploadChange}/>
                    },
                    {
                        name: 'confirm',
                        label: i18n.t('wizardSteps>confirm'),
                        component: <ImportConfirmStep onSubmit={this.handleImportConfirm}
                                                      operation={this.store.currentOperation}/>
                    }
                ];
                break;
            case ImportExportOperations.Export:
                steps = [
                    {
                        name: 'confirm',
                        label: i18n.t('wizardSteps>confirm'),
                        component: <ExportConfirmStep onSubmit={this.handleExportConfirm}
                                                      resetOperation={this.handleResetOperation}
                                                      operation={this.store.currentOperation}/>
                    }
                ];
                break;
            default:
                steps = [];
        }

        return steps;
    }

    handleResetOperation = () => {
        this.store.handleChooseAction(null);
    };

    handleImportConfirm = () => {
        if (this.store.file && !this.store.isSaving) {
            this.store.handleImport()
                .then(() => {
                    this.closeModal();

                    Dialog.showMessage(i18n.t('admin.backendProperties>importSuccessMessage'), '');
                })
                .catch(function () {
                    Dialog.showError(i18n.t('admin.backendProperties>importError'), '');
                });
        }
    }

    handleExportConfirm = () => {
        if (!this.store.isSaving) {
            this.store.handleExport().then(() => {
                this.closeModal();

                Dialog.showMessage(i18n.t('admin.backendProperties>startExportProcess'), '');
            });
        }
    }

    onSubmit () {
        if (this.store.currentOperation === ImportExportOperations.Export) {
            this.handleExportConfirm();
        } else {
            this.handleImportConfirm();
        }
    }

    closeModal = () => {
        this.store.initState();
        this.props.openModal(false);
    }

    render () {
        const operationSelected = Boolean(this.store.currentOperation);

        return (
            <Modal isOpen={this.props.isModalOpen}
                   noBodyPadding={true}
                   header={i18n.t('admin.backendProperties>wizardImportExport>title').toUpperCase()}
                   onClose={this.closeModal}>
                <div className={styles.modalContent}>
                    {operationSelected ?
                        <Wizard steps={this.steps} onSubmit={this.onSubmit}/> :
                        <ChooseAction disabledOperations={this.disabledOperations}
                                      operation={this.store.currentOperation}
                                      onChange={this.store.handleChooseAction}/>
                    }
                </div>
            </Modal>
        );
    }
}
