/**
 * File upload step
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import * as React from 'react';
import {i18n} from '@unidata/core-app';
import {Button, INTENT, joinClassNames, Wizard, Upload, UploadFile} from '@unidata/uikit';
import {observer} from 'mobx-react';
import * as styles from '../importExportModal.m.scss';

const {Footer, Navigation} = Wizard;

interface IProps {
    onChange: (file: UploadFile | null) => void;
    file: UploadFile | null;
    resetOperation: () => void;
}

@observer
export class UploadStep extends React.Component<IProps> {
    render () {
        const props = {
            onRemove: (): void => {
                this.props.onChange(null);
                this.setState({fileList: []});
            },
            beforeUpload: (file: UploadFile): boolean => {
                this.props.onChange(file);

                return false;
            },
            multiple: false,
            fileList: this.props.file ? [this.props.file] : [],
            accept: '.properties'
        };

        const allowNext = Boolean(this.props.file);

        return (
            <>
                <Navigation allowNext={allowNext}/>
                <div className={joinClassNames(styles.wizardStep, styles.uploadStep)}>
                    <Upload {...props}>
                        <Button intent={INTENT.PRIMARY} leftIcon={'upload'}>
                            {i18n.t('selectFile')}
                        </Button>
                    </Upload>
                </div>
                <Footer leftElement={
                    <Button
                        isGhost={true}
                        data-qaid={'prevbutton'}
                        isRound
                        onClick={this.props.resetOperation}
                    >
                        {i18n.t('wizard>prevStep')}
                    </Button>}
                />
            </>
        );
    }
}
