/**
 * Import / export selection component
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import * as React from 'react';
import difference from 'lodash/difference';
import {i18n} from '@unidata/core-app';
import {Radio} from '@unidata/uikit';
import {ImportExportOperations} from '../../../../type/ImportExportWizard';

import * as styles from '../importExportModal.m.scss';

interface IProps {
    onChange: (operation: ImportExportOperations) => void;
    operation: ImportExportOperations | null;
    disabledOperations?: ImportExportOperations [];
}

export class ChooseAction extends React.PureComponent<IProps> {

    get operations () {
        return [
            ImportExportOperations.Import,
            ImportExportOperations.Export
        ] as ImportExportOperations[];
    }

    get allowedOperations () {
        if (!this.props.disabledOperations || this.props.disabledOperations.length === 0) {
            return this.operations;
        }

        return difference(this.operations, this.props.disabledOperations);
    }

    handleOnChange = (e: any) => {
        this.props.onChange(e.target.value);
    }

    render () {
        const {operation} = this.props;

        return (
            <div className={styles.actionSelect}>
                <Radio.Group onChange={this.handleOnChange} value={operation}>
                    {this.allowedOperations.map((operation) =>
                        <Radio key={operation} value={operation}>
                            {i18n.t(`common:${operation}`)}
                        </Radio>
                    )}
                </Radio.Group>
            </div>
        );
    }
}
