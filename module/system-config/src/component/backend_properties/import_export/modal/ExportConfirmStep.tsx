/**
 * Confirmation step import / export
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import * as React from 'react';
import {i18n} from '@unidata/core-app';
import {Alert} from '@unidata/uikit';
import {ImportExportOperations} from '../../../../type/ImportExportWizard';
import {Button, INTENT, Wizard} from '@unidata/uikit';
import * as styles from '../importExportModal.m.scss';

const {Footer} = Wizard;

interface IProps {
    operation: ImportExportOperations;
    onSubmit: () => void;
    resetOperation: () => void;
}

enum MessageTypes {
    Info = 'info',
    Warning = 'warning'
}

export class ExportConfirmStep extends React.PureComponent<IProps> {
    get messages () {
        return [
            {alertType: MessageTypes.Info, text: i18n.t('admin.backendProperties>wizardImportExport>messages>export>info')}
        ];
    }

    render () {
        return (
            <React.Fragment>
                <div className={styles.wizardStep}>
                    <div className={styles.confirmStepHeader}>
                        {i18n.t('wizardSteps>confirm')}
                    </div>
                    {this.messages.map((message) =>
                        <Alert
                            key={message.text}
                            type={message.alertType}
                            description={message.text}
                            message=''
                            showIcon={message.alertType === MessageTypes.Warning}
                        />
                    )}
                </div>
                <Footer leftElement={
                    <Button
                        isGhost={true}
                        data-qaid={'prevbutton'}
                        isRound
                        onClick={this.props.resetOperation}
                    >
                        {i18n.t('wizard>prevStep')}
                    </Button>}
                />
            </React.Fragment>
        );
    }
}
