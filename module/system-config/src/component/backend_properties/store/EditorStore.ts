/**
 * Store of the editing screen BE Properties
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import {action, observable} from 'mobx';
import {BackendProperty} from '../../../model/backend_properties/BackendProperty';
import {BackendPropertySectionType} from '../../../type/BackendProperties';
import {Dialog, i18n} from '@unidata/core-app';
import {BackendPropertyService} from '../../../service/index';
import {ReactiveProp} from '@unidata/core';

export class BackendPropertiesEditorStore {
    @observable public sections: BackendPropertySectionType[] = [];
    @observable public isSaving: boolean = false;
    @observable public bProperties: BackendProperty[] = [];

    public get activeKeys () {
        return this.sections.map((section) => section.groupCode);
    }

    public get backendProperties () {
        if (this.sections.length === 0) {
            this.loadBackendProperties();
        }

        return this.sections;
    }

    public loadBackendProperties () {
        BackendPropertyService.getBackendProperties().then(action((properties: BackendProperty[]) => {
            properties.forEach(function (beProp: BackendProperty) {
                beProp.setReactive([ReactiveProp.DIRTY]);
            });

            this.bProperties = properties;
            this.sections = this.splitByGroupCodeAndSortByGroupName(properties);
        }));
    }

    private splitByGroupCodeAndSortByGroupName (properties: BackendProperty[]): BackendPropertySectionType[] {
        const groupByGroupCode = (result: any, item: BackendProperty) => {

            let group: BackendPropertySectionType = result[item.groupCode.getValue()];

            if (!group) {
                group = {
                    group: item.group.getValue(),
                    groupCode: item.groupCode.getValue(),
                    backendProperties: []
                };
            }

            const {backendProperties} = group;

            group.backendProperties = [...backendProperties, item];
            result[item.groupCode.getValue()] = group;

            return result;
        };

        const sortByGroupName = (sections: BackendPropertySectionType[]) => {
            return sections.sort((a: BackendPropertySectionType, b: BackendPropertySectionType) => {
                let result: number;

                if (a.group > b.group) {
                    result = 1;
                } else if (a.group < b.group) {
                    result = -1;
                } else {
                    result = 0;
                }

                return result;
            });
        };

        const grouped = properties.reduce(groupByGroupCode, {});
        const sorted = sortByGroupName(Object.values(grouped));

        return sorted;
    }

    @action
    public handleOnChange = (item: BackendProperty, name: string, value: string) => {
        item.value.setValue(value.toString());
    }

    public get canSave () {
        if (this.isSaving) {
            return false;
        }

        return (this.bProperties.filter((item: BackendProperty) => item.getDirty()).length > 0);
    }

    @action
    public handleSave = (): Promise<void> => {
        this.setSaving(true);

        const changes: any = this.bProperties
            .filter((item: BackendProperty) => item.getDirty())
            .reduce((result: any, item: BackendProperty) => {
                result[item.name.getValue()] = item.value.getValue();

                if (item.type.getValue() === 'Boolean') {
                   result[item.name.getValue()] = result[item.name.getValue()] === 'true';
                }

                return result;
            }, {});

        return BackendPropertyService.updateBackendProperties(changes).then(
            this.handleRequestSuccess,
            this.handleRequestFailed
        );
    };

    @action
    private handleRequestSuccess = () => {
        this.setSaving(false);

        Dialog.showMessage(
            '',
            i18n.t('admin.backendProperties>backendPropertiesSaveSuccessText'),
        );

        this.loadBackendProperties();

        return Promise.resolve();
    };

    @action
    private handleRequestFailed = (er?: Error) => {
        let message: string;

        this.setSaving(false);

        if (er instanceof Array) {
            message = er.map((error) => error.userMessageDetails || error.userMessage).join(' ');
        } else {
            message = i18n.t('admin.backendProperties>backendPropertiesSaveFailureTitle');
        }

        Dialog.showError(message, i18n.t('admin.backendProperties>backend_propertiesSaveFailureText'));

        return Promise.reject(er);
    };

    @action
    public setSaving = (isSaving: boolean) => {
        this.isSaving = isSaving;
    }
}
