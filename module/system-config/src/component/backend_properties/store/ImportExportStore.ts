/**
 * BackendProperties import / export store
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import {action, observable} from 'mobx';
import {ImportExportOperations} from '../../../type/ImportExportWizard';
import {BackendPropertyService} from '../../../service/BackendPropertyService';
import {UploadFile} from '@unidata/uikit';

export class ImportExportStore {
    @observable public isSaving: boolean = false;
    @observable public file: UploadFile | null = null;
    @observable public done: boolean = false;

    @observable public currentOperation: ImportExportOperations | null;

    @action
    public handleChooseAction = (currentOperation: ImportExportOperations | null) => {
        this.currentOperation = currentOperation;
    };

    @action
    public handleUploadChange = (file: UploadFile) => {
        this.file = file;
    };

    @action
    public handleImport = (): Promise<void> => {
        if (this.isSaving || !this.file) {
            return Promise.reject();
        }

        this.isSaving = true;

        return BackendPropertyService.importBackendProperties(this.file).then(action(() => {
            this.done = true;
            this.initState();
        })).catch(() => {
            this.isSaving = false;
        });
    };

    @action
    public handleExport = (): Promise<void> => {
        this.isSaving = true;

        return BackendPropertyService.exportBackendProperties().then(action(() => {
            this.initState();
        })).catch(() => {
            this.isSaving = false;
        });
    };

    @action
    public initState = () => {
        this.currentOperation = null;
        this.file = null;
        this.done = false;
        this.isSaving = false;
    }
}
