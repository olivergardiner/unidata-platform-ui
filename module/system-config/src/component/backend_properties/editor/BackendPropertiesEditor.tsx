﻿/**
 * Component that displays the list of Backend Properties
 *
 * @author: Ivan Marshalkin
 * @date: 2019-07-11
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {PropertyGroup} from './PropertyGroup';
import {Button, CardPanel, Editor, GROUP_TYPE, INTENT} from '@unidata/uikit';
import {BackendPropertiesEditorStore} from '../store/EditorStore';

interface IProps {
    readOnly?: boolean;
    store: BackendPropertiesEditorStore;
    openModal: (isOpen: boolean) => void;
}

@observer
export class BackendPropertiesEditor extends React.Component<IProps> {

    get store () {
        return this.props.store;
    }

    get readOnly () {
        return this.props.readOnly;
    }

    get extraButtons () {
        const handleImportExportClick = () => {
            this.props.openModal(true);
        };

        return (
            <React.Fragment>
                <Button
                    name='importOrExport'
                    isRound={true}
                    isGhost={true}
                    onClick={handleImportExportClick}
                 >
                    {`${i18n.t('import')} / ${i18n.t('export')}`}
                </Button>
                &nbsp;
                &nbsp;
                {!this.props.readOnly && (
                    <Button
                        leftIcon='save'
                        name='save'
                        isRound={true}
                        isDisabled={!this.store.canSave}
                        onClick={this.store.handleSave}
                    >
                        {i18n.t('save')}
                    </Button>
                )}
            </React.Fragment>
        );
    }

    render () {
        const readOnly = this.readOnly;

        if (this.store.backendProperties.length === 0) {
            return null;
        }

        return (
            <Editor
                title={i18n.t('admin.backendProperties>title')}
                groupSectionTitle={i18n.t('page.header>administration')}
                extraButtons={this.extraButtons}
                iconType='wrench'
                padded='very'
            >
                <CardPanel.Group groupType={GROUP_TYPE.VERTICAL}>
                    {this.store.backendProperties.map((section: any) =>
                        <CardPanel
                            key={section.groupCode}
                            title={section.group}
                            isCollapsed={!this.store.activeKeys.includes(section.groupCode)}
                        >
                            <PropertyGroup
                                section={section}
                                readOnly={readOnly}
                                onChange={this.store.handleOnChange} />
                        </CardPanel>
                    )}
                </CardPanel.Group>
            </Editor>
        );
    }
}
