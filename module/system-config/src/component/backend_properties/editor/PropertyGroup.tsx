/**
 * Property Group Component
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {BackendProperty} from '../../../model';
import {Field} from '@unidata/uikit';
import {BackendPropertySectionType} from '../../../type/BackendProperties';

interface IProps {
    section: BackendPropertySectionType;
    onChange: (item: BackendProperty, name: string, value: string) => void;
    readOnly?: boolean;
}

const inputTypes = {
    String: 'text',
    Integer: 'text'
} as {[key: string]: string};

@observer
export class PropertyGroup extends React.Component<IProps> {

    static inputTypes = inputTypes;

    get items () {
        return this.props.section.backendProperties;
    }

    buildFieldFor (item: BackendProperty): JSX.Element {

        const commonProps = {
            key: item.name.getValue(),
            name: item.name.getValue(),
            label: item.displayName.getValue(),
            sublabel: item.name.getValue(),
            disabled: this.props.readOnly || item.meta.readonly,
            error: item.lastValidationResult.values().next().value
        };

        const onChange = (item: BackendProperty, name: string, value: string) => {
            this.props.onChange(item, name, value);
        };

        const checkboxBuilder = (): JSX.Element => {

            const getValue = () => {
                return item.value.getValue() === true;
            };

            return (
                <Field.Checkbox
                    {...commonProps}
                    defaultChecked={getValue()}
                    onChange={onChange.bind(this, item)}
                />
            );
        };

        const inputBuilder = (): JSX.Element => {
            return (
                <Field.Input
                    {...commonProps}
                    type={inputTypes[item.type.getValue()] as 'text' | 'number'}
                    defaultValue={item.value.getValue()}
                    onChange={onChange.bind(this, item)}
                />
            );
        };

        switch (item.type.getValue()) {
            case 'Boolean':
                return checkboxBuilder();
            default:
                return inputBuilder();
        }
    }

    render () {
        return (
            <React.Fragment>
                {this.items.map((item: BackendProperty) =>
                    this.buildFieldFor(item)
                )}
            </React.Fragment>
        );
    }
}
