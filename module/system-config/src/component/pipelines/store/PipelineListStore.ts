/**
 * Pipeline list store
 *
 * @author: Ivan Marshalkin
 * @date: 2019-11-26
 */

import {PipelineService} from '../../../service/index';
import {action, observable} from 'mobx';
import {Pipeline} from '../../../model/pipeline/Pipeline';
import {Nullable} from '@unidata/core';
import {Dialog, i18n} from '@unidata/core-app';

interface IPipelineListStoreOptions {
    onSelectPipeline?: (pipeline: Pipeline) => null; // function called when selecting a pipeline in the list
}

export class PipelineListStore {
    // a list of loaded on our pipeline
    @observable.shallow
    private pipelines: Pipeline[] = [];

    // the currently selected pipeline
    @observable
    private selectedPipeline: Nullable<Pipeline> = null;

    @observable
    public filterText: string = '';

    // additional options passed to the store
    private options: IPipelineListStoreOptions;

    constructor (option: IPipelineListStoreOptions) {
        this.options = option;

        this.loadPipelines();
    }

    /**
     * Loads pipelines
     */
    @action
    public loadPipelines () {
        const self = this;

        PipelineService
            .getPipelineList()
            .then(
                this.successLoadPipelines.bind(this),
                this.failureLoadPipelines.bind(this)
            );
    }

    /**
     * Handler for successful pipeline loading
     *
     * @param pipelines
     */
    @action
    private successLoadPipelines (pipelines: Pipeline[]) {
        this.pipelines = pipelines;
    }

    /**
     * Handler for failed pipeline loading
     *
     * @param pipelines
     */
    @action
    private failureLoadPipelines (pipelines: Pipeline[]) {
        Dialog.showError(i18n.t('admin.pipelines>pipelineLoadFailureText'));
    }

    /**
     * Sets the selected pipeline
     *
     * @param pipeline
     */
    @action
    public setSelectedPipeline (pipeline: Pipeline) {
        const options = this.options;

        this.selectedPipeline = pipeline;

        if (options.onSelectPipeline) {
            options.onSelectPipeline(pipeline);
        }
    }

    /**
     * Resets the currently selected pipeline
     */
    @action
    public resetSelectedPipeline () {
        this.selectedPipeline = null;
    }

    /**
     * Returns the selected pipeline
     */
    public getSelectedPipeline (): Nullable<Pipeline> {
        return this.selectedPipeline;
    }

    /**
     * Returns a list of loaded pipelines
     */
    public getPipelines (): Pipeline[] {
        return this.pipelines;
    }

    /**
     * Creates a new pipeline
     */
    @action
    public createPipeline () {
        let pipeline: Pipeline = new Pipeline({});

        this.pipelines.push(pipeline);

        this.setSelectedPipeline(pipeline);
    }

    @action
    public setFilterText (text: string) {
        this.filterText = text;
    }
}
