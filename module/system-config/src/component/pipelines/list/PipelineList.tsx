/**
 * List of pipelines
 *
 * @author Ivan Marshalkin
 * @date 2019-11-26
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {hasPrintableChars, i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {PipelineListStore} from '../store/PipelineListStore';
import {Pipeline} from '../../../model/pipeline/Pipeline';
import {Button, Confirm, Input, INTENT, MenuSider, SIZE} from '@unidata/uikit';
import * as style from './PipelineList.m.scss';
import {action} from 'mobx';
import {Nullable} from '@unidata/core';

interface IProps {
    readOnly?: boolean;
    pipelineListStore: PipelineListStore;
}

interface IInjectProps extends IProps {
}

interface IState {
    isConfirmOpen: boolean;
    pipelineConfirmation: Nullable<Pipeline>;
}

@observer
export class PipelineList extends React.Component<IProps, IState> {
    state = {
        isConfirmOpen: false,
        pipelineConfirmation: null
    }

    get injected () {
        return this.props as IInjectProps;
    }

    get pipelineListStore () {
        return this.props.pipelineListStore;
    }

    get readOnly () {
        return Boolean(this.props.readOnly);
    }

    get pipelineListData () {
        const store = this.pipelineListStore;

        let filterText = store.filterText;

        return store.getPipelines()
                    .map((item) => {
                        return {
                            key: item.startId.getValue(),
                            name: item.startId.getValue(),
                            description: item.description.getValue(),
                            model: item
                        };
                    })
                    .filter(function (item) {
                        let description = item.description,
                            name = item.name;

                        if (!hasPrintableChars(filterText)) {
                            return true;
                        }

                        filterText = filterText.toUpperCase();

                        return description.toUpperCase().indexOf(filterText) !== -1 || name.toUpperCase().indexOf(filterText) !== -1;
                    });
    }

    get actions () {
        const canCreatePipeline = (!this.readOnly &&
            ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.ROLE_MANAGEMENT], Right.CREATE));

        if (!canCreatePipeline) {
            return null;
        }

        return (
            <Button
                key='button-add'
                isRound={true}
                name='createRole'
                size={SIZE.LARGE}
                onClick={this.onCreatePipelineButtonClick.bind(this)}
                title={i18n.t('admin.role>create')}
                leftIcon={'plus'}
                intent={INTENT.SECONDARY}
            />
        );
    }

    get gridColumns () {
        return [
            {
                accessor: 'displayName',
                id: 'displayName',
                Cell: (cell: any) => {
                    let pipeline: Pipeline = cell.original.model;

                    return (
                        <div className={style.cellWrapper}>
                            <div className={style.cellLIne}>{pipeline.startId.getValue()}</div>
                            <div className={style.cellLIne}>{pipeline.description.getValue()}</div>
                        </div>
                    );
                }
            }
        ];
    }

    get listFilters () {
        const self = this;

        return (
            <Input
                defaultValue={this.pipelineListStore.filterText}
                onChange={function (event) {
                    self.pipelineListStore.setFilterText(event.target.value);
                }}
            />
        );
    }

    onPipelineListItemClick (pipeline: Pipeline) {
        let store = this.pipelineListStore,
            selectedPipeline = store.getSelectedPipeline();

        if (selectedPipeline && selectedPipeline.getDirty()) {
            this.setState({
                isConfirmOpen: true,
                pipelineConfirmation: pipeline
            });

            return;
        }

        store.setSelectedPipeline(pipeline);
    }

    onCreatePipelineButtonClick () {
        this.pipelineListStore.createPipeline();
    }

    @action
    onConfirm = () => {
        let store = this.pipelineListStore,
            selectedPipeline = store.getSelectedPipeline(),
            pipelineConfirmation = this.state.pipelineConfirmation;

        if (!selectedPipeline) {
            return;
        }

        selectedPipeline.revert();

        if (pipelineConfirmation) {
            store.setSelectedPipeline(pipelineConfirmation);
        }

        this.closeConfirmation();
    }

    onConfirmClose = () => {
        this.closeConfirmation();
    }

    closeConfirmation = () => {
        this.setState({
            isConfirmOpen: false,
            pipelineConfirmation: null
        });
    }

    render () {
        return (
            <React.Fragment>
                <MenuSider
                    title={i18n.t('admin.pipelines>pipelineListTitle')}
                    data={this.pipelineListData}
                    columns={this.gridColumns}
                    currentItem={this.pipelineListStore.getSelectedPipeline()}
                    onClick={this.onPipelineListItemClick.bind(this)}
                    actions={this.actions}
                    hideHeader={true}
                    filters={this.listFilters}
                />

                <Confirm
                    onConfirm={this.onConfirm}
                    confirmationMessage={i18n.t('unsavedChanges')}
                    isOpen={this.state.isConfirmOpen}
                    onClose={this.onConfirmClose}
                />
            </React.Fragment>
        );
    }
}
