/**
 * Pipeline editor
 *
 * @author Ivan Marshalkin
 * @date 2019-11-26
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {Button, ButtonWithConfirmation, Divider, Editor, Field, INTENT, PLACEMENT} from '@unidata/uikit';
import {PipelineEditorStore} from './store/PipelineEditorStore';
import {StandaloneSegment} from './segment/standalone/StandaloneSegment';
import {NonStandaloneSegmentList} from './segment/list/NonStandaloneSegmentList';
import {SegmentType} from '../../../model/pipeline/type/SegmentType';
import * as style from './PipelineEditor.m.scss';

interface IProps {
    pipelineEditorStore: PipelineEditorStore;
}

interface IInjectProps extends IProps {
}

@observer
export class PipelineEditor extends React.Component<IProps> {
    constructor (props: Readonly<IProps>) {
        super(props);

        this.onSaveButtonClick = this.onSaveButtonClick.bind(this);
        this.onDeleteButtonClick = this.onDeleteButtonClick.bind(this);
        this.onSubjectChangeChange = this.onSubjectChangeChange.bind(this);
    }

    onSaveButtonClick () {
        let pipelineEditorStore = this.props.pipelineEditorStore;

        pipelineEditorStore.savePipeline();
    }

    onDeleteButtonClick () {
        let pipelineEditorStore = this.props.pipelineEditorStore;

        pipelineEditorStore.deletePipeline();
    }

    get extraButtons () {
        const pipelineEditorStore = this.props.pipelineEditorStore;

        return (
            <React.Fragment>
                {pipelineEditorStore.saveButtonVisible && (
                    <>
                        <Button
                            isRound={true}
                            leftIcon={'save'}
                            intent={INTENT.PRIMARY}
                            name='savePipeline'
                            isDisabled={!pipelineEditorStore.saveButtonEnabled}
                            onClick={this.onSaveButtonClick}
                        >
                            {i18n.t('admin.pipelines>saveButtonText')}
                        </Button>
                        &nbsp;
                        &nbsp;
                    </>
                )}
                {pipelineEditorStore.deleteButtonVisible && (
                    <ButtonWithConfirmation
                        isRound={true}
                        leftIcon={'trash2'}
                        isDisabled={!pipelineEditorStore.deleteButtonEnabled}
                        onConfirm={this.onDeleteButtonClick}
                        tooltipPlacement={PLACEMENT.BOTTOM_START}
                        confirmationMessage={i18n.t('admin.pipelines>deleteConfirmationText')}
                        title={i18n.t('admin.pipelines>deleteConfirmationTitle')}
                    />
                )}
            </React.Fragment>
        );
    }

    onSubjectChangeChange (name: string, value: string) {
        const pipelineEditorStore = this.props.pipelineEditorStore;

        pipelineEditorStore.getPipeline().subjectId.setValue(value);
    }


    render () {
        const pipelineEditorStore = this.props.pipelineEditorStore;

        return (
            <React.Fragment>
                <Editor
                    title={i18n.t('admin.pipelines>editorTitle')}
                    groupSectionTitle={i18n.t('page.header>administration')}
                    iconType='user'
                    extraButtons={this.extraButtons}
                >
                    <div className={style.piplineEditorContainer}>
                        <Field.Input
                            type='text'
                            label={i18n.t('admin.pipelines>subject') + ':'}
                            labelWidth={10}
                            inputWidth={90}
                            defaultValue={pipelineEditorStore.getPipeline().subjectId.getValue()}
                            onChange={this.onSubjectChangeChange}
                        />

                        <Divider/>

                        <StandaloneSegment
                            type={SegmentType.START}
                            segment={pipelineEditorStore.startSegment}
                            pipelineEditorStore={pipelineEditorStore}
                        />

                        <NonStandaloneSegmentList
                            pipelineEditorStore={pipelineEditorStore}
                        />

                        <Divider/>

                        <StandaloneSegment
                            type={SegmentType.FALLBACK}
                            segment={pipelineEditorStore.fallbackSegment}
                            pipelineEditorStore={pipelineEditorStore}
                        />

                        <Divider/>

                        <StandaloneSegment
                            type={SegmentType.FINISH}
                            segment={pipelineEditorStore.finishSegment}
                            pipelineEditorStore={pipelineEditorStore}
                        />
                    </div>
                </Editor>
            </React.Fragment>
        );
    }
}
