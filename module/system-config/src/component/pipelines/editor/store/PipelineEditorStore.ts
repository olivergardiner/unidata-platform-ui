/**
 * Pipeline editor's store
 *
 * @author: Ivan Marshalkin
 * @date: 2019-11-26
 */

import {PipelineService} from '../../../../service/PipelineService';
import {action, computed, observable} from 'mobx';
import {Pipeline} from '../../../../model/pipeline/Pipeline';
import {Nullable, ReactiveProp} from '@unidata/core';
import {Segment} from '../../../../model/pipeline/Segment';
import isEqual from 'lodash/isEqual';
import {SegmentType, StandaloneSegmentType} from '../../../../model/pipeline/type/SegmentType';
import {Dialog, i18n} from '@unidata/core-app';

interface IPipelineEditorStoreOptions {
    onDeletePipeline?: () => null; // function called when successfully deleting the pipeline
    onSavePipeline?: () => null; // function called when saving the pipeline successfully
}

export class PipelineEditorStore {
    @observable
    private pipeline: Pipeline;

    // the list of segments uniting startSegments, acceptingSegments
    @observable
    private pipelineSegments: Segment[] = [];

    // segments that can act as starting points
    private startSegments: Segment[] = [];

    // segments that can be applied to the currently selected start segment
    private acceptingSegments: Segment[] = [];

    // an indication that a re-sorting was performed, in reality, may
    @observable
    private reordered: boolean = false;

    // additional options passed to the side
    private options: IPipelineEditorStoreOptions;

    constructor (pipeline: Pipeline, options: IPipelineEditorStoreOptions) {
        this.options = options;
        this.setPipeline(pipeline);

        this.loadSegments();
    }

    /**
     * Sets the current pipeline
     *
     * @param pipeline
     */
    @action
    private setPipeline (pipeline: Pipeline) {
        pipeline.setReactive([ReactiveProp.DIRTY]);
        this.pipeline = pipeline;
    }

    /**
     * Returns the current pipeline
     */
    public getPipeline (): Pipeline {
        return this.pipeline;
    }

    /**
     * Loads segments that can be applied to the current pipeline
     */
    private loadSegments () {
        const startSegment = this.startSegment;

        let promises: Array<Promise<any>> = [];
        let startSegmentPromise: Promise<Segment[]>;
        let acceptingSegmentPromise: Promise<Segment[]>;
        let promiseAll: Array<Promise<any>> = [];

        startSegmentPromise = PipelineService.getStartSegments();
        startSegmentPromise.then(
            this.onSuccessStartSegmentsLoad.bind(this),
            this.onFailureStartSegmentsLoad.bind(this)
        );

        promiseAll.push(startSegmentPromise);

        if (startSegment && !this.pipeline.getPhantom()) {
            acceptingSegmentPromise = this.loadAcceptingSegments(startSegment.id.getValue());
            promiseAll.push(acceptingSegmentPromise);
        }

        Promise
            .all(promiseAll)
            .then(
                function () {
                },
                function () {
                }
            );
    }

    private loadAcceptingSegments (segmentId: string): Promise<Segment[]> {
        let acceptingSegmentPromise: Promise<Segment[]>;

        acceptingSegmentPromise = PipelineService.getAcceptingSegments(segmentId);

        acceptingSegmentPromise.then(
            this.onSuccessSegmentsLoad.bind(this),
            this.onFailureSegmentsLoad.bind(this)
        );

        return acceptingSegmentPromise;
    }

    @action
    private onSuccessStartSegmentsLoad (segments: Segment[]) {
        this.startSegments = segments;

        this.setPipelineSegments();
    }

    private onFailureStartSegmentsLoad () {
    }

    @action
    private onSuccessSegmentsLoad (segments: Segment[]) {
        this.acceptingSegments = segments;

        this.setPipelineSegments();
    }

    private onFailureSegmentsLoad () {
    }

    private setPipelineSegments () {
        const self = this;
        const start = this.startSegments;

        this.pipelineSegments = [];

        if (start) {
            start.forEach(function (segment) {
                self.pipelineSegments.push(segment);
            });
        }

        this.acceptingSegments.forEach(function (segment) {
            self.pipelineSegments.push(segment);
        });
    }

    /**
     * Current START segment of the pipeline
     */
    @computed
    public get startSegment (): Nullable<Segment> {
        return this.pipeline.segments.find2(function (item) {
            return item.segmentType.getValue() === SegmentType.START;
        });
    }

    /**
     * Current FINISH pipeline segment
     */
    @computed
    public get finishSegment (): Nullable<Segment> {
        return this.pipeline.segments.find2(function (item) {
            return item.segmentType.getValue() === SegmentType.FINISH;
        });
    }

    /**
     * Current FALLBACK segment of the pipeline
     */
    @computed
    public get fallbackSegment (): Nullable<Segment> {
        return this.pipeline.segments.find2(function (item) {
            return item.segmentType.getValue() === SegmentType.FALLBACK;
        });
    }

    /**
     * List of logically selected segments in the current pipeline
     */
    private get standaloneSegments ():  Segment[] {
        const standaloneSegments = [
            this.startSegment,
            this.finishSegment,
            this.fallbackSegment
        ];

        let result: Segment[] = [];

        standaloneSegments.forEach(function (value) {
            if (value) {
                result.push(value);
            }
        });

        return result;
    }

    /**
     * List of regular segments in the current pipeline
     */
    public get nonStandaloneSegments (): Segment[] {
        const self = this,
            standaloneSegments = this.standaloneSegments;

        let result: Segment[] = [];

        this.pipeline.segments.forEach(function (item) {
            if (standaloneSegments.indexOf(item) === -1) {
                result.push(item);
            }
        });

        return result;
    }

    /**
     * Returns true if the segment is logically selected
     *
     * @param segment
     */
    private isStandaloneSegment (segment: Segment) {
        const segments = this.standaloneSegments,
            standaloneSegmentTypes: string[] = [SegmentType.START, SegmentType.FINISH, SegmentType.FALLBACK];

        return standaloneSegmentTypes.indexOf(segment.segmentType.getValue()) !== -1;
    }

    /**
     * Returns true if the segment is normal
     *
     * @param segment
     */
    private isNonStandaloneSegment (segment: Segment) {
        return !this.isStandaloneSegment(segment);
    }

    /**
     * Deletes a segment from the pipeline
     *
     * @param segment
     */
    @action
    public deleteSegment (segment: Segment) {
        // if the starting segment is deleted, then the rest are deleted after it, because it determines applicability
        if (this.isStandaloneSegment(segment) && segment.segmentType.getValue() === SegmentType.START) {
            this.pipeline.segments.removeAll();
            this.acceptingSegments = [];
            this.setPipelineSegments();
        } else {
            this.pipeline.segments.remove(segment);
        }
    }

    /**
     * Returns a list of segments that are applicable to the pipeline
     */
    public getPipelineSegments () {
        return this.pipelineSegments;
    }

    /**
     * Adds a logically selected segment
     *
     * @param type
     * @param segment
     */
    @action
    public addStandaloneSegment (type: StandaloneSegmentType, segment: Segment) {
        const fallbackSegment = this.fallbackSegment;
        const finishSegment = this.finishSegment;

        let acceptingSegmentPromise: Promise<Segment[]>;

        switch (type) {
            case SegmentType.START:
                // the starting segment is always the first
                this.pipeline.segments.insertFirst(segment);
                this.pipeline.startId.setValue(segment.id.getValue());

                acceptingSegmentPromise = this.loadAcceptingSegments(segment.id.getValue());
                break;
            case SegmentType.FINISH:
                // finish the segment is always the last
                this.pipeline.segments.insertLast(segment);

                break;
            case SegmentType.FALLBACK:
                // after fallback either the last one or before finish
                if (finishSegment) {
                    this.pipeline.segments.inserBefore(segment, finishSegment);
                } else {
                    this.pipeline.segments.insertLast(segment);
                }

                break;
        }
    }

    /**
     * Adds a segment
     *
     * @param segment
     */
    @action
    public addNonStandaloneSegment (segment: Segment) {
        const fallbackSegment = this.fallbackSegment,
            finishSegment = this.finishSegment,
            pipeline = this.pipeline;

        if (fallbackSegment) {
            pipeline.segments.inserBefore(segment, fallbackSegment);
        } else if (finishSegment) {
            pipeline.segments.inserBefore(segment, finishSegment);
        } else {
            pipeline.segments.insertLast(segment);
        }
    }

    /**
     * Moves a segment higher
     *
     * @param segment
     */
    @action
    public liftPointSegmentUp (segment: Segment) {
        const pipeline = this.pipeline,
            index = pipeline.segments.indexOf(segment),
            sibling = pipeline.segments.get(index - 1);

        if (this.isStandaloneSegment(segment)) {
            return;
        }

        if (sibling && this.isStandaloneSegment(sibling)) {
            return;
        }

        pipeline.segments.swapItems(segment, sibling);

        this.calculateReorderedFlag();
    }

    /**
     * Moves the segment lower
     *
     * @param segment
     */
    @action
    public liftPointSegmentDown (segment: Segment) {
        const pipeline = this.pipeline,
            index = pipeline.segments.indexOf(segment),
            sibling = pipeline.segments.get(index + 1);

        if (this.isStandaloneSegment(segment)) {
            return;
        }

        if (sibling && this.isStandaloneSegment(sibling)) {
            return;
        }

        pipeline.segments.swapItems(segment, sibling);

        this.calculateReorderedFlag();
    }

    public isFirstNonStandaloneSegment (segment: Segment): boolean {
        const pipeline = this.pipeline,
            index = pipeline.segments.indexOf(segment),
            sibling = pipeline.segments.get(index - 1);

        if (!sibling) {
            return true;
        }

        if (sibling && this.isStandaloneSegment(sibling)) {
            return true;
        }

        return false;
    }

    public isLastNonStandaloneSegment (segment: Segment): boolean {
        const pipeline = this.pipeline,
            index = pipeline.segments.indexOf(segment),
            sibling = pipeline.segments.get(index + 1);

        if (!sibling) {
            return true;
        }

        if (sibling && this.isStandaloneSegment(sibling)) {
            return true;
        }

        return false;
    }

    public get saveButtonEnabled (): boolean {
        return this.pipeline.getDirty() === true || this.reordered;
    }

    public get saveButtonVisible (): boolean {
        return true;
    }

    public get deleteButtonEnabled (): boolean {
        return true;
    }

    public get deleteButtonVisible (): boolean {
        return true;
    }

    /**
     * Counts the sign that was used for re-ordering segments
     */
    @action
    private calculateReorderedFlag () {
        const pipeline = this.pipeline,
            items = pipeline.segments.getRange(),
            originalItems = pipeline.segments.getOriginal();

        this.reordered = isEqual(items, originalItems) ? false : true;
    }

    /**
     * Returns a list of segments (logically selected segments) available for selection in the modal window.
     *
     * @param type
     */
    public getAwailableStandaloneSegmentsByType (type: StandaloneSegmentType): Segment[] {
        let result: Segment[] = [],
            usedStandaloneSegmentsType: string[] = [];

        this.standaloneSegments.forEach(function (segment) {
            usedStandaloneSegmentsType.push(segment.segmentType.getValue());
        });

        this.pipelineSegments.forEach(function (segment) {
            if (segment.segmentType.getValue() === type && usedStandaloneSegmentsType.indexOf(type) === -1) {
                result.push(segment);
            }
        });

        return result;
    }

    /**
     * Returns a list of segments (regular segments) available for selection in the modal window.
     */
    public getAwailableNonStandaloneSegments () {
        const self = this,
            usedNonStandaloneSegmentIds: string[] = [];

        let result: Segment[] = [];

        this.nonStandaloneSegments.forEach(function (segment) {
            usedNonStandaloneSegmentIds.push(segment.id.getValue());
        });

        this.pipelineSegments.forEach(function (segment) {
            if (self.isNonStandaloneSegment(segment) && usedNonStandaloneSegmentIds.indexOf(segment.id.getValue()) === -1) {
                result.push(segment);
            }
        });

        return result;
    }

    /**
     * Saves the pipeline
     */
    public savePipeline () {
        PipelineService
            .savePipeline(this.pipeline)
            .then(
                this.onPipelineSaveSuccess.bind(this),
                this.onPipelineSaveFailure.bind(this)
            );
    }

    public onPipelineSaveSuccess () {
        const options = this.options;

        Dialog.showMessage(i18n.t('admin.pipelines>pipelineSaveSuccessText'));

        if (options.onSavePipeline) {
            options.onSavePipeline();
        }
    }

    public onPipelineSaveFailure () {
        Dialog.showError(i18n.t('admin.pipelines>pipelineSaveFailureText'));
    }

    /**
     * Deletes the pipeline
     */
    public deletePipeline () {
        PipelineService
            .deletePipeline(this.pipeline)
            .then(
                this.onPipelineDeleteSuccess.bind(this),
                this.onPipelineDeleteFailure.bind(this)
            );
    }

    public onPipelineDeleteSuccess () {
        const options = this.options;

        Dialog.showError(i18n.t('admin.pipelines>pipelineDeleteSuccessText'));

        if (options.onDeletePipeline) {
            options.onDeletePipeline();
        }
    }

    public onPipelineDeleteFailure () {
        Dialog.showError(i18n.t('admin.pipelines>pipelineDeleteFailureText'));
    }
}
