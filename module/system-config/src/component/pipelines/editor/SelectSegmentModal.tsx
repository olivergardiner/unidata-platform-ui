/**
 * Modal window for selecting a segment
 *
 * @author Ivan Marshalkin
 * @date 2019-11-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {DropDown, Modal} from '@unidata/uikit';
import {Segment} from '../../../model/pipeline/Segment';
import {i18n} from '@unidata/core-app';
import * as style from './SelectSegmentModal.m.scss';

interface IProps {
    open: boolean;
    onClose: () => void;
    onSelect: ((segment: Segment) => void);
    segments: Segment[];
}

interface IInjectProps extends IProps {
}

interface IState {
    isOpen: boolean;
}

@observer
export class SelectSegmentModal extends React.Component<IProps, IState> {
    constructor (props: Readonly<IProps>) {
        super(props);

        this.onSegmentClick = this.onSegmentClick.bind(this);
    }

    get modalFooter () {
        return null;
    }

    get modalHeader () {
        return (
            i18n.t('admin.pipelines>segmentModalTitle')
        );
    }

    onSegmentClick (segment: Segment) {
        this.props.onSelect(segment);
    }

    render () {
        const self = this;

        if (!this.props.open) {
            return null;
        }

        return (
            <Modal
                isOpen={this.props.open}
                onClose={this.props.onClose}
                footer={this.modalFooter}
                header={this.modalHeader}
            >
                <div className={style.modalContainer}>
                    {
                        this.props.segments.map(function (segment) {
                            return (
                                <DropDown.Item
                                    key={segment.id.getValue()}
                                    onClick={() => {
                                        self.onSegmentClick(segment);
                                    }}
                                >
                                    {segment.id.getValue()}
                                </DropDown.Item>
                            );
                        })
                    }
                </div>
            </Modal>
        );
    }
}
