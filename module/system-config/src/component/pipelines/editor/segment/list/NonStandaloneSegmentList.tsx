/**
 * The editor of a sequence of segments
 *
 * @author Ivan Marshalkin
 * @date 2019-11-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {PipelineEditorStore} from '../../store/PipelineEditorStore';
import {Segment} from '../../../../../model/pipeline/Segment';
import {SelectSegmentModal} from '../../SelectSegmentModal';
import {NonStandaloneSegment} from './NonStandaloneSegment';
import {Button, INTENT, SIZE} from '@unidata/uikit';
import * as style from './NonStandaloneSegmentList.m.scss';
import {i18n} from '@unidata/core-app';

interface IProps {
    pipelineEditorStore: PipelineEditorStore;
}

interface IInjectProps extends IProps {
}

interface IState {
    isOpen: boolean;
}

@observer
export class NonStandaloneSegmentList extends React.Component<IProps, IState> {
    state = {
        isOpen: false
    };

    constructor (props: Readonly<IProps>) {
        super(props);

        this.onDeleteSegmentButtonClick = this.onDeleteSegmentButtonClick.bind(this);
        this.onAddNonStandaloneSegmentButtonClick = this.onAddNonStandaloneSegmentButtonClick.bind(this);
        this.onSelectPipeline = this.onSelectPipeline.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
    }

    onDeleteSegmentButtonClick (segment: Segment) {
        const props = this.props,
            pipelineEditorStore = props.pipelineEditorStore;

        if (segment) {
            pipelineEditorStore.deleteSegment(segment);
        }
    }

    onAddNonStandaloneSegmentButtonClick () {
        this.showModal();
    }

    toggleModalState () {
        this.setState((prevState) => {
            return {
                isOpen: !prevState.isOpen
            };
        });
    }

    showModal () {
        if (!this.state.isOpen) {
            this.toggleModalState();
        }
    }

    hideModal () {
        if (this.state.isOpen) {
            this.toggleModalState();
        }
    }

    onCloseModal () {
        this.hideModal();
    }

    onSelectPipeline (segment: Segment) {
        const props = this.props,
            pipelineEditorStore = props.pipelineEditorStore;

        pipelineEditorStore.addNonStandaloneSegment(new Segment({
            id: segment.id.getValue(),
            segmentType: segment.segmentType.getValue(),
            description: segment.description.getValue()
        }));

        this.hideModal();
    }

    render () {
        const props = this.props,
            pipelineEditorStore = props.pipelineEditorStore;

        return (
            <div className={style.segmentListContainer}>
                {
                    props.pipelineEditorStore.nonStandaloneSegments.map(function (segment) {
                        return (
                            <div key={segment.modelId}>
                                <br/>

                                <NonStandaloneSegment
                                    segment={segment}
                                    pipelineEditorStore={pipelineEditorStore}
                                />
                            </div>
                        );
                    })
                }
                <br/>

                <Button
                    isRound={true}
                    intent={INTENT.SECONDARY}
                    onClick={this.onAddNonStandaloneSegmentButtonClick}
                    size={SIZE.MIDDLE}
                >
                    {i18n.t('admin.pipelines>addNewSegment')}
                </Button>

                <SelectSegmentModal
                    open={this.state.isOpen}
                    onClose={this.onCloseModal}
                    onSelect={this.onSelectPipeline}
                    segments={pipelineEditorStore.getAwailableNonStandaloneSegments()}
                />
            </div>
        );
    }
}
