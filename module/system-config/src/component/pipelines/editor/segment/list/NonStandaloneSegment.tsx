/**
 * Editor of a segment in the list
 *
 * @author Ivan Marshalkin
 * @date 2019-11-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {PipelineEditorStore} from '../../store/PipelineEditorStore';
import {Segment} from '../../../../../model/pipeline/Segment';
import {SegmentBar} from '../SegmentBar';
import {Button, CardPanel, INTENT, SIZE, Tag} from '@unidata/uikit';
import * as style from './NonStandaloneSegment.m.scss';
import {i18n} from '@unidata/core-app';
import {SegmentType} from '../../../../../model/pipeline/type/SegmentType';

interface IProps {
    segment: Segment;
    pipelineEditorStore: PipelineEditorStore;
}

interface IInjectProps extends IProps {
}

interface IState {
    isOpen: boolean;
}

@observer
export class NonStandaloneSegment extends React.Component<IProps, IState> {
    state = {
        isOpen: false
    };

    constructor (props: Readonly<IProps>) {
        super(props);

        this.onDeleteSegmentButtonClick = this.onDeleteSegmentButtonClick.bind(this);
    }

    onDeleteSegmentButtonClick () {
        const props = this.props,
            pipelineEditorStore = props.pipelineEditorStore,
            segment = props.segment;

        if (segment) {
            pipelineEditorStore.deleteSegment(segment);
        }
    }

    onLiftSegmentUpButtonClick (segment: Segment) {
        const props = this.props,
            pipelineEditorStore = props.pipelineEditorStore;

        pipelineEditorStore.liftPointSegmentUp(segment);
    }

    onLiftSegmentDownButtonClick (segment: Segment) {
        const props = this.props,
            pipelineEditorStore = props.pipelineEditorStore;

        pipelineEditorStore.liftPointSegmentDown(segment);
    }

    get segmentPanelTitle () {
        return (
            <div>
                {i18n.t('admin.pipelines>segmentType')}&nbsp;&gt;&nbsp;
                <Tag color={this.segmentTagColor}>
                    {this.props.segment.segmentType.getValue()}
                </Tag>
            </div>
        );
    }

    get segmentTagColor (): string {
        switch (this.props.segment.segmentType.getValue()) {
            case SegmentType.CONNECTOR:
                return '#9ADDDD';
            case SegmentType.POINT:
                return '#6699CC';
            default:
                return '';
        }
    }

    get extraButtons () {
        const self = this,
            pipelineEditorStore = this.props.pipelineEditorStore,
            segment = this.props.segment;

        return (
            <div className={style.extraButtonWrapper}>
                <Button
                    onClick={this.onLiftSegmentUpButtonClick.bind(self, segment)}
                    isGhost={true}
                    leftIcon={'chevron-up'}
                    size={SIZE.SMALL}
                    isDisabled={pipelineEditorStore.isFirstNonStandaloneSegment(segment)}
                />

                <Button
                    onClick={this.onLiftSegmentDownButtonClick.bind(self, segment)}
                    isGhost={true}
                    leftIcon={'chevron-down'}
                    size={SIZE.SMALL}
                    isDisabled={pipelineEditorStore.isLastNonStandaloneSegment(segment)}
                />

                <Button
                    onClick={this.onDeleteSegmentButtonClick.bind(self, segment)}
                    isRound={true}
                    intent={INTENT.DANGER}
                    size={SIZE.SMALL}
                    isGhost={true}
                    isMinimal={true}
                >
                    {i18n.t('admin.pipelines>deleteSegment')}
                </Button>
            </div>
        );
    }

    render () {
        const props = this.props,
            segment = props.segment;

        return (
            <CardPanel
                title={this.segmentPanelTitle}
                extraButtons={this.extraButtons}
                noBodyPadding={true}
                theme={'light'}
            >
                <div className={style.segmentContainer}>
                    <SegmentBar segment={segment}/>
                </div>
            </CardPanel>
        );
    }
}
