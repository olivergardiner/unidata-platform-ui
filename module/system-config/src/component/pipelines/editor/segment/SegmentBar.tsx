/**
 * Segment card
 *
 * @author Ivan Marshalkin
 * @date 2019-11-28
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {Segment} from '../../../../model/pipeline/Segment';
import * as style from './SegmentBar.m.scss';

interface IProps {
    segment: Segment;
}

interface IInjectProps extends IProps {
}

@observer
export class SegmentBar extends React.Component<IProps> {
    render () {
        const props = this.props,
            segment = props.segment;

        return (
            <div className={style.segmentBar}>
                <div className={style.lineBlock}>
                    <span className={style.lineTitle}>ID: </span>
                    <span className={style.lineValue}>{segment.id.getValue()}</span>
                </div>
                <div className={style.lineBlock}>
                    <span className={style.lineTitle}>description: </span>
                    <span className={style.lineValue}>{segment.description.getValue()}</span>
                </div>
            </div>
        );
    }
}
