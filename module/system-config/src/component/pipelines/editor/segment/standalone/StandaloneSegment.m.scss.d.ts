export const emptySegmentContainer: string;
export const editorSegment: string;
export const segmentContainer: string;
export const segmentIsEmpty: string;
export const buttonContainer: string;
