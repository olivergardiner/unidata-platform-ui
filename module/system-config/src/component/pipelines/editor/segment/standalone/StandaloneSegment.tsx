/**
 * Single segment editor
 *
 * @author Ivan Marshalkin
 * @date 2019-11-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {PipelineEditorStore} from '../../store/PipelineEditorStore';
import {Segment} from '../../../../../model/pipeline/Segment';
import {Nullable} from '@unidata/core';
import {SelectSegmentModal} from '../../SelectSegmentModal';
import {SegmentBar} from '../SegmentBar';
import {SegmentType, StandaloneSegmentType} from '../../../../../model/pipeline/type/SegmentType';
import {Button, CardPanel, INTENT, SIZE, Tag} from '@unidata/uikit';
import * as style from './StandaloneSegment.m.scss';
import {i18n} from '@unidata/core-app';

interface IProps {
    type: StandaloneSegmentType;
    segment: Nullable<Segment>;
    pipelineEditorStore: PipelineEditorStore;
}

interface IInjectProps extends IProps {
}

interface IState {
    isOpen: boolean;
}

@observer
export class StandaloneSegment extends React.Component<IProps, IState> {
    state = {
        isOpen: false
    };

    constructor (props: Readonly<IProps>) {
        super(props);

        this.toggleModalState = this.toggleModalState.bind(this);
        this.onDeleteSegmentButtonClick = this.onDeleteSegmentButtonClick.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
        this.onSelectPipeline = this.onSelectPipeline.bind(this);
    }

    toggleModalState () {
        this.setState((prevState) => {
            return {
                isOpen: !prevState.isOpen
            };
        });
    }

    showModal () {
        if (!this.state.isOpen) {
            this.toggleModalState();
        }
    }

    hideModal () {
        if (this.state.isOpen) {
            this.toggleModalState();
        }
    }

    onCloseModal () {
        this.hideModal();
    }

    onSelectPipeline (segment: Segment) {
        const props = this.props,
            pipelineEditorStore = props.pipelineEditorStore;

        pipelineEditorStore.addStandaloneSegment(props.type, new Segment({
            id: segment.id.getValue(),
            segmentType: segment.segmentType.getValue(),
            description: segment.description.getValue()
        }));

        this.hideModal();
    }

    onDeleteSegmentButtonClick () {
        const props = this.props,
            pipelineEditorStore = props.pipelineEditorStore,
            segment = props.segment;

        if (segment) {
            pipelineEditorStore.deleteSegment(segment);
        }
    }

    segmentEmpty () {
        const pipelineEditorStore = this.props.pipelineEditorStore;

        return (
            <div className={style.emptySegmentContainer}>
                <div className={style.segmentIsEmpty}>
                    {i18n.t('admin.pipelines>segmentIsEmpty')}
                </div>

                <Button
                    isRound={true}
                    intent={INTENT.SECONDARY}
                    onClick={this.showModal}
                    size={SIZE.MIDDLE}
                >
                    {i18n.t('admin.pipelines>selectSegment')}
                </Button>

                <SelectSegmentModal
                    open={this.state.isOpen}
                    onClose={this.onCloseModal}
                    onSelect={this.onSelectPipeline}
                    segments={pipelineEditorStore.getAwailableStandaloneSegmentsByType(this.props.type)}
                />
            </div>
        );
    }

    segmentEditor (segment: Segment) {
        return (
            <div className={style.editorSegment}>
                <div className={style.segmentContainer}>
                    <SegmentBar segment={segment}/>
                </div>

                <div className={style.buttonContainer}>
                    <Button
                        onClick={this.onDeleteSegmentButtonClick}
                        leftIcon={'trash2'}
                        isMinimal={true}
                    />
                </div>
            </div>
        );
    }

    get segmentPanelTitle () {
        return (
            <>
                {i18n.t('admin.pipelines>segmentType')}&nbsp;&gt;&nbsp;
                <Tag color={this.segmentTagColor}>
                    {this.props.type}
                </Tag>
            </>
        );
    }

    get segmentTagColor (): string {
        switch (this.props.type) {
            case SegmentType.FALLBACK:
                return '#9967CC';
            case SegmentType.FINISH:
                return '#66CC66';
            case SegmentType.START:
                return '#0066CC';
            default:
                return '';
        }
    }

    render () {
        const segment = this.props.segment;

        return (
            <CardPanel
                title={this.segmentPanelTitle}
                noBodyPadding={true}
                theme={'light'}
            >
                {!segment ? this.segmentEmpty() : this.segmentEditor(segment)}
            </CardPanel>
        );
    }
}
