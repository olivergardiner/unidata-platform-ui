/**
 * Bus routes definition operation service
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import {BusRoutesDefinition} from '../model/bus_routes/BusRoutesDefinition';
import {SaveBusRoutesDefinitionOp} from './op/bus_routes/SaveBusRoutesDefinitionOp';
import {DeleteBusRoutesDefinitionOp} from './op/bus_routes/DeleteBusRoutesDefinitionOp';
import {ReadBusRoutesDefinitionsOp} from './op/bus_routes/ReadBusRoutesDefinitionsOp';

export class BusRoutesDefinitionService {
    public static getBusRoutesDefinitions (): Promise<BusRoutesDefinition> {
        const op = new ReadBusRoutesDefinitionsOp();

        return op.execute();
    }

    public static saveBusRoutesDefinitions (busRoutesDefinition: BusRoutesDefinition): Promise<null> {
        const op = new SaveBusRoutesDefinitionOp(busRoutesDefinition);

        return op.execute();
    }

    public static deleteBusRoutesDefinitions (busRoutesDefinition: BusRoutesDefinition): Promise<null> {
        const op = new DeleteBusRoutesDefinitionOp(busRoutesDefinition);

        return op.execute();
    }
}
