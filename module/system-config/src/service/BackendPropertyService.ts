/**
 * Service for working with Backend Properties
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {BackendProperty} from '../model/backend_properties/BackendProperty';
import {ReadBackendPropertiesListOp} from './op/backendproperty/ReadBackendPropertiesListOp';
import {UpdateBackendPropertiesOp} from './op/backendproperty/UpdateBackendPropertiesOp';
import {UpdateBackendPropertiesConfigOp} from './op/backendproperty/UpdateBackendPropertiesConfigOp';
import {ReadBackendPropertiesConfigOp} from './op/backendproperty/ReadBackendPropertiesConfigOp';
import {UploadFile} from '@unidata/uikit';

export class BackendPropertyService {
    public static getBackendProperties (): Promise<BackendProperty[]> {
        let op = new ReadBackendPropertiesListOp();

        return op.execute();
    }

    public static updateBackendProperties (backendProperties: any []): Promise<any> {
        let op = new UpdateBackendPropertiesOp(backendProperties);

        return op.execute();
    }

    public static importBackendProperties (file: UploadFile): Promise<any> {
        let op = new UpdateBackendPropertiesConfigOp(file);

        return op.execute();
    }

    public static exportBackendProperties (): Promise<any> {
        let op = new ReadBackendPropertiesConfigOp();

        return op.execute();
    }
}
