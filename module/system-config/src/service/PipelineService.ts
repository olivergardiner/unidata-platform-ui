/**
 * Service for working with pipelines
 *
 * @author Ivan Marshalkin
 * @date 2019-11-26
 */

import {ReadPipelineListOp} from './op/pipeline/ReadPipelineListOp';
import {Pipeline} from '../model/pipeline/Pipeline';
import {Segment} from '../model/pipeline/Segment';
import {ReadAcceptingSegmentListOp} from './op/pipeline/ReadAcceptingSegmentListOp';
import {ReadStartSegmentListOp} from './op/pipeline/ReadStartSegmentListOp';
import {DeletePipelineOp} from './op/pipeline/DeletePipelineOp';
import {UpdatePipelineOp} from './op/pipeline/UpdatePipelineOp';
import {CreatePipelineOp} from './op/pipeline/CreatePipelineOp';

export class PipelineService {
    /**
     * Returns a list of pipelines
     */
    public static getPipelineList (): Promise<Pipeline[]> {
        let op = new ReadPipelineListOp();

        return op.execute();
    }

    /**
     * Returns the pipeline model
     */
    public static getPipeline (): Promise<Pipeline> {
        return (null as any);
    }

    /**
     * Creates a NEW pipeline
     */
    public static createPipeline (pipeline: Pipeline): Promise<null> {
        let op = new CreatePipelineOp(pipeline);

        return op.execute();
    }

    /**
     * Updates an EXISTING pipeline
     */
    public static updatePipeline (pipeline: Pipeline): Promise<null> {
        let op = new UpdatePipelineOp(pipeline);

        return op.execute();
    }

    /**
     * Saves the pipeline
     *
     */
    public static savePipeline (pipeline: Pipeline): Promise<null> {
        if (pipeline.getPhantom()) {
            return this.createPipeline(pipeline);
        }

        return this.updatePipeline(pipeline);
    }

    /**
     * Deletes the pipeline
     */
    public static deletePipeline (pipeline: Pipeline): Promise<null> {
        let op = new DeletePipelineOp(pipeline.startId.getValue(), pipeline.subjectId.getValue());

        return op.execute();
    }

    /**
     * Returns a list of segments that are applicable to the starting point
     *
     * @param startId - start point
     */
    public static getAcceptingSegments (startId: string): Promise<Segment[]> {
        let op = new ReadAcceptingSegmentListOp(startId);

        return op.execute();
    }

    /**
     * Returns a list of registered start segments
     */
    public static getStartSegments (): Promise<Segment[]> {
        let op = new ReadStartSegmentListOp();

        return op.execute();
    }
}
