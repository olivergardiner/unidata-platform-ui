/**
 * Backend properties update operation
 *
 * @author Denis Makarov
 * @date 2019-08-23
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class UpdateBackendPropertiesOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/configuration',
        method: 'put'
    };

    constructor (backendProperties: any []) {
        super();

        this.config.data = backendProperties;
    }
}

