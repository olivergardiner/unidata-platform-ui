/**
 * Operation for getting a list of backend properties
 *
 * @author Denis Makarov
 * @date 2019-08-23
 */

import {BackendProperty} from '../../../model/backend_properties/BackendProperty';
import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class ReadBackendPropertiesListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/configuration',
        method: 'get',
        model: BackendProperty,
        successProperty: '',
        rootProperty: ''
    };

    protected processOperationData (data: any): any {
        const classModel = this.config.model;

        let result: any = [];

        data.forEach(function (item: any) {
            if (item.value !== undefined) {
                result.push(new classModel(item));
            }
        });

        return result;
    }
}
