/**
 * Backend properties import operation
 *
 * @author Denis Makarov
 * @date 2019-08-23
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ReadBackendPropertiesConfigOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/configuration/export-config',
        method: 'get'
    };
}

