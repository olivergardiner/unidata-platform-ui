/**
 * Backend properties import operation
 *
 * @author Denis Makarov
 * @date 2019-08-23
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {UploadFile} from '@unidata/uikit';

export class UpdateBackendPropertiesConfigOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/configuration/import-config',
        method: 'post'
    };

    constructor (file: UploadFile) {
        super();

        this.config.data = file;
    }
}

