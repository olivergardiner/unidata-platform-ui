/**
 * Delete Apache Bus routes definition operation
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import {BusRoutesDefinition} from '../../../model/bus_routes/BusRoutesDefinition';
import {BaseHttpOp, IModelOpConfig} from '@unidata/core';

export class DeleteBusRoutesDefinitionOp extends BaseHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/bus_route/routes-definitions/{{id}}',
        method: 'delete',
        model: BusRoutesDefinition,
        successProperty: 'success',
        rootProperty: ''
    };

    constructor (busRoutesDefinition: BusRoutesDefinition) {
        super();

        this.urlContext = {
            id: busRoutesDefinition.routesDefinitionId.getValue()
        };
    }
}
