/**
 * Save Apache Bus routes definition operation
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import {BusRoutesDefinition} from '../../../model/bus_routes/BusRoutesDefinition';
import {IModelOpConfig, ModelListHttpOp} from '@unidata/core';

export class SaveBusRoutesDefinitionOp extends ModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/bus_route/routes-definitions',
        method: 'post',
        model: BusRoutesDefinition,
        successProperty: 'success',
        rootProperty: ''
    };

    constructor (busRoutesDefinition: BusRoutesDefinition) {
        super();

        this.config.data = busRoutesDefinition.serialize();
    }
}
