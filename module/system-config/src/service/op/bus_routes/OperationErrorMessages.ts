/**
 * Operation error messages
 *
 * @author Ivan Marshalkin
 * @date 2017-12-11
 */

export enum OperationErrorMessages {
    UNKNOWN = 'Unknown operation error.'
}
