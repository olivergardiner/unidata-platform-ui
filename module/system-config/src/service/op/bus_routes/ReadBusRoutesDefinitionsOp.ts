/**
 * Read Apache Bus routes definitions from server operation
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import {BusRoutesDefinition} from '../../../model/bus_routes/BusRoutesDefinition';
import {IModelOpConfig, ModelListHttpOp} from '@unidata/core';

export class ReadBusRoutesDefinitionsOp extends ModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/bus_route/routes-definitions',
        method: 'get',
        model: BusRoutesDefinition,
        successProperty: 'success',
        rootProperty: 'content'
    };
}
