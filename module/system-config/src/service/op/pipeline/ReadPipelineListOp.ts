/**
 * Operation for getting a list of pipelines
 *
 * @author Ivan Marshalkin
 * @date 2019-11-29
 */

import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {Pipeline} from '../../../model/pipeline/Pipeline';

export class ReadPipelineListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/system/pipeline/all',
        method: 'get',
        rootProperty: 'content.pipelines',
        model: Pipeline
    };
}
