/**
 * The operation of removing pipeline
 *
 * @author Ivan Marshalkin
 * @date 2019-11-29
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class DeletePipelineOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/system/pipeline/{{startId}}/{{subjectId}}',
        method: 'delete'
    };

    constructor (startId: string, subjectId: string) {
        super();

        this.urlContext = {
            startId: encodeURI(startId),
            subjectId: subjectId ? subjectId : '-'
        };
    }
}

