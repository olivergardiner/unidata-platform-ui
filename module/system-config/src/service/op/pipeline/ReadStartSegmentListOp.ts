/**
 * Operation for getting a list of starting segments
 *
 * @author Ivan Marshalkin
 * @date 2019-11-29
 */

import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {Segment} from '../../../model/pipeline/Segment';

export class ReadStartSegmentListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/system/pipeline/start',
        method: 'get',
        rootProperty: 'content.segments',
        model: Segment
    };
}
