/**
 * Pipeline creation operation
 *
 * @author Ivan Marshalkin
 * @date 2019-11-29
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {Pipeline} from '../../../model/pipeline/Pipeline';

export class CreatePipelineOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/system/pipeline/',
        method: 'post'
    };

    constructor (pipeline: Pipeline) {
        super();

        this.config.data = pipeline.serialize();
    }
}

