/**
 * The save operation pipeline
 *
 * @author Ivan Marshalkin
 * @date 2019-11-29
 */

import {AppHttpOp} from '@unidata/core-app';
import {Pipeline} from '../../../model/pipeline/Pipeline';
import {IOpConfig} from '@unidata/core';

export class UpdatePipelineOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/system/pipeline/',
        method: 'post'
    };

    constructor (pipeline: Pipeline) {
        super();

        this.config.data = pipeline.serialize();
    }
}

