﻿/**
 * BackendProperties Page
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {Layout} from '@unidata/uikit';
import {BackendPropertiesEditor} from '../../component/backend_properties/editor/BackendPropertiesEditor';
import {ImportExportModal} from '../../component/backend_properties/import_export/ImportExportModal';
import {BackendPropertiesPageStore} from './store/BackendPropertiesPageStore';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';

interface IProps {
}

@observer
export class BackendPropertiesPage extends React.Component<IProps> {
    componentCls: string = 'ud-page ud-page-backend-properties';

    store: BackendPropertiesPageStore = new BackendPropertiesPageStore();
    providerProps: any;

    constructor (props: IProps) {
        super(props);

    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>beproperties');
    }

    render () {
        let resources = [Res.ADMIN_SYSTEM_MANAGEMENT, Res.PLATFORM_PARAMETERS_MANAGEMENT],
            readOnly: boolean = !ResourceManager.userHasAnyResourceRight(resources, Right.UPDATE);

        return (
            <Layout className={this.componentCls}>
                <Layout.Content>
                    <BackendPropertiesEditor store={this.store.editorStore}
                                             openModal={this.store.handleImportExportModalOpen}
                                             readOnly={readOnly}/>
                    <ImportExportModal
                        store={this.store.importExportStore}
                        openModal={this.store.handleImportExportModalOpen}
                        isModalOpen={this.store.importExportModalIsOpen}
                        readOnly={readOnly}/>
                </Layout.Content>
            </Layout>
        );
    }
}
