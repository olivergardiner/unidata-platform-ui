/**
 * Backend Properties page store
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import {action, observable} from 'mobx';
import {BackendPropertiesEditorStore} from '../../../component/backend_properties/store/EditorStore';
import {ImportExportStore} from '../../../component/backend_properties/store/ImportExportStore';

export class BackendPropertiesPageStore {
    constructor () {
        this.handleImportExportModalOpen = this.handleImportExportModalOpen.bind(this);
        this.handleImportIsDone = this.handleImportIsDone.bind(this);
    }

    public importExportStore: ImportExportStore = new ImportExportStore();
    public editorStore: BackendPropertiesEditorStore = new BackendPropertiesEditorStore();

    @observable
    public importExportModalIsOpen: boolean = false;

    @action
    public handleImportExportModalOpen (isOpen: boolean) {
        this.importExportModalIsOpen = isOpen;
    }

    public handleImportIsDone () {
        this.editorStore.loadBackendProperties();
    }

}
