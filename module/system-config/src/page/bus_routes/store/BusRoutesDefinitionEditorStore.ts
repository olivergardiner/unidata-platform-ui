/**
 * Bus routes definition editor store
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import {action, observable} from 'mobx';
import {BusRoutesDefinition} from '../../../model/bus_routes/BusRoutesDefinition';
import {BusRoutesDefinitionService} from '../../../service/BusRoutesDefinitionService';
import {ReactiveProp} from '@unidata/core';
import {Dialog} from '@unidata/core-app';

interface IBusRouteEditorStoreOptions {
    onDeleteBusRoute?: () => null; // function called when successfully deleting the pipeline
    onSaveBusRoute?: () => null; // function called when saving the pipeline successfully
}

export class BusRoutesDefinitionEditorStore {
    @observable
    private busRoutesDefinition: BusRoutesDefinition;

    // additional options passed to the store
    private options: IBusRouteEditorStoreOptions;

    constructor (busRoutesDefinition: BusRoutesDefinition, options: IBusRouteEditorStoreOptions) {
        this.options = options;
        this.setBusRoutesDefinition(busRoutesDefinition);
    }

    /**
     * Sets the current pipeline
     *
     * @param busRoutesDefinition
     */
    @action
    private setBusRoutesDefinition (busRoutesDefinition: BusRoutesDefinition) {
        busRoutesDefinition.setReactive([ReactiveProp.DIRTY]);
        this.busRoutesDefinition = busRoutesDefinition;
    }

    /**
     * Returns the current pipeline
     */
    public getBusRoutesDefinition (): BusRoutesDefinition {
        return this.busRoutesDefinition;
    }

    public get saveButtonEnabled (): boolean {
        return this.busRoutesDefinition.getDirty() === true;
    }

    /**
     * Saving bus route
     */
    public saveBusRoutesDefinition () {
        let self = this;

        BusRoutesDefinitionService
            .saveBusRoutesDefinitions(this.busRoutesDefinition)
            .then(
                this.onBusRouteSaveSuccess.bind(this),
                this.onBusRouteSaveFailure.bind(this)
            );
    }

    public onBusRouteSaveSuccess () {
        const options = this.options;

        Dialog.showMessage('Save: success');

        if (options.onSaveBusRoute) {
            options.onSaveBusRoute();
        }
    }

    public onBusRouteSaveFailure () {
        Dialog.showError('Save: failure');
    }

    /**
     * Deletes the pipeline
     */
    public deleteBusRoutesDefinition () {
        BusRoutesDefinitionService
            .deleteBusRoutesDefinitions(this.busRoutesDefinition)
            .then(
                this.onBusRouteDeleteSuccess.bind(this),
                this.onBusRouteDeleteFailure.bind(this)
            );
    }

    public onBusRouteDeleteSuccess () {
        const options = this.options;

        Dialog.showMessage('Delete: success');

        if (options.onDeleteBusRoute) {
            options.onDeleteBusRoute();
        }
    }

    public onBusRouteDeleteFailure () {
        Dialog.showError('Delete: failure');
    }
}
