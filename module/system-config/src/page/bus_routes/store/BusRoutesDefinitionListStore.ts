/**
 * Bus routes definition list store
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import {action, observable} from 'mobx';
import {BusRoutesDefinitionService} from '../../../service/BusRoutesDefinitionService';
import {BusRoutesDefinition} from '../../../model/bus_routes/BusRoutesDefinition';
import {Nullable} from '@unidata/core';
import {Dialog} from '@unidata/core-app';

interface IPipelineListStoreOptions {
    onSelectBusRoutesDefinition?: (BusRoutesDefinition: BusRoutesDefinition) => null; // function called when selecting a pipeline in the list
}

export class BusRoutesDefinitionListStore {
    // a list of loaded on our pipeline
    @observable.shallow
    private busRoutesDefinitions: BusRoutesDefinition[] = [];

    // the currently selected pipeline
    @observable
    private selectedBusRoutesDefinition: Nullable<BusRoutesDefinition> = null;

    // additional options passed to the store
    private options: IPipelineListStoreOptions;

    constructor (option: IPipelineListStoreOptions) {
        this.options = option;

        this.loadBusRoutesDefinitions();
    }

    /**
     * load bus routes
     */
    @action
    public loadBusRoutesDefinitions () {
        BusRoutesDefinitionService
            .getBusRoutesDefinitions()
            .then(
                this.successLoadBusRoutesDefinitions.bind(this),
                this.failureLoadCamelRoutes.bind(this)
            );
    }

    /**
     * Handler for successful pipeline loading
     *
     * @param busRoutesDefinitions
     */
    @action
    private successLoadBusRoutesDefinitions (busRoutesDefinitions: BusRoutesDefinition[]) {
        this.busRoutesDefinitions = busRoutesDefinitions;
    }

    /**
     * Handler for failed pipeline loading
     *
     * @param busRoutesDefinitions
     */
    @action
    private failureLoadCamelRoutes (busRoutesDefinitions: BusRoutesDefinition[]) {
        Dialog.showError('load bus routes failure');
    }

    /**
     * Sets the selected pipeline
     *
     * @param busRoutesDefinition
     */
    @action
    public setSelectedBusRoutesDefinition (busRoutesDefinition: BusRoutesDefinition) {
        const options = this.options;

        this.selectedBusRoutesDefinition = busRoutesDefinition;

        if (options.onSelectBusRoutesDefinition) {
            options.onSelectBusRoutesDefinition(busRoutesDefinition);
        }
    }

    /**
     * Resets the currently selected bus route
     */
    @action
    public resetSelectedBusRoute () {
        this.selectedBusRoutesDefinition = null;
    }

    /**
     * Returns the selected bus route
     */
    public getSelectedBusRoutesDefinition (): Nullable<BusRoutesDefinition> {
        return this.selectedBusRoutesDefinition;
    }

    /**
     * Returns a list of uploaded bus routes
     */
    public getBusRoutesDefinitions (): BusRoutesDefinition[] {
        return this.busRoutesDefinitions;
    }

    /**
     * Creates a new pipeline
     */
    @action
    public createBusRoutesDefinition () {
        let busRoutesDefinition: BusRoutesDefinition = new BusRoutesDefinition({});

        this.busRoutesDefinitions.push(busRoutesDefinition);

        this.setSelectedBusRoutesDefinition(busRoutesDefinition);
    }
}
