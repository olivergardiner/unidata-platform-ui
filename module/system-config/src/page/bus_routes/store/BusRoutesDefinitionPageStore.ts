/**
 * Apache Bus routes page store
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import {BusRoutesDefinitionListStore} from './BusRoutesDefinitionListStore';
import {BusRoutesDefinitionEditorStore} from './BusRoutesDefinitionEditorStore';
import {BusRoutesDefinition} from '../../../model/bus_routes/BusRoutesDefinition';

export class BusRoutesDefinitionPageStore {
    public busRoutesDefinitionListStore: BusRoutesDefinitionListStore;
    public busRoutesDefinitionEditorStore: BusRoutesDefinitionEditorStore;

    constructor () {
        this.busRoutesDefinitionListStore = new BusRoutesDefinitionListStore({
            onSelectBusRoutesDefinition: this.onSelectBusRoutesDefinition.bind(this)
        });
    }

    public get providerProps () {
        let props = {};

        return props;
    }

    /**
     * Handler called when selecting a pipeline
     *
     * @param busRoutesDefinition
     */
    private onSelectBusRoutesDefinition (busRoutesDefinition: BusRoutesDefinition) {
        this.busRoutesDefinitionEditorStore = new BusRoutesDefinitionEditorStore(busRoutesDefinition, {
            onDeleteBusRoute: this.onDeleteBusRoute.bind(this)
        });
    }

    /**
     * Handler called when the pipeline is successfully deleted
     */
    private onDeleteBusRoute () {
    }

    public get busRoutesDefinitionEditorVisible (): boolean {
        return this.busRoutesDefinitionListStore.getSelectedBusRoutesDefinition() ? true : false;
    }
}
