/**
 * Bus routes definition editor component
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import * as React from 'react';
import {BusRoutesDefinitionEditorStore} from '../../store/BusRoutesDefinitionEditorStore';
import {observer} from 'mobx-react';
import {Button, Editor, Field} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';

interface IProps {
    busRoutesDefinitionEditorStore: BusRoutesDefinitionEditorStore;
}

@observer
export class BusRoutesDefinitionEditor extends React.Component<IProps> {
    constructor (props: Readonly<IProps>) {
        super(props);

        this.onSaveButtonClick = this.onSaveButtonClick.bind(this);
        this.onDeleteButtonClick = this.onDeleteButtonClick.bind(this);
    }

    onSaveButtonClick () {
        this.props.busRoutesDefinitionEditorStore.saveBusRoutesDefinition();
    }

    onDeleteButtonClick () {
        this.props.busRoutesDefinitionEditorStore.deleteBusRoutesDefinition();
    }

    get extraButtons () {
        const BusRoutesDefinitionEditorStore = this.props.busRoutesDefinitionEditorStore;

        return (
            <React.Fragment>
                <Button
                    leftIcon='save'
                    name='saveBusRoutesDefinition'
                    isRound={true}
                    isDisabled={!BusRoutesDefinitionEditorStore.saveButtonEnabled}
                    onClick={this.onSaveButtonClick}
                >{i18n.t('admin.pipelines>saveButtonText')}</Button>
                <Button
                    type='button'
                    leftIcon='trash2'
                    isRound={true}
                    name='deleteBusRoutesDefinition'
                    onClick={this.onDeleteButtonClick}
                >
                </Button>
            </React.Fragment>
        );
    }

    onRouteIdChange (oldValue: string, value: string) {
        const busRoutesDefinitionEditorStore = this.props.busRoutesDefinitionEditorStore;

        busRoutesDefinitionEditorStore.getBusRoutesDefinition().routesDefinitionId.setValue(value);
    }

    onRouteDefinitionChange (oldValue: string, value: string) {
        const busRoutesDefinitionEditorStore = this.props.busRoutesDefinitionEditorStore;

        busRoutesDefinitionEditorStore.getBusRoutesDefinition().routesDefinition.setValue(value);
    }

    render () {
        const busRoutesDefinitionEditorStore = this.props.busRoutesDefinitionEditorStore;
        const busRoutesDefinition = busRoutesDefinitionEditorStore.getBusRoutesDefinition();

        return (
            <React.Fragment>
                <Editor
                    title={i18n.t('admin.busRoutesDefinitions>editorTitle')}
                    groupSectionTitle={i18n.t('page.header>administration')}
                    iconType='user'
                    extraButtons={this.extraButtons}
                >
                    <Field.Input
                        type='text'
                        label={'Routes definition id:'}
                        defaultValue={busRoutesDefinition.routesDefinitionId.getValue()}
                        onChange={this.onRouteIdChange.bind(this)}
                    />

                    <Field.Textarea
                        label={'Routes definition:'}
                        name={busRoutesDefinition.routesDefinitionId.getValue()}
                        defaultValue={busRoutesDefinition.routesDefinition.getValue()}
                        onChange={this.onRouteDefinitionChange.bind(this)}
                    />
                </Editor>
            </React.Fragment>
        );
    }
}
