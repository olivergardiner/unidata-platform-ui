/**
 * Apache Bus routes list component
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import {observer} from 'mobx-react';
import * as React from 'react';
import {BusRoutesDefinitionListStore} from '../../store/BusRoutesDefinitionListStore';
import {BusRoutesDefinition} from '../../../../model/bus_routes/BusRoutesDefinition';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {Button, MenuSider, SIZE} from '@unidata/uikit';

interface IProps {
    readOnly?: boolean;
    busRoutesDefinitionListStore: BusRoutesDefinitionListStore;
}

interface IInjectProps extends IProps {
}

@observer
export class BusRoutesDefinitionList extends React.Component<IProps> {
    get injected () {
        return this.props as IInjectProps;
    }

    get busRoutesDefinitionListStore () {
        return this.props.busRoutesDefinitionListStore;
    }

    get readOnly () {
        return Boolean(this.props.readOnly);
    }

    get BusRoutesDefinitionListData () {
        return this.busRoutesDefinitionListStore.getBusRoutesDefinitions().map((item) => {
            return {
                key: item.routesDefinitionId.getValue(),
                name: item.routesDefinitionId.getValue(),
                model: item
            };
        });
    }

    get actions () {
        const canCreateBusRoutesDefinition = (!this.readOnly &&
            ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT], Right.CREATE));

        return (
            <React.Fragment>
                <Button
                    key='button-add'
                    isRound={true}
                    color='grey'
                    leftIcon='plus'
                    size={SIZE.LARGE}
                    name='createBusRoutesDefinition'
                    hidden={!canCreateBusRoutesDefinition}
                    onClick={this.onCreateBusRoutesDefinitionButtonClick.bind(this)}
                    title={i18n.t('admin.role>create')}
                />
            </React.Fragment>
        );
    }

    get gridColumns () {
        return [
            {
                accessor: 'displayName',
                id: 'displayName',
                Cell: (cell: any) => {
                    let BusRoutesDefinition: BusRoutesDefinition = cell.original.model;

                    return (
                        <React.Fragment>
                            <div>{BusRoutesDefinition.routesDefinitionId.getValue()}</div>
                        </React.Fragment>
                    );
                }
            }
        ];
    }

    onCreateBusRoutesDefinitionButtonClick () {
        this.busRoutesDefinitionListStore.createBusRoutesDefinition();
    }

    onBusRoutesDefinitionListItemClick (busRoutesDefinition: BusRoutesDefinition) {
        this.busRoutesDefinitionListStore.setSelectedBusRoutesDefinition(busRoutesDefinition);
    }

    render () {
        return (
            <>
                <MenuSider
                    title={i18n.t('admin.busRoutesDefinitions>busRoutesDefinitionListTitle')}
                    data={this.BusRoutesDefinitionListData}
                    columns={this.gridColumns}
                    currentItem={this.busRoutesDefinitionListStore.getSelectedBusRoutesDefinition()}
                    onClick={this.onBusRoutesDefinitionListItemClick.bind(this)}
                    actions={this.actions}
                    hideHeader={true}
                />
            </>
        );
    }
}
