/**
 * Bus routes definition configuration page
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */

import {observer, Provider} from 'mobx-react';
import * as React from 'react';
import {Layout} from '@unidata/uikit';
import {BusRoutesDefinitionList} from './component/list/BusRoutesDefinitionList';
import {BusRoutesDefinitionPageStore} from './store/BusRoutesDefinitionPageStore';
import {BusRoutesDefinitionEditor} from './component/editor/BusRoutesDefinitionEditor';
import {i18n} from '@unidata/core-app';

interface IProps {
}

interface IInjectProps extends IProps {
}

@observer
export class BusRoutesDefinitionPage extends React.Component<IProps> {
    static PAGE_CLASS: string = 'ud-page ud-page-bus-routes';

    store: BusRoutesDefinitionPageStore = new BusRoutesDefinitionPageStore();
    providerProps: any;

    constructor (props: IProps) {
        super(props);

        // the same props must be passed to the provider, otherwise mobs throws an exception
        // @see https://stackoverflow.com/questions/43550137/mobx-rerender-after-assign/43562824
        this.providerProps = this.store.providerProps;
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>bus_routes_definitions');
    }

    get injected () {
        return this.props as IInjectProps;
    }

    render () {
        const pageStore = this.store;

        return (
            <Provider {...this.providerProps}>
                <Layout className={BusRoutesDefinitionPage.PAGE_CLASS}>
                    <BusRoutesDefinitionList
                        busRoutesDefinitionListStore={pageStore.busRoutesDefinitionListStore}
                    />
                    <Layout.Content>
                        {
                            pageStore.busRoutesDefinitionEditorVisible &&
                            <BusRoutesDefinitionEditor
                                busRoutesDefinitionEditorStore={pageStore.busRoutesDefinitionEditorStore}
                            />
                        }
                    </Layout.Content>
                </Layout>
            </Provider>
        );
    }
}
