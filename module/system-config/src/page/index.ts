import {UeModuleTypeComponent} from '@unidata/core';
import {BackendPropertiesPage} from './backend_properties/BackendPropertiesPage';
import {PipelinePage} from './pipelines/PipelinePage';
import {BusRoutesDefinitionPage} from './bus_routes/BusRoutesDefinitionPage';
import {AppTypeManager} from '@unidata/core-app';

export * from './pipelines/PipelinePage';

export * from './backend_properties/BackendPropertiesPage';

export * from './bus_routes/BusRoutesDefinitionPage';

export const pages = [
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'backendPropertiesPage',
            active: true,
            system: false,
            component: BackendPropertiesPage,
            resolver: () => {
                return AppTypeManager.isSystemAdmin();
            },
            meta: {
                route: '/backend_properties'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'pipelinePage',
            active: true,
            system: false,
            component: PipelinePage,
            resolver: () => {
                return AppTypeManager.isSystemAdmin();
            },
            meta: {
                route: '/pipeline'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'busRoutesPage',
            active: true,
            system: false,
            component: BusRoutesDefinitionPage,
            resolver: () => true,
            meta: {
                route: '/busroutes'
            }
        }
    }
];
