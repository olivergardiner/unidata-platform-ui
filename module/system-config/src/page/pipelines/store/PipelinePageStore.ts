/**
 * Store for the pipeline settings page
 *
 * @author: Ivan Marshalkin
 * @date: 2019-11-26
 */
import {PipelineListStore} from '../../../component/pipelines/store/PipelineListStore';
import {PipelineEditorStore} from '../../../component/pipelines/editor/store/PipelineEditorStore';
import {Pipeline} from '../../../model/pipeline/Pipeline';


export class PipelinePageStore {
    public pipelineListStore: PipelineListStore;
    public pipelineEditorStore: PipelineEditorStore;

    constructor () {
        this.pipelineListStore = new PipelineListStore({
            onSelectPipeline: this.onSelectPipeline.bind(this)
        });
    }

    public get providerProps () {
        let props = {};

        return props;
    }

    /**
     * Handler called when selecting a pipeline
     *
     * @param pipeline
     */
    private onSelectPipeline (pipeline: Pipeline) {
        this.pipelineEditorStore = new PipelineEditorStore(pipeline, {
            onDeletePipeline: this.onPipelineDelete.bind(this)
        });
    }

    /**
     * Handler called when the pipeline is successfully deleted
     */
    private onPipelineDelete () {
        const pipelineListStore = this.pipelineListStore;

        pipelineListStore.loadPipelines();
        pipelineListStore.resetSelectedPipeline();
    }

    /**
     * Returns true if the pipeline editor should be visible
     */
    public get pipelineEditorVisible (): boolean {
        return this.pipelineListStore.getSelectedPipeline() ? true : false;
    }


}
