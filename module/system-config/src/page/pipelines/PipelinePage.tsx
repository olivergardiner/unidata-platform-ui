/**
 * The settings page on our pipeline
 *
 * @author: Ivan Marshalkin
 * @date: 2019-11-26
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {Layout} from '@unidata/uikit';
import {PipelinePageStore} from './store/PipelinePageStore';
import {i18n, Res, ResourceManager, UserManager} from '@unidata/core-app';
import {PipelineList} from '../../component/pipelines/list/PipelineList';
import {PipelineEditor} from '../../component/pipelines/editor/PipelineEditor';

interface IProps {}

interface IInjectProps extends IProps {
}

@observer
export class PipelinePage extends React.Component<IProps> {
    static PAGE_CLASS: string = 'ud-page ud-page-with-sider ud-page-pipelines';

    store: PipelinePageStore = new PipelinePageStore();
    providerProps: any;

    constructor (props: IProps) {
        super(props);

        // the same props must be passed to the provider, otherwise mobs throws an exception
        // @see https://stackoverflow.com/questions/43550137/mobx-rerender-after-assign/43562824
        this.providerProps = this.store.providerProps;
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>pipeline');
    }

    get injected () {
        return this.props as IInjectProps;
    }

    render () {
        let readOnly: boolean = ResourceManager.isReadOnly([Res.ADMIN_SYSTEM_MANAGEMENT, Res.ROLE_MANAGEMENT]);
        let roles = UserManager.getUserRoles();

        const pageStore = this.store;

        return (
            <Provider {...this.providerProps}>
                <Layout className={PipelinePage.PAGE_CLASS}>
                    <PipelineList
                        pipelineListStore={pageStore.pipelineListStore}
                    />

                    <Layout.Content>
                        {pageStore.pipelineEditorVisible && (
                            <PipelineEditor
                                pipelineEditorStore={pageStore.pipelineEditorStore}
                            />
                        )}
                    </Layout.Content>
                </Layout>
            </Provider>
        );
    }
}
