/**
 * empty comment line Viktor Magarlamov
 *
 * @author: Viktor Magarlamov
 * @date: 2019-04-05
 */

import {AbstractModel, anyField, AnyField, StringField, stringField} from '@unidata/core';
import {observable} from 'mobx';

export class BackendProperty extends AbstractModel {
    constructor (item: any) {
        super(item);

        this.meta = item.meta;
    }

    @stringField({primaryKey: true})
    public name: StringField;

    @stringField()
    public displayName: StringField;

    @stringField()
    public type: StringField;

    @stringField()
    public group: StringField;

    @stringField()
    public groupCode: StringField;

    @observable
    @anyField()
    public value: AnyField;

    public meta: any;
}
