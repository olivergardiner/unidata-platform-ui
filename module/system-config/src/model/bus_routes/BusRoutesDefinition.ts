/**
 * Apache Bus routes definition model
 *
 * @author: Aleksandr Malyshev
 * @date: 2019-12-25
 */
import {AbstractModel, stringField, StringField} from '@unidata/core';

export class BusRoutesDefinition extends AbstractModel {
    @stringField({primaryKey: true})
    public routesDefinitionId: StringField;

    @stringField()
    public routesDefinition: StringField;
}
