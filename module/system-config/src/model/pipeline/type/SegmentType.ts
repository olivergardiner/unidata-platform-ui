/**
 * Types of segments
 *
 * @author Ivan Marshalkin
 * @date 2019-12-02
 */

export enum SegmentType {
    START = 'START',
    FINISH = 'FINISH',
    FALLBACK = 'FALLBACK',
    CONNECTOR = 'CONNECTOR',
    POINT = 'POINT'
}

export type StandaloneSegmentType = SegmentType.START | SegmentType.FINISH | SegmentType.FALLBACK;
