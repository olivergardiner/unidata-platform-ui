/**
 * Segment model for pipeline
 *
 * @author Ivan Marshalkin
 * @date 2019-11-26
 */

import {AbstractModel, stringField, StringField} from '@unidata/core';

export class Segment extends AbstractModel {
    @stringField()
    public id: StringField;

    @stringField()
    public segmentType: StringField;

    @stringField()
    public description: StringField;
}
