/**
 * Pipeline model
 *
 * @author Ivan Marshalkin
 * @date 2019-11-26
 */

import {
    AbstractModel,
    hasMany,
    IAssociationDescriptorMap,
    ModelCollection,
    ModelMetaData,
    StringField,
    stringField
} from '@unidata/core';
import {Segment} from './Segment';

export class Pipeline extends AbstractModel {
    @stringField({primaryKey: true})
    public startId: StringField;

    @stringField()
    public description: StringField;

    @stringField()
    public subjectId: StringField;

    @hasMany()
    public segments: ModelCollection<Segment>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                segments: Segment
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
