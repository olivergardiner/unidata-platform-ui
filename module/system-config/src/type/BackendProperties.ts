/**
 * Types for Back end Properties
 *
 * @author Denis Makarov
 * @date 2019-08-23
 */

import {BackendProperty} from '../model/index';

export type BackendPropertySectionType = {
    group: string;
    groupCode: string;
    backendProperties: BackendProperty[];
};
