/**
 * Types for Wizard Import Export
 *
 * @author Denis Makarov
 * @date 2019-08-23
 */

export enum ImportExportOperations {
    Import = 'import',
    Export = 'export'
}
