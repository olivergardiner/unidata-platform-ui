/**
 * The module is responsible for pages containing the application's system settings.
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import {IModule, UeModuleTypeCallBack, UeModuleTypeComponent} from '@unidata/core';
import {pages} from './page';

export * from './model';

export * from './service';

const menuItems = [
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'backendPropertiesMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'beproperties',
                route: '/backend_properties',
                icon: 'wrench',
                groupName: 'administration',
                order: 'last'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'pipelineMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'pipeline',
                route: '/pipeline',
                icon: 'arrow-wave',
                groupName: 'administration',
                order: 'last'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'busRoutesMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'busroutes',
                route: '/busroutes',
                icon: 'bus',
                groupName: 'administration',
                order: 'last'
            }
        }
    }
];

export function init (): IModule {
    return {
        id: 'system-config',
        uemodules: [...pages, ...menuItems]
    };
}
