import {iconData} from './iconData';

export type IconName = keyof typeof iconData;

export enum IconIntent {
    DEFAULT = 'default',
    PRIMARY = 'primary',
    INFO = 'info',
    SUCCESS = 'success',
    WARNING = 'warning',
    DANGER = 'danger',
    GHOST = 'ghost',
    SECONDARY = 'secondary'
}

export {
    iconData
};
