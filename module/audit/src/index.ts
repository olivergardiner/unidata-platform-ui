/**
 * Module for working with the audit log
 *
 * @author: Vladimir Stebunov
 * @date: 2020-02-04
 */
import {IModule, UeModuleTypeComponent, UeModuleTypeCallBack} from '@unidata/core';
import {AuditLogPage} from './page/audit/AuditLogPage';
import {AppTypeManager} from '@unidata/core-app';

const menuItem = {
    'default': {
        type: UeModuleTypeCallBack.MENU_ITEM,
        moduleId: 'auditMenuItem',
        active: true,
        system: false,
        fn: () => {}, //not use
        resolver: () => true,
        meta: {
            name: 'auditlog',
            route: '/auditlog',
            icon: 'binoculars2',
            groupName: 'administration',
            order: 'last'
        }
    }
};

const page = {
    'default': {
        type: UeModuleTypeComponent.PAGE,
        moduleId: 'auditPage',
        active: true,
        system: false,
        component: AuditLogPage,
        resolver: () => {
            return AppTypeManager.isSystemAdmin();
        },
        meta: {
            route: '/auditlog'
        }
    }
};

export function init (): IModule {
    return {
        id: 'audit',
        uemodules: [menuItem, page]
    };
}
