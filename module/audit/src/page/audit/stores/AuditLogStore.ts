/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import {action, computed, isObservableArray, observable, ObservableMap, toJS} from 'mobx';
import forEach from 'lodash/forEach';
import isArray from 'lodash/isArray';
import {IStringKeyMap, ModelCollection, UeModuleCallback, ueModuleManager, UeModuleTypeCallBack} from '@unidata/core';
import {
    BackendStorage,
    BackendStorageKeys,
    BackendStorageService,
    Dialog,
    i18n,
    IBackendStorageRecord,
    SearchHit,
    SearchPreview,
    UserManager
} from '@unidata/core-app';
import {EntityGroupHelper, EntityGroupService, EntityTreeNode} from '@unidata/meta';
import {SourceSystemService, SourceSystem} from '@unidata/meta';
import {AuditService} from '../../../service/AuditService';
import {IOption} from '@unidata/uikit';
import {IAuditTypes} from '../../../service/op/audit/ReadAuditLogTypesOp';

export class AuditLogStore {
    @observable public auditRecords: ModelCollection<SearchHit> = ModelCollection.create(SearchHit);
    @observable public sourceSystems: ModelCollection<SourceSystem> = ModelCollection.create(SourceSystem);
    @observable public entityTree: EntityTreeNode;

    @observable public pages: number = 1;
    @observable public page: number = 1;
    @observable public defaultPageSize: number = 20;
    @observable public totalCount: number = 0;
    @observable public pageSize: number = this.defaultPageSize;
    @observable public pageLoading: boolean = true;
    @observable public tableLoading: boolean = true;
    @observable public searchFields: ObservableMap = new ObservableMap();
    @observable public tableDisplayMode: string = 'admin';
    @observable public customModeColumns: ObservableMap = new ObservableMap();
    @observable public isFilterable: boolean = true;
    /* eslint-disable-next-line */
    @observable public sortFields: ObservableMap<string, 'ASC' | 'DESC' | undefined> = new ObservableMap({'when_happened': 'DESC'});

    public auditEventTypes: IOption[] = [];
    public auditDomains: IOption[] = [];
    public lastRequestParams: any;

    public columns: IAuditTypes['columns'] = [];

    constructor () {
        Promise
            .all([
                BackendStorageService.loadRecords(UserManager.getUserLogin() as string),
                AuditService.getAuditTypes()
            ])
            .then(([backendStorageData, auditTypes]) => {
                BackendStorage.data = backendStorageData;

                this.buildTableDataFromBackendStorage();
                // this.initAuditLogAdditionalData(); TODO Brauer Ilya maybe this data will be in response from be

                this.auditEventTypes = auditTypes.types;
                this.auditDomains = auditTypes.domains;
                this.columns = auditTypes.columns;

                this.loadTableData();

            })
            .finally(() => {
                this.setPageLoading(false);
            });
    }


    @computed public get auditTableData (): IStringKeyMap [] {
        let tableData: IStringKeyMap [] = observable([]);

        forEach(this.auditRecords.items, function (auditRecord: SearchHit) {
            let plainData: IStringKeyMap = {};

            forEach(auditRecord.preview.items, function (previewItem: SearchPreview) {
                let value;

                if (previewItem.values.getValue() && previewItem.values.getValue().length > 1) {
                    value = previewItem.values.getValue();
                } else {
                    value = previewItem.value.getValue();
                }

                plainData[previewItem.field.getValue()] = value;
            });

            tableData.push(plainData);
        });

        return tableData;
    }

    @action
    public buildTableDataFromBackendStorage () {
        let backendStorageData = BackendStorage.data.find((el: IBackendStorageRecord) => el.key === 'audit_log_table'),
            auditTableData: any;

        if (!backendStorageData) {
            this.initCustomModeColumns();

            return;
        }

        auditTableData = JSON.parse(backendStorageData.value);

        if (!auditTableData) {
            this.initCustomModeColumns();

            return;
        }

        this.tableDisplayMode = auditTableData.mode;

        if (auditTableData.columns) {
            this.customModeColumns = new ObservableMap(auditTableData.columns);
        } else {
            this.initCustomModeColumns();
        }
    }

    @action
    public setFilterable () {
        this.isFilterable = !this.isFilterable;
        this.loadTableData();
    }

    @action
    public setPageLoading (pageLoading: boolean) {
        this.pageLoading = pageLoading;
    }

    @action
    public setTableLoading (tableLoading: boolean) {
        this.tableLoading = tableLoading;
    }

    // ToDo replace to bind
    @action.bound
    public setPage (page: number) {
        this.page = page;
    }

    @action.bound
    public setPages (pages: number) {
        this.pages = pages;
    }

    @action.bound
    public setPageSize (pageSize: number) {
        this.pageSize = pageSize;

        this.loadTableData();
    }

    @action
    public setTotalCount (count: number) {
        this.totalCount = count;
    }

    @action
    public setFilter (key: string, value: any) {
        this.searchFields.set(key, value);
    }

    @computed
    public get filtersCount () {
        let count = 0;

        this.searchFields.forEach(function (value) {
            if (value !== undefined) {
                count++;
            }
        });

        return count;
    }

    @action.bound
    public setSortField (key: string) {
        const oldSort = this.sortFields.get(key);

        switch (oldSort) {
            case 'ASC': {
                this.sortFields.set(key, 'DESC');
                break;
            }
            case 'DESC': {
                this.sortFields.set(key, undefined);
                break;
            }
            case undefined: {
                this.sortFields.set(key, 'ASC');
                break;
            }
        }
    }

    @action
    public clearFilters () {
        this.searchFields.clear();

        this.loadTableData();
    }

    @action
    public updateTableDisplayMode (tableDisplayMode: string) {
        this.tableDisplayMode = tableDisplayMode;
        this.saveTableSettings();

    }

    @action
    public updateAuditRecords (auditRecords: SearchHit[]) {
        this.auditRecords.replaceAll(auditRecords);
    }

    public startExport () {
        AuditService.exportAudit(this.lastRequestParams);
    }

    public loadTableData () {
        let me = this;

        let cfg: IStringKeyMap = {
            page: this.page,
            count: this.pageSize,
            searchFields: [],
            sortFields: []
        };

        me.setTableLoading(true);

        if (this.isFilterable) {
            me.searchFields.forEach(function (value: any, key: string) {
                let cfgFieldName: string;

                if (value) {
                    cfgFieldName = key;

                    if (cfgFieldName === 'when_happened') {
                        if (value[0]) {
                            cfg.from = new Date(value[0]);
                        }

                        if (value[1]) {
                            cfg.to = new Date(value[1]);
                        }
                    } else if (cfgFieldName === 'type') {
                        cfg.searchFields.push({fieldName: 'action', values: [value]});
                    } else if (isArray(value) || isObservableArray(value)) {
                        if (value.length > 0) {
                            cfg.searchFields.push({fieldName: cfgFieldName, values: value.slice()});
                        }
                    } else {
                        cfg.searchFields.push({fieldName: cfgFieldName, values: [value]});
                    }
                }
            });
        }

        me.sortFields.forEach(function (value, key) {
            if (value !== undefined) {
                cfg.sortFields.push({fieldName: key, order: value});
            }
        });

        this.loadAuditRecords(cfg);
    }

    private buildBackendStorageValue (): string {
        let value: any = {};

        value.columns = toJS(this.customModeColumns);
        value.mode = this.tableDisplayMode;

        return JSON.stringify(value);
    }

    private initAuditLogAdditionalData () {
        let promises: any[] = [],
            me = this;

        promises.push(EntityGroupService.getEntityGroup());
        promises.push(SourceSystemService.getSourceSystemList());

        return Promise.all(promises).then(action(function (result: any) {
            // TODO: Replace with a new tree
            me.entityTree = EntityGroupHelper.buildTreeNodeModel(result[0]);
            me.sourceSystems.replaceAll(result[1]);

            me.setPageLoading(false);
        }));
    }

    @action
    private initCustomModeColumns () {
        let columnsProp: IStringKeyMap = {};

        this.columns.forEach(function (column) {
            columnsProp[column.id] = {hidden: false};
        });

        this.customModeColumns = new ObservableMap(columnsProp);
    }

    public saveTableSettings () {
        BackendStorageService.saveRecord(
            UserManager.getUserLogin() as string,
            BackendStorageKeys.AUDIT_LOG_TABLE,
            this.buildBackendStorageValue()
        );
    }

    public loadAuditRecords (cfg: IStringKeyMap) {
        let me = this;

        AuditService.getAuditRecords(cfg).then(action(function (response: any) {
            me.lastRequestParams = cfg;
            me.updateAuditRecords(response.auditRecords);
            me.setPages(Math.ceil(response.totalCount / me.pageSize));
            me.setTotalCount(response.totalCount);
            me.setTableLoading(false);
        })).catch(action(function () {
            Dialog.showError(i18n.t('admin.audit>errorLoadingData'), i18n.t('common:error'));
            me.setTableLoading(false);
        }));
    }
}
