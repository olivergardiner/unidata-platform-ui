/**
 *
 * Mappings for AuditLog
 *
 * @author Denis Makarov
 * @date 2019-14-02
 */

import {IStringKeyMap} from '@unidata/core';
import {i18n} from '@unidata/core-app';

export class AuditLogDataMappings {
    public allColumns = [
        'when_happen',
        'parameters',
        'type',
        '$etalon_id',
        'endpoint',
        'server_ip',
        'client_ip'
    ];

    /* eslint-disable camelcase */
    public adminModeColumns: IStringKeyMap = {
        when_happen: 'when_happen',
        parameters: 'parameters',
        type: 'type',
        sourceSystem: 'sourceSystem',
        server_ip: 'server_ip',
        client_ip: 'client_ip'
    };

    public operatorModeColumns: IStringKeyMap = {
        operationId: 'operationId',
        user: 'user',
        date: 'date',
        details: 'details',
        action: 'action',
        originId: 'originId',
        etalonId: 'etalonId',
        entity: 'entity',
        taskId: 'taskId',
        sourceSystem: 'sourceSystem'
    };

    public subsystemMapper: IStringKeyMap = {
        WORKFLOW: i18n.t('admin.audit>workflows'),
        USERS: i18n.t('common:users'),
        DATA: i18n.t('admin.audit>dataHandling'),
        AUTH: i18n.t('admin.audit>authorization'),
        ROLES: i18n.t('admin.audit>security'),
        LABELS: i18n.t('admin.audit>labels')
    };

    public auditEventTypeMapper: IStringKeyMap = {
        create: i18n.t('admin.audit>createdUser'),
        update: i18n.t('admin.audit>updatedUser'),
        login: i18n.t('admin.audit>userLogin'),
        logout: i18n.t('admin.audit>userLogout'),
        'delete': i18n.t('admin.audit>removeRecord'),
        merge: i18n.t('admin.audit>mergeRecords'),
        get: i18n.t('admin.audit>getRecord'),
        restore: i18n.t('admin.audit>restoreRecord'),
        'meta-draft-apply': i18n.t('admin.audit>metaDraftApply'),
        'meta-draft-delete-element': i18n.t('admin.audit>metaDraftDeleteEl'),
        'meta-draft-remove': i18n.t('admin.audit>metaDraftRemove'),
        'meta-model-import': i18n.t('admin.audit>metaModelImport'),
        'model-upsert': i18n.t('admin.audit>modelUpsert'),
        'data-delete-relation': i18n.t('admin.audit>deleteDataRelation'),
        'data-upsert-classifier': i18n.t('admin.audit>upsertDataClassifier'),
        'data-delete-classifier': i18n.t('admin.audit>deleteDataClassifier'),
        'no-action': i18n.t('admin.audit>noAction'),
        'data-insert-action': i18n.t('admin.audit>dataInsertAction'),
        'data-update-action': i18n.t('admin.audit>dataUpdateAction'),
        'relation-insert-action': i18n.t('admin.audit>relationInsertAction'),
        'relation-update-action': i18n.t('admin.audit>relationUpdateAction'),
        'restore-period': i18n.t('admin.audit>restorePeriod'),
        'complete-wf-task': i18n.t('admin.audit>completeWfTask'),
        split: i18n.t('admin.audit>splitOrigin'),
        'security-role-create': i18n.t('admin.audit>securityRoleCreate'),
        'security-role-update': i18n.t('admin.audit>securityRoleUpdate'),
        'security-role-delete': i18n.t('admin.audit>securityRoleDelete'),
        'security-label-create': i18n.t('admin.audit>securityLabelCreate'),
        'security-label-update': i18n.t('admin.audit>securityLabelUpdate'),
        'security-label-delete': i18n.t('admin.audit>securityLabelDelete'),
        'security-label-attach': i18n.t('admin.audit>securityLabelAttach')
    };

    public columnNameMapper: any = {
        all: () => i18n.t('admin.audit>all'),
        operationId: () => i18n.t('admin.audit>operationId'),
        user: () => i18n.t('admin.audit>initiator'),
        details: () => i18n.t('admin.audit>details'),
        date: () => i18n.t('admin.audit>creationDate'),
        action: () => i18n.t('admin.audit>eventType'),
        taskId: () => i18n.t('admin.audit>taskId'),
        serverIp: () => i18n.t('admin.audit>serverIp'),
        clientIp: () => i18n.t('admin.audit>clientIp'),
        originId: () => i18n.t('admin.audit>originId'),
        etalonId: () => i18n.t('admin.audit>etalonId'),
        externalId: () => i18n.t('admin.audit>externalId'),
        entity: () => i18n.t('admin.labels>entityOrLookupEntity'),
        subSystem: () => i18n.t('admin.audit>subSystem'),
        sourceSystem: () => i18n.t('admin.labels>sourceSystem')
    };
}
