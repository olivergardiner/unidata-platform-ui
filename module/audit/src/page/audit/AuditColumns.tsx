/**
 * Audit. Setting the visibility of columns
 *
 * @author Brauer Ilya
 * @date 2020-08-26
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {AuditLogStore} from './stores/AuditLogStore';
import {Checkbox, CheckboxChangeEvent, DropDown, Icon, PLACEMENT, Popover, TRIGGER} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import {action, runInAction} from 'mobx';
import * as styles from './auditLogTable.m.scss';

interface IProps {
    store: AuditLogStore;
}

@observer
export class AuditColumns extends React.Component<IProps> {
    get store () {
        return this.props.store;
    }

    customColumnVisibilityChange = (key: string) => (e: CheckboxChangeEvent) => {
        runInAction(() => {
            this.store.customModeColumns.get(key).hidden = !e.target.checked;
        });
    };

    @action
    allColumnsVisibilityChange = (e: CheckboxChangeEvent) => {
        this.store.customModeColumns.forEach((value) => {
            value.hidden = !e.target.checked;
        });
    };


    render () {
        let allChecked = true;

        this.store.customModeColumns.forEach(function (value) {
            if (value.hidden) {
                allChecked = false;
            }
        });

        return (
            <Popover
                placement={PLACEMENT.BOTTOM_END}
                target={(
                    <div className={styles.settingIconContainer}>
                        <Icon
                            name={'setting'}
                            className={'ud-audit-custom-mode-trigger'}
                            data-qaid='customize-table'
                        />
                    </div>
                )}
                trigger={TRIGGER.CLICK}
            >
                <div>
                    <h4>{i18n.t('admin.audit>customPopoverTitle')}</h4>
                    <Checkbox
                        onChange={this.allColumnsVisibilityChange}
                        checked={allChecked}
                        data-qaid='all_checked_items'>
                        {i18n.t('admin.audit>allColumns')}
                    </Checkbox>

                    <DropDown.Splitter />

                    {Array.from(this.store.customModeColumns).map(([key, value]) => {
                        const checked = !value.hidden;

                        return (
                            <div key={key}>
                                <Checkbox
                                    key={key}
                                    onChange={this.customColumnVisibilityChange(key)}
                                    checked={checked}
                                    data-qaid={key + '_item'}>
                                    {this.store.columns.find((column) => column.id === key)?.description || key}
                                </Checkbox>
                            </div>
                        );
                    })}
                </div>
            </Popover>
        );
    }
}
