﻿/**
 * Table of logs
 *
 * @author Denis Makarov
 * @date 2019-02-14
 */

import * as React from 'react';
import {
    Button,
    CardPanel,
    Column,
    DateRangeInput,
    IFilterData,
    Input, IOption,
    PLACEMENT,
    Select, SIZE, Spinner, Table,
    Tooltip
} from '@unidata/uikit';
import {AuditLogStore} from './stores/AuditLogStore';
import {observer, Observer} from 'mobx-react';
import * as moment from 'moment';
import {i18n} from '@unidata/core-app';
import {AuditColumns} from './AuditColumns';
import * as styles from './auditLogTable.m.scss';
import {RangePickerProps} from 'antd/lib/date-picker';
import {RangePickerValue} from '../../../../uikit/src/component/dateinput/types';

interface IAuditLogTableProps {
    auditLogStore: AuditLogStore;
}

@observer
export class AuditLogTable extends React.Component<IAuditLogTableProps> {
    get auditLogStore () {
        return this.props.auditLogStore;
    }

    columns = this.props.auditLogStore.columns.map((column) => {
        return {
            accessor: column.id,
            Header: column.description,
            minWidth: 160,
            Cell: column.filterType === 'DatePicker' ? this.renderDate : AuditLogTable.defaultRenderer
        };
    });

    onChangeDate = (id: string) => (value: any) => {
        let startDateValue = '';
        let endDateValue = '';

        if (value[0]) {
            startDateValue = value[0].format('YYYY-MM-DD');
        }

        if (value[1]) {
            endDateValue = value[1].format('YYYY-MM-DD');
        }

        this.props.auditLogStore.setFilter(id, [startDateValue, endDateValue]);
    };

    renderDefaultFilter = (key: string) => () => {
        return (
            <Observer
                render={() => (
                    <Input
                        key={key}
                        placeholder={i18n.t('admin.audit>find')}
                        onChange={(e) => this.props.auditLogStore.setFilter(key, e.target.value)}
                        value={this.props.auditLogStore.searchFields.get(key)}
                        data-qaid={key + '_filter'}
                    />
                )}
            />
        );
    };

    renderDatePickerFilter = (id: string) => (props: RangePickerProps) => {
        const whenHappened = this.props.auditLogStore.searchFields.get(id);

        const value = [
            this.getFormattedDate(whenHappened && whenHappened[0]),
            this.getFormattedDate(whenHappened && whenHappened[1])
        ]  as RangePickerValue;

        return (
            <Observer
                render={() => (
                    <DateRangeInput
                        {...props}
                        key={id}
                        data-qaid={`${id}-filter`}
                        value={value}
                        onChange={this.onChangeDate(id)}
                        onCalendarChange={this.onChangeDate(id)}
                    />
                )}
            />
        );
    };

    renderSelectFilter = (id: string, options: IOption[]) => (props: any) => {
        return (
            <Observer
                render={() => (
                    <Select
                        {...props}
                        onChange={(action: any) => {
                            this.props.auditLogStore.setFilter(id, action);
                        }}
                        showSearch={true}
                        allowClear={true}
                        placeholder={i18n.t('admin.audit>select')}
                        value={this.props.auditLogStore.searchFields.get(id)}
                        dropdownMatchSelectWidth={false}
                        data-qaid={`${id}_filter_select`}
                        options={options}
                    />
                )}
            />
        );
    };

    filters: {[key: string]: IFilterData} = this.props.auditLogStore.columns.reduce<{[key: string]: IFilterData}>((result, column) => {
        let renderComponent: React.ComponentType<any> = this.renderDefaultFilter(column.id);

        if (column.filterType === 'DatePicker') {
            renderComponent = this.renderDatePickerFilter(column.id);
        } else if (column.filterType === 'Select') {
            if (column.id === 'type') {
                renderComponent = this.renderSelectFilter(column.id, this.props.auditLogStore.auditEventTypes);
            } else if (column.id === 'domain') {
                renderComponent = this.renderSelectFilter(column.id, this.props.auditLogStore.auditDomains);
            }
        }

        result[column.id] = {
            component: renderComponent,
            type: column.filterType
        };

        return result;
    }, {});

    static defaultRenderer (cell: any) {
        return AuditLogTable.withTooltip(cell.value, '');
    }

    static withTooltip (value: string, key: string) {
        return (
            <Tooltip placement={PLACEMENT.TOP_START} mouseEnterDelay={500} overlay={value}>
                <span data-qaid={key}>{value}</span>
            </Tooltip>
        );
    }

    getDisplayColumns (): Column [] {
        const me: AuditLogTable = this;

        const columns: Column [] = this.columns.filter(function (column: Column) {
            if (!me.auditLogStore.customModeColumns.get(column.accessor)) {
                return true;
            }

            return me.props.auditLogStore.customModeColumns.get(column.accessor).hidden === false;
        });

        return columns;
    }

    renderDate (cell: any) {
        let date, time, dateTime, result;

        if (!cell.value) {
            return '';
        }

        dateTime = cell.value.split('T');

        date = dateTime[0].split('-');
        time = dateTime[1].split(':');

        result = `${date[2]}.${date[1]}.${date[0]} ${time[0]}:${time[1]}:${time[2].split('.')[0]}`;

        return AuditLogTable.withTooltip(result, '');
    }

    getFormattedDate (date: any | undefined) {
        if (!date) {
            return undefined;
        }

        return moment(date).isValid() ? moment(date, 'YYYY-MM-DD') : undefined;
    }

    getRowProps (state: any, rowInfo: any) {
        if (rowInfo && rowInfo.original && rowInfo.original.success === false) {
            return {
                style: {
                    backgroundColor: '#FDD4CF'
                }
            };
        } else {
            return {};
        }
    }

    onFilter = (id: string, isClear?: boolean) => {
        const store = this.props.auditLogStore;

        if (isClear === true) {
            store.setFilter(id, undefined);
        }

        store.setPage(1);
        store.loadTableData();
    };

    onSort = (columnId: string) => {
        const store = this.props.auditLogStore;

        store.setSortField(columnId);
        store.loadTableData();
    };

    render () {
        let columns = this.getDisplayColumns(),
            store = this.props.auditLogStore;

        if (columns.length === 0) {
            return null;
        }

        let filtersCount = store.filtersCount;

        let filterButtonText = store.isFilterable ?
            i18n.t('admin.audit>filterOnCount', {count: filtersCount}) :
            i18n.t('admin.audit>filterOffCount', {count: filtersCount});

        return (
            <div className={styles.cardContainer}>
                <CardPanel
                    title={i18n.t('admin.audit>total', {total: store.totalCount})}
                    noBodyPadding={true}
                    extraButtons={(
                        <div className={styles.extraContainer}>
                            <Button
                                onClick={store.setFilterable.bind(store)}
                                isActive={Boolean(store.isFilterable)}
                                data-qaid='filter_group_button'
                                size={SIZE.SMALL}
                            >
                                {filterButtonText}
                            </Button>
                            <Button
                                onClick={store.clearFilters.bind(store)}
                                isRound={true}
                                leftIcon={'trash2'}
                                title={i18n.t('admin.audit>resetFilters')}
                                tooltipPlacement={PLACEMENT.BOTTOM_END}
                                data-qaid='trash_button'
                                size={SIZE.SMALL}
                            />

                            <AuditColumns store={store}/>
                        </div>
                    )}
                >
                    <Table
                        data={store.auditTableData}
                        columns={columns}
                        hasHeaderBg={true}
                        isStriped={true}
                        filters={this.filters}
                        filterValues={store.searchFields.toJSON()}
                        onFilter={this.onFilter}
                        sort={store.sortFields.toJSON()}
                        onSort={this.onSort}
                    />
                    {store.tableLoading && (
                        <div style={{position: 'absolute'}}>
                            <Spinner/>
                        </div>
                    )}
                    <Table.FooterPanel
                        currentPage={store.page}
                        pageSize={store.pageSize}
                        totalPages={store.pages}
                        totalRecords={store.totalCount}
                        reload={store.loadTableData}
                        setPage={(page: number) => {
                            store.setPage(page);
                            store.loadTableData();
                        }}
                        setPageSize={store.setPageSize}
                    />
                </CardPanel>
            </div>
        );
    }

}
