/**
 * Log page
 *
 * @author Denis Makarov
 * @date 2019-14-02
 */

import * as React from 'react';
import {AuditLogStore} from './stores/AuditLogStore';
import {observer} from 'mobx-react';
import {ButtonWithConfirmation, PageHeader, Skeleton} from '@unidata/uikit';
import {Dialog, i18n} from '@unidata/core-app';
import {AuditLogTable} from './AuditLogTable';

@observer
export class AuditLogPage extends React.Component {
    componentCls: string = 'ud-page ud-page-auditlog';

    auditLogStore: AuditLogStore;

    constructor (props: any) {
        super(props);

        this.initStores();
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>auditlog');
    }

    initStores () {
        this.auditLogStore = new AuditLogStore();
    }

    doExport = () => {
        Dialog.showMessage(i18n.t('admin.audit>exportStarted'));

        this.auditLogStore.startExport();
    };

    get extraButtons () {
        return (
            <ButtonWithConfirmation
                isRound={true}
                name='export'
                isGhost={true}
                onConfirm={this.doExport}
                confirmationMessage={i18n.t('admin.audit>exportConfirm')}
                data-qaid='export'
            >
                {i18n.t('admin.audit>exportButtonText')}
            </ButtonWithConfirmation>
        );
    }

    render () {
        return (
            <Skeleton active loading={this.auditLogStore.pageLoading}>
                <PageHeader
                    extraButtons={this.extraButtons}
                    iconType='users'
                    sectionTitle={i18n.t('admin.audit>title')}
                    groupSectionTitle={i18n.t('page.header>administration')}
                />
                <AuditLogTable auditLogStore={this.auditLogStore}/>
            </Skeleton>
        );
    }
}
