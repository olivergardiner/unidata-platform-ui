/**
 * Service for working with the log log
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {ReadAuditLogListOp} from './op/audit/ReadAuditLogListOp';
import {ExportAuditLogOp} from './op/audit/ExportAuditLogOp';
import {ReadAuditLogTypesOp, IAuditTypes} from './op/audit/ReadAuditLogTypesOp';

export class AuditService {
    public static getAuditRecords (cfg: any): Promise<any> {
        let op = new ReadAuditLogListOp(cfg);

        return op.execute();
    }

    public static exportAudit (cfg: any): Promise<any> {
        let op = new ExportAuditLogOp(cfg);

        return op.execute();
    }

    public static getAuditTypes (): Promise<IAuditTypes> {
        const op = new ReadAuditLogTypesOp();

        return op.execute();
    }
}
