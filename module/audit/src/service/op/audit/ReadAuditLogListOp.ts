﻿
import {AppModelListHttpOp, SearchHit} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class ReadAuditLogListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/audit/search',
        method: 'post',
        model: SearchHit,
        rootProperty: '',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    constructor (cfg: any) {
        super();

        this.config.data = cfg;
    }

    protected processOperationData (data: any): any {
        const classModel = this.config.model;

        let result: any = {};
        let auditRecords: any = [];

        data.hits.forEach(function (item: any) {
            auditRecords.push(new classModel(item));
        });

        result.auditRecords = auditRecords;
        result.totalCount = data.total_count;

        return result;
    }
}
