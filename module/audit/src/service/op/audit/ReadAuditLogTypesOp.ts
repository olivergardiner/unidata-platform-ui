﻿/**
 * Read audit types operation
 *
 * @author Brauer Ilya
 * @date 2020-08-26
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {IFilterData, IOption} from '@unidata/uikit';

interface IHeader {
    name: string;
    type: string;
}

interface IType {
    id: string;
    description: string;
    headers: IHeader[];
}

interface IDomain {
    name: string;
    description: string;
    types: IType[];
}

interface IResponse {
    domains: IDomain[];
    systemHeaders: IHeader[];
}

export interface IAuditTypes {
    types: IOption[];
    domains: IOption[];
    columns: Array<{
        id: string;
        description: string;
        filterType: IFilterData['type'];
    }>;
}

export class ReadAuditLogTypesOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/audit/types',
        method: 'get',
        rootProperty: ''
    };

    protected processOperationData (data: IResponse): IAuditTypes {
        const result: IAuditTypes = {
            types: [],
            domains: [],
            columns: []
        };

        data.systemHeaders.forEach(function (header) {
            if (header.name !== 'throwable' && header.name !== 'success') {
                let filterType: 'Input' | 'DatePicker' | 'Select' = 'Input';

                if (header.name === 'when_happened') {
                    filterType = 'DatePicker';
                } else if (header.name === 'type' || header.name === 'domain') {
                    filterType = 'Select';
                }

                result.columns.push({id: header.name, description: header.name, filterType});
            }
        });

        data.domains.forEach(function (domain) {
            result.domains.push({value: domain.name, title: domain.description});

            domain.types.forEach(function (type) {
                result.types.push({value: type.id, title: type.description});
            });
        });

        return result;
    }
}
