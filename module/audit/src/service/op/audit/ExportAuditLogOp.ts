import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ExportAuditLogOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/audit/export',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    constructor (cfg: any) {
        super();

        this.config.data = cfg;
    }
}
