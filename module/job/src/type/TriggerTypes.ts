/**
 * The conditions of operation
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-12
 */

export enum TriggerTypes {
    afterSuccess = 'afterSuccess',
    afterFail = 'afterFail'
}
