/**
 * empty comment line Viktor Magarlamov
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-12
 */

import {Job} from '../model/Job';
import {MetaParameter} from '../model/MetaParameter';

export enum ParameterType {
    Boolean = 'BOOLEAN',
    String = 'STRING',
    Long = 'LONG',
    Date = 'DATE'
}

export enum JobBageStatus {
    COMPLETED = 'success',
    STARTED = 'processing',
    FAILED = 'error',
    STOPPED = 'default',
    STOPPING = 'default',
    DEFAULT = 'default'
}

export enum JobStatus {
    Completed = 'COMPLETED',
    Progress = 'STARTED',
    Error = 'FAILED',
    Default = 'DEFAULT'
}

export enum JobSpecialName {
    usersSelector = 'usersSelector',
    definitionContent = 'definitionContent'
}

export type JobMetaHash = {
    [name: string]: MetaParameter[];
};

export type JobSearchParams = {
    text: string;
    enabledStatus: string | null;
    tags: string[];
    sortField: string;
    sortDirection: string;
    page: number;
    count: number;
    start: number;
    [key: string]: any;
};

export type JobListData = {
    name: string;
    jobNameReference: string;
    enabled: boolean;
    status: JobStatus;
    key: number;
    model: Job;
};

export type JobLastExecutionInfo = {
    status: string;
    startTime: string;
    endTime: string;
};
