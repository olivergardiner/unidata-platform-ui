/**
 * Possible operation States in the list of operations
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-12
 */

export enum EnabledState {
    Active = 'active',
    Inactive = 'inactive'
}
