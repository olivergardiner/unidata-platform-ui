/**
 * Job trigger service
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {JobTrigger} from '../model/JobTrigger';
import {CreateJobTriggerOp} from './op/jobtriggers/CreateJobTrigger';
import {DeleteJobTriggerOp} from './op/jobtriggers/DeleteJobTrigger';
import {ReadJobTriggerListOp} from './op/jobtriggers/ReadJobTriggerList';

export class JobTriggerService {
    public static createJobTrigger (jobId: number, jobTrigger: JobTrigger) {
        let op = new CreateJobTriggerOp(jobId, jobTrigger);

        return op.execute();
    }

    public static deleteJobTrigger (jobId: number, jobTriggerId: number) {
        let op = new DeleteJobTriggerOp(jobId, jobTriggerId);

        return op.execute();
    }

    public static readJobTriggerList (jobId: number): Promise<JobTrigger[]> {
        let op = new ReadJobTriggerListOp(jobId);

        return op.execute();
    }
}
