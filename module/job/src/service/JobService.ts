/**
 * Job service
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */


import {JobSearchParams} from '../type/JobType';
import {SearchJobOp} from './op/job/SearchJobOp';
import {Job} from '../model/Job';
import {CreateJobOp} from './op/job/CreateJobOp';
import {UpdateJobOp} from './op/job/UpdateJobOp';
import {DeleteJobOp} from './op/job/DeleteJobOp';
import {RunJobOp} from './op/job/RunJobOp';
import {StopJobOp} from './op/job/StopJobOp';
import {ReadJobMetadataOp} from './op/job/ReadJobMetadataOp';
import {ReadJobTagsOp} from './op/job/ReadJobTags';
import {ExportJobsOp} from './op/job/ExportJobsOp';
import {ImportJobsOp} from './op/job/ImportJobsOp';
import {UploadFile} from '@unidata/uikit';

export class JobService {
    public static searchJobs (searchParams: JobSearchParams) {
        let op = new SearchJobOp(searchParams);

        return op.execute();
    }

    public static saveJob (job: Job): Promise<null> {
        if (job.getPhantom()) {
            return this.createJob(job);
        }

        return this.updateJob(job);
    }

    public static createJob (job: Job) {
        let op = new CreateJobOp(job);

        return op.execute();
    }

    public static updateJob (job: Job) {
        let op = new UpdateJobOp(job);

        return op.execute();
    }

    public static deleteJob (id: number) {
        let op = new DeleteJobOp(id);

        return op.execute();
    }

    public static runJob (id: number) {
        let op = new RunJobOp(id);

        return op.execute();
    }

    public static stopJob (id: number) {
        let op = new StopJobOp(id);

        return op.execute();
    }

    public static readJobMetadata () {
        let op = new ReadJobMetadataOp();

        return op.execute();
    }

    public static readJobTags () {
        let op = new ReadJobTagsOp();

        return op.execute();
    }

    public static exportJobs () {
        let op = new ExportJobsOp();

        return op.execute();
    }

    public static importJobs (file: UploadFile) {
        let op = new ImportJobsOp(file);

        return op.execute();
    }
}
