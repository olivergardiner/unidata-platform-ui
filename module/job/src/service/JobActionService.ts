/**
 * Operations service
 *
 * @author: Brauer Ilya
 * @date: 2020-05-26
 */

import {ExecutionsOp} from './op/action/ExecutionOp';
import {ExecutionStepsOp} from './op/action/ExecutionStepOp';
import {RestartExecutionStepOp} from './op/action/RestartExecutionStepOp';

export class JobActionService {
    public static getExecutions (jobId: number, page: number, startAt: number, limit: number): Promise<any> {

        const op = new ExecutionsOp(jobId, page, startAt, limit);

        return op.execute();
    }

    public static getExecutionSteps (executionId: number, page: number, startAt: number, limit: number): Promise<any> {
        const op = new ExecutionStepsOp(executionId, page, startAt, limit);

        return op.execute().then((response: any) => {
            return Promise.resolve(response.data);
        }).catch((ex) => {
            return Promise.reject(ex);
        });
    }

    public static restartExecutionStep (executionId: number): Promise<any> {
        const op = new RestartExecutionStepOp(executionId);

        return op.execute().then((response: any) => {
            if (response.data.errors) {
                return Promise.reject(response.data.errors);
            }

            return Promise.resolve(response.data);
        }).catch((ex) => {
            return Promise.reject(ex);
        });
    }
}
