import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ExecutionsOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/executions/{{jobId}}/{{startAt}}/{{limit}}?page={{page}}',
        rootProperty: '',
        method: 'get'
    };

    constructor (jobId: number, page: number, startAt: number, limit: number) {
        super();

        this.urlContext = {
            jobId,
            page,
            startAt,
            limit
        };
    }
}
