import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ExecutionStepsOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/stepexecutions/{{executionId}}/{{startAt}}/{{limit}}?page={{page}}',
        rootProperty: '',
        method: 'get'
    };

    constructor (executionId: number, page: number, startAt: number, limit: number) {
        super();

        this.urlContext = {
            executionId,
            page,
            startAt,
            limit
        };
    }
}
