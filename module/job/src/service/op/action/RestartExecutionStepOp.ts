import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class RestartExecutionStepOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/restart/{{executionId}}',
        rootProperty: '',
        method: 'get'
    };

    constructor (executionId: number) {
        super();

        this.urlContext = {
            executionId
        };
    }
}
