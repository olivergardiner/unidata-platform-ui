/**
 * The delete operation triggers the jobs
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class DeleteJobTriggerOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/{{jobId}}/triggers/{{jobTriggerId}}',
        rootProperty: '',
        successProperty: '',
        method: 'delete'
    };

    constructor (jobId: number, jobTriggerId: number) {
        super();

        this.config.data = {
            jobTriggerId: jobTriggerId
        };

        this.urlContext = {
            jobId: jobId,
            jobTriggerId: jobTriggerId
        };
    }
}
