/**
 * Job trigger creation operation
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {JobTrigger} from '../../../model/JobTrigger';
import {AppModelHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class CreateJobTriggerOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/jobs/{{jobId}}/triggers',
        model: JobTrigger,
        rootProperty: '',
        successProperty: '',
        method: 'put'
    };

    constructor (jobId: number, jobTrigger: JobTrigger) {
        super();

        this.config.data = jobTrigger.serialize();
        this.urlContext = {
            jobId: jobId
        };
    }
}
