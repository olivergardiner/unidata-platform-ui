/**
 * Operation for getting a list of job triggers
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {JobTrigger} from '../../../model/JobTrigger';
import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp} from '@unidata/core-app';

export class ReadJobTriggerListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/jobs/{{jobId}}/triggers',
        model: JobTrigger,
        rootProperty: '',
        successProperty: '',
        method: 'get'
    };

    constructor (jobId: number) {
        super();

        this.urlContext = {
            jobId: jobId
        };
    }
}
