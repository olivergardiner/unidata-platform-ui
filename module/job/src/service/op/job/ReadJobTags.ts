/**
 * Operation for getting job tags
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */
import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ReadJobTagsOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/tags',
        method: 'get',
        rootProperty: '',
        successProperty: ''
    }
}
