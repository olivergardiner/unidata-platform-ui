/**
 * Job import operation
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {UploadFile} from '@unidata/uikit';

export class ImportJobsOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/import-jobs',
        rootProperty: '',
        successProperty: '',
        method: 'post'
    };

    constructor (file: UploadFile) {
        super();
        let data = new FormData();

        data.append('file', file as any); // ToDo Denis Makarov fix types
        this.config.data = data;
    }
}
