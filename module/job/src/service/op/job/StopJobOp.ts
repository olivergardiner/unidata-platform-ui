/**
 * Job update operation
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class StopJobOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/stop/{{id}}',
        rootProperty: '',
        successProperty: '',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        data: null
    };

    constructor (id: number) {
        super();

        this.urlContext = {
            id: id
        };
    }
}

