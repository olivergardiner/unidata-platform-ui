/**
 * Job update operation
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {Job} from '../../../model/Job';
import {AppModelHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class UpdateJobOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/jobs/{{id}}',
        model: Job,
        rootProperty: '',
        successProperty: '',
        method: 'put'
    };

    constructor (job: Job) {
        super();

        this.urlContext = {
            id: job.id.getValue()
        };
        this.config.data = job.serialize();
    }
}

