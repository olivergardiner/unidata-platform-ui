/**
 * Export operation job
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ExportJobsOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/export-jobs',
        rootProperty: '',
        successProperty: '',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {}
    };
}
