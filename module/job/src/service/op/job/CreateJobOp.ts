/**
 * Operation for creating a new job
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {Job} from '../../../model/Job';
import {AppModelHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class CreateJobOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/jobs',
        model: Job,
        rootProperty: '',
        method: 'put'
    };

    constructor (job: Job) {
        super();

        this.config.data = job.serialize();
    }
}
