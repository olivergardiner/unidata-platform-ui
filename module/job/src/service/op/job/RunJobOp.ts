/**
 * Operation start the job
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class RunJobOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/start/{{id}}',
        rootProperty: '',
        successProperty: '',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        data: null
    };

    constructor (id: number) {
        super();

        this.urlContext = {
            id: id
        };
    }
}

