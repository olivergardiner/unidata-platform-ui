/**
 * Operation for getting a meta name for joba
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {JobMeta} from '../../../model/JobMeta';
import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class ReadJobMetadataOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/jobs/jobmetanames',
        method: 'get',
        model: JobMeta,
        rootProperty: '',
        successProperty: ''
    }
}
