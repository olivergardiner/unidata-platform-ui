/**
 * Job search operation
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {Job} from '../../../model/Job';
import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp} from '@unidata/core-app';

export class SearchJobOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/jobs/search',
        method: 'post',
        model: Job,
        successProperty: '',
        rootProperty: 'content'
    }

    constructor (params: any) {
        super();
        this.config.data = params;
    }
}
