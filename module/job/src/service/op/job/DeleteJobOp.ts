/**
 * The operation of removing the jobs
 *
 * @author Sergey Shishigin
 * @date 2019-08-26
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class DeleteJobOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/jobs/{{id}}',
        rootProperty: '',
        successProperty: '',
        method: 'delete'
    };

    constructor (id: number) {
        super();

        this.urlContext = {
            id: id
        };
    }
}

