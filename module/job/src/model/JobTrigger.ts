/**
 * Model job trigger
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-05
 */

import {
    AbstractModel,
    booleanField,
    BooleanField,
    NumberField,
    numberField,
    stringField,
    StringField
} from '@unidata/core';
import {observable} from 'mobx';

export class JobTrigger extends AbstractModel {
    @observable
    @numberField({primaryKey: true})
    public id: NumberField;

    @observable
    @numberField()
    public startJobId: NumberField;

    @observable
    @booleanField()
    public successRule: BooleanField;

    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public description: StringField;
}
