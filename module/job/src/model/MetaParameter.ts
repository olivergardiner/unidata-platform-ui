/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-02-06
 */

import {AbstractModel, anyField, AnyField, BooleanField, booleanField, stringField, StringField} from '@unidata/core';

export class MetaParameter extends AbstractModel {

    @stringField()
    public name: StringField;

    @anyField()
    public value: AnyField;

    @booleanField()
    public multi_select: BooleanField;

    @stringField()
    public type: StringField;
}
