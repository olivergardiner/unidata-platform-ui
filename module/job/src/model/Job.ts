/**
 * empty comment line Viktor Magarlamov
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-03
 */

import {
    AbstractModel,
    arrayField,
    ArrayField,
    booleanField,
    BooleanField,
    hasMany,
    IAssociationDescriptorMap,
    IFieldValidator,
    ModelCollection,
    ModelMetaData,
    numberField,
    NumberField,
    Required,
    stringField,
    StringField
} from '@unidata/core';
import {IKeyValueMap} from 'mobx';
import {MetaParameter} from './MetaParameter';
import {JobLastExecutionInfo} from '../type/JobType';

export class Job extends AbstractModel {

    @numberField({primaryKey: true})
    public id: NumberField;

    @stringField()
    public name: StringField;

    @stringField()
    public description: StringField;

    @booleanField()
    public enabled: BooleanField;

    @booleanField()
    public error: BooleanField;

    @stringField()
    public cronExpression: StringField;

    @booleanField()
    public skipCronWarnings: BooleanField;

    @stringField()
    public jobNameReference: StringField;

    @arrayField({allowNull: true, defaultValue: []})
    public tags: ArrayField;

    @hasMany()
    public parameters: ModelCollection<MetaParameter>;

    public lastExecution: JobLastExecutionInfo;

    constructor (item: any) {
        super(item);

        this.lastExecution = item.lastExecution;

        if (!item.name) {
            item.name = new StringField('');
        }

        if (!item.id) {
            item.id = new StringField('');
        }
    }

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                parameters: MetaParameter
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public static validators: IKeyValueMap<IFieldValidator[]> = {
        name: [
            {
                rule: Required
            }
        ],
        jobNameReference: [
            {
                rule: Required
            }
        ]
    };
}
