/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-02-06
 */

import {
    AbstractModel,
    hasMany,
    IAssociationDescriptorMap,
    IFieldValidator,
    ModelCollection,
    ModelMetaData,
    stringField,
    StringField
} from '@unidata/core';
import {IKeyValueMap} from 'mobx';
import {MetaParameter} from './MetaParameter';

export class JobMeta extends AbstractModel {
    @stringField()
    public jobNameReference: StringField;

    @hasMany()
    public parameters: ModelCollection<MetaParameter>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                parameters: MetaParameter
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public static validators: IKeyValueMap<IFieldValidator[]> = {
    };
}
