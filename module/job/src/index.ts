/**
 * Module for working with operations
 *
 * @author: Vladimir Stebunov
 * @date: 2020-02-21
 */
import {IModule, UeModuleTypeComponent, UeModuleTypeCallBack} from '@unidata/core';
import {JobsPage} from './page/job/JobsPage';
import {AppTypeManager} from '@unidata/core-app';

const menuItem = {
    'default': {
        type: UeModuleTypeCallBack.MENU_ITEM,
        moduleId: 'jobsMenuItem',
        active: true,
        system: false,
        fn: () => {},
        resolver: () => true,
        meta: {
            name: 'operations',
            route: '/operations/list',
            icon: 'server',
            groupName: 'administration',
            order: 'first'
        }
    }
};

const page = {
    'default': {
        type: UeModuleTypeComponent.PAGE,
        moduleId: 'jobsPage',
        active: true,
        system: false,
        component: JobsPage,
        resolver: () => {
            return AppTypeManager.isSystemAdmin();
        },
        meta: {
            route: '/operations/list'
        }
    }
};

export function init (): IModule {
    return {
        id: 'jobs',
        uemodules: [menuItem, page]
    };
}
