/**
 * The Manager running the operation
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-08
 */

import * as React from 'react';
import * as moment from 'moment';
import {inject, observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {Button, CardPanel, Icon, INTENT, Table} from '@unidata/uikit';
import {LaunchManagerStore} from '../../store/LaunchManagerStore';
import {TablePagination} from './TablePagination';
import {DetailsPopup} from './DetailsPopup';
import {JobPermission} from '../../JobPermission';
import {action} from 'mobx';
import {IconIntent} from '@unidata/icon';

interface IProps {
}

interface IInjectProps extends IProps {
    launchManagerStore: LaunchManagerStore;
    handleEditorTabClick: ({key}: {key: string}) => void;
    handleRunJobClick: () => void;
    handleDetailsClick: () => void;
    handleRestartExecutionStep: (id: number) => void;
}

interface IState {
    detailsContent: {details: string; exitCode: string} | null;
    selectedExecution: number | null;
}

@inject(
    'launchManagerStore', 'handleEditorTabClick', 'handleEditorTabClick',
    'handleRunJobClick', 'handleDetailsClick', 'handleRestartExecutionStep'
)
@observer
export class LaunchManager extends React.Component<IProps, IState> {

    static TIME_FORMAT: string = 'YYYY-MM-DD HH:mm:ss';

    static STATUS_ICONS: {
        [key: string]: {icon: string; color: string; intent: IconIntent};
    } = {
        COMPLETED: {icon: 'check', color: 'green', intent: IconIntent.SUCCESS},
        STARTED: {icon: 'clock', color: 'blue', intent: IconIntent.PRIMARY},
        STARTING: {icon: 'clock', color: 'blue', intent: IconIntent.PRIMARY},
        STOPPING: {icon: 'clock', color: 'grey', intent: IconIntent.DEFAULT},
        STOPPED: {icon: 'clock', color: 'grey', intent: IconIntent.DEFAULT},
        FAILED: {icon: 'close', color: 'red', intent: IconIntent.DANGER}
    };

    state: IState = {
        detailsContent: null,
        selectedExecution: null
    };

    componentDidUpdate (prevProps: IProps, prevState: IState) {
        if (!prevState.detailsContent && this.state.detailsContent) {
            this.injected.handleDetailsClick();
        }
    }

    get injected () {
        return this.props as IInjectProps;
    }

    get store () {
        return this.injected.launchManagerStore;
    }

    get executionFormattedData () {
        return this.store.executions.map((item) => {

            return {
                key: item.id,
                id: item.id,
                startTime: this.formatData(item.startTime),
                endTime: this.formatData(item.endTime),
                statusText: this.translateStatus(item.status),
                statusIcon: this.findStatusIcon(item.status),
                status: item.status,
                exitDescription: item.exitDescription
            };
        });
    }

    get executionColumns () {
        return [
            {
                accessor: 'startTime',
                Header: i18n.t('admin.jobs>execution>startTime')
            },
            {
                accessor: 'endTime',
                Header: i18n.t('admin.jobs>execution>endTime')
            },
            {
                accessor: 'status',
                Header: i18n.t('admin.jobs>execution>status'),
                width: 200,
                Cell: ({original}: {original: any}) => {
                    return (
                        <div style={{color: original.statusIcon.color}}>
                            <Icon name={original.statusIcon.icon} intent={original.statusIcon.intent}/> {original.statusText}
                        </div>
                    );
                }
            },
            {
                accessor: 'details',
                width: 35,
                Cell: ({original}: {original: any}) => {

                    const handleDetailsClick = () => {
                        this.setState({
                            detailsContent: {
                                details: original.exitDescription,
                                exitCode: ''
                            }
                        });
                    };

                    return (
                        <div>
                            {original.exitDescription &&
                                <Icon
                                    name='info-circle'
                                    intent={IconIntent.PRIMARY}
                                    title={i18n.t('admin.jobs>execution>details')}
                                    onClick={handleDetailsClick}
                                />
                            }
                        </div>
                    );
                }
            }
        ];
    }

    get stepsFormattedData () {
        return this.store.executionSteps.map((item) => {

            return {
                key: item.id,
                id: item.id,
                name: item.stepName,
                startTime: this.formatData(item.startTime),
                endTime: this.formatData(item.endTime),
                statusText: this.translateStepStatus(item.status),
                statusIcon: this.findStatusIcon(item.status),
                details: item.exitDescription,
                exitCode: item.exitCode
            };
        });
    }

    get stepsColumns () {
        return [
            {
                accessor: 'name',
                Header: i18n.t('admin.jobs>execution>name')
            },
            {
                accessor: 'startTime',
                Header: i18n.t('admin.jobs>execution>startTime')
            },
            {
                accessor: 'endTime',
                Header: i18n.t('admin.jobs>execution>endTime')
            },
            {
                accessor: 'status',
                width: 200,
                Header: i18n.t('admin.jobs>execution>status'),
                Cell: ({original}: {original: any}) => {
                    return (
                        <div style={{color: original.statusIcon.color}}>
                            <Icon name={original.statusIcon.icon} intent={original.statusIcon.intent}/> {original.statusText}
                        </div>
                    );
                }
            },
            {
                accessor: 'details',
                width: 35,
                Cell: ({original}: {original: any}) => {

                    const handleDetailsClick = () => {
                        this.setState({
                            detailsContent: {
                                details: original.details,
                                exitCode: original.exitCode
                            }
                        });
                    };

                    return (
                        <div>
                            {original.details &&
                                <Icon
                                    name='info-circle'
                                    title={i18n.t('admin.jobs>execution>details')}
                                    onClick={handleDetailsClick}
                                    intent={IconIntent.PRIMARY}
                                />
                            }
                        </div>
                    );
                }
            }
        ];
    }

    get extraButtons () {

        if (!JobPermission.hasExecuteRight) {
            return null;
        }

        return (
            <Button
                intent={INTENT.PRIMARY}
                isMinimal={true}
                leftIcon={'play-circle'}
                onClick={this.injected.handleRunJobClick}
            >
                {i18n.t('admin.jobs>launch')}
            </Button>
        );
    }

    formatData (data: string): string {
        return data ? moment(data).format(LaunchManager.TIME_FORMAT) : '';
    }

    translateStatus (status: string | null) {
        if (status) {
            return i18n.t(`admin.jobs>statuses>${status}`);
        }

        return '';
    }

    translateStepStatus (status: string | null) {
        if (status) {
            return i18n.t(`admin.steps>statuses>${status}`);
        }

        return '';
    }

    findStatusIcon (status: string | null) {
        if (status) {
            return LaunchManager.STATUS_ICONS[status];
        }

        return '';
    }

    @action
    handleOnClose = () => {
        this.store.isDetailOpen = false;
        this.setState({detailsContent: null});
    }

    get stepsTableTitle () {
        const title = i18n.t('admin.jobs>stepList');

        const info = i18n.t('admin.jobs>executedInfo', {
            total: this.store.totalExecutionSteps,
            executed: this.store.completedExecutionSteps
        });

        return (
            <div>
                {title}: <span className='titleDescription'>{info}</span>
            </div>
        );
    }

    render () {
        const {detailsContent} = this.state;
        const height = this.state.selectedExecution ? 'calc(50% - 16px)' : '100%';

        return (
            <>
                <div style={{height}}>
                    <CardPanel
                        title={i18n.t('admin.jobs>executionList')}
                        extraButtons={this.extraButtons}
                        noBodyPadding={true}
                        data-qaid='executionList'
                    >
                        <Table
                            hasHeaderBg={false}
                            data={this.executionFormattedData}
                            columns={this.executionColumns}
                            viewRowsCount={this.store.executionPageSize}
                            onRowClick={(rowIndex: number) => {
                                const id = this.executionFormattedData[rowIndex].id;

                                this.store.hanldeExecutionClick(id);
                                this.setState({selectedExecution: rowIndex});
                            }}
                            checkedRows={this.state.selectedExecution !== null ? [this.state.selectedExecution] : undefined}
                        />
                        <TablePagination
                            handleRefreshClick={this.store.loadExecutions}
                            totalRows={this.store.totalExecutions}
                        />
                    </CardPanel>
                </div>

                {this.store.executionSteps.length > 0 && (
                    <div style={{height: '50%', marginTop: '16px'}}>
                        <CardPanel
                            title={this.stepsTableTitle}
                            noBodyPadding={true}
                            data-qaid='executionSteps'
                        >
                            <Table
                                columns={this.stepsColumns}
                                data={this.stepsFormattedData}
                                hasHeaderBg={false}
                                viewRowsCount={this.store.executionStepPageSize}
                            />
                            <TablePagination
                                handleRefreshClick={this.store.loadExecutionSteps}
                                totalRows={this.store.totalExecutionSteps}
                            />
                        </CardPanel>
                    </div>
                )}

                {detailsContent &&
                    <DetailsPopup
                        isOpen={this.store.isDetailOpen}
                        content={detailsContent}
                        onClose={this.handleOnClose}
                    />
                }
            </>
       );
    }
}
