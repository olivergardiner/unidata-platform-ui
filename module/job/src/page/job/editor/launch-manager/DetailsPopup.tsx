/**
 * Popup with details of the operation
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-16
 */

import * as React from 'react';
import {i18n} from '@unidata/core-app';
import {Modal, SIZE} from '@unidata/uikit';

interface IProps {
    content: {exitCode: string; details: string};
    onClose: () => void;
    isOpen: boolean;
}

export class DetailsPopup extends React.Component<IProps> {

    get modalProps () {
        return {
            isOpen: this.props.isOpen,
            header: i18n.t('admin.jobs>taskExecError'),
            footer: null,
            onClose: this.props.onClose
        };
    }

    render () {
        const {content} = this.props;

        return (
            <Modal {...this.modalProps}>
                <div className='content'>

                    {content.exitCode &&
                        <React.Fragment>
                            <b>{i18n.t('errorCode')}</b>
                            <p>{content.exitCode}</p>
                        </React.Fragment>
                    }

                    <b>{i18n.t('details')}</b>
                    <p>{content.details}</p>
                </div>
            </Modal>
        );
    }
}
