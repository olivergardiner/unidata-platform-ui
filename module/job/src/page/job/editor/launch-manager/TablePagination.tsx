/**
 * Pagination Table
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-08
 */

import * as React from 'react';
import {Table} from '@unidata/uikit';

interface IProps {
    handleRefreshClick: (currentPage: number, limit: number) => void;
    totalRows: number;
}

interface IState {
    currentPage: number;
    pageSize: number;
}

export class TablePagination extends React.Component<IProps, IState> {

    state = {
        currentPage: 1,
        pageSize: 6
    };

    setPage = (page: number) => {
        this.setState({currentPage: page}, this.reloadData);
    };

    setPageSize = (pageSize: number) => {
        this.setState({pageSize}, this.reloadData);
    };

    reloadData = () => {
        const {currentPage, pageSize} = this.state;

        this.props.handleRefreshClick(currentPage - 1, (currentPage - 1) * pageSize);
    };

    render () {
        const totalPages = Math.ceil(this.props.totalRows / this.state.pageSize);

        return (
            <Table.FooterPanel
                currentPage={this.state.currentPage}
                pageSize={this.state.pageSize}
                totalPages={totalPages}
                totalRecords={this.props.totalRows}
                reload={this.reloadData}
                setPage={this.setPage}
                setPageSize={this.setPageSize}
            />
        );
    }
}
