﻿/**
 * Operations screen editor
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-03
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {Button, Editor, INTENT, TabBar, ButtonWithConfirmation, Menu, PageHeader} from '@unidata/uikit';
import {JobForm} from './job-form/JobForm';
import {LaunchManager} from './launch-manager/LaunchManager';
import {Pages} from '@unidata/types';
import {EditorStore} from '../store/EditorStore';
import {ListStore} from '../store/ListStore';
import {JobPermission} from '../JobPermission';
import {ReactiveProp} from '@unidata/core';
import * as styles from './jobEditor.m.scss';

interface IProps {
    readOnly?: boolean;
    currentTab: Pages;
}

interface IInjectProps extends IProps {
    editorStore: EditorStore;
    listStore: ListStore;
    handleEditorTabClick: ({key}: {key: string}) => void;
    handleSaveJobClick: () => void;
    handleDestroyJobClick: () => void;
}

@inject('editorStore', 'listStore', 'handleEditorTabClick', 'handleSaveJobClick', 'handleDestroyJobClick')
@observer
export class JobEditor extends React.Component<IProps> {

    get injected () {
        return this.props as IInjectProps;
    }

    get store () {
        return this.injected.editorStore;
    }

    get allowDelete () {
        if (this.props.readOnly) {
            return false;
        }

        if (JobPermission.hasDeleteRight) {
            return true;
        }

        return false;
    }

    get allowEdit () {
        const currentJob = this.store.currentItem;

        if (this.props.readOnly) {
            return false;
        }

        if (!currentJob) {
            return false;
        }

        // the user has rights to update and the job is not new
        if (!currentJob.getPhantom() && JobPermission.hasUpdateRight) {
            return true;
        }

        // the user has rights to create a new job
        if (currentJob.getPhantom() && JobPermission.hasCreateRight) {
            return true;
        }

        return false;
    }

    get headerTabs () {
        return (
            <TabBar
                onClick={this.injected.handleEditorTabClick}
                defaultSelectedKeys={[this.props.currentTab]}
            >
                <Menu.Item key={Pages.Editor} data-qaid='parameters'>
                    {i18n.t('admin.jobs>parameters')}
                </Menu.Item>
                <Menu.Item key={Pages.Manager} data-qaid='manager'>
                    {i18n.t('admin.jobs>launchManager')}
                </Menu.Item>
            </TabBar>
        );
    }

    get extraButtons () {
       return (
            <React.Fragment>
                {!this.props.readOnly && (
                    <>
                        <Button
                            leftIcon={'save'}
                            isRound={true}
                            intent={INTENT.PRIMARY}
                            isDisabled={!this.store.canSave}
                            onClick={this.injected.handleSaveJobClick}
                            data-qaid='saveButton'
                        >
                            {i18n.t('admin.labels>saveButtonText')}
                        </Button>
                        &nbsp;
                        &nbsp;
                    </>
                )}
                {this.allowDelete && (
                    <ButtonWithConfirmation
                        isRound={true}
                        leftIcon={'trash2'}
                        isDisabled={!this.store.canDestroy}
                        onConfirm={this.injected.handleDestroyJobClick}
                        confirmationMessage={i18n.t('admin.jobs>destroyConfirmation')}
                        data-qaid='destroyButton'
                    />
                )}
            </React.Fragment>
        );
    }

    render () {
        const {currentTab} = this.props;

        if (!this.store.currentItem) { return null; }

        this.store.currentItem.setReactive([ReactiveProp.DIRTY]);

        return (
            <>
                <PageHeader
                    iconType='server'
                    sectionTitle={i18n.t('admin.jobs>editorTitle')}
                    groupSectionTitle={i18n.t('page.header>administration')}
                    itemTitle={this.store.currentItem.name.getValue()}
                    horizontalMenu={this.headerTabs}
                    extraButtons={this.extraButtons}
                />
                <div className={styles.jobEditorContainer}>
                    {currentTab === Pages.Editor && <JobForm readOnly={!this.allowEdit} />}
                    {currentTab === Pages.Manager && <LaunchManager/>}
                </div>
            </>
        );
    }
}
