﻿/**
 * Form with the properties of operations
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-08
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {CardPanel, Field, Switcher} from '@unidata/uikit';
import {Job} from '../../../../model/Job';
import {JobMetaHash} from '../../../../type/JobType';
import {getErrorMessage} from '@unidata/core-app';
import {MetaParameter} from '../../../../model/MetaParameter';

interface IProps {
    readOnly: boolean;
    job: Job;
    allTags: string[];
    allMetaData: JobMetaHash;
    onChange: (name: string, value: string | boolean) => void;
    onJobNameReferenceChange: (value: string, metaData: MetaParameter[]) => void;
}

@observer
export class PropertiesForm extends React.Component<IProps> {

    get renderTitle () {
        const checked: boolean = this.props.job.enabled.getValue();
        const labelKey: string = checked ? 'active' : 'inactive';

        return (
            <div>
                <Switcher
                    size='small'
                    checked={checked}
                    disabled={this.props.readOnly}
                    onChange={this.handleEnabledChange}
                    data-qaid='activeSwitcher'
                />
                <span> {i18n.t(`admin.jobs>${labelKey}`)}</span>
            </div>
        );
    }

    get allTags () {
        return this.props.allTags.map((tag: string) => {
            return {title: tag, value: tag};
        });
    }

    get formattedMetaData () {
        return Object.keys(this.props.allMetaData).map((key: string) => {
            return {
                title: key,
                value: key
            };
        });
    }

    get jobTags () {
        return this.props.job.tags.getValue() ? this.props.job.tags.getValue().slice() : [];
    }

    handleEnabledChange = (value: boolean) => {
        this.props.onChange('enabled', value);
    }

    handleJobNameReferenceChange = (name: string, value: string) => {
        this.props.onJobNameReferenceChange(value, this.props.allMetaData[value]);
    }

    render () {
        const {job, onChange, allMetaData, readOnly} = this.props;

        return (
            <CardPanel
                internal
                title={this.renderTitle}
            >
                <Field.Select
                    name='jobNameReference'
                    data-qaid='jobNameReference'
                    label={i18n.t('admin.jobs>type')}
                    options={this.formattedMetaData}
                    value={job.jobNameReference.getValue() || undefined}
                    onSelect={this.handleJobNameReferenceChange}
                    disabled={readOnly || !job.phantom}
                    error={getErrorMessage(job, 'jobNameReference')}
                />

                <Field.Input
                    name='name'
                    data-qaid='name'
                    label={i18n.t('admin.jobs>name')}
                    defaultValue={job.name.getValue()}
                    onChange={onChange}
                    disabled={readOnly}
                    error={getErrorMessage(job, 'name')}
                />

                <Field.Textarea
                    name='description'
                    data-qaid='description'
                    label={i18n.t('admin.jobs>description')}
                    defaultValue={job.description.getValue()}
                    onChange={onChange}
                    disabled={readOnly}
                    error={getErrorMessage(job, 'description')}
                />

                <Field.Input
                    name='cronExpression'
                    data-qaid='cronExpression'
                    label={i18n.t('admin.jobs>cronExpression')}
                    defaultValue={job.cronExpression.getValue()}
                    onChange={onChange}
                    disabled={readOnly}
                    error={getErrorMessage(job, 'cronExpression')}
                />

                <Field.Select
                    name='tags'
                    data-qaid='tags'
                    mode='tags'
                    label={i18n.t('admin.jobs>tags')}
                    options={this.allTags}
                    value={this.jobTags}
                    onSelect={(value) => onChange('tags', value)}
                    onDeselect={(value) => onChange('tags', value)}
                    disabled={readOnly}
                />
            </CardPanel>
       );
    }
}
