﻿/**
 * Form with parameters for starting the operation
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-08
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {Job} from '../../../../model/Job';
import {JobTrigger} from '../../../../model/JobTrigger';
import {TriggerTypes} from '../../../../type/TriggerTypes';
import {Field, Value} from '@unidata/uikit';

interface IProps {
    triggerType: TriggerTypes;
    trigger: JobTrigger | null;
    jobs: Job[];
    readOnly: boolean;
    handleTriggerChange: (triggerType: string, jobId: number | null) => void;
}

@observer
export class TriggerForm extends React.Component<IProps> {

    get options () {
        return this.props.jobs.map((job: Job) => {
            return {title: job.name.getValue(), value: job.id.getValue()};
        });
    }

    get currentValue () {
        if (this.props.trigger) {
            return this.props.trigger.startJobId.getValue();
        }

        return undefined;
    }

    handleSelect = (name: string, value: string) => {
        this.props.handleTriggerChange(name, Number(value));
    }

    handleChange = (name: string, value: Value) => {
        if (value === undefined || value === null) {
            this.props.handleTriggerChange(name, null);
        }
    }

    render () {
        const {triggerType, readOnly} = this.props;

        return (
            <Field.Select
                name={triggerType}
                label={i18n.t(`admin.jobs>${triggerType}`)}
                placeholder={i18n.t(`admin.jobs>nextOperation`)}
                options={this.options}
                value={this.currentValue}
                onSelect={this.handleSelect}
                onChange={this.handleChange}
                allowClear={true}
                disabled={readOnly}
                data-qaid={`${triggerType}_item`}
            />
       );
    }
}
