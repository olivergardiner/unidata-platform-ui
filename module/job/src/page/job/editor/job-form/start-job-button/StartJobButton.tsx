import * as React from 'react';
import {Icon, SIZE} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';

import * as styles from './startJobButton.m.scss';

interface IProps {
    text: string;
    iconName: string;
    isDisabled: boolean;
    onClick: any;
}

export class StartJobButton extends React.Component<IProps, any> {
    render () {

        const classNames = [styles.container];

        if (this.props.isDisabled === true) {
            classNames.push(styles.isDisabled);
        }

        return (
            <div onClick={this.props.onClick} className={classNames.join(' ')} data-qaid='startJobButton'>
                <div className={styles.icon}><Icon name={this.props.iconName as any} size={SIZE.EXTRA_LARGE}/></div>
                <div className={styles.text}>{i18n.t(this.props.text)}</div>
            </div>
        );
    }
}
