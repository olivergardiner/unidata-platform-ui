﻿/**
 * Form with parameters of the operation
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-08
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {CardPanel, Field, DateInput} from '@unidata/uikit';
import {MetaParameter} from '../../../../model/MetaParameter';
import {JobMetaHash, JobSpecialName, ParameterType} from '../../../../type/JobType';
import {ueModuleManager, UeModuleTypeCallBack} from '@unidata/core';
import {isObservableArray} from 'mobx';
import * as moment from 'moment';

interface IProps {
    jobType: string;
    jobParameters: any;
    metaData: JobMetaHash;
    readOnly: boolean;
    onChange: (name: string, value: (string | number | boolean | null), meta: MetaParameter, jobParameter: MetaParameter) => void;
}

const JAVA_COMPOTABLE_ZONED_DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSSZ';

@observer
export class ParametersForm extends React.Component<IProps> {

    //TODO: fix types
    private UEModules: any;

    constructor (props: IProps) {
        super(props);

        this.UEModules = ueModuleManager.getModulesByType(UeModuleTypeCallBack.JOB_PARAMETER);
    }

    get currentMetaProperty () {
        const {jobType, metaData} = this.props;

        if (!jobType) {
            return [];
        }

        return metaData[jobType] || [];
    }

    buildParameterField = (metaParameter: MetaParameter) => {

        let metaParameterName = metaParameter.name.getValue(),
            metaParameterValue = metaParameter.value.getValue();

        const jobParameter = this.findParameterByName(metaParameterName);

        const commonProps = {
            key: metaParameterName,
            name: metaParameterName,
            label: metaParameterName,
            disabled: this.props.readOnly
        };

        const handleChange = (name: string, value: string | boolean) => {
            this.props.onChange(name, value, metaParameter, jobParameter);
        };

        //TODO: fix type
        const builders = this.UEModules && this.UEModules.filter((module: any) => {
            return module.default.meta.metaParameterName === metaParameterName;
        });

        if (builders && builders[0]) {
            return builders[0].default.fn(commonProps, jobParameter, handleChange);
        }

        switch (metaParameter.type.getValue()) {
            case ParameterType.Boolean:
                return this.checkboxBuilder(commonProps, jobParameter, handleChange);
            case ParameterType.Date:
                return this.datePickerBuilder(commonProps, jobParameter, handleChange);
            default:
                if (metaParameterName === JobSpecialName.definitionContent) {
                    // UN-6882
                    return this.extendedInputBuilder(commonProps, jobParameter, handleChange);
                } else {
                    if (metaParameterValue instanceof Array) {
                        return this.selectBuilder(commonProps, jobParameter, handleChange, metaParameter);
                    } else {
                        return this.inputBuilder(commonProps, jobParameter, handleChange);
                    }
                }
        }
    };

    checkboxBuilder = (
        props: { key: string; name: string; label: string },
        jobParameter: MetaParameter,
        handleChange: (name: string, value: boolean) => void
    ): JSX.Element => {
        return (
            <Field.Checkbox
                {...props}
                key={jobParameter.name.getValue()}
                onChange={handleChange}
                defaultChecked={jobParameter ? jobParameter.value.getValue() : null}
                data-qaid={jobParameter.name.getValue()}
            />
        );
    }

    datePickerBuilder = (
        props: { key: string; name: string; label: string },
        jobParameter: MetaParameter,
        handleChange: (name: string, value: string) => void
    ): JSX.Element => {
        const val = moment(jobParameter.value.getValue());

        const handlePickerChange = (value: any) => {
            handleChange(name, value.format(JAVA_COMPOTABLE_ZONED_DATE_TIME_FORMAT));
        };

        return (<Field
            {...props}
            key={jobParameter.name.getValue()}
            data-qaid={jobParameter.name.getValue()}
        >
            <DateInput
                format='DD-MM-YYYY'
                onChange={handlePickerChange}
                value={val}
            />
        </Field>);
    };

    inputBuilder = (
        props: { key: string; name: string; label: string },
        jobParameter: MetaParameter,
        handleChange: (name: string, value: string) => void
    ): JSX.Element => {
        return (
            <Field.Input
                {...props}
                type='text'
                key={jobParameter.name.getValue()}
                data-qaid={jobParameter.name.getValue()}
                defaultValue={jobParameter ? jobParameter.value.getValue() : null}
                onChange={handleChange}
            />
        );
    }

    extendedInputBuilder = (
        props: { key: string; name: string; label: string },
        jobParameter: MetaParameter,
        handleChange: (name: string, value: string) => void
    ): JSX.Element => {
        return (
            <Field.Textarea
                {...props}
                key={jobParameter.name.getValue()}
                data-qaid={jobParameter.name.getValue()}
                defaultValue={jobParameter ? jobParameter.value.getValue() : null}
                onChange={handleChange}
            />
        );
    }

    selectBuilder = (
        props: { key: string; name: string; label: string },
        jobParameter: MetaParameter,
        handleChange: (name: string, value: string) => void,
        item: MetaParameter
    ): JSX.Element => {

        const value = () => {
            let value = '';

            if (jobParameter) {
                value = jobParameter.value.getValue();

                if (isObservableArray(value)) {
                    value = value.slice();
                }
            }

            return value;
        };

        const mode = () => {
            return item.multi_select.getValue() ? 'multiple' : 'default';
        };

        return (
            <Field.Select
                {...props}
                mode={mode()}
                key={jobParameter.name.getValue()}
                data-qaid={jobParameter.name.getValue()}
                onSelect={handleChange}
                onDeselect={handleChange}
                value={value()}
                options={(item.value.getValue() as []).map((i) => {
                    return {title: i, value: i};
                })}
            />
        );
    }

    buildArrayValue (value: string, delimiter: string): string[] {
        if (value && value.length) {
            return value.split(delimiter);
        }

        return [];
    }

    findParameterByName = (name: string) => {
        return this.props.jobParameters.find((item: MetaParameter) => item.name.getValue() === name);
    }

    render () {
        return (
            <CardPanel
                internal
                title={i18n.t('admin.jobs>parameters')}
                data-qaid='jobSettingsCardPanel'
            >
                {this.currentMetaProperty.map((item) => this.buildParameterField(item))}
            </CardPanel>
        );
    }
}
