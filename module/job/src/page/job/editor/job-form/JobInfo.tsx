﻿/**
 * The information bar about the operation
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-11
 */

import * as React from 'react';
import * as moment from 'moment';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {CardPanel, Col, Row} from '@unidata/uikit';
import {Job} from '../../../../model/Job';
import {JobStatus} from '../../../../type/JobType';
import {JobPermission} from '../../JobPermission';
import {StartJobButton} from './start-job-button/StartJobButton';

interface IProps {
    job: Job;
    onRunClick: () => void;
    onStopClick: () => void;
}

@observer
export class JobInfo extends React.Component<IProps> {

    static TIME_FORMAT: string = 'YYYY-MM-DD HH:mm:ss';

    get lastExecution () {
        return this.props.job.lastExecution || {};
    }

    get startTime () {
        const time: string | null = this.lastExecution.startTime;

        if (time) {
            return moment(time).format(JobInfo.TIME_FORMAT);
        }

        return '';
    }

    get endTime () {
        const time: string | null = this.lastExecution.endTime;

        if (time) {
            return moment(time).format(JobInfo.TIME_FORMAT);
        }

        return '';
    }

    get executionStatus () {
        const status: string | null = this.lastExecution.status;

        if (status) {
            return i18n.t(`admin.jobs>statuses>${status}`);
        }

        return '';
    }

    get buttonAttributes () {
        const jobStatus = this.props.job.lastExecution ? this.props.job.lastExecution.status : 'DEFAULT';

        let icon = 'play-circle';
        let tKey = 'admin.jobs>launch';
        let action = this.props.onRunClick;

        if (jobStatus === JobStatus.Progress) {
            icon = 'border';
            tKey = 'admin.jobs>stop';
            action = this.props.onStopClick;
        }

        return {icon, tKey, action};
    }

    render () {
        const {job} = this.props;

        return (
            <CardPanel>
                <Row gutter={40}>
                    <Col className='info-panel-left' span={12}>
                        <StartJobButton
                            iconName={this.buttonAttributes.icon}
                            text={this.buttonAttributes.tKey}
                            onClick={this.buttonAttributes.action}
                            isDisabled={!JobPermission.hasExecuteRight || !job.enabled.getValue() || job.getPhantom() || job.getDirty()}
                        />
                    </Col>

                    <Col className='info-panel-right' span={12}>
                        <Row justify='center'>
                            <Col span={24} data-qaid='lastExecutionStatus'>
                               <b>{i18n.t('admin.jobs>lastExecutionStatus')}:</b> {this.executionStatus}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12} data-qaid='startTime'>
                               <b>{i18n.t('admin.jobs>execution>startTime')}:</b> {this.startTime}
                            </Col>
                            <Col span={12} data-qaid='endTime'>
                               <b>{i18n.t('admin.jobs>execution>endTime')}:</b> {this.endTime}
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </CardPanel>
       );
    }
}
