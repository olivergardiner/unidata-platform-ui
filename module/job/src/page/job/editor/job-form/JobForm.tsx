/**
 * Form with parameters of the operation
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-08
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {Col, Row} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import {EditorStore} from '../../store/EditorStore';
import {ListStore} from '../../store/ListStore';
import {CardPanel} from '@unidata/uikit';
import {JobInfo} from './JobInfo';
import {PropertiesForm} from './PropertiesForm';
import {ParametersForm} from './ParametersForm';
import {TriggerForm} from './TriggerForm';
import {TriggerTypes} from '../../../../type/TriggerTypes';
import * as styles from './../jobEditor.m.scss';

interface IProps {
    readOnly: boolean;
}

interface IInjectProps extends IProps {
    editorStore: EditorStore;
    listStore: ListStore;
    handleEditorTabClick: ({key}: {key: string}) => void;
    handleRunJobClick: () => void;
    handleStopJobClick: () => void;
}

@inject('editorStore', 'listStore', 'handleEditorTabClick', 'handleRunJobClick', 'handleStopJobClick')
@observer
export class JobForm extends React.Component<IProps> {

    get injected () {
        return this.props as IInjectProps;
    }

    get store () {
        return this.injected.editorStore;
    }

    get allTags () {
        return this.injected.listStore.allTags;
    }

    get metaData () {
        return this.injected.listStore.metaData;
    }

    get availableJobs () {
        const jobs = this.injected.listStore.allJobs.getRange();
        const selectedJob = this.injected.listStore.currentItem;

        if (!selectedJob) {
            return [];
        }

        const currentItemId = selectedJob.id.getValue();

        return jobs.filter((i) => i.id.getValue() !== currentItemId);
    }

    get triggers () {
        return this.store.triggers;
    }

    render () {
        const {currentItem} = this.injected.editorStore;
        const {handleRunJobClick, handleStopJobClick} = this.injected;
        const {readOnly} = this.props;

        return (
            <React.Fragment>
                {currentItem && currentItem!.getPhantom() ||
                    <div>
                        <JobInfo
                            job={currentItem!}
                            onStopClick={handleStopJobClick}
                            onRunClick={handleRunJobClick}
                        />
                    </div>
                }
                <br/>

                <div className={styles.jobFormContainer}>
                    <CardPanel title={i18n.t('admin.jobs>settings')}>
                        <Row gutter={40}>
                            <Col span={12}>
                                <PropertiesForm
                                    job={this.store.currentItem!}
                                    allTags={this.allTags}
                                    allMetaData={this.metaData}
                                    onChange={this.store.handleChange}
                                    onJobNameReferenceChange={this.store.handleJobNameReferenceChange}
                                    readOnly={readOnly}
                                />
                            </Col>

                            <Col span={12}>
                                <ParametersForm
                                    jobType={this.store.currentItem!.jobNameReference.getValue()}
                                    jobParameters={this.store.currentItem!.parameters}
                                    metaData={this.metaData}
                                    onChange={this.store.handleChangeParameter}
                                    readOnly={readOnly}
                                />
                            </Col>

                            <Col span={24}>
                                <CardPanel
                                    internal
                                    title={i18n.t('admin.jobs>runNextOperation')}
                                >
                                    <Col span={12} className='trigger-success-column'>
                                        <TriggerForm
                                            jobs={this.availableJobs}
                                            triggerType={TriggerTypes.afterSuccess}
                                            trigger={this.triggers[TriggerTypes.afterSuccess]}
                                            handleTriggerChange={this.store.handleTriggerChange}
                                            readOnly={readOnly}
                                        />
                                    </Col>

                                    <Col span={12} className='trigger-fail-column'>
                                        <TriggerForm
                                            jobs={this.availableJobs}
                                            triggerType={TriggerTypes.afterFail}
                                            trigger={this.triggers[TriggerTypes.afterFail]}
                                            handleTriggerChange={this.store.handleTriggerChange}
                                            readOnly={readOnly}
                                        />
                                    </Col>
                                </CardPanel>
                            </Col>
                        </Row>
                    </CardPanel>
                </div>
            </React.Fragment>
       );
    }
}
