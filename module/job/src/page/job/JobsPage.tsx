﻿/**
 * Operations page
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {Layout} from '@unidata/uikit';
import {JobList} from './list/JobList';
import {JobEditor} from './editor/JobEditor';
import {JobPermission} from './JobPermission';
import {i18n} from '@unidata/core-app';
import {ImportExportModal} from './import_export/ImportExportModal';
import {JobPageStore} from './store/Store';

interface IProps {
}

@observer
export class JobsPage extends React.Component<IProps> {

    static PAGE_CLASS: string = 'ud-page ud-page-with-sider ud-page-job ';

    store: JobPageStore = new JobPageStore();
    providerProps: any;

    constructor (props: IProps) {
        super(props);

        // the same props must be passed to the provider, otherwise mobs throws an exception
        // @see https://stackoverflow.com/questions/43550137/mobx-rerender-after-assign/43562824
        this.providerProps = this.store.props;
    }

    componentDidMount (): void {
        this.store.loadAll();
    }

    componentWillUnmount () {
        this.store.removeReloadInterval();
    }

    get readOnly () {
        return !JobPermission.hasCreateRight;
    }

    get currentPage () {
        return this.store.currentPage;
    }

    get wizardStore () {
        return this.store.wizardStore;
    }

    private openImportExportModal = (isOpen: boolean) => {
        this.store.setImportExportModalOpen(isOpen);
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>job');
    }

    render () {
        return (
            <Provider {...this.providerProps}>
                <Layout className={JobsPage.PAGE_CLASS}>
                    <JobList
                        readOnly={this.readOnly}
                        store={this.store}
                    />

                    <Layout.Content>
                        <JobEditor
                            currentTab={this.currentPage}
                        />
                        <ImportExportModal
                            readOnly={this.readOnly}
                            openModal={this.openImportExportModal}
                            isModalOpen={this.store.importExportModalOpen}
                            store={this.wizardStore}/>
                    </Layout.Content>
                </Layout>
            </Provider>
        );
    }
}
