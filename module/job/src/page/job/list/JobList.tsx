﻿/**
 * List of operations
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-03
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {ButtonWithConfirmation, MenuSider, Button, INTENT, SIZE, Confirm, Badge} from '@unidata/uikit';
import {Job} from '../../../model/Job';
import {Filter} from './Filter';
import {EnabledState} from '../../../type/EnabledState';
import {JobBageStatus, JobListData} from '../../../type/JobType';
import {JobPageStore} from '../store/Store';

interface IProps {
    readOnly: boolean;
    store: JobPageStore;
}


interface IState {
    isConfirmOpen: boolean;
    targetJob: Job;
}

const EmptyJob = new Job({});

@observer
export class JobList extends React.Component<IProps, IState> {
    selectedItem: Job | null = null;

    state = {
        isConfirmOpen: false,
        targetJob: EmptyJob
    }

    get columns () {
        return [
            {
                maxWidth: 20,
                minWidth: 20,
                Cell: ({original}: {[key: string]: JobListData}) => {

                    return (
                        <Badge status={JobBageStatus[original.status]} />
                    );
                }
            },
            {
                Cell: ({original}: {[key: string]: JobListData}) => {

                    return (
                        <div
                            className={original.enabled ? EnabledState.Active : EnabledState.Inactive}
                            title={original.name}
                        >
                            {original.name}
                        </div>
                    );
                }
            },
            {
                Cell: ({original}: {[key: string]: JobListData}) => {

                    return (
                        <div
                            className={original.enabled ? EnabledState.Active : EnabledState.Inactive}
                            title={original.jobNameReference}
                        >
                            {original.jobNameReference}
                        </div>
                    );
                }
            },
            {
                id: 'clone',
                maxWidth: 40,
                minWidth: 40,
                Cell: ({original}: {[key: string]: JobListData}) => {
                    return (
                        <ButtonWithConfirmation
                            isMinimal={true}
                            leftIcon={'copy'}
                            title={i18n.t('clone')}
                            confirmationMessage={i18n.t('unsavedChanges')}
                            onConfirm={this.onCloneJobButtonClick(original.model)}
                        />
                    );
                }
            }
        ];
    }

    get data () {
        return this.props.store.listStore.jobs.items.slice().map((job: Job) => {
            return {
                name: job.name.getValue(),
                jobNameReference: job.jobNameReference.getValue(),
                enabled: job.enabled.getValue(),
                status: job.lastExecution ? job.lastExecution.status : 'DEFAULT',
                key: job.id.getValue(),
                model: job
            } as JobListData;
        });
    }

    onAddJobButtonClick = () => {
        this.props.store.openEditorPage();
        this.props.store.handleAddJobClick();
    }

    onCloneJobButtonClick = (clone: Job) => {
        return () => {
            this.props.store.openEditorPage();
            this.props.store.handleCloneJobClick(clone);
        };
    }

    get addJobButton () {
        return this.props.store.isNeedConfirmToSwitchToNew() ?
            (
                <ButtonWithConfirmation
                    isRound={true}
                    intent={INTENT.SECONDARY}
                    size={SIZE.LARGE}
                    leftIcon={'plus'}
                    title={i18n.t('create')}
                    onConfirm={this.onAddJobButtonClick}
                    confirmationMessage={i18n.t('unsavedChanges')}
                    data-qaid='add-job'
                />
            ) :
            (
                <Button
                    isRound={true}
                    intent={INTENT.SECONDARY}
                    size={SIZE.LARGE}
                    leftIcon={'plus'}
                    title={i18n.t('create')}
                    onClick={this.onAddJobButtonClick}
                    data-qaid='add-job'
                />
            );
    }

    get actions () {
        return (
            <React.Fragment>
                {!this.props.readOnly && this.addJobButton}

                <Button
                    isRound={true}
                    intent={INTENT.SECONDARY}
                    size={SIZE.LARGE}
                    leftIcon={'puzzle'}
                    title={`${i18n.t('import')} / ${i18n.t('export')}`}
                    onClick={this.props.store.handleImportExportClick}
                    data-qaid='clone-job'
                />
            </React.Fragment>
        );
    }

    get filters () {
        return (
            <Filter
                tagOptions={this.props.store.listStore.tagOptions}
                statusOptions={this.props.store.listStore.statusOptions}
                sortOptions={this.props.store.listStore.sortOptions}
                onSearchParamsChange={this.props.store.handleSearchParamChange}
                searchParams={this.props.store.listStore.searchParams}
            />
        );
    }

    handleListItemClick = () => {
        this.setState({isConfirmOpen: false});
        this.selectedItem = this.state.targetJob;
        this.props.store.openEditorPage();
        this.props.store.handleListItemClick(this.selectedItem);
    }


    onSwitchClose = () => {
        this.setState({isConfirmOpen: false});
    }

    checkSwitch = (target: Job) => {
        if (this.props.store.isNeedConfirmToSwitch(target)) {
            this.setState({isConfirmOpen: true, targetJob: target});

            return;
        }

        this.setState({targetJob: target}, this.handleListItemClick);
    }

    render () {
        return (
            <React.Fragment>
                <MenuSider
                    title={i18n.t('admin.jobs>jobListTitle')}
                    data={this.data}
                    columns={this.columns}
                    actions={this.actions}
                    currentItem={this.props.store.listStore.currentItem}
                    onClick={this.checkSwitch}
                    filters={this.filters}
                />
                <Confirm
                    onConfirm={this.handleListItemClick}
                    confirmationMessage={i18n.t('unsavedChanges')}
                    isOpen={this.state.isConfirmOpen}
                    onClose={this.onSwitchClose}
                />
            </React.Fragment>
        );
    }
}
