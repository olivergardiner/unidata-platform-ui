/**
 * Input field with operation filter
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-10
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {IOption, Select, Value, Input, Row, Col, Icon} from '@unidata/uikit';
import {JobSearchParams} from '../../../type/JobType';

interface IProps {
    tagOptions: IOption[];
    statusOptions: IOption[];
    sortOptions: IOption[];
    onSearchParamsChange: (name: string, value: string | string[]) => void;
    searchParams: JobSearchParams;
}

interface IState {
    isCollapsed: boolean;
}

@observer
export class Filter extends React.Component<IProps, IState> {
    state = {
        isCollapsed: true
    };

    get searchField () {
        const {onSearchParamsChange, searchParams} = this.props;

        const suffixStyle = {
            display: (searchParams.text && searchParams.text.length > 0) ? 'inline-block' : 'none',
            cursor: 'pointer'
        };

        const suffix = () => {
            return (
                <Icon
                    name='close'
                    onClick={() => onSearchParamsChange('text', '')}
                    style={suffixStyle}
                />
            );
        };

        return (
            <Input
                name='text'
                onChange={(e) => onSearchParamsChange('text', e.target.value)}
                defaultValue={searchParams.text}
                placeholder={i18n.t('common:search')}
                suffix={suffix()}
                data-qaid='filter-job'
            />
        );
    }

    get filters () {
        const {
            tagOptions, statusOptions, sortOptions,
            onSearchParamsChange, searchParams
        } = this.props;

        return (
            <React.Fragment>
                {tagOptions.length > 0 &&
                <div data-qaid='tags-select'>
                    <Select
                        name='tags'
                        mode='multiple'
                        options={tagOptions}
                        onSelect={(value: Value) => onSearchParamsChange('tags', value as any)}
                        onChange={(value: Value) => onSearchParamsChange('tags', value as any)}
                        value={searchParams.tags.slice()}
                        placeholder={i18n.t('admin.jobs>chooseTags')}
                        data-qaid='select-tags'
                    />
                </div>
                }

                <Row gutter={8} className='ud-field-filters'>
                    <Col span={12}>
                        <div data-qaid='enabledStatus'>
                            <Select
                                name='enabledStatus'
                                options={statusOptions}
                                value={searchParams.enabledStatus || 'all'}
                                onSelect={(value: Value) => onSearchParamsChange('enabledStatus', value as any)}
                                data-qaid='status-input'
                            />
                        </div>
                    </Col>
                    <Col span={12}>
                        <div data-qaid='sortField'>
                            <Select
                                name='sortField'
                                options={sortOptions}
                                value={searchParams.sortField}
                                onSelect={(value: Value) => onSearchParamsChange('sortField', value as any)}
                                data-qaid='sort-field-select'
                            />
                        </div>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }

    handleAdditionClick = () => {
        this.setState((prevState) => ({
            isCollapsed: !prevState.isCollapsed
        }));
    }

    render () {
        const {isCollapsed} = this.state;

        return (
            <div>
                {this.searchField}
                <br/>
                <br/>
                <Row>
                    <Col className='additional' span={24}>
                        <a className='additional-link' onClick={this.handleAdditionClick} data-qaid='additional-link'>
                            {i18n.t('admin.jobs>filter')}
                        </a>
                    </Col>
                </Row>
                {isCollapsed || this.filters}
            </div>
        );
    }
}
