/**
 * Confirmation step import / export
 *
 * @author: Denis Makarov
 * @date: 2020-02-27
 */

import * as React from 'react';
import {i18n} from '@unidata/core-app';
import {ImportExportOperations} from '../type/ImportExportOperations';
import {Wizard, Alert} from '@unidata/uikit';
import * as styles from '../importExportModal.m.scss';

const {Footer, Navigation} = Wizard;

interface IProps {
    operation: ImportExportOperations;
    onSubmit: () => void;
}

enum MessageTypes {
    Info = 'info',
    Warning = 'warning'
}

export class ImportConfirmStep extends React.PureComponent<IProps> {
    get messages () {
        return [
            {alertType: MessageTypes.Info, text: i18n.t('admin.jobs>wizardImportExport>messages>import>info')},
            {alertType: MessageTypes.Warning, text: i18n.t('admin.jobs>wizardImportExport>messages>import>warning')}
        ];
    }

    render () {
        return (
            <React.Fragment>
                <Navigation/>
                <div className={styles.wizardStep}>
                    {this.messages.map((message) =>
                        <Alert
                            key={message.text}
                            type={message.alertType}
                            description={message.text}
                            message=''
                            showIcon={message.alertType === MessageTypes.Warning}
                        />
                    )}
                </div>
                <Footer/>
            </React.Fragment>
        );
    }
}
