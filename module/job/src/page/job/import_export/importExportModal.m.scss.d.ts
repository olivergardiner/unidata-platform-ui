export const modalContent: string;
export const wizardStep: string;
export const uploadStep: string;
export const actionSelect: string;
export const confirmStepHeader: string;
