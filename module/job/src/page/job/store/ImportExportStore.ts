/**
 * Store for wizard Import/Export
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-12
 */

import {action, observable} from 'mobx';
import {ImportExportOperations} from '../import_export/type/ImportExportOperations';
import {JobService} from '../../../service/JobService';
import {UploadFile} from '@unidata/uikit';

export class ImportExportStore {
    @observable public isSaving: boolean = false;
    @observable public file: UploadFile | null = null;
    @observable public currentOperation: ImportExportOperations | null;

    @action
    public handleChooseAction = (currentOperation: ImportExportOperations | null) => {
        this.currentOperation = currentOperation;
    };

    @action
    public handleUploadChange = (file: UploadFile) => {
        this.file = file;
    };

    @action
    public handleImport = (): Promise<void> => {
        if (this.isSaving || !this.file) {
            return Promise.reject();
        }

        this.isSaving = true;

        return JobService.importJobs(this.file).then(action((response) => {
            this.initState();
        })).catch((err: any) => {
            this.isSaving = false;
        });
    };

    @action
    public handleExport = (): Promise<void> => {
        return JobService.exportJobs().then(action(() => {
            this.initState();
        })).catch((err) => {
            this.isSaving = false;
        });
    };

    @action
    public initState = () => {
        this.file = null;
        this.isSaving = false;
        this.currentOperation = null;
    }
}
