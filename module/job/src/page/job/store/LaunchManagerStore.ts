/**
 * Store of the operation launch Manager
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-08
 */

import {action, observable} from 'mobx';
import {Job} from '../../../model/Job';
import {Dialog} from '@unidata/core-app';
import {i18n} from '@unidata/core-app';
import {JobActionService} from '../../../service/JobActionService';

export class LaunchManagerStore {
    @observable public currentItem: (Job | null) = null;
    @observable public currentExecutionId: (number | null) = null;

    @observable public executions: any[] = [];
    @observable public totalExecutions: number = 0;
    @observable public executionPageSize: number = 25;

    @observable public executionSteps: any[] = [];
    @observable public totalExecutionSteps: number = 0;
    @observable public completedExecutionSteps: number = 0;
    @observable public executionStepPageSize: number = 6;

    @observable public isDetailOpen: boolean = false;

    @action
    public setCurrentItem = (job: Job) => {
        this.currentItem = job;
        this.executionSteps = [];
        this.loadExecutions();
    }

    @action
    public loadExecutions = (page: number = 0, startAt: number = 0) => {
        this.executions = [];

        if (!this.currentItem!.getPhantom()) {
            JobActionService.getExecutions(this.currentItem!.id.getValue(), page, startAt, this.executionPageSize)
                .then(action((response: any) => {
                    this.executions = response.content;
                    this.totalExecutions = response.total_count;
                }))
                .catch((err) => {
                    console.log('JobError', err);
                });
        }
    }

    @action
    public hanldeExecutionClick = (executionId: number) => {
        this.currentExecutionId = executionId;
        this.executionSteps = [];
        this.loadExecutionSteps();
    }

    @action
    public loadExecutionSteps = (page: number = 0, startAt: number = 0) => {
        JobActionService.getExecutionSteps(this.currentExecutionId!, page, startAt, this.executionStepPageSize)
            .then(action((response: any) => {
                this.executionSteps = response.content;
                this.totalExecutionSteps = response.total_count;
                this.completedExecutionSteps = response.completed_count;
            }))
            .catch((err) => {
                console.log('JobError', err);
            });
    }

    @action
    public handleRestartExecutionStep = (executionId: number) => {
        JobActionService.restartExecutionStep(executionId).then(
            () => {
                this.loadExecutions();
                Dialog.showMessage('', i18n.t('admin.jobs>restartSuccessText'));
            },
            this.handleRequestFailed
        );
    }

    private handleRequestFailed = (er?: Error) => {
        let message: string;

        if (er instanceof Array) {
            message = er.map((error) => error.userMessageDetails || error.userMessage).join(' ');
        } else {
            message = i18n.t('admin.jobs>saveFailureTitle');
        }

        Dialog.showError(message, i18n.t('admin.jobs>saveFailureText'));

        return Promise.reject(er);
    }
}
