/**
 * Store of the operations editor
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-03
 */

import {action, isObservableArray, observable} from 'mobx';
import {Job} from '../../../model/Job';
import {JobTrigger} from '../../../model/JobTrigger';
import {MetaParameter} from '../../../model/MetaParameter';
import {JobSpecialName} from '../../../type/JobType';
import {TriggerTypes} from '../../../type/TriggerTypes';
import {Dialog} from '@unidata/core-app';
import {i18n} from '@unidata/core-app';
import isArray from 'lodash/isArray';
import {JobService} from '../../../service/JobService';
import {JobTriggerService} from '../../../service/JobTriggerService';

export class EditorStore {
    @observable public currentItem: (Job | null) = null;
    @observable public isSaving: boolean = false;
    @observable public triggers: {[keys in TriggerTypes]: JobTrigger | null} = {
        [TriggerTypes.afterFail]: null,
        [TriggerTypes.afterSuccess]: null
    };
    @observable public cronErrorMessage: string | undefined;

    @action
    public setCurrentItem = (job: Job | null) => {
        this.currentItem = job;
        this.cronErrorMessage = undefined;
        this.loadTriggers();
    }

    @action
    public setCurrentItemLastExecutionBy = (job: Job) => {
        if (this.currentItem) {
            this.currentItem.lastExecution = job.lastExecution;
        }
    }

    @action
    private loadTriggers = () => {
        if (this.currentItem && !this.currentItem.getPhantom()) {
            JobTriggerService.readJobTriggerList(this.currentItem!.id.getValue()).then(action((triggers: JobTrigger[]) => {
                this.triggers = triggers.reduce((result: any, trigger: JobTrigger) => {
                    const triggerType: TriggerTypes = trigger.successRule.getValue() ? TriggerTypes.afterSuccess : TriggerTypes.afterFail;

                    result[triggerType] = trigger;

                    return result;
                }, {
                    [TriggerTypes.afterSuccess]: this.triggers[TriggerTypes.afterSuccess],
                    [TriggerTypes.afterFail]: this.triggers[TriggerTypes.afterFail]
                });
            })).catch((err) => {
                console.log('JobError', err);
            });
        }
    }

    @action
    public clearTriggers = () => {
        this.triggers = {
            [TriggerTypes.afterSuccess]: null,
            [TriggerTypes.afterFail]: null
        };
    }

    @action
    public handleChange = (name: string, value: any) => {
        if (name === 'tags') {
            this.updateTags(value);
        } else {
            (this.currentItem as any)[name].setValue(value);
        }
    }

    @action
    public handleJobNameReferenceChange = (value: string, metaParameters: MetaParameter[]) => {
        if (value !== this.currentItem!.jobNameReference.getValue()) {
            this.currentItem!.parameters.removeAll();
        }

        this.currentItem!.jobNameReference.setValue(value);

        for (let metaParameter of metaParameters) {
            this.addParameter(metaParameter);
        }
    }

    @action
    private updateTags = (value: string) => {
        let tags: string[] = this.currentItem!.tags.getValue() || [];
        let indexOfTag: number = tags.indexOf(value);

        if (indexOfTag === -1) {
            tags.push(value);
        } else {
            tags.splice(indexOfTag, 1);
        }

        this.currentItem!.tags.setValue(tags);
    }

    @action
    public handleChangeParameter = (name: string, value: string, meta: MetaParameter, jobParameter: MetaParameter) => {
        let currentParameter: MetaParameter = jobParameter || this.addParameter(meta);
        let oldValue: any = currentParameter.value.getValue();

        if (meta.name.getValue() === JobSpecialName.usersSelector) {
            // if the parameter name is users Selector, we calculate value by concatenating the values using |
            value = this.computeParameterValue('|', value, oldValue);
        } else if (meta.multi_select.getValue() && isObservableArray(meta.value.getValue())) {
            // if the multi_select parameter and the meta contains an array of values, we modify the value array
            value = this.updateArrayValues(oldValue, value);
        }

        currentParameter.value.setValue(value);
    }

    private updateArrayValues (array: any, value: string) {
        let index: number;

        if (isObservableArray(array)) {
            array = array.slice();
        }

        if (array && !isArray(array)) {
            array = [array];
        }

        array = array || [];
        index = array.indexOf(value);

        if (index === -1) {
            array.push(value);
        } else {
            array.splice(index, 1);
        }

        return array;
    }

    private computeParameterValue (delimiter: string, value: string, oldValue?: string) {
        let values: string[] = [],
            index: number;

        if (oldValue && oldValue.length) {
            values = oldValue.split(delimiter);
        }

        index = values.indexOf(value);

        if (index === -1) {
            values.push(value);
        } else {
            values.splice(index, 1);
        }

        return values.join(delimiter);
    }

    @action
    private addParameter = (meta: MetaParameter): MetaParameter => {
        let metaParameterValue = meta.value.getValue();

        const dataParameter = new MetaParameter(meta.serialize());

        if (isObservableArray(metaParameterValue)) {
            dataParameter.value.setValue(metaParameterValue.slice()[0]);
        }

        this.currentItem!.parameters.add(dataParameter);

        return dataParameter;
    }

    @action
    public handleTriggerChange = (triggerType: TriggerTypes, jobId: number | null) => {
        let trigger: JobTrigger | null = this.triggers[triggerType];

        if (!trigger) {
            trigger = new JobTrigger({
                successRule: triggerType === TriggerTypes.afterSuccess
            });
        }

        trigger.startJobId.setValue(jobId);

        this.triggers[triggerType] = trigger;
    }

    @action
    public saveJob = (): Promise<void> => {
        let savePromise: Promise<any>;

        if (!this.currentItem!.getDirty()) {
            return Promise.resolve();
        }

        this.setSaving(true);

        this.currentItem!.validate();

        if (this.currentItem!.lastValidationResult.size > 0) {
            this.handleRequestFailed();

            return Promise.reject();
        }

        savePromise = JobService.saveJob(this.currentItem!);

        savePromise
            .then((job: Job) => {
                    this.currentItem!.id.setValue(job.id.getValue());
                    this.currentItem!.commit();
                },
                this.handleRequestFailed
            );

        return savePromise;
    }

    @action
    public saveTriggers = (): Promise<any> => {
        const jobId: number = this.currentItem!.id.getValue();
        const changedTriggers: JobTrigger[] = this.changedTriggers;
        const promises: Array<Promise<void>> = [];

        if (changedTriggers.length === 0) {
            return Promise.resolve();
        }

        for (let trigger of changedTriggers) {
            if (trigger.startJobId.getValue()) {
                trigger.name.setValue(`${jobId}_${trigger.successRule.getValue() ? 'success' : 'failure'}`);

                promises.push(new Promise<void>((resolve, reject) => {
                    return JobTriggerService.createJobTrigger(jobId, trigger).then(() => {
                        trigger.commit();
                    });
                }));
            } else if (!trigger.getPhantom()) {
                promises.push(new Promise<void>((resolve, reject) => {
                    return JobTriggerService.deleteJobTrigger(jobId, trigger.id.getValue()).then(() => {
                        trigger.commit();
                    });
                }));
            }
        }

        return Promise.all(promises);
    }

    @action
    public destroyJob = (): Promise<void> => {

        if (this.currentItem!.getPhantom()) {
            this.setCurrentItem(null);

            return this.handleRequestSuccess(i18n.t('admin.jobs>destroySuccessText'));
        }

        return JobService.deleteJob(this.currentItem!.id.getValue()).then(
            () => {
                this.setCurrentItem(null);
                this.handleRequestSuccess(i18n.t('admin.jobs>destroySuccessText'));
            },
            this.handleRequestFailed
        );
    }

    @action
    public runJob = (): Promise<void> => {
        return JobService.runJob(this.currentItem!.id.getValue()).then(
            () => {
                this.handleRequestSuccess(i18n.t('admin.jobs>runSuccessText'));
            },
            this.handleRequestFailed
        );
    }

    @action
    public stopJob = (): Promise<void> => {
        return JobService.stopJob(this.currentItem!.id.getValue()).then(
            () => {
                this.handleRequestSuccess(i18n.t('admin.jobs>stopSuccessText'));
            },
            this.handleRequestFailed
        );
    }

    @action
    private handleRequestSuccess = (message: string) => {
        this.setSaving(false);

        Dialog.showMessage('', message);

        return Promise.resolve();
    }

    private handleRequestFailed = (er?: any) => {
        let message: string;

        this.setSaving(false);

        if (er instanceof Array) {
            message = er.map((error) => error.userMessageDetails || error.userMessage).join(' ');
        } else if (er && er.isOperationError && er.axiosResponse) {
            message = er.axiosResponse.data.errors.map((error: any) => error.userMessageDetails || error.userMessage).join(' ');
        } else {
            message = i18n.t('admin.jobs>saveFailureTitle');
        }

        Dialog.showError(message, i18n.t('admin.jobs>saveFailureText'));
    }

    @action
    public setSaving = (isSaving: boolean) => {
        this.isSaving = isSaving;
    }

    public get canSave () {
        if (this.isSaving) {
            return false;
        }

        if (this.currentItem) {
            if (this.currentItem.getDirty()) {
                return true;
            }

            if (this.changedTriggers.length > 0) {
                return true;
            }
        }

        return false;
    }

    private get changedTriggers () {
        return Object.values(this.triggers).filter((item: JobTrigger | null) => {
                return  item !== null;
            }).filter((item: JobTrigger) => {
                return item.getDirty() || item.getPhantom();
            }) as JobTrigger[];
    }

    public get canDestroy () {
        if (this.isSaving) {
            return false;
        }

        return this.currentItem !== null;
    }
}
