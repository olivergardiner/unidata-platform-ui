/**
 *  Store of the list of operations
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-03
 */

import {action, observable} from 'mobx';
import {i18n} from '@unidata/core-app';
import {ModelCollection} from '@unidata/core';
import {Job} from '../../../model/Job';
import {JobMetaHash, JobSearchParams} from '../../../type/JobType';
import {JobMeta} from '../../../model/JobMeta';
import {JobService} from '../../../service/JobService';

export class ListStore {
    @observable public jobs: ModelCollection<Job> = ModelCollection.create(Job);
    @observable public currentItem: (Job | null) = null;
    @observable public searchParams: JobSearchParams = {
        text: '',
        enabledStatus: null,
        tags: [],
        sortField: 'name',
        sortDirection: 'ASC',
        page: 1,
        count: 9999,
        start: 0
    };

    @observable public allTags: string[] = [];
    public metaData: JobMetaHash = {};

    private currentPromise: Promise<any>;
    // list of all operations
    public allJobs: ModelCollection<Job> = ModelCollection.create(Job);

    // upload all the necessary data to get started
    public loadAll (): Promise<any> {
        let promises: Array<Promise<any>> = [];

        promises.push(this.loadItems());
        promises.push(this.loadMetaData());
        promises.push(this.loadTags());

        return Promise.all(promises).then(() => {
            // saving all available operations to a separate list
            this.allJobs = this.jobs.clone();
        });
    }

    // upload a list of works by parameters from the search
    public loadItems (): Promise<any> {
        let promise = JobService.searchJobs(this.buildDefaultSearchParams(this.searchParams)).then((jobs: Job[]) => {
            // canceling requests that are no longer relevant
            if (promise !== this.currentPromise) {
                return Promise.reject('search canceled');
            }

            if (this.currentItem) {
                // installing the new current process only if it appears in the upload
                let reloadedCurrentItem = jobs.find((job: Job) => job.id.getValue() === this.currentItem!.id.getValue());

                if (!reloadedCurrentItem) {
                    jobs.push(this.currentItem);
                }
            }

            this.jobs.clearCollection();
            this.jobs.replaceAll(jobs);

            if (this.currentItem) {
                this.setCurrentItem(this.jobs.find((job: Job) => job.id.getValue() === this.currentItem!.id.getValue()));
            }

            return  Promise.resolve();
        }).catch((err: string) => {
            console.log('JobError', err);
        });

        this.currentPromise = promise;

        return this.currentPromise;
    }

    private buildDefaultSearchParams (searchParams: JobSearchParams): any {
        // TODO: Make a separate structure for params
        let params = {
            text: searchParams.text,
            enabledStatus: searchParams.enabledStatus,
            tags: searchParams.tags.slice(), // making a slice to "protect" from ObservableArray in tags
            sortFields: [
                {
                    field: searchParams.sortField,
                    order: searchParams.sortDirection,
                    type: 'String'
                }
            ],
            page: searchParams.page,
            count: searchParams.count,
            start: searchParams.start,
            lastExecutionStatus: null
        };

        return params;
    }

    private loadMetaData (): Promise<any> {
        return JobService.readJobMetadata().then((jobMetas: JobMeta[]) => {
            this.metaData = jobMetas.reduce((result: JobMetaHash, jobMeta: JobMeta) => {
                let jobNameReference = jobMeta.jobNameReference.getValue();

                result[jobNameReference] = jobMeta.parameters.items.slice();

                return result;
            }, {});
        }).catch((err: string) => {
            console.log('JobError', err);
        });
    }

    public loadTags (): Promise<any> {
        return JobService.readJobTags().then(action((tags: string[]) => {
            this.allTags = tags;
        })).catch((err: string) => {
            console.log('JobError', err);
        });
    }

    @action
    public addJob = (clonedJob: Job | null): Job => {
        let job: (Job | undefined) = this.jobs.items.find((job) => job.getPhantom());

        if (job) {
            this.jobs.remove(job);
        }

        if (clonedJob) {
            let data: {[key: string]: any} = clonedJob.serialize();

            delete data.id;
            delete data.name;
            delete data.enabled;

            job = new Job(data);
        } else {
            job = new Job({});
        }

        this.jobs.add(job);
        this.setCurrentItem(job);
        this.allJobs.add(job);

        return job;
    }

    @action
    public setCurrentItem = (job: Job | null): void => {
        this.currentItem = job;
    }

    public get tagOptions () {
        return this.allTags.map((tag) => {
            return {title: tag, value: tag};
        });
    }

    public get statusOptions () {
        return [
            {title: i18n.t('admin.jobs>statusOptions>all'), value: 'all'},
            {title: i18n.t('admin.jobs>statusOptions>active'), value: 'active'},
            {title: i18n.t('admin.jobs>statusOptions>inactive'), value: 'inactive'}
        ];
    }

    public get sortOptions () {
        return [
            {title: i18n.t('admin.jobs>sortOptions>name'), value: 'name'},
            {title: i18n.t('admin.jobs>sortOptions>type'), value: 'type'}
        ];
    }

    @action
    public handleSearchParamChange = (name: string, value: string | string[]) => {
        if (name === 'tags' && value instanceof Array) {
            this.searchParams.tags = value;
        } else if (name !== 'tags') {
            if (name === 'enabledStatus' && value === 'all') {
                this.searchParams[name] = null;
            } else {
                this.searchParams[name] = value;
            }
        }

        this.loadItems();
    }
}
