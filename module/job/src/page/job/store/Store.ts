/**
 * Main Storer of the Operations page
 *
 * @author: Viktor Magarlamov
 * @date: 2019-07-03
 */

import {action, observable} from 'mobx';
import {i18n} from '@unidata/core-app';
import {ListStore} from './ListStore';
import {EditorStore} from './EditorStore';
import {ImportExportStore} from './ImportExportStore';
import {LaunchManagerStore} from './LaunchManagerStore';
import {Pages} from '@unidata/types';
import {Job} from '../../../model/Job';

export class JobPageStore {

    private static RELOAD_INTERVAL: number = 30000;
    private reloadInterval: any;

    @observable public currentPage: Pages = Pages.Editor;

    public listStore: any = new ListStore();
    public editorStore: any = new EditorStore();
    public launchManagerStore: any = new LaunchManagerStore();
    public wizardStore: any = new ImportExportStore();

    @observable public importExportModalOpen: boolean = false;

    constructor () {
        this.setReloadInterval();
    }

    public loadAll () {
        this.listStore.loadAll();
    }

    public get props () {
        let methods = Object.assign(
            {
                handleListItemClick: this.handleListItemClick,
                handleAddJobClick: this.handleAddJobClick,
                handleEditorTabClick: this.handleEditorTabClick,
                handleSaveJobClick: this.handleSaveJobClick,
                handleDestroyJobClick: this.handleDestroyJobClick,
                handleRunJobClick: this.handleRunJobClick,
                handleStopJobClick: this.handleStopJobClick,
                handleImportExportClick: this.handleImportExportClick,
                handleDetailsClick: this.handleDetailsClick,
                handleRestartExecutionStep: this.handleRestartExecutionStep,
                handleSearchParamChange: this.handleSearchParamChange,
                openEditorPage: this.openEditorPage,
                isNeedConfirmToSwitchToNew: this.isNeedConfirmToSwitchToNew
            }
        );

        for (let key of Object.keys(methods)) {
            methods[key] = methods[key].bind(this);
        }

        return Object.assign(this.stores, methods);
    }

    private setReloadInterval = () => {

        this.reloadInterval = setInterval(() => {
            let editorStore = this.editorStore;
            let currentItem = editorStore.currentItem;

            if (!currentItem || (!editorStore.canSave && !currentItem.getPhantom())) {
                this.listStore.loadItems().then(() => {
                    this.editorStore.setCurrentItem(this.listStore.currentItem);
                });
            }
        }, JobPageStore.RELOAD_INTERVAL);
    }

    public removeReloadInterval = () => {
        clearInterval(this.reloadInterval);
    }

    @action
    public setImportExportModalOpen (isOpen: boolean) {
        this.importExportModalOpen = isOpen;
    }

    public get stores () {
        return {
            listStore: this.listStore,
            editorStore: this.editorStore,
            launchManagerStore: this.launchManagerStore,
            wizardStore: this.wizardStore
        };
    }

    @action
    public handleListItemClick = (job: Job): void => {
        this.editorStore.clearTriggers();
        this.editorStore.setCurrentItem(job);
        this.listStore.setCurrentItem(job);
    }

    @action
    public handleAddJobClick = () => {
        let job = this.listStore.addJob(null);

        return this.editorStore.setCurrentItem(job);
    }

    @action
    public handleCloneJobClick = (source: Job) => {
        let clone = this.listStore.addJob(source);

        return this.editorStore.setCurrentItem(clone);
    }

    @action
    public handleSaveJobClick = () => {
        this.editorStore.saveJob()
            .then(
                () => {
                    this.editorStore.saveTriggers();
                    this.listStore.loadItems();
                    this.listStore.loadTags();
                    this.editorStore.handleRequestSuccess(i18n.t('admin.jobs>saveSuccessText'));
                },
                function () {
                });
    };

    @action
    public handleDestroyJobClick = () => {
        return this.editorStore.destroyJob().then(() => {
            this.listStore.setCurrentItem(null);
            this.listStore.loadItems();
        });
    }

    public handleRunJobClick = () => {
        return this.editorStore.runJob().then(() => {
            this.listStore.loadItems().then(() => {
                this.editorStore.setCurrentItem(this.listStore.currentItem);
            });
        });
    }

    public handleStopJobClick = () => {
        return this.editorStore.stopJob().then(() => {
            this.listStore.loadItems().then(() => {
                this.editorStore.setCurrentItem(this.listStore.currentItem);
            });
        });
    }

    @action
    public handleEditorTabClick = ({key}: {key: Pages}) => {
        this.currentPage = key;

        if (key === Pages.Manager) {
            this.launchManagerStore.setCurrentItem(this.listStore.currentItem);
        }
    }

    public handleImportExportClick = () => {
       this.setImportExportModalOpen(true);
    }

    @action
    public handleDetailsClick = () => {
        this.launchManagerStore.isDetailOpen = true;
    }

    public handleRestartExecutionStep = (id: number) => {
        this.launchManagerStore.handleRestartExecutionStep(id);
    }

    public handleSearchParamChange = (name: string, value: string | null | string[]) => {
        this.editorStore.setCurrentItem(null);
        this.listStore.setCurrentItem(null);
        this.listStore.handleSearchParamChange(name, value);
    }

    @action
    public openEditorPage = () => {
        this.currentPage = Pages.Editor;
    }

    public isNeedConfirmToSwitchToNew = (): boolean => {
        return Boolean(this.editorStore.canSave);
    }

    public isNeedConfirmToSwitch = (target: Job): boolean => {
        return Boolean(this.editorStore.canSave) &&
            this.listStore.currentItem.modelId !== target.modelId;
    }
}
