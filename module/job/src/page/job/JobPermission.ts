/**
 * The guard of rights on the jobs
 *
 * @author Ivan Marshalkin
 * @date 2019-08-07
 */

import {Res, ResourceManager, Right} from '@unidata/core-app';

export class JobPermission {
    // Determines whether the user has been delegated the rights to create operations
    public static get hasCreateRight (): boolean {
        if (ResourceManager.userHasRight(Res.DATA_OPERATIONS_MANAGEMENT, Right.CREATE)) {
            return true;
        }

        if (ResourceManager.userHasRight(Res.ADMIN_SYSTEM_MANAGEMENT, Right.CREATE)) {
            return true;
        }

        return false;
    }

    // Determines whether the user has been delegated the right to delete operations
    public static get hasDeleteRight (): boolean {
        if (ResourceManager.userHasRight(Res.DATA_OPERATIONS_MANAGEMENT, Right.DELETE)) {
            return true;
        }

        if (ResourceManager.userHasRight(Res.ADMIN_SYSTEM_MANAGEMENT, Right.DELETE)) {
            return true;
        }

        return false;
    }

    // Determines whether the user has been delegated the right to delete operations
    public static get hasUpdateRight (): boolean {
        if (ResourceManager.userHasRight(Res.DATA_OPERATIONS_MANAGEMENT, Right.UPDATE)) {
            return true;
        }

        if (ResourceManager.userHasRight(Res.ADMIN_SYSTEM_MANAGEMENT, Right.UPDATE)) {
            return true;
        }

        return false;
    }

    // Determines whether the user has been delegated the rights to launch operations
    public static get hasExecuteRight (): boolean {
        if (ResourceManager.userHasRight(Res.ADMIN_SYSTEM_MANAGEMENT, Right.READ)) {
            return true;
        }

        if (ResourceManager.userHasRight(Res.EXECUTE_DATA_OPERATIONS, Right.READ)) {
            return true;
        }

        return false;
    }
}
