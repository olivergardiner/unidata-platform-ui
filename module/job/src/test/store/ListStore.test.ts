/**
 * Testim store Operations/List of operations
 *
 * @author: Vladimir Stebunov
 * @date: 2019-12-11
 */

import {ListStore} from '../../component/jobs/store/ListStore';
import {Job} from '../../model/Job';
import mockAxios from '../mocks/axios';

describe('Job List Store',  () => {
    it('can create', () => {
        const listStore = new ListStore();

        const defaultSearchParam = {
            count: 9999,
            enabledStatus: null,
            page: 1,
            sortDirection: 'ASC',
            sortField: 'name',
            start: 0,
            tags: [],
            text: ''
        };


        expect(listStore.searchParams).toEqual(defaultSearchParam);
        expect(listStore.jobs.items).toEqual([]);

    });

    it('can load all', async () => {

        const simpleJob = new Job({id: '999', name: 'jobTest'});

        (mockAxios as jest.Mock).mockResolvedValueOnce({
                status: 200,
                data: {
                    success: true,
                    errors: null,
                    content: [{
                        id: '999',
                        name: 'jobTest'
                    }]
                }
            }
        );

        const listStore = new ListStore();

        await listStore.loadAll();

        expect(listStore.jobs.items.length).toEqual(1);
        expect(listStore.jobs.items[0].id.getValue()).toEqual(simpleJob.id.getValue());
        expect(listStore.jobs.items[0].name.getValue()).toEqual(simpleJob.name.getValue());
    });

    it('dont erase full list after search',  async () => {
        const listStore = new ListStore();

        (mockAxios as jest.Mock).mockResolvedValueOnce({
                status: 200,
                data: {
                    success: true,
                    errors: null,
                    content: [{
                        id: '999',
                        name: 'jobTest'
                    }]
                }
            }
        );

        await listStore.loadAll();
        await listStore.loadItems();
        expect(listStore.jobs.items.length).toEqual(0);
        expect(listStore.allJobs.items.length).toEqual(1);
    });

    it('add job and select new item as current', () => {
        const listStore = new ListStore();
        const newJob = new Job({});

        listStore.addJob(newJob);

        expect(listStore.allJobs.items.length).toEqual(1);
        expect(listStore.currentItem).toBe(listStore.jobs.get(0));
        expect(listStore.jobs.size()).toEqual(1);
    });

    it('search dont lost job after adding', async () => {
        const listStore = new ListStore();
        const newJob = new Job({});

        (mockAxios as jest.Mock).mockResolvedValueOnce({
                status: 200,
                data: {
                    success: true,
                    errors: null,
                    content: [{
                        id: '999',
                        name: 'jobTest'
                    }]
                }
            }
        );
        await listStore.loadAll();
        listStore.addJob(newJob);

        await listStore.loadItems().then(() => {
            expect(listStore.jobs.size()).toEqual(1);
            expect(listStore.currentItem).toEqual(newJob);
        });


    });


});
