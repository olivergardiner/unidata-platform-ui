/**
 * test for EntityGroupHelper
 *
 * @author: Denis Makarov
 * @date: 2019-08-29
 */

import {EntityGroupHelper} from './EntityGroupHelper';
import {Catalog} from '../../model/entity/Catalog';
import {EntityTreeNode} from '../../model/entity/EntityTreeNode';

let resultMock = {
    id: undefined,
    name: undefined,
    children: [
        {
            id: 'entity1',
            name: 'entity1',
            children: []
        },
        {
            id: 'entity2',
            name: 'entity2',
            children: []
        },
        {
            id: 'first',
            name: 'first',
            children: [
                {
                    id: 'entity3',
                    name: 'entity3',
                    children: []
                },
                {
                    id: 'entity4',
                    name: 'entity4',
                    children: []
                },
                {
                    id: 'first.second',
                    name: 'second',
                    children: [
                        {
                            id: 'entity5',
                            name: 'entity5'
                        },
                        {
                            id: 'entity6',
                            name: 'entity6'
                        },
                        {
                            id: 'lookup1',
                            name: 'lookup1'
                        },
                        {
                            id: 'lookup2',
                            name: 'lookup2'
                        }
                    ]
                }
            ]
        }
    ]

};

let resultFilterMock = {
    id: undefined,
    name: undefined,
    children: [
        {
            id: 'entity1',
            name: 'entity1',
            children: []
        },
        {
            id: 'entity2',
            name: 'entity2',
            children: []
        },
        {
            id: 'first',
            name: 'first',
            children: [
                {
                    id: 'entity4',
                    name: 'entity4',
                    children: []
                }
            ]
        }
    ]

};

describe('EntityGroupHelper', () => {
    describe('#buildTreeNodeModel', () => {
        it('correctly builds empty structure', () => {
            let treeNodeModel = EntityGroupHelper.buildTreeNodeModel([]);

            expect(treeNodeModel.children.size()).toEqual(0);
        });

        it('don\'t fall for null', () => {
            expect(() => EntityGroupHelper.buildTreeNodeModel(null!)).not.toThrow();
        });

        it('correctly builds catalogs', () => {
            let catalogMocks = getFilledCatalogMocks();
            let result = EntityGroupHelper.buildTreeNodeModel(catalogMocks).serialize();

            expect(result).toMatchObject(resultMock);
        });

        it('correctly applies filter', () => {
            let catalogMocks = getFilledCatalogMocks();

            let filter = function (node: EntityTreeNode) {
                return node.name.getValue() !== 'entity3' && node.name.getValue() !== 'second';
            };

            let result = EntityGroupHelper.buildTreeNodeModel(catalogMocks, filter).serialize();

            expect(result).toMatchObject(resultFilterMock);
        });
    });
});

function getFilledCatalogMocks () {
    let catalog1 = new Catalog({
        groupName: 'ROOT',
        entities: [
            {name: 'entity1', displayName: 'entity 1'},
            {name: 'entity2', displayName: 'entity 2'}
        ]
    });

    let catalog2 = new Catalog({
        groupName: 'ROOT.first',
        entities: [
            {name: 'entity3', displayName: 'entity 3'},
            {name: 'entity4', displayName: 'entity 4'}
        ]
    });

    let catalog3 = new Catalog({
        groupName: 'ROOT.first.second',
        entities: [
            {name: 'entity5', displayName: 'entity 5'},
            {name: 'entity6', displayName: 'entity 6'}
        ],
        lookupEntities: [
            {name: 'lookup1', displayName: 'lookup 1'},
            {name: 'lookup2', displayName: 'lookup 2'}
        ]
    });

    return [catalog1, catalog2, catalog3];
}
