/**
 * A helper class for groups of directories/registers
 * @author: Sergey Shishigin
 * @date: 2018-09-21
 */

import concat from 'lodash/concat';
import each from 'lodash/each';

import {
    Catalog,
    CatalogAbstractEntity,
    CatalogEntity,
    CatalogLookupEntity,
    EntityTreeNode,
    EntityTreeNodeType
} from '../../model';
import {Nullable} from '@unidata/core';

/**
 * @deprecated
 */
export class EntityGroupHelper {
    private static DELIMITER = '.';

    /**
     * Build a model for the root node of the directory/registry group tree
     *
     * @deprecated
     * @param {Catalog[]} catalogs elements of a flat list of reference/registry groups
     * @param {Function} filter elements of a flat list of reference/registry groups
     * @return {Tree Node Model} Root node of the directory/registry tree
     */
    public static buildTreeNodeModel (catalogs: Catalog[], filterFn?: (node: EntityTreeNode) => boolean): EntityTreeNode {
        let rootTreeNodeModel = new EntityTreeNode({});

        // going through the elements of the flat folder
        each(catalogs, (catalog: Catalog) => {
            let groupName = catalog.groupName.getValue(),
                entityTreeNode: Nullable<EntityTreeNode> = new EntityTreeNode({}),
                childrenEntityTreeNode: EntityTreeNode,
                parts = groupName.split(this.DELIMITER),
                path: string;

            parts.shift();

            // iterating through the nodes deep in accordance with groupName, creating nodes along the way in case of their absence
            entityTreeNode = rootTreeNodeModel;
            each(parts, (part: string, index: number, collection: string[]) => {
                path = collection.slice(0, index + 1).join(this.DELIMITER);

                if (entityTreeNode) {
                    // apply the filter, if it is set
                    if (!filterFn || (filterFn && filterFn(entityTreeNode))) {
                        entityTreeNode = EntityGroupHelper.getNextGroupTreeNode(entityTreeNode, part, path, filterFn);
                    }
                }
            });

            // check that the current node was not filtered earlier
            if (entityTreeNode) {
                // filling the current node with values
                EntityGroupHelper.fillGroupTreeNode(entityTreeNode, catalog);

                // filling the node with child entities entity
                each(catalog.entities.items, (entity: CatalogEntity) => {
                    path = concat(parts, name).join(this.DELIMITER);

                    if (entityTreeNode) {
                        childrenEntityTreeNode = this.createEntityTreeNode(entity, EntityTreeNodeType.Entity, path, entityTreeNode);

                        // apply the filter, if it is set
                        if (!filterFn || (filterFn && filterFn(childrenEntityTreeNode))) {
                            entityTreeNode.children.add(childrenEntityTreeNode);
                        }
                    }
                });

                // filling the node with child entities lookup Entity
                each(catalog.lookupEntities.items, (entity: CatalogLookupEntity) => {
                    path = concat(parts, name).join(this.DELIMITER);

                    if (entityTreeNode) {
                        childrenEntityTreeNode = this.createEntityTreeNode(entity, EntityTreeNodeType.LookupEntity, path, entityTreeNode);

                        // apply the filter, if it is set
                        if (!filterFn || (filterFn && filterFn(childrenEntityTreeNode))) {
                            entityTreeNode.children.add(childrenEntityTreeNode);
                        }
                    }
                });
            }
        });

        return rootTreeNodeModel;
    }

    /**
     * Fill the found node with values from a flat list item in the folder
     *
     * @param {TreeNodeModel} treeNodeModel
     * @param {Catalog} catalog
     */
    private static fillGroupTreeNode (treeNodeModel: EntityTreeNode, catalog: Catalog) {
        treeNodeModel.title.setValue(catalog.title.getValue());
    }

    /**
     * Get the next Entity TreeNode node relative to the current one
     * If the node is missing, create it
     *
     * @deprecated
     * @param {Entity TreeNode} current Entity Group Tree Node Current node
     * @param {string} key the Key
     * @param {string} path Path to the next node
     * @param {Function} filter Path to the next node
     * @return {EntityTreeNode}
     */
    private static getNextGroupTreeNode (currentEntityGroupTreeNode: EntityTreeNode, key: string, path: string, filterFn?: (node: EntityTreeNode) => boolean): Nullable<EntityTreeNode> { // eslint-disable-line max-len
        let entityGroupTreeNode: Nullable<EntityTreeNode>,
            data: any;

        entityGroupTreeNode = currentEntityGroupTreeNode.children.find((t: EntityTreeNode) => t.name.getValue() === key) as EntityTreeNode;

        if (!entityGroupTreeNode) {

            data = {name: key, parentNode: currentEntityGroupTreeNode, path: path, id: path, type: EntityTreeNodeType.Group};
            entityGroupTreeNode = new EntityTreeNode(data);

            // apply the filter, if it is set
            if (!filterFn || (filterFn && filterFn(entityGroupTreeNode))) {
                currentEntityGroupTreeNode.children.add(entityGroupTreeNode);
            } else {
                entityGroupTreeNode = null;
            }
        }

        return entityGroupTreeNode;
    }

    /**
     * Create a directory/registry node
     *
     * @deprecated
     * @param {CatalogAbstractEntity} catalogEntity
     * @param {string} type
     * @param {string} path
     * @param {EntityTreeNode} parentNode
     * @return {EntityTreeNode}
     */
    private static createEntityTreeNode (
        catalogEntity: CatalogAbstractEntity,
        type: EntityTreeNodeType,
        path: string, parentNode: EntityTreeNode
    ): EntityTreeNode {
        let name = catalogEntity.name.getValue(),
            displayName = catalogEntity.displayName.getValue();

        return new EntityTreeNode({title: displayName, name: name, path: path, id: name, type: type, parentNode: parentNode});
    }
}
