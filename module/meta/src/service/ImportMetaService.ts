/**
 * The meta import service
 *
 * @author Brauer ilya
 * @date 2020-07-28
 */

import {UploadFile} from '@unidata/uikit';
import {BaseImportMetaOp} from './op/import_export/BaseImportMetaOp';
import {BaseImportMeasurementOp} from './op/import_export/BaseImportMeasurementOp';

export class ImportMetaService {

    public static baseImportMeta (file: UploadFile, recreate: boolean): Promise<any> {
        let op = new BaseImportMetaOp(file, recreate);

        return op.execute();
    }

    public static baseImportMeasurement (file: UploadFile, recreate: boolean): Promise<any> {
        let op = new BaseImportMeasurementOp(file, recreate);

        return op.execute();
    }
}
