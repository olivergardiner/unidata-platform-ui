/**
 * Service for data source systems
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {ReadSourceSystemListOp} from './op/sourcesystem/ReadSourceSystemListOp';
import {ReadSourceSystemOp} from './op/sourcesystem/ReadSourceSystemOp';
import {SourceSystem} from '../model/sourcesystem/SourceSystem';
import {ISourceSystem} from './op/sourcesystem/ReadSourceSystemOp';

export class SourceSystemService {
    public static getSourceSystemList (): Promise<SourceSystem[]> {
        let op = new ReadSourceSystemListOp();

        return op.execute();
    }

    public static getSourceSystem (): Promise<ISourceSystem> {
        let op = new ReadSourceSystemOp();

        return op.execute();
    }
}
