/**
 * Global enumeration service.
 *
 * @author Brauer Ilya
 * @date 2020-02-12
 */

import {Enumeration} from '../model/enumeration/Enumeration';
import {ReadEnumerationListOp} from './op/enumeration/ReadEnumerationListOp';

export class EnumerationService {
    /**
     * Retrieves the list of enumerations
     */
    public static getEnumerations (): Promise<Enumeration[]> {
        let op = new ReadEnumerationListOp();

        return op.execute();
    }
}
