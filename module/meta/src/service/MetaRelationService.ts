/**
 * Service for meta-stases
 *
 * @author Sergey Shishigin
 * @date 2019-12-10
 */
import {RelationDirectionType} from '@unidata/types';
import {Entity} from '../model';
import {ReadMetaRelationListOp} from './op/metarelation/ReadMetaRelationListOp';

export class MetaRelationService {
    /**
     * To load the registers associated with the specified
     *
     * @param direction relation direction
     * @param entityName name of the registry
     */
    public static getRelatedEntities (direction: RelationDirectionType, entityName: string): Promise<Entity[]> {
        let op = new ReadMetaRelationListOp(direction, entityName);

        return op.execute();
    }
}
