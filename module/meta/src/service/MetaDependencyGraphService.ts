/**
 * Service for working with the meta-dependency graph
 *
 * @author: Denis Makarov
 * @date: 2020-03-05
 */

import {ReadMetaDependencyGraphOp} from './op/dependency_graph/ReadMetaDependencyGraphOp';
import {MetaType} from '../type/MetaDependencyGraph';
import {MetaDependencyGraph} from '../model/dependency_graph/MetaDependencyGraph';

export class MetaDependencyGraphService {
    /**
     * get a meta dependency graph from the server
     *
     * @param for Types types of vertexes that we are building the graph for
     * @param skipTypes Types of vertices that are excluded from the issuance
     * @param is Draft draft Flag
     */
    public static getMetaDependencyGraph (forTypes: MetaType [], skipTypes: MetaType [], isDraft?: boolean): Promise<MetaDependencyGraph> {
        let op = new ReadMetaDependencyGraphOp(forTypes, skipTypes, isDraft);

        return op.execute();
    }
}
