/**
 * Metamodel service
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {EntityType} from '@unidata/types';
import {AbstractEntity} from '../model/entity/AbstractEntity';
import {Entity} from '../model/entity/Entity';
import {LookupEntity} from '../model/entity/LookupEntity';
import {ReadMetaRecordOp} from './op/metarecord/ReadMetaRecordOp';
import {IMetaEntity, ReadMetaEntityListOp} from './op/metarecord/ReadMetaEntityListOp';
import {ReadMetaRecordListOp} from './op/metarecord/ReadMetaRecordListOp';
import {ReadCustomMetaRecordListOp} from './op/metarecord/ReadCustomMetaRecordListOp';

export class MetaRecordService {
    /**
     * Get entity or lookup entity
     *
     * @param entityType
     * @param entityName
     */
    public static getMetaRecord (entityType: EntityType, entityName: string): Promise<Entity | LookupEntity> {
        let op = new ReadMetaRecordOp(entityType, entityName);

        return op.execute();
    }

    /**
     * Get entity
     *
     * @param entityName
     */
    public static getEntityMetaRecord (entityName: string): Promise<Entity> {
        return MetaRecordService.getMetaRecord(EntityType.Entity, entityName) as Promise<Entity>;
    }

    /**
     * Get lookup entity
     *
     * @param entityName
     */
    public static getLookupEntityMetaRecord (entityName: string): Promise<LookupEntity> {
        return MetaRecordService.getMetaRecord(EntityType.LookupEntity, entityName) as Promise<LookupEntity>;
    }

    public static getMetaEntityList (entityType: EntityType): Promise<IMetaEntity[]> {
        const op = new ReadMetaEntityListOp(entityType);

        return op.execute();
    }

    /**
     * Get entity or lookup entity list
     * @param entityType
     */
    public static getMetaRecordList (entityType: EntityType): Promise<AbstractEntity[]> {
        let op = new ReadMetaRecordListOp(entityType);

        return op.execute();
    }

    /**
     * Get abstract entity list by url
     * @param url
     */
    public static getCustomMetaRecordList (url: string): Promise<AbstractEntity[]> {
        let op = new ReadCustomMetaRecordListOp(url);

        return op.execute();
    }
}
