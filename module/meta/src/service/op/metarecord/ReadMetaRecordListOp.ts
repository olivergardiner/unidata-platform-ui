/**
 *  Get entity or lookup entity operation
 *
 * @author Sergey Shishigin
 * @date 2020-05-14
 */

import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp, ServerUrlManager} from '@unidata/core-app';
import {AbstractEntity} from '../../../model';
import {EntityType} from '@unidata/types';

export class ReadMetaRecordListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        rootProperty: 'content',
        model: AbstractEntity,
        method: 'get'
    };

    private entityType: EntityType;

    constructor (entityType: EntityType) {
        super();

        this.entityType = entityType;
    }

    protected buildUrl (): string {
        let url: string;

        if (this.entityType === EntityType.Entity) {
            url = '/meta/entities/';
        } else {
            url = '/meta/lookup-entities/' ;
        }

        return ServerUrlManager.buildUrl(url);
    }
}
