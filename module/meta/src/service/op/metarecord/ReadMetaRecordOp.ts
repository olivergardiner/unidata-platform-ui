/**
 * Operation for getting a metamodel
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {EntityType} from '@unidata/types';
import {AppModelHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {Entity, LookupEntity} from '../../../model';

export class ReadMetaRecordOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        method: 'get',
        model: Entity // the type of entity to create is specified in the constructor
    };

    private entityType: EntityType;
    private entityName: string;

    // TODO: add support for flags "draft" and "checkData"
    // example http://localhost:8080/unidata-backend/api/internal/meta/lookup-entities/sprav?_dc=1566481683284&draft=false&checkData=true
    constructor (entityType: EntityType, entityName: string) {
        super();

        this.entityType = entityType;
        this.entityName = entityName;

        if (entityType === EntityType.Entity) {
            this.config.model = Entity;
        } else {
            this.config.model = LookupEntity;
        }
    }

    protected buildUrl (): string {
        let url: string;

        if (this.entityType === EntityType.Entity) {
            url = '/meta/entities/' + String(this.entityName);
        } else {
            url = '/meta/lookup-entities/' + String(this.entityName);
        }

        return ServerUrlManager.buildUrl(url);
    }
}
