/**
 *  Get abstract entity list by url operation
 *
 * @author Sergey Shishigin
 * @date 2020-05-14
 */

import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp, ServerUrlManager} from '@unidata/core-app';
import {AbstractEntity} from '../../../model';

export class ReadCustomMetaRecordListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        rootProperty: 'content',
        model: AbstractEntity,
        method: 'get'
    };

    private customUrl: string;

    constructor (customUrl: string) {
        super();

        this.customUrl = customUrl;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(this.customUrl);
    }
}
