/**
 * Operation to get the list Entity or LookupEntity
 *
 * @author Ilya Brauer
 * @date 2019-11-07
 */

import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp, ServerUrlManager} from '@unidata/core-app';
import {EntityType} from '@unidata/types';
import {Entity} from '../../../model';

export interface IMetaEntity {
    customProperties: any[];
    description: string;
    displayName: string;
    hidden: boolean;
    name: string;
    readOnly: boolean;
    rights: null | any;
}

export class ReadMetaEntityListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        method: 'get',
        // url: '/internal/meta/entities',
        model: Entity // the type of entity to create is specified in the constructor
    };

    private entityType: EntityType;

    constructor (entityType: EntityType) {
        super();

        this.entityType = entityType;
    }

    protected buildUrl (): string {
        let url: string;

        if (this.entityType === EntityType.Entity) {
            url = '/meta/entities';
        } else {
            url = '/meta/lookup-entities';
        }

        return ServerUrlManager.buildUrl(url);
    }

    protected processOperationData (data: IMetaEntity[]): IMetaEntity[] {

        return data;
    }
}
