/**
 * Operation for getting a list endpoint
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {Catalog} from '../../../model';

export class ReadEntityGroupListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: 'meta/entities-group',
        method: 'get',
        model: Catalog,
        rootProperty: 'content'
    };

    constructor (filled: boolean, draft: boolean = false) {
        super();

        this.config.params = {
            filled: filled,
            draft: draft
        };
    }

    protected processOperationData (data: any): any {
        const classModel = this.config.model;

        let result: any = [];

        data.groupNodes.forEach(function (item: any) {
            result.push(new classModel(item));
        });

        return result;
    }
}
