/**
 * Base import meta model operation
 *
 * @author Brauer Ilya
 * @date 2020-07-28
 */
import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {UploadFile} from '@unidata/uikit';

export class BaseImportMetaOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/meta/model',
        method: 'post'
    };

    constructor (file: UploadFile, recreate: boolean) {
        super();

        const recreateOption = recreate === true ? 'true' : 'false';
        const data = new FormData();

        data.append('file', file as any);
        data.append('recreate', recreateOption);

        this.config.data = data;
    }
}
