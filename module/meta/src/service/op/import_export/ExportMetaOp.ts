/**
 * The meta export operation
 *
 * @author: Brauer Ilya
 * @date: 2020-07-28
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export interface IOptionsExportMeta {
    roles: boolean;
    users: boolean;
}

export class ExportMetaOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/meta/model-ie/export',
        rootProperty: '',
        successProperty: '',
        method: 'post'
    };

    constructor (options: IOptionsExportMeta) {
        super();

        this.config.data = {
            ...options
        };
    }
}
