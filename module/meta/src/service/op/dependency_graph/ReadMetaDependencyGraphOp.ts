/**
 * Operation for reading the meta-dependency graph
 *
 * @author Denis Makarov
 * @date 2019-03-06
 */
import {IModelOpConfig, ModelHttpOp} from '@unidata/core';
import {MetaType} from '../../../type/MetaDependencyGraph';
import {MetaDependencyGraph} from '../../../model/dependency_graph/MetaDependencyGraph';
import {ServerUrlManager} from '@unidata/core-app';

export class ReadMetaDependencyGraphOp extends ModelHttpOp {
    protected config: IModelOpConfig = {
        method: 'post',
        model: MetaDependencyGraph,
        headers: {
            'Content-Type': 'application/json'
        }
    };

    private readonly isDraft: boolean;

    constructor (forTypes: MetaType [], skipTypes: MetaType [], isDraft?: boolean) {
        super();

        const data: any = this.config.data = {};

        data.forTypes = forTypes || [];
        data.skipTypes = skipTypes || [];

        this.isDraft = Boolean(isDraft);
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`meta/model/dependency?draft=${Boolean(this.isDraft)}`);
    }
}
