/**
 * Operation for getting a list of enumerations
 *
 * @author Brauer Ilya
 * @date 2020-02-12
 */

import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {Enumeration} from '../../../model/enumeration/Enumeration';

export class ReadEnumerationListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/meta/enumerations?draft=false',
        method: 'get',
        model: Enumeration,
        rootProperty: ''
    };
}
