/**
 * Operation for getting a list of measurement units
 *
 * @author: Aleksandr Bavin
 * @date: 2020-04-27
 */

import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {MeasurementValue} from '../../../model/measurement/MeasurementValue';

export class ReadMeasurementValuesListOp extends AppModelListHttpOp {

    protected config: IModelOpConfig = {
        url: '/meta/measurement-values?draft=false',
        method: 'get',
        model: MeasurementValue
    };

}
