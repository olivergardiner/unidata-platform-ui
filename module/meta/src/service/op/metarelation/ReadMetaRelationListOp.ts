/**
 * The operation of obtaining the metamodels associated with this
 *
 * @author Sergey Shishigin
 * @date 2019-12-10
 */

import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {RelationDirectionType} from '@unidata/types';
import {Entity} from '../../../model';

export class ReadMetaRelationListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/meta/relations/{{direction}}/{{entityName}}',
        rootProperty: 'content',
        model: Entity,
        method: 'get'
    };

    constructor (direction: RelationDirectionType, entityName: string) {
        super();

        this.urlContext = {
            direction: direction,
            entityName: entityName
        };
    }
}
