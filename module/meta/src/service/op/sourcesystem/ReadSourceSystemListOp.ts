/**
 * Operation for getting a list of data source systems
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {SourceSystem} from '../../../model/sourcesystem/SourceSystem';
import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp} from '@unidata/core-app';

export class ReadSourceSystemListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: 'meta/source-systems',
        method: 'get',
        model: SourceSystem,
        rootProperty: '',
        successProperty: ''
    };

    protected processOperationData (data: any): any {
        const classModel = this.config.model;

        let result: any = [];

        data.sourceSystem.forEach(function (item: any) {
            result.push(new classModel(item));
        });

        return result;
    }
}
