/**
 * Operation for getting data source systems including adminSystemName
 *
 * @author Brauer Ilya
 * @date 2020-03-12
 */

import {SourceSystem} from '../../../model/sourcesystem/SourceSystem';
import {IModelOpConfig} from '@unidata/core';
import {AppHttpOp} from '@unidata/core-app';

export interface ISourceSystem {
    adminSystemName: string;
    sourceSystem: SourceSystem[];
}

export class ReadSourceSystemOp extends AppHttpOp {
    protected config: IModelOpConfig = {
        url: 'meta/source-systems',
        method: 'get',
        model: SourceSystem,
        rootProperty: '',
        successProperty: ''
    };

    protected processOperationData (data: any): ISourceSystem {
        const classModel = this.config.model;
        const sourceSystem: any = [];

        data.sourceSystem.forEach(function (item: any) {
            sourceSystem.push(new classModel(item));
        });

        return {
            adminSystemName: data.adminSystemName,
            sourceSystem
        };
    }
}
