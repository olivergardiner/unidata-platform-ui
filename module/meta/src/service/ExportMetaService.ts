/**
 * The meta export service
 *
 * @author Brauer ilya
 * @date 2020-07-28
 */

import {ExportMetaOp, IOptionsExportMeta} from './op/import_export/ExportMetaOp';

export class ExportMetaService {

    public static exportMeta (options: IOptionsExportMeta): Promise<void> {
        let op = new ExportMetaOp(options);

        return op.execute();
    }

}
