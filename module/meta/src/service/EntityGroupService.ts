/**
 * Service for groups of registers / directories.
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {ReadEntityGroupListOp} from './op/entitygroup/ReadEntityGroupListOp';
import {Catalog} from '../model';

export class EntityGroupService {
    /**
     * Gets the structure of the registry / reference directory
     */
    public static getEntityGroup (filled: boolean = true, draft: boolean = false): Promise<Catalog[]> {
        let op = new ReadEntityGroupListOp(filled, draft);

        return op.execute();
    }
}
