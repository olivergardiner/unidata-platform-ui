/**
 * Service of units of measurement
 *
 * @author: Aleksandr Bavin
 * @date: 2020-04-27
 */

import {MeasurementValue} from '../model/measurement/MeasurementValue';
import {ReadMeasurementValuesListOp} from './op/measurement/ReadMeasurementValuesListOp';

export class MeasurementValuesService {

    public static getMeasurementValues (): Promise<MeasurementValue[]> {
        let op = new ReadMeasurementValuesListOp();

        return op.execute();
    }

}
