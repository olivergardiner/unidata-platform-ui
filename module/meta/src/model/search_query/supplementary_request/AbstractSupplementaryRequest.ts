/**
 * Part of a search query of the supplemental Request type. Used as an inheritance class
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractFormField, IFormField} from '../form_field/AbstractFormField';

export enum SupplementaryDataType {
    ETALON_REL = 'ETALON_REL',
    CLASSIFIER = 'CLASSIFIER'
}

export interface ISupplementaryRequest {
    dataType: SupplementaryDataType;
    entity: string;
    returnFields?: string[];
    formFields: IFormField[];
}

export type IAbstractSupplementaryRequest = Omit<AbstractSupplementaryRequest, 'addFormField' | 'clearFormFields' | 'toJson'>;

export class AbstractSupplementaryRequest {
    public readonly dataType: SupplementaryDataType;
    public readonly entity: string;
    public returnFields?: string[];
    public formFields: AbstractFormField[];

    constructor (data: IAbstractSupplementaryRequest) {
        this.dataType = data.dataType;
        this.entity = data.entity;
        this.returnFields = data.returnFields;
        this.formFields = [];
    }

    public addFormField (formField: AbstractFormField | AbstractFormField[]) {
        if (Array.isArray(formField)) {
            this.formFields.push(...formField);
        } else {

            this.formFields.push(formField);
        }
    }

    public clearFormFields () {
        this.formFields = [];
    }

    public toJson (): ISupplementaryRequest {
        return {
            dataType: this.dataType,
            entity: this.entity,
            returnFields: this.returnFields,
            formFields: this.formFields.map((formField) => formField.toJson())
        };
    }
}
