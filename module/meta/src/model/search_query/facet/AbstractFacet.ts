/**
 * Part of a Facet search query
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

export enum FACETS {
    UN_RANGED = 'un_ranged',
    PENDING_ONLY = 'pending_only',
    ERRORS_ONLY = 'errors_only',
    NO_ERRORS = 'no_errors',
    INACTIVE_ONLY = 'inactive_only',
    ACTIVE_ONLY = 'active_only',
    INCLUDE_ACTIVE_PERIODS = 'include_active_periods',
    INCLUDE_INACTIVE_PERIODS = 'include_inactive_periods',
    OPERATION_TYPE_CASCADED = 'operation_type_cascaded',
    OPERATION_TYPE_DIRECT = 'operation_type_direct',
    OPERATION_TYPE_COPY = 'operation_type_copy'
}

export class AbstractFacet {

    private facetList: FACETS[] = [];

    constructor (data: FACETS | FACETS[]) {
        if (Array.isArray(data)) {
            this.facetList = data;
        } else {
            this.facetList = [data];
        }
    }

    public addFacet (data: FACETS | FACETS[]) {
        if (Array.isArray(data)) {
            this.facetList.push(...data);
        } else {
            this.facetList.push(data);
        }
    }

    public toJson () {
        return this.facetList;
    }

}
