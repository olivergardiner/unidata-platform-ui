/**
 * Part of the Time type FormField search query
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractFormField, SEARCH_TYPE} from './AbstractFormField';
import {IAbstractFormField} from './AbstractFormField';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {observable} from 'mobx';

export type ITimeFormField = IAbstractFormField & {
    value?: string | null;
    range?: [string|null, string|null];
}

export class TimeFormField extends AbstractFormField {
    public type = SIMPLE_DATA_TYPE.TIME;

    @observable.ref
    public value?: string | null;

    @observable.ref
    public range?: [string|null, string|null];

    public toJson () {
        const formField = super.getCommonFormField();

        if (this.searchType === SEARCH_TYPE.EXIST) {
            return {
                ...formField,
                value: null
            };
        }

        if (this.range !== undefined) {
            return {
                ...formField,
                range: this.range
            };
        }

        return {
            ...formField,
            value: this.value
        };
    }

    public getValue () {
        if (this.range !== undefined) {
            return this.range;
        }

        return this.value;
    }
}
