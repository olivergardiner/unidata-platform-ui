/**
 * Part of the form Field search query of the String type
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractFormField, SEARCH_TYPE} from './AbstractFormField';
import {IAbstractFormField} from './AbstractFormField';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {observable} from 'mobx';

export type IStringFormField = IAbstractFormField & {
    value: string | null;
}

export class StringFormField extends AbstractFormField {
    public type = SIMPLE_DATA_TYPE.STRING;

    @observable.ref
    public value: string | null;

    public toJson () {
        return {
            ...super.getCommonFormField(),
            value: this.searchType === SEARCH_TYPE.EXIST ? null : this.value
        };
    }

    public getValue () {
        return this.value;
    }
}
