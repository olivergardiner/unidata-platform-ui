/**
 * Part of the Enum type FormField search query
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractFormField, SEARCH_TYPE} from './AbstractFormField';
import {IAbstractFormField} from './AbstractFormField';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {observable} from 'mobx';
import {IOption} from '@unidata/uikit';

export type IEnumFormField = IAbstractFormField;

export class EnumFormField extends AbstractFormField {
    public type = SIMPLE_DATA_TYPE.STRING;

    @observable.ref
    public value: string | null;

    public toJson () {
        return {
            ...super.getCommonFormField(),
            value: this.searchType === SEARCH_TYPE.EXIST ? null : this.value
        };
    }

    public getValue (options: IOption[]): IOption | undefined {
        return options.find((option) => {
            return this.value !== null && option.value === this.value;
        });
    }
}
