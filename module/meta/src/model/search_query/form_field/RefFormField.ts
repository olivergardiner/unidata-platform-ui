/**
 * Part of the FormField search query of the Ref type (reference to the reference list)
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractFormField, SEARCH_TYPE} from './AbstractFormField';
import {IAbstractFormField} from './AbstractFormField';
import {observable} from 'mobx';
import {IOption} from '@unidata/uikit';

export type IRefFormField = IAbstractFormField & {
    value: string | number | null;
}

export class RefFormField extends AbstractFormField {

    @observable.ref
    public value: string | number | null;

    public toJson () {
        return {
            ...super.getCommonFormField(),
            value: this.searchType === SEARCH_TYPE.EXIST ? null : this.value
        };
    }

    public getValue (options: IOption[]): IOption | undefined {
        return options.find((option) => {
            return this.value !== null && option.value === this.value;
        });
    }
}
