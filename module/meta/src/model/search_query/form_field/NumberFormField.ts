/**
 * Part of the number type FormField search query
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractFormField, SEARCH_TYPE} from './AbstractFormField';
import {IAbstractFormField} from './AbstractFormField';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {observable} from 'mobx';

export type INumberFormField = IAbstractFormField & {
    value?: number | null;
    range?: [number|null, number|null];
}

export class NumberFormField extends AbstractFormField {
    public type = SIMPLE_DATA_TYPE.NUMBER;

    @observable.ref
    public value?: number | null;

    @observable.ref
    public range?: [number|null, number|null];

    public toJson () {
        const formField = super.getCommonFormField();

        if (this.searchType === SEARCH_TYPE.EXIST) {
            return {
                ...formField,
                value: null
            };
        }

        if (this.range !== undefined) {
            return {
                ...formField,
                range: this.range
            };
        }

        return {
            ...super.getCommonFormField(),
            value: this.value
        };
    }

    public getValue () {
        if (this.range !== undefined) {
            return this.range;
        }

        return this.value;
    }
}
