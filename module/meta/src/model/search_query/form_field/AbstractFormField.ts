/**
 * Part of a Form Field search query. Used as a class for inheriting specific types of search fields
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {action, computed, observable} from 'mobx';
import {IOption} from '@unidata/uikit';
import {ARRAY_DATA_TYPE, CODE_DATA_TYPE, SIMPLE_DATA_TYPE} from '@unidata/types';

export type Inverted = 'true' | 'false';

export type ConcatType = 'AND' | 'OR';

export type FormFieldType = SIMPLE_DATA_TYPE | ARRAY_DATA_TYPE | CODE_DATA_TYPE;

export type AbstractFormFieldValue = string | null | number | boolean |
    IOption[] | IOption | [number | null, number | null] | [string | null, string | null] | string[];

export enum SEARCH_TYPE {
    EXACT = 'EXACT',
    FUZZY = 'FUZZY',
    MORPHOLOGICAL = 'MORPHOLOGICAL',
    EXIST = 'EXIST',
    START_WITH = 'START_WITH',
    LIKE = 'LIKE',
    RANGE = 'RANGE'
}

type IBaseFormField = {
    name: string;
    type?: FormFieldType;
    searchType?: SEARCH_TYPE;
    inverted?: Inverted;
    concatType?: ConcatType;
};

type RangeValue = [string|number|null, string|number|null] | null;
type Value = string | null | boolean | number;

export type IAbstractFormField = IBaseFormField & ({
    value?: Value;
} | {
    range?: RangeValue;
})

export type IFormField = {
    name: string;
    type: FormFieldType;
    searchType: SEARCH_TYPE;
    inverted: boolean;
    concatType?: ConcatType;
    value?: string | null | boolean | number;
    range?: [string|number|null, string|number|null] | null;
}

export class AbstractFormField {
    public readonly name: string;
    public readonly type: FormFieldType;

    @observable
    public searchType: SEARCH_TYPE;
    @observable
    public inverted: Inverted;
    @observable
    public concatType: ConcatType;

    public value?: Value;
    public range?: RangeValue;

    constructor (data: IAbstractFormField) {
        this.name = data.name;

        if (data.type !== undefined) {
            this.type = data.type;
        }

        this.change(data);
    }

    @action
    public change (data: IAbstractFormField) {
        this.searchType = data.searchType || SEARCH_TYPE.EXACT;
        this.inverted = data.inverted || 'false';
        this.concatType = data.concatType || 'OR';

        if ('value' in data) {
            this.value = data.value;
        }

        if ('range' in data) {
            this.range = data.range;
        }
    }

    public getValue (options?: IOption[]): AbstractFormFieldValue | undefined {
        throw new Error('You must implement "getValue" method');
    }

    protected getCommonFormField () {
        return {
            name: this.name,
            type: this.type,
            searchType: this.searchType,
            inverted: this.inverted === 'true' ? true : false,
            concatType: this.concatType
        };
    }

    public toJson (): IFormField {
        throw new Error('You must implement "getFormField" method');
    }
}
