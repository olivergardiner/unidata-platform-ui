/**
 * Part of the Timestamp type FormField search query
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractFormField, SEARCH_TYPE} from './AbstractFormField';
import {IAbstractFormField} from './AbstractFormField';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {observable} from 'mobx';

export type ITimestampFormField = IAbstractFormField & {
    range: [string | null, string | null] | null;
}

export class TimestampFormField extends AbstractFormField {
    public type = SIMPLE_DATA_TYPE.TIMESTAMP;

    @observable.ref
    public range: [string | null, string | null] | null;

    public toJson () {
        const formField = super.getCommonFormField();

        if (this.searchType === SEARCH_TYPE.EXIST || this.range === null) {
            return {
                ...formField,
                range: null
            };
        }

        return {
            ...formField,
            range: this.range
        };
    }

    public getValue () {
        return this.range;
    }
}
