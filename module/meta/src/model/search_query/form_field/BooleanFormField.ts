/**
 * Part of the Boolean Form Field search query
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractFormField, SEARCH_TYPE} from './AbstractFormField';
import {IAbstractFormField} from './AbstractFormField';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {action, observable} from 'mobx';
import {IOption} from '@unidata/uikit';

export type IBooleanFormField = IAbstractFormField & {
    value: boolean | null;
}

export class BooleanFormField extends AbstractFormField {
    public type = SIMPLE_DATA_TYPE.BOOLEAN;

    @observable.ref
    public value: boolean | null;

    public toJson () {
        return {
            ...super.getCommonFormField(),
            value: this.searchType === SEARCH_TYPE.EXIST ? null : this.value
        };
    }

    @action
    public change (data: IAbstractFormField) {
        super.change(data);

        if ('value' in data) {
            this.value = data.value === 'true' ? true : false;
        }
    }

    public getValue (options: IOption[]): IOption | undefined {
        return options.find((option) => {
            if (this.value === true) {
                return option.value === 'true';
            } else if (this.value === false) {
                return option.value === 'false';
            }

            return false;
        });
    }

    // private getBooleanValue () {
    //     switch (this.value) {
    //         case null:
    //             return null;
    //         case 'true':
    //             return true;
    //         case 'false':
    //             return false;
    //         default:
    //             throw new Error('Unexpected value');
    //     }
    // }
}
