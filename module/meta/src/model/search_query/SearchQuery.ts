/**
 * The main class for generating search queries. Allows you to add parts of the query and generates an I Search Query for them
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractFormField, IFormField} from './form_field/AbstractFormField';
import {FACETS} from './facet/AbstractFacet';
import {
    AbstractSupplementaryRequest,
    ISupplementaryRequest
} from './supplementary_request/AbstractSupplementaryRequest';
import {AbstractFacet} from './facet/AbstractFacet';
import moment from 'moment';
import {QueryPart} from '../search_term/AbstractSearchTerm';
import {AbstractFormGroup, IFormGroup} from './form_group/AbstractFormGroup';
import {ARRAY_DATA_TYPE, CODE_DATA_TYPE, RelType, SIMPLE_DATA_TYPE} from '@unidata/types';
import {AliasCodeAttribute, ArrayAttribute, CodeAttribute, SimpleAttribute} from '..';

export interface IDisplayAttr {
    name: string;
    displayName: string;
    order?: number;
    description: string;
    isShow: boolean;
    isSearchable: boolean;
    children?: IDisplayAttr[];
    isRelation: boolean;
    isArray: boolean;
    relType?: RelType;
    lookupEntityType: string;
    metaAttribute?: SimpleAttribute | CodeAttribute | AliasCodeAttribute | ArrayAttribute;
    valueType?: SIMPLE_DATA_TYPE | ARRAY_DATA_TYPE | CODE_DATA_TYPE;
    enumName?: string;
}

export type ISearchQuery = {
    asOf?: string;
    formFields?: IFormField[];
    formGroups?: IFormGroup[];
    countOnly: boolean;
    fetchAll: boolean;
    facets?: FACETS[];
    text?: string;
    qtype?: 'FUZZY';
    supplementaryRequests?: ISupplementaryRequest[];
    entity: string;
    returnFields: string[];
    searchFields: string[];
}

export const systemVariables = {
    $etalonId: '$etalon_id',
    $from: '$from',
    $to: '$to',
    $direct: '$direct',
    $relName: '$rel_name',
    $etalonIdTo: '$etalon_id_to'
};

export class SearchQuery {

    private asOf: string = moment().toISOString();
    private facets: AbstractFacet[] = [];
    private formFields: AbstractFormField[] = [];
    private formGroups: AbstractFormGroup[] = [];
    private supplementaryRequests: AbstractSupplementaryRequest[] = [];
    private text?: string;
    private qtype?: 'FUZZY';

    private readonly entity?: string;
    private readonly returnFields?: string[];
    private readonly searchFields?: string[];

    constructor (data?: {entity?: string; attributes?: IDisplayAttr[]}) {
        if (data) {
            this.entity = data.entity;
            this.searchFields = [
                systemVariables.$etalonId,
                systemVariables.$from,
                systemVariables.$to
            ].concat((data.attributes || []).map((item) => item.name));
            this.returnFields = [
                systemVariables.$etalonId,
                systemVariables.$from,
                systemVariables.$to
            ].concat((data.attributes || []).reduce<string[]>((result, item) => {
                if (item.children) {
                    item.children.forEach((childItem) => {
                        result.push(childItem.name);
                    });
                }

                result.push(item.name);

                return result;
            }, []));
        }
    }

    public setAsOf (queryPart: QueryPart) {
        if (queryPart.formFields && queryPart.formFields.length > 0) {

            const to = queryPart.formFields[0].toJson();
            const from = queryPart.formFields[1] ? queryPart.formFields[1].toJson() : null;

            if (to && from && to.range && from.range && to.range[0] === from.range[1]) {
                this.asOf = to.range[0] as string;
                this.add({facet: queryPart.facet});
            } else {
                this.asOf = '';
                this.add(queryPart);
            }
        } else {
            throw new Error('Error in active SearchTerm');
        }
    }

    public add (item: QueryPart) {
        if (item.formFields) {
            item.formFields.forEach((formField) => {
                this.formFields.push(formField);
            });
        }

        if (item.formGroups) {
            item.formGroups.forEach((formGroup) => {
                this.formGroups.push(formGroup);
            });
        }

        if (item.facet) {
            this.facets.push(item.facet);
        }

        if (item.supplementaryRequest) {
            this.supplementaryRequests.push(item.supplementaryRequest);
        }
    }

    public setKeyword (keyword: string) {
        if (keyword.length > 0) {
            this.text = keyword;
            this.qtype = 'FUZZY';
        } else {
            this.text = undefined;
            this.qtype = undefined;
        }
    }

    public getSearchQuery (): ISearchQuery {
        const facets = this.facets.reduce<FACETS[]>((result, facet) => {
            result.push(...facet.toJson());

            return result;
        }, []);

        const formFields = this.formFields.map((formField) => {
            return formField.toJson();
        });

        const formGroups = this.formGroups.map((formGroup) => {
            return formGroup.toJson();
        });

        const supplementaryRequests = this.supplementaryRequests.map((supReq) => {
            return supReq.toJson();
        });

        return {
            asOf: (this.asOf === '') ? undefined : this.asOf,
            fetchAll: formFields.length === 0 && this.qtype === undefined ? true : false,
            countOnly: false,
            facets: facets.length > 0 ? facets : undefined,
            formFields: formFields,
            formGroups: formGroups,
            supplementaryRequests: supplementaryRequests.length === 0 ? undefined : supplementaryRequests,
            entity: this.entity || '',
            returnFields: this.returnFields || [],
            searchFields: this.searchFields || [],
            text: this.text,
            qtype: this.qtype
        };
    }

}
