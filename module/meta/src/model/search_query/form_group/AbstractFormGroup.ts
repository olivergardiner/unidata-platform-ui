/**
 * Part of a Form Group search query.
 *
 * @author Brauer Ilya
 * @date 2020-07-27
 */

import {AbstractFormField, IFormField} from '../../../index';

export type IFormGroup = {
    childGroups: IFormGroup[];
    formFields: IFormField[];
    groupType: 'AND'; // todo Brauer Ilya: for now, we don't know any another case
}

export class AbstractFormGroup {
    public readonly groupType = 'AND';
    public formFields: AbstractFormField[] = [];

    public addFormField (formField: AbstractFormField | AbstractFormField[]) {
        if (Array.isArray(formField)) {
            this.formFields.push(...formField);
        } else {

            this.formFields.push(formField);
        }
    }

    public clearFormFields () {
        this.formFields = [];
    }

    public toJson (): IFormGroup {
        return {
            childGroups: [],
            formFields: this.formFields.map((formField) => formField.toJson()),
            groupType: this.groupType
        };
    }
}
