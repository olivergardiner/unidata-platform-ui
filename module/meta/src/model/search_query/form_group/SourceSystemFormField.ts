/**
 * Form Group search query for SourceSystem request.
 *
 * @author Brauer Ilya
 * @date 2020-07-27
 */

import {AbstractFormGroup, IFormGroup} from './AbstractFormGroup';

export class SourceSystemFormField extends AbstractFormGroup {
    private fieldName = '$external_keys'; // name for every formField in query part

    public toJson (): IFormGroup {
        return {
            childGroups: [],
            formFields: this.formFields.map((formField) => {
                return {
                    ...formField.toJson(),
                    name: this.fieldName,
                    value: `${formField.name}:${formField.getValue()}`
                };
            }),
            groupType: this.groupType
        };
    }
}
