/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-29
 */

import {AbstractAttribute} from './AbstractAttribute';
import {
    hasOne,
    IAssociationDescriptorMap,
    IntegerField,
    integerField,
    ModelMetaData,
    Nullable,
    StringField,
    stringField
} from '@unidata/core';
import {observable} from 'mobx';
import {NestedEntity} from '../entity/NestedEntity';

export class ComplexAttribute extends AbstractAttribute {

    @observable
    @stringField()
    public nestedEntityType: StringField;

    @observable
    @integerField({defaultValue: 0})
    public minCount: IntegerField;

    @observable
    @integerField({allowNull: true})
    public maxCount: IntegerField;

    @observable
    @stringField()
    public subEntityKeyAttribute: StringField;

    @observable
    @hasOne()
    public nestedEntity: Nullable<NestedEntity>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap  = {
            hasOne: {
                nestedEntity: NestedEntity
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
