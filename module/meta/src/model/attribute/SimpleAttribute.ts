/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-29
 */

import {AbstractSimpleAttribute} from './AbstractSimpleAttribute';
import {computed, observable} from 'mobx';
import {
    arrayField,
    ArrayField,
    booleanField,
    BooleanField,
    DictionaryField,
    StringField,
    stringField
} from '@unidata/core';
import {AttributeTypeCategory, SIMPLE_DATA_TYPE} from '@unidata/types';

export class SimpleAttribute extends AbstractSimpleAttribute {

    @observable
    @booleanField({defaultValue: false})
    public nullable: BooleanField;

    @observable
    @booleanField({defaultValue: true})
    public unique: BooleanField;

    @observable
    @stringField()
    public simpleDataType: StringField;

    @observable
    @stringField()
    public enumDataType: StringField;

    @observable
    @stringField()
    public lookupEntityType: StringField;

    @observable
    @stringField()
    public linkDataType: StringField;

    @observable
    @stringField()
    public lookupEntityCodeAttributeType: StringField;

    @observable
    @stringField({allowNull: true})
    public valueId: StringField;

    @observable
    @stringField({allowNull: true})
    public defaultUnitId: StringField;

    @observable
    @stringField()
    public dictionaryDataType: DictionaryField;

    @observable
    @arrayField({allowNull: true, defaultValue: []})
    public lookupEntityDisplayAttributes: ArrayField;

    @observable
    @arrayField({allowNull: true, defaultValue: []})
    public lookupEntitySearchAttributes: ArrayField;

    @observable
    @booleanField({defaultValue: false})
    public useAttributeNameForDisplay: BooleanField;

    @observable
    @booleanField({defaultValue: false})
    public searchMorphologically: BooleanField;

    @observable
    @booleanField({defaultValue: false})
    public displayable: BooleanField;

    @observable
    @booleanField({defaultValue: false})
    public mainDisplayable: BooleanField;

    @observable
    @booleanField({defaultValue: false})
    public searchable: BooleanField;

    @computed
    public get typeValue (): SIMPLE_DATA_TYPE {
        let typeCategory = this.typeCategory;

        if (!typeCategory || !(this as any)[typeCategory]) {
            throw `can't find field by typeCategory = ${typeCategory}`;
        }

        if (typeCategory === AttributeTypeCategory.dictionaryDataType) {
            return SIMPLE_DATA_TYPE.STRING;
        }

        return (this as any)[typeCategory].getValue();
    }

    @computed
    public get typeCategory (): AttributeTypeCategory {
        if (Boolean(this.enumDataType.getValue())) {
            return AttributeTypeCategory.enumDataType;
        } else if (Boolean(this.lookupEntityType.getValue())) {
            return AttributeTypeCategory.lookupEntityType;
        } else if (Boolean(this.linkDataType.getValue())) {
            return AttributeTypeCategory.linkDataType;
        } else if (Boolean(this.dictionaryDataType.getValue())) {
            return AttributeTypeCategory.dictionaryDataType;
        }

        return AttributeTypeCategory.simpleDataType;
    }
}
