/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-29
 */

import {AbstractSimpleAttribute} from './AbstractSimpleAttribute';
import {computed, observable} from 'mobx';
import {BooleanField, booleanField, stringField, StringField} from '@unidata/core';
import {AttributeTypeCategory, CODE_DATA_TYPE} from '@unidata/types';

export class CodeAttribute extends AbstractSimpleAttribute {

    @observable
    @booleanField({defaultValue: false})
    public nullable: BooleanField;

    @observable
    @booleanField({defaultValue: true})
    public unique: BooleanField;

    @observable
    @booleanField({defaultValue: true})
    public displayable: BooleanField;

    @observable
    @booleanField({defaultValue: false})
    public mainDisplayable: BooleanField;

    @observable
    @booleanField({defaultValue: true})
    public searchable: BooleanField;

    @observable
    @stringField()
    public simpleDataType: StringField;

    @computed
    public get typeValue (): CODE_DATA_TYPE {
        let typeCategory = this.typeCategory;

        if (!typeCategory || !(this as any)[typeCategory]) {
            throw `can't find field by typeCategory = ${typeCategory}`;
        }

        return (this as any)[typeCategory].getValue();
    }

    @computed
    public get typeCategory (): AttributeTypeCategory {
        return AttributeTypeCategory.simpleDataType;
    }

}
