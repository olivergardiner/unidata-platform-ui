/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-29
 */

import {AbstractAttribute} from './AbstractAttribute';
import {stringField, StringField} from '@unidata/core';
import {computed, observable} from 'mobx';
import {AttributeTypeCategory} from '@unidata/types';

export class AbstractSimpleAttribute extends AbstractAttribute {

    @observable
    @stringField()
    public mask: StringField;

    @computed
    public get typeCategory (): AttributeTypeCategory {
        return AttributeTypeCategory.simpleDataType;
    }
}
