/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-29
 */

import {computed, observable} from 'mobx';
import {
    ArrayField,
    arrayField,
    BooleanField,
    booleanField,
    DictionaryField,
    stringField,
    StringField
} from '@unidata/core';
import {AbstractAttribute} from './AbstractAttribute';
import {ARRAY_DATA_TYPE, AttributeTypeCategory} from '@unidata/types';

export class ArrayAttribute extends AbstractAttribute {

    @observable
    @booleanField({defaultValue: false})
    public searchable: BooleanField;

    @observable
    @booleanField({defaultValue: false})
    public displayable: BooleanField;

    @observable
    @stringField()
    public lookupEntityType: StringField;

    @observable
    @stringField()
    public lookupEntityCodeAttributeType: StringField;

    @observable
    @arrayField({allowNull: true, defaultValue: []})
    public lookupEntityDisplayAttributes: ArrayField;

    @observable
    @arrayField({allowNull: true, defaultValue: []})
    public lookupEntitySearchAttributes: ArrayField;

    @observable
    @stringField()
    public dictionaryDataType: DictionaryField;

    @observable
    @stringField()
    public arrayDataType: StringField;

    @computed
    public get typeValue (): ARRAY_DATA_TYPE {
        let typeCategory = this.typeCategory;

        if (!typeCategory || !(this as any)[typeCategory]) {
            throw `can't find field by typeCategory = ${typeCategory}`;
        }

        if (typeCategory === AttributeTypeCategory.dictionaryDataType) {
            return ARRAY_DATA_TYPE.STRING;
        }

        return (this as any)[typeCategory].getValue();
    }

    @computed
    public get typeCategory (): AttributeTypeCategory {
        if (Boolean(this.lookupEntityType.getValue())) {
            return AttributeTypeCategory.lookupEntityType;
        } else if (Boolean(this.dictionaryDataType.getValue())) {
            return AttributeTypeCategory.dictionaryDataType;
        }

        return AttributeTypeCategory.arrayDataType;
    }
}
