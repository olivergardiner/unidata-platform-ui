/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-29
 */

import {
    AbstractModel,
    booleanField,
    BooleanField,
    integerField,
    IntegerField,
    stringField,
    StringField
} from '@unidata/core';
import {observable} from 'mobx';

export class AbstractAttribute extends AbstractModel {

    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

    @observable
    @stringField()
    public description: StringField;

    @observable
    @booleanField()
    public readOnly: BooleanField;

    @observable
    @booleanField()
    public hidden: BooleanField;

    @observable
    @integerField({required: true})
    public order: IntegerField;
}
