/**
 * Model MetaDependencyGraph
 *
 * @author: Denis Makarov
 * @date: 2020-03-05
 */

import {observable} from 'mobx';
import {Vertex} from './Vertex';
import {Edge} from './Edge';
import {AbstractModel, hasMany, IAssociationDescriptorMap, ModelCollection, ModelMetaData} from '@unidata/core';

export class MetaDependencyGraph extends AbstractModel {
    @observable
    @hasMany({ofType: Edge})
    public edges: ModelCollection<Edge>;

    @observable
    @hasMany({ofType: Vertex})
    public vertexes: ModelCollection<Vertex>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                vertexes: Vertex,
                edges: Edge

            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
