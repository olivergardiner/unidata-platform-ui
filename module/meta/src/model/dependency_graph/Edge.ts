/**
 * Model MetaDependencyGraph
 *
 * @author: Denis Makarov
 * @date: 2020-03-05
 */

import {observable} from 'mobx';
import {Vertex} from './Vertex';
import {AbstractModel, hasOne, IAssociationDescriptorMap, ModelMetaData, StringField, stringField} from '@unidata/core';

export class Edge extends AbstractModel {
    @observable
    @stringField({})
    public existence: StringField;

    @observable
    @hasOne({})
    public from: Vertex;

    @observable
    @hasOne({})
    public to: Vertex;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasOne: {
                from: Vertex,
                to: Vertex

            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
