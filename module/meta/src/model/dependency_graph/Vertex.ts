/**
 * Model MetaDependencyGraph
 *
 * @author: Denis Makarov
 * @date: 2020-03-05
 */

import {observable} from 'mobx';
import {
    AbstractModel,
    anyField,
    AnyField,
    hasMany,
    IAssociationDescriptorMap,
    ModelCollection, ModelMetaData,
    stringField,
    StringField
} from '@unidata/core';

export class Vertex extends AbstractModel {
    @observable
    @stringField({})
    public id: StringField;

    @observable
    @stringField({})
    public action: StringField;

    @observable
    @stringField({})
    public displayName: StringField;

    @observable
    @stringField({})
    public statuses: AnyField;

    @observable
    @stringField({})
    public existence: StringField;

    @observable
    @stringField({})
    public type: StringField;

    @observable
    @anyField({})
    public customProps: AnyField;

    @observable
    @hasMany({})
    public parent: ModelCollection<Vertex>;

    @observable
    @hasMany({})
    public child: ModelCollection<Vertex>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                parent: Vertex,
                child: Vertex
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public getCustomPropValue (key: string): string | null {
        let customProps: any = this.customProps.getValue(),
            map,
            value = null;

        if (!customProps.length) {
            return value;
        }

        map = (customProps as any []).reduce(function (map, obj) {
            map[obj.key] = obj.value;

            return map;
        }, {});

        return map[key] || null;
    }


}
