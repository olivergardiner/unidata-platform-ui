/**
 * Enumeration model
 *
 * @author Brauer Ilya
 * @date 2020-02-12
 */

import {AbstractModel, hasMany, IAssociationDescriptorMap, ModelCollection, ModelMetaData, StringField, stringField} from '@unidata/core';
import {EnumerationValue} from './EnumerationValue';

export class Enumeration extends AbstractModel {
    @hasMany()
    public values: ModelCollection<EnumerationValue>;

    @stringField()
    public name: StringField;

    @stringField()
    public displayName: StringField;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                values: EnumerationValue
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
