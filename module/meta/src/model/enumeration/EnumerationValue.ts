/**
 * Unit of enumeration model
 *
 * @author Brauer Ilya
 * @date 2020-02-12
 */

import {AbstractModel, StringField, stringField} from '@unidata/core';

export class EnumerationValue extends AbstractModel {
    @stringField()
    public name: StringField;

    @stringField()
    public displayName: StringField;
}
