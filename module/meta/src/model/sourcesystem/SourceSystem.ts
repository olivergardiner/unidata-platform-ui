/**
 * Data source model
 *
 * @author: Denis Makarov
 * @date: 2019-02-17
 */

import {
    AbstractModel,
    hasMany,
    IAssociationDescriptorMap,
    IntegerField,
    integerField,
    ModelCollection,
    ModelMetaData,
    StringField,
    stringField
} from '@unidata/core';
import {KeyValuePair} from '../../type/KeyValuePair';

export class SourceSystem extends AbstractModel {
    @stringField()
    public name: StringField;

    @stringField()
    public description: StringField;

    @integerField({defaultValue: 0})
    public weight: IntegerField;

    @hasMany()
    public customProperties: ModelCollection<KeyValuePair>;

    public initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                customProperties: KeyValuePair
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
