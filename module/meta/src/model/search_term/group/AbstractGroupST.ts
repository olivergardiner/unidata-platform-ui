/**
 * Abstract search term of the group type
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractSearchTerm, IAbstractSearchTerm} from '../index';
import {action, computed, observable} from 'mobx';

export type IAbstractGroupST = IAbstractSearchTerm & {
    attributes: AbstractSearchTerm[];
    entity: string;
}

export class AbstractGroupST extends AbstractSearchTerm {
    @observable.shallow
    protected readonly items: AbstractSearchTerm[] = [];

    public readonly entity: string;
    public readonly attributes: AbstractSearchTerm[];

    constructor (data: IAbstractGroupST) {
        super(data);

        this.entity = data.entity;
        this.attributes = data.attributes;
    }

    @action
    public addItem (item: AbstractSearchTerm) {
        this.items.push(item);
    }

    @action
    public deleteItem (index: number) {
        this.items.splice(index, 1);
    }

    @computed
    public get itemsData () {
        return this.items;
    }
}
