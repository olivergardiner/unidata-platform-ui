/**
 * Search term of the group type. Communication
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractAttributeST} from '../index';
import {BooleanFormField} from '../../search_query/form_field/BooleanFormField';
import {StringFormField} from '../../search_query/form_field/StringFormField';
import {AbstractGroupST, IAbstractGroupST} from './AbstractGroupST';
import {
    AbstractSupplementaryRequest,
    SupplementaryDataType
} from '../../search_query/supplementary_request/AbstractSupplementaryRequest';
import {action, observable} from 'mobx';
import {systemVariables} from '../../search_query';
import {JsonData} from '../AbstractSearchTerm';

type IRelationST = IAbstractGroupST & {
    initDirect: string;
    initRelName: string;
    entity: string;
    relationIds?: string[];
}

export class RelationST extends AbstractGroupST {
    public readonly entity: string;
    private readonly initDirect: string;
    private readonly initRelName: string;

    @observable.ref
    public relationIds: string[] = [];
    // private relationFilers: any[] = [];

    constructor (data: IRelationST) {
        super(data);

        this.entity = data.entity;
        this.initDirect = data.initDirect;
        this.initRelName = data.initRelName;

        if (data.relationIds) {
            this.relationIds = data.relationIds;
        }

        if (data.attributes) {
            data.attributes.forEach((attribute) => {
                if (
                    attribute instanceof AbstractAttributeST &&
                    attribute.itemsData.some((item) => item.getValue() !== null && item.getValue() !== undefined)
                ) {
                    this.items.push(attribute);
                }
            });
        }
    }

    @action
    public setRelationIds (ids: string[]) {
        this.relationIds = ids;
    }

    private getInitItems (): [BooleanFormField, StringFormField] {
        return [
            new BooleanFormField({
                name: '$direct',
                value: this.initDirect
            }),
            new StringFormField({
                name: '$rel_name',
                value: this.initRelName
            })
        ];
    }

    public getQuery () {
        const sup = new AbstractSupplementaryRequest({
            dataType: SupplementaryDataType.ETALON_REL,
            entity: this.entity,
            formFields: this.getInitItems()
        });

        if (this.relationIds.length > 0) {
            const relationFormFields = this.relationIds.map((relId) => {
                const value = relId.split('__'); // todo Brauer Ilya - now the unique id includes $from and $to via __

                return new StringFormField({
                    name: systemVariables.$etalonIdTo,
                    value: value[0]
                });
            });

            sup.addFormField(relationFormFields);
        }

        if (this.items.length > 0) {
            this.items.forEach((item) => {
                if (item instanceof AbstractAttributeST) {
                    (item.getQuery().formFields || []).forEach((formField) => {
                        sup.addFormField(formField);
                    });
                }
            });
        }

        return {
            supplementaryRequest: sup
        };
    }

    protected get constructorName (): string {
        return 'RelationST';
    }

    public getJsonData (): JsonData {
        return {
            ...super.getJsonData(),
            json: {
                initDirect: this.initDirect,
                initRelName: this.initRelName,
                entity: this.entity,
                relationIds: this.relationIds,
                attributes: this.attributes.map((attribute) => attribute.getJsonData())
            }
        };
    }

}
