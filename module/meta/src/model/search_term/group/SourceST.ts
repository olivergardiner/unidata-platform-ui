/**
 * SourceSystem SearchTerm model
 *
 * @author Brauer Ilya
 * @date 2020-07-27
 */

import {
    AbstractAttributeST,
    AbstractGroupST,
    AbstractSearchTerm,
    IAbstractGroupST,
    JsonData,
    QueryPart
} from '../index';
import {SourceSystemFormField} from '../../search_query';

export class SourceST extends AbstractGroupST {
    public attributes: AbstractSearchTerm[] = [];

    constructor (data: IAbstractGroupST) {
        super(data);

        if (data.attributes) {
            data.attributes.forEach((attribute) => {
                if (
                    attribute instanceof AbstractAttributeST &&
                    attribute.itemsData.some((item) => item.getValue() !== null && item.getValue() !== undefined)
                ) {
                    this.items.push(attribute);
                }
            });
        }
    }

    public setAttributes = (attributes: AbstractSearchTerm[]) => {
        this.attributes = attributes;
    };

    public getQuery (): QueryPart {
        const query = new SourceSystemFormField();

        if (this.items.length > 0) {
            this.items.forEach((item) => {
                if (item instanceof AbstractAttributeST) {
                    (item.getQuery().formFields || []).forEach((formField) => {
                        query.addFormField(formField);
                    });
                }
            });
        }

        return {
            formGroups: [query]
        };
    }

    protected get constructorName (): string {
        return 'SourceST';
    }

    public getJsonData (): JsonData {
        return {
            ...super.getJsonData(),
            json: {
                entity: this.entity,
                attributes: this.attributes.map((attribute) => attribute.getJsonData())
            }
        };
    }
}
