/**
 * Search term of the creation/change date type
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {DateAttributeST} from './attribute/DateAttributeST';
import {DateFormField} from '../search_query/form_field/DateFormField';
import moment from 'moment';
import {JsonData} from './AbstractSearchTerm';

export class CreateUpdateST extends DateAttributeST {
    // for this type of ST there can always be only one item in the formFields list
    public getFormField () {
        return this.itemsData[0];
    }

    public changeItem (data: any) {
        super.changeItem(0, data);
    }

    public getQuery () {
        if (this.items[0].getValue() === null) {
            return {};
        } else {
            const [rangeFrom = null, rangeTo = null] = this.items[0].getValue() || [];

            return {formFields: [
                new DateFormField({
                    name: this.key,
                    range: [
                        rangeFrom,
                        rangeFrom === rangeTo && rangeFrom !== null && rangeTo !== null ?
                            moment.utc(rangeTo).add(1, 'd').toISOString() :
                            rangeTo
                    ]
                })
            ]};
        }
    }

    protected get constructorName (): string {
        return 'CreateUpdateST';
    }

    public getJsonData (): JsonData {
        const {formFields} = this.getQuery();

        return {
            ...super.getJsonData(),
            formFields: (formFields || []).map((formField) => formField.toJson())
        };
    }

}
