/**
 * A search term of the attribute type. Time
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractAttributeST, IAbstractAttribute} from './AbstractAttributeSearchTerm';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {ITimeFormField, TimeFormField} from '../../search_query/form_field/TimeFormField';

type ITimeAttributeST = Omit<IAbstractAttribute, 'type'> & {
    items?: ITimeFormField[];
}

export class TimeAttributeST extends AbstractAttributeST<TimeFormField, SIMPLE_DATA_TYPE> {
    protected type = SIMPLE_DATA_TYPE.TIME;

    constructor (data: ITimeAttributeST) {
        super(data);

        if (data.items && data.items.length > 0) {
            this.items = data.items.map((item) => {
                return new TimeFormField({
                    ...item,
                    name: this.key
                });
            });
        }
    }

    protected getInitItem () {
        return new TimeFormField({value: null, name: this.key});
    }

    protected get constructorName (): string {
        return 'TimeAttributeST';
    }
}
