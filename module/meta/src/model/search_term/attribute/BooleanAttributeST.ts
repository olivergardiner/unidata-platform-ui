/**
 * A search term of the attribute type. Boolean
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractAttributeST, IAbstractAttribute} from './AbstractAttributeSearchTerm';
import {i18n} from '@unidata/core-app';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {BooleanFormField, IBooleanFormField} from '../../search_query';
import {IOption} from '@unidata/uikit';
import {JsonData} from '../../../index';

type IBooleanAttributeST = Omit<IAbstractAttribute, 'type'> & {
    items?: IBooleanFormField[];
}

export class BooleanAttributeST extends AbstractAttributeST<BooleanFormField, SIMPLE_DATA_TYPE> {
    protected type = SIMPLE_DATA_TYPE.BOOLEAN;

    constructor (data: IBooleanAttributeST) {
        super(data);

        if (data.items !== undefined) {
            this.items = data.items.map((item) => {
                return new BooleanFormField({
                    ...item,
                    name: this.key
                });
            });
        }
    }

    protected getInitItem () {
        const initValue = true;

        return new BooleanFormField({value: initValue, name: this.key});
    }

    public getOptions (): IOption[] {
        return [
            {
                title: i18n.t('module.data>search.panel>attributesMeta>yes'),
                value: 'true'
            },
            {
                title: i18n.t('module.data>search.panel>attributesMeta>no'),
                value: 'false'
            }
        ];
    }

    public getJsonData (): JsonData {
        return {
            ...super.getJsonData(),
            json: {
                options: this.getOptions()
            }
        };
    }

    protected get constructorName (): string {
        return 'BooleanAttributeST';
    }
}
