/**
 * A search term of the attribute type. Number or Integer
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractAttributeST, IAbstractAttribute} from './AbstractAttributeSearchTerm';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {INumberFormField, NumberFormField} from '../../search_query/form_field/NumberFormField';
import {IntegerFormField} from '../../search_query/form_field/IntegerFormField';

type INumberAttributeST = Omit<IAbstractAttribute, 'type'> & {
    items?: INumberFormField[];
    type: SIMPLE_DATA_TYPE.NUMBER | SIMPLE_DATA_TYPE.INTEGER;

    measurement?: {
        groupName: string;
        displayName: string;
        shortName: string;
    };
}

export class NumberAttributeST extends AbstractAttributeST<NumberFormField | IntegerFormField, SIMPLE_DATA_TYPE> {
    public readonly type: SIMPLE_DATA_TYPE.NUMBER | SIMPLE_DATA_TYPE.INTEGER;
    public readonly measurement?: {
        groupName: string;
        displayName: string;
        shortName: string;
    };

    constructor (data: INumberAttributeST) {
        super(data);

        this.measurement = data.measurement;

        if (data.items !== undefined) {
            const Ctor = this.type === SIMPLE_DATA_TYPE.NUMBER ? NumberFormField : IntegerFormField;

            this.items = data.items.map((item) => {
                return new Ctor({
                    ...item,
                    name: this.key});
            });
        }
    }

    protected getInitItem () {
        const Ctor = this.type === SIMPLE_DATA_TYPE.NUMBER ? NumberFormField : IntegerFormField;

        return new Ctor({value: null, name: this.key});
    }

    protected get constructorName (): string {
        return 'NumberAttributeST';
    }
}
