/**
 * A search term of the attribute type. Enum
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractAttributeST, IAbstractAttribute} from './AbstractAttributeSearchTerm';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {EnumFormField, IEnumFormField} from '../../search_query/form_field/EnumFormField';
import {IOption} from '@unidata/uikit';
import {JsonData} from '../../../index';

type IEnumAttributeST = Omit<IAbstractAttribute, 'type'> & {
    items?: IEnumFormField[];
    options: IOption[];
}

export class EnumAttributeST extends AbstractAttributeST<EnumFormField, SIMPLE_DATA_TYPE> {
    private readonly options: IOption[];
    public readonly type: SIMPLE_DATA_TYPE = SIMPLE_DATA_TYPE.STRING;

    constructor (data: IEnumAttributeST) {
        super(data);

        this.options = data.options;

        if (data.items !== undefined) {
            this.items = data.items.map((item) => {
                return new EnumFormField({
                    ...item,
                    name: this.key});
            });
        }
    }

    protected getInitItem () {
        return new EnumFormField({value: null, name: this.key});
    }

    public getOptions (): IOption[] {
        return this.options;
    }

    public getJsonData (): JsonData {
        return {
            ...super.getJsonData(),
            json: {
                options: this.getOptions()
            }
        };
    }

    protected get constructorName (): string {
        return 'EnumAttributeST';
    }
}
