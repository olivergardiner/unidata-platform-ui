/**
 * A search term of the attribute. Ref type
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractAttributeST, IAbstractAttribute} from './AbstractAttributeSearchTerm';
import {CODE_DATA_TYPE} from '@unidata/types';
import {IRefFormField, RefFormField} from '../../search_query/form_field/RefFormField';
import {IOption} from '@unidata/uikit';
import {observable, runInAction} from 'mobx';
import {JsonData} from '../../../index';

type IRefAttributeST = Omit<IAbstractAttribute, 'type'> & {
    items?: IRefFormField[];
    options: (() => Promise<IOption[]>) | IOption[];
    type: CODE_DATA_TYPE.STRING | CODE_DATA_TYPE.INTEGER;
}

export class RefAttributeST extends AbstractAttributeST<RefFormField, CODE_DATA_TYPE> {
    @observable.ref
    private options: IOption[] = [];
    protected readonly type: CODE_DATA_TYPE.STRING | CODE_DATA_TYPE.INTEGER;

    constructor (data: IRefAttributeST) {
        super(data);

        if (Array.isArray(data.options)) {
            this.options = data.options;
        } else {
            data.options().then((list) => {
                runInAction(() => {
                    this.options = list;
                });
            });
        }

        if (data.items !== undefined) {
            this.items = data.items.map((item) => {
                return new RefFormField({
                    ...item,
                    name: this.key,
                    type: this.type
                });
            });
        }
    }

    protected getInitItem () {
        return new RefFormField({value: null, name: this.key, type: this.type});
    }

    public getOptions (): IOption[] {
        return this.options;
    }

    public getJsonData (): JsonData {
        return {
            ...super.getJsonData(),
            json: {
                options: this.getOptions()
            }
        };
    }

    protected get constructorName (): string {
        return 'RefAttributeST';
    }
}
