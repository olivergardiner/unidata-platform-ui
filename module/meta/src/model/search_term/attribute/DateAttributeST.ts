/**
 * A search term of the attribute type. Date or Timestamp
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractAttributeST, IAbstractAttribute} from './AbstractAttributeSearchTerm';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {DateFormField, IDateFormField, TimestampFormField} from '../../search_query';

type IDateAttributeST = Omit<IAbstractAttribute, 'type'> & {
    items?: Array<Omit<IDateFormField, 'name'>>;
    type: SIMPLE_DATA_TYPE.DATE | SIMPLE_DATA_TYPE.TIMESTAMP;
}

export class DateAttributeST extends AbstractAttributeST<DateFormField | TimestampFormField, SIMPLE_DATA_TYPE> {
    public readonly type: SIMPLE_DATA_TYPE.DATE | SIMPLE_DATA_TYPE.TIMESTAMP;

    constructor (data: IDateAttributeST) {
        super(data);

        if (data.items !== undefined) {
            const Ctor = this.type === SIMPLE_DATA_TYPE.DATE ? DateFormField : TimestampFormField;

            this.items = data.items.map((item) => {
                return new Ctor({
                    ...item,
                    name: this.key});
            });
        }
    }

    protected getInitItem () {
        const Ctor = this.type === SIMPLE_DATA_TYPE.DATE ? DateFormField : TimestampFormField;

        return new Ctor({value: null, name: this.key});
    }

    protected get constructorName (): string {
        return 'DateAttributeST';
    }
}
