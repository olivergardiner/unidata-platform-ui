/**
 * A search term of the attribute type. String
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractAttributeST, IAbstractAttribute} from './AbstractAttributeSearchTerm';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {IStringFormField, StringFormField} from '../../search_query/form_field/StringFormField';

type IStringAttributeST = Omit<IAbstractAttribute, 'type'> & {
    items?: Array<Omit<IStringFormField, 'name'>>;
}

export class StringAttributeST extends AbstractAttributeST<StringFormField, SIMPLE_DATA_TYPE.STRING> {
    protected type = SIMPLE_DATA_TYPE.STRING;

    constructor (data: IStringAttributeST) {
        super(data);

        if (data.items !== undefined) {
            this.items = data.items.map((item) => {
                return new StringFormField({
                    ...item,
                    name: this.key
                });
            });
        }
    }

    protected getInitItem () {
        return new StringFormField({value: null, name: this.key});
    }

    protected get constructorName (): string {
        return 'StringAttributeST';
    }
}
