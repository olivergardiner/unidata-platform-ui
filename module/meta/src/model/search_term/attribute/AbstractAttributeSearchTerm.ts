/**
 * Abstract search term of the Attribute type
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractSearchTerm, IAbstractSearchTerm, JsonData, QueryPart, SEARCH_TERM_GROUP} from '../AbstractSearchTerm';
import {
    AttributeTypeCategory,
    CODE_DATA_TYPE,
    SIMPLE_DATA_TYPE
} from '@unidata/types';
import {action, computed, observable} from 'mobx';
import {
    AliasCodeAttribute,
    ArrayAttribute,
    CodeAttribute,
    EnumerationManager,
    MeasurementValuesManager,
    SimpleAttribute
} from '../../../index';
import {
    BooleanAttributeST, DateAttributeST,
    EnumAttributeST,
    NumberAttributeST,
    RefAttributeST,
    StringAttributeST,
    TimeAttributeST
} from './index';
import {AbstractFormField, FormFieldType} from '../../search_query';
import {IOption} from '@unidata/uikit';

export type IAbstractAttribute = IAbstractSearchTerm & {
    type?: FormFieldType;
}

export type IVisibleValue = {
    concatType: string;
    inverted: string;
    text: Array<{helper?: string; value?: string}>;
}

export class AbstractAttributeST<T extends AbstractFormField, Q extends FormFieldType> extends AbstractSearchTerm {
    @observable.shallow
    protected items: T[];

    protected type: FormFieldType;

    protected constructor (data: IAbstractAttribute) {
        super(data);

        if (data.type !== undefined) {
            this.type = data.type;
        }

        this.items = [this.getInitItem()];
    }

    protected getInitItem (): T {
        throw new Error('You must implement "getInitItem" method');
    }

    public getJsonData (): JsonData {
        return {
            ...super.getJsonData(),
            formFields: this.items.map((formField) => formField.toJson())
        };
    }

    @action
    public addItem () {
        this.items.push(this.getInitItem());
    }

    @action
    public deleteItem (index: number) {
        this.items.splice(index, 1);
    }

    public changeItem (index: number, data: any) {
        this.items[index].change(data);
    }

    @computed
    public get itemsData () {
        return this.items;
    }

    public getQuery (): QueryPart {
        return {
            formFields: this.items.slice()
        };
    }

    public static createAttributeST = (
        metaAttribute: SimpleAttribute | CodeAttribute | AliasCodeAttribute | ArrayAttribute,
        getRefOptions: (
            lookupEntityType: string,
            lookupEntityDisplayAttributes: string[],
            lookupEntitySearchAttributes: string[]
        ) => () => Promise<IOption[]>,
        key?: string,
        displayName?: string
    ): AbstractSearchTerm => {

        const commonData = {
            key: key || metaAttribute.name.getValue(),
            displayName: displayName || metaAttribute.displayName.getValue(),
            termGroup: SEARCH_TERM_GROUP.ATTRIBUTE
        };

        // Attribute of the enumeration type
        if (metaAttribute instanceof SimpleAttribute && metaAttribute.typeCategory === AttributeTypeCategory.enumDataType) {
            const enumData = EnumerationManager.getEnum(metaAttribute.typeValue);

            if (enumData !== undefined) {
                return new EnumAttributeST({
                    ...commonData,
                    options: enumData.values.items.map((enumItem) => {
                        return {
                            value: enumItem.name.getValue(),
                            title: enumItem.displayName.getValue()
                        };
                    })
                });
            }
        }

        // Attribute of the local enumeration type
        if ((metaAttribute instanceof SimpleAttribute || metaAttribute instanceof ArrayAttribute) &&
            metaAttribute.dictionaryDataType.getValue() !== '') {
            const dictionaryList = metaAttribute.dictionaryDataType.getValue().split(metaAttribute.dictionaryDataType.delimiter);

            return new EnumAttributeST({
                ...commonData,
                options: dictionaryList.map((enumItem) => {
                    return {
                        value: enumItem,
                        title: enumItem
                    };
                })
            });
        }

        // Attribute of the reference reference type
        if ((metaAttribute instanceof SimpleAttribute || metaAttribute instanceof ArrayAttribute) &&
            metaAttribute.lookupEntityType.getValue() !== '') {
            return new RefAttributeST({
                ...commonData,
                type: metaAttribute.lookupEntityCodeAttributeType.getValue() as CODE_DATA_TYPE,
                options: getRefOptions(
                    metaAttribute.lookupEntityType.getValue(),
                    metaAttribute.lookupEntityDisplayAttributes.getValue(),
                    metaAttribute.lookupEntitySearchAttributes.getValue()
                )
            });
        }

        const typeValue = metaAttribute.typeValue;

        // other types
        switch (typeValue) {
            case SIMPLE_DATA_TYPE.STRING:
            case SIMPLE_DATA_TYPE.CLOB:
            case SIMPLE_DATA_TYPE.BLOB: {
                return new StringAttributeST({
                    ...commonData
                });
            }

            case SIMPLE_DATA_TYPE.NUMBER:
            case SIMPLE_DATA_TYPE.INTEGER: {
                const measurement = (metaAttribute instanceof SimpleAttribute) ?
                    MeasurementValuesManager.getMeasurementValue(metaAttribute.valueId.getValue()) : undefined;

                let measurementUnit;

                if (measurement !== undefined) {
                    measurementUnit = measurement.measurementUnits.find2((item) => {
                        return metaAttribute instanceof SimpleAttribute && item.id.getValue() === metaAttribute.defaultUnitId.getValue();
                    });
                }

                return new NumberAttributeST({
                    ...commonData,
                    type: typeValue,
                    measurement: measurement === undefined || !measurementUnit ? undefined : {
                        groupName: measurement.name.getValue(),
                        displayName: measurementUnit.name.getValue(),
                        shortName: measurementUnit.shortName.getValue()
                    }
                });
            }

            case SIMPLE_DATA_TYPE.BOOLEAN: {
                return new BooleanAttributeST({
                    ...commonData
                });
            }

            case SIMPLE_DATA_TYPE.DATE:
            case SIMPLE_DATA_TYPE.TIMESTAMP: {
                return new DateAttributeST({
                    ...commonData,
                    type: typeValue
                });
            }

            case SIMPLE_DATA_TYPE.TIME: {
                return new TimeAttributeST({
                    ...commonData
                });
            }

            default:
                return new StringAttributeST({
                    ...commonData
                });
        }
    };
}
