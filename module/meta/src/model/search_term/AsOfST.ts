/**
 * The type of search term relevance
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {DateAttributeST} from './index';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {systemVariables, DateFormField, TimestampFormField, AbstractFacet, FACETS} from '../search_query';
import {JsonData} from './AbstractSearchTerm';

export class AsOfST extends DateAttributeST {
    // for this type of ST there can always be only one item in the formFields list
    public getFormField () {
        return this.itemsData[0];
    }

    public changeItem (data: any) {
        super.changeItem(0, data);
    }

    public getQuery () {
        if (this.items[0].getValue() === null) {
            return {};
        }

        const ctor = this.type === SIMPLE_DATA_TYPE.DATE ? DateFormField : TimestampFormField;
        const [rangeFrom, rangeTo] = this.items[0].getValue() || [];
        const res = [];

        if (rangeTo) {
            res.push(new ctor({
                name: systemVariables.$to,
                type: this.type,
                range: [rangeTo, null]
            }));
        }

        if (rangeFrom) {
            res.push(new ctor({
                name: systemVariables.$from,
                type: this.type,
                range: [null, rangeFrom]
            }));
        }

        return {
            formFields: res,
            facet: new AbstractFacet(FACETS.UN_RANGED)
        };
    }

    protected get constructorName (): string {
        return 'AsOfST';
    }

    public getJsonData (): JsonData {
        const {formFields, facet} = this.getQuery();

        return {
            ...super.getJsonData(),
            formFields: (formFields || []).map((formField) => formField.toJson()),
            facets: facet ? facet.toJson() : []
        };
    }
}
