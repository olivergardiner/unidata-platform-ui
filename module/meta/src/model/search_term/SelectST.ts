/**
 * Search term of the list type
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {AbstractSearchTerm, IAbstractSearchTerm} from './index';
import {action, observable} from 'mobx';
import {IOption} from '@unidata/uikit';
import {AbstractFacet, FACETS} from '../search_query/facet/AbstractFacet';
import {EnumFormField} from '../search_query/form_field/EnumFormField';
import {JsonData} from './AbstractSearchTerm';

type ISelectFacetST = IAbstractSearchTerm & {
    value: string[];
    options: IOption[];
    isMulti?: boolean;
    returnType?: 'facet' | 'formField';
}

export class SelectST extends AbstractSearchTerm {
    @observable.ref
    public value: string[];

    private options: IOption[];
    public readonly isMulti?: boolean;
    private readonly returnType: 'facet' | 'formField';

    constructor (data: ISelectFacetST) {
        super(data);

        this.value = data.value;
        this.options = data.options;
        this.isMulti = data.isMulti;
        this.returnType = data.returnType || 'facet';
    }

    @action
    public setValue (value: string[]) {
        this.value = value;
    }

    public getQuery () {
        if (this.returnType === 'facet') {
            return {
                facet: new AbstractFacet(this.value.map((valItem) => (valItem as FACETS)))
            };
        } else {
            return {
                formFields: this.value.map((valueItem) => {
                    return new EnumFormField({value: valueItem, name: this.key});
                })
            };
        }
    }

    protected get constructorName (): string {
        return 'SelectST';
    }

    public getJsonData (): JsonData {
        const {formFields, facet} = this.getQuery();

        return {
            ...super.getJsonData(),
            formFields: (formFields || []).map((formField) => formField.toJson()),
            facets: facet ? facet.toJson() : [],
            json: {
                options: this.getOptions(),
                isMulti: this.isMulti,
                returnType: this.returnType
            }
        };
    }

    public getOptions (): IOption[] {
        return this.options;
    }

    public setOptions (options: IOption[]) {
        this.options = options;
    }
}
