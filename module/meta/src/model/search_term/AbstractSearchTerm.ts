/**
 * Abstract search attribute. Stores some part (s) of the Search Query
 *
 * @author Brauer Ilya
 * @date 2020-06-10
 */

import {Uuid} from '@unidata/core-app';

import {
    EnumAttributeST,
    NumberAttributeST,
    DateAttributeST,
    TimeAttributeST,
    StringAttributeST,
    BooleanAttributeST,
    RefAttributeST
} from './attribute';

import {
    AbstractFormField,
    AbstractFacet,
    AbstractSupplementaryRequest, IFormField, FACETS, ISupplementaryRequest,
    AbstractFormGroup, IFormGroup
} from '../search_query';

export enum SEARCH_TERM_GROUP {
    ATTRIBUTE = 'attribute',
    CLASSIFY = 'classify',
    RELATIONS = 'relations',
    QUALITY = 'quality',
    SYSTEM = 'system'
}

export type IAbstractSearchTerm = {
    id?: string;
    key: string;
    displayName?: string;
    parentId?: string;
    termGroup: SEARCH_TERM_GROUP;
};

export type SearchTerm =
    StringAttributeST |
    NumberAttributeST |
    BooleanAttributeST |
    DateAttributeST |
    EnumAttributeST |
    RefAttributeST |
    TimeAttributeST;

export type QueryPart = {
    formFields?: AbstractFormField[];
    formGroups?: AbstractFormGroup[];
    facet?: AbstractFacet;
    supplementaryRequest?: AbstractSupplementaryRequest;
}

export type JsonData = IAbstractSearchTerm & {
    id: string;
    formFields: IFormField[];
    formGroups: IFormGroup[];
    facets: FACETS[];
    supplementaryRequest: ISupplementaryRequest[];
    constructorName: string;
    // options?: IOption[];
    json: any;
}

export abstract class AbstractSearchTerm {

    public readonly id: string; // unique identifier for term
    public readonly key: string; // search key (path)
    public readonly displayName: string; // display name of the term
    public parentId?: string; // link to the parent
    public readonly termGroup: SEARCH_TERM_GROUP; // division by business group

    protected constructor (data: IAbstractSearchTerm) {
        this.id = data.id ? data.id : Uuid.v1();
        this.key = data.key;
        this.displayName = data.displayName || '';
        this.parentId = data.parentId;
        this.termGroup = data.termGroup;
    }

    public setParentId (parentId: string) {
        this.parentId = parentId;
    }

    public getQuery (): QueryPart {
        throw new Error('You must implement "getQuery" method');
    }

    protected get constructorName (): string {
        throw new Error('You must implement constructorName property');
    }

    public getJsonData (): JsonData {
        return {
            id: this.id,
            key: this.key,
            displayName: this.displayName,
            parentId: this.parentId,
            termGroup: this.termGroup,
            formFields: [],
            formGroups: [],
            facets: [],
            supplementaryRequest: [],
            constructorName: this.constructorName,
            json: {}
        };
    }
}
