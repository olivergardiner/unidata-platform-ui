/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-29
 */

import {observable} from 'mobx';
import {hasMany, IAssociationDescriptorMap, ModelCollection, ModelMetaData} from '@unidata/core';
import {SimpleAttribute} from '../attribute/SimpleAttribute';
import {ComplexAttribute} from '../attribute/ComplexAttribute';
import {ArrayAttribute} from '../attribute/ArrayAttribute';
import {AbstractEntity} from './AbstractEntity';

export class NestedEntity extends AbstractEntity {

    @observable
    @hasMany()
    public simpleAttributes: ModelCollection<SimpleAttribute>;

    @observable
    @hasMany()
    public complexAttributes: ModelCollection<ComplexAttribute>;

    @observable
    @hasMany()
    public arrayAttributes: ModelCollection<ArrayAttribute>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                simpleAttributes: SimpleAttribute,
                complexAttributes: ComplexAttribute,
                arrayAttributes: ArrayAttribute
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public getAllAttributes () {
        return [
            ...this.simpleAttributes.getRange(),
            ...this.arrayAttributes.getRange(),
            ...this.complexAttributes.getRange()
        ];
    }
}
