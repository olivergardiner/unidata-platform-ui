/**
 * Directory entry model for registry/reference groups
 *
 * @author: Denis Makarov
 * @date: 2018-09-05
 */

import {AbstractModel, hasMany, ModelCollection, stringField, StringField} from '@unidata/core';
import {observable} from 'mobx';
import {CatalogEntity} from './CatalogEntity';
import {CatalogLookupEntity} from './CatalogLookupEntity';

export class Catalog extends AbstractModel {
    @observable
    @stringField({unique: true})
    public groupName: StringField;

    @observable
    @stringField()
    public title: StringField;

    @observable
    @hasMany({ofType: CatalogEntity})
    public entities: ModelCollection<CatalogEntity>;

    @observable
    @hasMany({ofType: CatalogLookupEntity})
    public lookupEntities: ModelCollection<CatalogLookupEntity>;
}
