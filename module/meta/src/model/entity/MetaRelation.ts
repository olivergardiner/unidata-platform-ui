/**
 * Meta relaton
 *
 * @author: Stebunov Vladimir
 * @date: 2019-10-14
 */

import {observable} from 'mobx';
import {
    AbstractModel,
    anyField,
    AnyField,
    booleanField,
    BooleanField,
    EnumField,
    hasMany,
    IAssociationDescriptorMap,
    ModelCollection,
    ModelMetaData,
    stringField,
    StringField
} from '@unidata/core';
import {SimpleAttribute} from '../attribute/SimpleAttribute';
import {ArrayAttribute} from '../attribute/ArrayAttribute';
import {KeyValuePair} from '../../type/KeyValuePair';
import {RelType} from '@unidata/types';

export class MetaRelation extends AbstractModel {
    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

    @observable
    @stringField()
    public description: StringField;

    @observable
    @stringField()
    public fromEntity: StringField;

    @observable
    @stringField()
    public toEntity: StringField;

    @observable
    @stringField()
    public relType: EnumField<RelType>;

    @observable
    @anyField()
    public toEntityDefaultDisplayAttributes: AnyField;

    @observable
    @booleanField()
    public useAttributeNameForDisplay: BooleanField;

    @observable
    @booleanField()
    public required: BooleanField;

    @observable
    @stringField()
    public hasData: BooleanField;

    @observable
    @booleanField({defaultValue: false})
    public displayable: BooleanField;

    @observable
    @hasMany()
    public arrayAttributes: ModelCollection<ArrayAttribute>;

    @observable
    @hasMany()
    public simpleAttributes: ModelCollection<SimpleAttribute>;

    @observable
    @hasMany()
    public customProperties: ModelCollection<KeyValuePair>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                arrayAttributes: ArrayAttribute,
                simpleAttributes: SimpleAttribute,
                customProperties: KeyValuePair
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

}
