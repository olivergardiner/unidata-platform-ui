/**
 * Attribute group
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {AbstractModel, arrayField, ArrayField, IntegerField, integerField, StringField, stringField} from '@unidata/core';
import {computed} from 'mobx';

export class AttributeGroup extends AbstractModel {
    public static UNNAMED_GROUP = 'UNNAMED_GROUP';

    @arrayField()
    public attributes: ArrayField;

    @integerField()
    public column: IntegerField;

    @integerField()
    public row: IntegerField;

    @stringField()
    public title: StringField;

    @computed
    public get id () {
        return this.column.getValue() + ':' + this.row.getValue();
    }
}
