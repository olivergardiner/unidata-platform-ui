/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-29
 */

import {observable} from 'mobx';
import {AnyField, anyField, hasMany, IAssociationDescriptorMap, ModelCollection, ModelMetaData, AbstractField} from '@unidata/core';
import {SimpleAttribute} from '../attribute/SimpleAttribute';
import {ArrayAttribute} from '../attribute/ArrayAttribute';
import {ComplexAttribute} from '../attribute/ComplexAttribute';
import {AbstractEntity} from './AbstractEntity';
import {MetaRelation} from './MetaRelation';
import {IValidityPeriod} from './IValidityPeriod';
import {DateGranularityMode} from '@unidata/types';
import {AbstractAttribute} from '../attribute/AbstractAttribute';

export class Entity extends AbstractEntity {
    @observable
    @hasMany()
    public simpleAttributes: ModelCollection<SimpleAttribute>;

    @observable
    @hasMany()
    public complexAttributes: ModelCollection<ComplexAttribute>;

    @observable
    @hasMany()
    public arrayAttributes: ModelCollection<ArrayAttribute>;

    @observable
    @hasMany()
    public relations: ModelCollection<MetaRelation>;

    @observable
    @anyField()
    public validityPeriod: AnyField<IValidityPeriod>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                simpleAttributes: SimpleAttribute,
                complexAttributes: ComplexAttribute,
                arrayAttributes: ArrayAttribute,
                relations: MetaRelation
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }


    /**
     * Returns granularity of the metamodel date, if omitted, returns that it is DATE
     *
     * @returns {DateGranularityMode}
     */
    public getGranularityMode (): DateGranularityMode {
        const validityPeriod = this.validityPeriod.getValue();

        return validityPeriod && validityPeriod.mode || DateGranularityMode.DATE;
    }

    public getMetaAttributesByPropertyValue (
        property: string,
        value: any) {

        let allAttributes: AbstractAttribute[] = [];
        let filteredAttributes: AbstractAttribute[] = [];

        allAttributes = allAttributes.concat(this.simpleAttributes.getRange());
        allAttributes = allAttributes.concat(this.arrayAttributes.getRange());
        allAttributes = allAttributes.concat(this.complexAttributes.getRange());

        filteredAttributes = allAttributes.filter((attr: ArrayAttribute) => {
            const field = (attr as any)[property];

            if (field instanceof AbstractField) {
                return field.getValue() === value;
            }

            return false;
        });

        return filteredAttributes;

    }
}
