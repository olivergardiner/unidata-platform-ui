/**
 * Catalog: registry element
 *
 * @author: Sergey Shishigin
 * @date: 2018-09-20
 */
import {CatalogAbstractEntity} from './CatalogAbstractEntity';

export class CatalogEntity extends CatalogAbstractEntity {

}
