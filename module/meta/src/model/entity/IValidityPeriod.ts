import {DateGranularityMode} from '@unidata/types';

export interface IValidityPeriod {
    mode: DateGranularityMode;
    end: string;
    start: string;
}
