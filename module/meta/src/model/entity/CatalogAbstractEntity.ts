/**
 * Abstract catalog item
 * @author Sergey Shishigin
 * @date 2018-09-20
 */
import {AbstractModel, StringField, stringField} from '@unidata/core';
import {observable} from 'mobx';

export class CatalogAbstractEntity extends AbstractModel {
    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;
}
