/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-29
 */

import {observable} from 'mobx';
import {
    AnyField,
    anyField,
    hasMany,
    hasOne,
    IAssociationDescriptorMap,
    ModelCollection,
    ModelMetaData,
    Nullable,
    AbstractField
} from '@unidata/core';
import {CodeAttribute} from '../attribute/CodeAttribute';
import {SimpleAttribute} from '../attribute/SimpleAttribute';
import {AliasCodeAttribute} from '../attribute/AliasCodeAttribute';
import {ArrayAttribute} from '../attribute/ArrayAttribute';
import {AbstractEntity} from './AbstractEntity';
import {IValidityPeriod} from './IValidityPeriod';
import {DateGranularityMode} from '@unidata/types';
import {AbstractAttribute} from '../attribute/AbstractAttribute';

export class LookupEntity extends AbstractEntity {
    @observable
    @hasOne({required: true})
    public codeAttribute: Nullable<CodeAttribute>;

    @observable
    @hasMany()
    public simpleAttributes: ModelCollection<SimpleAttribute>;

    @observable
    @hasMany()
    public aliasCodeAttributes: ModelCollection<AliasCodeAttribute>;

    @observable
    @hasMany()
    public arrayAttributes: ModelCollection<ArrayAttribute>;

    @observable
    @anyField()
    public validityPeriod: AnyField<IValidityPeriod>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasOne: {
                codeAttribute: CodeAttribute
            },
            hasMany: {
                simpleAttributes: SimpleAttribute,
                aliasCodeAttributes: AliasCodeAttribute,
                arrayAttributes: ArrayAttribute
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }


    /**
     * Returns granularity of the metamodel date, if omitted, returns that it is DATE
     *
     * @returns {DateGranularityMode}
     */
    public getGranularityMode (): DateGranularityMode {
        const validityPeriod = this.validityPeriod.getValue();

        return validityPeriod && validityPeriod.mode || DateGranularityMode.DATE;
    }

    public getMetaAttributesByPropertyValue (
        property: string,
        value: any) {

        let allAttributes: AbstractAttribute[] = [];
        let filteredAttributes: AbstractAttribute[] = [];

        allAttributes = allAttributes.concat(this.simpleAttributes.getRange());
        allAttributes = allAttributes.concat(this.arrayAttributes.getRange());
        allAttributes = allAttributes.concat(this.aliasCodeAttributes.getRange());

        filteredAttributes = allAttributes.filter((attr: ArrayAttribute) => {
            const field = (attr as any)[property];

            if (field instanceof AbstractField) {
                return field.getValue() === value;
            }

            return false;
        });

        return filteredAttributes;

    }
}
