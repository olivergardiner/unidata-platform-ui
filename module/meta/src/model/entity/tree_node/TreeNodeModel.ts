/**
 * Default tree node
 *
 * @author: Sergey Shishigin
 * @date: 2018-09-20
 */
import {observable} from 'mobx';
import {hasMany, ModelCollection} from '@unidata/core';
import {AbstractTreeNodeModel} from './AbstractTreeNodeModel';

export class TreeNodeModel extends AbstractTreeNodeModel {
    @observable
    @hasMany({ofType: TreeNodeModel})
    public children: ModelCollection<TreeNodeModel>;
}
