/**
 * Abstract tree node
 *
 * @author: Sergey Shishigin
 * @date: 2018-09-20
 */

import {AbstractModel, StringField, stringField} from '@unidata/core';
import {observable} from 'mobx';

export class AbstractTreeNodeModel extends AbstractModel {
    @observable
    @stringField({required: true})
    public id: StringField;

    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public path: StringField;

    @observable
    @stringField()
    public title: StringField;

    @observable
    @stringField()
    public iconType: StringField;
}
