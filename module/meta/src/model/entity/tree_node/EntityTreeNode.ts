/**
 * Node of the registry/reference tree of the "Registry/reference" type"
 *
 * @author: Sergey Shishigin
 * @date: 2018-09-20
 */

import {
    EnumField,
    hasMany,
    IAssociationDescriptorMap,
    ModelCollection,
    ModelMetaData,
    stringField
} from '@unidata/core';
import {observable} from 'mobx';
import {AbstractTreeNodeModel} from './AbstractTreeNodeModel';

export enum EntityTreeNodeType {
    Group = 'Group',
    Entity = 'Entity',
    LookupEntity = 'LookupEntity'
}

export class EntityTreeNode extends AbstractTreeNodeModel {

    public searchValue: string;

    @observable
    @stringField()
    public type: EnumField<EntityTreeNodeType>;

    @observable
    @hasMany({ofType: EntityTreeNode})
    public children: ModelCollection<EntityTreeNode>;

    // parentNode it doesn't work yet. disabled due to the fact that you are using
    // @observable
    // @hasOne()
    // components parentNode: EntityTreeNode;
    //
    // components isRoot() {
    //     return this.parentNode === null;
    // }

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            // hasOne: {
            //     parentNode: EntityTreeNode
            // },
            hasMany: {
                children: EntityTreeNode
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
