/**
 * Base class for the registry / reference model. Simply indicates that the directory and registry models are homogeneous entities.
 *
 * @author Ivan Marshalkin
 * @date 2018-09-06
 */

import {
    AbstractModel,
    anyField,
    AnyField,
    BooleanField,
    booleanField,
    hasMany,
    IAssociationDescriptorMap, ModelCollection,
    ModelMetaData,
    stringField,
    StringField
} from '@unidata/core';
import {observable} from 'mobx';
import {AttributeGroup} from './AttributeGroup';

export class AbstractEntity extends AbstractModel {
    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

    @observable
    @stringField()
    public description: StringField;

    @observable
    @stringField()
    public hasData: BooleanField;

    @observable
    @stringField()
    public groupName: StringField;

    // TODO: perhaps describe it more accurately
    @observable
    @anyField()
    public validityField: AnyField;

    // TODO: perhaps describe it more accurately
    @observable
    @anyField()
    public classifiers: AnyField;

    @observable
    @stringField()
    public version: StringField;

    @observable
    @booleanField()
    public dashboardVisible: BooleanField;

    @observable
    @booleanField()
    public draft: BooleanField;

    @observable
    @hasMany()
    public attributeGroups: ModelCollection<AttributeGroup>;


    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                attributeGroups: AttributeGroup
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public getAttributeGroupById (id: string) {
        const group = this.attributeGroups.getRange().find((group) => {
            return group.id === id;
        });

        return group || null;
    }


}
