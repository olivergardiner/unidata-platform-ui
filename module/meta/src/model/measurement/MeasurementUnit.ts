/**
 * Unit of measurement value model
 *
 * @author: Aleksandr Bavin
 * @date: 2020-04-27
 */

import {AbstractModel, BooleanField, booleanField, numberField, NumberField, StringField, stringField} from '@unidata/core';

export class MeasurementUnit extends AbstractModel {
    @stringField()
    public id: StringField;

    @stringField()
    public name: StringField;

    @stringField()
    public shortName: StringField;

    @numberField()
    public valueId: NumberField;

    @stringField()
    public convectionFunction: StringField;

    @booleanField()
    public base: BooleanField;

}
