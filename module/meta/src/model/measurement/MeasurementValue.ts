/**
 * Measurement value model
 *
 * @author: Aleksandr Bavin
 * @date: 2020-04-27
 */

import {AbstractModel, hasMany, IAssociationDescriptorMap, ModelCollection, ModelMetaData, StringField, stringField} from '@unidata/core';
import {MeasurementUnit} from './MeasurementUnit';

export class MeasurementValue extends AbstractModel {

    @stringField()
    public id: StringField;

    @stringField()
    public name: StringField;

    @stringField()
    public shortName: StringField;

    @hasMany()
    public measurementUnits: ModelCollection<MeasurementUnit>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                measurementUnits: MeasurementUnit
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

}
