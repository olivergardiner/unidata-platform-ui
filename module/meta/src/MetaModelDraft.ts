/**
 * Manager for draft mode
 * TODO: remove the bundle with Unidata.modules.notifier.DraftModeNotifier after completely moving to React
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-12
 */

import {action, observable} from 'mobx';

enum DraftModeNotifierEventType {
    UNKNOWN = 1,
    DRAFTMODECHANGE = 2,
    APPLYDRAFT = 3,
    REMOVEDRAFT = 4
}

// interface for Unidata.module.notifier.DraftModeNotifier
interface IDraftModeNotifier {
    subscribe(type: DraftModeNotifierEventType, fn: {(draftMode: boolean): void}, scope?: any): any;
    getDraftMode(): boolean;
    setDraftMode(draftMode: boolean): void;
    toggleDraftMode(): void;
}

export class MetaModelDraft {

    @observable private static currentDraftMode: boolean;

    private static DraftModeNotifier: IDraftModeNotifier | undefined;
    private static inited: boolean;

    private static init () {
        if (this.inited) {
            return;
        }

        if (window.Unidata && window.Unidata.module) {
            this.DraftModeNotifier = window.Unidata.module.notifier.DraftModeNotifier;
        }

        if (this.DraftModeNotifier) {
            this.DraftModeNotifier.subscribe(
                DraftModeNotifierEventType.DRAFTMODECHANGE,
                (draftMode: boolean) => {
                    this.draftMode = draftMode;
                }
            );

            this.setDraftMode(this.DraftModeNotifier.getDraftMode());
        }

        this.inited = true;
    }

    public static get draftMode () {
        this.init();

        return this.currentDraftMode;
    }


    public static set draftMode (draftMode: boolean) {
        this.init();

        if (this.DraftModeNotifier && draftMode !== this.DraftModeNotifier.getDraftMode()) {
            this.DraftModeNotifier.setDraftMode(draftMode);
        }

        this.setDraftMode(draftMode);
    }

    @action
    private static setDraftMode (draftMode: boolean) {
        this.currentDraftMode = draftMode;
    }

    public static toggleDraftMode () {
        // direct access to the class in order to avoid binding when used in components
        MetaModelDraft.draftMode = !MetaModelDraft.draftMode;
    }

}
