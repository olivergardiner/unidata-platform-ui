import {ITreeNode, TreeUtil} from '@unidata/uikit';
import {action, IObservableArray, observable} from 'mobx';
import {AbstractTreeTableStore} from '@unidata/core-app';
import {CatalogAbstractEntity} from '../../../model/entity/CatalogAbstractEntity';
import {Catalog} from '../../../model/entity/Catalog';
import {CatalogEntity} from '../../../model/entity/CatalogEntity';
import {CatalogLookupEntity} from '../../../model/entity/CatalogLookupEntity';
import {EntityGroupUtils} from '../../../utils/EntityGroupUtils';

export class EntityTreeTableStore extends AbstractTreeTableStore {
    private catalogs: Catalog [];
    protected modelCache: Map<string, CatalogAbstractEntity> = new Map();
    protected nodesCache: Map<string, ITreeNode> = new Map();
    @observable public treeData: IObservableArray<ITreeNode> = observable.array([], {deep: false});
    @observable protected selectedModels: IObservableArray<CatalogAbstractEntity> = observable.array([], {deep: false});
    @observable protected selectedNodes: IObservableArray<ITreeNode> = observable.array([], {deep: false});

    public transformData (data: Catalog[], disabledNodes?: string[]) {
        let me = this,
            treeNodeData,
            result: ITreeNode [];

        data.forEach(function (catalog) {
            catalog.entities.items.forEach(function (catalogEntity: CatalogEntity) {
                me.modelCache.set(catalogEntity.name.getValue(), catalogEntity);
            });

            catalog.lookupEntities.items.forEach(function (catalogLookup: CatalogLookupEntity) {
                me.modelCache.set(catalogLookup.name.getValue(), catalogLookup);
            });
        });

        treeNodeData = EntityGroupUtils.buildTreeNodeModel(data);

        EntityGroupUtils.clearEmptyGroups(treeNodeData);
        this.nodesCache = this.buildTreeNodeCache(treeNodeData, new Map());

        if (treeNodeData.children) {
            result = treeNodeData.children.sort(TreeUtil.sortTree);
        } else {
            result = [];
        }

        return result;
    }

    private buildTreeNodeCache (node: ITreeNode, cache: Map<string, ITreeNode>) {
        let me = this;

        if (node.key) {
            cache.set(node.key, node);
        }

        if (node.children) {
            node.children.forEach((child) => {
                me.buildTreeNodeCache(child, cache);
            });
        }

        return cache;
    }

    @action
    public select (node: ITreeNode) {
        let selected: CatalogAbstractEntity | undefined;

        selected = this.modelCache.get(node.key);

        if (!selected) {
            return;
        }

        this.selectedModels.replace([selected]);
        this.selectedNodes.replace([node]);
    }

    @action
    public setData (data: Catalog[], disabledNodes?: string []) {
        this.catalogs = data;
        this.treeData.replace(this.transformData(data, disabledNodes));
    }

    public getData () {
        return this.catalogs;
    }

    public getSelectedModels (): CatalogAbstractEntity [] {
        return this.selectedModels;
    }

    public getDisplayPath (): string[] {
        const path: string[] = [];
        const node = this.selectedNodes[0];

        if (!node) {
            return [];
        }

        const getParentDisplayNode = (path: string[], node: ITreeNode): void => {
            if (node.row && node.row.displayName) {
                path.unshift(node.row.displayName);
            }

            if (node.parent && node.parent.row) {
                getParentDisplayNode(path, node.parent);
            }
        };

        getParentDisplayNode(path, node);

        return path;
    }

    @action
    public clearSelected () {
        let selectedEntities = this.getSelectedModels();
        let selectedItems = this.getSelectedNodes();

        selectedEntities.splice(0);
        selectedItems.splice(0);
    }

    public getModelByKey (key: string): CatalogEntity | CatalogLookupEntity | undefined {
        return this.modelCache.get(key);
    }

}
