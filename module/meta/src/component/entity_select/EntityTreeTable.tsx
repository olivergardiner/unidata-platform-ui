import * as React from 'react';
import {observer} from 'mobx-react';
import {AbstractTreeTableStore, i18n} from '@unidata/core-app';
import {CatalogEntity} from '../../model/entity/CatalogEntity';
import {CatalogLookupEntity} from '../../model/entity/CatalogLookupEntity';
import {ColorScheme, ITreeColumn, ITreeNode, Tree} from '@unidata/uikit';

interface IProps {
    treeTableStore: AbstractTreeTableStore;
    onEntitySelect: (catalogEntity: CatalogEntity | CatalogLookupEntity) => void;
    onBeforeNodeSelect?: (node: ITreeNode) => boolean;
}

@observer
export class EntityTreeTable extends React.Component<IProps> {
    constructor (props: IProps) {
        super(props);

        this.onNodeSelect = this.onNodeSelect.bind(this);
    }

    columns: ITreeColumn [] = [
        {
            name: 'displayName',
            displayName: i18n.t('common:displayName')
        }
    ];

    onNodeSelect (node: ITreeNode) {
        this.props.treeTableStore.select(node);
        this.props.onEntitySelect(this.props.treeTableStore.getSelectedModels().slice()[0] as CatalogEntity | CatalogLookupEntity);
    }

    get store () {
        return this.props.treeTableStore;
    }

    render () {
        const {treeTableStore} = this.props;

        return (
            <Tree nodes={treeTableStore.treeData.slice()}
                       selectedNodes={treeTableStore.getSelectedKeys()}
                       columns={this.columns}
                       colorScheme={ColorScheme.dark}
                       onNodeSelect={this.onNodeSelect}
                       onBeforeNodeSelect={this.props.onBeforeNodeSelect}
            />
        );
    }
}
