/**
 * The meta export modal window
 *
 * @author Brauer Ilya
 * @date 2020-07-28
 */

import * as React from 'react';
import {Checkbox, Confirm} from '@unidata/uikit';
import {ExportMetaService} from '../../index';
import {Dialog, i18n} from '@unidata/core-app';

import * as styles from './metaExportModal.m.scss';

interface IProps {
    isOpen: boolean;
    onCloseModal: () => void;
}

interface IState {
    users: boolean;
    roles: boolean;
}

export class MetaExportModal extends React.Component<IProps, IState> {

    state: IState = {
        users: false,
        roles: false
    };

    onChange = (key: keyof IState) => () => {
        this.setState((prevState) => {
            return {
                ...prevState,
                [key]: !prevState[key]
            };
        });
    };

    onCloseModal = () => {
        this.props.onCloseModal();
    };

    doExport = () => {
        ExportMetaService
            .exportMeta({
                users: this.state.users,
                roles: this.state.roles
            })
            .then(() => {
                Dialog.showMessage(i18n.t('module.meta>export>exportStarted'));
            })
            .finally(() => {
                this.props.onCloseModal();
            });
    };

    render () {

        return (
            <Confirm
                isOpen={this.props.isOpen}
                header={i18n.t('module.meta>export>modalHeader')}
                onClose={this.onCloseModal}
                onConfirm={this.doExport}
                confirmationMessage={(
                    <div className={styles.container}>
                        {i18n.t('module.meta>export>modalText')}

                        <div className={styles.includes}>
                            <b>{i18n.t('module.meta>export>modalIncludeText')}</b>

                            <div className={styles.checkboxContainer}>
                                <Checkbox checked={this.state.users} onChange={this.onChange('users')}/>
                                {i18n.t('module.meta>export>users')}
                            </div>

                            <div className={styles.checkboxContainer}>
                                <Checkbox checked={this.state.roles} onChange={this.onChange('roles')}/>
                                {i18n.t('module.meta>export>roles')}
                            </div>
                        </div>
                    </div>
                )}
            />
        );
    }
}
