/**
 * The meta import modal window
 *
 * @author Brauer Ilya
 * @date 2020-07-28
 */

import * as React from 'react';
import {Button, Checkbox, Confirm, INTENT, Upload, UploadFile} from '@unidata/uikit';
import {ImportMetaService} from '../../index';
import {Dialog, i18n, UdLogger} from '@unidata/core-app';

import * as styles from './metaImportBaseModal.m.scss';

interface IProps {
    isOpen: boolean;
    onCloseModal: () => void;
}

const MEASURE_FILE = 'measure.xml';
const MODEL_FILE = 'model.xml';
const SECURITY_FILE = 'security.xml';

interface IState {
    override: boolean;
    measure: UploadFile | null;
    model: UploadFile | null;
    security: UploadFile | null;
}

export class MetaImportBaseModal extends React.Component<IProps, IState> {

    state: IState = {
        override: false,
        measure: null,
        model: null,
        security: null
    };

    onChange = (key: keyof IState) => () => {
        this.setState((prevState) => {
            return {
                ...prevState,
                [key]: !prevState[key]
            };
        });
    };

    onCloseModal = () => {
        this.props.onCloseModal();
    };

    doImport = () => {
        let importMeasure: Promise<void> = Promise.resolve();
        let importModel: Promise<void> = Promise.resolve();
        let importSecurity: Promise<void> = Promise.resolve();

        if (this.state.measure !== null) {
            importMeasure = ImportMetaService.baseImportMeasurement(this.state.measure, this.state.override);
        }

        if (this.state.model !== null) {
            importModel = ImportMetaService.baseImportMeta(this.state.model, this.state.override);
        }

        if (this.state.security !== null) {
            // todo Brauer Ilya: add import for security after backend
        }

        Promise.resolve()
            .then(() => importMeasure)
            .then(() => importModel)
            .then(() => importSecurity)
            .then(() => {
                Dialog.showMessage(i18n.t('module.meta>import>importStarted'));
            })
            .finally(() => {
                this.props.onCloseModal();
            });
    };

    beforeUpload = (file: UploadFile): boolean => {
        if (file.name === MEASURE_FILE) {
            this.setState({measure: file});
        } else if (file.name === MODEL_FILE) {
            this.setState({model: file});
        } else if (file.name === SECURITY_FILE) {
            this.setState({security: file});
        } else {
            Dialog.showWarning(i18n.t('module.meta>import>unknownName'));
        }

        return false;
    };

    onRemove = (file: UploadFile) => {
        if (file.name === MEASURE_FILE) {
            this.setState({measure: null});
        } else if (file.name === MODEL_FILE) {
            this.setState({model: null});
        } else if (file.name === SECURITY_FILE) {
            this.setState({security: null});
        }
    };

    render () {

        const fileList = [];

        if (this.state.measure !== null) {
            fileList.push(this.state.measure);
        }

        if (this.state.model !== null) {
            fileList.push(this.state.model);
        }

        if (this.state.security !== null) {
            fileList.push(this.state.security);
        }

        return (
            <Confirm
                isOpen={this.props.isOpen}
                header={i18n.t('module.meta>import>modalHeader')}
                onClose={this.onCloseModal}
                onConfirm={this.doImport}
                isDisableConfirm={fileList.length === 0}
                confirmationMessage={(
                    <div className={styles.body}>
                        <div className={styles.uploadButton}>
                            <Upload
                                multiple={true}
                                accept={'.xml'}
                                fileList={fileList}
                                beforeUpload={this.beforeUpload}
                                onRemove={this.onRemove}
                            >
                                <Button
                                    intent={INTENT.PRIMARY}
                                    isFilled={true}
                                    data-qaid={'selectfile'}
                                    leftIcon={'upload'}
                                >
                                    {i18n.t('selectFile')}
                                </Button>
                            </Upload>
                        </div>
                        <div>
                            <br/>
                            <Checkbox
                                checked={this.state.override}
                                onChange={this.onChange('override')}
                                disabled={fileList.length === 0}
                            />
                            &nbsp;
                            {i18n.t('module.meta>import>recreate')}
                        </div>
                    </div>
                )}
            />
        );
    }
}
