/**
 * Interface for setting up and getting data for the registry/reference tree
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-24
 */

export interface IEntityTreeConfig {
    showRoot?: boolean;
    showEmptyGroups?: boolean;
    hideEntities?: boolean;
    hideLookupEntities?: boolean;
    draftMode?: boolean;
    searchText?: string;
}
