/**
 * Tree of registers/reference lists
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-24
 */
import * as React from 'react';
import {ITreeNode, Tree} from '@unidata/uikit';
import {action, computed, observable} from 'mobx';
import {observer} from 'mobx-react';
import {EntityTreeStore} from './store/EntityTreeStore';
import {IEntityTreeConfig} from './IEntityTreeConfig';
import {EntityTreeType, EntityTreeTypeConstructor} from '../../type';
import {Catalog, CatalogEntity, CatalogLookupEntity, EntityTreeNodeType} from '../../model';
import {EntityStoreManager} from './store/EntityStoreManager';


export interface IEntityTreeProps extends IEntityTreeConfig {
    multiSelect?: boolean;
    selectableGroups?: boolean;

    defaultSelectedNodes?: string[];
    selectedNodes?: string[];

    onEntitySelect?: (entity: EntityTreeType, selectedEntities: EntityTreeType[]) => void;
    onEntityDeselect?: (entity: EntityTreeType, selectedEntities: EntityTreeType[]) => void;
}

@observer
export class EntityTree extends React.Component<IEntityTreeProps> {

    private store = new EntityTreeStore();

    @observable
    private selectedNodes: string[] = [];

    constructor (props: IEntityTreeProps) {
        super(props);

        this.onNodeSelect = this.onNodeSelect.bind(this);

        this.selectedNodes = this.props.selectedNodes?.slice() || this.props.defaultSelectedNodes?.slice() || [];
    }

    private getEntityType (treeNodeType: string | undefined): EntityTreeTypeConstructor | undefined {
        if (!treeNodeType) {
            return undefined;
        }

        switch (treeNodeType) {
            case EntityTreeNodeType.Group:
                return Catalog;
            case EntityTreeNodeType.Entity:
                return CatalogEntity;
            case EntityTreeNodeType.LookupEntity:
                return CatalogLookupEntity;
            default:
                throw new Error(`Unknown type "${treeNodeType}"`);
        }
    }

    @action
    private onNodeSelect (node: ITreeNode) {
        // prohibiting the selection of groups
        if (!this.props.selectableGroups && node.type === EntityTreeNodeType.Group) {
            return;
        }

        let nodeKey = node.key,
            entity = EntityStoreManager.findEntity(
                nodeKey,
                Boolean(this.props.draftMode),
                this.getEntityType(node.type)
            );

        if (!entity) {
            throw new Error(`Entity "${node.key}" not found.`);
        }

        let selectedNodes = this.props.selectedNodes?.slice() || this.selectedNodes.slice(),
            selectedNodeIndex = selectedNodes.indexOf(nodeKey);

        if (this.props.multiSelect) {
            if (selectedNodeIndex !== -1) {
                if (this.props.selectedNodes) { // for managed values - do not change
                    selectedNodes.splice(selectedNodeIndex, 1);
                    this.selectedNodes = selectedNodes;
                }

                this.onDeselect(entity);
            } else {
                if (this.props.selectedNodes) { // for managed values - do not change
                    selectedNodes.push(nodeKey);
                    this.selectedNodes = selectedNodes;
                }

                this.onSelect(entity);
            }
        } else {
            if (selectedNodeIndex !== -1) {
                if (this.props.selectedNodes) { // for managed values - do not change
                    this.selectedNodes = [];
                }

                this.onDeselect(entity);
            } else {
                let oldNode = this.selectedNodes[0];

                if (oldNode) {
                    let oldEntity = EntityStoreManager.findEntity(
                        oldNode,
                        Boolean(this.props.draftMode)
                    );

                    if (oldEntity) {
                        this.onDeselect(oldEntity);
                    }
                }

                if (this.props.selectedNodes) { // for managed values - do not change
                    this.selectedNodes = [nodeKey];
                }

                this.onSelect(entity);
            }
        }
    }

    @computed
    get selectedEntities () {
        let result = [];

        for (let node of this.selectedNodes) {
            let entity = EntityStoreManager.findEntity(
                node,
                Boolean(this.props.draftMode)
            );

            if (!entity) {
                throw new Error(`Entity "${node}" not found.`);
            }

            result.push(entity);
        }

        return result;
    }

    onSelect (entity: EntityTreeType) {
        if (this.props.onEntitySelect) {
            this.props.onEntitySelect(entity, this.selectedEntities);
        }
    }

    onDeselect (entity: EntityTreeType) {
        if (this.props.onEntityDeselect) {
            this.props.onEntityDeselect(entity, this.selectedEntities);
        }
    }

    get isLoading () {
        return EntityStoreManager.isLoading(Boolean(this.props.draftMode));
    }

    componentDidUpdate (): void {
        this.store.setConfig(this.props);
    }

    render () {
        let selectedNodes = this.props.selectedNodes || this.selectedNodes.slice();

        return (
            <Tree
                onNodeSelect={this.onNodeSelect}
                columns={[{name: 'displayName', displayName: 'displayName'}]}
                nodes={this.store.nodes}
                selectedNodes={selectedNodes}
                isLoading={this.isLoading}
                searchText={this.props.searchText}
            />
        );
    }

}
