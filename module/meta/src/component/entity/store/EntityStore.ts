/**
 * Store with a list of registers/reference lists
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-24
 */

import {action, observable} from 'mobx';
import {Catalog, CatalogEntity, CatalogLookupEntity} from '../../../model';
import {EntityTreeType, EntityTreeTypeConstructor} from '../../../type';
import {EntityGroupService} from '../../../service';

export class EntityStore {

    private readonly draft: boolean;

    private loadingPromise: Promise<Catalog[]>;

    @observable
    private loading: boolean = false;

    @observable
    private firstLoaded: boolean = false;

    @observable
    private entityCatalogList: Catalog[] = [];

    constructor (draft: boolean) {
        this.draft = draft;
        this.onEntitiesLoaded = this.onEntitiesLoaded.bind(this);
        this.load = this.load.bind(this);
    }

    public isLoading () {
        return this.loading;
    }

    private isFirstLoaded () {
        return this.firstLoaded;
    }

    public getCatalogList (promised: true): Promise<Catalog[]>;
    public getCatalogList (): Catalog[];
    public getCatalogList (promised?: true): Catalog[] | Promise<Catalog[]> {
        // uploading with a promise
        if (promised) {
            if (this.isFirstLoaded()) {
                return this.loadingPromise;
            } else {
                return this.load();
            }
        }

        // we load it at the first request
        if (!this.isFirstLoaded() && !this.isLoading()) {
            setTimeout(this.load);
        }

        return this.entityCatalogList;
    }

    public reload () {
        this.load();
    }

    public findEntity (name: string): null | EntityTreeType;
    public findEntity<T extends EntityTreeTypeConstructor> (name: string, type?: T): null | InstanceType<T>;
    public findEntity (name: string, type?: EntityTreeTypeConstructor): null | EntityTreeType {
        return this.find(name, this.entityCatalogList, type);
    }

    private find (
        name: string,
        catalogs: Catalog[],
        type?: EntityTreeTypeConstructor
    ): null | EntityTreeType {
        let result: null | EntityTreeType = null;

        if (!catalogs.length) {
            return result;
        }

        for (let catalog of catalogs) {
            if ((!type || type === CatalogEntity)) {
                result = catalog.entities.find2(this.findFilter.bind(this, name));

                if (result) {
                    return result;
                }
            }

            if ((!type || type === CatalogLookupEntity)) {
                result = catalog.lookupEntities.find2(this.findFilter.bind(this, name));

                if (result) {
                    return result;
                }
            }

            if ((!type || type === Catalog) && catalog.groupName.getValue() === name) {
                return catalog;
            }
        }

        return result;
    }

    private findFilter (name: string, item: Catalog | CatalogEntity | CatalogLookupEntity): boolean {
        if (item instanceof Catalog) {
            return item.groupName.getValue() === name;
        } else {
            return item.name.getValue() === name;
        }
    }

    @action
    private load () {
        if (this.isLoading()) {
            return this.loadingPromise;
        }

        this.loading = true;

        this.loadingPromise = EntityGroupService
            .getEntityGroup(true, this.draft)
            .then(this.onEntitiesLoaded);

        return this.loadingPromise;
    }

    @action
    private onEntitiesLoaded (entityList: Catalog[]) {
        this.entityCatalogList = entityList;
        this.firstLoaded = true;
        this.loading = false;

        return entityList;
    }

}
