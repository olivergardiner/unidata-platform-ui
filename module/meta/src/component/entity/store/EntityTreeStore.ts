/**
 * Store for the registry/reference tree
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-24
 */

import {action, computed, observable} from 'mobx';
import {ITreeNode} from '@unidata/types';
import {TreeUtil} from '@unidata/uikit';
import {EntityStoreManager} from './EntityStoreManager';
import {IEntityTreeConfig} from '../IEntityTreeConfig';
import {EntityGroupUtils} from '../../../utils';
import {EntityTreeNodeType} from '../../../model';

export class EntityTreeStore {

    @observable
    private selectedNodes: string[] = [];

    // a set of properties to run computedTree
    @observable private showRoot: boolean | undefined = false;
    @observable private showEmptyGroups: boolean | undefined = false;
    @observable private hideEntities: boolean | undefined = false;
    @observable private hideLookupEntities: boolean | undefined = false;
    @observable private draftMode: boolean | undefined = false;
    @observable private searchText: string | undefined = undefined;

    constructor () {
        this.filterNodes = this.filterNodes.bind(this);
    }

    @action
    public setConfig (config: IEntityTreeConfig) {
        this.showRoot = config.showRoot;
        this.showEmptyGroups = config.showEmptyGroups;
        this.hideEntities = config.hideEntities;
        this.hideLookupEntities = config.hideLookupEntities;
        this.draftMode = config.draftMode;
        this.searchText = config.searchText;
    }

    @computed
    public get nodes (): ITreeNode[] {
        let entityList = EntityStoreManager.getCatalogList(Boolean(this.draftMode)),
            rootNode: ITreeNode,
            tree: ITreeNode[] = [];

        if (!entityList.length) {
            return tree;
        }

        rootNode = EntityGroupUtils.buildTreeNodeModel(
            entityList,
            this.filterNodes
        );

        // hiding empty groups
        if (!this.showEmptyGroups) {
            EntityGroupUtils.clearEmptyGroups(rootNode);
        }

        // sorting by display name
        if (rootNode.children) {
            tree = rootNode.children.sort(TreeUtil.sortTree);
        }

        // with or without a root node
        if (this.showRoot) {
            return [rootNode];
        } else {
            return tree;
        }
    }

    /**
     * Filtering nodes when building a tree
     */
    private filterNodes (node: ITreeNode) {
        // hiding reference books
        if (this.hideEntities &&
            node.type === EntityTreeNodeType.Entity
        ) {
            return false;
        }

        // hiding registries
        if (this.hideLookupEntities &&
            node.type === EntityTreeNodeType.LookupEntity
        ) {
            return false;
        }

        // filtering by searchText
        if (this.searchText &&
            node.type !== EntityTreeNodeType.Group
        ) {
            let searchText = this.searchText.toLowerCase();

            if (node.row?.name?.toLowerCase().indexOf(searchText) === -1 &&
                node.row?.displayName?.toLowerCase().indexOf(searchText) === -1
            ) {
                return false;
            }
        }

        return true;
    }

}
