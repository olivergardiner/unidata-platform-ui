/**
 * Static store with a list of registers/directories
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-24
 */

import {Catalog} from '../../../model';
import {EntityTreeType, EntityTreeTypeConstructor} from '../../../type';
import {EntityStore} from './EntityStore';

export class EntityStoreManager {

    private static store: EntityStore;
    private static draftStore: EntityStore;

    private static getStore (draft: boolean) {
        if (draft) {
            if (!this.draftStore) {
                this.draftStore = new EntityStore(true);
            }

            return this.draftStore;
        } else {
            if (!this.store) {
                this.store = new EntityStore(false);
            }

            return this.store;
        }
    }

    public static isLoading (draft: boolean) {
        return this.getStore(draft).isLoading();
    }

    public static getCatalogList (draft: boolean, promised: true): Promise<Catalog[]>;
    public static getCatalogList (draft: boolean): Catalog[];
    public static getCatalogList (draft: boolean, promised?: true): Catalog[] | Promise<Catalog[]> {
        if (promised) {
            return this.getStore(draft).getCatalogList(promised);
        } else {
            return this.getStore(draft).getCatalogList();
        }
    }

    public static reload (draft: boolean) {
        this.getStore(draft).reload();
    }

    public static isCatalog (entity: EntityTreeType): entity is Catalog {
        return entity instanceof Catalog;
    }

    public static getEntityName (entity: EntityTreeType): string {
        if (this.isCatalog(entity)) {
            return entity.groupName.getValue();
        } else {
            return entity.name.getValue();
        }
    }

    public static getEntityDisplayName (entity: EntityTreeType): string {
        if (this.isCatalog(entity)) {
            return entity.title.getValue();
        } else {
            return entity.displayName.getValue();
        }
    }

    public static findEntity (name: string, draft: boolean): null | EntityTreeType;
    public static findEntity<T extends EntityTreeTypeConstructor> (name: string, draft: boolean, type?: T): null | InstanceType<T>;
    public static findEntity (name: string, draft: boolean, type?: EntityTreeTypeConstructor): null | EntityTreeType {
        return this.getStore(draft).findEntity(name, type);
    }

}
