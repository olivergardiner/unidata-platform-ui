/**
 * Select for the registry / directory
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-24
 */
import * as React from 'react';
import {i18n} from '@unidata/core-app';
import {ISelectProps, Select} from '@unidata/uikit';
import {EntityStoreManager, EntityTree, IEntityTreeProps} from '../index';
import {EntityTreeType} from '../../type/index';
import {observer} from 'mobx-react';
import {action, computed, observable, runInAction} from 'mobx';
import * as styles from './EntitySelect.m.scss';
import {EntityTreeStore} from './store/EntityTreeStore';

interface IProps {
    value?: string | string[]; // for a managed component
    defaultValue?: string | string[]; // for an unmanaged component
}

type Props = IProps & Omit<IEntityTreeProps, 'multiSelect' | 'searchText' | 'selectedNodes'>;

type SelectProps = Pick<ISelectProps, 'mode' | 'size' | 'name' | 'disabled' | 'placeholder'>;

export type IEntitySelectProps = Props & SelectProps;

@observer
export class EntitySelect extends React.Component<IEntitySelectProps> {

    static defaultProps: IEntitySelectProps = {
        disabled: false,
        size: 'default',
        mode: 'default',
        showRoot: false,
        draftMode: false,
        hideEntities: false,
        hideLookupEntities: false,
        selectableGroups: false
    };

    store = new EntityTreeStore();

    selectElement: Select | null = null;

    @observable
    selectedNodes: string[] = [];

    @observable
    searchText: string;

    @observable
    dropdownVisible: boolean = false;

    constructor (props: IEntitySelectProps) {
        super(props);

        let selectedNodes = this.props.defaultValue;

        if (selectedNodes) {
            EntityStoreManager.getCatalogList(Boolean(this.props.draftMode));
            this.selectedNodes = Array.isArray(selectedNodes) ? selectedNodes : [selectedNodes];
        }

        this.dropdownRender = this.dropdownRender.bind(this);
        this.onDropdownMouseDown = this.onDropdownMouseDown.bind(this);
        this.updateValues = this.updateValues.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onEntitySelect = this.onEntitySelect.bind(this);
        this.onEntityDeselect = this.onEntityDeselect.bind(this);
        this.onDeselect = this.onDeselect.bind(this);
        this.onDropdownVisibleChange = this.onDropdownVisibleChange.bind(this);
        this.updateValuesMapper = this.updateValuesMapper.bind(this);
    }

    onEntitySelect (entity: EntityTreeType, selectedEntities: EntityTreeType[]) {
        if (this.props.onEntitySelect) {
            this.props.onEntitySelect(entity, selectedEntities);
        }

        if (!this.props.mode || this.props.mode === 'default') {
            this.setDropdownVisible(false);
        }

        this.updateValues(selectedEntities);
    }

    onEntityDeselect (entity: EntityTreeType, selectedEntities: EntityTreeType[]) {
        if (this.props.onEntityDeselect) {
            this.props.onEntityDeselect(entity, selectedEntities);
        }

        this.updateValues(selectedEntities);
    }

    @action
    updateValues (selectedEntities: EntityTreeType[]) {
        this.selectedNodes = selectedEntities.map(this.updateValuesMapper);
    }

    updateValuesMapper (entity: EntityTreeType) {
        return EntityStoreManager.getEntityName(entity);
    }

    onDropdownMouseDown (event: React.MouseEvent) {
        event.preventDefault();
    }

    @action
    onSearch (searchText: string) {
        this.searchText = searchText;
    }

    @action
    onDeselect (value: string) {
        this.selectedNodes.splice(this.selectedNodes.indexOf(value), 1);
    }

    get multiSelect () {
        return !(!this.props.mode || this.props.mode === 'default');
    }

    @action
    setDropdownVisible (visible: boolean) {
        this.dropdownVisible = visible;
    }

    onDropdownVisibleChange (visible: boolean) {
        this.setDropdownVisible(visible);
    }

    @computed
    get values () {
        return this.propsValues || this.selectedNodes.slice();
    }

    @computed
    get propsValues (): string[] | undefined {
        const value = this.props.value;

        if (!value) {
            return undefined;
        } else {
            return Array.isArray(value) ? value : [value];
        }
    }

    @computed
    get selectValue (): string[] {
        let draftMode = Boolean(this.props.draftMode),
            catalogs = EntityStoreManager.getCatalogList(draftMode),
            values = this.values,
            result = [];

        if (catalogs.length && !EntityStoreManager.isLoading(draftMode)) {
            for (let value of values) {
                let entity = EntityStoreManager.findEntity(value, draftMode);

                if (entity) {
                    result.push(EntityStoreManager.getEntityDisplayName(entity));
                } else {
                    throw new Error(`Entity "${value}" not found`);
                }
            }
        } else {
            result = values;
        }

        return result;
    }

    get entityTreeProps (): IEntityTreeProps {
        return {
            draftMode: this.props.draftMode,
            showRoot: this.props.showRoot,
            showEmptyGroups: this.props.showEmptyGroups,
            hideEntities: this.props.hideEntities,
            hideLookupEntities: this.props.hideLookupEntities,
            searchText: this.searchText,
            selectedNodes: this.values,
            defaultSelectedNodes: this.props.defaultSelectedNodes,
            selectableGroups: this.props.selectableGroups,
            multiSelect: this.multiSelect,
            onEntitySelect: this.onEntitySelect,
            onEntityDeselect: this.onEntityDeselect
        };
    }

    dropdownRender () {
        return (
            <div
                className={styles.dropdownContainer}
                onMouseDown={this.onDropdownMouseDown}
            >
                <EntityTree {...this.entityTreeProps} />
            </div>
        );
    }

    get selectProps (): ISelectProps {
        return {
            mode: this.props.mode,
            options: [],
            onDeselect: this.onDeselect,
            value: this.selectValue,
            placeholder: this.props.placeholder ?? i18n.t('module.meta>entityOrLookupEntity'),
            dropdownRender: this.dropdownRender,
            onSearch: this.onSearch,
            size: this.props.size,
            name: this.props.name,
            showSearch: true,
            disabled: this.props.disabled,
            loading: EntityStoreManager.isLoading(Boolean(this.props.draftMode)),
            onDropdownVisibleChange: this.onDropdownVisibleChange,
            isOpen: this.dropdownVisible
        };
    }

    setRef = (el: Select) => {
       this.selectElement = el;
    };

    focus () {
        if (this.selectElement !== null) {
            this.selectElement.focus();
        }
    }

    render () {
        return <Select {...this.selectProps} ref={this.setRef}/>;
    }

}
