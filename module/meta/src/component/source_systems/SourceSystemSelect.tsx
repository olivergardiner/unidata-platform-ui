/**
 * Select for the source system
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-26
 */
import * as React from 'react';
import {i18n} from '@unidata/core-app';
import {IOption, ISelectProps, Select, Value} from '@unidata/uikit';
import {observer} from 'mobx-react';
import {action, computed, observable} from 'mobx';
import {SourceSystemStoreManager} from './store/SourceSystemStoreManager';
import {SourceSystem} from '../../model';

interface IProps {
    onChange?: (value: string) => void;
}

type SelectProps = Pick<ISelectProps, 'mode' | 'size' | 'name' | 'disabled' | 'value'>;

export type ISourceSystemSelectProps = IProps & SelectProps;

@observer
export class SourceSystemSelect extends React.Component<ISourceSystemSelectProps> {

    static defaultProps: ISourceSystemSelectProps = {
        disabled: false,
        size: 'default',
        mode: 'default'
    };

    @observable
    selectedValue: Value | undefined = this.props.value;

    constructor (props: ISourceSystemSelectProps) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange (value: Value) {
        this.setValue(value);

        if (this.props.onChange) {
            this.props.onChange(value.toString());
        }
    }

    @action
    setValue (value: Value | undefined) {
        this.selectedValue = value;
    }

    @computed
    get selectOptions (): IOption[] {
        let list = SourceSystemStoreManager.getSourceSystemsList();

        return list.map(this.selectOptionsMapper);
    }

    selectOptionsMapper (sourceSystem: SourceSystem): IOption {
        let name = sourceSystem.name.getValue();

        return {
            value: name,
            title: name
        };
    }

    get selectProps (): ISelectProps {
        return {
            mode: this.props.mode,
            options: this.selectOptions,
            onChange: this.onChange,
            value: this.selectedValue,
            placeholder: i18n.t('module.meta>sourceSystemSelect>placeholder'),
            size: this.props.size,
            name: this.props.name,
            showSearch: true,
            disabled: this.props.disabled,
            loading: SourceSystemStoreManager.isLoading()
        };
    }

    render () {
        return <div><Select {...this.selectProps} /></div>;
    }

}
