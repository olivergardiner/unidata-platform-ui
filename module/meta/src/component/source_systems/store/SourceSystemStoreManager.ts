/**
 * Static store with a list of sources
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-24
 */

import {action, observable} from 'mobx';
import {SourceSystem} from '../../../model';
import {SourceSystemService} from '../../../service';

export class SourceSystemStoreManager {

    private static inited: boolean;

    @observable
    private static loading: boolean;

    @observable
    private static firstLoaded: boolean;

    @observable
    private static sourceSystems: SourceSystem[];

    public static init () {
        if (this.inited) {
            return;
        }

        this.load = this.load.bind(this);
        this.onLoaded = this.onLoaded.bind(this);
        this.loading = false;
        this.firstLoaded = false;
        this.sourceSystems = [];
        this.inited = true;
    }

    public static isLoading () {
        return  this.loading;
    }

    public static getSourceSystemsList (): SourceSystem[] {
        // we load it at the first request
        if (!this.firstLoaded && !this.loading) {
            setTimeout(this.load);
        }

        return this.sourceSystems;
    }

    public static reload () {
        this.load();
    }

    @action
    private static load () {
        if (this.loading) {
            return;
        }

        this.loading = true;

        SourceSystemService
            .getSourceSystemList()
            .then(this.onLoaded);
    }

    @action
    private static onLoaded (sourceSystems: SourceSystem[]) {
        this.firstLoaded = true;
        this.loading = false;
        this.sourceSystems = sourceSystems;
    }

}

SourceSystemStoreManager.init();
