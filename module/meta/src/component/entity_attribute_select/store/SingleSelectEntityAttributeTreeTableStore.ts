import {EntityAttributeTreeTableStore} from './EntityAttributeTreeTableStore';
import {action} from 'mobx';
import {ITreeNode} from '@unidata/uikit';

export class SingleSelectEntityAttributeTreeTableStore extends EntityAttributeTreeTableStore {
    @action
    public select (node: ITreeNode, allowSelect?: boolean, allowDeselect?: boolean) {
        this.clearSelected();
        super.select(node, allowSelect, allowDeselect);

    }
}
