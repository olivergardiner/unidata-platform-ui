import keyBy from 'lodash/keyBy';
import findIndex from 'lodash/findIndex';
import {action, IObservableArray, observable} from 'mobx';
import {AbstractTreeTableStore, coalesceDefined} from '@unidata/core-app';
import {ITreeNode} from '@unidata/uikit';
import {AttributeType} from '../../../type/AttributeType';
import {IStringKeyMap} from '@unidata/core';
import {AbstractAttribute} from '../../../model/attribute/AbstractAttribute';
import {Entity} from '../../../model/entity/Entity';
import {LookupEntity} from '../../../model/entity/LookupEntity';

export class EntityAttributeTreeTableStore extends AbstractTreeTableStore {
    protected modelCache: Map<string, AbstractAttribute> = new Map();
    protected nodesCache: Map<string, ITreeNode> = new Map();
    private entity: Entity | LookupEntity;
    private rootNode: ITreeNode;
    private showRoot: boolean;
    @observable public treeData: IObservableArray<ITreeNode> = observable.array([], {deep: false});
    @observable protected selectedModels: IObservableArray<AbstractAttribute> = observable.array([], {deep: false});
    @observable protected selectedNodes: IObservableArray<ITreeNode> = observable.array([], {deep: false});
    private hideAttributeFilter: any;
    private isSimpleAttributesHidden: boolean;
    private isComplexAttributesHidden: boolean;
    private isArrayAttributesHidden: boolean;

    constructor (showRoot?: boolean) {
        super();

        this.showRoot = showRoot !== undefined ? showRoot : false;
    }


    public getRootNode (): ITreeNode {
        return this.rootNode;
    }

    public setRootNode (rootNode: ITreeNode) {
        this.rootNode = rootNode;
    }

    private static buildPath (parentPath: string, name: string) {
        let path;

        path = parentPath ? parentPath + '.' + name : name;

        return path;
    }

    private static buildDisplayPath (parentDisplayPath: string[], name: string) {
        return parentDisplayPath.concat(name);
    }

    public transformData (record: Entity | LookupEntity, disabledNodes?: string []): ITreeNode [] {
        let disabledNodesAsMap = keyBy(disabledNodes || []),
            nodes = this.prepareTreeData(record, '', [], disabledNodesAsMap);

        if (!this.showRoot) {
            return nodes;
        }

        this.setRootNode({
            key: record.name.getValue(),
            expanded: true,
            children: nodes,
            row: {
                name: record.name.getValue(),
                displayName: record.displayName.getValue()
            }
        });

        return [this.getRootNode()];
    }

    private prepareTreeData (parent: any, parentPath: string, parentDisplayPath: string[], disabledNodesAsMap: IStringKeyMap) {
        let node: ITreeNode,
            record,
            nodes: ITreeNode[] = [],
            path,
            displayPath: string[],
            me = this,
            attributeName,
            attributeDisplayName;

        parentPath = parentPath || '';
        parentDisplayPath = parentDisplayPath || [];

        if (!parent) {
            return [];
        }

        function addSimpleAttributeToTree (record: AbstractAttribute, type: AttributeType) {
            let node;

            if (me.hideAttributeFilter && me.hideAttributeFilter(record)) {
                return;
            }

            attributeName = record.name.getValue();
            attributeDisplayName = record.displayName.getValue();
            path = EntityAttributeTreeTableStore.buildPath(parentPath, attributeName);
            displayPath = EntityAttributeTreeTableStore.buildDisplayPath(parentDisplayPath, attributeDisplayName);
            node = {
                key: path,
                children: null,
                expanded: false,
                disabled: Boolean(disabledNodesAsMap[path]),
                type: type,
                path: path,
                iconCls: 'simple-node-icon',
                row: {
                    name: record.name.getValue(),
                    displayName: record.displayName.getValue(),
                    order: record.order && record.order.getValue(),
                    displayPath: displayPath
                }
            };
            nodes.push(node);

            me.nodesCache.set(path, node);
            me.modelCache.set(path, record);
        }

        if (!me.isSimpleAttributesHidden &&
            parent.codeAttribute) {
            record = parent.codeAttribute;

            if (record !== null && record !== undefined) {
                addSimpleAttributeToTree(record, AttributeType.CodeAttribute);
            }
        }

        if (!me.isSimpleAttributesHidden &&
            parent.aliasCodeAttributes) {
            parent.aliasCodeAttributes.items.forEach(
                (record: AbstractAttribute) => addSimpleAttributeToTree(record, AttributeType.AliasCodeAttribute)
            );
        }

        if (!me.isSimpleAttributesHidden &&
            parent.simpleAttributes) {
            parent.simpleAttributes.items.forEach(
                (record: AbstractAttribute) => addSimpleAttributeToTree(record, AttributeType.SimpleAttribute)
            );
        }

        if (!me.isComplexAttributesHidden &&
            parent.complexAttributes) {
            parent.complexAttributes.items.forEach(function (record: any) {
                attributeName = record.name.getValue();
                attributeDisplayName = record.displayName.getValue();
                path = EntityAttributeTreeTableStore.buildPath(parentPath, attributeName);
                displayPath = EntityAttributeTreeTableStore.buildDisplayPath(parentDisplayPath, attributeDisplayName);

                node = {
                    key: path,
                    path: path,
                    type: AttributeType.ComplexAttribute,
                    expanded: false,
                    disabled: Boolean(disabledNodesAsMap[path]),
                    children: [],
                    row: {
                        name: record.name.getValue(),
                        displayName: record.displayName.getValue(),
                        order: record.order && record.order.getValue(),
                        displayPath: displayPath
                    }
                };

                if (record.nestedEntity !== undefined) {
                    node.children = me.prepareTreeData(record.nestedEntity, path, displayPath, disabledNodesAsMap);
                }

                nodes.push(node);
                me.nodesCache.set(path, node);
                me.modelCache.set(path, record);
            });
        }

        if (!me.isArrayAttributesHidden && parent.arrayAttributes) {
            parent.arrayAttributes.items.forEach(
                (record: AbstractAttribute) => addSimpleAttributeToTree(record, AttributeType.ArrayAttribute)
            );
        }

        nodes.sort(function (a: any, b: any) {
            return a.order - b.order;
        });

        return nodes;
    }

    @action
    public setData (data: Entity | LookupEntity, disabledNodes?: string []) {
        this.entity = data;
        this.modelCache.clear();
        this.treeData.replace(this.transformData(data, disabledNodes));
    }

    public getData () {
        return this.entity;
    }

    public getSelectedModels () {
        return this.selectedModels;
    }

    @action
    public clearSelected () {
        let selectedEntities = this.getSelectedModels();
        let selectedItems = this.getSelectedNodes();

        selectedEntities.splice(0);
        selectedItems.splice(0);
    }

    @action
    public select (node: ITreeNode, allowSelect?: boolean, allowDeselect?: boolean) {
        let attribute;

        allowSelect = coalesceDefined(allowSelect, true);
        allowDeselect = coalesceDefined(allowDeselect, true);

        if (!node.path) {
            return;
        }

        // choose nodes
        if (this.selectedNodes.indexOf(node) > -1 && allowDeselect) {
            this.selectedNodes.splice(this.selectedNodes.indexOf(node), 1);
        } else if (allowSelect) {
            this.selectedNodes.push(node);
        }

        this.modelCache.forEach(function (value: AbstractAttribute, key: string) {
            if (key === node.path) {
                attribute = value;
            }
        });

        if (!attribute) {
            return;
        }

        // choose models
        let index = findIndex(this.getSelectedModels(), attribute);

        if (index > -1 && allowDeselect) {
            this.getSelectedModels().splice(index, 1);
        } else if (allowSelect) {
            this.getSelectedModels().push(attribute);
        }
    }
}
