/**
 * Utils for metaPath
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {
    AbstractAttribute,
    AliasCodeAttribute,
    ArrayAttribute,
    CodeAttribute,
    ComplexAttribute,
    Entity,
    LookupEntity,
    NestedEntity,
    SimpleAttribute
} from '../model';
import {IStringKeyMap, ModelCollection, Nullable} from '@unidata/core';

export class UPathMetaUtil {
    public static DELIMITER = '.';

    public static buildAttributePathMap (metaRecord: Entity | LookupEntity | NestedEntity, rootPathParts: string [], filters?: any) {
        rootPathParts = rootPathParts || [];

        let pathMap: IStringKeyMap<AbstractAttribute> = this.buildSimpleAttributePathMap(metaRecord, rootPathParts, filters);

        pathMap = {...pathMap, ...this.buildArrayAttributePathMap(metaRecord, rootPathParts, filters)};

        if (metaRecord instanceof Entity || metaRecord instanceof NestedEntity) {
            let complexAttributes = metaRecord.complexAttributes.getRange();

            pathMap = this.buildAbstractAttributePathMap(complexAttributes, rootPathParts, filters, pathMap, this.DELIMITER);

            complexAttributes.forEach((complexAttr) => {
                let entity = complexAttr.nestedEntity;
                let localPathParts = [...rootPathParts, complexAttr.name.getValue()];

                if (entity) {
                    pathMap = {...pathMap, ...this.buildAttributePathMap(entity, localPathParts, filters)};
                }
            });
        }

        return pathMap;

    }

    private static buildSimpleAttributePathMap (entity: Entity | LookupEntity | NestedEntity, rootPathParts: string[], filters: any) {
        let pathMap: IStringKeyMap = {};
        let attributes: AbstractAttribute [] = entity.simpleAttributes.getRange();
        let aliasCodeAttributes = null;
        let localPathParts = [];
        let key;

        attributes.sort((attr: SimpleAttribute, attr2: SimpleAttribute) => {
            return attr.order.getValue() > attr2.order.getValue() ? 1 : -1;
        });

        rootPathParts = rootPathParts || [];

        if (entity instanceof LookupEntity) {
            aliasCodeAttributes = entity.aliasCodeAttributes.getRange();

            if (entity.codeAttribute) {
                let codeAttr = entity.codeAttribute;

                localPathParts = [...rootPathParts, codeAttr.name.getValue()];
                key = localPathParts.join(this.DELIMITER);
                pathMap[key] = codeAttr;
            }
        }

        pathMap = this.buildAbstractAttributePathMap(attributes, rootPathParts, filters, pathMap, this.DELIMITER);

        if (aliasCodeAttributes) {
            pathMap = this.buildAbstractAttributePathMap(aliasCodeAttributes, rootPathParts, filters, pathMap, this.DELIMITER);
        }

        return pathMap;
    }

    private static buildArrayAttributePathMap (entity: Entity | LookupEntity | NestedEntity, filters: any, rootPathParts: string[]) {
        let pathMap: IStringKeyMap = {};
        let attributes = entity.arrayAttributes.getRange();

        rootPathParts = rootPathParts || [];

        pathMap = this.buildAbstractAttributePathMap(attributes, rootPathParts, filters, pathMap, this.DELIMITER);

        return pathMap;
    }

    private static buildAbstractAttributePathMap (attributes: AbstractAttribute [],
                                                  rootPathParts: string [], filters: any, pathMap: IStringKeyMap, delimiter: string) {
        let localPathParts;
        let key;

        if (filters) {
            //ToDo implement
        }

        attributes.forEach((attr) => {
            localPathParts = [...rootPathParts, attr.name.getValue()];

            key = localPathParts.join(delimiter);
            pathMap[key] = attr;
        });

        if (filters) {
            // ToDo
        }

        return pathMap;
    }

    public static buildAttributePath (entity: Nullable<Entity | LookupEntity | NestedEntity>,
                                      attribute: AbstractAttribute, field: string, rootPathParts?: string [] | string) {
        let isContains: boolean = false;
        let path = null;
        let localPathParts: string [] = [];
        let stringArrayRootPathParts: string [] = [];

        if (!entity) {
            return [];
        }

        field = field || 'name';

        if (typeof rootPathParts === 'string') {
            stringArrayRootPathParts = rootPathParts.split(this.DELIMITER);
        }

        if (attribute instanceof CodeAttribute) {
            if (attribute === (entity as LookupEntity).codeAttribute) {
                isContains = true;
            }
        } else if (attribute instanceof AliasCodeAttribute) {
            isContains = this.attributeExists(attribute, (entity as LookupEntity).aliasCodeAttributes);
        } else if (attribute instanceof SimpleAttribute) {
            isContains = this.attributeExists(attribute, entity.simpleAttributes);
        } else if (attribute instanceof ComplexAttribute) {
            isContains = this.attributeExists(attribute, (entity as Entity).complexAttributes);
        } else if (attribute instanceof ArrayAttribute) {
            isContains = this.attributeExists(attribute, entity.arrayAttributes);
        }

        if (isContains) {
            let fieldModel = (attribute as any)[field];
            let fieldValue = fieldModel ? fieldModel.getValue() : '';

            path = [...stringArrayRootPathParts, fieldValue];
        } else if ((entity as Entity).complexAttributes) {
            (entity as Entity).complexAttributes
                .getRange()
                .forEach((complexAttr) => {

                    localPathParts = [...stringArrayRootPathParts, (complexAttr as any)[field].getValue()];

                    path = this.buildAttributePath(complexAttr.nestedEntity, attribute, field, localPathParts);

                    return !path;
                });
        }

        return path;
    }

    private static attributeExists (attribute: AbstractAttribute, attributes?: ModelCollection<AbstractAttribute>) {
        if (!attributes || !attribute) {
            return false;
        }

        const attr = attributes.getRange()
            .find((attr) => {
                return attr.name.getValue() === attribute.name.getValue();
            });

        return Boolean(attr);
    }

    public static findAttributeByPath (entity: Entity | LookupEntity | NestedEntity, path: string | string []) {
        let pathParts: string [];
        let nestedEntity: Nullable<Entity | LookupEntity | NestedEntity> = entity;
        let result;
        let found: AbstractAttribute | null = null;

        // split path string to an array of path parts
        if (typeof path === 'string') {
            pathParts = path.split(this.DELIMITER);
        } else {
            pathParts = path;
        }

        if (pathParts) {
            result = pathParts.every((pathPart: string, index: number) => {
                found = null;

                if (index < pathParts.length - 1) {
                    // iterate over complex attributes until last level
                    if (!nestedEntity) {
                        found = null;
                    } else {
                        found = this.findComplexAttributeByName(nestedEntity, pathPart);
                    }

                    nestedEntity = found ? (found as ComplexAttribute).nestedEntity : null;
                } else {
                    // find attribute on last level
                    if (!nestedEntity) {
                        found = null;
                    } else {
                        found = this.findAttributeByName(nestedEntity, pathPart);
                    }
                }

                return found;
            });
        }

        return result ? found : null;
    }

    private static findComplexAttributeByName (nestedEntity: Entity | LookupEntity | NestedEntity, name: string) {
        let complexAttributes: ComplexAttribute [];
        let attribute;

        if ((nestedEntity as Entity).complexAttributes) {
            complexAttributes = (nestedEntity as Entity).complexAttributes.getRange();
            attribute = complexAttributes.find((attr, index) => {
                return attr.name.getValue() === name;
            });
        }

        return attribute || null;
    }

    private static findAttributeByName (nestedEntity: Entity | LookupEntity | NestedEntity, pathPart: string) {
        let simpleAttributes: SimpleAttribute [];
        let attribute;

        if (nestedEntity.simpleAttributes) {
            simpleAttributes = nestedEntity.simpleAttributes.getRange();
            attribute = simpleAttributes.find((attr) => {
                return attr.name.getValue() === name;
            });
        }

        return attribute || null;
    }

    public static findAttributesByPaths (entity: Entity | LookupEntity | NestedEntity, paths: string []) {
        let attributes;

        attributes = paths.map((path) => {
            return this.findAttributeByPath(entity, path);
        });

        return attributes;
    }
}
