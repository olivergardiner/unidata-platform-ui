/**
 * A helper class for groups of directories/registers
 * @author: Sergey Shishigin
 * @date: 2018-09-21
 */

import concat from 'lodash/concat';
import each from 'lodash/each';
import {Catalog, CatalogAbstractEntity, CatalogEntity, CatalogLookupEntity, EntityTreeNodeType} from '../index';
import {Nullable} from '@unidata/core';
import {ITreeNode} from '@unidata/uikit';

export class EntityGroupUtils {
    private static DELIMITER = '.';
    private static ROOT = 'ROOT';

    private static IconType: any = {
        Entity: 'book',
        LookupEntity: 'profile'
    }

    /**
     * Build a model for the root node of the directory/registry group tree
     *
     * @param {Catalog[]} catalogs elements of a flat list of reference/registry groups
     * @param {Function} filter elements of a flat list of reference/registry groups
     * @return {ITreeNode} Root node of the directory/registry tree
     */
    // ToDo fix filters
    public static buildTreeNodeModel (catalogs: Catalog[], filterFn?: (node: ITreeNode) => boolean): ITreeNode {
        let rootTreeNodeModel: ITreeNode = {
            key: 'ROOT',
            children: [],
            disabled: true,
            expanded: true,
            row: {}
        };

        // going through the elements of the flat folder
        each(catalogs, (catalog: Catalog) => {
            let groupName = catalog.groupName.getValue(),
                entityTreeNode: Nullable<ITreeNode> = {} as ITreeNode,
                childrenEntityTreeNode: ITreeNode,
                parts = groupName.split(EntityGroupUtils.DELIMITER),
                path: string;

            parts.shift();
            // iterating through the nodes deep in accordance with groupName, creating nodes along the way in case of their absence
            entityTreeNode = rootTreeNodeModel;

            each(parts, (part: string, index: number, collection: string[]) => {
                path = collection.slice(0, index + 1).join(EntityGroupUtils.DELIMITER);

                if (entityTreeNode) {
                    // apply the filter, if it is set
                    if (!filterFn || (filterFn && filterFn(entityTreeNode))) {
                        entityTreeNode = this.getNextGroupTreeNode(entityTreeNode, part, path, filterFn);
                    }
                }
            });

            // check that the current node was not filtered earlier
            if (entityTreeNode) {
                // filling the current node with values
                this.fillGroupTreeNode(entityTreeNode, catalog);

                // filling the node with child entities entity
                each(catalog.entities.items, (entity: CatalogEntity) => {
                    path = concat(parts, entity.name.getValue()).join(EntityGroupUtils.DELIMITER);

                    if (entityTreeNode) {
                        childrenEntityTreeNode = this.createEntityTreeNode(entity, EntityTreeNodeType.Entity, path, entityTreeNode);

                        // apply the filter, if it is set
                        if (!filterFn || (filterFn && filterFn(childrenEntityTreeNode))) {
                            if (!entityTreeNode.children) {
                                entityTreeNode.children = [];
                            }

                            entityTreeNode.children.push(childrenEntityTreeNode);
                        }
                    }
                });

                // filling the node with child entities lookup Entity
                each(catalog.lookupEntities.items, (entity: CatalogLookupEntity) => {
                    path = concat(parts, entity.name.getValue()).join(EntityGroupUtils.DELIMITER);

                    if (entityTreeNode) {
                        childrenEntityTreeNode = this.createEntityTreeNode(entity, EntityTreeNodeType.LookupEntity, path, entityTreeNode);

                        // apply the filter, if it is set
                        if (!filterFn || (filterFn && filterFn(childrenEntityTreeNode))) {
                            if (!entityTreeNode.children) {
                                entityTreeNode.children = [];
                            }

                            entityTreeNode.children.push(childrenEntityTreeNode);
                        }
                    }
                });
            }
        });

        return rootTreeNodeModel;
    }

    public static clearEmptyGroups (node: ITreeNode) {
            let childLength = 0,
                children;

            if (!node) {
                return childLength;
            }

            children = node.children;

            if (children) {
                for (let i = children.length - 1; i >= 0; i--) {
                    if (children[i].type === EntityTreeNodeType.Group && !this.clearEmptyGroups(children[i])) {
                        children.splice(i, 1);
                    }
                }

                childLength = children.length;
            }

            return childLength;
    }

    /**
     * Fill the found node with values from a flat list item in the folder
     *
     * @param {ITreeNode} treeNodeModel
     * @param {Catalog} catalog
     */
    private static fillGroupTreeNode (treeNodeModel: ITreeNode, catalog: Catalog) {
        if (treeNodeModel.row) {
            treeNodeModel.row.name = catalog.groupName.getValue();
            treeNodeModel.row.displayName = catalog.title.getValue();
        }

    }

    /**
     * Get the next Entity TreeNode node relative to the current one
     * If the node is missing, create it
     *
     * @ * @param {Entity TreeNode} current Entity Group Tree Node Current node
     * @param {string} key the Key
     * @param {string} path Path to the next node
     * @param {Function} filter Path to the next node
     * @return {ITreeNode}
     */
    private static getNextGroupTreeNode (currentEntityGroupTreeNode: ITreeNode, key: string, path: string, filterFn?: (node: ITreeNode) => boolean): Nullable<ITreeNode> { // eslint-disable-line max-len
        let entityGroupTreeNode: Nullable<ITreeNode>,
            treeNode: ITreeNode;

        if (!currentEntityGroupTreeNode.children) {
            currentEntityGroupTreeNode.children = [];
        }

        entityGroupTreeNode = currentEntityGroupTreeNode.children.find(
            (t: ITreeNode) => t.path === key && t.type === EntityTreeNodeType.Group
        ) as ITreeNode;

        if (!entityGroupTreeNode) {
            let type = EntityTreeNodeType.Group;

            treeNode = {
                key: path + '_' + type,
                parent: currentEntityGroupTreeNode,
                path: path,
                type: type,
                children: [],
                disabled: true,
                expanded: true,
                row: {}
            };
            entityGroupTreeNode = treeNode;

            // apply the filter, if it is set
            if (!filterFn || (filterFn && filterFn(entityGroupTreeNode))) {
                currentEntityGroupTreeNode.children.push(entityGroupTreeNode);
            } else {
                entityGroupTreeNode = null;
            }
        }

        return entityGroupTreeNode;
    }

    /**
     * Create a directory/registry node
     *
     * @param {CatalogAbstractEntity} catalogEntity
     * @param {string} type
     * @param {string} path
     * @param {ITreeNode} parent
     * @return {ITreeNode}
     */
    private static createEntityTreeNode (
        catalogEntity: CatalogAbstractEntity,
        type: EntityTreeNodeType,
        path: string, parent: ITreeNode
    ): ITreeNode {
        let name = catalogEntity.name.getValue(),
            displayName = catalogEntity.displayName.getValue(),
            node: ITreeNode;

        node = {
            key: catalogEntity.name.getValue(),
            path: path,
            type: type,
            icon: this.IconType[type],
            parent: parent,
            row: {
                displayName: displayName,
                name: name
            },
            children: null,
            expanded: true
        };

        return node;
    }

    /**
     * The tree of registers / directories to a flat view of its elements (registers/directories)
     *
     * @param catalogs
     */
    public static toList (catalogs: Catalog[]): CatalogAbstractEntity[] {
        let list: CatalogAbstractEntity[] = [];

        catalogs.forEach((catalog: Catalog) => {
           catalog.entities.items.forEach((entity: CatalogAbstractEntity) => {
               list.push(entity);
           });

           catalog.lookupEntities.items.forEach((entity: CatalogAbstractEntity) => {
               list.push(entity);
           });
        });

        return list;
    }
}
