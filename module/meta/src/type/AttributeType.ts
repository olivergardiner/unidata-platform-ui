/**
 * Types of metamodel attributes.
 *
 * DEPRECATED!!!!
 *
 * @author: Viktor Magarlamov
 * @date: 2019-03-21
 *
 * @deprecated
 */
export enum AttributeType {
    SimpleAttribute = 'SimpleAttribute',
    AliasCodeAttribute = 'AliasCodeAttribute',
    CodeAttribute = 'CodeAttribute',
    ComplexAttribute = 'ComplexAttribute',
    ArrayAttribute = 'ArrayAttribute'
}
