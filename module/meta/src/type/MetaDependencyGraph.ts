/**
 * Types for the meta dependency graph
 *
 * @author: Denis Makarov
 * @date: 2020-03-05
 */

import {Vertex} from '../model/dependency_graph/Vertex';

export enum Status {                   // the presence of certain statuses and messages to them
    OK = 'OK',
    ERROR = 'ERROR',
    WARNING = 'WARNING'
}

export enum Existence {
    NEW = 'NEW',             // only available in imported data
    EXIST = 'EXIST',         // only available in the system
    UPDATE = 'UPDATE',       // it is present both in the system and in the imported data
    NOT_FOUND = 'NOT_FOUND'  // not anywhere
}

export enum Action {
    NONE = 'NONE',           // do nothing
    UPSERT = 'UPSERT'        // import
}

export enum MetaType {
    ENTITY = 'ENTITY',
    LOOKUP = 'LOOKUP',
    ENUM = 'ENUM',
    CLASSIFIER = 'CLASSIFIER',
    MEASURE = 'MEASURE',
    CUSTOM_CF = 'CUSTOM_CF',
    COMPOSITE_CF = 'COMPOSITE_CF',
    MATCH_RULE = 'MATCH_RULE',
    MERGE_RULE = 'MERGE_RULE',
    SOURCE_SYSTEM = 'SOURCE_SYSTEM',
    ZIP = 'ZIP',
    RELATION = 'RELATION',
    NESTED_ENTITY = 'NESTED_ENTITY',
    GROUPS = 'GROUPS'
}

export enum EdgeDirection {
    INBOUND = 'INBOUND',
    OUTBOUND = 'OUTBOUND'
}

export type ConnectedVertexes = {
    relation: Vertex [];
    lookup: Vertex [];
}

export type RightCoherenceWarning = {
    securedResource: string;
    relatedSecuredResource: string;
    rights: string [] | string;
    message: string;
}
