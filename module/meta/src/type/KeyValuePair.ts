/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-02-19
 */

import {AbstractModel, stringField, StringField} from '@unidata/core';
import {observable} from 'mobx';

export class KeyValuePair extends AbstractModel {
    @observable
    @stringField({primaryKey: true})
    public tempId: StringField;

    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public value: StringField;

}
