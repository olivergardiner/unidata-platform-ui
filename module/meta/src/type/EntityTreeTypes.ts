/**
 * Types for the model tree
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-27
 */
import {ClassCtor} from '@unidata/core';
import {Catalog, CatalogEntity, CatalogLookupEntity} from '../model';

export type EntityTreeType = Catalog | CatalogEntity | CatalogLookupEntity;

export type EntityTreeTypeConstructor = ClassCtor<EntityTreeType>;
