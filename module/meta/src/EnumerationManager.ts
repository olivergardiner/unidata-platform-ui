/**
 * Manager for storing enum
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */
import {Enumeration} from './model/enumeration/Enumeration';

export class EnumerationManager {

    private static enum: {[key: string]: Enumeration} = {};

    public static getEnum (name: string): Enumeration | undefined {
        return EnumerationManager.enum[name];
    }

    public static setEnum (name: string, enumData: Enumeration): void {
        if (EnumerationManager.enum[name]) {
            console.warn(`You will replace enum ${name}:`, {...EnumerationManager.enum[name]}, 'with new:', enumData);
        }

        EnumerationManager.enum[name] = enumData;
    }
}
