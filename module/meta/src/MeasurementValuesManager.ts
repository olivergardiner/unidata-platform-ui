/**
 * Manager for storing measurement values
 *
 * @author: Aleksandr Bavin
 * @date: 2020-04-28
 */

import {MeasurementValue} from './model/measurement/MeasurementValue';

export class MeasurementValuesManager {

    private static measurementValue: {[key: string]: MeasurementValue} = {};

    public static getMeasurementValue (id: string): MeasurementValue | undefined {
        return MeasurementValuesManager.measurementValue[id];
    }

    public static setMeasurementValue (measurement: MeasurementValue): void {
        const id = measurement.id.getValue();

        if (MeasurementValuesManager.measurementValue[id]) {
            console.warn(`You will replace enum ${measurement.name.getValue()}:`, {...MeasurementValuesManager.measurementValue[id]}, 'with new:', measurement);
        }

        MeasurementValuesManager.measurementValue[id] = measurement;
    }

}
