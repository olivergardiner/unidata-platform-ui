import {IModule} from '@unidata/core';
import {menuItems, pages} from './page';
import * as EntityWorkflowAssignment from './uemodule/entity_workflow_assignment/EntityWorkflowAssignment';
import * as EntityMatchingParameter from './uemodule/matching_parameter/EntityMatchingParameter';
import * as EntityMatchingMapping from './uemodule/matching_mapping/EntityMatchingMapping';
import * as EntityList from './uemodule/matching_entity_list/EntityList';
import * as LoadMetaRecord from './uemodule/load_meta_record/LoadMetaRecord';
import {AppDataLoaderMeta} from './uemodule/app_data_loader_meta/AppDataLoaderMeta';

export * from './model';

export * from './component';

export * from './service';

export * from './utils';

export * from './type';

export * from './component';

export {EnumerationManager} from './EnumerationManager';

export {MeasurementValuesManager} from './MeasurementValuesManager';

export {MetaModelDraft} from './MetaModelDraft';

export {EntityGroupHelper} from './_deprecated/helper/EntityGroupHelper';

export function init (): IModule {
    return {
        id: 'meta',
        uemodules: [
            EntityWorkflowAssignment,
            EntityMatchingParameter,
            EntityList,
            EntityMatchingMapping,
            LoadMetaRecord,
            AppDataLoaderMeta,
            ...pages,
            ...menuItems]
    };
}
