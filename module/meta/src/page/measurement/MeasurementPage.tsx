﻿/**
 * The "Units of measurement" screen"
 *
 * @author Ivan Marshalkin
 * @date 2020-02-12
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {MeasurementPageStore} from './store/MeasurementPageStore';
import {ExtWidget, Layout} from '@unidata/uikit';

interface IProps {}

interface IInjectProps extends IProps {
}

@observer
export class MeasurementPage extends React.Component<IProps> {
    store: MeasurementPageStore = new MeasurementPageStore();
    providerProps: any;

    constructor (props: IProps) {
        super(props);

        // the provider must pass the same props, otherwise mobx throws an exception
        // @see https://stackoverflow.com/questions/43550137/mobx-rerender-after-assign/43562824
        this.providerProps = {
            pageStore: this.store
        };
    }

    get injected () {
        return this.props as IInjectProps;
    }

    render () {
        return (
            <Provider {...this.providerProps}>
                <Layout>
                    <Layout.Content>
                        <ExtWidget
                            autoDestroy={true}
                            createWidget={function () {
                                let view;

                                view = window.Ext.widget({
                                    xtype: 'admin.measurement',
                                    height: '100%',
                                    width: '100%'
                                });

                                return view;
                            }}
                            style={{
                                width: '100%',
                                height: '100%'
                            }}
                        >
                        </ExtWidget>

                    </Layout.Content>
                </Layout>
            </Provider>
        );
    }
}
