import {UeModuleTypeCallBack, UeModuleTypeComponent} from '@unidata/core';
import {MetaModelPage} from './metamodel/MetaModelPage';
import {SchemaPage} from './schema/SchemaPage';
import {SourceSystemsPage} from './sourcesystems/SourceSystemsPage';
import {FunctionPage} from './function/FunctionPage';
import {MeasurementPage} from './measurement/MeasurementPage';
import {EnumeraionPage} from './enumeration/EnumeraionPage';
import {AppTypeManager} from '@unidata/core-app';

export const menuItems = [
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'metamodelMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'metamodel',
                route: '/metamodel',
                icon: 'puzzle',
                groupName: 'model',
                activeInDraftMode: true,
                order: 'last'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'schemaMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'schema',
                route: '/schema',
                icon: 'site-map',
                groupName: 'model',
                order: 'last'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'sourcesystemsMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'sourcesystems',
                route: '/sourcesystems',
                icon: 'cable2',
                groupName: 'model',
                activeInDraftMode: true,
                order: 'last'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'functionsMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'functions',
                route: '/functions',
                icon: 'cog',
                groupName: 'model',
                activeInDraftMode: true,
                order: 'last'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'measurementMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'measurement',
                route: '/measurement',
                icon: 'ruler',
                groupName: 'model',
                activeInDraftMode: true,
                order: 'last'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'enumerationsMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'enumerations',
                route: '/enumerations',
                icon: 'list3',
                groupName: 'model',
                activeInDraftMode: true,
                order: 'last'
            }
        }
    }
];

export const pages = [
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'metamodel',
            active: true,
            system: false,
            component: MetaModelPage,
            resolver: () => {
                return AppTypeManager.isDataAdmin();
            },
            meta: {
                route: '/metamodel'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'schema',
            active: true,
            system: false,
            component: SchemaPage,
            resolver: () => {
                return AppTypeManager.isDataAdmin();
            },
            meta: {
                route: '/schema'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'sourcesystems',
            active: true,
            system: false,
            component: SourceSystemsPage,
            resolver: () => {
                return AppTypeManager.isDataAdmin();
            },
            meta: {
                route: '/sourcesystems'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'functions',
            active: true,
            system: false,
            component: FunctionPage,
            resolver: () => {
                return AppTypeManager.isDataAdmin();
            },
            meta: {
                route: '/functions'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'measurement',
            active: true,
            system: false,
            component: MeasurementPage,
            resolver: () => {
                return AppTypeManager.isDataAdmin();
            },
            meta: {
                route: '/measurement'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'enumerations',
            active: true,
            system: false,
            component: EnumeraionPage,
            resolver: () => {
                return AppTypeManager.isDataAdmin();
            },
            meta: {
                route: '/enumerations'
            }
        }
    }
];
