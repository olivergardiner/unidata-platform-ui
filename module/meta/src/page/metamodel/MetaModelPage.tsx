﻿/**
 * Model "Editor screen"
 *
 * @author Ivan Marshalkin
 * @date 2020-02-12
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {MetaModelPageStore} from './store/MetaModelPageStore';
import {ExtWidget, Layout} from '@unidata/uikit';

interface IProps {}

interface IInjectProps extends IProps {
}

@observer
export class MetaModelPage extends React.Component<IProps> {
    store: MetaModelPageStore = new MetaModelPageStore();
    providerProps: any;
    extWidgetRef: any;

    constructor (props: IProps) {
        super(props);

        // the same props must be passed to the provider, otherwise mobs throws an exception
        // @see https://stackoverflow.com/questions/43550137/mobx-rerender-after-assign/43562824
        this.providerProps = {
            pageStore: this.store
        };

        this.extWidgetRef = React.createRef();
    }

    get injected () {
        return this.props as IInjectProps;
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        const widget = this.extWidgetRef.current.getWidget();

        if (widget) {
            return widget.wikiPage();
        }

        return null;
    }


    render () {
        return (
            <Provider {...this.providerProps}>
                <Layout>
                    <Layout.Content>
                        <ExtWidget
                            ref={this.extWidgetRef}
                            autoDestroy={true}
                            createWidget={function () {
                                let view;

                                view = window.Ext.widget({
                                    xtype: 'admin.entity.layout',
                                    height: '100%',
                                    width: '100%'
                                });

                                return view;
                            }}
                            style={{
                                width: '100%',
                                height: '100%'
                            }}
                        >
                        </ExtWidget>

                    </Layout.Content>
                </Layout>
            </Provider>
        );
    }
}
