/**
 * UI UE ENTITY_LIST
 * list of registries and reference lists for the matching settings page
 *
 * @author Denis Makarov
 * @date 2020-03-19
 */
import {UeModuleTypeComponent} from '@unidata/core';
import {EntityListStore} from './store/EntityListStore';
import {EntityTreeTable} from './../../component/entity_select/EntityTreeTable';

export default {
    type: UeModuleTypeComponent.ENTITY_LIST,
    moduleId: 'entity-list',
    active: true,
    system: false,
    resolver: () => true,
    meta: {
        storeCtor: EntityListStore
    },
    component: EntityTreeTable
};
