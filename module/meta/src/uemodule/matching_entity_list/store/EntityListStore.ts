/**
 * Store a list of registries/reference lists
 *
 * @author Sergey Shishigin
 * @date 2019-09-19
 */

import {ICatalogEntity, IEntityListStore, ITreeNode} from '@unidata/types';
import {EntityTreeTableStore} from '../../../component/entity_select/store/EntityTreeTableStore';
import {EntityGroupService} from '../../../service/EntityGroupService';
import {observable} from 'mobx';
import {CatalogAbstractEntity} from '../../../model/entity/CatalogAbstractEntity';

export class EntityListStore implements IEntityListStore {
    @observable
    public entityTreeTableStore: EntityTreeTableStore = new EntityTreeTableStore();

    public selectEntity (treeNode: ITreeNode) {
        this.entityTreeTableStore.select(treeNode);
    }

    public loadEntityTree () {
        let me = this;

        return EntityGroupService.getEntityGroup().then((entityGroups) => {
            me.entityTreeTableStore.setData(entityGroups);
        });
    }

    public getSelectedEntity () {
        let selectedModels = this.entityTreeTableStore.getSelectedModels();

        if (!selectedModels) {
            return null;
        }

        if (selectedModels[0] instanceof CatalogAbstractEntity) {
            return selectedModels[0] as ICatalogEntity;
        }

        return selectedModels[0];
    }

    public getDisplayPath () {
        return this.entityTreeTableStore.getDisplayPath();
    }

    public getTreeTableStore () {
        return this.entityTreeTableStore;
    }

}
