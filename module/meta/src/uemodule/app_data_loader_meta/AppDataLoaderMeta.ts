/**
 * Initializing the app's meta data
 *
 * @author: Aleksandr Bavin
 * @date: 2020-04-27
 */

import {UeModuleTypeCallBack} from '@unidata/core';
import {EnumerationManager, EnumerationService, MeasurementValuesManager} from '../..';
import {MeasurementValuesService} from '../../service/MeasurementValuesService';

export const AppDataLoaderMeta = {
    'default': {
        type: UeModuleTypeCallBack.APP_DATA_LOADER,
        moduleId: 'meta',
        active: true,
        system: false,
        fn: () => {
            // loading transfers
            let enumerationsPromise = EnumerationService.getEnumerations().then(function (data) {
                for (let dataItem of data) {
                    EnumerationManager.setEnum(dataItem.name.getValue(), dataItem);
                }
            });

            // download units of measurement
            let measurementValuesPromise = MeasurementValuesService.getMeasurementValues().then(function (data) {
                for (let dataItem of data) {
                    MeasurementValuesManager.setMeasurementValue(dataItem);
                }
            });

            return Promise.all([
                enumerationsPromise,
                measurementValuesPromise
            ]);
        },
        resolver: () => true,
        meta: {}
    }
};
