/**
 * UI UE MATCHING_MAPPING
 * panel with mapping settings for matching fields for the registry / directory attribute
 *
 * @author Denis Makarov
 * @date 2020-03-19
 */
import {UeModuleTypeComponent} from '@unidata/core';
import {i18n} from '@unidata/core-app';
import {MatchingParameterType} from '@unidata/types';
import {EntityMatchingMappingStore} from './store/EntityMatchingMappingStore';

export default {
    type: UeModuleTypeComponent.MATCHING_MAPPING,
    moduleId: 'matching-mapping',
    active: true,
    system: false,
    resolver: () => true,
    meta: {
        title: () => i18n.t('matching>attributes'),
        newAttributeText: () => i18n.t('matching>addEntityAttribute'),
        storeCtor: EntityMatchingMappingStore,
        matchingParameterTypes: [MatchingParameterType.ENTITY_ATTRIBUTE]
    }
};
