/**
 * Store for a panel displaying matching fields
 *
 * @author Denis Makarov
 * @date 2020-03-19
 */

import {action, computed, observable} from 'mobx';
import {
    IEntity,
    IMatchingFieldData,
    IMatchingMappingStoreAccessor,
    IMatchingMappingTypeStore,
    IMatchingParameterItem,
    MatchingParameterType
} from '@unidata/types';
// eslint-disable-next-line max-len
import {SingleSelectEntityAttributeTreeTableStore} from '../../../component/entity_attribute_select/store/SingleSelectEntityAttributeTreeTableStore';
import {AbstractStore} from '@unidata/core';
import {Entity} from '../../../model/entity/Entity';
import {LookupEntity} from '../../../model/entity/LookupEntity';
import {ITreeNode} from '@unidata/uikit';

export class EntityMatchingMappingStore extends AbstractStore implements IMatchingMappingTypeStore {
    @observable
    public entity: IEntity | null;

    public entityAttributeTreeTableStore = new SingleSelectEntityAttributeTreeTableStore();

    private matchingMappingStoreAccessor: IMatchingMappingStoreAccessor;

    constructor (entity: IEntity, matchingMappingStoreAccessor: IMatchingMappingStoreAccessor) {
        super();

        this.entity = entity;
        this.matchingMappingStoreAccessor = matchingMappingStoreAccessor;
    }

    @computed
    public get selectedMatchingFields (): string [] {
        return this.matchingMappingStoreAccessor.getMatchingMappingsData()
            .map((data: IMatchingFieldData) => {
                return data.fieldPath;
            });
    }

    @computed
    public get canCreateMatchingMapping () {
        return true;
    }

    @action
    public handleResetMatchingFieldValue (matchingFieldType: string) {
    }

    public initStore () {
        return Promise.resolve(this.updateEntityAttributesTreeStoreData());
    }

    public updateEntityAttributesTreeStoreData () {
        if (this.entity) {
            this.entityAttributeTreeTableStore.setData(this.entity as Entity | LookupEntity);
        }
    }

    @action
    public createDefaultMatchingMappings (existingMappings: Map<string, number>) {
        const entityAttributeNum = existingMappings.get(MatchingParameterType.ENTITY_ATTRIBUTE);

        if (!entityAttributeNum || entityAttributeNum === 0) {
            this.matchingMappingStoreAccessor.createNewMatchingMapping(MatchingParameterType.ENTITY_ATTRIBUTE);
        }
    }

    public getTreeStore (type: string) {
        return this.entityAttributeTreeTableStore;
    }

    public canRemoveMatchingMapping (type: string, index: number, allMappingsTypes: string[]) {
        return index > 0;
    }

    public handleAddNewMatchingMapping () {
        this.matchingMappingStoreAccessor.createNewMatchingMapping(MatchingParameterType.ENTITY_ATTRIBUTE);
    }

    public handleAddFieldMapping (mappingType: string, fieldValue: string) {
    }

    public isMatchingValueHighlighted (node: ITreeNode, matchingParameterType: string, matchingParameters: IMatchingParameterItem []) {
        let type: MatchingParameterType,
            matchingParameter,
            result: boolean = false;

        if (!matchingParameterType) {
            return false;
        }

        if (matchingParameters) {
            matchingParameter = matchingParameters.find((matchingParameter: IMatchingParameterItem) => {
                let result: boolean;

                if (matchingParameter.type !== matchingParameterType) {
                    return false;
                }

                return matchingParameter.path === node.path;
            });
        }

        result = Boolean(matchingParameter);

        return result;
    }
}

