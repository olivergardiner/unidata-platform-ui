/**
 * Component for selecting a matching parameter in the form of a directory attribute tree
 *
 * @author Denis Makarov
 * @date 2020-03-19
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {Checkbox, ITreeColumn, Tree} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import {ITreeNode, MatchingEntityTreeProps} from '@unidata/types';
import {EntityAttributeTreeTableStore} from '../../../component/entity_attribute_select/store/EntityAttributeTreeTableStore';

import * as styles from './matchingEntityAttributeTreeTable.m.scss';

@observer
export class MatchingEntityAttributeTreeTable extends React.Component<MatchingEntityTreeProps> {
    constructor (props: MatchingEntityTreeProps) {
        super(props);

        this.onNodeSelect = this.onNodeSelect.bind(this);
    }

    get store () {
        return this.props.treeTableStore as EntityAttributeTreeTableStore;
    }

    columns: ITreeColumn [] = [
        {
            name: 'displayName',
            displayName: i18n.t('common:displayName'),
            renderer: (node: ITreeNode) => {
                if (node.selected) {
                    return <span className={styles.selected}>
                        <span className={'icon-power'}/>
                        <span>{node.row['displayName']}</span>
                    </span>;
                }

                return node.row['displayName'];
            }
        },
        {
            name: 'selected',
            renderer: (node: ITreeNode) => {
                const selected = node.selected || false;

                const select = (e: any) => {
                    this.onNodeSelect(node);
                };

                if (!node.path) {
                    return false;
                }

                return <Checkbox onChange={select} onClick={(e) => e.stopPropagation()} checked={selected}/>;
            },
            displayName: ''
        }
    ];

    onNodeSelect (node: ITreeNode) {
        let selectionAllowed = true;

        if (this.props.onBeforeSelect) {
            selectionAllowed = this.props.onBeforeSelect(this.store.getSelectedNodes());
        }

        this.store.select(node, selectionAllowed);

        if (this.props.onSelect) {
            this.props.onSelect(this.store.getSelectedNodes());
        }
    }


    render () {
        const treeTableStore = this.store;

        return (
            <Tree nodes={treeTableStore.treeData.slice()}
                  selectedNodes={treeTableStore.getSelectedKeys()}
                  columns={this.columns}
                  onNodeSelect={this.onNodeSelect}/>
        );
    }
}
