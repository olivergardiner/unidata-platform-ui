/**
 * Loader for directory attributes used as matching parameters
 *
 * @author Denis Makarov
 * @date 2020-03-19
 */

import {ITreeNode} from '@unidata/uikit';
import {IEntity, IMatchingParameterItem, IMatchingParameterLoader} from '@unidata/types';

export class EntityMatchingParameterLoader implements IMatchingParameterLoader {

    constructor () {
        this.transformNodes = this.transformNodes.bind(this);
        this.loadMatchingParameters = this.loadMatchingParameters.bind(this);
    }

    public transformNodes (nodes: ITreeNode []) {
        return nodes.map((node: ITreeNode)=> {
            let displayPath: string [] = [];
            let matchingParameterData: IMatchingParameterItem;

            if (node.row && node.row.displayPath) {
                displayPath = node.row.displayPath as string [];
            }

            matchingParameterData = {
                type: 'ENTITY_ATTRIBUTE',
                path: node.path || '',
                displayName: displayPath
            };

            return matchingParameterData;
        });
    }

    public loadMatchingParameters (entity: IEntity) {
        return Promise.resolve(entity);
    }


}
