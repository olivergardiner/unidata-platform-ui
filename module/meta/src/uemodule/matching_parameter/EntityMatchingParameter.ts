/**
 * UI UE MATCHING_PARAMETER
 * component for selecting a matching parameter
 *
 * @author Denis Makarov
 * @date 2020-03-19
 */
import {UeModuleTypeComponent} from '@unidata/core';
import {i18n} from '@unidata/core-app';
import {EntityAttributeTreeTableStore} from '../../component/entity_attribute_select/store/EntityAttributeTreeTableStore';
import {EntityMatchingParameterLoader} from './store/EntityMatchingParameterLoader';
import {MatchingEntityAttributeTreeTable} from './entity_attribute_table/MatchingEntityAttributeTreeTable';
import {MatchingParameterType} from '@unidata/types';

const loader = new EntityMatchingParameterLoader();

export default {
    type: UeModuleTypeComponent.MATCHING_PARAMETER,
    moduleId: 'matching-parameter',
    active: true,
    system: false,
    resolver: () => true,
    meta: {
        menuKey: 'entity',
        menuText: () => i18n.t('module.meta>entitiesOrLookupEntities'),
        matchingParameterDisplayTypes: {
            ENTITY_ATTRIBUTE: () => i18n.t('matching>entityAttributeType')
        },
        matchingParameterTypes: [
            MatchingParameterType.ENTITY_ATTRIBUTE
        ],
        treeStore: new EntityAttributeTreeTableStore(),
        loader: loader
    },
    component: MatchingEntityAttributeTreeTable

};
