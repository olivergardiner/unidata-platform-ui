/**
 * Panel for adding a registry / reference list for configuring the workflow
 *
 * @author Sergey Shishigin
 * @date 2019-06-26
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {Button, Field, INTENT, TreeNodeType, Col, Row} from '@unidata/uikit';
import {computed} from 'mobx';
import {WorkflowAssignmentEntityStore} from './WorkflowAssignmentEntityStore';
import {IWorkflowAddProps, WorkflowTypeCode} from '@unidata/types';
import {EntityTreeNode} from '../../model/entity/tree_node/EntityTreeNode';

interface IProps {
    store: WorkflowAssignmentEntityStore;
}

@observer
export class WorkflowAssignmentEntityAddPanel extends React.Component<IProps & IWorkflowAddProps> {
    constructor (props: IProps & IWorkflowAddProps) {
        super(props);
        this.handleEntityItemAdd = this.handleEntityItemAdd.bind(this);
        this.filterAssignmentEntities = this.filterAssignmentEntities.bind(this);
    }

    get store () {
        return this.props.store;
    }

    get isAddButtonDisabled () {
        return this.store.selectedEntityName === null;
    }

    handleOnChange = (value: string) => {
        if (value === undefined) {
            this.store.handleEntitySelect(null);
        }
    }

    get selectedEntityName (): string | null {
        return this.store.selectedEntityName;
    }

    get handleEntitySelect () {
        return this.store.handleEntitySelect.bind(this.store);
    }

    handleEntityItemAdd () {
        const selectedEntityName = this.selectedEntityName,
                defaultAssignmentType = 'ALL',
                defaultAssignmentProcessType = WorkflowTypeCode.RECORD_EDIT;

        if (selectedEntityName) {
            this.store.handleEntityItemAdd();
            this.props.handleItemAdd(selectedEntityName, defaultAssignmentType, defaultAssignmentProcessType);
        }
    }

    @computed
    get treeData () {
        return this.store.buildTreeData(this.props.assignmentsHashByEntityName, this.filterAssignmentEntities);
    }

    /**
     * we leave only reference lists and registries that are included in the assignableEntities list,
     * * which forms the backend
     * This list does not currently include registries that are CONTAINS relationships
     * @param {EntityTreeNode} node
     * @param {string[]} assignableNames
     * @returns {boolean}
     */
    private filterAssignmentEntities (node: TreeNodeType): boolean {
        let result: boolean = true,
            name = node.value;

        if (this.props.assignableNames) {
            if (node.leaf) {
                result = Boolean(this.props.assignableNames.find((allowName: string) => {
                    return allowName === name;
                }));
            }
        }

        return result;
    }

    render () {
        const {handleItemAdd, readOnly} = this.props;

        if (this.treeData.length === 0) {
            return null;
        }

        return (
            <Row
                className='add-workflow-process-panel'
                data-qaid={this.props['data-qaid']}
            >
                <Col className='button-column' span={3} >
                    <Button
                        name='addProcess'
                        isDisabled={true}
                        isFilled={true}
                    >
                        {i18n.t('admin.workflowprocess>selectEntityText')}
                    </Button>
                </Col>
                <Col className='select-column' span={18}>
                    <Field.TreeSelect
                        placeholder={i18n.t('admin.workflowprocess>entityOrLookupEntity')}
                        options={this.treeData}
                        disabled={readOnly}
                        onSelect={this.handleEntitySelect}
                        onChange={this.handleOnChange}
                        value={this.selectedEntityName}
                        labelWidth={0}
                        inputWidth={100}
                        allowClear={true}
                    />
                </Col>
                <Col className='button-column' span={3} >
                     <Button
                         onClick={this.handleEntityItemAdd}
                         name='addProcess'
                         isDisabled={readOnly || this.isAddButtonDisabled}
                         intent={INTENT.PRIMARY}
                         isFilled={true}
                     >
                         {i18n.t('admin.workflowprocess>addWfProcessButtonText')}
                     </Button>
                </Col>
            </Row>
        );
    }
}
