/**
 *  Star for business processes of directories/registries
 *
 * @author Sergey Shishigin
 * @date 2020-03-04
 */
import {action, observable, ObservableMap} from 'mobx';
import {IWorkflowAssignmentAddStore, WorkflowTypeCategory, WorkflowTypeCode} from '@unidata/types';
import {IStringKeyMap} from '@unidata/core';
import {Catalog} from '../../model/entity/Catalog';
import {CatalogAbstractEntity} from '../../model/entity/CatalogAbstractEntity';
import {Entity} from '../../model/entity/Entity';
import {EntityGroupHelper} from '../../_deprecated/helper/EntityGroupHelper';
import {EntityGroupService} from '../../service/EntityGroupService';
import {EntityGroupUtils} from '../../utils/EntityGroupUtils';
import {EntityTreeNode} from '../../model/entity/tree_node/EntityTreeNode';
import {TreeNodeType} from '@unidata/uikit';

export class WorkflowAssignmentEntityStore implements IWorkflowAssignmentAddStore {
    public entityCatalogs: Catalog[] = [];
    public entityList: CatalogAbstractEntity[] = [];
    @observable public rootTreeNodeModel: EntityTreeNode = EntityGroupHelper.buildTreeNodeModel([]);
    private allEntityNodes: {[name: string]: any} = {};
    @observable public selectedEntityName: string | null = null;

    constructor () {
        this.filterMetaObject = this.filterMetaObject.bind(this);
        this.handleEntityItemAdd = this.handleEntityItemAdd.bind(this);
        this.handleEntitySelect = this.handleEntitySelect.bind(this);
    }

    public loadAll (): Promise<Catalog[]> {
        let p: Promise<Catalog[]>;

        p = EntityGroupService.getEntityGroup().then((catalogs: Catalog[]) => {
            this.entityCatalogs = catalogs;
            this.entityList = EntityGroupUtils.toList(catalogs);
            this.rootTreeNodeModel = this.buildRootTreeNode(catalogs);

            return Promise.resolve(catalogs);
        });

        return p;
    }

    public buildTreeData (assignmentsHashByEntityName: ObservableMap, filterFn?: (node: TreeNodeType) => boolean): TreeNodeType[] {
        const icons = {
            Entity: 'book',
            LookupEntity: 'profile'
        } as {[key: string]: 'book' | 'profile'};

        this.allEntityNodes = {};

        let data: TreeNodeType[] = [];

        const sortTree = (a: IStringKeyMap, b: IStringKeyMap): number => {
            if (a.children.length > b.children.length) {
                return -1;
            } else if (a.children.length < b.children.length) {
                return 1;
            }

            return a.title > b.title ? 1 : -1;
        };

        const toTree = (children: any): TreeNodeType[] => {
            let nodes: TreeNodeType[] = [];

            for (let i = 0; i < children.getCount(); ++i) {
                const model = children.get(i);
                const modelChildren: any[] = toTree(model.children).sort(sortTree);
                const isLeaf = (model.type.getValue() !== 'Group' && modelChildren.length === 0);

                const nodeItem = {
                    value: model.name.getValue(),
                    title: model.title.getValue(),
                    icon: icons[model.type.getValue()],
                    key: model.id.getValue(),
                    children: modelChildren,
                    selectable: modelChildren.length === 0,
                    leaf: isLeaf,
                    disabled: (assignmentsHashByEntityName.get(model.name.getValue()) !== undefined)
                };

                this.allEntityNodes[model.name.getValue()] = nodeItem;
                nodes.push(nodeItem);
            }

            return nodes;
        };

        data = toTree(this.rootTreeNodeModel.children).sort(sortTree);

        if (filterFn) {
            data = data.filter(filterFn);
        }

        return data;
    }

    public filterMetaObject (name: string): boolean {
        let entityExists: boolean = false;

        entityExists = this.entityCatalogs.some(function (item: Catalog) {
            let isEntityExists: boolean = false,
                isLookupEntityExists: boolean = false;

            isEntityExists = item.entities.items.some((entity: Entity) => {
                return entity.name.getValue() === name;
            });

            isLookupEntityExists = item.lookupEntities.items.some((entity: Entity) => {
                return entity.name.getValue() === name;
            });

            return isEntityExists || isLookupEntityExists;
        });

        return entityExists;
    }

    public filterProcessTypes (category: WorkflowTypeCategory): boolean {
        return category === WorkflowTypeCategory.RECORD;
    }

    public filterTriggerTypes (code: string): boolean {
        return code !== WorkflowTypeCode.RECORD_DELETE;
    }

    /**
     * Build the root node of the registry/reference tree
     *
     * @param {Catalog[]} catalogs
     * @returns {EntityTreeNode}
     */
    public buildRootTreeNode (catalogs: Catalog []): EntityTreeNode {
        let root: EntityTreeNode;

        // TODO: Replace with a new tree
        root = EntityGroupHelper.buildTreeNodeModel(this.entityCatalogs);

        return root;
    }

    @action
    public handleEntitySelect = (entityName: string | null) => {
        this.selectedEntityName = entityName;
    }

    @action
    public handleEntityItemAdd = () => {
        if (this.selectedEntityName) {
            // hiding it in the selection list
            this.allEntityNodes[this.selectedEntityName].disabled = true;
            this.selectedEntityName = null;
        }
    }

    public initHashes (entityHash: { [p: string]: any }) {
        entityHash = this.entityList.reduce((result: any, entity: CatalogAbstractEntity) => {
            result[entity.name.getValue()] = {
                displayName: entity.displayName.getValue()
            };

            return result;
        }, entityHash);

        return entityHash;
    }

    public hasWorkflow (processesHashByCode: {[name: string]: any[]}) {
        let recordEditProcesses = processesHashByCode[WorkflowTypeCode.RECORD_EDIT],
            recordDeleteProcesses = processesHashByCode[WorkflowTypeCode.RECORD_DELETE];

        return recordEditProcesses &&  recordEditProcesses.length > 0 ||
            recordDeleteProcesses && recordDeleteProcesses.length > 0;
    }
}
