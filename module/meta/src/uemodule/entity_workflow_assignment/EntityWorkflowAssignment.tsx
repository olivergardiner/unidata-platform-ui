/**
 *  UI UE Business process settings for registries/directories
 *
 * @author Sergey Shishigin
 * @date 2020-03-05
 */

import {IComponentPropsByUEType, UeModuleTypeComponent} from '@unidata/core';
import * as React from 'react';
import {WorkflowAssignmentEntityStore} from './WorkflowAssignmentEntityStore';
import {WorkflowAssignmentEntityAddPanel} from './WorkflowAssignmentEntityAddPanel';
import {i18n} from '@unidata/core-app';

let store = new WorkflowAssignmentEntityStore();

type IProps = IComponentPropsByUEType[UeModuleTypeComponent.WORKFLOW_ASSIGNMENT];

export default {
    type: UeModuleTypeComponent.WORKFLOW_ASSIGNMENT,
    moduleId: 'entity_workflow_assignment',
    active: true,
    system: false,
    resolver: () => true,
    meta: {
        order: 0,
        menuKey: 'entity',
        menuText: function () {
            return i18n.t('module.meta>entitiesOrLookupEntities');
        },
        dataQaId: 'entityTab',
        store: store
    },
    component: class ClassifierWorkflowAssignment extends React.Component<IProps> {
        render () {
            return (
                <WorkflowAssignmentEntityAddPanel
                    store={store}
                    assignmentsHashByEntityName={this.props.assignmentsHashByEntityName}
                    handleItemAdd={this.props.handleItemAdd}
                    data-qaid={this.props['data-qaid']}
                    assignableNames={this.props.assignableNames}
                />
            );
        }
    }
};
