/**
 * UI UE MATCHING_MAPPING
 * panel with mapping settings for matching fields for the registry / directory attribute
 *
 * @author Denis Makarov
 * @date 2020-03-19
 */
import {UeModuleTypeCallBack} from '@unidata/core';
import {CatalogEntity} from '../../model';
import {MetaRecordService} from '../../service/MetaRecordService';

import {EntityType, ICatalogEntity} from '@unidata/types';

export default {
    type: UeModuleTypeCallBack.GET_META_RECORD,
    moduleId: 'meta',
    active: true,
    system: false,
    fn: (entity: ICatalogEntity) => {
        let entityType;

        if (entity instanceof CatalogEntity) {
            entityType = EntityType.Entity;
        } else {
            entityType = EntityType.LookupEntity;
        }

        return MetaRecordService.getMetaRecord(entityType, entity.name.getValue());
    },
    resolver: () => true,
    meta: {}
};
