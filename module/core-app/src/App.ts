/**
 * Authentication Manager
 *
 * @author Sergey Shishigin
 * @date 2020-02-06
 */
import {AuthenticateDataType} from './type';
import {AuthTokenManager} from './AuthTokenManager';
import {UserLocale} from './locale/UserLocale';
import {UserManager} from './security';
import {LicenseManager} from './LicenseManager';
import {AppSession} from './appsession';
import {VersionManager} from './VersionManager';
import {MaintenanceManager, StartMaintenanceListener, StopMaintenanceListener} from './MaintenanceManager';
import {WebSocketManager} from './WebSocketManager';
import {MessageType} from './network/type/Message';
import {AppModule, ModuleName} from './app_module';

// TODO: Later this class will consist of ReactApp fillings
export class App {

    public static getModule<T extends ModuleName> (name: T) {
        return AppModule.getModule(name);
    }

    public static authenticate (authenticateData: AuthenticateDataType) {
        AuthTokenManager.setToken(authenticateData.token);
        AuthTokenManager.setTokenTTL(authenticateData.tokenTTL);
        VersionManager.setBuildVersion(authenticateData.buildVersion);
        UserManager.setRights(authenticateData.rights);
        UserManager.setUser(authenticateData.userInfo);
        UserManager.setForcePasswordChange(authenticateData.forcePasswordChange);
        LicenseManager.setLicense(authenticateData.license);
        AppSession.start();
        UserLocale.apply(authenticateData.userInfo.locale);
        WebSocketManager.init(authenticateData.token);
        WebSocketManager.subscribe(MessageType.START_MAINTENANCE, new StartMaintenanceListener());
        WebSocketManager.subscribe(MessageType.STOP_MAINTENANCE, new StopMaintenanceListener());

        // uploading user data (to be added later)
        // we need to discuss whether to load here and cache or to load at the place of use
        // Unidata.BackendStorage.loadByCurrentUser().then(
    }

    public static deauthenticate () {
        AuthTokenManager.setToken(null);
        UserManager.setRights(null);
        UserManager.setUser(null);
        LicenseManager.setLicense(null);
        this.reload();
    }

    public static reload () {
        window.location.reload();
    }

    public static hasExtJsApplication (): boolean {
        if (window.Ext && window.Unidata && window.Unidata.app) {
            return true;
        }

        return false;
    }

    /**
     * Checking that the backend requires you to update your password
     */
    public static isForceResetPassword () {
        return UserManager.getForcePasswordChange();
    }

    /**
     * Checking that maintenance mode should be enabled
     */
    public static isOnMaintenance () {
        return MaintenanceManager.getOnMaintenance();
    }

}
