/**
 *  Manager that blocks the screen during maintenance mode on the server
 * (for now, this mode only works when importing a model)
 *
 * @author Vladimir Stebunov
 * @date 2020-04-24
 */

import {action, observable} from 'mobx';
import {IWebSocketListener} from './type/IWebSocketListener';
import {StartMaintenance, StopMaintenance} from './network/type/Message';

export class MaintenanceManager {

    @observable private static isOnMaintenance: boolean = false;

    public static getOnMaintenance () {
        return MaintenanceManager.isOnMaintenance;
    }

    @action
    public static startMaintenance () {
        MaintenanceManager.isOnMaintenance = true;
    }

    @action
    public static stopMaintenance () {
        MaintenanceManager.isOnMaintenance = false;
    }
}

export class StartMaintenanceListener implements IWebSocketListener {
    public handle = (message: StartMaintenance) => {
        MaintenanceManager.startMaintenance();
    }
}

export class StopMaintenanceListener implements IWebSocketListener {
    public handle = (message: StopMaintenance) => {
        MaintenanceManager.stopMaintenance();
    }
}
