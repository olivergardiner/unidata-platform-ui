/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-10-22
 */

import {observable, runInAction} from 'mobx';
import i18next from 'i18next';
import {Locales} from './locale/types';
import {LocaleService} from './service/LocaleService';
import {UdLogger} from './UdLogger';

export let i18n = observable({
    t: i18next.t,
    locale: 'ru'
});

export {
    Locales,
    i18next
};

export let languages: Locales[] = [Locales.Ru, Locales.En];

export let i18nCfg = {
    lng: Locales.Ru,
    fallbackLng: Locales.Ru,
    ns: [
        'default',
        'common',
        'wiki'
    ],
    defaultNS: 'default',
    initImmediate: true,
    keySeparator: '>',
    interpolation: {
        escapeValue: false
    }
};

function loadLocalization (locale: string, namespace: string, moduleName?: string) {
    return new Promise((resolve, reject) => {
        LocaleService
            .getResourceBundle(locale, namespace, moduleName)
            .then((data: any) => {
                try {
                    i18next.addResourceBundle(locale, namespace, data);
                    resolve();
                } catch (err) {
                    UdLogger.error(`i18n: invalid json bundle ${locale}-${namespace}`);
                    reject();
                }
            })
            .catch((err: Error) => {
                if (moduleName !== undefined) {
                    UdLogger.warn(`i18n: No locale for module ${moduleName}`);
                    resolve();
                }

                reject(err);
            });
    });
}

export function loadCommonLocalization (locale: string) {
    const allPromises = i18nCfg.ns.map((namespace: string) => {
        return loadLocalization(locale, namespace);
    });

    return Promise.all(allPromises);
}

export function loadModuleLocalization (moduleName: string, locale: string) {
    return loadLocalization(locale, 'default', moduleName);
}

export function changeLanguage (locale: string) {
    let promise;

        promise = i18next.changeLanguage(locale);

        // only works in a standalone app
        (window as any).localStorage.setItem('locale', locale);

        runInAction(() => {
            i18n.t = i18next.getFixedT(locale, i18nCfg.ns);
            i18n.locale = locale;
        });

        return promise;
}
