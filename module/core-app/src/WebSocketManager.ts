/**
 *  Manager responsible for receiving and processing PUSH messages
 *
 * @author Vladimir Stebunov
 * @date 2020-04-24
 */
import {WebSocketConnection} from './network/WebSocketConnection';
import {ClientMessage, ServerMessage, MessageType} from './network/type/Message';
import {UeModuleCallback, ueModuleManager, UeModuleTypeCallBack} from '@unidata/core';
import {IWebSocketListener} from './type/IWebSocketListener';

export class WebSocketManager {

    private static connection: WebSocketConnection;

    private static listener: Partial<{ [key in MessageType]: IWebSocketListener[] }> = {};

    private static isPaused: boolean = false;

    /**
     * Launching a websocket listener and collecting all its handlers
     *
     * @param token the token for a user
     * @param handler handlers that are passed inside the app
     */
    public static init (token: string): void {
        WebSocketManager.connection = new WebSocketConnection(token);

        let moduleListener = ueModuleManager.getModulesByType(UeModuleTypeCallBack.WEBSOCKET_LISTENER);

        moduleListener.forEach((initalize: UeModuleCallback<UeModuleTypeCallBack.WEBSOCKET_LISTENER>) => {
            const {type, listener} = initalize.default.fn();

            WebSocketManager.subscribe(type as MessageType, listener as IWebSocketListener);
        });

        WebSocketManager.connection.handleResponse = WebSocketManager.handleResponse;
    }

    private static handleResponse = (message: ServerMessage) => {
        if (WebSocketManager.isPaused) {
            console.log('Websocket manager is paused');

            return;
        }

        WebSocketManager.listener[message.type]?.forEach((listener) => {
            listener.handle(message);
        });
    }

    /**
     * Send message
     *
     */
    public static send = (message: ClientMessage) => {
        const json = JSON.stringify(message);

        WebSocketManager.connection.send(json);
    }

    public static subscribe = (type: MessageType, listener: IWebSocketListener) => {
        if (!WebSocketManager.listener[type]) {
            WebSocketManager.listener[type] = [];
        }

        WebSocketManager.listener[type]?.push(listener);
    }

    public static unsubscribe = (type: MessageType, listener: IWebSocketListener) => {
        if (WebSocketManager.listener[type]) {
            WebSocketManager.listener[type]?.slice(WebSocketManager.listener[type]?.indexOf(listener), 1);
        }
    }

    public static pauseAll = () => {
        WebSocketManager.isPaused = true;
    }

    public static resumeAll = () => {
        WebSocketManager.isPaused = false;
    }

}
