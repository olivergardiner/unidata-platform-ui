/**
 *  Manager that stores the authorization token
 *
 * @author Ivan Marshalkin
 * @date 2017-12-05
 */
import {Nullable} from '@unidata/core';

const tokenKey: string = 'ud-token';
const tokenTTLKey: string = 'ud-tokenttl';

export class AuthTokenManager {
    public static getToken (): Nullable<string> {
        return localStorage.getItem(tokenKey);
    }

    public static setToken (token: Nullable<string>) {
        if (token) {
            localStorage.setItem(tokenKey, token);
        } else {
            localStorage.removeItem(tokenKey);
        }
    }

    public static getTokenTTL (): number {
        return Number(localStorage.getItem(tokenTTLKey));

    }

    public static setTokenTTL (value: number) {
        localStorage.setItem(tokenTTLKey, value.toString());
    }
}
