/**
 * Enumerating resources.
 *
 * @author Denis Makarov
 * @date 2018-10-23
 */

export let Res = {
    // This right cancels the restriction for the data operator to process up to 30 records per operation at a time
    // (however, there is a limit of 50,000 entries).
    BULK_OPERATIONS_OPERATOR: 'BULK_OPERATIONS_OPERATOR',

    // The right defines access to:
    //
    // to the "Users" section (can be overridden by the "Accounts" right);
    // to the "Roles" section (can be overridden by the user Role right);
    // to the "security Labels" section (can be overridden by the security Label right);
    // to the "Operations" section (can be overridden by the Start operations right);
    // launch operations (may be overridden by the right of the Start-up of operations);
    // section " Business processes»;
    // to the " Log " section (can be overridden by the Log right);
    // to the "platform Parameters" section (can be overridden by the "platform Parameters" right).
    ADMIN_SYSTEM_MANAGEMENT: 'ADMIN_SYSTEM_MANAGEMENT',

    // The right defines access to: the "Users" section. Takes precedence over the ADMIN_SYSTEM_MANAGEMENT right
    USER_MANAGEMENT: 'USER_MANAGEMENT',

    // The right defines access to: the "Roles" section. Takes precedence over the ADMIN_SYSTEM_MANAGEMENT right
    ROLE_MANAGEMENT: 'ROLE_MANAGEMENT',

    // This right defines access to: the "security Tags" section. Takes precedence over the ADMIN_SYSTEM_MANAGEMENT right
    SECURITY_LABELS_MANAGEMENT: 'SECURITY_LABELS_MANAGEMENT',

    // The right defines access to: the "Operations" section. Takes precedence over the ADMIN_SYSTEM_MANAGEMENT right
    DATA_OPERATIONS_MANAGEMENT: 'DATA_OPERATIONS_MANAGEMENT',

    // The right defines access to"Launch operations". Takes precedence over the ADMIN_SYSTEM_MANAGEMENT right
    EXECUTE_DATA_OPERATIONS: 'EXECUTE_DATA_OPERATIONS',

    // The right defines access to the "Log". Takes precedence over the ADMIN_SYSTEM_MANAGEMENT right
    AUDIT_ACCESS: 'AUDIT_ACCESS',

    // The right defines the access to the "platform Settings". Takes precedence over the ADMIN_SYSTEM_MANAGEMENT right
    PLATFORM_PARAMETERS_MANAGEMENT: 'PLATFORM_PARAMETERS_MANAGEMENT',

    // The metamodel Administration right defines access to:
    //
    // section " data Model»;
    // section " data Schema»;
    // section " data Sources»;
    // section " Units of measurement»;
    // to the "Transfers" section»;
    // to the "Functions" section.
    ADMIN_DATA_MANAGEMENT: 'ADMIN_DATA_MANAGEMENT',

    // The right to assign tasks to specific users.
    ADMIN_WORKFLOW_MANAGEMENT: 'ADMIN_WORKFLOW_MANAGEMENT',

    // The right defines access to the Classifiers section of the data administrator interface.
    ADMIN_CLASSIFIER_MANAGEMENT: 'ADMIN_CLASSIFIER_MANAGEMENT',

    // The right defines access to the "duplicate Settings" section of the data administrator interface.
    ADMIN_MATCHING_MANAGEMENT: 'ADMIN_MATCHING_MANAGEMENT',

    // The right defines access to the operation of mass physical deletion of records
    BULK_WIPE_REMOVE_RECORDS: 'BULK_WIPE_REMOVE_RECORDS'
};
