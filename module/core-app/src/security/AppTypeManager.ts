/**
 * Manager, determine type of application
 *
 * @author Brauer Ilya
 * @date 2020-03-25
 */

enum AppType {
    SYSTEM_ADMIN = 'systemAdmin',
    DATA_ADMIN = 'dataAdmin',
    DATA_STEWARD = 'dataSteward'
}

export class AppTypeManager {

    private static appType: string[] = [];

    public static setAppType (appType: string[]) {
        return AppTypeManager.appType = appType;
    }

    public static isSystemAdmin () {
        return AppTypeManager.appType.includes(AppType.SYSTEM_ADMIN);
    }

    public static isDataAdmin () {
        return AppTypeManager.appType.includes(AppType.DATA_ADMIN);
    }

    public static isDataSteward () {
        return AppTypeManager.appType.includes(AppType.DATA_STEWARD);
    }
}
