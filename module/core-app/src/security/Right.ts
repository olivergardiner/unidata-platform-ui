/**
 * Listing rights
 *
 * @author Denis Makarov
 * @date 2018-10-23
 */
export enum Right {
    CREATE = 'create',
    READ = 'read',
    UPDATE = 'update',
    DELETE = 'delete'
}
