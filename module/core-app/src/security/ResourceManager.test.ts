/**
 * Tests for ResourceManager
 *
 * @author: Denis Makarov
 * @date: 2018-10-24
 */

import {UserManager} from './UserManager';
import {Right} from './Right';
import {ResourceManager} from './ResourceManager';

let userIsAdminMock = {
    admin: true
};

let userNotAdminMock = {
    admin: false
};

let rightsMock = [
    {
        securedResource: {
            name: 'MIGHTY'
        },
        create: true,
        read: true,
        update: true,
        'delete': true

    },
    {
        securedResource: {
            name: 'WEAK'
        },
        create: false,
        read: false,
        update: false,
        'delete': false
    },
    {
        securedResource: {
            name: 'READONLY'
        },
        create: false,
        read: true,
        update: false,
        'delete': false
    }
];

beforeEach(() => {
    UserManager.setRights(rightsMock);
});

describe('ResourceManager', () => {
    it('userHasAnyRight works correctly for non admin', () => {
        UserManager.setUser(userNotAdminMock);

        expect(ResourceManager.userHasAnyRight('MIGHTY', [Right.CREATE, Right.UPDATE])).toBe(true);
        expect(ResourceManager.userHasAnyRight('MIGHTY', [Right.CREATE])).toBe(true);
        expect(ResourceManager.userHasAnyRight('MIGHTY', [Right.DELETE, Right.UPDATE, Right.CREATE, Right.READ])).toBe(true);

        expect(ResourceManager.userHasAnyRight('WEAK', [Right.CREATE, Right.UPDATE])).toBe(false);
        expect(ResourceManager.userHasAnyRight('WEAK', [Right.CREATE])).toBe(false);
        expect(ResourceManager.userHasAnyRight('WEAK', [Right.DELETE, Right.UPDATE, Right.CREATE, Right.READ])).toBe(false);
    });

    it('userHasAnyRight works correctly for admin', () => {
        UserManager.setUser(userIsAdminMock);

        expect(ResourceManager.userHasAnyRight('WEAK', [Right.CREATE, Right.UPDATE])).toBe(true);
        expect(ResourceManager.userHasAnyRight('WEAK', [Right.CREATE])).toBe(true);
        expect(ResourceManager.userHasAnyRight('WEAK', [Right.DELETE, Right.UPDATE, Right.CREATE, Right.READ])).toBe(true);
    });

    it('userHasRights works correctly for admin', () => {
        UserManager.setUser(userNotAdminMock);

        expect(ResourceManager.userHasRights('READONLY', [Right.READ])).toBe(true);
        expect(ResourceManager.userHasRights('READONLY', [Right.DELETE, Right.UPDATE, Right.CREATE])).toBe(false);
        expect(ResourceManager.userHasRights('READONLY', [Right.READ, Right.DELETE])).toBe(false);
    });

    it('isReadOnly returns true for non admin', () => {
        UserManager.setUser(userNotAdminMock);

        expect(ResourceManager.isReadOnly(['READONLY'])).toBe(true);
    });

    it('isReadOnly returns false for non admin', () => {
        UserManager.setUser(userNotAdminMock);

        expect(ResourceManager.isReadOnly(['MIGHTY'])).toBe(false);
    });

    it('isReadOnly returns false for admin', () => {
        UserManager.setUser(userIsAdminMock);

        expect(ResourceManager.isReadOnly(['READONLY'])).toBe(false);
    });

});
