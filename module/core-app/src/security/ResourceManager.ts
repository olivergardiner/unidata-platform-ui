import forEach from 'lodash/forEach';
import isArray from 'lodash/isArray';
import isString from 'lodash/isString';
import {UserManager} from './UserManager';
import {Right} from './Right';

/**
 *  System resource rights Manager
 *
 * @author Denis Makarov
 * @date 2019-06-04
 */

export class ResourceManager {
    public static userHasAnyRight (resource: string, rightNames: Right[]): boolean {
        let hasRight = false;

        if (isArray(rightNames)) {
            forEach(rightNames, function (rightName) {
                let result;

                result = ResourceManager.userHasRight(resource, rightName);

                if (result) {
                    hasRight = true;

                    return false; // stopping iteration
                }

                return true;
            });
        }

        return hasRight;
    }

    public static userHasRights (resource: string, rightNames: Right[]): boolean {
        let hasRights = true;

        if (isArray(rightNames)) {
            forEach(rightNames, function (rightName) {
                let result;

                result = ResourceManager.userHasRight(resource, rightName);

                if (!result) {
                    hasRights = false;

                    return false; // stopping iteration
                }

                return true;
            });
        } else {
            hasRights = false;
        }

        return hasRights;
    }

    public static userHasRight (resource: string, right: Right): boolean {
        let hasRight = false,
            rights = UserManager.getRights(),
            rightName = right;

        // the administrator can do everything
        if (UserManager.isUserAdmin()) {
            hasRight = true;

            return hasRight;
        }

        if (isArray(rights) && isString(rightName)) {
            forEach(rights, function (userRight) {
                if (userRight &&
                    userRight.securedResource &&
                    userRight.securedResource.name === resource &&
                    userRight[rightName] === true) {
                    hasRight = true;

                    return false;
                }

                return true;
            });
        }

        return hasRight;
    }

    // Todo use cache
    public static userHasAnyResourceRight (resources: string [], right: Right) {
        let hasRight = false,
            rights = UserManager.getRights(),
            rightName = right;

        if (UserManager.isUserAdmin()) {
            hasRight = true;

            return hasRight;
        }

        forEach(resources, function (resource) {
            forEach(rights, function (userRight) {
                if (userRight &&
                    userRight.securedResource &&
                    userRight.securedResource.name === resource &&
                    userRight[rightName] === true) {
                    hasRight = true;

                    return false;
                }

                return true;
            });
        });

        return hasRight;
    }

    public static isReadOnly (resources: string[]) {
        return ResourceManager.userHasAnyResourceRight(resources, Right.READ) &&
            !ResourceManager.userHasAnyResourceRight(resources, Right.UPDATE) &&
            !ResourceManager.userHasAnyResourceRight(resources, Right.DELETE) &&
            !ResourceManager.userHasAnyResourceRight(resources, Right.CREATE);
    }
}
