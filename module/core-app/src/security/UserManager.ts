/**
 *  Manager that stores user data
 *
 * @author Denis Makarov
 * @date 2017-10-24
 */
import {Nullable} from '@unidata/core';
import {UserInfoType} from '../type/UserInfoType';
import {RightType} from '../type/RightType';

export class UserManager {
    private static rights: Nullable<RightType[]>;
    private static user: Nullable<UserInfoType>;
    private static forcePasswordChange: boolean = false;

    public static getRights () {
        return UserManager.rights;
    }

    public static setRights (value: any) {
        UserManager.rights = value;
    }

    public static getUser (): Nullable<UserInfoType> {
        return UserManager.user;
    }

    public static getUserLogin (): Nullable<string> {
        let user = UserManager.getUser();

        if (!user) {
            return null;
        }

        return user.login;
    }

    public static setUser (userInfo: UserInfoType | null) {
        UserManager.user = userInfo;
    }

    public static isUserAdmin () {
        let user;

        user = this.getUser();

        if (user && user.admin) {
            return true;
        }

        return false;
    }

    public static getUserRoles (): string[] {
        let user = UserManager.getUser();

        if (!user) {
            return [];
        }

        return user.roles;
    }

    /**
     * Set the password reset flag
     */
    public static getForcePasswordChange (): boolean {
        return UserManager.forcePasswordChange;
    }

    /**
     * Take the password reset flag
     */
    public static setForcePasswordChange (flag: boolean) {
        UserManager.forcePasswordChange = flag;
    }

}
