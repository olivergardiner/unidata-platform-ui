/**
 *  Global config of Unidata (blank)
 *
 * @author Sergey Shishigin
 * @date 2019-10-25
 */

import {CustomerConfigType} from './type';
import clone from 'lodash/clone';

export class UnidataConfig {
    private static customerConfig: CustomerConfigType;

    public static setCustomerCfg (config: CustomerConfigType): void {
        this.customerConfig = config;
    }

    private static getCustomerCfg (): CustomerConfigType {
        return this.customerConfig;
    }

    /**
     * The address where the backend is located
     */
    public static getServerUrl (): string {
        let customerCfg = this.getCustomerCfg();

        if (!/^http/.test(customerCfg.serverUrl)) {
            return window.location.origin + customerCfg.serverUrl;
        }

        return customerCfg.serverUrl ? customerCfg.serverUrl : '';
    }

    /**
     * Return the format that is sent from the server date/time
     */
    public static getServerDateTimeFormat (): string {
        return 'YYYY-MM-DDTHH:mm:ss.SSS';
    }

    /**
     * Return the format that is sent from the server date/time
     */
    public static getServerDateFormat (): string {
        return 'YYYY-MM-DDT00:00:00.000';
    }

    /**
     * Application locale at the time of application launch
     */
    public static getDefaultLocale () {
        const availableLocale = ['en', 'ru'];
        const browserLocale = navigator?.language?.slice(0, 2);

        return availableLocale.indexOf(browserLocale) > -1 ? browserLocale : 'en';
    }

    /**
     * Returns an array of URLs for loading UI UE
     */
    public static getPlatformExternalModules (): string[] {
        let customerCfg = this.getCustomerCfg();

        if (!customerCfg.EXTERNAL_MODULES || !Array.isArray(customerCfg.EXTERNAL_MODULES)) {
            return [];
        }

        return clone(customerCfg.EXTERNAL_MODULES);
    }

    /**
     * Indicates whether to display the wiki Button
     */
    public static getWikiEnabled (): boolean {
        let customerCfg = this.getCustomerCfg();

        return Boolean(customerCfg.WIKI_ENABLED);
    }

    /**
     * Returns the wiki host
     */
    public static getWikiHost (): string {
        let customerCfg = this.getCustomerCfg();

        return customerCfg.WIKI_HOST ? String(customerCfg.WIKI_HOST) : '';
    }

    /**
     * Get app mode
     */
    public static getAppType (): string[] {
        let customerCfg = this.getCustomerCfg();

        return customerCfg.APP_TYPE;
    }

    /**
     * The address where the push backend is located
     */
    public static getPushServerUrl (): string {
        return  this.getServerUrl()
                          .replace('http', 'ws')
                          .replace('api/', 'websocket');
    }

    /**
     * Maximum number of items for bulk operations if the Res.BULK_OPERATIONS_OPERATOR permission is not available
     */
    public static getBulkSelectionLimit (): number {
        return this.getCustomerCfg().BULK_SELECTION_LIMIT || 30;
    }
}
