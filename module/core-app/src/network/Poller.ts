/**
 * Abstract poller
 * Starting and stopping an instance changes the number of people interested in the survey.
 * If there are no interested parties in the survey, the survey stops.
 * A survey is attempted once per second for each class.
 * The poll method is called no more often than the specified interval value.
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-26
 */

export abstract class Poller {

    // for correct typing this.constructor
    // eslint-disable-next-line @typescript-eslint/explicit-member-accessibility
    ['constructor']: typeof Poller;

    private static isAllPollsPaused: boolean = false;
    private static startedPollsCount: number = 0; // number of interested in the survey

    private static lastPollTimestamp: number = 0; // time of the last survey
    private static pollPingTimer: number;

    private static isPollStarted: boolean = false;
    private static isPollPaused: boolean = false;

    private isInstancePollStarted: boolean = false;

    abstract readonly interval: number; // query interval (ms)
    abstract poll (): void; // method called by the interval

    /**
     * Pauses all the questions
     */
    public static pauseAll () {
        Poller.isAllPollsPaused = true;
    }

    /**
     * Continues surveys, if they are running
     */
    public static resumeAll () {
        Poller.isAllPollsPaused = true;
    }

    public start () {
        if (this.isInstancePollStarted) {
            return;
        }

        this.isInstancePollStarted = true;

        this.constructor.startedPollsCount++;

        if (!this.constructor.isPollStarted) {
            this.constructor.isPollStarted = true;

            this.doPoll();
            this.constructor.pollPingTimer = window.setInterval(this.doPoll.bind(this), 1000);
        }
    }

    public stop () {
        if (!this.isInstancePollStarted) {
            return;
        }

        this.isInstancePollStarted = false;

        this.constructor.startedPollsCount--;

        if (this.constructor.startedPollsCount <= 0) {
            clearInterval(this.constructor.pollPingTimer);
            this.constructor.isPollStarted = false;
            this.constructor.startedPollsCount = 0;
        }
    }

    public pause (): void {
        this.constructor.isPollPaused = true;
    }

    public resume (): void {
        this.constructor.isPollPaused = false;
    }

    private doPoll (): void {
        if (
            !Poller.isAllPollsPaused && // polls are allowed globally
            !this.constructor.isPollPaused && // the pool is resolved for the instance
            (Date.now() - this.constructor.lastPollTimestamp) > this.interval // is it time for the survey
        ) {
            this.constructor.lastPollTimestamp = Date.now();
            this.poll();
        }
    }

}
