/**
 * Ajax request interceptor on the global level
 *
 * @author Ivan Marshalkin
 * @date 2019-08-23
 */

import {AxiosRequestConfigEx, AxiosRequestInterceptor, AxiosResponseInterceptor, Nullable, AxiosError, AxiosResponse} from '@unidata/core';
import {AuthTokenManager} from '../AuthTokenManager';
import {App} from '../App';
import {Dialog} from '../util';
import has from 'lodash/has';
import {AppSession} from '../appsession';
import {NetworkErrorUtil} from '../util/NetworkErrorUtil';
import {ServerErrorSeverity} from './type/ServerErrorSeverity';
import {i18n} from '../i18n';
import {ErrorDetail} from '../type/ToastType';

export class AjaxRequestInterceptor {
    private static requestInterceptor: Nullable<AxiosRequestInterceptor>;
    private static responseInterceptor: Nullable<AxiosResponseInterceptor>;

    public static initInterceptors () {
        AjaxRequestInterceptor.cancelInterceptors();

        AjaxRequestInterceptor.initRequestInterceptor();
        AjaxRequestInterceptor.initResponseInterceptor();
    }

    public static cancelInterceptors () {
        if (AjaxRequestInterceptor.requestInterceptor) {
            AjaxRequestInterceptor.requestInterceptor.cancelInterceptor();
            AjaxRequestInterceptor.requestInterceptor = null;
        }

        if (AjaxRequestInterceptor.responseInterceptor) {
            AjaxRequestInterceptor.responseInterceptor.cancelInterceptor();
            AjaxRequestInterceptor.responseInterceptor = null;
        }
    }

    public static initRequestInterceptor () {
        AjaxRequestInterceptor.requestInterceptor = new AxiosRequestInterceptor(null, {
            onComplete: function (config: AxiosRequestConfigEx) {

                config = AjaxRequestInterceptor.transformDisableCacheParam(config);
                config = AjaxRequestInterceptor.transformAuthorizationHeaderParam(config);
                config = AjaxRequestInterceptor.updateSession(config);

                return Promise.resolve(config);
            },
            onReject: function (error) {
                return Promise.reject(error);
            }
        });
    }
    /**
     * Performs a transformation to prevent the browser from caching requests
     *
     * @param {AxiosRequestConfig} requestCfg
     * @param {ITransformCfg} transformCfg
     * @returns {AxiosRequestConfig}
     */
    private static transformDisableCacheParam (requestCfg: AxiosRequestConfigEx): AxiosRequestConfigEx {
        let disableCacheParam: string = '_dc';

        requestCfg.params = requestCfg.params || {};
        requestCfg.params[disableCacheParam] = Number(new Date());

        return requestCfg;
    }

    /**
     * Converts the header for authorization on the backend side
     *
     * @param {AxiosRequestConfig} requestCfg
     * @param {ITransformCfg} transformCfg
     * @returns {AxiosRequestConfig}
     */
    private static transformAuthorizationHeaderParam (requestCfg: AxiosRequestConfigEx): AxiosRequestConfigEx {
        let authorizationHeaderParam: string = 'Authorization';
        let token: Nullable<string> = AuthTokenManager.getToken();

        // TODO: to add validation. this header is added only if the request is sent to the unidata backend address
        requestCfg.headers = requestCfg.headers || {};

        if (token !== undefined) {
            requestCfg.headers[authorizationHeaderParam] = token;
        }

        return requestCfg;
    }

    public static initResponseInterceptor () {
        const me = this;

        AjaxRequestInterceptor.responseInterceptor = new AxiosResponseInterceptor(null, {
            onComplete: function (response) {
                me.processResponse(response.request, response);

                return Promise.resolve(response);
            },
            onReject: function (error: AxiosError) {
                me.processResponse(error.request, error.response);

                return Promise.reject(error);
            }
        });
    }

    private static updateSession (config: AxiosRequestConfigEx): AxiosRequestConfigEx {
        // TODO: the session should be updated after a successful response is received from the server?
        // because the session can theoretically be interrupted on the server
        if (!has(config.headers, 'PROLONG_TTL') || config.headers['PROLONG_TTL'] === 'true') {
            AppSession.update();
        }

        return config;
    }

    private static processResponse (request: any, response?: AxiosResponse): void {
        const status: number = request.status,
             errors = NetworkErrorUtil.getErrors(response);

        let msg: string | string[] = '',
            opts: ErrorDetail | undefined = undefined;

        if (response && response.config && (response.config as AxiosRequestConfigEx).disableErrorHandling) {
            console.warn('request error handler disabled', request, response);

            return;
        }

        switch (status) {
            case 0:
                msg = i18n.t('app.error>serverConnection');

                Dialog.showError(msg);

                return;
            case 200:

                // we work out if there is an error in the response field
                if (response && NetworkErrorUtil.responseHasError(response)) {
                    const detail = NetworkErrorUtil.getErrorDetails(response),
                        msg = NetworkErrorUtil.getErrorMessage(errors[0]) || i18n.t('app.error>unknownError');

                    Dialog.showError(msg, undefined, detail);
                }

                return;
            case 401:
                App.deauthenticate();

                return;
            case 402:
                msg = i18n.t('app.error>invalidLicense');

                if (NetworkErrorUtil.responseHasError(response)) {
                    msg = NetworkErrorUtil.getErrorMessage(errors[0]);
                }

                Dialog.showError(msg);

                return;
            case 404:
            case 405:
            case 500:
                if (status === 404) {
                    msg = i18n.t('app.error>urlNotFound');
                } else if (status === 405) {
                    msg = i18n.t('app.error>unsupportedRequest');
                } else if (status === 500) {
                    msg = i18n.t('app.error>internalServerError');
                }

                if (response && NetworkErrorUtil.responseHasError(response)) {
                    opts = NetworkErrorUtil.getErrorDetails(response);

                    msg = NetworkErrorUtil.getErrorMessage(errors[0]) || msg;
                }

                Dialog.showError(msg, undefined, opts, false, ServerErrorSeverity.CRITICAL);

                return;
        }
    }
}
