/**
 * Class for working with WebSocket
 *
 * @author Victor Magarlamov
 * @date 2019-04-22
 */

import {UnidataConfig} from '../UnidataConfig';
import {ServerMessage} from './type/Message';


// TODO: split into two parts core and app
export class WebSocketConnection {
    // available number of connection attempts
    private static MAX_RECONNECT_COUNT = 3;

    // endpoint for establishing a connection
    private URL: string = UnidataConfig.getPushServerUrl();

    private socket: any;
    private token: string;

    private reconnectCount: number = 0;

    public handleResponse: (m: ServerMessage) => void;

    constructor (token: string) {
        this.token = token;
        this.connect();
    }

    public connect () {
        if (this.URL.length === 0) {
            console.warn('pushServerUrl nod defined in customer.json');

            return;
        }

        // establishing a permanent connection to the server
        this.socket = new WebSocket(this.URL);

        this.socket.onopen = this.handleSocketOpen;
        this.socket.onmessage = this.handleOnMessage;
        this.socket.onclose = this.handleSocketClose;
        this.socket.onerror = this.handleSocketFail;

        this.reconnectCount += 1;
    }

    /*
     * the handler for a successful connection
     * passing the user's token to the server.
     * this socket channel is linked to this token on the server,
     * you don't need to send it again
     */
    private handleSocketOpen = () => {
        console.log('ws - connected succesful');

        // example
        //
        // const message = {type: 'info', token: this.token};
        // this.send(JSON.stringify(message));
        //

        this.send(this.token);

        this.reconnectCount = 0;
    }

    /*
     * sending a message to the server
     */
    public send = (message: string) => {
        this.socket.send(message);
    }

    /*
     * message handler from the server
     */
    private handleOnMessage = (message: any) => {
        if (message.data) {
            const jsonResult = JSON.parse(message.data);

            this.handleResponse(jsonResult);
        }
    }

    /*
     * the handler for closing the socket from server side
     *
     * usually, if the closure is not accidental, the reason is passed to event
     * if there is no reason, you can try reconnecting by calling connect
     */
    private handleSocketClose = (event: any) => {
        if (event.wasClean) {
            return;
        } else if (this.reconnectCount <= WebSocketConnection.MAX_RECONNECT_COUNT) {
            console.log('ws - We lost it!');
            this.connect();
        }

        console.log('Code: ' + event.code + ' cause: ' + event.reason);
    }

    /*
     * the error handler a connection has been opened
     * making MAX_RECONNECT_COUNT attempts to reset the connection
     */
    private handleSocketFail = (error: any) => {
        console.log('ws - error ' + error.message);

        if (this.reconnectCount <= WebSocketConnection.MAX_RECONNECT_COUNT) {
            this.connect();
        }
    }
}
