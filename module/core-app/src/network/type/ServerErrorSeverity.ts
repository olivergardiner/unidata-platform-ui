/**
 * Error criticality
 *
 * @author Ivan Marshalkin
 * @date 2020-02-19
 */

export enum ServerErrorSeverity {
    'LOW' = 'LOW',
    'NORMAL' = 'NORMAL',
    'HIGH' = 'HIGH',
    'CRITICAL' = 'CRITICAL'
}
