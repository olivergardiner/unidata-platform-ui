export type ServerMessage = StartMaintenance | StopMaintenance;

export type StartMaintenance = {
    type: MessageType.START_MAINTENANCE;
}

export type StopMaintenance = {
    type: MessageType.STOP_MAINTENANCE;
}

export enum MessageType {
    START_MAINTENANCE = 'START_MAINTENANCE',
    STOP_MAINTENANCE = 'STOP_MAINTENANCE'
}

export type ClientMessage = {};
