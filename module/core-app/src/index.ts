﻿declare global {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    interface Window {
        Ext: any;
        Unidata: any;
    }
}

export * from './MagicProperties';

export * from './model/search';

export * from './ServerUrlManager';

export * from './LicenseManager';

export * from './VersionManager';

export * from './operation/AppModelHttpOp';

export * from './operation/AppHttpOp';

export * from './operation/AppModelListHttpOp';

export * from './locale/UserLocale';

export * from './storage/BackendStorage';

export * from './store';

export {IBackendStorageRecord} from './storage/IBackendStorageRecord';

export * from './util';

export * from './UnidataConfig';

export * from './network/AjaxRequestInterceptor';

export * from './network/WebSocketConnection';

export * from './network/Poller';

export * from './AuthTokenManager';

export * from './service';

export * from './i18n';

export * from './security';

export * from './type';

export * from './AuthTokenManager';

export * from './App';

export * from './app_module';

export * from './appsession';

export * from './PageManager';

export * from './wiki/wiki_button/WikiButton';

export * from './util/UdUtils';

export * from './util/StringUtil';

export * from './UdLogger';

export * from './hotkey/GlobalHotKeyManager';

export * from './store';

export * from './MenuManager';

export {ServerErrorSeverity} from './network/type/ServerErrorSeverity';

export * from './MaintenanceManager';

export {ServerMessage, ClientMessage} from './network/type/Message';
