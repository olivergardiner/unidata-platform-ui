/**
 * Auxiliary class for working with pages
 *
 * @author: Vladimir Stebunov
 * @date: 2020-02-26
 */
export class PageManager {

    private static currentPage: any

    /**
     * Giving the pointer to the current page
     */
    public static getCurrentPage (): any {
        return PageManager.currentPage;
    }

    /**
     * Remembers the current page instance
     *
     * @param page
     */
    public static setCurrentPage (page: any) {
        PageManager.currentPage = page;
    }

}
