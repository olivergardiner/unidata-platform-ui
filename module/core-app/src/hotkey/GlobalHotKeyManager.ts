/**
 * Global hotkey Manager
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-17
 */
import keycode from 'keycode';

interface IHotKeyManagerConfig {
    letterKey: string;
    ctrlKey?: boolean;
    altKey?: boolean;
    shiftKey?: boolean;
}

interface IHotKeyCallback {
    (): void;
}

interface IHotKeyMap {
    [key: string]: IHotKeyCallback;
}

export abstract class GlobalHotKeyManager {

    private static inited: boolean;
    private static map: IHotKeyMap;

    private static init () {
        if (this.inited) {
            return;
        }

        this.onKeyUp = this.onKeyUp.bind(this);
        this.map = {};
        this.inited = true;

        this.enable();
    }

    private static onKeyUp (event: KeyboardEvent) {
        let mapKey = this.getMapKey(event);

        if (this.map[mapKey]) {
            this.map[mapKey]();
        }
    }

    private static getMapKey (from: KeyboardEvent | IHotKeyManagerConfig): string {
        let {ctrlKey, altKey, shiftKey} = from,
            letterKey: string,
            mapKey: string;

        if ('keyCode' in from) {
            letterKey = keycode(from);
        } else {
            letterKey = keycode(keycode(from.letterKey));
        }

        mapKey = [
            letterKey,
            Boolean(ctrlKey),
            Boolean(altKey),
            Boolean(shiftKey)
        ].join('.');

        return mapKey;
    }

    public static enable () {
        window.document.addEventListener('keyup', this.onKeyUp);
    }

    public static disable () {
        window.document.removeEventListener('keyup', this.onKeyUp);
    }

    /**
     * Registering a global hotkey handler
     */
    public static register (hotkey: IHotKeyManagerConfig, callback: IHotKeyCallback, overwrite: boolean = false) {
        this.init();

        let mapKey = this.getMapKey(hotkey);

        if (!overwrite && this.map[mapKey]) {
            throw new Error(`HotKey "${mapKey}" already registered.`);
        } else {
            this.map[mapKey] = callback;
        }
    }

}
