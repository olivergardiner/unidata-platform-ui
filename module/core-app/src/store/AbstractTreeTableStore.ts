/**
 * Abstract stor for working with trees
 * Used if you need to work with data models
 *
 * @author Denis Makarov
 * @date 2020-11-19
 */

import {action, IObservableArray} from 'mobx';
import {AbstractModel, AbstractStore} from '@unidata/core';
import {ITreeNode} from '@unidata/types';

export abstract class AbstractTreeTableStore extends AbstractStore {
    public abstract getData (): (AbstractModel[] | AbstractModel); //returns the original data set in setData()
    public abstract getSelectedModels (): AbstractModel[]; // an array of the selected models
    public abstract select (node: ITreeNode, allowSelect?: boolean, allowDeselect?: boolean): void;  // selecting a node
    public abstract setData (data: (AbstractModel[] | AbstractModel), disabledNodes?: string[]): void; // setting the initial data
    public abstract treeData: ITreeNode[]; // tree ITreeNode
    protected abstract transformData (data: (AbstractModel[] | AbstractModel), disabledNodes?: string []): ITreeNode[];  // converting data to a tree and creating caches
    protected abstract nodesCache: Map<string, ITreeNode>; // node cache
    protected abstract modelCache: Map<string, AbstractModel>;  // model cache
    protected abstract selectedModels: IObservableArray<AbstractModel>;
    protected abstract selectedNodes: IObservableArray<ITreeNode>;

    @action
    public updateSelection (selectedKeys: string []) {
        let selectedNodes: ITreeNode [] = [];
        let selectedModels: AbstractModel [] = [];

        this.clearSelected();

        for (let key of selectedKeys) {
            let node = this.nodesCache.get(key);
            let model = this.modelCache.get(key);

            if (node) {
                selectedNodes.push(node);
            }

            if (model) {
                selectedModels.push(model);
            }
        }

        this.selectedModels.replace(selectedModels);
        this.selectedNodes.replace(selectedNodes);
    }

    @action
    public clearSelected () {
        let selectedEntities = this.getSelectedModels();
        let selectedItems = this.getSelectedNodes();

        selectedEntities.splice(0);
        selectedItems.splice(0);
    }

    @action
    public updateDisabledNodes (keys: string []) {
        this.nodesCache.forEach(function (value: ITreeNode) {
            value.disabled = false;
        });
        let nodesMap = this.getAllNodesFlatMap();

        keys.forEach(function (key: string) {
            let node = nodesMap.get(key);

            if (!node) {
                return;
            }

            node.disabled = true;
        });
    }

    public getAllNodesFlatMap () {
        return this.nodesCache;
    }

    public getModelByKey (key: string): AbstractModel | undefined {
        return this.modelCache.get(key);
    }

    public getSelectedNodes () {
        return this.selectedNodes as ITreeNode [];
    }

    public getSelectedKeys () {
        return this.getSelectedNodes().map((node: ITreeNode) => node.key);
    }
}
