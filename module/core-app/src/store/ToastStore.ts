/**
 * Notification store
 *
 * @author Stebunov Vladimir
 * @date 2020-03-19
 */

import {action, observable, runInAction} from 'mobx';
import {ToastMessage} from '../type/ToastType';
import {ueModuleManager, UeModuleTypeCallBack} from '@unidata/core';

export class ToastStore {

    private readonly limit: number = 5;
    private readonly closeDelay: number = 5000;
    private readonly disableAutoClose: boolean = false;
    private readonly disappearDelay: number = 1500;

    @observable.shallow
    private showed: ToastMessage[] = [];
    @observable.shallow
    public disappeared: ToastMessage[] = [];
    @observable.shallow
    private queue: ToastMessage[] = [];

    constructor () {
        const modules = ueModuleManager.getModulesByType(UeModuleTypeCallBack.TOAST_OPTIONS);

        for (let module of modules) {
            if (module.default.meta.closeDelay !== undefined) {
                this.closeDelay = module.default.meta.closeDelay;
            }

            if (module.default.meta.disableAutoClose !== undefined) {
                this.disableAutoClose = module.default.meta.disableAutoClose;
            }

            break; // use the first one, ignore the rest
        }
    }

    public get list () {
        return this.showed;
    }

    public isDisappeared (message: ToastMessage) {
        return this.disappeared.indexOf(message) !== -1;
    }

    @action
    public add = (message: ToastMessage) => {

        if (this.showed.length >= this.limit) {
            this.queue.push(message);

            return;
        }

        this.showed.push(message);

        if (!this.disableAutoClose && !message.manualclose) {
            setTimeout(() => {
                runInAction(() => {
                    this.remove(message);
                });
            }, this.closeDelay);
        }
    }

    @action
    public remove = (message: ToastMessage) => {
        if (this.showed.indexOf(message) === -1) {

            return;
        }

        this.disappeared.push(message);
        setTimeout(() => {
            runInAction(() => {
                this.showed.splice(this.showed.indexOf(message), 1);
                this.disappeared.splice(this.disappeared.indexOf(message), 1);
                this.moveQueue();
            });
        }, this.disappearDelay);
    }

    @action
    private moveQueue = () => {
        const first: ToastMessage | undefined = this.queue.shift();

        if (first) {
            this.add(first);
        }
    }
}
