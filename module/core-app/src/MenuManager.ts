/**
 * Menu item Manager
 *
 * @author Brauer Ilya
 * @date 2020-04-14
 */
import {IMenuGroup, IMenuItem} from './type';
import {UeModuleCallback, ueModuleManager, UeModuleTypeCallBack} from '@unidata/core';
import {AppTypeManager} from './security';
import {UdLogger} from './UdLogger';

interface IMenuItemWithOrder extends IMenuItem {
    order?: number | 'first' | 'last';
}

export class MenuManager {
    private static menuList: IMenuGroup[];

    public static getMenuList (): IMenuGroup[] {
        if (MenuManager.menuList === undefined) {
            UdLogger.error('Try to get not initialized menuList');

            return [];
        } else {
            return MenuManager.menuList;
        }
    }

    public static init () {
        if (MenuManager.menuList) {
            UdLogger.error('Try to init another instance of menuList');
        }

        let menuGroups: IMenuGroup[] = [
            {
                name: 'dataManagement',
                items: []
            },
            {
                name: 'model',
                items: []
            },
            {
                name: 'administration',
                items: []
            }
        ];

        /**
         * Initialize external menu modules and add them to the menu
         * enable all menu items that are the same as the main menu
         */
        const genericMenuItems = ueModuleManager.getModulesByType(UeModuleTypeCallBack.MENU_ITEM);

        if (genericMenuItems && genericMenuItems.length > 0) {
            menuGroups.forEach((group: IMenuGroup) => {
                const menuItems = genericMenuItems.filter((module: UeModuleCallback<UeModuleTypeCallBack.MENU_ITEM>) => {
                    return module.default.meta.groupName === group.name;
                })
                    .map((item: UeModuleCallback<UeModuleTypeCallBack.MENU_ITEM>) => {
                        return {
                            name: item.default.meta.name,
                            route: item.default.meta.route,
                            icon: item.default.meta.icon,
                            activeInDraftMode: item.default.meta.activeInDraftMode,
                            order: item.default.meta.order,
                            menuColor: item.default.meta.menuColor,
                            isActiveFor: item.default.meta.isActiveFor
                        } as IMenuItemWithOrder;
                    });

                group.items = MenuManager.merge(group.items, menuItems);
            });
        }

        // processing the menu using user exit
        let userExitMenuItems = ueModuleManager.getModulesByType(UeModuleTypeCallBack.GROUP_MENU);

        if (userExitMenuItems) {
            userExitMenuItems.forEach((userExit: UeModuleCallback<UeModuleTypeCallBack.GROUP_MENU>) => {
                menuGroups = userExit.default.fn(menuGroups);
            });
        }

        this.menuList = menuGroups.filter((menuGroupItem) => {
            return (menuGroupItem.name === 'dataManagement' && AppTypeManager.isDataSteward()) ||
                (menuGroupItem.name === 'model' && AppTypeManager.isDataAdmin()) ||
                (menuGroupItem.name === 'administration' && AppTypeManager.isSystemAdmin()) ||
                (menuGroupItem.name === 'nongrouped');
        });
    }

    private static merge (menuItems: IMenuItem[], additions: IMenuItemWithOrder[]): IMenuItem[] {
        let notAdded: IMenuItemWithOrder[] = [];

        additions.forEach((addition) => {
            if (typeof addition.order === 'number' && !isNaN(addition.order as any)) {
                menuItems.splice(addition.order, 0, addition);
                delete addition.order;
            } else {
                notAdded.push(addition);
            }
        });

        notAdded.forEach((addition) => {
            if (addition.order === 'first') {
                menuItems.unshift(addition);
            } else if (addition.order === 'last') {
                menuItems.push(addition);
            } else {
                console.warn('menu item ' + addition.name + ' has wrong order:' + addition.order);
            }

            delete addition.order;
        });

        return menuItems;
    }
}
