/**
 *  WTF class ... or interface... or type ...$END$
 *
 * @author Sergey Shishigin
 * @date 2019-12-27
 */
import {ServerUrlManager} from '../ServerUrlManager';
import {ModelHttpOp} from '@unidata/core';

export class AppModelHttpOp extends ModelHttpOp {
    constructor () {
        super();
        this.baseURL = ServerUrlManager.getBaseUrl();
    }
}
