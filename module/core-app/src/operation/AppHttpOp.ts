/**
 *  WTF class ... or interface... or type ...$END$
 *
 * @author Sergey Shishigin
 * @date 2019-12-27
 */
import {BaseHttpOp} from '@unidata/core';
import {ServerUrlManager} from '../ServerUrlManager';

export class AppHttpOp extends BaseHttpOp {

    constructor () {
        super();
        this.baseURL = ServerUrlManager.getBaseUrl();
    }
}
