/**
 *  WTF class ... or interface... or type ...$END$
 *
 * @author Sergey Shishigin
 * @date 2019-12-27
 */
import {ServerUrlManager} from '../ServerUrlManager';
import {ModelListHttpOp} from '@unidata/core';

export class AppModelListHttpOp extends ModelListHttpOp {
    constructor () {
        super();
        this.baseURL = ServerUrlManager.getBaseUrl();
    }
}
