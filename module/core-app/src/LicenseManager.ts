/**
 * License manager
 *
 * @author Sergey Shishigin
 * @date 2020-02-06
 */
import {LicenseType} from './type/LicenseType';
import {Nullable} from '@unidata/core';

export class LicenseManager {
    private static license: Nullable<LicenseType>;

    public static getLicense (): Nullable<LicenseType> {
        return this.license;
    }

    public static setLicense (value: Nullable<LicenseType>) {
        this.license = value;
    }
}
