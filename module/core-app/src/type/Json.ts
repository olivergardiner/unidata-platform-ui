/**
 * Type of decoded value from json
 *
 * see
 * https://github.com/microsoft/TypeScript/issues/1897
 *
 * @author Ivan Marshalkin
 * @date 2020-06-01
 */

export type JSONPrimitive = string | number | boolean | null;

export type JSONValue = JSONPrimitive | JSONObject | JSONArray;

export type JSONObject = { [member: string]: JSONValue };

export type JSONArray = JSONValue[];
