/**
 * Types for notifications
 *
 * @author Stebunov Vladimir
 * @date 2020-03-19
 */
import {ServerErrorSeverity} from '../network/type/ServerErrorSeverity';

export type ToastMessage = {
    id: string;
    type: ToastTypeEnum;
    description: string | string[];
    title?: string;
    manualclose?: boolean;
    detail?: ErrorDetail;
    severity?: ServerErrorSeverity;
}

export enum ToastTypeEnum {
    Error = 'error',
    Warning = 'warning',
    Info = 'info'
}

export type ErrorDetail = {
    errorUrl?: string;
    stackTrace?: string;
    userMessage?: string;
    userMessageDetails?: string;
}
