/**
 * The interface of the main menu
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import {IMenuItem} from './IMenuItem';

export interface IMenuGroup {
    name: string;
    items: IMenuItem[];
}
