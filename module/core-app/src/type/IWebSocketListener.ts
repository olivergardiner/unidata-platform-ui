/**
 *  listener
 *
 * @author Vladimir Stebunov
 * @date 2020-05-25
 */

import {ServerMessage} from '../network/type/Message';

export interface IWebSocketListener {
    handle: (message: ServerMessage) => void;
}
