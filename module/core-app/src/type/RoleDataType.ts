/**
 * Information on the role
 *
 * @author Sergey Shishigin
 * @date 2020-02-05
 */
export type RoleDataType = {
    createdAt: string | null;
    createdBy: string | null;
    displayName: string;
    name: string;
    properties: string[];
    rights: string[];
    securityLabels: string[];
    type: string;
    updatedAt: string | null;
    updatedBy: string | null;
}
