/**
 * Information about rights
 *
 * @author Sergey Shishigin
 * @date 2020-02-05
 */
export type RightType = {
    create: boolean;
    createdAt: string | null;
    createdBy: string | null;
    delete: boolean;
    read: boolean;
    securedResource: {
        category: string;
        children: string[];
        createdAt: string | null;
        createdBy: string | null;
        displayName: string;
        name: string;
        parent: string | null;
        type: string;
        updatedAt: string | null;
        updatedBy: string | null;
    };
    update: boolean;
    updatedAt: string | null;
    updatedBy: string | null;
}
