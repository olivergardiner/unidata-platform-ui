/**
 * Type for an Observable modx object, for storage in non-observable data structures
 *
 * @author Denis Makarov
 * @date: 2019-10-18
 */

import {IObservableObject} from 'mobx';

export type ObservableObject<T> = IObservableObject & T;
