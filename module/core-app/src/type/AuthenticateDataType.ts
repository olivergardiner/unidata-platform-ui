/**
 * Authentication data
 *
 * @author Sergey Shishigin
 * @date 2020-02-05
 */
import {LicenseType} from './LicenseType';
import {RightType} from './RightType';
import {UserInfoType} from './UserInfoType';

export type AuthenticateDataType = {
    buildVersion: string;
    forcePasswordChange: boolean;
    forceLicenseChange: boolean;
    license: LicenseType;
    rights: RightType[];
    token: string;
    tokenTTL: number;
    userInfo: UserInfoType;
}
