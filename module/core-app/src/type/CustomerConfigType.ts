/**
 * User configuration of the platform
 *
 * @author Sergey Shishigin
 * @date 2020-02-05
 */

export type CustomerConfigType = {
    APP_TYPE: string[];
    APP_TYPE_URL: {
        dataAdmin: string;
        systemAdmin: string;
        dataSteward: string;
    };
    serverUrl: string;
    pushServerUrl: string;

    DASHBOARD?: {
        HIDE_TASKS_COUNT?: boolean;
        HIDE_EXPORT_BUTTON?: boolean;
    };
    QUERY_PRESET_PANEL_VISIBLE?: boolean;
    SEARCH_ROWS?: number;
    SEARCH_OPERATION_TYPE?: boolean;
    BULK_SELECTION_LIMIT?: number;
    CHECK_SOURCESYSTEM_WEIGHT_UNIQUE?: boolean;
    MAX_TABS?: number;
    MERGE_RECORD_DISPLAY_COUNT?: number;
    DECIMAL_PRECISION?: number;
    CDN_VENDOR_LIBS?: string[];
    PLATFORM_EXTERNAL_CLASSES?: string[];
    EXTERNAL_MODULES?: string[];
    FIRST_RELATION_SEARCH_RECORDS_COUNT?: number;
    TIMEINTERVAL_ENABLED?: boolean;
    TASKS_POLL_INTERVAL?: number;
    AJAX_REQUEST_TIMEOUT?: number | null;
    ORIGIN_CAROUSEL_MAXCOUNT?: number;
    ORIGIN_CAROUSEL_DISPLAYCOUNT?: number;
    WIKI_HOST?: string | null;
    WIKI_ENABLED?: boolean;
    MERGE_MANUAL_WINNERS?: boolean;
    ENABLE_CLASSIFIER_CONSTRUCTOR?: boolean;
    ENABLE_ENTITY_RELATIONS_GRAPH?: boolean;
}
