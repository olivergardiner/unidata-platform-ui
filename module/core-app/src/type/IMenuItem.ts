import {ComponentType} from 'react';
import {IconName} from '@unidata/icon';

/**
 * Interface for a menu item
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */

export interface IMenuItem {
    name: string;
    route: string;
    component: ComponentType;
    icon: IconName;
    menuColor?: string; // see uikit specific/menu_item/menuColors.ts
    activeInDraftMode?: boolean;
    pinned?: boolean;
    isActiveFor?: string[]; // if menu item should be active not only for route, set here all possible routs
}
