/**
 * User property information
 *
 * @author Sergey Shishigin
 * @date 2020-02-05
 */
export type UserPropertyType = {
    displayName: string;
    id: number;
    name: string;
    readOnly: boolean;
    required: boolean;
    value: any;
}
