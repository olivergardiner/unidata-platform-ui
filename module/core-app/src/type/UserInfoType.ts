/**
 * User information
 *
 * @author Sergey Shishigin
 * @date 2020-02-05
 */
import {UserEndpointType} from './UserEndpointType';
import {RoleDataType} from './RoleDataType';
import {UserPropertyType} from './UserPropertyType';

export type UserInfoType = {
    active: boolean;
    admin: boolean;
    createdAt: string | null;
    createdBy: string | null;
    email: string;
    emailNotification: boolean;
    endpoints: UserEndpointType[];
    external: boolean;
    firstName: string;
    fullName: string;
    lastName: string;
    locale: 'ru' | 'en';
    login: string;
    properties: UserPropertyType[];
    roles: string[];
    rolesData: RoleDataType[];
    securityDataSource: string;
    securityLabels: [];
    updatedAt: string | null;
    updatedBy: string | null;
}
