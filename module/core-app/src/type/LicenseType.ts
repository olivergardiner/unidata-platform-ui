/**
 * License information
 *
 * @author Sergey Shishigin
 * @date 2020-02-05
 */
import {LicenseMode} from './LicenseMode';

export type LicenseType = {
    expirationDate: string;
    licenseMode: LicenseMode;
    licenseModeDisplayName: string;
    modules: string[];
    owner: string | null;
    valid: boolean;
    validationError: string | null;
    version: string;
    versionDisplayName: string;
}
