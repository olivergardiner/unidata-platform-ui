/**
 * Info about user endpoint
 *
 * @author Sergey Shishigin
 * @date 2020-02-05
 */
export type UserEndpointType = {
    description: string;
    displayName: string;
    name: string;
}
