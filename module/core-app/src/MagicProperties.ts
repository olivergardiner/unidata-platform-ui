/**
 * List of available "magic" variables (attributes from users, roles, ...)
 *
 * @author Brauer Ilya
 * @date 2020-04-27
 */

export class MagicProperties {
    // User properties
    public static SHOW_MENU_ON_START = 'showMenuOnStart'; // Display the menu on the main page

    public static resolvers = {
        [MagicProperties.SHOW_MENU_ON_START]: (value: any) => value === 'true'
    };
}
