/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */
import {IBackendStorageRecord} from './IBackendStorageRecord';

export class BackendStorage {
    public static data: IBackendStorageRecord [] = [];
}
