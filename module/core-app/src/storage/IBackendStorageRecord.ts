/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

export interface IBackendStorageRecord {
    key: string;
    user: string;
    value: string;
}
