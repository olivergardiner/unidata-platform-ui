/**
 * The version Manager application
 *
 * @author Denis Makarov
 * @date 2020-03-04
 */

export class VersionManager {
    private static buildVersion: string;

    public static getBuildVersion (): string {
        return this.buildVersion;
    }

    public static setBuildVersion (value: string) {
        this.buildVersion = value;
    }
}
