/**
 User locale interface for data output

 @author Vladimir Stebunov
 @date 2019-10-29
 */

export interface IBaseLocale {
    // Date
    getDateFormat: () => string;
    getDateTimeFormat: () => string;
    getDateTimeMillsFormat: () => string;
    getTimeFormat: () => string;
    getParseDateFormat: () => string;
    getParseTimestampFormat: () => string;
    getParseTimeFormat: () => string;

    // Numbers
    getDecimalSeparator: () => string;
}
