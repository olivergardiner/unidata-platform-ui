import {IBaseLocale} from './IBaseLocale';

export class EnLocale implements IBaseLocale {
    public getDateFormat (): string {
        return 'DD.MM.YYYY';
    }

    public getDateTimeFormat (): string {
        return 'DD.MM.YYYY H:mm:s';
    }

    public getDateTimeMillsFormat (): string {
        return 'DD.MM.YYYY H:mm:s.u';
    }

    public getTimeFormat (): string {
        return 'HH:mm:ss';
    }

    public getParseDateFormat (): string {
        return 'DD.MM.YYYY';
    }

    public getParseTimestampFormat (): string {
        return 'YYYY-MM-DD HH:mm:ss';
    }

    public getParseTimeFormat (): string {
        return 'HH:mm:ss';
    }

    public getDecimalSeparator (): string {
        return '.';
    }
}
