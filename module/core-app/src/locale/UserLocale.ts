/**
 * The class stores the values for formatting in the current locale of the user
 *
 * @author Stebunov Vladimir
 * @date 2019-10-28
 */
import {IBaseLocale} from './IBaseLocale';
import {RuLocale} from './RuLocale';
import {EnLocale} from './EnLocale';

export class UserLocale {

    private static locale: IBaseLocale;

    public static apply (locale: 'ru' | 'en'): void {
        switch (locale) {
            case 'ru':
                UserLocale.locale = new RuLocale();
                break;
            case 'en':
                UserLocale.locale = new EnLocale();
                break;
            default:
                console.warn('This locale not implemented, switched to default');
                UserLocale.locale = new RuLocale();
        }
    }

    public static getDateFormat (): string {
        return UserLocale.locale.getDateFormat();
    }

    public static getDateTimeFormat (): string {
        return UserLocale.locale.getDateTimeFormat();
    }

    public static getDateTimeMillsFormat (): string {
        return UserLocale.locale.getDateTimeMillsFormat();
    }

    public static getTimeFormat (): string {
        return UserLocale.locale.getTimeFormat();
    }

    public static getParseDateFormat (): string {
        return UserLocale.locale.getParseDateFormat();
    }

    public static getParseTimestampFormat (): string {
        return UserLocale.locale.getParseTimestampFormat();
    }

    public static getParseTimeFormat (): string {
        return UserLocale.locale.getParseTimeFormat();
    }

    public static getDecimalSeparator (): string {
        return UserLocale.locale.getDecimalSeparator();
    }
}
