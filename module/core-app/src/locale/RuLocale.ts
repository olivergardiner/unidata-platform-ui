import {IBaseLocale} from './IBaseLocale';

export class RuLocale implements IBaseLocale {
    public getDateFormat (): string {
        return 'DD.MM.YYYY';
    }

    public getDateTimeFormat (): string {
        return 'DD.MM.YYYY HH:mm:ss';
    }

    public getDateTimeMillsFormat (): string {
        return 'DD.MM.YYYY HH:mm:ss.u';
    }

    public getTimeFormat (): string {
        return 'HH:mm:ss';
    }

    public getParseDateFormat (): string {
        return 'DD.MM.YYYY';
    }

    public getParseTimestampFormat (): string {
        return 'YYYY-MM-DD HH:mm:ss';
    }

    public getParseTimeFormat (): string {
        return 'HH:mm:ss';
    }

    public getDecimalSeparator (): string {
        return ',';
    }
}
