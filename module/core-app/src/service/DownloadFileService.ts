/**
 * File download service
 *
 * @author Brauer Ilya
 * @date 2020-03-12
 */

import {AuthTokenManager, ServerUrlManager} from '../index';

export class DownloadFileService {

    /**
     * Download by link
     * @param url
     */
    public static byUrl (url: string) {
        DownloadFileService.downloadByUrl(DownloadFileService.buildDownloadUrl(url));
    }

    /**
     * Create a file from json data
     */
    public static byJSONData (data: string, name: string) {
        const blobObject = new Blob([data], {type: 'application/json'});

        if (window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blobObject, name);
        } else {
            const url = window.URL.createObjectURL(blobObject);

            DownloadFileService.downloadByUrl(url, name);
        }
    }

    private static buildDownloadUrl (url: string) {
        return ServerUrlManager.buildUrl(`${url}?token=${AuthTokenManager.getToken()}`);
    }

    private static downloadByUrl (url: string, downloadName?: string) {
        const link = document.createElement('a');

        link.setAttribute('href', url);
        link.setAttribute('target', '_blank');

        if (downloadName !== undefined) {
            link.setAttribute('download', downloadName);
        }

        document.body.appendChild(link);
        link.click();

        document.body.removeChild(link);
    }
}
