/**
 * Localization service
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {ReadResourceBundleOp} from './op/locale/ReadResourceBundleOp';

export class LocaleService {

    /**
     * Retrieves localization data
     *
     * @param locale-locale code name
     * @param namespace-code space for localized messages
     * @param moduleName-name of the module
     */
    public static getResourceBundle (locale: string, namespace: string, moduleName?: string): Promise<any> {
        let op = new ReadResourceBundleOp(locale, namespace, moduleName);

        return op.execute();
    }

}
