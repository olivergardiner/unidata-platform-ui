/**
 * Password change operation
 *
 * @author Vladimir Stebunov
 * @date 2020-03-03
 */

import {IOpConfig} from '@unidata/core';
import {AppHttpOp} from '../../..';

export class ChangePasswordOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/authentication/setpassword',
        method: 'post'
    };

    constructor (username: string, password: string, newPassword: string) {
        super();

        this.config.data = {
            userName: username,
            oldPassword: password,
            password: newPassword
        };
    }
}
