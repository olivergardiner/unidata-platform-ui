/**
 * The operation authorization of the user
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {IOpConfig} from '@unidata/core';
import {AppHttpOp} from '../../../index';

export class LogoutOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/authentication/logout',
        method: 'post',
        rootProperty: '',
        successProperty: '',
        headers: {
            'Content-Type': 'application/json'
        },
        data: null,
        successStatusCode: 202
    };
}
