/**
 * The operation authorization of the user
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {IOpConfig} from '@unidata/core';
import {AppHttpOp} from '../../../index';

export class LoginOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/authentication/login',
        method: 'post'
    };

    constructor (login: string, password: string) {
        super();

        this.config.data = {
            password: password,
            userName: login
        };
    }
}
