/**
 * User authorization operation for the current token
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {IOpConfig} from '@unidata/core';
import {AppHttpOp} from '../../../index';

export class AuthenticateOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/authentication/get-current-user',
        method: 'get'
    };
}
