/**
 * Password setting operation
 *
 * @author Vladimir Stebunov
 * @date 2020-03-03
 */

import {IOpConfig} from '@unidata/core';
import {AppHttpOp} from '../../..';

export class SetPasswordOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/authentication/setpassword',
        method: 'post'
    };

    constructor (username: string, password: string) {
        super();


        this.config.data = {
            userName: username,
            password: password
        };
    }
}
