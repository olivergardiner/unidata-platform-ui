/**
 * Operation to save an entry to server storage
 *
 * @author Denis Makarov
 * @date 2019-08-23
 */

import {IOpConfig} from '@unidata/core';
import {AppHttpOp} from '../../..';

export class UpdateBackendStorageOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/custom-storage/',
        method: 'put'
    };

    constructor (user: string, key: string, value: string) {
        super();

        this.config.data = [
            {key, user, value}
        ];
    }
}
