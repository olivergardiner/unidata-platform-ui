/**
 * The operation of obtaining the server store
 *
 * @author Denis Makarov
 * @date 2019-08-23
 */
import {IOpConfig} from '@unidata/core';
import {AppHttpOp, ServerUrlManager} from '../../..';

export class ReadBackendStorageListOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/custom-storage',
        method: 'get'
    };

    private user: string | undefined;
    private key: string | undefined;

    constructor (config: {key: string; user?: string});
    constructor (config: {key?: string; user: string});
    constructor (config: {key?: string; user?: string}) {
        super();

        this.user = config.user;
        this.key = config.key;

        // if the search is by user_name + key, we send them as query params
        if (config.key && config.user) {
            this.config.params = {
                userName: config.user,
                key: config.key
            };
        }
    }

    protected buildUrl (): string {
        if (this.key && this.user) {
            return super.buildUrl();
        } else if (this.key) {
            return ServerUrlManager.buildUrl(`/core/custom-storage/key/${this.key}`);
        } else if (this.user) {
            return ServerUrlManager.buildUrl(`/core/custom-storage/user-name/${this.user}`);
        }

        throw new Error('can\'t build url, key or user_name must be specified');
    }
}
