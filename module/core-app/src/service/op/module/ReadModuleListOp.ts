/**
 * Operation for getting a list of additional role properties
 *
 * @author Brauer Ilya
 * @date 2020-03-31
 */

import {IOpConfig} from '@unidata/core';
import {AppHttpOp} from '../../../index';

export class ReadModuleListOp extends AppHttpOp {
    protected config: IOpConfig = {
        method: 'get',
        url: '/core/module',
        rootProperty: ''
    };
}
