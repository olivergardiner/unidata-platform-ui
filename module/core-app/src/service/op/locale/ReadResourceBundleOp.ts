/**
 * Operation for getting the localization file
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {BaseHttpOp, IOpConfig} from '@unidata/core';
import {ServerUrlManager} from '../../../index';
import {AxiosResponse} from 'axios';
import {JsonUtil} from '../../../util/JsonUtil';

export class ReadResourceBundleOp extends BaseHttpOp {
    protected config: IOpConfig = {
        method: 'get',
        successProperty: '',
        rootProperty: ''
    };

    private locale: string;
    private namespace: string;
    private moduleName?: string;

    constructor (locale: string, namespace: string, moduleName?: string) {
        super();

        this.locale = locale;
        this.namespace = namespace;
        this.moduleName = moduleName;

        // when loading a locale for a module, no errors are needed, since it may simply not exist
        this.config.disableErrorHandling = moduleName !== undefined ? true : false;
    }

    protected buildUrl (): string {
        const prefix = ServerUrlManager.getResourcePrefixUrl();

        let url = this.moduleName !== undefined ?
            `${prefix}/react-resources/locale/module/${this.moduleName}/${this.locale}/${this.locale}-${this.namespace}.json` :
            `${prefix}/react-resources/locale/${this.locale}/${this.locale}-${this.namespace}.json`;

        return url;
    }

    protected responseDataValid (value: AxiosResponse): boolean {
        if (JsonUtil.decode(value.request.responseText, true) === null) {
            return false;
        }

        return super.responseDataValid(value);
    }
}
