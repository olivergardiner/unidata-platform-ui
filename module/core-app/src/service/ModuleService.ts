/**
 * Module verification service
 *
 * @author Brauer Ilya
 * @date 2020-03-31
 */
import {ReadModuleListOp} from './op/module/ReadModuleListOp';
import {IBackendModule} from '@unidata/types';


export class ModuleService {

    /**
     * Retrieves the list of available modules in the system
     *
     * @param locale-locale code name
     * @param namespace-code space for localized messages
     */
    public static getModuleList (): Promise<IBackendModule []> {
        let op = new ReadModuleListOp();

        return op.execute();
    }

}
