﻿/**
 * The user authorization service
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {LoginOp} from './op/authorization/LoginOp';
import {LogoutOp} from './op/authorization/LogoutOp';
import {AuthenticateOp} from './op/authorization/AuthenticateOp';
import {ChangePasswordOp} from './op/authorization/ChangePasswordOp';
import {SetPasswordOp} from './op/authorization/SetPasswordOp';

export class AuthorizationService {
    // TODO: describe the interface for loginInfo returned information
    /**
     * Authorizes the user with a username and password
     *
     * @param login
     * @param password
     */
    public static login (login: string, password: string): Promise<any> {
        let op = new LoginOp(login, password);

        return op.execute();
    }

    /**
     * Deauthorizes the current user
     */
    public static logout (): Promise<any> {
        let op = new LogoutOp();

        return op.execute();
    }

    // TODO: describe the interface for loginInfo returned information
    /**
     * Authorizes the user with the current authorization token
     */
    public static authenticate (): Promise<any> {
        let op = new AuthenticateOp();

        return op.execute();
    }

    /**
     * Changes the user's password at their request
     */
    public static changePassword (username: string, password: string, newPassword: string): Promise<any> {
        let op = new ChangePasswordOp(username, password, newPassword);

        return op.execute();
    }

    /**
     * Changes the user's password at the request of the server
     */
    public static setPassword (username: string, newPassword: string): Promise<any> {
        let op = new SetPasswordOp(username, newPassword);

        return op.execute();
    }
}
