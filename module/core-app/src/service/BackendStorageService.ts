
/**
 * Service for working with Backend Properties
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {IBackendStorageRecord} from '..';
import {ReadBackendStorageListOp} from './op/backendstorage/ReadBackendStorageListOp';
import {UpdateBackendStorageOp} from './op/backendstorage/UpdateBackendStorageOp';

export enum BackendStorageKeys {
    MAIN_MENU_PIN = 'main_menu_pin',
    AUDIT_LOG_TABLE = 'audit_log_table',
    SEARCH_TERMS = 'search_terms'
}

export class BackendStorageService {

    public static loadRecords (userName: string, key: BackendStorageKeys): Promise<IBackendStorageRecord[]>;
    public static loadRecords (userName: string, key?: string): Promise<IBackendStorageRecord[]>;
    public static loadRecords (userName: string, key?: string): Promise<IBackendStorageRecord[]> {
        let op = new ReadBackendStorageListOp({
            user: userName,
            key: key
        });

        return op.execute();
    }

    public static saveRecord (user: string, key: BackendStorageKeys, value: string): Promise<any[]>;
    public static saveRecord (user: string, key: string, value: string): Promise<any[]>;
    public static saveRecord (user: string, key: string, value: string): Promise<any[]> {
        let op = new UpdateBackendStorageOp(user, key, value);

        return op.execute();
    }
}
