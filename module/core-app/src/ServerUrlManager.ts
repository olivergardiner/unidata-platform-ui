/**
 * Auxiliary class for working with URLS
 *
 * @author Ivan Marshalkin
 * @date 2017-12-16
 */

import {HttpUrl} from '@unidata/core';

export class ServerUrlManager {
    private static baseUrl: string = ''; // for API requests
    private static resourcePrefixUrl: string = '.'; // for requests to frontend resources (localization, svg...)

    public static setBaseUrl (baseUrl: string): void {
        ServerUrlManager.baseUrl = baseUrl;
    }

    public static getBaseUrl (): string {
        return ServerUrlManager.baseUrl;
    }

    public static buildUrl (postUrl: string = ''): string {
        return HttpUrl.buildUrl(this.getBaseUrl(), postUrl);
    }

    public static getResourcePrefixUrl (): string {
        return ServerUrlManager.resourcePrefixUrl;
    }
}
