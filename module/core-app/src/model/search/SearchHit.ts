/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-09-04
 */

import {
    AbstractModel,
    arrayField,
    ArrayField,
    hasMany,
    IAssociationDescriptorMap, IStringKeyMap,
    ModelCollection,
    ModelMetaData,
    StringField,
    stringField,
    NumberField,
    numberField
} from '@unidata/core';
import {SearchPreview} from './SearchPreview';

export class SearchHit extends AbstractModel {

    @stringField({primaryKey: true})
    public id: StringField;

    @arrayField()
    public source: ArrayField;

    @hasMany()
    public preview: ModelCollection<SearchPreview>;

    @stringField()
    public status: StringField;

    @numberField()
    public score: NumberField;

    constructor (data: any, descriptorConfig?: IStringKeyMap, initConfig?: IStringKeyMap, onChangeDirty?: () => void) {
        super(data, descriptorConfig, initConfig, onChangeDirty);
    }

    public initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                preview: SearchPreview
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    /**
     * Returns a preview in the form of an object {field: value}
     */
    public getAsObject (): Record<string, any> {
        let previewItems = this.preview.items.slice(),
            result: Record<string, any> = {};

        for (let previewItem of previewItems) {
            result[previewItem.field.getValue()] = previewItem.value.getValue();
        }

        return result;
    }

}
