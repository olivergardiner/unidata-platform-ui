/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-09-04
 */

import {
    AbstractModel,
    arrayField,
    ArrayField,
    hasMany,
    IAssociationDescriptorMap,
    ModelCollection,
    ModelMetaData,
    stringField,
    StringField
} from '@unidata/core';
import {ExtendedValue} from './ExtendedValue';

export class SearchPreview extends AbstractModel {

    @stringField()
    public field: StringField;

    /**
     * DEPRECATED!
     *
     * This property is deprecated. You must use values
     */
    @stringField()
    public value: StringField;

    @arrayField()
    public values: ArrayField;

    @hasMany()
    public extendedValues: ModelCollection<ExtendedValue>;

    public initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                extendedValues: ExtendedValue
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
