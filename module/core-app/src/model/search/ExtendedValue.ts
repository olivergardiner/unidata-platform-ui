/**
 * Model for a value from the reference list in simpleAttribute
 *
 * @author: Ilya Brauer
 * @date: 2019-10-29
 */

import {AbstractModel, stringField, StringField} from '@unidata/core';

export class ExtendedValue extends AbstractModel {

    @stringField()
    public displayValue: StringField;

    @stringField()
    public linkedEtalonId: StringField;

}
