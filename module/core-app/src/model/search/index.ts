/**
 * Search models are placed in core-app, as they are required in different parts of the app.
 * TODO-implement an abstract search module that should contain these models and descriptions of abstract response types
 */

export * from './SearchHit';

export * from './ExtendedValue';

export * from './SearchPreview';
