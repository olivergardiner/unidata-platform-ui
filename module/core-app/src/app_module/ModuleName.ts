/**
 * Names of all modules
 *
 * @author: Aleksandr Bavin
 * @date: 2020-06-22
 */
export enum ModuleName {
    UNKNOWN = 'unknown', // example of a module that is not in the source code

    // System modules
    ICON = 'icon',
    TYPES = 'types',
    CORE = 'core',
    CORE_APP = 'core-app',
    UIKIT = 'uikit',

    // CE modules
    META = 'meta',
    DRAFT = 'draft',
    DATA = 'data',
    SECURITY = 'security',
    AUDIT = 'audit',
    SYSTEM_CONFIG = 'system-config',
    JOB = 'job',

    // EE modules
    CLASSIFIER = 'classifier',
    DQ = 'dq',
    MATCHING = 'matching',
    WORKFLOW = 'workflow',
    DASHBOARD = 'dashboard'
}

export type ModuleType = {
    /* eslint-disable @typescript-eslint/ban-ts-comment */
    // @ts-ignore-start
    // @ts-ignore
    [ModuleName.UNKNOWN]: typeof import('@unidata/unknown'); // example of a module that is not in the source code

    // system modules
    // @ts-ignore
    [ModuleName.ICON]: typeof import('@unidata/icon');
    // @ts-ignore
    [ModuleName.TYPES]: typeof import('@unidata/types');
    // @ts-ignore
    [ModuleName.CORE]: typeof import('@unidata/core');
    // @ts-ignore
    [ModuleName.CORE_APP]: typeof import('@unidata/core-app');
    // @ts-ignore
    [ModuleName.UIKIT]: typeof import('@unidata/uikit');

    // CE modules
    // @ts-ignore
    [ModuleName.META]?: typeof import('@unidata/meta');
    // @ts-ignore
    [ModuleName.DRAFT]?: typeof import('@unidata/draft');
    // @ts-ignore
    [ModuleName.DATA]?: typeof import('@unidata/data');
    // @ts-ignore
    [ModuleName.SECURITY]?: typeof import('@unidata/security');
    // @ts-ignore
    [ModuleName.AUDIT]?: typeof import('@unidata/audit');
    // @ts-ignore
    [ModuleName.SYSTEM_CONFIG]?: typeof import('@unidata/system-config');
    // @ts-ignore
    [ModuleName.JOB]?: typeof import('@unidata/job');

    // EE modules
    // @ts-ignore
    [ModuleName.CLASSIFIER]?: typeof import('@unidata/classifier');
    // @ts-ignore
    [ModuleName.DQ]?: typeof import('@unidata/dq');
    // @ts-ignore
    [ModuleName.MATCHING]?: typeof import('@unidata/matching');
    // @ts-ignore
    [ModuleName.WORKFLOW]?: typeof import('@unidata/workflow');
    // @ts-ignore
    [ModuleName.DASHBOARD]?: typeof import('@unidata/dashboard');
    // @ts-ignore-end
}
