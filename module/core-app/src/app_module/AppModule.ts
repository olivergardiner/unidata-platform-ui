/**
 * Class for internal access to system modules
 *
 * @author: Aleksandr Bavin
 * @date: 2020-06-18
 */
import {ModuleName, ModuleType} from './ModuleName';
// import {ModuleType} from './ModuleType';

type UnknownModule = {[key: string]: any};
type AnyToNever<T extends ModuleName> = Exclude<any, ModuleType[T]> extends never ? never : ModuleType[T];
type GetModuleType<T extends ModuleName> = AnyToNever<T> extends never ? UnknownModule | undefined : ModuleType[T];

export class AppModule {

    private static instance: AppModule;

    private modules: any = {};

    public inited: boolean = false;

    constructor () {
        if (AppModule.instance) {
            throw new Error('Instance is available only during initialization');
        }

        AppModule.instance = this;
    }

    public addModule (name: string, module: any) {
        this.modules[name] = module;
    }

    public static getModule<T extends ModuleName> (name: T): GetModuleType<T> {
        if (!AppModule.instance.inited) {
            throw new Error('Modules are not yet initialized');
        }

        return AppModule.instance.modules[name];
    }

}
