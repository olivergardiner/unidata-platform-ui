/**
 *  Session component
 *
 * @author Sergey Shishigin
 * @date 2020-02-13
 */
import {AuthTokenManager} from './AuthTokenManager';

const sessionKey: string = 'ud-session';

export class Session  {
    private sessionInterval: any = null;
    private autoUpdateSessionInterval: any = null;
    private autoUpdateSessionCount: number = 0;
    private static readonly MS_IN_SECOND = 1000;

    private getTokenTTL () {
        return AuthTokenManager.getTokenTTL();
    }

    private getSession () {
        return Number(localStorage.getItem(sessionKey));
    }

    private setSessionToNow () {
        this.setSession(Number(new Date()));
    }

    private setSession (value: number) {
        localStorage.setItem(sessionKey, value.toString());
    }

    public update () {
        if (!this.getSession()) { // we do not extend the session if there is no session
            return;
        }

        this.setSessionToNow();
    }

    private clear () {
        localStorage.removeItem(sessionKey);
    }

    private tickSession () {
        if (!this.isAlive()) {
            this.stop();

            window.dispatchEvent(new CustomEvent('sessionexpired'));
        }
    }

    private isAlive () {
        let now = Number(new Date()),
            session = this.getSession(),
            ttl = this.getTokenTTL(),
            alive;

        alive = session + Session.MS_IN_SECOND * ttl > now;

        return alive;
    }

    public start () {
        this.clear();

        this.setSessionToNow();
        this.sessionInterval = setInterval(this.tickSession.bind(this), Session.MS_IN_SECOND);
    }

    public stop () {
        clearInterval(this.sessionInterval);
        this.clear();
    }

    private tickAutoUpdateSession () {
        this.update();
    }

    public startAutoUpdate () {
        this.autoUpdateSessionCount++;

        if (this.autoUpdateSessionInterval) {
            return;
        }

        this.update();

        this.autoUpdateSessionInterval = setInterval(this.tickAutoUpdateSession.bind(this), this.getTokenTTL() / 2 * Session.MS_IN_SECOND);
    }

    public stopAutoUpdate () {
        this.autoUpdateSessionCount--;

        if (this.autoUpdateSessionCount < 0) {
            this.autoUpdateSessionCount = 0;
        }

        if (!this.autoUpdateSessionCount) {
            clearInterval(this.autoUpdateSessionInterval);

            this.autoUpdateSessionInterval = null;
        }
    }
}
