/**
 * The help button that appears on the right side of the pages
 *
 * @author: Vladimir Stebunov
 * @date: 2020-02-27
 */

import * as React from 'react';
import styles from './wikiButton.m.scss';
import {PageManager} from '../../PageManager';
import {UnidataConfig} from '../../UnidataConfig';
import {i18n} from '../../i18n';

interface IProps {}
interface IState {
    classes: string[];
    disabled: boolean;
}

const WIKI_INCREASE_TIMEOUT = 800;

export class WikiButton extends React.Component<IProps, IState> {

    deferExpandTimerId: any;

    constructor (props: IProps) {
        super(props);

        this.state = {
            classes: [styles.unWikiButton],
            disabled: false
        };
    }

    /**
     * Updated information about the availability of help
     */
    update () {
        const currentPage = PageManager.getCurrentPage();

        let disabled = false;

        if (!(currentPage)) {
            disabled = true;
        } else if (!currentPage.wikiPage || !currentPage.wikiPage()) {
            disabled = true;
        }

        this.setState({disabled: disabled});
    }

    /**
     * Getting the url depending on the resulting wiki Page field
     *
     * @returns url
     */
    getWikiUrl () {
        const page = PageManager.getCurrentPage();

        let wikiUrl = null;

        if (page.wikiPage) {
            wikiUrl = page.wikiPage();
        }

        if (wikiUrl) {
            wikiUrl = this.buildWikiUrl(wikiUrl);

            // if the Protocol is not specified then use the Protocol of the host on which the platform is running
            // regular string decoding: the string starts with characters (from 1 to 4) and the following character ":"
            wikiUrl = wikiUrl.match(/^.{1,4}:/) ? wikiUrl : '//' + wikiUrl;
        }

        return wikiUrl;
    }

    /**
     * Returns the wiki url of the page based on the wiki host
     *
     * @param url
     * @returns {*}
     */
    buildWikiUrl (url: string) {
        return this.getWikiHost() + url;
    }

    /**
     * Returns the wiki host
     *
     * @returns {string}
     */
    getWikiHost () {
        return UnidataConfig.getWikiHost();
    }

    onMouseEnter (): void {
        this.update();

        this.setState((prevState) => ({classes: [...prevState.classes, styles.unMouseOver]}));

        this.deferExpandTimerId = setTimeout(() => {
            this.setState((prevState) => ({classes: [...prevState.classes, styles.unMouseOverExpand]}));
        }, WIKI_INCREASE_TIMEOUT);
    }

    onMouseLeave (): void {
        this.setState({classes: [styles.unWikiButton]});

        if (this.deferExpandTimerId) {
            clearTimeout(this.deferExpandTimerId);
            this.deferExpandTimerId = null;
        }
    }

    onWikiLinkClick () {
        if (this.state.disabled) {
            return;
        }

        const wikiWindow = window.open(this.getWikiUrl(), '_blank');

        if (wikiWindow) {
            wikiWindow.focus();
        }
    }

    render (): any {
        const iconCls = [styles.unWikiIcon];

        if (this.state.disabled) {
            iconCls.push(styles.unWikiIconDisabled);
        }

        return (
            <div className={styles.unWiki}>
                <div
                    className={this.state.classes.join(' ')}
                    onMouseEnter={this.onMouseEnter.bind(this)}
                    onMouseLeave={this.onMouseLeave.bind(this)}
                >
                    <div className={styles.unShadowContainer}>
                        <div className={styles.unSlideButtonHelptext}>
                            {i18n.t('wiki>slideButtonText')}
                        </div>
                        <div
                            className={iconCls.join(' ')}
                            onClick={this.onWikiLinkClick.bind(this)}
                            style={{
                                backgroundImage: 'url(\'react-resources/images/wikiicon.png\')' //can't specify a relative path in scss
                            }}
                        ></div>
                    </div>
                </div>
            </div>
        );
    }
}
