export const unWiki: string;
export const unWikiButton: string;
export const unSlideButtonHelptext: string;
export const unWikiIcon: string;
export const unWikiIconDisabled: string;
export const unShadowContainer: string;
export const unMouseOver: string;
export const unMouseOverExpand: string;