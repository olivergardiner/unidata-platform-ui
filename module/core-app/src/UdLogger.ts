/**
 *  The logger application events
 *
 * @author Sergey Shishigin
 * @date 2020-02-28
 */
import {Dialog} from './util';
import {i18n} from './i18n';

export class UdLogger {
    public static log (msg: string) {
        console.log(msg);
    }

    public static warn (msg: string) {
        console.warn(msg);
    }

    public static error (msg: string) {
        if (process.env.NODE_ENV === 'development') {
            throw Error(msg);
        } else {
            // TODO: In the future, use logging here (e.g. Uni data Journal. error(msg))
            UdLogger.showSystemError(msg);
        }
    }

    private static showSystemError (msg: string) {
        const prefix = i18n.t('app.error>appError'),
              postfix = i18n.t('app.error>contactSupport');

        let fullMsg = prefix + '. ' + msg + '. ' + postfix;

        Dialog.showError(fullMsg);
    }
}
