declare global {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    interface Window {
        Unidata: any;
    }
}

export * from './DateTimeUtil';

export * from './TypeChecker';

export * from './ValidationUtils';

export {Dialog} from './Dialog';

export {Uuid} from './Uuid';

export {hasPrintableChars} from './StringUtil';
