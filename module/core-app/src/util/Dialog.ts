/**
 * Utility class for displaying notifications (toast)
 *
 * @author Denis Makarov
 * @date 2018-10-19
 */

import {ToastStore} from '../store/ToastStore';
import {message} from 'antd';
import {ErrorDetail, ToastTypeEnum} from '../type/ToastType';
import {ServerErrorSeverity} from '../network/type/ServerErrorSeverity';
import {Uuid} from './Uuid';

export class Dialog {

    private static store: ToastStore;

    public static getStore (): ToastStore {
        if (!Dialog.store) {
            Dialog.store = new ToastStore();
        }

        return Dialog.store;
    }

    public static showMessage (message: string | string[], title?: string, detail?: ErrorDetail, manualclose?: boolean) {
        Dialog.getStore().add({id: Uuid.v1(), type: ToastTypeEnum.Info, description: message, title, detail, manualclose});
    }

    public static showError (
        message: string | string[],
        title?: string,
        detail?: ErrorDetail,
        manualclose?: boolean,
        severity?: ServerErrorSeverity
    ) {
        // don't hide the error if there are details
        if (detail) {
            manualclose = true;
        }

        Dialog.getStore().add({id: Uuid.v1(), type: ToastTypeEnum.Error, description: message, title, detail, manualclose, severity});
    }

    public static showWarning (message: string | string[], title?: string, detail?: ErrorDetail, manualclose?: boolean) {
        Dialog.getStore().add({id: Uuid.v1(), type: ToastTypeEnum.Warning, description: message, title, detail, manualclose});
    }

    public static showSimpleMessage (messageText: string) {
        // Transition period we make messages without a title and options in a new way
        //TODO: you can add the global configuration to the General configuration
        message.config({
            top: 70
        });

        message.success(messageText);
    }
}
