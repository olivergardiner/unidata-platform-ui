/**
 * A set of service methods for working with simple types
 *
 * @author Denis Makarov
 * @date 2019-07-18
 */
import isUndefined from 'lodash/isUndefined';

export function coalesceDefined (...args: any) {
    let len = args.length,
        i;

    for (i = 0; i < len; i++) {
        if (!isUndefined(args[i])) {
            return args[i];
        }
    }

    return undefined;
}
