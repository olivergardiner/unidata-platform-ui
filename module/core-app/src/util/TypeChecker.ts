/**
 *  WTF class ... or interface... or type ...$END$
 *
 * @author Sergey Shishigin
 * @date 2019-12-11
 */

export class TypeChecker {
     /**
     * Returns `true` if the passed value is a JavaScript Date object, `false` otherwise.
     */
     public static isDate (value: any): boolean {
        return toString.call(value) === '[object Date]';
    }
}
