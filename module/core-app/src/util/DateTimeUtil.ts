/**
 *  DateTime util
 *
 * @author Sergey Shishigin
 * @date 2019-10-25
 */
import * as moment from 'moment';
import {UnidataConfig} from '../UnidataConfig';

const TIME_FORMAT = 'hh:mm:ss';
const TIME_REGEX: RegExp = /^(([01][0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]$/;

export class DateTimeUtil {
    public static format (dt: Date, format: string) {
        return moment(dt).format(format);
    }

    public static formatToServer (dt: Date) {
        return this.format(dt, UnidataConfig.getServerDateTimeFormat());
    }

    public static isTimeAfter (time1: string, time2: string): boolean {
        return moment(time1, TIME_FORMAT).isAfter(moment(time2, TIME_FORMAT));
    }

    public static isValidTime (time: string): boolean {
        return TIME_REGEX.test(time);
    }
}
