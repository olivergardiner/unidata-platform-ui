/**
 * Generating unique UUIDs
 *
 * @author Brauer Ilya
 *
 * @date 2020-05-14
 */
import {v1 as uuidV1, v4 as uuidV4} from 'uuid';

export class Uuid {
    public static v1 () {
        return uuidV1();
    }

    public static v4 () {
        return uuidV4;
    }
}
