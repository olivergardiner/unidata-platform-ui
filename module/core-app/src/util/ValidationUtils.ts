/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-12-27
 */

import {i18n} from '../i18n';

export function getErrorMessage (model: any, fieldName: string): any {
    let result: any;

    const validationResult: any = model.lastValidationResult;

    if (!validationResult) { return result; }

    if (validationResult.get(fieldName)) {
        result = i18n.t(validationResult.get(fieldName)[0]);
    }

    return result;
}
