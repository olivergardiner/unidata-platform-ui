/**
 * Util for string
 *
 * @author Stebunov Vladimir
 *
 * @date 2020-05-14
 */

export function hasPrintableChars (s: string): boolean {
    return Boolean(s.trim().length);
}

export function capitalize (s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
}
