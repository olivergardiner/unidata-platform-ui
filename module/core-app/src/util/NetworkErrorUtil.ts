/**
 * Auxiliary class for working with errors sent from BE
 *
 * @author Ivan Marshalkin
 * @date 2020-02-18
 */

import {Nullable, AxiosResponse} from '@unidata/core';
import {ServerErrorSeverity} from '../network/type/ServerErrorSeverity';
import {ErrorDetail} from '../type';

interface IServerError {
    type: string;
    severity: ServerErrorSeverity;
    errorCode: string | null;
    userMessage: string | null;
    userMessageDetails: string | null;
    internalMessage: string | null;
    params: any[];
}


type ServerErrorSeverityLevel = {
    [K in ServerErrorSeverity]: number
};

const severityLevel: ServerErrorSeverityLevel = {
    [ServerErrorSeverity.LOW]: 0,
    [ServerErrorSeverity.NORMAL]: 1,
    [ServerErrorSeverity.HIGH]: 2,
    [ServerErrorSeverity.CRITICAL]: 3
};

export class NetworkErrorUtil {
    public static responseHasError (response?: AxiosResponse): boolean {
        let erros = NetworkErrorUtil.getErrors(response);

        return erros.length ? true : false;
    }

    public static getErrors (response?: AxiosResponse): IServerError[] {
        if (response && response.data && response.data.errors && response.data.errors.length) {
            return response.data.errors;
        }

        return [];
    }

    public static getErrorByCode (response: AxiosResponse, errorCode: string): Nullable<IServerError> {
        let errors = this.getErrors(response);
        let error: IServerError | undefined;

        error = errors.find(function (item) {
            return item.errorCode === errorCode;
        });

        return error ? error : null;
    }

    public static getErrorMessage (error: IServerError, defaultValue: string = ''): string {
        return error.userMessage ? error.userMessage : defaultValue;
    }

    public static getErrorMessageDetail (error: IServerError, defaultValue: string = ''): string {
        return error.userMessageDetails ? error.userMessageDetails : defaultValue;
    }

    public static getStackTrace (response?: AxiosResponse): string {
        let stackTrace: string = '';

        if (response && response.data && response.data.stackTrace) {
            stackTrace = response.data.stackTrace;
        }

        return stackTrace;
    }

    public static getResponseURL (response?: AxiosResponse): string {
        let url: string = '';

        if (response && response.request && response.request.responseURL) {
            url = response.request.responseURL;
        }

        return url;
    }

    public static getErrorMessages (response?: AxiosResponse): string[] {
        let errors = this.getErrors(response);

        return errors
            .map(function (error) {
                return NetworkErrorUtil.getErrorMessage(error);
            }).filter(function (msg) {
                return msg !== '';
            });
    }

    public static getErrorMessageDetails (response?: AxiosResponse): string[] {
        let errors = this.getErrors(response);

        return errors
            .map(function (error) {
                return NetworkErrorUtil.getErrorMessageDetail(error);
            }).filter(function (msg) {
                return msg !== '';
            });
    }

    public static getErrorMaxSeverity (response?: AxiosResponse): Nullable<ServerErrorSeverity> {
        const errors = this.getErrors(response);

        let maxSeverity: ServerErrorSeverity;

        if (!errors.length) {
            return null;
        } else {
            maxSeverity = errors[0].severity;
        }

        errors.forEach(function (error) {
            let maxSeverityLevel = severityLevel[maxSeverity],
                errorSeverityLevel = severityLevel[error.severity];

            if (maxSeverityLevel < errorSeverityLevel) {
                maxSeverity = error.severity;
            }
        });

        return maxSeverity;
    }

    /**
     * Making additional fields for the message from the request and error
     *
     * @param response
     */
    public static getErrorDetails (response: AxiosResponse): ErrorDetail {
        const errors = NetworkErrorUtil.getErrors(response),
          opts: ErrorDetail = {
            errorUrl: NetworkErrorUtil.getResponseURL(response),
            stackTrace: NetworkErrorUtil.getStackTrace(response),
            userMessage: NetworkErrorUtil.getErrorMessage(errors[0])
        };

        let details = NetworkErrorUtil.getErrorMessageDetails(response);

        if (details.length > 0) {
            opts.userMessageDetails = details.join('\n');
        }

        return opts;
    }

}
