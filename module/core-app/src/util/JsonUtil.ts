/**
 * A utility class for working with json
 *
 * @author Ivan Marshalkin
 * @date 2020-05-29
 */
import {JSONValue} from '../type/Json';

export class JsonUtil {
    /**
     * Returns the decoded value, or throws an exception if the value cannot be decoded. If the parameter is specified
     * safe = true returns null instead of throwing an exception
     *
     * @param json
     * @param safe
     */
    public static decode (json: string, safe: boolean): JSONValue | null {
        try {
            return window.JSON.parse(json);
        } catch (e) {
            if (safe) {
                return null;
            }

            throw e;
        }
    }
}
