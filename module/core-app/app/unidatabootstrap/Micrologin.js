import {Microloader} from "./Microloader";

function getDefaultLocale () {
    var availableLocale = ['en', 'ru'],
        browserLocale = navigator && navigator.language && navigator.language.slice(0, 2);

    return availableLocale.indexOf(browserLocale) > -1 ? browserLocale : 'en';
}

export var Micrologin = {
    isUnidataReact: true,

    dom: null,

    panelHeader: null,

    loginTab: null,
    warningTab: null,
    restoreTab: null,

    nameInput: null,
    passwordInput: null,
    nameInputWrap: null,
    passwordInputWrap: null,

    loginButton: null,
    localeSwitch: null,
    localeMenu: null,

    logoContainer: null,

    wrongAuthorizationText: null,

    warningTabTitle: null,
    warningTabText: null,
    warningTabBackButton: null,

    restorePasswordTitle: null,
    restorePasswordDescription: null,
    restoreTabInput: null,
    restoreTabInputLabel: null,
    restoreTabEmailInput: null,
    or: null,
    restoreButton: null,
    wrongRestoreText: null,
    passwordWasSent: null,
    forgotPasswordLink: null,
    restoreTabGoBackLink: null,

    focusCls: 'focused',
    loginBodyCls: 'un-login-body',

    authenticateData: null,

    locale: getDefaultLocale(),

    warningErrorCode: null,

    /**
     * Property to be redefined by the Customizer
     *
     * @returns {string|*}
     */
    platformFaviconUrl: 'react-resources/favicon.ico?v=6',

    /**
     * Property to be redefined by the Customizer
     *
     * @returns {string|*}
     */
    platformTitle: 'Unidata',

    translates: {
        ru: {
            signInButton: 'Войти',
            namePlaceholder: 'Имя пользователя',
            passwordPlaceholder: 'Пароль',
            warningTabTitle: 'ВНИМАНИЕ!',
            warningTabText: 'Срок действия лицензии истек',
            licenseeHardwareSecurityText: 'UUID установленной лицензии не совпадает с UUID сервера Юнидата',
            warningTabBackButton: 'Назад',
            wrongAuthorizationText: 'Неправильный логин или пароль.<br>Повторите еще раз.',
            unknownError: 'Ошибка связи с сервером. Обратитесь к администратору системы.',
            disallowAdminAppModeText: 'Недостаточно прав для входа в раздел администрирования',
            restorePasswordTitle: 'Выслать временный пароль',
            restorePasswordDescription: 'На ваш почтовый адрес придет новый пароль и инструкция по восстановлению доступа.',
            restoreButton: 'Выслать пароль',
            labelLogin: 'Введите логин:',
            emailPlaceholder: 'Почтовый адрес',
            wrongLoginOrEmail: 'Неправильный логин / почтовый адрес',
            forgotPassword: 'Забыли пароль?',
            passwordWasSent: 'Временный пароль выслан на ваш email. Повторная отправка будет доступна через: ',
            sec: ' сек.',
            cancel: 'Отмена',
            or: 'или'
        },
        en: {
            signInButton: 'Log in',
            namePlaceholder: 'Username',
            passwordPlaceholder: 'Password',
            warningTabTitle: 'ATTENTION!',
            warningTabText: 'The license has expired',
            licenseeHardwareSecurityText: 'UUID of the installed license does not match the UUID of the server',
            warningTabBackButton: 'Back',
            wrongAuthorizationText: 'Incorrect login or password. <br> Please try again.',
            unknownError: 'Error communicating with the server. Contact your system administrator.',
            disallowAdminAppModeText: 'You have insufficient login rights to administration panel',
            restorePasswordTitle: 'Send temp password',
            restorePasswordDescription: 'New password and instructions will be sent to your email.',
            restoreButton: 'Send password',
            labelLogin: 'Enter login:',
            emailPlaceholder: 'Email',
            wrongLoginOrEmail: 'Login / email is wrong',
            forgotPassword: 'Forgot your password?',
            passwordWasSent: 'New password was sent to email. Retry after: ',
            sec: ' secs',
            cancel: 'Cancel',
            or: 'or'
        }
    },

    initComponent: function () {
        var template = document.querySelector('#logintpl'),
            urlParams = this.getUrlParams(),
            restorePasswordDelay = this.readFromLocalStorage('restorePasswordDelay'),
            fragment;

        if (!template) {
            return;
        }

        fragment = jQuery(template.innerHTML);
        fragment.appendTo(jQuery('body'));

        jQuery('body').addClass(this.loginBodyCls);

        fragment.addClass(this.getPlatformBackgroundCls());
        fragment.addClass(this.getBackgroundCls());

        this.dom = fragment.get(0);

        this.initComponentReference();
        this.initComponentEvent();

        this.initCustomization();

        this.showTab(this.loginTab);
        jQuery(this.wrongAuthorizationText).hide();

        jQuery(this.logoContainer).append(Micrologin.getLogoTpl());

        this.disableLoginButton();

        this.setTranslate(this.locale);

        this.updatePlatformFavicon();
        this.updatePlatformTitle();

        if (urlParams['activationCode']) {
            this.activateTempRestorePassword(urlParams['activationCode']);

            window.history.pushState(null, null, window.location.href.substring(0, window.location.href.indexOf('?activationCode')));
            window.history.pushState(null, null, window.location.href.substring(0, window.location.href.indexOf('&activationCode')));
        }

        if (restorePasswordDelay) {
            this.restorePasswordDelay = restorePasswordDelay;
            setTimeout(this.setRestorePasswordTimer.bind(this), 1000);
        }
    },

    destroyComponent: function () {
        jQuery('body').removeClass(this.loginBodyCls);
        jQuery(this.dom).remove();
    },

    initComponentReference: function () {
        var fragment = jQuery(this.dom);

        if (!fragment) {
            return;
        }

        this.panelHeader = fragment.find('.panel-heading').get(0);

        this.loginTab = fragment.find('.un-login-tab').get(0);
        this.warningTab = fragment.find('.un-warning-tab').get(0);
        this.restoreTab = fragment.find('.un-restore-tab').get(0);

        this.nameInput = fragment.find('input[type=text]').get(0);
        this.passwordInput = fragment.find('input[type=password]').get(0);

        this.nameInputWrap = fragment.find('.form-group:has(input[type=text])').get(0);
        this.passwordInputWrap = fragment.find('.form-group:has(input[type=password])').get(0);

        this.localeSwitch = fragment.find('.un-local-switch').get(0);
        this.localeMenu = jQuery(this.localeSwitch).find('.menu').get(0);

        this.logoContainer = fragment.find('.logo-container').get(0);

        this.wrongAuthorizationText = jQuery(this.loginTab).find('[role=wrong-authorization-text]').get(0);
        this.loginButton = jQuery(this.loginTab).find('.btn').get(0);

        this.warningTabTitle = jQuery(this.warningTab).find('[role=warning-title]').get(0);
        this.warningTabText = jQuery(this.warningTab).find('[role=warning-text]').get(0);
        this.warningTabBackButton = jQuery(this.warningTab).find('[role=warningtab-back-btn]').get(0);
        this.forgotPasswordLink = fragment.find('[role=forgot-password]').get(0);

        this.restorePasswordTitle = jQuery(this.restoreTab).find('[role=restore-password-title]').get(0);
        this.restorePasswordDescription = jQuery(this.restoreTab).find('[role=restore-password-description]').get(0);
        this.restoreTabInput = jQuery(this.restoreTab).find('input[type=text]').get(0);
        this.restoreTabInputLabel = jQuery(this.restoreTab).find('[role=login]').get(0);
        this.restoreTabEmailInput = jQuery(this.restoreTab).find('input[type=text]').get(1);
        this.or = jQuery(this.restoreTab).find('[role=or]').get(0);
        this.restoreButton = jQuery(this.restoreTab).find('.btn').get(0);
        this.wrongRestoreText = jQuery(this.restoreTab).find('[role=wrong-restore-text]').get(0);
        this.passwordWasSent = jQuery(this.restoreTab).find('[role=password-was-sent]').get(0);
        this.restoreTabGoBackLink = jQuery(this.restoreTab).find('[role=go-back]').get(0);
    },

    initComponentEvent: function () {
        jQuery('body').bind('click', this.onBodyClick.bind(this));

        jQuery(this.nameInput).bind('focus', this.onNameInputFocus.bind(this));
        jQuery(this.nameInput).bind('blur', this.onNameInputBlur.bind(this));

        jQuery(this.passwordInput).bind('focus', this.onPasswordInputFocus.bind(this));
        jQuery(this.passwordInput).bind('blur', this.onPasswordInputBlur.bind(this));

        jQuery(this.nameInput).bind('keydown', this.onNameInputKeyDown.bind(this));
        jQuery(this.passwordInput).bind('keydown', this.onPasswordInputKeyDown.bind(this));

        jQuery(this.nameInput).bind('change paste keydown', this.onNameInputChange.bind(this));
        jQuery(this.passwordInput).bind('change paste keydown', this.onPasswordInputChange.bind(this));

        jQuery(this.loginButton).bind('click', this.onLoginButtonClick.bind(this));
        jQuery(this.warningTabBackButton).bind('click', this.onWarningTabBackButtonClick.bind(this));

        jQuery(this.localeSwitch).find('.menu-wrap a').bind('click', this.onLocaleSwitchItemClick.bind(this));
        jQuery(this.localeSwitch).bind('click', this.onLocaleSwitchClick.bind(this));

        jQuery(this.forgotPasswordLink).bind('click', this.onForgetPasswordLinkClick.bind(this));
        jQuery(this.restoreTabInput).bind('keyup', this.onRestoreTabLoginInputChange.bind(this));
        jQuery(this.restoreTabEmailInput).bind('keyup', this.onRestoreTabEmailInputChange.bind(this));
        jQuery(this.restoreButton).bind('click', this.onRestoreButtonClick.bind(this));
        jQuery(this.restoreButton).bind('click', this.onRestoreButtonClick.bind(this));
        jQuery(this.restoreTabGoBackLink).bind('click', this.onWarningTabBackButtonClick.bind(this));
    },

    /**
     * Entry point
     */
    runMicrologin: function () {
        var sso = Micrologin.getQueryParam('sso');

        // the parameter so == true means integration with custom authorization
        // and you need to send an authorization request in any case
        if (sso === 'true' || sso === 'on') {
            Micrologin.doSsoAuthorization();
            // authorization by token is possible
        } else if (Micrologin.getToken()) {
            Micrologin.doTokenAuthorization();
        } else {
            Micrologin.initComponent();
        }
    },

    /**
     * Starts downloading the app
     */
    runMicroloader: function () {
        Microloader.runMicroloader();
    },

    /**
     * Returns true if the user is allowed to launch the app, otherwise returns an error code
     *
     * @param data
     * @returns {*}
     */
    authorizationGuard: function (data) {
        data = data || {};

        if (window.customerConfig.APP_TYPE.indexOf('dataAdmin') > -1 || window.customerConfig.APP_TYPE.indexOf('systemAdmin') > -1) {
            if (!this.allowAdminAppMode(data)) {
                return 'DISALLOW_ADMIN_APP_MODE';
            }
        }

        return true;
    },

    allowAdminAppMode: function (data) {
        var allow = false,
            resourceName,
            adminRights;

        if (data && data.userInfo && data.userInfo.admin) {
            return true;
        }

        adminRights = [
            'ADMIN_CLASSIFIER_MANAGEMENT',
            'ADMIN_DATA_MANAGEMENT',
            'ADMIN_MATCHING_MANAGEMENT',
            'ADMIN_SYSTEM_MANAGEMENT',
            'USER_MANAGEMENT',
            'ROLE_MANAGEMENT',
            'SECURITY_LABELS_MANAGEMENT',
            'DATA_OPERATIONS_MANAGEMENT',
            'PLATFORM_PARAMETERS_MANAGEMENT',
            'EXECUTE_DATA_OPERATIONS',
            'AUDIT_ACCESS'
        ];

        jQuery.each(data.rights || [], function (index, right) {
            resourceName = '';

            if (right && right.securedResource) {
                resourceName = right.securedResource.name;
            }

            if (adminRights.indexOf(resourceName) !== -1) {
                if (right.create || right.read || right.update || right.delete) {
                    allow = true;
                }
            }
        });

        return allow;
    },

    /**
     * Method for customizing
     *
     * @returns {string|*}
     */
    initCustomization: function () {
    },

    getLocale: function () {
        return this.locale;
    },

    /**
     * Method for customizing
     *
     * @returns {string|*}
     */
    getLogoTpl: function () {
        var locale = this.getLocale(),
            html;

        html = '<object class="un-login-logo" data="react-resources/unidatabootstrap/images/logoplatform-' + locale + '.svg" type="image/svg+xml" style="width: 200px;">' + // jscs:ignore maximumLineLength
            '<img src="react-resources/unidatabootstrap/images/logoplatform-' + locale + '.png" alt="Platform logo" />' +
            '</object>';

        return html;
    },

    /**
     * Method for customizing
     *
     * @returns {string|*}
     */
    getPlatformBackgroundCls: function () {
        var cls = 'un-application-usermode',
            appType = window.customerConfig.APP_TYPE;

        if (appType.indexOf('dataSteward') > -1) {
            cls = 'un-application-usermode';
        } else if (appType.indexOf('dataAdmin') > -1 || appType.indexOf('systemAdminAdmin') > -1) {
            cls = 'un-application-adminmode';
        }

        return cls;
    },

    /**
     * Method for customizing
     *
     * @returns {string|*}
     */
    getBackgroundCls: function () {
        return '';
    },

    /**
     * Updates the site's favicon
     */
    updatePlatformFavicon: function () {
        var head = document.head || document.getElementsByTagName('head')[0],
            element = document.querySelectorAll('link[rel="icon"]')[0],
            url = this.platformFaviconUrl;

        if (element) {
            head.removeChild(element);
        }

        element = document.createElement('link');
        element.href = url;
        element.rel = 'icon';
        element.type = 'image/x-icon';

        head.appendChild(element);
    },

    /**
     * Method for customizing
     *
     * @returns {*}
     */
    transformResourceBundle: function (language, namespace, data) {
        return data;
    },

    /**
     * Updates the tab title of the website
     */
    updatePlatformTitle: function () {
        var element = document.querySelectorAll('title')[0];

        if (element) {
            element.innerHTML = this.platformTitle;
        }
    },

    onBodyClick: function () {
        jQuery(this.localeMenu).hide();
    },

    onLocaleSwitchClick: function (e) {
        jQuery(this.localeMenu).toggle();
        e.stopPropagation();
    },

    onNameInputFocus: function () {
        jQuery(this.nameInputWrap).addClass(this.focusCls);
    },

    onNameInputBlur: function () {
        jQuery(this.nameInputWrap).removeClass(this.focusCls);
    },

    onPasswordInputFocus: function () {
        jQuery(this.passwordInputWrap).addClass(this.focusCls);
    },

    onPasswordInputBlur: function () {
        jQuery(this.passwordInputWrap).removeClass(this.focusCls);
    },

    onLoginButtonClick: function () {
        this.doLogin();
    },

    onWarningTabBackButtonClick: function () {
        this.showTab(this.loginTab);
    },

    onNameInputChange: function (e) {
        this.updateLoginButtonDisable();

        if (e.keyCode !== 13) {
            this.animateWrongAuthorizationText();
        }
    },

    onPasswordInputChange: function (e) {
        this.updateLoginButtonDisable();

        if (e.keyCode !== 13) {
            this.animateWrongAuthorizationText();
        }
    },

    onNameInputKeyDown: function (e) {
        if (e.keyCode === 9 || e.keyCode === 13) {
            e.stopPropagation();
            e.preventDefault();
        }

        switch (e.keyCode) {
            case 9:
                jQuery(this.passwordInput).focus();
                jQuery(this.passwordInput).select();
                break;
            case 13:
                this.doLogin();
                break;
        }
    },

    onPasswordInputKeyDown: function (e) {
        if (e.keyCode === 9 || e.keyCode === 13) {
            e.stopPropagation();
            e.preventDefault();
        }

        switch (e.keyCode) {
            case 9:
                jQuery(this.nameInput).focus();
                jQuery(this.nameInput).select();
                break;
            case 13:
                this.doLogin();
                break;
        }
    },

    onRestoreTabLoginInputChange: function (e) {
        jQuery(this.restoreTabEmailInput).attr('disabled', jQuery(this.restoreTabInput).val().length !== 0);
        jQuery(this.wrongRestoreText).html('');
    },

    onRestoreTabEmailInputChange: function (e) {
        jQuery(this.restoreTabInput).attr('disabled', jQuery(this.restoreTabEmailInput).val().length !== 0);
        jQuery(this.wrongRestoreText).html('');
    },

    onLocaleSwitchItemClick: function (e) {
        var locale = e.target.getAttribute('locale');

        if (!locale) {
            return;
        }

        this.locale = locale;

        this.setTranslate(locale);

        jQuery(this.nameInput).focus();
    },

    onForgetPasswordLinkClick: function () {
        this.showTab(this.restoreTab);
    },

    onRestoreButtonClick: function () {
        this.doRestorePassword();
    },

    /**
     * Animates the text of a failed authorization
     */
    animateWrongAuthorizationText: function () {
        if (jQuery(this.wrongAuthorizationText).is(':visible')) {
            jQuery(this.wrongAuthorizationText).stop().animate({opacity: '0.3'}, 500);
        }
    },

    /**
     * Sets the locale
     *
     * @param locale
     */
    setTranslate: function (locale) {
        var translate = this.getTranslate(locale);

        if (!translate) {
            return;
        }

        jQuery(this.loginButton).html(translate.signInButton);
        jQuery(this.nameInput).attr('placeholder', translate.namePlaceholder);
        jQuery(this.passwordInput).attr('placeholder', translate.passwordPlaceholder);

        jQuery(this.warningTabTitle).text(translate.warningTabTitle);
        jQuery(this.wrongAuthorizationText).html(translate.wrongAuthorizationText);
        jQuery(this.warningTabBackButton).html(translate.warningTabBackButton);

        jQuery(this.restorePasswordTitle).html(translate.restorePasswordTitle);
        jQuery(this.restorePasswordDescription).html(translate.restorePasswordDescription);
        jQuery(this.restoreTabInput).attr('placeholder', translate.namePlaceholder);
        jQuery(this.restoreTabEmailInput).attr('placeholder', translate.emailPlaceholder);
        jQuery(this.or).html(translate.or);
        jQuery(this.restoreButton).html(translate.restoreButton);
        jQuery(this.forgotPasswordLink).html(translate.forgotPassword);
        jQuery(this.restoreTabGoBackLink).html(translate.cancel);
        jQuery(this.passwordWasSent).html(translate.passwordWasSent);

        jQuery(this.warningTabText).html(this.buildWarningTabText());

        jQuery(this.localeMenu).find('a').removeClass('selected');
        jQuery(this.localeMenu).find('a[locale=' + locale + ']').addClass('selected');

        jQuery(this.logoContainer).empty();
        jQuery(this.logoContainer).append(Micrologin.getLogoTpl());
    },

    formatDate: function (date, locale) {
        var year = date.getFullYear(),
            month = String(date.getMonth() + 1).padStart(2, '0'),
            day = String(date.getDate()).padStart(2, '0'),
            hours = String(date.getHours()).padStart(2, '0'),
            minutes = String(date.getMinutes()).padStart(2, '0'),
            dateText;

        if (locale === 'ru') {
            dateText = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes;
        } else if (locale === 'en') {
            dateText = month + '/' + day + '/' + year + ' ' + hours + ':' + minutes;
        }

        return dateText;
    },

    /**
     * Returns the authorization token
     *
     * @returns {string}
     */
    getToken: function () {
        var token = localStorage.getItem('ud-token');

        if (token === 'null') {
            token = null;
        }

        return token;
    },

    /**
     * Remembers the authorization token
     *
     * @param token
     */
    setToken: function (token) {
        if (token) {
            localStorage.setItem('ud-token', token);
        } else {
            localStorage.removeItem('ud-token');
        }
    },

    /**
     * Returns an object with translation texts for the passed locale / or returns a translation for the current locale
     *
     * @param locale
     * @returns {*}
     */
    getTranslate: function (locale) {
        if (!locale) {
            locale = this.getLocale();
        }

        return this.translates[locale];
    },

    /**
     * Returns the value of the get parameter
     *
     * @param param
     * @returns {*}
     */
    getQueryParam: function (param) {
        var found = null;

        window.location.search.substr(1).split('&').forEach(function (item) {
            if (param == item.split('=')[0]) {
                found = item.split('=')[1];
            }
        });

        return found;
    },

    getAuthorizationInputData: function () {
        var data;

        data = {
            login: jQuery(this.nameInput).val(),
            password: jQuery(this.passwordInput).val()
        };

        return data;
    },

    updateLoginButtonDisable: function () {
        var data = this.getAuthorizationInputData();

        if (data.login.length && data.password.length) {
            this.enableLoginButton();
        } else {
            this.disableLoginButton();
        }
    },

    disableLoginButton: function () {
        jQuery(this.loginButton).attr('disabled', true);
    },

    enableLoginButton: function () {
        jQuery(this.loginButton).attr('disabled', false);
    },

    showTab: function (tab) {
        jQuery(this.loginTab).hide();
        jQuery(this.warningTab).hide();
        jQuery(this.restoreTab).hide();

        if (tab === this.restoreTab) {
            jQuery(this.panelHeader).hide();
            jQuery(this.passwordWasSent).hide();
        } else {
            jQuery(this.panelHeader).show();
        }

        jQuery(tab).show();
    },

    /**
     * Performs authorization with the data entered by the user
     */
    doLogin: function () {
        var me = this,
            authData = this.getAuthorizationInputData(),
            promise;

        if (!String(authData.login).length || !String(authData.password).length) {
            return;
        }

        promise = this.login(authData.login, authData.password, this.locale);
        promise
            .done(function (data) {
                this.authenticateData = data;
                this.setToken(data.token);

                window.authenticateData = data;

                jQuery(this.wrongAuthorizationText).hide();

                this.destroyComponent();

                this.runMicroloader();
            }.bind(this))
            .fail(function (data) {
                var handled = false;

                this.authenticateData = null;
                window.authenticateData = null;
                this.setToken(null);

                if (data === 'DISALLOW_ADMIN_APP_MODE') {
                    me.warningErrorCode = 'DISALLOW_ADMIN_APP_MODE';

                    jQuery(this.warningTabText).html(this.buildWarningTabText());

                    this.showTab(this.warningTab);

                    handled = true;
                } else if (data && data.errors) {
                    data.errors.forEach(function (item) {
                        if (item.errorCode === 'EX_SECURITY_LICENSE_INVALID' ||
                            item.errorCode === 'EX_SECURITY_HW_FOR_LICENSE_INVALID') {
                            me.warningErrorCode = item.errorCode;

                            if (item.errorCode === 'EX_SECURITY_LICENSE_INVALID') {
                                // при таком коде ошибки бекенд присылает дату истечения лицензии в секции userMessageDetails
                                this.licenseeExpireDate = Date.parse(item.userMessageDetails, this.dateTimeFormat);

                                if (this.licenseeExpireDate) {
                                    this.licenseeExpireDate = new Date(this.licenseeExpireDate);
                                }
                            }

                            jQuery(this.warningTabText).html(this.buildWarningTabText());

                            this.showTab(this.warningTab);

                            handled = true;
                        } else if (item.errorCode === 'EX_SECURITY_CANNOT_LOGIN') {
                            jQuery(this.wrongAuthorizationText).show();
                            jQuery(this.wrongAuthorizationText).stop().css({opacity: '1'});

                            handled = true;
                        }
                    }.bind(this));
                }

                if (!handled) {
                    this.handleXhrErrors(data);
                }
            }.bind(this));
    },

    buildWarningTabText: function () {
        var translate = this.getTranslate(),
            warningTabText = translate.warningTabText,
            licenseeExpireDate = this.licenseeExpireDate,
            txt;

        switch (this.warningErrorCode) {
            case 'EX_SECURITY_LICENSE_INVALID':
                txt = warningTabText;

                if (licenseeExpireDate) {
                    txt = warningTabText + '<br/>' + this.formatDate(licenseeExpireDate, this.getLocale());
                }
                break;
            case 'EX_SECURITY_HW_FOR_LICENSE_INVALID':
                txt = translate.licenseeHardwareSecurityText;
                break;
            case 'DISALLOW_ADMIN_APP_MODE':
                txt = translate.disallowAdminAppModeText;
                break;
        }

        return txt;
    },

    /**
     * Performs user authorization using a token
     */
    doTokenAuthorization: function () {
        var me = this,
            token = this.getToken(),
            promise;

        promise = this.authenticate(token);
        promise
            .done(function (data) {
                this.authenticateData = data;
                window.authenticateData = data;
                this.setToken(data.token);
                this.setTranslate(data.userInfo.locale);

                this.runMicroloader();
            }.bind(this))
            .fail(function (data) {
                this.authenticateData = null;
                window.authenticateData = null;
                this.setToken(null);

                this.initComponent();

                if (data === 'DISALLOW_ADMIN_APP_MODE') {
                    me.warningErrorCode = 'DISALLOW_ADMIN_APP_MODE';

                    jQuery(this.warningTabText).html(this.buildWarningTabText());

                    this.showTab(this.warningTab);
                } else if (data.status !== 401) {
                    this.handleXhrErrors(data.responseJSON);
                }
            }.bind(this));
    },

    /**
     * Performs authorization for the user subject to the authorization of the integrator
     */
    doSsoAuthorization: function () {
        var login = '',
            password = '',
            promise;

        promise = this.login(login, password, this.locale);
        promise
            .done(function (data) {
                this.authenticateData = data;
                this.setToken(data.token);

                this.runMicroloader();
            }.bind(this))
            .fail(function () {
                this.authenticateData = null;
                this.setToken(null);

                this.initComponent();
            }.bind(this));
    },

    /**
     * Sends a password recovery request
     */
    doRestorePassword: function () {
        var me = this,
            translate = this.getTranslate(),
            login = jQuery(this.restoreTabInput).val(),
            email = jQuery(this.restoreTabEmailInput).val(),
            data = {},
            isSending = this.isSending,
            promise,
            values;

        if (isSending) {
            return;
        }

        this.isSending = true;
        this.restorePasswordDelay = 60;

        jQuery(this.wrongRestoreText).html('');

        if (login && login.length > 1) {
            data['login'] = login;
        }

        if (email && email.indexOf('@') > -1) {
            data['email'] = email;
        }

        values = Object.keys(data).map(function (key) {
            return data[key];
        });

        if (values.length === 0) {
            jQuery(this.wrongRestoreText).html(translate.wrongLoginOrEmail);
            this.isSending = false;

            return;
        }

        promise = this.restorePassword(data);
        promise
            .done(function () {
                setTimeout(this.setRestorePasswordTimer.bind(this), 1000);
            }.bind(this))
            .fail(function () {
                setTimeout(this.setRestorePasswordTimer.bind(this), 1000);
            }.bind(this));
    },

    setRestorePasswordTimer: function () {
        var translate = this.getTranslate();

        this.restorePasswordDelay -= 1;
        this.writeToLocalStorage('restorePasswordDelay', this.restorePasswordDelay);

        if (this.restorePasswordDelay > 0) {
            jQuery(this.restoreButton).attr('disabled', true);
            jQuery(this.passwordWasSent).show();
            jQuery(this.passwordWasSent).html(translate.passwordWasSent + this.restorePasswordDelay + translate.sec);
            this.timer = setTimeout(this.setRestorePasswordTimer.bind(this), 1000);
        } else {
            this.isSending = false;
            jQuery(this.passwordWasSent).hide();
            jQuery(this.restoreButton).attr('disabled', false);
            this.removeFromLocalStorage('restorePasswordDelay');
        }
    },

    login: function (login, password, locale) {
        var me = this,
            deferred = jQuery.Deferred(),
            data;

        data = {
            password: password || '',
            userName: login || '',
            locale: locale
        };

        jQuery.ajax({
            type: 'POST',
            url: Microloader.buildServerUrl('core/authentication/login'),
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data, textStatus, xhr) {
                var guardCode;

                if (xhr && xhr.status === 200 && data && data.success) {
                    guardCode = me.authorizationGuard(data.content);

                    if (guardCode !== true) {
                        deferred.reject(guardCode);
                    }

                    deferred.resolve(data.content);
                } else {
                    deferred.reject(xhr.responseJSON);
                }
            },
            error: function (xhr) {
                deferred.reject(xhr.responseJSON);
            }
        });

        return deferred.promise();
    },

    authenticate: function (token) {
        var me = this,
            deferred = jQuery.Deferred();

        jQuery.ajax({
            type: 'GET',
            url: Microloader.buildServerUrl('core/authentication/get-current-user'),
            data: {
                '_dc': Number(new Date())
            },
            headers: {
                'Authorization': token
            },
            success: function (data, textStatus, xhr) {
                var guardCode;

                if (xhr && xhr.status === 200 && data && data.token) {
                    guardCode = me.authorizationGuard(data);

                    if (guardCode !== true) {
                        deferred.reject(guardCode);
                    }

                    deferred.resolve(data);
                } else {
                    deferred.reject(xhr);
                }
            },
            error: function (xhr) {
                deferred.reject(xhr);
            }
        });

        return deferred.promise();
    },

    restorePassword: function (data) {
        var me = this,
            deferred = jQuery.Deferred();

        jQuery.ajax({
            type: 'POST',
            url: Microloader.buildServerUrl('core/security/user/forgot-password'),
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data, textStatus, xhr) {
                var guardCode;

                if (xhr && xhr.status === 200 && data && data.success) {
                    guardCode = me.authorizationGuard(data.content);

                    if (guardCode !== true) {
                        deferred.reject(guardCode);
                    }

                    deferred.resolve(data.content);
                } else {
                    deferred.reject(xhr.responseJSON);
                }
            },
            error: function (xhr) {
                deferred.reject(xhr.responseJSON);
            }
        });

        return deferred.promise();
    },

    activateTempRestorePassword: function (token) {
        var me = this,
            deferred = jQuery.Deferred();

        jQuery.ajax({
            type: 'GET',
            url: Microloader.buildServerUrl('core/authentication/activate-password'),
            data: {
                'activationCode': token
            }
        });

        return deferred.promise();
    },

    /**
     * Loads customization for the login form
     *
     * @param data
     * @param nextCallback
     */
    loadMicrologinCustomization: function (data, nextCallback) {
        var url = Microloader.buildCustomerUrl('CUX/Micrologin.js');

        Microloader.loadScript(url,
            function () {
                if (Microloader.isFunction(nextCallback)) {
                    nextCallback();
                }
            },
            function () {
                // for this method we do not care that this file is not loaded because it may not technically exist and that means
                // behavior is not overridden
                if (Microloader.isFunction(nextCallback)) {
                    nextCallback();
                }
            }
        );
    },

    /**
     * Handles erroneous requests with BETWEEN
     *
     * @param data
     */
    handleXhrErrors: function (data) {
        var msgs = [],
            translate = this.getTranslate(),
            msg = translate.unknownError;

        if (data && data.errors) {
            data.errors.forEach(function (item) {
                if (item.userMessage) {
                    msgs.push(item.userMessage);
                }
            }.bind(this));
        }

        if (msgs.length) {
            msg = msgs.join('<br>');
        }

        iziToast.show({
            message: msg,
            timeout: null,
            progressBar: false,
            icon: 'icon-notification-circle',
            class: 'un-error',
            transitionIn: null,
            transitionOut: null,
            transitionInMobile: null,
            transitionOutMobile: null
        });
    },

    getUrlParams: function () {
        var params = {},
            parts;

        parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            params[key] = value;
        });

        return params;
    },

    writeToLocalStorage: function (key, value) {
        var storage = window.localStorage;

        if (storage) {
            storage.setItem(key, value);
        }
    },

    readFromLocalStorage: function (key) {
        var storage = window.localStorage;

        if (storage) {
            return storage.getItem(key);
        }
    },

    removeFromLocalStorage: function (key) {
        var storage = window.localStorage;

        if (storage) {
            storage.removeItem(key);
        }
    }
};
