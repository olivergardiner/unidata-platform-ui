import * as aja from 'aja';
import {Micrologin} from './Micrologin';

export var Microloader = {
    customerConfig: null,
    integrationMode: false, // indicates whether the extjs application needs to be integrated
    isFunction: function (value) {
        return Boolean(value) && typeof value === 'function';
    },
    Array: {
        indexOf: function (array, item, from) {
            return Array.prototype.indexOf.call(array, item, from);
        },
        unique: function (array) {
            var clone = [],
                i = 0,
                ln = array.length,
                item;

            for (; i < ln; i++) {
                item = array[i];

                if (Microloader.Array.indexOf(clone, item) === -1) {
                    clone.push(item);
                }
            }

            return clone;
        },
        each: function (array, fn, scope, reverse) {
            var i,
                ln = array.length;

            if (reverse !== true) {
                for (i = 0; i < ln; i++) {
                    if (fn.call(scope || array[i], array[i], i, array) === false) {
                        return i;
                    }
                }
            } else {
                for (i = ln - 1; i > -1; i--) {
                    if (fn.call(scope || array[i], array[i], i, array) === false) {
                        return i;
                    }
                }
            }

            return true;
        }
    },
    Object: {
        isEmpty: function (object) {
            var key;

            for (key in object) {
                if (object.hasOwnProperty(key)) {
                    return false;
                }
            }

            return true;
        }
    },
    String: {
        trim: function (string) {
            var trimRegex;

            // jscs:disable
            trimRegex = /^[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000]+|[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000]+$/g;
            // jscs:enable

            if (string) {
                string = string.replace(trimRegex, '');
            }

            return string || '';
        }
    },
    chain: (function () {
        // see https://jsfiddle.net/rcknr/fv289szj/
        function bind(f, g) {
            return function (a, callback, errback) {
                f(a, function (result) {
                    return g(result, callback, errback);
                }, errback);
            };
        }

        function chain() {
            var args = Array.prototype.slice.call(arguments),
                f = args.shift();

            while (args.length > 0) {
                f = bind(f, args.shift());
            }

            return f;
        }

        return chain;
    }()),
    loadJson: function (url, successCallback, errorCallback, opt) {
        var timeout;

        opt = opt || {};
        timeout = opt.timeout || 30000;

        aja()
            .url(url)
            .cache(false)
            .type('html')
            .timeout(timeout)
            .on('success', function (jsonData) {
                successCallback(jsonData);
            })
            .on('error', function () {
                errorCallback();
            })
            .on('40x', function () {
                errorCallback();
            })
            .on('50x', function () {
                errorCallback();
            })
            .on('timeout', function () {
                errorCallback();
            })
            .go();
    },
    loadScript: function (url, successCallback, errorCallback) {
        aja()
            .url(url)
            .cache(false)
            .type('script')
            .on('success', function (data) {
                successCallback(data);
            })
            .on('error', function () {
                errorCallback();
            })
            .on('40x', function () {
                errorCallback();
            })
            .on('50x', function () {
                errorCallback();
            })
            .go();
    },
    runMicroloader: function () {
        Microloader.chain(
            Microloader.initMicroloader,
            Microloader.fetchPlatformConfig,
            Microloader.fetchReactManifest,
            Microloader.bootstrapApplication,
            Microloader.loadInternationalizationJson,
            Microloader.bootstrapExtJsApplication,
            Microloader.launchApplication
        )();
    },

    initMicroloader: function (data, nextCallback) {
        // for the production build we force the integration of the extjs application
        if (window['unidataBuildDate'] !== '@BUILD_DATE@') {
            Microloader.integrationMode = true;

            if (Microloader.isFunction(nextCallback)) {
                nextCallback();
            }

            return;
        }

        // determining whether the extjs application is available for integration
        Microloader.loadJson('bootstrap.js',
            function (jsonData) {
                Microloader.integrationMode = true;

                if (Microloader.isFunction(nextCallback)) {
                    nextCallback();
                }
            },
            function () {
                if (Microloader.isFunction(nextCallback)) {
                    nextCallback();
                }
            }, {
                timeout: 3000
            });
    },

    loadCustomerJson: function (data, nextCallback) {
        var url;

        if (!Microloader.Object.isEmpty(Microloader.customerConfig)) {
            if (Microloader.isFunction(nextCallback)) {
                nextCallback();
            }

            return;
        }

        url = Microloader.buildCustomerUrl('customer.json');

        Microloader.loadJson(url,
            function (jsonData) {
                try {
                    Microloader.customerConfig = eval('(' + jsonData + ')');
                    // temporarily duplicating the customer Config in Sencha
                    window.customerConfig = Microloader.customerConfig;
                } catch (e) {
                    console.log('Unidata microloader: can not parse customer.json');

                    return;
                }

                if (Microloader.isFunction(nextCallback)) {
                    nextCallback();
                }
            },
            function () {
                console.log('Error: customer.json load failure');
                alert('customer.json load failure');


            });
    },
    initUnidataCustomerCustomization: function (data, nextCallback) {
        var match = window.location.search.match(/(\?|&)action\=([^&]*)/),
            customerCfg = Microloader.customerConfig,
            serverUrl = customerCfg.serverUrl,
            logoutUrl = serverUrl + 'core/authentication/logout',
            token = localStorage.getItem('ud-token');

        // log out if the GET parameter is present ?action=logout
        if (match && match[2] === 'logout') {
            aja()
                .url(logoutUrl)
                .method('POST')
                .header('Authorization', token)
                .header('Content-Type', 'application/json')
                .cache(false)
                .on('success', function () {
                    document.write('Произошел выход из системы');
                })
                .on('error', function () {
                    document.write('Выход из системы не произведен из-за ошибки');
                })
                .go();
        } else {
            if (Microloader.isFunction(nextCallback)) {
                nextCallback();
            }
        }
    },
    loadInternationalizationJson: function (data, nextCallback) {
        var itemCount = 0,
            loadedCount = 0,
            notLoadedCount = 0,
            languages = [],
            namespaces,
            defaultLng = 'ru',
            i18next = window.Unidata.i18n,
            locale = Micrologin.getLocale();

        // sencha dev server может быть не запущен
        if (!Microloader.integrationMode) {
            if (Microloader.isFunction(nextCallback)) {
                nextCallback();
            }

            return;
        }

        namespaces = [
            'common',
            'default',
            'glossary',
            'validation',
            'wiki'
        ];

        languages.push(defaultLng);
        languages.push(locale);

        languages = Microloader.Array.unique(languages);
        namespaces = Microloader.Array.unique(namespaces);

        itemCount = languages.length * namespaces.length;

        Microloader.Array.each(languages, function (language) {
            Microloader.Array.each(namespaces, function (namespace) {
                Microloader.loadJson('resources/locale/' + language + '/' + language + '-' + namespace + '.json',
                    function (data) {
                        loadedCount += 1;

                        console.log('Unidata microloader: i18next json loaded', language, namespace);

                        try {
                            data = JSON.parse(data);

                            data = Micrologin.transformResourceBundle(language, namespace, data);

                            i18next.addResourceBundle(language, namespace, data);
                        } catch (e) {
                            console.log('Unidata microloader: can not parse i18next json', language, namespace);
                        }

                        if (loadedCount === itemCount) {
                            if (Microloader.isFunction(nextCallback)) {
                                nextCallback();
                            }
                        } else if (loadedCount + notLoadedCount === itemCount) {
                            console.log('Unidata microloader: can not load i18n json');


                        }
                    },
                    function () {
                        notLoadedCount += 1;
                    }
                );
            });
        });
    },
    /**
     * Creating a platformConfig for storing data with backend
     * And get data for the current periods,
     */
    fetchPlatformConfig: function (data, nextCallback) {
        var backendPropertyGroup = 'unidata.properties.group.validity',
            validityStart = 'unidata.validity.period.start',
            validityEnd = 'unidata.validity.period.end',
            url = Microloader.buildServerUrl('core/configuration/' + backendPropertyGroup),
            token = localStorage.getItem('ud-token');

        // нет необходимости загружать данные с сервера, если лицензия исткла т.к. BE не ответит
        // и после обновления лицензии приложение будет перезапущено
        if (Micrologin && Micrologin.authenticateData && Micrologin.authenticateData.forceLicenseChange) {
            if (Microloader.isFunction(nextCallback)) {
                nextCallback();
            }

            return;
        }

        jQuery.ajax({
            url: url,
            method: 'GET',
            data: {
                '_dc': Date.now()
            },
            headers: {
                'Authorization': token
            },
            success: function (data, textStatus, xhr) {
                var validityDatesRecords = data,
                    start,
                    end;

                if (xhr && xhr.status === 200 && validityDatesRecords) {
                    Microloader.Array.each(validityDatesRecords, function (date) {
                        if (date.name === validityStart) {
                            start = date.value;

                            return false;
                        }

                    });

                    Microloader.Array.each(validityDatesRecords, function (date) {
                        if (date.name === validityEnd) {
                            end = date.value;

                            return false;
                        }

                    });

                    window.platformConfig = {
                        'VALIDITY_DATES': {}
                    };
                    window.platformConfig['VALIDITY_DATES']['START'] = start;
                    window.platformConfig['VALIDITY_DATES']['END'] = end;
                }

                if (Microloader.isFunction(nextCallback)) {
                    nextCallback();
                }
            }, failure: function (err) {
                if (err) {
                    console.log('Unidata microloader: error while trying to fetch platform config', err);
                }
            }
        });

    },
    fetchReactManifest: function (data, nextCallback) {
        var url = '',
            fileName = 'app-react.json';

        url = Microloader.buildCustomerUrl(fileName);

        Microloader.loadJson(url,
            function (jsonData) {
                try {
                    window.reactManifest = eval('(' + jsonData + ')');
                } catch (e) {
                    console.log('Unidata microloader: can not parse ' + fileName);

                    return;
                }

                if (Microloader.isFunction(nextCallback)) {
                    nextCallback();
                }
            },
            function () {
                console.log('Error: ' + fileName + ' load failure');
                alert(fileName + ' load failure');


            });
    },

    injectStyle: function (styleName, disableCache) {
        var head = document.querySelectorAll('head')[0],
            style = document.createElement('link');

        if (disableCache) {
            style.href = styleName + '?_dc=' + Date.now();
        } else {
            style.href = styleName
        }

        style.rel = 'stylesheet';

        // is IE?
        if (head.appendChild) {
            head.appendChild(style);
        } else {
            head.append(style);
        }
    },

    /**
     * Appends script tag to body
     *
     * @param scriptName - js file name
     * @param disableCache - flag determines whether we need to add _dc
     */
    injectScript: function (scriptName, disableCache) {
        var body = document.querySelectorAll('body')[0],
            script = document.createElement('script');

        script.type = 'text/javascript';

        if (disableCache) {
            script.src = scriptName + '?_dc=' + Date.now()
        } else {
            script.src = scriptName
        }

        // is IE?
        if (body.appendChild) {
            body.appendChild(script);
        } else {
            body.append(script);
        }
    },
    bootstrapApplication: function (data, nextCallback) {
        var body = document.querySelectorAll('body')[0],
            manifest = window.reactManifest,
            chunkNames = manifest.js,
            styleNames = manifest.css,
            fakeInputStyle = 'position: absolute; width: 0; height: 0; display: block; left: 0 !important; top: 0 !important; border: 0; margin: 0; padding: 0';

        // creating a basic parent container,
// as well as 2 fake fields to fix problems with "violent" autocomplete
// this way the app will not have autocomplete for username and password
// (the autocomplete login screen remains)
// learn more in UN-9816 chrome autocomplete bug
        body.innerHTML = '<div id="root"></div>'
            + '<input id="username" name="username" type="text" style="' + fakeInputStyle + '" />'
            + '<input id="password" name="password" type="password" style="' + fakeInputStyle + '" />';

        styleNames.forEach(function (styleName) {
            Microloader.injectStyle(styleName, false);  // css bundle already has hash in its name
        });

        chunkNames.forEach(function (scriptName) {
            Microloader.injectScript(scriptName, false); // react bundle already has hash in its name
        });

        window.addEventListener('unidata-react-initialized', function () {
            var nsPrefix = 'sencha_',
                defaultNsPrefix = nsPrefix + 'default:';

            window.Unidata.i18n = Object.assign(
                {},
                window.UnidataReact.i18n,
                {
                    t: function (translateKey, options) {
                        var nsSeparator = window.Unidata.i18n.options.nsSeparator;

                        if (translateKey.includes(nsSeparator)) {
                            translateKey = nsPrefix + translateKey
                        } else {
                            translateKey = defaultNsPrefix + translateKey
                        }

                        return window.UnidataReact.i18n.t(translateKey, options);
                    },
                    addResourceBundle: function (locale, namespace, data) {
                        return window.UnidataReact.i18n.addResourceBundle(locale, nsPrefix + namespace, data);
                    },
                    addResources: function (locale, namespace, data) {
                        return window.UnidataReact.i18n.addResources(locale, nsPrefix + namespace, data);
                    }
                });

            if (Microloader.isFunction(nextCallback)) {
                nextCallback();
            }

        });
    },

    bootstrapExtJsApplication: function (data, nextCallback) {
        var locale = Micrologin.getLocale();

        if (!process.env.EE_MODULES || process.env.EE_MODULES.length === 0) {
            if (Microloader.isFunction(nextCallback)) {
                nextCallback();
            }

            return;
        }

        // sencha dev server may not be running
        if (!Microloader.integrationMode) {
            if (Microloader.isFunction(nextCallback)) {
                nextCallback();
            }

            return;
        }

        window['Ext'] = window['Ext'] || {};

        // only for production
        if (locale && window['unidataBuildDate'] !== '@BUILD_DATE@') {
            window['Ext'].manifest = 'app-' + locale + '.json';
        }

        window.addEventListener('unidata-initialized', function () {
            if (Microloader.isFunction(nextCallback)) {
                nextCallback();
            }
        });

        Microloader.injectScript('bootstrap.js', true);
    },

    buildServerUrl: function (url) {
        var customerCfg = Microloader.customerConfig,
            serverUrl = '';

        if (customerCfg) {
            serverUrl = customerCfg.serverUrl;
        }

        return serverUrl + url;
    },
    buildCustomerUrl: function (url) {
        var customerUrl = Microloader.String.trim(String(window.customerUrl));

        customerUrl = customerUrl.replace(/\/+$/, '').replace(/\\+$/, '');

        if (customerUrl.length) {
            customerUrl += '/';
        }

        return customerUrl + url;
    },
    launchApplication: function () {
        window.UnidataReact.app.run();
    }
};

