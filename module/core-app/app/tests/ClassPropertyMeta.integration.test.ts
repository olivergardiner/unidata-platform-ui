/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-28
 */


import {ExtendedModel} from './models/ExtendedModel';
import {SimpleModel} from './models/SimpleModel';
import {ClassPropertyMeta} from '../../module/core/src/data/ClassPropertyMeta';
import {AbstractModel} from '../../module/core/src/data';

describe('ClassPropertyMeta Integration', () => {
    it('sets all prototype chain meta', () => {
        expect(ClassPropertyMeta.getDescriptors(ExtendedModel, 'fieldMainDescriptor').length).toBe(3);
        expect(ClassPropertyMeta.getDescriptors(SimpleModel, 'fieldMainDescriptor').length).toBe(3);
        expect(ClassPropertyMeta.getDescriptors(SimpleModel, 'hasOneMainDescriptor').length).toBe(2);
        expect(ClassPropertyMeta.getDescriptors(SimpleModel, 'hasManyMainDescriptor').length).toBe(1);
        expect(ClassPropertyMeta.getDescriptors(AbstractModel, 'fieldMainDescriptor').length).toBe(0);
    });

});
