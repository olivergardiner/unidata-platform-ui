/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-28
 */

import {AbstractModel, StringField, stringField} from '@unidata/core';
import {observable} from 'mobx';

export class PeopleModel extends AbstractModel {

    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public lastName: StringField;
}
