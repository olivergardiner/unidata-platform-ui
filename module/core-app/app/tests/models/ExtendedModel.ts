/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-28
 */

import {observable} from 'mobx';

import {arrayField, ArrayField, numberField, NumberField, StringField, stringField} from '@unidata/core';
import {SimpleModel} from './SimpleModel';

export class ExtendedModel extends SimpleModel {
    @observable
    @numberField({primaryKey: true})
    public id: NumberField;

    @observable
    @stringField()
    public country: StringField;

    @observable
    @arrayField()
    public childrenNames: ArrayField;
}
