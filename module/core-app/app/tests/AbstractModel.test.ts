/**
 * Tests fo AbstractModel
 * ToDo fix, one expected per case
 *
 * @author: Denis Makarov
 * @date: 2018-08-21
 */

import {SimpleModelStore} from './globalMocks';
import {SimpleModel} from './models/SimpleModel';
import {ArtifactModel} from './models/ArtifactModel';
import {ExtendedModel} from './models/ExtendedModel';
import {PeopleModel} from './models/PeopleModel';

let store: any;

let simpleModelMock1 = {
    id: 2,
    name: 'Donald',
    lastName: 'Grumph',
    age: 75,
    boss: {
        name: 'Melania',
        lastName: 'Grumph'
    },
    artifacts: [
        {
            id: 1,
            name: 'Bulava',
            cost: 150
        },
        {
            id: 2,
            name: 'Mouse',
            cost: 10
        }
    ]
};

let simpleModelMock2 = {
    id: 3,
    name:
        'Gim Chen',
    lastName:
        'Been',
    age:
        46,
    boss:
        {
            name: 'Gim Chen',
            lastName:
                'Seen'
        },
    artifacts: [
        {
            id: 3,
            name: 'Pen',
            cost: 55
        }
    ]

};

beforeEach(() => {
    store = new SimpleModelStore();
    store.add(new ExtendedModel(simpleModelMock1));
    store.add(new ExtendedModel(simpleModelMock2));
});

describe('AbstractModel', () => {
    it('correctly sets dirty', () => {
        let simpleModel = store.items[0];

        simpleModel.name.setValue('John');

        expect(simpleModel.getModelDirty()).toBe(true);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(true);
    });

    it('correctly sets Deep Dirty', () => {
        let simpleModel = store.items[0];

        simpleModel.artifacts.get(0).cost.setValue(100);

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(true);
        expect(simpleModel.getDirty()).toBe(true);
    });

    it('correctly updates dirty flags after commit', () => {
        let simpleModel = store.items[0];

        simpleModel.artifacts.get(0).cost.setValue(100);
        simpleModel.name.setValue('John');
        simpleModel.commit();

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);

    });

    it('correctly updates dirty flags after revert', () => {
        let simpleModel = store.items[0];

        simpleModel.artifacts.get(0).cost.setValue(100);
        simpleModel.name.setValue('John');
        simpleModel.revert();

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);
    });

    it('correctly updates dirty flags after field value back to original', () => {
        let simpleModel = store.items[0];

        simpleModel.name.setValue('John');
        simpleModel.name.setValue('John');

        expect(simpleModel.getModelDirty()).toBe(true);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(true);

        simpleModel.name.setValue('Donald');

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);
    });

    it('correctly updates dirty flags after hasOne value back to original', () => {
        let simpleModel = store.items[0];
        let tempBoss = simpleModel.boss;

        simpleModel.boss = new PeopleModel({name: 'Jared'});

        expect(simpleModel.getModelDirty()).toBe(true);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(true);

        simpleModel.boss = tempBoss;

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);
    });

    it('correctly updates dirty flag after value back to original', () => {
        let simpleModel = store.items[0];

        simpleModel.name.setValue('John');
        simpleModel.name.setValue('Donald');
        simpleModel.artifacts.get(0).cost.setValue(100);
        simpleModel.artifacts.get(0).cost.setValue(150);

        expect(simpleModel.getDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);
    });

    it('correctly updates dirty flag after hasOne changed', () => {
        let simpleModel = store.items[0];

        simpleModel.boss.name.setValue('Jessica');

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(true);
        expect(simpleModel.getDirty()).toBe(true);
    });

    it('correctly updates dirty flag after hasMany changed', () => {
        let simpleModel = store.items[0];
        let artifactModel = new ArtifactModel({name: 'axe', cost: 450});

        simpleModel.artifacts.add(artifactModel);

        expect(simpleModel.artifacts.size() === 3);
        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(true);
        expect(simpleModel.getDirty()).toBe(true);

        simpleModel.artifacts.removeAt(2);

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);
    });

    it('correctly updates dirty flag after hasOne removed', () => {
        let simpleModel = store.items[0];

        simpleModel.boss = null;

        expect(simpleModel.getDirty()).toBe(true);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(true);
    });

    it('correctly revert changes after hasOne removed', () => {
        let simpleModel = store.items[0];

        simpleModel.boss = null;
        simpleModel.revert();

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);

        expect(simpleModel.boss.name.getValue() === 'Melania');
        expect(simpleModel.boss.lastName.getValue() === 'Grumph');
    });

    it('correctly understands changes no more belongs to hasOne', () => {
        let simpleModel = store.items[0];
        let detachedBoss = simpleModel.boss;

        simpleModel.boss = null;
        simpleModel.commit();

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);

        detachedBoss.name.setValue('Geliermo');

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);

    });

    it('correctly understands changes no more belongs to hasMany', () => {
        let simpleModel = store.items[0];
        let detachedArtifact = simpleModel.artifacts.get(0);

        simpleModel.artifacts.removeAt(0);
        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(true);
        expect(simpleModel.getDirty()).toBe(true);

        simpleModel.commit();

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);

        detachedArtifact.name.setValue('Axe');

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);
    });

    it('correctly delete subscription after revert in hasMany', () => {
        let simpleModel = store.items[0];
        let artifactModel = new ArtifactModel({name: 'axe', cost: 450});

        simpleModel.artifacts.add(artifactModel);
        simpleModel.revert();

        artifactModel.name.setValue('hammer');

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
        expect(simpleModel.getDirty()).toBe(false);
    });

    it('correctly calculates dirty flags after remove hasMany', () => {
        let simpleModel = store.items[0];

        simpleModel.artifacts.removeAt(1);

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(true);
        expect(simpleModel.getDirty()).toBe(true);
    });

    it('correctly calculates dirty flags after add and instant remove hasMany', () => {
        let simpleModel = store.items[0];

        simpleModel.artifacts.add(new ArtifactModel({name: 'axe', cost: 450}));
        simpleModel.artifacts.removeAt(2);

        expect(simpleModel.getModelDirty()).toBe(false);
        expect(simpleModel.getDeepDirty()).toBe(false);
    });

    it('correctly initiate malformed data', () => {
        let malformedData = {
            name: 'Rick',
            home: 'Denver'
        };

        store.add(new SimpleModel(malformedData));
        let simpleModel = store.items[2];

        expect(simpleModel.name.getValue() === 'Rick');
        expect(simpleModel.artifacts.size() === 0);
        expect(simpleModel.boss === null);
        expect(simpleModel.age === null);
    });

    it('get callback on change field', () => {
        const someData = {
            name: 'Iosiv',
            lastName: 'Stalin'
        };

        const onChange = jest.fn();
        const simpleModel = new PeopleModel(someData, undefined, undefined, onChange);

        simpleModel.lastName.setValue('Kobzon');

        expect(onChange).toHaveBeenCalled();
    });

    it('get callback on change in deep field', () => {
        const mock = simpleModelMock2;
        const onChange = jest.fn();
        const simpleModel = new SimpleModel(mock, undefined, undefined, onChange);

        simpleModel.artifacts.items[0].cost.setValue(33);

        expect(onChange).toHaveBeenCalled();
    });
});
