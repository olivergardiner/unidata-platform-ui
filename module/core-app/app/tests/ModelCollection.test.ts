/**
 * Tests Model Collection
 * ToDo fix, one expected for one case
 *
 * @author: Denis Makarov
 * @date: 2018-08-21
 */

import {ArtifactModel} from './models/ArtifactModel';
import {ModelCollection} from '@unidata/core/dist/data/collection/ModelCollection';

let modelCollection: ModelCollection<ArtifactModel>;
let artifact1;
let artifact2;
let artifact3;

let modelCollectionMock = {

    artifacts: [
        {
            id: 1,
            name: 'rifle',
            cost: 150
        },
        {
            id: 2,
            name: 'hammer',
            cost: 10
        },
        {
            id: 3,
            name: 'pen',
            cost: 55
        }
    ]
};

beforeEach(() => {
    // здесь мы моделируем механизм создания ModelCollection внутри AbstractModel
    modelCollection = ModelCollection.create(ArtifactModel);
    artifact1 = new ArtifactModel(modelCollectionMock.artifacts[0]);
    artifact2 = new ArtifactModel(modelCollectionMock.artifacts[1]);
    artifact3 = new ArtifactModel(modelCollectionMock.artifacts[3]);
    modelCollection.addItems([artifact1, artifact2, artifact3]);
    modelCollection.commit();
});

describe('ModelCollection', () => {
    it('creates new collection', () => {
        expect(modelCollection.size()).toBe(3);
        expect(modelCollection.getDeepDirty()).toBe(false);
        expect(modelCollection.getCollectionDirty()).toBe(false);
        expect(modelCollection.getDirty()).toBe(false);
        expect(modelCollection.getRemoved().length).toBe(0);
        expect(modelCollection.getOriginal().length).toBe(3);
        expect(modelCollection.getAdded().length).toBe(0);

    });

    it('correctly calculates "added" array', () => {
        let artifactToAdd = new ArtifactModel({name: 'Machine Gun', cost: 1500});
        let phantomArtifact = new ArtifactModel({id: 999, name: 'Hammer', cost: 20});

        modelCollection.add(artifactToAdd);

        expect(modelCollection.getAdded().length).toBe(1);

        modelCollection.add(phantomArtifact);

        expect(modelCollection.getAdded().length).toBe(1);
    });

    it('correctly calculates "removed" array', () => {
        let artifactPhantom = new ArtifactModel({name: 'Machine Gun', cost: 1500});

        modelCollection.add(artifactPhantom);
        modelCollection.remove(artifactPhantom);

        expect(modelCollection.getRemoved().length).toBe(0);

        modelCollection.removeAt(0);

        expect(modelCollection.getRemoved().length).toBe(1);
    });

    it('correctly removes by object', () => {
        let modelToRemove = modelCollection.get(0);
        let modelToRemove2 = modelCollection.get(1);

        modelCollection.remove(modelToRemove);
        modelCollection.remove(modelToRemove2);

        expect(modelCollection.getRemoved().length).toBe(2);
        expect(modelCollection.size()).toBe(1);
    });

    it('correctly removes by index', () => {
        expect(modelCollection.size()).toBe(3);

        modelCollection.removeAt(2);
        modelCollection.removeAt(1);
        modelCollection.removeAt(0);

        expect(modelCollection.getRemoved().length).toBe(3);
        expect(modelCollection.size()).toBe(0);
    });

    it('correctly removes all elements', () => {
        expect(modelCollection.size()).toBe(3);

        modelCollection.removeAll();

        expect(modelCollection.getRemoved().length).toBe(3);
        expect(modelCollection.size()).toBe(0);
    });

    it('correctly sets collectionDirty after remove', () => {
        modelCollection.removeAt(0);

        expect(modelCollection.getCollectionDirty()).toBe(true);
    });

    it('correctly sets collectionDirty after remove phantom', () => {
        modelCollection.add(new ArtifactModel({name: 'Machine Gun', cost: 4500}));
        modelCollection.removeAt(3);

        expect(modelCollection.getCollectionDirty()).toBe(false);
    });

    it('correctly sets deepDirty', () => {
        let modelToEdit = modelCollection.get(0);

        modelToEdit.cost.setValue(100500);

        expect(modelCollection.getDeepDirty()).toBe(true);
    });

    it('callback onChange called as expected', () => {
        const onChange = jest.fn();

        modelCollection = ModelCollection.create(ArtifactModel, onChange);

        modelCollection.add(new ArtifactModel({name: 'axe', cost: 450}));
        modelCollection.add(new ArtifactModel({name: 'stone', cost: 10}));
        modelCollection.add(new ArtifactModel({name: 'sword', cost: 605}));
        modelCollection.add(new ArtifactModel({name: 'bow', cost: 1300}));
        modelCollection.add(new ArtifactModel({name: 'your mom', cost: 2000}));

        expect(onChange).toHaveBeenCalledTimes(10); // (По разу на элеменет коллекции и в модели)
    });

    it('correctly adds new elements and revert', () => {
        modelCollection.add(new ArtifactModel({name: 'axe', cost: 450}));
        modelCollection.add(new ArtifactModel({name: 'stone', cost: 10}));

        expect(modelCollection.getAdded().length).toBe(2);

        modelCollection.revert();

        expect(modelCollection.getAdded().length).toBe(0);
    });

    it('correctly add and instant remove', () => {
        modelCollection.add(new ArtifactModel({name: 'axe', cost: 450}));

        expect(modelCollection.getAdded().length).toBe(1);
        expect(modelCollection.getRemoved().length).toBe(0);

        modelCollection.removeAt(3);

        expect(modelCollection.getAdded().length).toBe(0);
        expect(modelCollection.getRemoved().length).toBe(0);

        expect(modelCollection.getDeepDirty()).toBe(false);
        expect(modelCollection.getCollectionDirty()).toBe(false);
        expect(modelCollection.getDirty()).toBe(false);
    });

    it('correctly subscribe new item', () => {
        let model = new ArtifactModel({name: 'axe', cost: 450});

        modelCollection.add(model);

        expect(modelCollection.getDeepDirty()).toBe(false);

        model.cost.setValue(100500);

        expect(modelCollection.getDeepDirty()).toBe(true);
        expect(modelCollection.getCollectionDirty()).toBe(true);
        expect(modelCollection.getDirty()).toBe(true);
    });

    it('correctly unsubscribe removed item', () => {
        let model = new ArtifactModel({name: 'axe', cost: 450});

        modelCollection.add(model);
        modelCollection.removeAt(3);

        model.cost.setValue(100500);

        expect(modelCollection.getDeepDirty()).toBe(false);
        expect(modelCollection.getCollectionDirty()).toBe(false);
        expect(modelCollection.getDirty()).toBe(false);
    });

    it('correctly commits changes', () => {
        let model = new ArtifactModel({name: 'axe', cost: 450});
        let model2 = new ArtifactModel({name: 'potato', cost: 3});

        modelCollection.removeAt(0);
        modelCollection.add(model);
        modelCollection.add(model2);

        modelCollection.commit();

        expect(modelCollection.size()).toBe(4);
        expect(modelCollection.getDeepDirty()).toBe(false);
        expect(modelCollection.getCollectionDirty()).toBe(false);
        expect(modelCollection.getDirty()).toBe(false);
    });

    it('correctly reverts changes', () => {
        let model = new ArtifactModel({name: 'axe', cost: 450});
        let model2 = new ArtifactModel({name: 'potato', cost: 3});

        modelCollection.removeAt(0);
        modelCollection.get(0).cost.setValue(100500);
        modelCollection.add(model);
        modelCollection.add(model2);

        modelCollection.revert();

        expect(modelCollection.get(0).cost.getValue()).toBe(150);
        expect(modelCollection.size()).toBe(3);
        expect(modelCollection.getDeepDirty()).toBe(false);
        expect(modelCollection.getCollectionDirty()).toBe(false);
        expect(modelCollection.getDirty()).toBe(false);
    });

    it('correctly unsubscribe events when remove collection item', function () {
        let removedModel: ArtifactModel;

        removedModel = modelCollection.get(0);
        modelCollection.removeAt(0);
        removedModel.name.setValue('some changed value');
    });
});
