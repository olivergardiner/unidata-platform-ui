export default jest.fn(function () {
    return Promise.resolve({
        status: 200,
        data: {
            success: true,
            errors: null,
            content: []
        }
    });
})