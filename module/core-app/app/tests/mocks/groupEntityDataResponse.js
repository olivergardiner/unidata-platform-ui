export default {
    status: 200,
    data: {
        'success': true,
        'content': {
            'groupNodes': [
                {
                    'title': 'Корневая группа',
                    'groupName': 'ROOT',
                    'lookupEntities': [
                        {
                            'name': 'NewHelp',
                            'displayName': 'NewHelp',
                            'dashboardVisible': false
                        },
                        {
                            'name': 'linkme',
                            'displayName': 'СправочникДляССылок',
                            'dashboardVisible': false
                        }
                    ],
                    'entities': [
                        {
                            'name': 'vreg3',
                            'displayName': ' реестр с форматом периода актуальности Дата',
                            'dashboardVisible': false
                        }, {
                            'name': 'emptyp',
                            'displayName': 'emptyp',
                            'dashboardVisible': false
                        }, {
                            'name': 'oldEntityToRemove',
                            'displayName': 'oldEntityToRemove',
                            'dashboardVisible': false
                        }, {
                            'name': 'newEntityToAdd',
                            'displayName': 'newEntityToAdd',
                            'dashboardVisible': false
                        }, {
                            'name': 'music',
                            'displayName': 'music',
                            'dashboardVisible': false
                        }, {
                            'name': 'regclas',
                            'displayName': 'REgWithClass',
                            'dashboardVisible': false
                        }, {
                            'name': 'grannyONe',
                            'displayName': 'ГранулярностьДата',
                            'dashboardVisible': false
                        }, {
                            'name': 'reg3',
                            'displayName': 'Регистр с датой временем',
                            'dashboardVisible': false
                        }, {
                            'name': 'RegistryWithTwoAttr',
                            'displayName': 'Регистр с двумя атрибутами',
                            'dashboardVisible': false
                        }, {
                            'name': 'vreg1',
                            'displayName': 'Регистр1',
                            'dashboardVisible': false
                        }, {
                            'name': 'vreg2',
                            'displayName': 'Регистр2',
                            'dashboardVisible': false
                        }, {
                            'name': 'regreg',
                            'displayName': 'Регистрация классификатора',
                            'dashboardVisible': false
                        }, {
                            'name': 'LinkLink',
                            'displayName': 'РеестрССылокойНаСправочник',
                            'dashboardVisible': false
                        }, {
                            'name': 'perAct',
                            'displayName': 'С периодом акутальности',
                            'dashboardVisible': false
                        }, {
                            'name': 'withlinks',
                            'displayName': 'Со связями',
                            'dashboardVisible': false
                        }, {
                            'name': 'RequiredAttr',
                            'displayName': 'Требуемый атрибут',
                            'dashboardVisible': false
                        }
                    ]
                }
            ]
        },
        'errors': null
    }
};
