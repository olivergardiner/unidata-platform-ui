export default {
    status: 200,
    data: {
        'success': true,
        'content': [{
            'id': 11,
            'entityName': 'oldEntityToRemove',
            'processType': 'RECORD_EDIT',
            'triggerType': 'ALL',
            'processDefinitionId': 'approvalProcess',
            'createDate': '2019-12-13T08:50:28.570+0000',
            'updateDate': null,
            'createdBy': 'admin',
            'updatedBy': null
        }, {
            'id': 7,
            'entityName': 'emptyp',
            'processType': 'RECORD_EDIT',
            'triggerType': 'ALL',
            'processDefinitionId': 'approvalProcess',
            'createDate': '2019-12-11T13:59:47.638+0000',
            'updateDate': null,
            'createdBy': 'admin',
            'updatedBy': null
        }],
        'errors': null
    }
};
