import {ArtifactModel} from '../models/ArtifactModel';
import {IModelOpConfig, ModelHttpOp} from '@unidata/core';

export class TestOp extends ModelHttpOp {
    public config: IModelOpConfig = {
        url: '/test/api',
        method: 'get',
        model: ArtifactModel,
        rootProperty: ''
    };
}
