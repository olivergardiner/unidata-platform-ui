/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-23
 */

import {ClassPropertyMeta} from '@unidata/core/dist/data/ClassPropertyMeta';
import {StringField} from '@unidata/core/dist/data/field/StringField';
import {IntegerField} from '@unidata/core/dist/data/field/IntegerField';
import {ClassPropertyDescriptor} from '@unidata/core/dist/data/ClassPropertyDescriptor';
import {ModelFieldDataType} from '@unidata/core/dist/data/model/ModelFieldDataType';

class MockConstructor {

}

let propertyDescriptorMock =  new ClassPropertyDescriptor(StringField, 'name', {configKey: 'configValue'}, ModelFieldDataType.field);
let propertyDescriptorMock2 =  new ClassPropertyDescriptor(IntegerField, 'age', {configKey: 'configValue'}, ModelFieldDataType.field);

let classPropertyMetaMock = {
    targetCtor: MockConstructor,
    annotationKey: 'fieldMainDescriptor',
    propertyDescriptor: propertyDescriptorMock
};

let classPropertyMetaMock2 = {
    targetCtor: MockConstructor,
    annotationKey: 'fieldMainDescriptor',
    propertyDescriptor: propertyDescriptorMock2
};

describe('ClassPropertyMeta', () => {
    it('adds new descriptors', () => {
        ClassPropertyMeta.addDescriptor(
            classPropertyMetaMock.targetCtor,
            classPropertyMetaMock.annotationKey,
            classPropertyMetaMock.propertyDescriptor
        );
        ClassPropertyMeta.addDescriptor(
            classPropertyMetaMock2.targetCtor,
            classPropertyMetaMock2.annotationKey,
            classPropertyMetaMock2.propertyDescriptor
        );

        expect(ClassPropertyMeta.getDescriptors(classPropertyMetaMock.targetCtor, classPropertyMetaMock.annotationKey).length).toBe(2);
    });

});
