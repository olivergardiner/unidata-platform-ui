/**
 * empty comment line Sergey Shishigin
 *
 * @author: Sergey Shishigin
 * @date: 2018-09-05
 */

import {EventChannel} from '@unidata/core/dist/event/EventChannel';

let testEvent: EventChannel<Window, null>;

beforeEach(() => {
    testEvent = new EventChannel();
});

describe('EventChannel', () => {
    it('create and dispatch only one event', () => {
        const onTestEvent = jest.fn();

        testEvent.one(onTestEvent, this);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        testEvent.getDispatcher().dispatch(this, null);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        testEvent.getDispatcher().dispatch(this, null);

        expect(onTestEvent).toHaveBeenCalledTimes(1);
    });

    it('test method has', () => {
        const onTestEvent = jest.fn();

        let resultPositive: boolean;
        let resultNegative: boolean;

        testEvent.subscribe(onTestEvent, this);
        resultPositive = testEvent.has(onTestEvent, this);
        resultNegative = testEvent.has(onTestEvent);

        expect(resultPositive).toBe(true);
        expect(resultNegative).toBe(false);
    });

    it('test method clear', () => {
        const onTestEvent1 = jest.fn();
        const onTestEvent2 = jest.fn();

        testEvent.subscribe(onTestEvent1, this);
        testEvent.subscribe(onTestEvent2, this);

        testEvent.clear();
        expect(testEvent.has(onTestEvent1, this)).toBe(false);
        expect(testEvent.has(onTestEvent2, this)).toBe(false);
    });
});
