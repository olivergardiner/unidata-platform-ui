/**
 * Tests for AbstractField
 * ToDo fix, one expected for one case
 *
 * @author: Denis Makarov
 * @date: 2018-08-21
 */

import {StringField} from '@unidata/core/dist/data/field/StringField';
import {IntegerField} from '@unidata/core/dist/data/field/IntegerField';
import {ArrayField} from '@unidata/core/dist/data/field/ArrayField';
import {NumberField} from '@unidata/core/dist/data/field/NumberField';
import {DateField} from '@unidata/core/dist/data/field/DateField';
import {BooleanField} from '@unidata/core/dist/data/field/BooleanField';

describe('AbstractField', () => {
    it('correctly sets dirty', () => {
        let stringField = new StringField('value1');
        let integerField = new IntegerField(42);
        let numberField = new NumberField(12.11111);
        let arrayField = new ArrayField([1, 2, 3]);
        let dateField = new DateField(new Date('2006-12-31'));
        let booleanField = new BooleanField(true);

        stringField.setValue('value2');
        integerField.setValue(43);
        numberField.setValue(43.212213213);
        arrayField.setValue([2, 3, 4]);
        dateField.setValue(new Date('2007-01-01'));
        booleanField.setValue(false);

        expect(stringField.getDirty()).toBe(true);
        expect(integerField.getDirty()).toBe(true);
        expect(numberField.getDirty()).toBe(true);
        expect(arrayField.getDirty()).toBe(true);
        expect(dateField.getDirty()).toBe(true);
        expect(booleanField.getDirty()).toBe(true);
    });

    it('correctly understands value return to original', () => {
        let stringField = new StringField('value1');
        let integerField = new IntegerField(42);
        let numberField = new NumberField(12.11111);
        let arrayField = new ArrayField([1, 2, 3]);
        let dateField = new DateField(new Date('2006-12-31'));
        let booleanField = new BooleanField(true);

        stringField.setValue('value2');
        integerField.setValue(33);
        numberField.setValue(43.212213213);
        arrayField.setValue([2, 3, 4]);
        dateField.setValue(new Date('2007-01-01'));
        booleanField.setValue(false);

        stringField.setValue('value1');
        integerField.setValue(42);
        numberField.setValue(12.11111);
        arrayField.setValue([1, 2, 3]);
        dateField.setValue(new Date('2006-12-31'));
        booleanField.setValue(true);

        expect(stringField.getDirty()).toBe(false);
        expect(integerField.getDirty()).toBe(false);
        expect(numberField.getDirty()).toBe(false);
        expect(arrayField.getDirty()).toBe(false);
        expect(dateField.getDirty()).toBe(false);
        expect(booleanField.getDirty()).toBe(false);
    });

    it('correctly sets value silently ', () => {
        let stringField = new StringField('value1');

        stringField.updateDirty = jest.fn();

        stringField.setValue('value33333');
        stringField.setValue('value22222', true);
        stringField.setValue('value44444');

        expect(stringField.updateDirty).toHaveBeenCalledTimes(2);
    });

    it('correctly reverts value', () => {
        let stringField = new StringField('value1');
        let integerField = new IntegerField(42);
        let numberField = new NumberField(12.11111);
        let arrayField = new ArrayField([1, 2, 3]);
        let dateField = new DateField(new Date('2006-12-31'));
        let booleanField = new BooleanField(true);

        stringField.setValue('value2');
        integerField.setValue(33);
        numberField.setValue(43.212213213);
        arrayField.setValue([2, 3, 4]);
        dateField.setValue(new Date('2007-01-01'));
        booleanField.setValue(false);

        stringField.revert();
        integerField.revert();
        numberField.revert();
        arrayField.revert();
        dateField.revert();
        booleanField.revert();

        expect(stringField.getDirty()).toBe(false);
        expect(integerField.getDirty()).toBe(false);
        expect(numberField.getDirty()).toBe(false);
        expect(arrayField.getDirty()).toBe(false);
        expect(dateField.getDirty()).toBe(false);
        expect(booleanField.getDirty()).toBe(false);

        expect(stringField.getValue()).toBe('value1');
        expect(integerField.getValue()).toBe(42);
        expect(numberField.getValue()).toBe(12.11111);
        expect(arrayField.getValue()).toEqual([1, 2, 3]);
        expect(dateField.getValue()).toEqual(new Date('2006-12-31'));
        expect(booleanField.getValue()).toBe(true);

    });

    it('correctly reverts value', () => {
        let stringField = new StringField('value1');
        let integerField = new IntegerField(42);
        let numberField = new NumberField(12.11111);
        let arrayField = new ArrayField([1, 2, 3]);
        let dateField = new DateField(new Date('2006-12-31'));
        let booleanField = new BooleanField(true);

        stringField.setValue('value2');
        integerField.setValue(33);
        numberField.setValue(43.212213213);
        arrayField.setValue([2, 3, 4]);
        dateField.setValue(new Date('2007-01-01'));
        booleanField.setValue(false);

        stringField.revert();
        integerField.revert();
        numberField.revert();
        arrayField.revert();
        dateField.revert();
        booleanField.revert();

        expect(stringField.getDirty()).toBe(false);
        expect(integerField.getDirty()).toBe(false);
        expect(numberField.getDirty()).toBe(false);

        expect(arrayField.getDirty()).toBe(false);
        expect(dateField.getDirty()).toBe(false);
        expect(booleanField.getDirty()).toBe(false);

        expect(stringField.getValue()).toBe('value1');
        expect(integerField.getValue()).toBe(42);
        expect(numberField.getValue()).toBe(12.11111);
        expect(arrayField.getValue()).toEqual([1, 2, 3]);
        expect(dateField.getValue()).toEqual(new Date('2006-12-31'));
        expect(booleanField.getValue()).toBe(true);

    });

    it('correctly compare null value', () => {
        let stringField = new StringField('');

        stringField.setValue(null);
        expect(stringField.getDirty()).toBe(true);
    });

});
