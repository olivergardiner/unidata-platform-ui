/**
 * Store for the modal window " About the System"
 *
 * @author: Denis Makarov
 * @date: 2020-04-05
 */

import {action, observable} from 'mobx';

export enum AboutSystemTab {
    System = 'System',
    Modules = 'Modules',
    License = 'License'
}

export class AboutSystemStore {
    @observable
    public currentTab: AboutSystemTab = AboutSystemTab.System;

    @action
    public setCurrentTab (tab: AboutSystemTab) {
        this.currentTab = tab;
    }
}
