/**
 * Key-value field for the "About System" modal window"
 *
 * @author: Denis Makarov
 * @date: 2020-04-05
 */

import * as React from 'react';
import * as styles from './AboutSystemField.m.scss';

interface IProps {
    fieldKey: string;
    fieldValue: string;
}

export const AboutSystemField = (props: IProps) => {
        return (
            <div className={styles.field}>
                <div className={styles.fieldKey}>{props.fieldKey}:</div>
                <div className={styles.fieldValue}>{props.fieldValue}</div>
            </div>
        );
};
