/**
 * Tab with information about modules in the "About System" modal window"
 *
 * @author: Denis Makarov
 * @date: 2020-04-05
 */

import * as React from 'react';
import {useEffect, useState} from 'react';
import {ModuleService} from '@unidata/core-app';
import * as styles from '../AboutSystemModal.m.scss';
import {Skeleton} from 'antd';
import {AboutSystemField} from '../field/AboutSystemField';
import {Scrollable} from '@unidata/uikit';
import {IBackendModule} from '@unidata/types';

export const ModulesTab = () => {
    const [modules, setModules] = useState<IBackendModule []>([]);
    const [modulesLoaded, setModulesLoaded] = useState(false);

    useEffect(() => {
            if (modulesLoaded) {
                return;
            }

            ModuleService.getModuleList()
                .then((modules: IBackendModule []) => {
                    setModules(modules);
                    setModulesLoaded(true);
                });
        }
    );

    return (
        <div className={styles.fieldsContainer}>
            <Scrollable>
                <Skeleton paragraph={{rows: 8, width: '80%'}} active={true} loading={!modulesLoaded}/>
                {modulesLoaded && modules.map((module) => {
                    return (<AboutSystemField key={module.id} fieldKey={module.description} fieldValue={module.version}/>);
                })}
            </Scrollable>
        </div>
    );
};
