/**
 * Tab with basic information about the system in the "About System" modal window"
 *
 * @author: Denis Makarov
 * @date: 2020-04-05
 */

import * as React from 'react';
import {i18n, VersionManager} from '@unidata/core-app';
import moment from 'moment';
import {AboutSystemField} from '../field/AboutSystemField';

import * as styles from '../AboutSystemModal.m.scss';

export const SystemTab = () => {
        const unidataVersion: string = (window as any).unidataVersion;
        const backendVersion: string = VersionManager.getBuildVersion();
        const source: string = (window as any).location.origin;
        const unidataBuildDate: string = moment().format((window as any).unidataBuildDate);

        return (
            <div className={styles.fieldsContainer}>
               <AboutSystemField fieldKey={i18n.t('app.menu>about>clientVersion')} fieldValue={unidataVersion}/>
               <AboutSystemField fieldKey={i18n.t('app.menu>about>backendVersion')} fieldValue={backendVersion}/>
               <AboutSystemField fieldKey={i18n.t('app.menu>about>source')} fieldValue={source}/>
               <AboutSystemField fieldKey={i18n.t('app.menu>about>buildDate')} fieldValue={unidataBuildDate}/>
            </div>
        );
};
