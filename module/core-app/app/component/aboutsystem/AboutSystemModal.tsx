/**
 * Modal window "About the System"
 *
 * @author: Denis Makarov
 * @date: 2020-04-05
 */

import * as React from 'react';
import * as styles from './AboutSystemModal.m.scss';
import {Modal, TabBar} from '@unidata/uikit';
import {i18n, LicenseManager, LicenseMode, UnidataConfig, UserManager} from '@unidata/core-app';
import {Menu} from 'antd';
import {AboutSystemStore, AboutSystemTab} from './store/AboutSystemStore';
import {observer} from 'mobx-react';
import {SystemTab} from './tabs/SystemTab';
import {ModulesTab} from './tabs/ModulesTab';
import {LicenseTab} from './tabs/LicenseTab';

interface IProps {
    isOpen: boolean;
    onClose: () => void;
    store: AboutSystemStore;

}

@observer
export class AboutSystemModal extends React.Component<IProps> {
    constructor (props: IProps) {
        super(props);

        this.changeCurrentTab = this.changeCurrentTab.bind(this);
    }

    get store () {
        return this.props.store;
    }

    changeCurrentTab ({key}: {key: AboutSystemTab}) {
        this.props.store.setCurrentTab(key);
    }

    private renderHeader () {
        const locale = UserManager.getUser()?.locale || UnidataConfig.getDefaultLocale();
        const urlSvg = `react-resources/unidatabootstrap/images/logoplatform-${locale}.svg`;

        return (<div className={styles.logoContainer}>
            <object className={styles.logo} data={`${urlSvg}`} type='image/svg+xml'>
            </object>
        </div>);
    }

    private renderCurrentTab () {
        if (this.store.currentTab === AboutSystemTab.System) {
            return (<SystemTab/>);
        } else if (this.store.currentTab === AboutSystemTab.Modules) {
            return (<ModulesTab/>);
        } else if (this.store.currentTab === AboutSystemTab.License) {
            return (<LicenseTab/>);
        } else {
            return null;
        }
    }

    private showLicenseTab () {
        const license = LicenseManager.getLicense();

        if (license && license.licenseMode === LicenseMode.Production) {
            return true;
        }

        return false;
    }


    render () {
        const header = this.renderHeader();
        const currentTab = this.renderCurrentTab();
        const showLicenseTab: boolean = this.showLicenseTab();

        return (
            <Modal isOpen={this.props.isOpen}
                   header={header}
                   noBodyPadding={true}
                   onClose={this.props.onClose}
            >
                <div className={styles.modalBody}>
                    <div className={styles.tabsContainer}>
                        <TabBar
                            onClick={this.changeCurrentTab}
                            defaultSelectedKeys={[this.props.store.currentTab.toString()]}
                        >
                            <Menu.Item key={AboutSystemTab.System}>
                                {i18n.t('app.menu>version')}
                            </Menu.Item>
                            <Menu.Item key={AboutSystemTab.Modules}>
                                {i18n.t('app.menu>modules')}
                            </Menu.Item>
                            {showLicenseTab && <Menu.Item key={AboutSystemTab.License}>
                                {i18n.t('app.menu>license')}
                                </Menu.Item>
                            }
                        </TabBar>
                    </div>
                    <div style={{height: '100%'}}>
                        {currentTab}
                    </div>
                </div>
            </Modal>
        );
    }

}
