/**
 * Password change/setting window
 *
 * @author Vladimir Stebunov
 * @date 2020-03-03
 */

import * as React from 'react';
import * as styles from './ChangePasswordWindow.m.scss';
import {Button, Icon, Input, INTENT, Modal, SIZE, InputAntd} from '@unidata/uikit';
import {App, AuthorizationService, Dialog, i18n, UdLogger, UserManager} from '@unidata/core-app';
import {Tooltip} from 'antd';
import {UeModuleCallback, ueModuleManager, UeModuleTypeCallBack} from '@unidata/core';

interface IProps {
    isOpen: boolean;
    hasClose?: boolean;
    onClose?: () => void;
    withCurrentPassword?: boolean;
}

interface IState {
    currentPassword: string;
    newPassword: string;
    isOpenWarning: boolean;
    warningText: string;
    currentPasswordIsReadOnly: boolean;
    newPasswordIsReadOnly: boolean;
}

interface IResponse {
    success: boolean;
}

type fieldState = Pick<IState, 'currentPassword'|'newPassword'>;

const CHANGE_PASSWORD_REDIRECT_TIMEOUT = 2000;

export class ChangePasswordWindow extends React.Component<IProps, IState> {

    static defaultProps = {
        hasClose: true,
        withCurrentPassword: true
    };

    firstInputRef: React.RefObject<InputAntd>;

    state: IState = {
        currentPassword: '',
        newPassword: '',
        isOpenWarning: false,
        warningText: '',
        currentPasswordIsReadOnly: false,
        newPasswordIsReadOnly: false
    };

    constructor (props: IProps) {
        super(props);

        this.firstInputRef = React.createRef();
    }

    /**
     * Close the error message
     */
    hideWarning () {
        this.setState({isOpenWarning: false});
    }

    /**
     * To change the state of the field
     * @param e the change event of the field
     */
    onFieldChange (e: React.SyntheticEvent<HTMLInputElement>) {
        this.setState({[e.currentTarget.name]: e.currentTarget.value} as fieldState);
    }

    /**
     * Draw an error window
     * @param warningText error message
     */
    showWarning (warningText: string) {
        this.setState({
            isOpenWarning: true,
            warningText
        });
    }

    /**
     * Checking the password for the userexit rules
     *
     * @param newPassword new password
     */
    calcPolicyWarnings (newPassword: string): string[] {
        const passwordPolicy = ueModuleManager.getModulesByType(UeModuleTypeCallBack.PASSWORD_POLICY),
            result: string[] = [];

        if (passwordPolicy) {
            passwordPolicy.forEach((policy: UeModuleCallback<UeModuleTypeCallBack.PASSWORD_POLICY>) => {
                const warning = policy.default.fn(newPassword);

                if (warning) {
                    result.push(warning);
                }
            });
        }

        return result;
    }

    /**
     * Processing and submitting the form
     */
    onFormChange () {
        const {newPassword, currentPassword} = this.state;

        if (newPassword !== '') {
            const warning = this.calcPolicyWarnings(newPassword);

            if (warning.length > 0) {
                this.showWarning(warning[0]);

                return;
            }

            if (newPassword === currentPassword) {
                this.showWarning(i18n.t('login>password.equalPasswords'));

                return;
            }
        } else {
            this.showWarning(i18n.t('login>password.passwordRequired'));

            return;
        }

        let currentUsername = String(UserManager.getUserLogin());

        const authorizationServicePromise = this.props.withCurrentPassword ?
            AuthorizationService.changePassword(currentUsername, this.state.currentPassword, this.state.newPassword) :
            AuthorizationService.setPassword(currentUsername, this.state.newPassword);

        authorizationServicePromise.then((response: IResponse) => {
            if (response.success) {

                Dialog.showSimpleMessage(i18n.t('login>password.successfullyChanged'));

                this.setState({
                    currentPasswordIsReadOnly: true,
                    newPasswordIsReadOnly: true
                });

                // a small delay in order for the user to see the pop up
                setTimeout(function () {
                    App.deauthenticate();
                }, CHANGE_PASSWORD_REDIRECT_TIMEOUT);
            } else {
                UdLogger.error('Response from server not success:' + JSON.stringify(response));
            }
        });

        this.setState({
            currentPassword: '',
            newPassword: ''
        });
    }

    /**
     * Checking that the form is valid affects the state of the submit button
     */
    isFormValid (): boolean {
        let formValid = this.state.newPassword !== '';

        if (this.props.withCurrentPassword) {
            formValid = formValid && this.state.currentPassword !== '';
        }

        return formValid;
    }

    /**
     * Actions when closing the window
     */
    onClose (): void {
        this.setState({
            currentPassword: '',
            newPassword: ''
        });

        if (this.props.onClose) {
            this.props.onClose();
        }
    }

    /**
     * Drawing the buttons of the main window
     */
    renderFooter () {
        const isButtonDisabled = !this.isFormValid();

        return (
            <div className={styles.buttonFooter}>
                <Button
                    isGhost={true}
                    isRound={true}
                    isDisabled={!this.props.hasClose}
                    onClick={this.onClose.bind(this)}>{i18n.t('cancel_noun')}</Button>
                <Button
                    intent={INTENT.PRIMARY}
                    isRound={true}
                    isDisabled={isButtonDisabled}
                    onClick={this.onFormChange.bind(this)}>{i18n.t('login>password.changeButton')}</Button>
            </div>
        );
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {

        if (this.firstInputRef.current && !prevProps.isOpen && this.props.isOpen) {
            this.firstInputRef.current.focus();
        }

        if (this.state.isOpenWarning &&
            (prevState.newPassword !== this.state.newPassword ||
                prevState.currentPassword !== this.state.currentPassword)) {
            this.hideWarning();
        }
    }

    render () {
        return (
            <Modal
                isOpen={this.props.isOpen}
                header={i18n.t('app.menu>user>changePassword')}
                shouldCloseOnClickOutside={false}
                footer={this.renderFooter()}
                size={SIZE.EXTRA_SMALL}
                onClose={this.onClose.bind(this)}
                hasCloseIcon={this.props.hasClose}
            >
                {this.props.withCurrentPassword ?
                    <React.Fragment>
                        <div className={styles.label}>
                            {i18n.t('login>password.currentPassword')}
                        </div>
                        <Input
                            autoFocus={true}
                            type={'password'}
                            onChange={this.onFieldChange.bind(this)}
                            value={this.state.currentPassword}
                            prefix={<Icon name={'key'}/>}
                            autoComplete={'off'}
                            className={styles.input}
                            placeholder={i18n.t('login>password.currentPassword')}
                            readOnly={this.state.currentPasswordIsReadOnly}
                            forwardRef={this.firstInputRef}
                            name={'currentPassword'}
                        />
                    </React.Fragment> :
                    null
                }
                <div className={styles.label}>
                    {i18n.t('login>password.newPassword')}
                </div>
                <Tooltip
                    placement='bottomLeft'
                    visible={this.state.isOpenWarning}
                    overlayClassName={styles.tooltip}
                    align={{offset: [0, -7]}}
                    title={this.state.warningText} >
                    <Input.Password
                        type={'password'}
                        onChange={this.onFieldChange.bind(this)}
                        value={this.state.newPassword}
                        prefix={<Icon name={'key'}/>}
                        autoComplete={'off'}
                        className={styles.input}
                        placeholder={i18n.t('login>password.newPassword')}
                        readOnly={this.state.newPasswordIsReadOnly}
                        name={'newPassword'}
                        visibilityToggle={true}
                    />
                </Tooltip>
            </Modal>
        );
    }
}
