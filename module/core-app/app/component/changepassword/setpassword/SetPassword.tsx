/**
 * Screen for setting a password on a command from the server
 *
 * @author Vladimir Stebunov
 * @date 2020-03-06
 */

import * as React from 'react';
import * as styles from './SetPassword.m.scss';
import {ChangePasswordWindow} from '../changepasswordform/ChangePasswordWindow';
import {App, AppTypeManager} from '@unidata/core-app';
import {joinClassNames} from '@unidata/uikit';
import {
    UeModuleCallback,
    ueModuleManager,
    UeModuleTypeCallBack
} from '@unidata/core';
import {CSSProperties} from 'react';

export class SetPassword extends React.Component {

    /**
     * Output the background depending on the application mode
     *
     * @returns styles
     */
    getPlatformBackgroundCls () {
        let cls = styles.applicationUsermode;

        switch (true) {
            case AppTypeManager.isDataSteward():
                cls = styles.applicationUsermode;
                break;
            case AppTypeManager.isDataAdmin():
            case AppTypeManager.isSystemAdmin():
                cls = styles.applicationAdminmode;
                break;
        }

        return styles.applicationUsermode;
    }

    /**
     * Customization of the background via UE
     */
    getUEBackgroundStyle (): CSSProperties | undefined {

        const customizeModule = ueModuleManager.getModulesByType(UeModuleTypeCallBack.SET_PASSWORD_BACKGROUND);

        let styles: CSSProperties = {};

        customizeModule.forEach((customize: UeModuleCallback<UeModuleTypeCallBack.SET_PASSWORD_BACKGROUND>) => {
            styles = Object.assign(styles, customize.default.fn());
        });

        return styles;
    }

    onCancel () {
        App.deauthenticate();
    }

    render (): any {
        const cls = joinClassNames(
            styles.logincont,
            this.getPlatformBackgroundCls()
        );

        return (
            <div className={cls} style={this.getUEBackgroundStyle()}>
                <ChangePasswordWindow
                    isOpen={true}
                    withCurrentPassword={false}
                    hasClose={true}
                    onClose={this.onCancel}
                />
            </div>
        );
    }
}
