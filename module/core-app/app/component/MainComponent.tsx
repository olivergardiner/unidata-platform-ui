﻿/**
 * Main component: displays the page and switches between them
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {Component} from 'react';
import {Layout} from 'antd';
import {Switch} from 'react-router-dom';
import {MenuHeader} from './header/MenuHeader';
import * as styles from './MainComponent.m.scss';
import {AppRoute} from '../AppRoute';
import {WikiButton} from '@unidata/core-app';
import {BackendNotificationsArea} from './benotifications/BackendNotificationsArea';

const {Content} = Layout;

export class MainComponent extends Component {

    /**
     * Returns true if the wiki functionality is enabled
     *
     * @returns {boolean}
     */
    getWikiEnabled (): boolean {
        const customerCfg = window.customerConfig || {};

        return Boolean(customerCfg.WIKI_ENABLED);
    }

    render () {
        return (
            <Layout>
                <MenuHeader />
                <Layout>
                    <Content className={styles.layout}>
                        <BackendNotificationsArea />
                        <Switch>
                            {AppRoute.getPageRoutes()}
                        </Switch>
                    </Content>
                </Layout>
                {this.getWikiEnabled() ? <WikiButton/> : null}
            </Layout>
        );
    }
}
