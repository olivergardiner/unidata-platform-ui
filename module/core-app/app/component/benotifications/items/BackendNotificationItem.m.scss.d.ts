export const notification: string;
export const notificationCreateDate: string;
export const notificationContent: string;
export const notificationRemove: string;
export const notificationIsRemoved: string;
export const notificationInnerWrap: string;
export const notificationDetails: string;
