/**
 * Notification
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-02
 */

import * as React from 'react';
import * as styles from './BackendNotificationItem.m.scss';
import {BackendNotificationModel} from '../../../model/notification/BackendNotificationModel';
import * as moment from 'moment';
import {AuthTokenManager, i18n, ServerUrlManager, UserLocale} from '@unidata/core-app';
import {Icon, joinClassNames, Modal} from '@unidata/uikit';
import {BackendNotificationsStore} from '../store/BackendNotificationsStore';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';

interface IProps {
    notification: BackendNotificationModel;
}

@observer
export class BackendNotificationItem extends React.Component<IProps> {

    @observable private isRemoved: boolean = false;
    @observable private detailsVisible: boolean = false;
    private static reloadTimer: number;

    constructor (props: IProps) {
        super(props);

        this.toggleDetails = this.toggleDetails.bind(this);
        this.onDetailsClick = this.onDetailsClick.bind(this);
        this.onRemoveClick = this.onRemoveClick.bind(this);
    }

    getDownloadUrl () {
        let notification = this.props.notification,
            binaryDataId = notification.binaryDataId.getValue(),
            characterDataId = notification.characterDataId.getValue(),
            type,
            id,
            url;

        if (binaryDataId) {
            type = 'blob';
            id = binaryDataId;
        } else if (characterDataId) {
            type = 'clob';
            id = characterDataId;
        } else {
            return null;
        }

        url = `${ServerUrlManager.getBaseUrl()}data/entities/${type}/${id}?token=${AuthTokenManager.getToken()}`;

        return url;
    }

    parseContent (content: string): string {
        // line transfers to <br />
        content = content.replace(/(\r\n|\n|\r)/g, '<br />');

        // double transfers <p>
        content = content.split('<br /><br />').join('</p><p>');
        content = `<p>${content}</p>`;

        return content;
    }

    @action
    onRemoveClick () {
        this.isRemoved = true;

        BackendNotificationsStore.remove(this.props.notification);

        // start the download after animation and to the exclusion of excess load when quick delete items
        clearTimeout(BackendNotificationItem.reloadTimer);
        BackendNotificationItem.reloadTimer =  window.setTimeout(BackendNotificationsStore.load.bind(BackendNotificationsStore), 1000);
    }

    get className () {
        return joinClassNames(
            styles.notification,
            [styles.notificationIsRemoved, Boolean(this.isRemoved)],
        );
    }

    onDetailsClick (event: React.MouseEvent) {
        event.preventDefault();
        this.toggleDetails();
    }

    @action
    toggleDetails () {
        this.detailsVisible = !this.detailsVisible;

        setTimeout(BackendNotificationsStore.setNotificationAreaLocked.bind(BackendNotificationsStore, this.detailsVisible), 0);
    }

    renderDetails () {
        let notification = this.props.notification,
            details = notification.details.getValue();

        if (!details || details === '') {
            return null;
        }

        details = this.parseContent(details);

        return (
            <React.Fragment>
                <a
                    onClick={this.onDetailsClick}
                    href="#"
                    data-qaid='details'
                >
                    {i18n.t('notifications>details')}
                </a>
                <Modal
                    header={i18n.t('notifications>details')}
                    isOpen={this.detailsVisible}
                    onClose={this.toggleDetails}
                >
                    <div className={styles.notificationDetails} dangerouslySetInnerHTML={{__html: details}} />
                </Modal>
            </React.Fragment>
        );
    }

    render () {
        let notification = this.props.notification,
            createDate: Date | string = notification.createDate.getValue(),
            content = notification.content.getValue(),
            downloadUrl = this.getDownloadUrl(),
            downloadElement,
            detailsElement;

        content = this.parseContent(content);
        createDate = moment(createDate).format(UserLocale.getDateTimeFormat());
        createDate = createDate.split(' ').join(' | ');

        detailsElement = this.renderDetails();

        if (downloadUrl) {
            downloadElement = (
                <a
                    target='_blank'
                    rel='noopener noreferrer'
                    href={downloadUrl}
                    data-qaid='download'
                >
                    {i18n.t('notifications>download')}
                </a>
            );
        }

        return (
            <div className={this.className}>
                <div className={styles.notificationInnerWrap}>
                    <div className={styles.notificationCreateDate}>{createDate}</div>
                    <div
                        className={styles.notificationContent}
                        data-qaid='content'
                        dangerouslySetInnerHTML={{__html: content}}
                    />
                    <div className={styles.notificationRemove} onClick={this.onRemoveClick}>
                        <Icon name={'close'} />
                    </div>
                    {detailsElement}
                    {downloadElement}
                </div>
            </div>
        );
    }

}
