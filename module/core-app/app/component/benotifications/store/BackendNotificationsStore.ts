/**
 * Store with notifications
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-02
 */
import {BackendNotificationService} from '../../../service/BackendNotificationService';
import {action, observable} from 'mobx';
import {BackendNotificationModel} from '../../../model/notification/BackendNotificationModel';
import {BackendNotificationsCountStorePoller} from './BackendNotificationsCountStorePoller';

export class BackendNotificationsStore {

    public static init () {
        this.notifications = [];
        this.loading = false;
        this.notificationAreaVisible = false;
        this.notificationAreaLocked = false;
    }

    @observable
    public static notifications: BackendNotificationModel[];

    @observable
    public static loading: boolean;

    @observable
    private static notificationAreaVisible: boolean;

    @observable
    private static notificationAreaLocked: boolean;

    @action
    public static setNotificationAreaLocked (locked: boolean) {
        this.notificationAreaLocked = locked;
    }

    @action
    public static setNotificationAreaVisible (visible: boolean) {
        if (!this.notificationAreaLocked) {
            this.notificationAreaVisible = visible;
        }
    }

    public static toggleNotificationAreaVisible () {
        this.setNotificationAreaVisible(!this.notificationAreaVisible);
    }

    public static isNotificationAreaVisible () {
        return this.notificationAreaVisible;
    }

    public static isLoading () {
        return this.loading;
    }

    @action
    public static load () {
        let promise = BackendNotificationService.readNotifications();

        this.loading = true;
        promise.then(this.onNotificationsRead.bind(this));

        return promise;
    }

    @action
    public static clear () {
        this.notifications = [];
    }

    public static remove (notification: BackendNotificationModel) {
        BackendNotificationsCountStorePoller.setCount(BackendNotificationsCountStorePoller.count - 1);

        return BackendNotificationService.removeNotification(notification.id.getValue());
    }

    public static removeAll () {
        BackendNotificationsCountStorePoller.setCount(0);

        return BackendNotificationService.removeNotifications();
    }

    @action
    private static onNotificationsRead (notifications: BackendNotificationModel[]) {
        BackendNotificationsCountStorePoller.setCount(notifications.length);

        this.notifications = notifications;
        this.loading = false;
    }

}

BackendNotificationsStore.init();
