/**
 * Poll the number of notifications
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-02
 */
import {BackendNotificationService} from '../../../service/BackendNotificationService';
import {action, observable} from 'mobx';
import {Poller} from '@unidata/core-app';

export class BackendNotificationsCountStorePoller extends Poller {

    @observable
    public static count: number;

    public readonly interval: number = 1000 * 60;

    public poll (): void {
        BackendNotificationService.readNotificationsCount().then(BackendNotificationsCountStorePoller.setCount.bind(this));
    }

    @action
    public static setCount (count: number) {
        BackendNotificationsCountStorePoller.count = count;
    }

}

BackendNotificationsCountStorePoller.count = 0;
