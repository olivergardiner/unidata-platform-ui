/**
 * Container with notifications
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-02
 */

import * as React from 'react';
import * as styles from './BackendNotificationsArea.m.scss';
import {BackendNotificationsStore} from './store/BackendNotificationsStore';
import {ButtonWithConfirmation, joinClassNames, SIZE} from '@unidata/uikit';
import {observer} from 'mobx-react';
import {BackendNotificationItem} from './items/BackendNotificationItem';
import {BackendNotificationModel} from '../../model/notification/BackendNotificationModel';
import {i18n} from '@unidata/core-app';

interface IProps {
}

@observer
export class BackendNotificationsArea extends React.Component<IProps> {

    private prevNotificationAreaVisible: boolean;

    private divRef: HTMLElement;

    constructor (props: IProps) {
        super(props);

        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.setDivRef = this.setDivRef.bind(this);
    }

    get className () {
        return joinClassNames(
            styles.notificationArea,
            [styles.notificationAreaVisible, Boolean(BackendNotificationsStore.isNotificationAreaVisible())],
        );
    }

    setDivRef (node: any) {
        this.divRef = node;
    }

    handleClickOutside (event: MouseEvent) {
        if (!(event.target && event.target instanceof HTMLElement)) {
            return;
        }

        if (this.divRef.contains(event.target)) {
            return;
        }

        BackendNotificationsStore.setNotificationAreaVisible(false);
    }

    addDocumentClickListener () {
        document.addEventListener('click', this.handleClickOutside);
    }

    removeDocumentClickListener () {
        document.removeEventListener('click', this.handleClickOutside);
    }

    componentDidUpdate () {
        if (BackendNotificationsStore.isNotificationAreaVisible()) {
            this.addDocumentClickListener();
        } else {
            this.removeDocumentClickListener();
        }
    }

    componentWillUnmount () {
        this.removeDocumentClickListener();
    }

    onClearClick () {
        if (!BackendNotificationsStore.notifications.length) {
            return;
        }

        BackendNotificationsStore.clear();

        BackendNotificationsStore
            .removeAll()
            .then(BackendNotificationsStore.load.bind(BackendNotificationsStore));
    }

    renderFooter (notifications: BackendNotificationModel[]) {
        return (
            <div className={styles.notificationAreaFooter}>
                <ButtonWithConfirmation
                    isDisabled={!Boolean(notifications.length)}
                    size={SIZE.SMALL}
                    isRound={true}
                    isGhost={true}
                    confirmationMessage={i18n.t('notifications>confirmRemoveNotification')}
                    onConfirm={this.onClearClick}
                >
                    {i18n.t('notifications>clear')}
                </ButtonWithConfirmation>
            </div>
        );
    }

    render () {
        let notifications = BackendNotificationsStore.notifications.slice(),
            storeIsLoading = BackendNotificationsStore.isLoading(),
            notificationAreaVisible = BackendNotificationsStore.isNotificationAreaVisible(),
            prevNotificationAreaVisible = this.prevNotificationAreaVisible;

        if (
            notificationAreaVisible &&
            prevNotificationAreaVisible !== notificationAreaVisible &&
            !storeIsLoading
        ) {
            BackendNotificationsStore.load();
        }

        this.prevNotificationAreaVisible = notificationAreaVisible;

        return (
            <div ref={this.setDivRef} className={this.className}>
                <div className={styles.notificationAreaItems}>
                    {notifications.map(function (notification: BackendNotificationModel) {
                        return <BackendNotificationItem key={notification.id.getValue()} notification={notification} />;
                    })}
                </div>
                {this.renderFooter(notifications)}
            </div>
        );
    }

}
