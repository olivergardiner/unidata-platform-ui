﻿/**
 * Static class with menu and routing settings
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */

import {
    BackendStorageKeys,
    BackendStorageService,
    IMenuGroup,
    IMenuItem, MenuManager,
    UserManager
} from '@unidata/core-app';
import {action, observable, toJS} from 'mobx';

export class MenuConfig {

    private static config: IMenuGroup[];
    private static pinned: {[itemName: string]: boolean}; // observable
    private static groupsPinCounter: {[groupName: string]: number};
    private static groupsPin: {[groupName: string]: boolean}; // observable
    private static groupNameByItemName: {[itemName: string]: string} = {};
    private static itemsList: {[itemName: string]: IMenuItem} = {};
    @observable static pinEditMode: boolean = false;

    /**
     * Groups
     */
    static getGroups () {
        return this.initConfig();
    }

    /**
     * Flat list of all sections
     */
    static getItems () {
        this.initConfig();

        return this.itemsList;
    }

    /**
     * Whether the menu item is loaded
     */
    static isPinned (itemName: string): boolean {
        return this.pinned[itemName];
    }

    /**
     * Are there elements in the group with a pin
     */
    static hasPinnedItems (groupName: string): boolean {
        return this.groupsPin[groupName];
    }

    @action
    static setPinned (itemName: string, pinned: boolean) {
        let groupName = this.groupNameByItemName[itemName];

        if (this.pinned[itemName] !== pinned) {
            if (pinned) {
                this.groupsPinCounter[groupName]++;
            } else {
                this.groupsPinCounter[groupName]--;
            }

            this.groupsPin[groupName] = Boolean(this.groupsPinCounter[groupName]);
            this.pinned[itemName] = pinned;
        }
    }

    static savePinned () {
        return BackendStorageService.saveRecord(
            UserManager.getUserLogin() as string,
            BackendStorageKeys.MAIN_MENU_PIN,
            JSON.stringify(toJS(this.pinned))
        );
    }

    static loadPinned () {
        let promise = BackendStorageService.loadRecords(
            UserManager.getUserLogin() as string,
            BackendStorageKeys.MAIN_MENU_PIN
        );

        this.initConfig();

        promise = promise
            .then(function (result) {
                if (result && result[0]) {
                    return JSON.parse(result[0].value);
                }
            })
            .then(this.onPinsLoaded.bind(this));

        return promise;
    }

    private static onPinsLoaded (result: {[itemName: string]: boolean} | undefined) {
        for (let itemName in result) {
            if (Object.prototype.hasOwnProperty.call(result, itemName)) {
                this.setPinned(itemName, result[itemName]);
            }
        }
    }

    private static initConfig () {
        if (this.config) {
            return this.config;
        }

        const config = MenuManager.getMenuList();

        this.initPinned(config);

        this.config = config;

        return this.config;
    }

    private static initPinned (config: IMenuGroup[]) {
        if (this.pinned) {
            return;
        }

        let pinned: any = {},
            groupsPin: any = {},
            groupsPinCounter: any = {};

        for (let group of config) {
            groupsPinCounter[group.name] = 0;

            for (let item of group.items) {
                let itemIsPinned = Boolean(item.pinned);

                if (itemIsPinned) {
                    groupsPinCounter[group.name]++;
                }

                this.groupNameByItemName[item.name] = group.name;

                pinned[item.name] = itemIsPinned;
                this.itemsList[item.name] = item;
            }

            groupsPin[group.name] = Boolean(groupsPinCounter[group.name]);
        }

        this.groupsPinCounter = groupsPinCounter;
        this.groupsPin = observable(groupsPin);
        this.pinned = observable(pinned);
    }

}

