/**
 * Logo
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';
import {i18n, ServerUrlManager} from '@unidata/core-app';
import {observer} from 'mobx-react';

interface IProps {
    hideText?: boolean;
}

@observer
export class Logo extends React.Component<IProps> {

    render () {
        const prefix = ServerUrlManager.getResourcePrefixUrl();

        let logoBasePath = `${prefix}/react-resources/svg/logo`,
            signPath = `${logoBasePath}/main-menu-logo-sign.svg`,
            hideText = this.props.hideText,
            textPath = '';

        if (!hideText) {
            textPath = `${logoBasePath}/main-menu-logo-text-${i18n.locale}.svg`;
        }

        return (
            <div className='ud-logo'>
                <div className='ud-logo-sign'>
                    <object data={signPath} type="image/svg+xml" />
                </div>
                {!hideText && (
                    <div className='ud-logo-text'>
                        <object data={textPath} type="image/svg+xml" />
                    </div>
                )}
            </div>
        );
    }

}
