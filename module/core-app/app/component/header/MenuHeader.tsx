/**
 * Header with menu
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';
import {Layout} from 'antd';
import {Logo} from './logo/Logo';
import {PinnedMenu} from './menu/PinnedMenu';
import {MainMenuButton} from './menu/MainMenuButton';
import {MenuConfig} from './MenuConfig';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import {RightHeaderMenu} from './RightHeaderMenu';
import {DraftIndicator} from './menu/item/DraftIndicator';

@observer
export class MenuHeader extends React.Component {

    @observable isLoading = true;

    constructor (props: any) {
        super(props);

        this.onLoaded = this.onLoaded.bind(this);
        MenuConfig.loadPinned().then(this.onLoaded);
    }

    @action
    onLoaded () {
        this.isLoading = false;
    }

    render () {
        let isLoading = this.isLoading;

        return (
            <Layout.Header className='ud-menu-header'>
                <Logo/>
                {isLoading ? null : <MainMenuButton/>}
                <DraftIndicator />
                {isLoading ? null : <PinnedMenu/>}
                <RightHeaderMenu/>
            </Layout.Header>
        );
    }

}
