export const draftModeSwitcher: string;
export const draftModeOn: string;
export const modal: string;
export const modalContent: string;
export const modalFooter: string;
