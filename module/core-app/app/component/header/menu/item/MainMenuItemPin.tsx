/**
 * Displays the status of the pin
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';
import {MenuConfig} from '../../MenuConfig';
import {observer} from 'mobx-react';

interface IProps {
    name: string;
}

@observer
export class MainMenuItemPin extends React.Component<IProps> {

    /**
     * css class for pin
     */
    get pinClassName (): string {
        let result = ['ud-main-menu-item-pin'];

        if (MenuConfig.isPinned(this.props.name)) {
            result.push('ud-main-menu-item-pin-is-pinned');
        }

        return result.join(' ');
    }

    render () {
        return <div className={this.pinClassName} />;
    }

}
