/**
 * Interface that handles clicks on a menu item
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';

export interface IMenuItemClickHandler {
    (event: React.MouseEvent, itemName: string): void;
}
