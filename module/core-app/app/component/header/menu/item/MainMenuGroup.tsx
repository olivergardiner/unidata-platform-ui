/**
 * Group of menu items
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';
import {MenuConfig} from '../../MenuConfig';
import {MainMenuItem} from './MainMenuItem';
import {observer} from 'mobx-react';
import {IMenuItemClickHandler} from './IMenuItemClickHandler';
import {Route, RouteComponentProps, matchPath} from 'react-router';
import {i18n, IMenuGroup, IMenuItem} from '@unidata/core-app';
import {joinClassNames} from '@unidata/uikit';

interface IProps extends IMenuGroup {
    onItemClick?: IMenuItemClickHandler;
    hideUnpinned?: boolean;
}

@observer
export class MainMenuGroup extends React.Component<IProps> {

    constructor (props: IProps) {
        super(props);

        this.renderItem = this.renderItem.bind(this);
    }

    renderItems () {
        return this.props.items.map(this.renderItem);
    }

    renderItem (item: IMenuItem) {
        return <Route
            key={item.name}
            render={this.renderMenuItem.bind(this, item)}
        />;
    }

    renderMenuItem (item: IMenuItem, props: RouteComponentProps<any>) {
        const path = props.location.pathname;

        const isActive = item.isActiveFor === undefined ? path === item.route :
            item.isActiveFor.some((route) => matchPath(path, {
                path: route
            }) !== null);

        return <MainMenuItem
            key={item.name}
            hideUnpinned={this.props.hideUnpinned}
            onClick={this.props.onItemClick}
            active={isActive}
            {...item}
        />;
    }

    get className () {
        return joinClassNames(
            'ud-main-menu-group', // base class
            ['is-empty', !MenuConfig.hasPinnedItems(this.props.name)]
        );
    }

    render () {
        return (
            <div className={this.className}>
                {this.props.hideUnpinned !== true && (
                    <div className='ud-main-menu-group-title'>
                        {i18n.t(`page.header>${this.props.name}`)}
                    </div>
                )}
                <div className='ud-main-menu-group-items'>
                    {this.renderItems()}
                </div>
            </div>
        );
    }
}
