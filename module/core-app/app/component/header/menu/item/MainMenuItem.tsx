/**
 * Menu Item
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';
import {MenuConfig} from '../../MenuConfig';
import {i18n, IMenuItem} from '@unidata/core-app';
import {Link} from 'react-router-dom';
import {MainMenuItemPin} from './MainMenuItemPin';
import {observer} from 'mobx-react';
import {IMenuItemClickHandler} from './IMenuItemClickHandler';
import {MetaModelDraft} from '@unidata/meta';
import {joinClassNames, MenuItem} from '@unidata/uikit';

interface IProps extends IMenuItem {
    active?: boolean;
    onClick?: IMenuItemClickHandler;
    hideUnpinned?: boolean;
}

@observer
export class MainMenuItem extends React.Component<IProps> {

    constructor (props: IProps) {
        super(props);
        this.onItemClick = this.onItemClick.bind(this);
    }

    onItemClick (event: React.MouseEvent) {
        if (this.disabled) {
            event.preventDefault();

            return;
        }

        if (this.props.onClick) {
            this.props.onClick(event, this.props.name);
        }
    }

    get className () {
        let pinned = MenuConfig.isPinned(this.props.name);

        return joinClassNames(
            'ud-main-menu-item', // base class
            ['is-pinned', pinned],
        );
    }

    get disabled () {
        return MetaModelDraft.draftMode && !Boolean(this.props.activeInDraftMode);
    }

    render () {
        const isPinned = MenuConfig.isPinned(this.props.name);

        if (this.props.hideUnpinned === true) {
            return (
                <Link to={this.props.route} className={this.className} onClick={this.onItemClick}>
                    <MenuItem
                        isShow={isPinned === true}
                        icon={this.props.icon}
                        title={i18n.t(`app.menu>${this.props.name}`)}
                        isActive={this.props.active}
                        isDisabled={this.disabled}
                        isMinimal={true}
                        menuColor={this.props.menuColor as any}
                    />
                </Link>
            );
        }

        return (
            <Link to={this.props.route} className={this.className} onClick={this.onItemClick}>
                <MainMenuItemPin name={this.props.name}/>
                <MenuItem
                    icon={this.props.icon}
                    title={i18n.t(`app.menu>${this.props.name}`)}
                    isActive={this.props.active}
                    isDisabled={this.disabled}
                    menuColor={this.props.menuColor as any}
                />
            </Link>
        );
    }
}
