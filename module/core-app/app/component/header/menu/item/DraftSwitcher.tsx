/**
 * Draft mode switch
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-12
 */

import React from 'react';
import {observer} from 'mobx-react';
import {Switch} from 'antd';
import {AppTypeManager, i18n} from '@unidata/core-app';
import * as styles from './DraftSwitcher.m.scss';
import {MetaModelDraft} from '@unidata/meta';
import {Button, Icon, INTENT, joinClassNames, Modal} from '@unidata/uikit';
import {action, observable} from 'mobx';

@observer
export class DraftSwitcher extends React.Component {

    @observable
    private confirmModalVisible: boolean = false;

    constructor (props: any) {
        super(props);

        this.onSwitcherClick = this.onSwitcherClick.bind(this);
        this.onExitClick = this.onExitClick.bind(this);
        this.onCancelClick = this.onCancelClick.bind(this);
    }

    get className () {
        return joinClassNames(
            styles.draftModeSwitcher,
            [styles.draftModeOn, MetaModelDraft.draftMode]
        );
    }

    get text () {
        if (MetaModelDraft.draftMode) {
            return i18n.t('app.menu>draft>on');
        } else {
            return i18n.t('app.menu>draft>off');
        }
    }

    @action
    setConfirmModalVisible (confirmModalVisible: boolean) {
        this.confirmModalVisible = confirmModalVisible;
    }

    onSwitcherClick () {
        if (MetaModelDraft.draftMode) {
            this.setConfirmModalVisible(true);
        } else {
            MetaModelDraft.draftMode = true;
        }
    }

    onExitClick () {
        MetaModelDraft.draftMode = false;
        this.setConfirmModalVisible(false);
    }

    onCancelClick () {
        this.setConfirmModalVisible(false);
    }

    get modalFooter () {
        return (
            <div className={styles.modalFooter}>
                <Button
                    isGhost={true}
                    onClick={this.onCancelClick}
                    data-qaid='cancel'
                >
                    {i18n.t('common:cancel_noun')}
                </Button>
                <Button
                    intent={INTENT.PRIMARY}
                    onClick={this.onExitClick}
                    data-qaid='exit'
                >
                    {i18n.t('common:exit')}
                </Button>
            </div>
        );
    }

    render () {
        if (!AppTypeManager.isDataAdmin()) {
            return <div/>;
        }

        return (
            <React.Fragment>
                <div className={this.className}>
                    <div onClick={this.onSwitcherClick}>
                        <Switch checked={MetaModelDraft.draftMode} size={'small'} />{this.text}
                    </div>
                </div>
                <Modal
                    className={styles.modal}
                    isOpen={this.confirmModalVisible}
                    header={i18n.t('app.menu>draft>draft')}
                    shouldCloseOnClickOutside={false}
                    onClose={this.onCancelClick}
                    footer={this.modalFooter}
                    data-qaid='switchDraftModal'
                >
                    <div className={styles.modalContent}>
                        <Icon name={'warning'} />
                        <div>
                            <p><b>{i18n.t('app.menu>draft>confirmText1')}</b></p>
                            <p>{i18n.t('app.menu>draft>confirmText2')}</p>
                        </div>
                    </div>
                </Modal>
            </React.Fragment>
        );
    }
}
