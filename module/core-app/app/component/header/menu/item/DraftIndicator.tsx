/**
 * Draft mode switch
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-12
 */

import React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import * as styles from './DraftIndicator.m.scss';
import {MetaModelDraft} from '@unidata/meta';
import {joinClassNames} from '@unidata/uikit';

@observer
export class DraftIndicator extends React.Component {

    get className () {
        return joinClassNames(
            styles.draftModeIndicator,
            [styles.draftModeOn, MetaModelDraft.draftMode]
        );
    }

    render () {
        return (
            <div className={this.className}>
                {i18n.t('app.menu>draft>draft')}
            </div>
        );
    }
}
