/**
 * A menu filled with items
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';
import {MainMenu} from './MainMenu';

export class PinnedMenu extends React.Component {

    render () {
        return <MainMenu className='ud-pinned-menu' hideUnpinned={true} />;
    }

}
