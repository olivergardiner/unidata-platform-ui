/**
 * Button that opens the drop-down menu
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';
import {Popover} from 'antd';
import {FullMenu} from './FullMenu';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import {MenuConfig} from '../MenuConfig';
import {Button, INTENT, SIZE} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';

@observer
export class MainMenuButton extends React.Component {

    @observable popoverVisible: boolean = false;

    constructor (props: any) {
        super(props);
        this.onButtonClick = this.onButtonClick.bind(this);
        this.setPopoverVisible = this.setPopoverVisible.bind(this);
    }

    onButtonClick () {
        // we forbid hiding the menu in editing mode
        if (MenuConfig.pinEditMode) {
            return;
        }

        this.setPopoverVisible(!this.popoverVisible);
    }

    @action
    setPopoverVisible (visible: boolean) {
        this.popoverVisible = visible;
    }

    get buttonCls () {
        let cls = ['ud-main-menu-button'];

        if (this.popoverVisible) {
            cls.push('is-active');
        }

        return cls.join(' ');
    }

    render () {
        return (
            <div className={this.buttonCls}>
                <Popover
                    placement="bottomLeft"
                    content={<FullMenu setPopoverVisible={this.setPopoverVisible}/>}
                    trigger="click"
                    visible={this.popoverVisible}
                    overlayClassName='ud-main-menu-popover'
                >
                    <Button
                        size={SIZE.EXTRA_LARGE}
                        isMinimal={true}
                        intent={INTENT.SECONDARY}
                        leftIcon={'menu'}
                        onClick={this.onButtonClick}
                        data-qaid='main'
                        title={i18n.t('app.menu>tooltip')}
                    />
                </Popover>
            </div>
        );
    }

}
