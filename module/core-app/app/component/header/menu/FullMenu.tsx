/**
 * Full menu, from setting up pins
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';
import {MainMenu} from './MainMenu';
import {MenuConfig} from '../MenuConfig';
import {i18n} from '@unidata/core-app';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import {Button, INTENT, joinClassNames} from '@unidata/uikit';
import {DraftSwitcher} from './item/DraftSwitcher';
import * as styles from './FullMenu.m.scss';
import {MetaModelDraft} from '@unidata/meta';

interface IProps {
    setPopoverVisible: {(visible: boolean): void};
}

@observer
export class FullMenu extends React.Component<IProps> {

    @observable
    mouseIsOver: boolean = false;

    prevDraftMode: boolean;

    divRef: HTMLDivElement;

    constructor (props: any) {
        super(props);

        this.onItemClick = this.onItemClick.bind(this);
        this.onMouseOver = this.onMouseOver.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);
        this.togglePinEditMode = this.togglePinEditMode.bind(this);
        this.hidePopover = this.hidePopover.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.setDivRef = this.setDivRef.bind(this);
    }

    componentDidMount () {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount () {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside (event: MouseEvent) {
        if (this.divRef &&
            !this.divRef.contains(event.target as Node) &&
            event.target &&
            event.target instanceof HTMLElement &&
            !event.target.classList.contains('ud-main-menu-button')
        ) {
            this.hidePopover();
        }
    }

    @action
    togglePinEditMode () {
        let pinEditMode = MenuConfig.pinEditMode;

        if (pinEditMode) {
            MenuConfig.savePinned();
        }

        MenuConfig.pinEditMode = !pinEditMode;
    }

    onMouseOver () {
        this.setMouseIsOver(true);
    }

    onMouseLeave () {
        this.setMouseIsOver(false);
    }

    @action
    setMouseIsOver (flag: boolean) {
        this.mouseIsOver = flag;
    }

    onItemClick (event: React.MouseEvent, itemName: string) {
        if (MenuConfig.pinEditMode) {
            MenuConfig.setPinned(itemName, !MenuConfig.isPinned(itemName));
            event.preventDefault();
        } else {
            this.hidePopover();
        }
    }

    get fullMenuClass () {
        let css = ['ud-main-menu-full'];

        if (MenuConfig.pinEditMode) {
            css.push('ud-main-menu-pin-edit-mode');
        }

        return css.join(' ');
    }

    get iconType () {
        return MenuConfig.pinEditMode ? 'check' : 'filledSetting';
    }

    get buttonContent () {
        if (MenuConfig.pinEditMode) {
            return i18n.t('common:save');
        }

        return i18n.t('common:configure');
    }

    hidePopover () {
        // we forbid hiding the menu in editing mode
        if (!MenuConfig.pinEditMode) {
            this.props.setPopoverVisible(false);
        }
    }

    setDivRef (node: HTMLDivElement) {
        this.divRef = node;
    }

    get buttonClassName () {
        return joinClassNames(
            'ud-pin-edit-button',
            ['is-disabled', MetaModelDraft.draftMode]
        );
    }

    render () {
        let draftMode = MetaModelDraft.draftMode;

        // exit editing mode if draft mode is enabled
        if (draftMode && draftMode !== this.prevDraftMode && MenuConfig.pinEditMode) {
            this.togglePinEditMode();
        }

        this.prevDraftMode = draftMode;

        return (
            <div ref={this.setDivRef} className={this.fullMenuClass} data-qaid='main-menu'>
                <div className={styles.toolbar}>
                    <DraftSwitcher />
                    <div className={this.buttonClassName}>
                        <Button
                            isGhost={true}
                            isMinimal={true}
                            isDisabled={draftMode}
                            onClick={this.togglePinEditMode}
                            onMouseOver={this.onMouseOver}
                            onMouseLeave={this.onMouseLeave}
                            rightIcon={this.iconType}
                        >
                            {this.buttonContent}
                        </Button>
                    </div>
                </div>
                <MainMenu onItemClick={this.onItemClick} />
            </div>
        );
    }
}
