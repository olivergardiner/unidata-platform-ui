/**
 * Main menu-General class
 *
 * @author: Aleksandr Bavin
 * @date: 2019-12-05
 */
import * as React from 'react';
import {MenuConfig} from '../MenuConfig';
import {MainMenuGroup} from './item/MainMenuGroup';
import {observer} from 'mobx-react';
import {IMenuItemClickHandler} from './item/IMenuItemClickHandler';
import {IMenuGroup} from '@unidata/core-app';

interface IProps {
    onItemClick?: IMenuItemClickHandler;
    hideUnpinned?: boolean; // hide items that don't have a pin
    className?: string;
}

@observer
export class MainMenu extends React.Component<IProps> {

    static defaultProps = {
        hideUnpinned: false,
        className: 'ud-main-menu'
    };

    constructor (props: IProps) {
        super(props);
        this.renderMenuGroup = this.renderMenuGroup.bind(this);
    }

    renderMenuGroups () {
        return MenuConfig.getGroups().map(this.renderMenuGroup);
    }

    renderMenuGroup (group: IMenuGroup) {
        return <MainMenuGroup
            key={group.name}
            hideUnpinned={this.props.hideUnpinned}
            onItemClick={this.props.onItemClick}
            {...group}
        />;
    }

    render () {
        return (
            <div className={this.props.className}>
                {this.renderMenuGroups()}
            </div>
        );
    }
}
