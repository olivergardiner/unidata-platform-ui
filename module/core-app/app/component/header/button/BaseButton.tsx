/**
 * Base class for buttons in the header panel
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-17
 */
import * as React from 'react';
import {BaseButtonDecal} from './items/BaseButtonDecal';
import * as styles from './BaseButton.m.scss';
import {BaseButtonIcon} from './items/BaseButtonIcon';
import {BaseButtonText} from './items/BaseButtonText';
import {CssClasses} from '../CssClasses';
import {observer} from 'mobx-react';
import {joinClassNames, mergeClassNamesCollections} from '@unidata/uikit';

interface IProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    icons?: Array<string | null | undefined>;
    text?: string;
    forwardRef?: React.Ref<HTMLButtonElement>;
    decal?: React.ReactNode;
    cssClasses?: CssClasses<typeof styles>;
}

@observer
export class BaseButton extends React.Component<IProps> {

    private mergedCssClasses: CssClasses<typeof styles>;

    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    getClassName (cssClasses: CssClasses<typeof styles>) {
        let props = this.props;

        return joinClassNames(
            cssClasses.button,
            [cssClasses.buttonDisabled, Boolean(props.disabled)],
        );
    }

    renderButton () {
        let {decal, icons, cssClasses, text, forwardRef, disabled, ...restProps} = {... this.props},
            className,
            renderedIcons;

        cssClasses = this.cssClasses;
        className = this.getClassName(cssClasses);
        renderedIcons = this.renderIcons(cssClasses);

        return (
            <button {...restProps} ref={forwardRef} className={className}>
                {renderedIcons}
                <BaseButtonText cssClasses={cssClasses} text={text} />
                <BaseButtonDecal cssClasses={cssClasses}>{decal}</BaseButtonDecal>
            </button>
        );
    }

    renderIcons (cssClasses: CssClasses<typeof styles>) {
        let icons = this.props.icons,
            result = [];

        if (!icons || !icons.length) {
            return null;
        }

        for (let index = 0, ln = icons.length; index < ln; index++) {
            let iconName = icons[index];

            if (iconName) {
                result.push(
                    <BaseButtonIcon key={iconName + index} cssClasses={cssClasses} name={iconName} index={index} />
                );
            }
        }

        return result;
    }

    render () {
        return this.renderButton();
    }

}
