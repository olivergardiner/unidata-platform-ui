/**
 * Button text
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-18
 */
import * as React from 'react';
import {CssClasses} from '../../CssClasses';
import * as styles from './BaseButtonText.m.scss';
import {i18n} from '@unidata/core-app';
import {observer} from 'mobx-react';
import {mergeClassNamesCollections} from '@unidata/uikit';

interface IProps {
    text?: string;
    cssClasses?: CssClasses<typeof styles>;
}

@observer
export class BaseButtonText extends React.Component<IProps> {

    private mergedCssClasses: CssClasses<typeof styles>;

    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    render () {
        let value = this.props.text;

        if (!value) {
            return null;
        }

        value = i18n.t(value);

        return (
            <div className={this.cssClasses.text}>
                {value}
            </div>
        );
    }

}
