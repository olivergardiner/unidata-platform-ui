/**
 * The label on the button
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-18
 */
import * as React from 'react';
import {CssClasses} from '../../CssClasses';
import * as styles from './BaseButtonDecal.m.scss';
import {observer} from 'mobx-react';
import {mergeClassNamesCollections} from '@unidata/uikit';

interface IProps {
    cssClasses?: CssClasses<typeof styles>;
    children?: string | number | React.ReactNode;
}

@observer
export class BaseButtonDecal extends React.Component<IProps> {

    private mergedCssClasses: CssClasses<typeof styles>;

    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    render () {
        let children = this.props.children;

        if (!children) {
            return null;
        }

        return (
            <div className={this.cssClasses.decal}>
                {children}
            </div>
        );
    }

}
