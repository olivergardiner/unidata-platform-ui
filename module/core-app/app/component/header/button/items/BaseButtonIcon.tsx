/**
 * Button icon
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-18
 */
import * as React from 'react';
import * as styles from './BaseButtonIcon.m.scss';
import {CssClasses} from '../../CssClasses';
import {observer} from 'mobx-react';
import {joinClassNames, mergeClassNamesCollections} from '@unidata/uikit';

interface IProps {
    name: string;
    index: number;
    cssClasses?: CssClasses<typeof styles>;
}

@observer
export class BaseButtonIcon extends React.Component<IProps> {

    private mergedCssClasses: CssClasses<typeof styles>;

    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    get className (): string | undefined {
        let iconName = this.props.name;

        // for icons with the name un-icor-N, we leave the name as it is
        if (iconName.indexOf('un-icon-') === -1) {
            iconName = `icon-${this.props.name}`;
        }

        return joinClassNames(
            this.cssClasses.icon,
            iconName
        );
    }

    render () {
        return <i style={{order: this.props.index}} className={this.className} />;
    }

}
