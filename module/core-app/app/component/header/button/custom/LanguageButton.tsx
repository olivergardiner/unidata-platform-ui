/**
 * Language switch button
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */
import * as React from 'react';
import {HeaderPickerButton} from './HeaderPickerButton';
import {changeLanguage, i18n, Locales} from '@unidata/core-app';
import {InnerPickerButton} from './InnerPickerButton';
import {Confirm} from '@unidata/uikit';

interface IState {
    pickerVisible: boolean;
    isConfirmVisible: boolean;
    newLocale: string;
}

export class LanguageButton extends React.Component <{}, IState> {

    constructor (props: any) {
        super(props);

        this.state = {
            pickerVisible: false,
            isConfirmVisible: false,
            newLocale: Locales.Ru
        };
    }

    confirmChangeLanguage (locale: Locales) {
        this.setState({
            isConfirmVisible: true,
            newLocale: locale,
            pickerVisible: false
        });
    }

    onVisibleChange (visible: boolean) {
        this.setState({pickerVisible: visible});
    }

    onClose = () => {
        this.setState({isConfirmVisible: false});
    }

    onConfirm = () => {
        this.setState({isConfirmVisible: false});

        changeLanguage(this.state.newLocale).then(() => {
            window.location.reload();
        });
    }

    render () {
        return (
            <>
                <HeaderPickerButton
                    icon='flag2'
                    text={`app.locales>${i18n.locale}`}
                    pickerVisible={this.state.pickerVisible}
                    onVisibleChange={this.onVisibleChange.bind(this)}
                >
                    <InnerPickerButton
                        text={'app.locales>ru'}
                        onClick={this.confirmChangeLanguage.bind(this, Locales.Ru)}
                    />
                    <InnerPickerButton
                        text={'app.locales>en'}
                        onClick={this.confirmChangeLanguage.bind(this, Locales.En)}
                    />
                </HeaderPickerButton>
                <Confirm
                    isOpen={this.state.isConfirmVisible}
                    onClose={this.onClose}
                    onConfirm={this.onConfirm}
                    confirmationMessage={i18n.t('app.menu>locale>change')}
                />
            </>
        );
    }

}
