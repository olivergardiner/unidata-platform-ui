﻿/**
 * Button with a drop-down list
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */
import * as React from 'react';
import * as styles from './HeaderPickerButton.m.scss';
import {CssClasses} from '../../CssClasses';
import {Picker} from '../../picker/Picker';
import {HeaderButton} from './HeaderButton';
import {mergeClassNamesCollections} from '@unidata/uikit';

interface IProps {
    icon?: string;
    text?: string;
    count?: number;
    cssClasses?: CssClasses<typeof styles>;
    pickerVisible: boolean;
    onVisibleChange: (visible: boolean) => void;
}


export class HeaderPickerButton extends React.Component<IProps> {
    private mergedCssClasses: CssClasses<typeof styles>;

    constructor (props: IProps) {
        super(props);

        this.renderButton = this.renderButton.bind(this);
    }


    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    renderButton () {
        let {
            icon,
            text,
            count,
            children,
            cssClasses,
            pickerVisible,
            onVisibleChange,
            ...restProps
        } = {...this.props};

        return (
            <HeaderButton
                icons={[icon, 'chevron-down']}
                cssClasses={this.cssClasses}
                text={text}
                count={count}
                {...restProps}
            />
        );
    }

    render () {
        let children = this.props.children;

        return (
            <Picker
                placement='bottomRight'
                trigger='click'
                visible={this.props.pickerVisible}
                cssClasses={styles}
                content={children}
                onVisibleChange={this.props.onVisibleChange}
            >
                {this.renderButton()}
            </Picker>
        );
    }
}
