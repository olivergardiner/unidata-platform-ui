/**
 * User button
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */
import * as React from 'react';
import {HeaderPickerButton} from './HeaderPickerButton';
import {App, UserManager} from '@unidata/core-app';
import {InnerPickerButton} from './InnerPickerButton';
import {PickerSplitter} from './items/PickerSplitter';
import {ChangePasswordButton} from './items/ChangePasswordButton';
import {BackendNotificationsButton} from './BackendNotificationsButton';
import {BackendNotificationsCountStorePoller} from '../../../benotifications/store/BackendNotificationsCountStorePoller';
import * as styles from './UserButton.m.scss';
import {observer} from 'mobx-react';
import {AboutSystemButton} from './AboutSystemButton';

interface IProps {
}

interface IState {
    isChangePasswordOpen: boolean;
    isAboutSystemOpen: boolean;
    pickerVisible: boolean;
}

@observer
export class UserButton extends React.Component<IProps, IState> {

    poller = new BackendNotificationsCountStorePoller();

    user: any;

    constructor (props: IProps) {
        super(props);

        this.user = UserManager.getUser();
        this.onExitClick = this.onExitClick.bind(this);
        this.onNotificationsButtonClick = this.onNotificationsButtonClick.bind(this);
        this.onAboutSystemButtonClick = this.onAboutSystemButtonClick.bind(this);
        this.onAboutSystemClose = this.onAboutSystemClose.bind(this);

        this.state = {
            isChangePasswordOpen: false,
            isAboutSystemOpen: false,
            pickerVisible: false
        };
    }

    componentDidMount (): void {
        this.poller.start();
    }

    componentWillUnmount (): void {
        this.poller.stop();
    }

    get text (): string {
        let text: string = this.user.fullName.trim();

        if (text === '') {
            text = this.user.login;
        }

        return text;
    }

    /**
     * Calling the password change window
     */
    onChangePasswordClick () {
        this.setState({isChangePasswordOpen: true});
    }

    /**
     * Closing the password change window
     */
    onChangePasswordClose () {
        this.setState({isChangePasswordOpen: false, pickerVisible: false});
    }

    /**
     * Hiding the submenu
     */
    onVisibleChange (visible: boolean) {
        if (!this.state.isChangePasswordOpen) {
            this.setState({pickerVisible: visible});
        }
    }

    onNotificationsButtonClick () {
        this.setState({pickerVisible: false});
    }

    onAboutSystemButtonClick () {
        this.setState({isAboutSystemOpen: true});
    }

    onAboutSystemClose () {
        this.setState({isAboutSystemOpen: false, pickerVisible: false});
    }

    onExitClick () {
        App.deauthenticate();
    }

    render () {
        return (
            <HeaderPickerButton
                icon='user'
                text={this.text}
                count={BackendNotificationsCountStorePoller.count}
                pickerVisible={this.state.pickerVisible}
                onVisibleChange={this.onVisibleChange.bind(this)}
                cssClasses={styles}
                data-qaid='user'
            >
                <div data-qaid='user-menu'>
                    <ChangePasswordButton
                        isChangePasswordOpen={this.state.isChangePasswordOpen}
                        onChangePasswordClick={this.onChangePasswordClick.bind(this)}
                        onChangePasswordClose={this.onChangePasswordClose.bind(this)}
                    />
                    <BackendNotificationsButton
                        onClick={this.onNotificationsButtonClick}
                    />
                    <AboutSystemButton
                        onClick={this.onAboutSystemButtonClick}
                        isModalOpen={this.state.isAboutSystemOpen}
                        onModalClose={this.onAboutSystemClose}
                    />
                    <PickerSplitter/>
                    <InnerPickerButton icon='exit' text={'app.menu>user>exit'} onClick={this.onExitClick}/>
                </div>
            </HeaderPickerButton>
        );
    }

}
