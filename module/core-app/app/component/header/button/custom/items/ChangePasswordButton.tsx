/**
 *  Password change button
 *
 * @author Vladimir Stebunov
 * @date 2020-03-10
 */
import * as React from 'react';
import {InnerPickerButton} from '../InnerPickerButton';
import {ChangePasswordWindow} from '../../../../changepassword/changepasswordform/ChangePasswordWindow';

interface IProps {
    onChangePasswordClick: () => void;
    onChangePasswordClose: () => void;
    isChangePasswordOpen: boolean;
}

export class ChangePasswordButton extends React.Component<IProps> {

    render () {
        const {
            onChangePasswordClick,
            onChangePasswordClose,
            isChangePasswordOpen
        } = this.props;

        return (
            <React.Fragment>
                <InnerPickerButton
                    icon='lock'
                    text={'app.menu>user>changePassword'}
                    onClick={onChangePasswordClick}
                />
                <ChangePasswordWindow
                    onClose={onChangePasswordClose}
                    isOpen={isChangePasswordOpen}
                    hasClose={true} />
            </React.Fragment>
        );
    }
}
