/**
 * Separator for buttons in the drop-down menu
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */
import * as React from 'react';
import * as styles from './PickerSplitter.m.scss';
import {Splitter} from '../../../splitter/Splitter';

export class PickerSplitter extends React.Component {

    render () {
        return <Splitter cssClasses={styles} />;
    }

}
