/**
 * Button counter
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-18
 */
import * as React from 'react';
import * as styles from './Counter.m.scss';
import {observer} from 'mobx-react';
import {CssClasses} from '../../../CssClasses';
import {mergeClassNamesCollections} from '@unidata/uikit';

interface IProps {
    count?: number;
    cssClasses?: CssClasses<typeof styles>;
}

@observer
export class Counter extends React.Component<IProps> {

    private mergedCssClasses: CssClasses<typeof styles>;

    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    render () {
        let value: string | number | undefined = this.props.count;

        if (!value) {
            return null;
        }

        // if the counter is greater than 99, we display " 99+" for short
        if (value > 99) {
            value = '99+';
        }

        return <div className={this.cssClasses.counter}><span>{value}</span></div>;
    }

}
