/**
 * Button in the drop-down menu
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */
import * as React from 'react';
import * as styles from './InnerPickerButton.m.scss';
import {CssClasses} from '../../CssClasses';
import {BaseButton} from '../BaseButton';
import {observer} from 'mobx-react';
import {Counter} from './items/Counter';
import {mergeClassNamesCollections} from '@unidata/uikit';

interface IProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    forwardRef?: React.Ref<HTMLButtonElement>;
    icon?: string;
    text?: string;
    count?: number;
    cssClasses?: CssClasses<typeof styles>;
}

@observer
export class InnerPickerButton extends React.Component<IProps> {

    private mergedCssClasses: CssClasses<typeof styles>;

    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    render () {
        let {cssClasses, count, icon, ...restProps} = {... this.props};

        cssClasses = this.cssClasses;

        return (
            <BaseButton
                icons={[icon]}
                cssClasses={this.cssClasses}
                decal={<Counter cssClasses={{counter: cssClasses.counter}} count={count} />}
                {...restProps}
            />
        );
    }

}
