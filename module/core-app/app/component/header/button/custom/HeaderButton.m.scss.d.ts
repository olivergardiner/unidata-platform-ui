export const button: string;
export const buttonDisabled: string;
export const icon: string;
export const text: string;
export const decal: string;
export const counter: string;
