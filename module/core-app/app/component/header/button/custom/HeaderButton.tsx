/**
 * General button for the menu bar
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */
import * as React from 'react';
import * as styles from './HeaderButton.m.scss';
import {CssClasses} from '../../CssClasses';
import {BaseButton} from '../BaseButton';
import {Counter} from './items/Counter';
import {observer} from 'mobx-react';
import {mergeClassNamesCollections} from '@unidata/uikit';

interface IProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    icons?: Array<string | null | undefined>;
    text?: string;
    count?: number;
    forwardRef?: React.Ref<HTMLButtonElement>;
    cssClasses?: CssClasses<typeof styles>;
}

@observer
export class HeaderButton extends React.Component<IProps> {

    private mergedCssClasses: CssClasses<typeof styles>;

    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    render () {
        let {cssClasses, className, count, ...restProps} = {... this.props};

        cssClasses = this.cssClasses;

        return (
            <BaseButton
                cssClasses={cssClasses}
                decal={<Counter cssClasses={{counter: cssClasses.counter}} count={count} />}
                {...restProps}
            />
        );
    }

}
