/**
 * Notification button
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-03
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {BackendNotificationsCountStorePoller} from '../../../benotifications/store/BackendNotificationsCountStorePoller';
import {BackendNotificationsStore} from '../../../benotifications/store/BackendNotificationsStore';
import {InnerPickerButton} from './InnerPickerButton';

interface IProps {
    onClick?: {(event: React.MouseEvent): void};
}

@observer
export class BackendNotificationsButton extends React.Component<IProps> {

    private poller = new BackendNotificationsCountStorePoller();

    constructor (props: IProps) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }

    componentDidMount (): void {
        this.poller.start();
    }

    componentWillUnmount (): void {
        this.poller.stop();
    }

    onClick (event: React.MouseEvent) {
        BackendNotificationsStore.setNotificationAreaVisible(true);

        if (this.props.onClick) {
            this.props.onClick(event);
        }
    }

    render () {
        return (
            <InnerPickerButton
                onClick={this.onClick}
                count={BackendNotificationsCountStorePoller.count}
                text='app.menu>user>notifications'
                icon='un-icon-6'
            />
        );
    }

}
