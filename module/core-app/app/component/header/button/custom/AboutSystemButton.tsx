/**
 * Button to open the information window " about the System"
 *
 * @author: Denis Makarov
 * @date: 2020-04-05
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {InnerPickerButton} from './InnerPickerButton';
import {AboutSystemModal} from '../../../aboutsystem/AboutSystemModal';
import {AboutSystemStore} from '../../../aboutsystem/store/AboutSystemStore';

interface IProps {
    onClick?: {(event: React.MouseEvent): void};
    onModalClose: () => void;
    isModalOpen: boolean;
}

@observer
export class AboutSystemButton extends React.Component<IProps> {

    store: AboutSystemStore;

    constructor (props: IProps) {
        super(props);

        this.store = new AboutSystemStore();
        this.onClick = this.onClick.bind(this);
    }

    onClick (event: React.MouseEvent) {
        if (this.props.onClick) {
            this.props.onClick(event);
        }
    }

    render () {
        return (
            <>
                <InnerPickerButton
                    onClick={this.onClick}
                    text={'app.menu>user>about'}
                    icon='cog'
                />
                <AboutSystemModal
                    isOpen={this.props.isModalOpen}
                    store={this.store}
                    onClose={this.props.onModalClose}
                />
            </>
        );
    }

}
