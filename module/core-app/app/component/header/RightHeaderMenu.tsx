/**
 * Buttons on the right side of the header
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */
import * as React from 'react';
import {UserButton} from './button/custom/UserButton';
import {LanguageButton} from './button/custom/LanguageButton';
import {Splitter} from './splitter/Splitter';
import * as styles from './RightHeaderMenu.m.scss';
import {observer} from 'mobx-react';
import {ueModuleManager, UeModuleTypeComponent} from '@unidata/core';
import {HeaderButton} from './button/custom/HeaderButton';
import {AppTypeManager} from '@unidata/core-app';

@observer
export class RightHeaderMenu extends React.Component {

    renderWorkflowButton () {
        let workflowButton = ueModuleManager.getModuleById('workflowMenuButton');

        if (workflowButton) {
            if (ueModuleManager.isComponentModule(workflowButton, UeModuleTypeComponent.DATA_PROVIDER) &&
                workflowButton.default.resolver()
            ) {
                return (
                    <React.Fragment>
                        <workflowButton.default.component>
                            {this.renderWorkflowButtonContent}
                        </workflowButton.default.component>
                        <Splitter cssClasses={{splitter: styles.splitter}} />
                    </React.Fragment>
                );
            }
        }

        return null;
    }

    renderWorkflowButtonContent (count: number) {
        return (
            <a href='#/tasks'>
                <HeaderButton
                    icons={['clipboard-check']}
                    count={count}/>
            </a>
        );
    }

    render () {
        const isShowLanguageButton = AppTypeManager.isSystemAdmin() &&
            AppTypeManager.isDataAdmin() && AppTypeManager.isDataSteward();

        return (
            <div className={styles.rightHeaderMenu}>
                {
                    isShowLanguageButton &&
                    (
                        <>
                            <LanguageButton/>
                            <Splitter cssClasses={{splitter: styles.splitter}} />
                        </>
                    )
                }
                {this.renderWorkflowButton()}
                <UserButton />
            </div>
        );
    }

}
