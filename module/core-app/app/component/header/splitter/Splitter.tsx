/**
 * The delimiter for the buttons
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */
import * as React from 'react';
import * as styles from './Splitter.m.scss';
import {CssClasses} from '../CssClasses';
import {mergeClassNamesCollections} from '@unidata/uikit';

interface IProps {
    cssClasses?: CssClasses<typeof styles>;
}

export class Splitter extends React.Component<IProps> {

    private mergedCssClasses: CssClasses<typeof styles>;

    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    render () {
        return <div className={this.cssClasses.splitter} />;
    }

}
