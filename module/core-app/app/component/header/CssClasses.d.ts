export type CssClasses<T> = Partial<Record<keyof T, string>>;
