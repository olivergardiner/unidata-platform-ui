/**
 * Custom Popover
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */
import * as React from 'react';
import {Popover as AntdPopover} from 'antd';
import {PopoverProps} from 'antd/lib/popover';
import * as styles from './Picker.m.scss';
import {CssClasses} from '../CssClasses';
import {mergeClassNamesCollections} from '@unidata/uikit';

interface IProps extends PopoverProps {
    cssClasses?: CssClasses<typeof styles>;
}

export class Picker extends React.Component<IProps> {
    private mergedCssClasses: CssClasses<typeof styles>;

    constructor (props: IProps) {
        super(props);
    }

    get cssClasses () {
        if (!this.mergedCssClasses) {
            this.mergedCssClasses = mergeClassNamesCollections(styles, this.props.cssClasses) || {};
        }

        return this.mergedCssClasses;
    }

    render () {
        let {children, overlayClassName, ...restProps} = {...this.props};

        return (
            <AntdPopover
                overlayClassName={this.cssClasses.picker}
                {...restProps}
            >
                {children}
            </AntdPopover>
        );
    }
}
