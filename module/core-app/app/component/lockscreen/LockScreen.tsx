/**
 *  Screen lock (by session timeout)
 *
 * @author Sergey Shishigin
 * @date 2020-02-10
 */
import * as React from 'react';
import {RefObject} from 'react';
import {Button, Icon, Input, INTENT} from '@unidata/uikit';
import {
    App,
    AuthenticateDataType,
    AuthorizationService, AuthTokenManager,
    GlobalHotKeyManager,
    hasPrintableChars,
    i18n,
    Poller,
    UserManager
} from '@unidata/core-app';
import * as styles from './LockScreen.m.scss';

interface IProps {}
interface IState {
    user: string;
    password: string;
}

export class LockScreen extends React.Component<IProps, IState> {
    static ref: RefObject<HTMLDivElement> = React.createRef();
    static loginRef: RefObject<any> = React.createRef();
    static passwordRef: RefObject<any> = React.createRef();

    state: IState = {
        user: '',
        password: ''
    };

    static lock () {
        let element = LockScreen.ref.current;

        if (element) {
            element.style.display = 'block';
        }

        AuthTokenManager.setToken(null);
        GlobalHotKeyManager.disable();
        Poller.pauseAll();
        LockScreen.loginRef.current.focus();
    }

    static unlock () {
        let element = LockScreen.ref.current;

        if (element) {
            element.style.display = 'none';
        }

        GlobalHotKeyManager.enable();
        Poller.resumeAll();
    }

    reset () {
        this.setState({
            user: '',
            password: ''
        });
    }

    login (user: string, password: string) {
        let currentUsername = UserManager.getUserLogin();

        AuthorizationService.login(user, password)
            .then((authenticateData: AuthenticateDataType) => {
                let username;

                App.authenticate(authenticateData);
                username = UserManager.getUserLogin();

                if (username !== currentUsername) {
                    App.reload();
                }

                LockScreen.unlock();
                this.reset();
            });
    }

    onLoginButtonClick () {
        let user = this.state.user,
            password = this.state.password;

        this.login(user, password);
    }

    onUserChange (e: React.SyntheticEvent<HTMLInputElement>) {
        const user = e.currentTarget.value;

        this.setState({user: user});
    }

    onPasswordChange (e: React.SyntheticEvent<HTMLInputElement>) {
        const password = e.currentTarget.value;

        this.setState({password: password});
    }

    onLoginBlur = () => {
        LockScreen.passwordRef.current.focus();
    }

    onPasswordBlur = () => {
        LockScreen.loginRef.current.focus();
    }

    onLoginFocus = () => {
        LockScreen.loginRef.current.select();
    }

    onPasswordFocus = () => {
        LockScreen.passwordRef.current.select();
    }

    render () {

        const isFilledForm = hasPrintableChars(this.state.user) && hasPrintableChars(this.state.password);

        return  (
            <div ref={LockScreen.ref} className={styles.lockScreen}>
                <div className={styles.form}>
                        <div className={styles.description}>{i18n.t('login>sessionExpired')}</div>
                        <Input
                            className={styles.input}
                            type={'text'}
                            onChange={this.onUserChange.bind(this)}
                            onPressEnter={this.onLoginButtonClick.bind(this)}
                            value={this.state.user}
                            prefix={<Icon name={'user'}/>}
                            autoComplete={'off'}
                            tabIndex={1}
                            onBlur={this.onLoginBlur}
                            onFocus={this.onLoginFocus}
                            forwardRef={LockScreen.loginRef}
                        />
                        <Input.Password
                            className={styles.input}
                            type={'password'}
                            onChange={this.onPasswordChange.bind(this)}
                            onPressEnter={this.onLoginButtonClick.bind(this)}
                            value={this.state.password}
                            prefix={<Icon name={'key'}/>}
                            tabIndex={2}
                            autoComplete={'off'}
                            ref={LockScreen.passwordRef}
                            onBlur={this.onPasswordBlur}
                            onFocus={this.onPasswordFocus}
                        />
                        <Button
                            isDisabled={!isFilledForm}
                            intent={INTENT.PRIMARY}
                            isFilled={true}
                            onClick={this.onLoginButtonClick.bind(this)}>
                            {i18n.t('login>loginButton')}
                        </Button>
                    </div>
            </div>
        );
    }
}
