/**
 * Error details window
 *
 * @author Stebunov Vladimir
 * @date 2020-03-19
 */

import * as React from 'react';
import {i18n, ServerUrlManager} from '@unidata/core-app';
import {Modal, SIZE} from '@unidata/uikit';
import * as styles from './errordetailmodal.m.scss';

interface IProps {
    isOpenModal: boolean;
    onClose: () => void;
    userMessageDetails?: string;
    stackTrace?: string;
    errorUrl?: string;
    errorCode?: string | number;
    userMessage?: string;
}

export class ErrorDetailModal extends React.Component<IProps, {}> {

    renderUserMessageDetails () {
        return (
            <div>
                {this.props.userMessageDetails}
            </div>
        );
    }

    renderDefault () {
        const {
            errorCode,
            errorUrl,
            userMessage,
            stackTrace
        } = this.props;

        return (
            <>
                <div>
                    {i18n.t('util>supportMessage')}
                </div>
                {errorCode ?
                    <div>
                        {i18n.t('util>errorCode')} {errorCode}
                    </div> : null
                }
                {userMessage ?
                    <div>
                        {i18n.t('util>message')} {userMessage}
                    </div> : null
                }
                {errorUrl ?
                    <div>
                        API URL: {errorUrl.replace(ServerUrlManager.getBaseUrl(), '')}
                    </div> : null
                }
                {stackTrace ?
                    <div className={styles.errorDetailText}>
                        Stack trace: {stackTrace}
                    </div> : null
                }
            </>
        );
    }

    render () {
        const {isOpenModal,
            onClose
        } = this.props;

        return (
            <Modal
                data-qaid={'errorDetailModal'}
                isOpen={isOpenModal}
                onClose={onClose}
                header={i18n.t('util>errorDetails')}
                size={SIZE.SMALL}
            >
                {this.props.userMessageDetails ? this.renderUserMessageDetails() : this.renderDefault() }
            </Modal>
        );
    }
}
