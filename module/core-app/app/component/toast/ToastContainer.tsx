/**
 * Container for displaying notifications and error details
 *
 * @author Stebunov Vladimir
 * @date 2020-03-19
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import * as styles from './toastcontainer.m.scss';
import {Toast, Overlay} from '@unidata/uikit';
import {ErrorDetailModal} from './ErrorDetailModal';
import {Dialog, ToastMessage, ErrorDetail, ToastStore} from '@unidata/core-app';

interface IState {
    isOpenModal: boolean;
    stackTrace: string | undefined;
    errorUrl: string | undefined;
    errorCode: string | number | undefined;
    userMessage: string | undefined;
    userMessageDetails: string | undefined;
}

@observer
export class ToastContainer extends React.Component<{}, IState> {

    private store: ToastStore = Dialog.getStore();

    state: IState = {
        isOpenModal: false,
        stackTrace: undefined,
        errorUrl: undefined,
        errorCode: undefined,
        userMessage: undefined,
        userMessageDetails: undefined
    };

    onNotyClose = (message: ToastMessage) => (e: any) => {
        this.store.remove(message);
    }

    showModal = (details: ErrorDetail) => (e: any) => {
        this.setState({
            isOpenModal: true,
            stackTrace: details.stackTrace,
            errorUrl: details.errorUrl,
            userMessage: details.userMessage,
            userMessageDetails: details.userMessageDetails
        });
    }

    hideModal () {
        this.setState({
            isOpenModal: false
        });
    }

    render () {

        return (
            <>
                <Overlay isOpen={this.store.list.length > 0}>
                    <div className={styles.container}>
                        {this.store.list.map((toast: ToastMessage) => {
                            return (
                                <Toast
                                    key={toast.id}
                                    isOpen={true}
                                    onClose={this.onNotyClose(toast)}
                                    showModal={toast.detail && this.showModal(toast.detail)}
                                    message={toast}
                                    isDissapeared={this.store.isDisappeared(toast)}
                                />
                            );
                        })}
                    </div>
                </Overlay>
                <ErrorDetailModal
                    isOpenModal={this.state.isOpenModal}
                    onClose={this.hideModal.bind(this)}
                    userMessageDetails={this.state.userMessageDetails}
                    stackTrace={this.state.stackTrace}
                    errorUrl={this.state.errorUrl}
                    errorCode={this.state.errorCode}
                    userMessage={this.state.userMessage}
                />
            </>
        );
    }
}
