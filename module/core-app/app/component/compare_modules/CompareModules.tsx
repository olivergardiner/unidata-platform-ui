import * as React from 'react';
import {Button, Icon, INTENT, Modal, SIZE} from '@unidata/uikit';
import {DownloadFileService, i18n, ModuleService} from '@unidata/core-app';

import * as styles from './compareModules.m.scss';
import {IBackendModule} from '@unidata/types';
import {IconIntent} from '@unidata/icon';

interface IState {
    isOpen: boolean;
}

export class CompareModules extends React.PureComponent<{}, IState> {

    state = {
        isOpen: false
    };

    // modules from webpack is array (see webpack/env/prod.env.js)
    feModules: string[] = (process.env.MODULES as unknown as string[]) || [];

    // webpack transfer process.env.BE_MODULES to array of strings (see webpack/env/prod.env.js)
    generatedBeModules: string[] = (process.env.BE_MODULES as unknown as string[]) || [];
    receivedBeModules: string[] = [];

    lessBe: string[] = [];

    componentDidMount (): void {
        ModuleService.getModuleList()
            .then((result: IBackendModule[]) => {
                this.receivedBeModules = result.map((item) => item.id);

                this.lessBe = this.calcModulesDiff(this.generatedBeModules, this.receivedBeModules);

                if (this.lessBe.length > 0) {
                    this.setState({isOpen: true});
                }
            });
    }

    /**
     * A method that compares the generated modules (necessary for the front to work) and the actual ones obtained
     *
     * Go through the generated module and if we don't find the current item in the list that we received from the back,
     * write to the list of modules that we need from the backup to work with.
     *
     * * If the condition les Modules.length === 0 is correct at the end of the loop, we assume that the module check has passed
     */
    calcModulesDiff = (generatedModules: string[], receivedModules: string[]) => {
        const lessModules: string[] = [];

        for (let i = 0, length = generatedModules.length; i < length; i++) {
            if (!receivedModules.includes(generatedModules[i])) {
                lessModules.push(generatedModules[i]);
            }
        }

        return lessModules;
    };

    downloadLog = () => {
        const data = JSON.stringify({
            feModules: this.feModules,
            lessBe: this.lessBe,
            generatedBeModules: this.generatedBeModules.sort(),
            receivedBeModules: this.receivedBeModules.sort()
        });

        DownloadFileService.byJSONData(data, 'modules_log.json');
    };

    render () {
        return (
            <Modal
                isOpen={this.state.isOpen}
                shouldCloseOnClickOutside={false}
                hasCloseIcon={false}
                header={i18n.t('verifyModules>attention')}
                size={SIZE.MIDDLE}
                footer={(
                    <div className={styles.buttonContainer}>

                        {/**
                         Temporary button so that you can skip the module mismatch
                         Delete after stabilization 6.0
                         */}
                        <Button
                            isGhost={true}
                            onClick={() => this.setState({isOpen: false})}
                        >
                            {i18n.t('common:skip')}
                        </Button>

                        <Button
                            intent={INTENT.PRIMARY}
                            onClick={this.downloadLog}
                        >
                            {i18n.t('verifyModules>downloadLog')}
                        </Button>
                    </div>
                )}
            >
                <div className={styles.container}>
                    <Icon name={'warning'} intent={IconIntent.DANGER}/>
                    <div dangerouslySetInnerHTML={{__html: i18n.t('verifyModules>text')}}/>
                </div>
            </Modal>
        );
    }
}
