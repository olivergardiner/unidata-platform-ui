/**
 *  Screen lock (by mistake from the server)
 *
 * @author Vladimir Stebunov
 * @date 2020-04-24
 */
import * as React from 'react';
import {i18n} from '@unidata/core-app';
import {Modal} from '@unidata/uikit';
import {Spin} from 'antd';
import * as styles from './MaintenanceMask.m.scss';

interface IProps {
    isOpen: boolean;
}

export class MaintenanceMask extends React.Component<IProps> {

    render () {
        return (
            <Modal isOpen={this.props.isOpen} hasCloseIcon={false} >
                <div className={styles.spinBlock}>
                    <Spin/>
                </div>
                <div>
                    {i18n.t('application>maintenanceMode')}
                </div>
            </Modal>
        );
    }
}
