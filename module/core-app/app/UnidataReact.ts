/**
 * Глобальное пространство имен для react приложения
 *
 * @author Ivan Marshalkin
 * @date 2019-08-26
 */

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {AppSession, AppTypeManager, AuthTokenManager, Dialog, i18next, ServerUrlManager} from '@unidata/core-app';

import {Field, Modal, Confirm, UdMarkdownEditorWrapper} from '@unidata/uikit';
import {ModuleManager} from '@unidata/core';

export let UnidataReact: any = {
    React: React,       // НЕ УДАЛЯТЬ! Используется при реализации внешних модулей
    ReactDOM: ReactDOM, // НЕ УДАЛЯТЬ! Используется при реализации внешних модулей
    i18n: i18next, // НЕ УДАЛЯТЬ! Используется при интеграции с ExtJS
    app: null, // НЕ УДАЛЯТЬ! Задается в index.ts
    SystemJs: window.System,
    service: {
    },
    manager: {
        AuthTokenManager: AuthTokenManager,
        ServerUrlManager: ServerUrlManager,
        ModuleManager: ModuleManager,
        Session: AppSession,
        Dialog: Dialog,
        AppTypeManager: AppTypeManager
    },
    security: {
    },
    // Доступ к компонентам для внешних модулей
    components: {
        Confirm: Confirm,
        Modal: Modal,
        FieldInput: Field.Input,
        FieldCheckbox: Field.Checkbox,
        FieldSwitch: Field.Switcher,
        FieldSelect: Field.Select,
        UdMarkdownEditorWrapper: UdMarkdownEditorWrapper
    },
    constants: {
        DateTimeFormat: 'YYYY-MM-DDTHH:mm:ss.SSSZ'
    }
};
