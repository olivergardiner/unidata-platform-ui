/**
 * HOC for pages
 * adds a job with the wiki
 * after switching from Sencha, replace
 *
 * @author: Vladimir Stebunov
 * @date: 2020-02-26
 */
import React, {RefObject, ComponentType, Component} from 'react';
import {PageManager} from '@unidata/core-app';

export const withWiki = <T extends Component, OriginalProps extends {}>(
    WrappedComponent: ComponentType<OriginalProps>
) => {

    type Props = OriginalProps;

    class WithWiki extends Component<Props> {

        componentInstance: RefObject<T> = React.createRef();

        constructor (props: Props) {
            super(props);
        }

        componentDidMount () {
            PageManager.setCurrentPage(this.componentInstance.current);
        }

        componentWillUnmount () {
            PageManager.setCurrentPage(null);
        }

        render () {
            return <WrappedComponent ref={this.componentInstance} {...this.props} />;
        }
    }

    const RefForwardingFactory = (props: Props) => (
        <WithWiki {...props as any} />
    );

    return RefForwardingFactory;
};

