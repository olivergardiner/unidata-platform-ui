/**
 * Notification type
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-04
 */

export enum NotificationModelType {
    TEXT_REPORT = 'Text report',
    META_FULL_EXPORT = 'META_FULL_EXPORT',
    STATISTIC_FULL_EXPORT = 'STATISTIC_FULL_EXPORT',
    CONFIG_EXPORT = 'CONFIG_EXPORT',
    AUDIT_EXPORT = 'AUDIT_EXPORT',
    XLSX_EXPORT = 'XLSX_EXPORT',
}
