/**
 * Notification
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-02
 */

import {NotificationModelType} from './NotificationModelType';
import {AbstractModel, dateField, DateField, EnumField, StringField, stringField} from '@unidata/core';

export class BackendNotificationModel extends AbstractModel {

    @stringField({primaryKey: true})
    public id: StringField;

    @dateField()
    public createDate: DateField;

    @stringField()
    public createdBy: StringField;

    @stringField({allowNull: true})
    public binaryDataId: StringField;

    @stringField({allowNull: true})
    public characterDataId: StringField;

    @stringField()
    public type: EnumField<NotificationModelType>;

    @stringField()
    public content: StringField;

    @stringField()
    public details: StringField;

}
