﻿/**
 * Independent react app
 *
 * @author Ivan Marshalkin
 * @date 2018-09-11
 */

import * as React from 'react';
import {render} from 'react-dom';
import {Provider, Observer} from 'mobx-react';
import {Route, Router} from 'react-router';
import {RouterStore, syncHistoryWithStore} from 'mobx-react-router';
import {createHashHistory} from 'history';
import * as jQuery from 'jquery';
import {App, AuthenticateDataType, MenuManager, PageManager, ServerUrlManager, UnidataConfig} from '@unidata/core-app';
import {MainComponent} from './component/MainComponent';
import {Overlays} from '@unidata/uikit';

// Fonts
import './scss/etc/fonts/menu-font.scss';
import './scss/etc/fonts/open-sans.scss';
import './scss/etc/fonts/unidata-old.scss';
// Icon font
import '../../../react-resources/fonts/font-awesome-4.5.0/css/font-awesome.css';

import DevTools from 'mobx-react-devtools';
import {LockScreen} from './component/lockscreen/LockScreen';
import {SetPassword} from './component/changepassword/setpassword/SetPassword';
import {ueModuleManager, UeModuleTypeCallBack, UeModuleTypeComponent} from '@unidata/core';
import {ToastContainer} from './component/toast/ToastContainer';
import {CompareModules} from './component/compare_modules/CompareModules';
import {MaintenanceMask} from './component/maintenancemask/MaintenanceMask';

let showMobxDevTool: boolean = false;

export class ReactApp {
    private bodyCls: string = 'ud-body';

    /**
     * Initializing the app (before calling run)
     */
    init (): Promise<any> {
        ServerUrlManager.setBaseUrl(UnidataConfig.getServerUrl());

        return this.initAppData();
    }

    /**
     * Инициализация данных приложения
     */
    private initAppData () {
        let modules = ueModuleManager.getModulesByType(UeModuleTypeCallBack.APP_DATA_LOADER),
            promises = [];

        for (let module of modules) {
            promises.push(module.default.fn());
        }

        return Promise.all(promises);
    }

    run () {
        let self = this;
        let authenticateData = (window as any).authenticateData as AuthenticateDataType;

        App.authenticate(authenticateData);
        // в customerCfg.LOCALE лежит уже подмененная локаль, подумать как сделать толковее

        // Инициализируем меню
        MenuManager.init();

        window.addEventListener('sessionexpired', this.onSessionExpired);

        window.addEventListener('beforeunload', (event): string | void => {
            let confirmMessage: string;

            const page = PageManager.getCurrentPage();

            if (page && page.onBeforeUnload) {
                confirmMessage = page.onBeforeUnload();

                if (confirmMessage) {
                    // Cancel the event as stated by the standard.
                    event.preventDefault();
                    // Chrome requires returnValue to be set.
                    event.returnValue = confirmMessage;

                    return confirmMessage;
                }
            }
        });

        self.render();

        // временно отключено
        // const socket = new SocketManager(AuthTokenManager.getToken());
    }

    private render () {
        const isApplicationEnabled = !App.isForceResetPassword();

        // проставляем базовый класс body (убираем крутилку загрузки приложения)
        jQuery('body').addClass(this.bodyCls);

        const browserHistory = createHashHistory();
        const routingStore = new RouterStore();

        const providedData = {
            routing: routingStore
        };

        const history = syncHistoryWithStore(browserHistory, routingStore);

        render(
            <Overlays.Provider>
                <LockScreen />
                <div className='ud-admin'>
                    {showMobxDevTool && <DevTools/>}
                    <Provider {...providedData}>
                        <Router history={history}>
                            {isApplicationEnabled ? <Route component={MainComponent}/> : <SetPassword/>}
                        </Router>
                        <ToastContainer />
                        {this.renderHotKey()}
                    </Provider>
                </div>
                <CompareModules/>
                <Observer>
                    {() => <MaintenanceMask isOpen={App.isOnMaintenance()}/>}
                </Observer>
            </Overlays.Provider>,
            document.getElementById('root')
        );
    }

    private renderHotKey () {
        let modules = ueModuleManager.getModulesByType(UeModuleTypeComponent.GLOBAL_HOTKEY, {active: true}),
            hotkeyComponents = [];

        for (let module of modules) {
            hotkeyComponents.push(<module.default.component key={module.default.moduleId}/>);
        }

        return hotkeyComponents;
    }

    private onSessionExpired () {
        LockScreen.lock();
    }
}
