/**
 * Notification service from BE
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-02
 */

import {BackendNotificationModel} from '../model/notification/BackendNotificationModel';
import {ReadBackendNotificationsOp} from './op/benotifications/ReadBackendNotificationsOp';
import {ReadBackendNotificationsCountOp} from './op/benotifications/ReadBackendNotificationsCountOp';
import {DeleteBackendNotificationsOp} from './op/benotifications/DeleteBackendNotificationsOp';

export class BackendNotificationService {

    public static readNotifications (): Promise<BackendNotificationModel[]> {
        let op = new ReadBackendNotificationsOp();

        return op.execute();
    }

    public static removeNotifications (): Promise<any> {
        let op = new DeleteBackendNotificationsOp();

        return op.execute();
    }

    public static removeNotification (notificationId: string): Promise<any> {
        let op = new DeleteBackendNotificationsOp(notificationId);

        return op.execute();
    }

    public static readNotificationsCount (): Promise<number> {
        let op = new ReadBackendNotificationsCountOp();

        return op.execute();
    }

}
