/**
 * Operation for receiving notifications
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-05
 */

import {IModelOpConfig} from '@unidata/core';
import {BackendNotificationModel} from '../../../model/notification/BackendNotificationModel';
import {AppModelListHttpOp} from '@unidata/core-app';

export class DeleteBackendNotificationsOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/user/notifications/{{notificationId}}',
        method: 'delete',
        model: BackendNotificationModel
    }

    constructor (notificationId?: string) {
        super();

        this.urlContext = {
            notificationId: notificationId
        };
    }
}
