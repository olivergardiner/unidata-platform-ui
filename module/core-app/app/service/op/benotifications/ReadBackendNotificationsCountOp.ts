/**
 * Operation for getting the number of notifications
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-02
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ReadBackendNotificationsCountOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/user/notifications/count',
        method: 'get',
        headers: {
            PROLONG_TTL: 'false'
        }
    }
}
