/**
 * Operation for receiving notifications
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-02
 */

import {IModelOpConfig} from '@unidata/core';
import {BackendNotificationModel} from '../../../model/notification/BackendNotificationModel';
import {AppModelListHttpOp} from '@unidata/core-app';

export class ReadBackendNotificationsOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/user/notifications',
        method: 'get',
        model: BackendNotificationModel
    }
}
