// systemjs 6.x
declare module 'systemjs/dist/system.js' {
    let noTypeInfoYet: any;
    export = noTypeInfoYet;
}

declare module 'systemjs/dist/s.js' {
    let noTypeInfoYet: any;
    export = noTypeInfoYet;
}

// systemjs 0.20.x
declare module 'systemjs/dist/system-production' {
    let noTypeInfoYet: any;
    export = noTypeInfoYet;
}

declare module 'systemjs/dist/system' {
    let noTypeInfoYet: any;
    export = noTypeInfoYet;
}
