/**
 * Вспомогательный класс для работы с роутингом в приложении
 *
 * @author Ivan Marshalkin
 * @date 2020-02-20
 */

import {ueModuleManager, UeModuleReactComponent, UeModuleTypeComponent} from '@unidata/core';
import {Redirect, Route} from 'react-router-dom';
import * as React from 'react';
import {ReactComponentElement, ReactElement, ReactNode} from 'react';
import {App, AppTypeManager, UdLogger} from '@unidata/core-app';
import {withWiki} from './layout/with_wiki/WithWiki';

export class AppRoute {
    private static pageRoutes: any = null;

    static getPageRoutes () {
        if (!this.pageRoutes) {
            this.pageRoutes = this.getModuleRoutes();
        }

        return this.pageRoutes;
    }

    private static getModuleRoutes (): ReactElement[] {
        let usedPaths: string[] = [],
            routes: ReactElement[] = [],
            modules = ueModuleManager.getModulesByType(UeModuleTypeComponent.PAGE) as
                Array<UeModuleReactComponent<UeModuleTypeComponent.PAGE>>;

        if (!modules) {
            return routes;
        }

        modules.forEach(function (module) {
            const moduleRoutePath = module.default.meta.route;

            if (usedPaths.indexOf(moduleRoutePath) === -1) {
                if (module.default.resolver()) {
                    routes.push(
                        <Route
                            key={moduleRoutePath}
                            exact
                            path={moduleRoutePath}
                            component={withWiki(module.default.component)}
                        />
                    );
                }

                usedPaths.push(moduleRoutePath);
            } else {
                UdLogger.error(`Duplicate page route path = ${moduleRoutePath}`);
            }
        });

        routes.push(AppRoute.defaultPage());

        return routes;
    }

    // todo Brauer Ilya добавить возможность получать дефолтную страницу из профиля(настроек?) пользователя
    private static defaultPage () {
        return (
            <Redirect key={'redirect_to_main_page'} to={'/'}/>
        );
    }
}
