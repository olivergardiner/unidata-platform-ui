import * as jQuery from 'jquery';

import '../../../react-resources/unidatabootstrap/images/logoplatform-en.png';
import '../../../react-resources/unidatabootstrap/images/logoplatform-en.svg';
import '../../../react-resources/unidatabootstrap/images/logoplatform-ru.png';
import '../../../react-resources/unidatabootstrap/images/logoplatform-ru.svg';
import '../../../react-resources/unidatabootstrap/images/logoplatform.png';
import '../../../react-resources/favicon.ico';

import '../../../react-resources/unidatabootstrap/css/animate.css';

import '../../../react-resources/fonts/linearicons/style.css';

import './unidatabootstrap/scss/Micrologin.scss';
import './unidatabootstrap/scss/IziToasCustomization.scss';
import './unidatabootstrap/scss/iziToast.scss';
import './unidatabootstrap/scss/LocalizationMenu.scss';

import './unidatabootstrap/iziToast';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import {Microloader} from './unidatabootstrap/Microloader';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import {Micrologin} from './unidatabootstrap/Micrologin';

(window as any).jQuery = jQuery;
(window as any).Unidata = (window as any).Unidata || {};
(window as any).Unidata.Micrologin = Micrologin;

// Customization customerUrl
// eslint-disable-next-line no-useless-escape
let match = window.location.search.match(/(\?|&)customerurl\=([^&]*)/);

if (match && match[2] && !(window as any).customerUrl.length) {
    (window as any).customerUrl = decodeURIComponent(match[2]);
}

// Download and start the app
Microloader.chain(
    Microloader.loadCustomerJson,
    Microloader.initUnidataCustomerCustomization,
    Micrologin.loadMicrologinCustomization,
    Micrologin.runMicrologin
)();
