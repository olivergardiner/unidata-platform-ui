﻿/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2018-09-11
 */

import './scss/main.scss';

import i18next from 'i18next';
import {
    AjaxRequestInterceptor,
    AppModule,
    AppTypeManager,
    changeLanguage,
    i18nCfg,
    UnidataConfig,
    loadCommonLocalization,
    loadModuleLocalization
} from '@unidata/core-app';
import {configure, runInAction} from 'mobx';
import {UnidataReact} from './UnidataReact';
import {ModuleManager, ueModuleManager} from '@unidata/core';
import '@unidata/uikit/index.css';
import {ReactApp} from './ReactApp';

type UnidataReactType = typeof UnidataReact;

declare global {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    interface Window {
        Ext: any;
        Unidata: any;
        customerConfig: any;
        mobxGlobals: any;
        UnidataReact: UnidataReactType;
    }
}

let appModule = new AppModule();
let app: ReactApp = new ReactApp();
let i18nPromise: any;

// Список модулей для вставки. Заполняется автоматически из папки module в лоадере для webpack в ud-cli
const modules: any = {};

/**
 * 0.
 *
 * Загружаем бизнес-модули
 */
let modulePromise: any = Promise.all(Object.keys(modules).map((moduleName) => {
    return modules[moduleName as keyof typeof modules]()
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        .then((module: any) => {
            appModule.addModule(moduleName, module);
            ModuleManager.addModule(module.init());

            return loadModuleLocalization(moduleName.trim(), UnidataConfig.getDefaultLocale());
        });
}));

/**
 * 1.
 *
 * Настраиваем mobx и другие необходимые части программы
 */

if (!window.customerConfig) {
    throw new Error('No customer config');
}

UnidataConfig.setCustomerCfg(window.customerConfig);

// Todo придумать куда перенести конфигурацию mobx
configure({enforceActions: 'observed'});

// Выносим глобально функции mobx, необходимые для debug
window.mobxGlobals = {
    runInAction: runInAction
};

// глобальное пространство имен для react приложения
window.UnidataReact = UnidataReact;

// перехватчик запросов отправляемых через axios
AjaxRequestInterceptor.initInterceptors();

// Определяем тип для приложения
AppTypeManager.setAppType(UnidataConfig.getAppType());

/**
 * 2.
 *
 * инициализируем i18n и загружаем общие локали
 */

i18nPromise = i18next.init(i18nCfg);
i18nPromise
    .then(function () {
        return loadCommonLocalization(UnidataConfig.getDefaultLocale());
    })
    .catch(function () {
        console.error('Error when loading i18n resources');
    });


/**
 * 3.
 *
 * Загружаем UI UE
 */

let uiuePromise = ueModuleManager.importModules(UnidataConfig.getPlatformExternalModules());

/**
 * 4.
 *
 * Приложение готово к работе
 */
i18nPromise
    .then(modulePromise)
    .then(uiuePromise)
    .then(function () {
        // change language последним, так как в предыдущих промисах тоже может быть загрузка локалей
        return changeLanguage(UnidataConfig.getDefaultLocale());
    })
    .then(function () {
        appModule.inited = true;
        UnidataReact.app = app;

        // временное решение для управлением порядком загрузки частей приложения
        // функционал необходимо удалить при реализации UN-12218
        const ev = new CustomEvent('unidata-react-initialized', {
            detail: null
        });

        app.init().then(() => {
            window.dispatchEvent(ev);
        });
    })
    .catch(function (err: Error) {
        console.error('Error on application startup', err);
    });
