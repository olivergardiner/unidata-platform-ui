/**
 * Type of model: the registry or the directory
 *
 * @author Ivan Marshalkin
 * @date 2018-09-06
 */

export enum EntityType {
    Entity = 'Entity',
    LookupEntity = 'LookupEntity'
}
