/**
 * The direction of the links
 *
 * @author Sergey Shishigin
 * @date 2019-12-10
 */

export enum RelationDirectionType {
    from = 'from',  // incoming link to this registry / directory or entry
    to = 'to'       // outgoing link for this registry / directory or entry
}
