/**
 * empty comment line Viktor Magarlamov
 *
 * @author: Viktor Magarlamov
 * @date: 2019-03-21
 */

export enum Pages {
    Editor = 'editor',
    Settings = 'settings',
    Manager = 'manager'
}
