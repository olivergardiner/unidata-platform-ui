/**
 * Acceptable types of code attributes
 *
 * @author Ilya Brauer
 * @date 2019-11-06
 */

export enum CODE_DATA_TYPE {
    STRING = 'String',
    INTEGER = 'Integer'
}
