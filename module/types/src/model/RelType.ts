/**
 * Relation types
 *
 * @author Sergey Shishigin
 * @date 2019-12-12
 */

export enum RelType {
    ManyToMany ='ManyToMany',
    Contains = 'Contains',
    References = 'References',
    Incoming = 'Incoming'
}
