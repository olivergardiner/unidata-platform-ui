/**
 * Categories of attribute types
 *
 * @author Ivan Marshalkin
 * @date 2019-07-03
 */

export enum AttributeTypeCategory {
    simpleDataType = 'simpleDataType',
    enumDataType = 'enumDataType',
    lookupEntityType = 'lookupEntityType',
    linkDataType = 'linkDataType',
    arrayDataType = 'arrayDataType',
    dictionaryDataType = 'dictionaryDataType'
}
