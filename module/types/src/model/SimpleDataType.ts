/**
 * Acceptable types of simple attributes
 *
 * @author Ivan Marshalkin
 * @date 2019-10-22
 */

export enum SIMPLE_DATA_TYPE {
    STRING = 'String',
    INTEGER = 'Integer',
    NUMBER = 'Number',
    BOOLEAN = 'Boolean',
    DATE = 'Date',
    TIMESTAMP = 'Timestamp',
    TIME = 'Time',
    BLOB = 'Blob',
    CLOB = 'Clob',
    LOOKUP = 'Lookup'
}
