/**
 * Acceptable types of array attributes
 *
 * @author Ivan Marshalkin
 * @date 2019-10-22
 */

export enum ARRAY_DATA_TYPE {
    STRING = 'String',
    INTEGER = 'Integer',
    NUMBER = 'Number',
    DATE = 'Date',
    TIMESTAMP = 'Timestamp',
    TIME = 'Time',
    LOOKUP = 'Lookup'
}
