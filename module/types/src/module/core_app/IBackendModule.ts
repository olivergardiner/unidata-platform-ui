/**
 * Description of the Backend module
 *
 * @author: Denis Makarov
 * @date: 2020-04-05
 */

export interface IBackendModule {
    id: string;
    description: string;
    version: string;
}

