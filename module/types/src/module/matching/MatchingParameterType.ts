/**
 * Types of matching parameter
 *
 * @author Sergey Shishigin
 * @date 2019-07-18
 */
export enum MatchingParameterType {
    ENTITY_ATTRIBUTE = 'ENTITY_ATTRIBUTE',
    CLASSIFIER = 'CLASSIFIER',
    CLASSIFIER_NODE = 'CLASSIFIER_NODE',
    CLASSIFIER_ATTRIBUTE = 'CLASSIFIER_ATTRIBUTE',
    RELATION = 'RELATION',
    RELATION_ATTRIBUTE = 'RELATION_ATTRIBUTE',
    CONSTANT = 'CONSTANT',
}


