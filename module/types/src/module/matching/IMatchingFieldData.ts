/**
 * Match field data
 *
 * @author Denis Makarov
 * @date 2020-03-23
 */

export interface IMatchingFieldData {
    mappingType: string;
    fieldPath: string;
}
