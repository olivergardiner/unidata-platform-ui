/**
 * Set of parent store functions for MatchingMappingTypeStore
 *
 * @author Denis Makarov
 * @date 2020-03-23
 */

import {IMatchingFieldData} from './IMatchingFieldData';

export interface IMatchingMappingStoreAccessor {
    getMatchingMappingsData: () => IMatchingFieldData [];
    createNewMatchingMapping: (type: string) => void;
    removeMatchingMappingForType: (type: string) => void;
}

