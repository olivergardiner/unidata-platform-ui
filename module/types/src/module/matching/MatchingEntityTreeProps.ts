/**
 * Props for the matching parameter selection tree
 *
 * @author Sergey Shishigin
 * @date 2019-10-15
 */
import {ITreeNode} from '../../tree/TreeNodeType';

export type MatchingEntityTreeProps = {
    treeTableStore: any;
    onBeforeSelect?: (nodes: ITreeNode []) => boolean;
    onSelect?: (nodes: ITreeNode []) => {};
}
