/**
 * Matching-parameter object
 *
 * @author Sergey Shishigin
 * @date 2019-10-15
 */
export interface IMatchingParameterItem {
    type: string;
    path: string;
    displayName: string [];
}
