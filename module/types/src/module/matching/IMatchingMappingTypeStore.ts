/**
 * Store for a specific type of matching parameter,
 * stores data for selecting matching fields (tree data)
 *
 * @author Denis Makarov
 * @date 2020-03-23
 */

import {IMatchingParameterItem} from './IMatchingParameterItem';
import {ITreeNode} from '../../tree/TreeNodeType';

export interface IMatchingMappingTypeStore {
    initStore: () => Promise<any>;
    getTreeStore: (type: string) => any;
    canCreateMatchingMapping: boolean;
    canRemoveMatchingMapping: (type: string, index: number, allMappingsTypes: string[]) => boolean;
    createDefaultMatchingMappings: (existingMappings: Map<string, number>) => void;
    handleResetMatchingFieldValue: (type: string) => void;
    handleAddNewMatchingMapping: () => void;
    handleAddFieldMapping: (mappingType: string, fieldValue: string) => void;
    isMatchingValueHighlighted: (node: ITreeNode, matchingParameterType: string, matchingParameters: IMatchingParameterItem []) => boolean;
}
