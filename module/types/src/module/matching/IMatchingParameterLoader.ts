/**
 * Data loader for matching parameters
 *
 * @author Denis Makarov
 * @date 2020-03-30
 */

import {IEntity, IMatchingParameterItem, ITreeNode} from '../../';

export interface IMatchingParameterLoader {
    transformNodes: (nodes: ITreeNode []) => IMatchingParameterItem [];
    loadMatchingParameters: (entity: IEntity) => Promise<any>;
}
