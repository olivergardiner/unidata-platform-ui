import {ICatalogEntity} from './';
import {ITreeNode} from './../../tree/TreeNodeType';

export interface IEntityListStore {
    selectEntity: (node: ITreeNode) => void;
    loadEntityTree: () => Promise<any>;
    getSelectedEntity: () => ICatalogEntity | null;
    getDisplayPath: () => string [];
    getTreeTableStore: () => any;
}
