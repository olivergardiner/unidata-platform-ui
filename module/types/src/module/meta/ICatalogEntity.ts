/**
 * The Catalog Entity type for use outside the meta module
 *
 * @author Denis Makarov
 * @date 2020-03-23
 */

export interface ICatalogEntity {
    name: {
        getValue: () => string;
    };

    displayName: {
        getValue: () => string;
    };
}
