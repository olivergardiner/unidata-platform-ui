/**
 *  Interface for adding business process settings
 *
 * @author Sergey Shishigin
 * @date 2020-03-11
 */
import {ObservableMap} from 'mobx';
import {WorkflowTypeCode} from './WorkflowTypeCode';

export interface IWorkflowAddProps {
    handleItemAdd: (name: string, defaultAssignmentType?: string, defaultAssignmentProcessType?: WorkflowTypeCode) => void;
    assignmentsHashByEntityName: ObservableMap;
    readOnly?: boolean;
    'data-qaid'?: string;
    assignableNames?: string[];
}
