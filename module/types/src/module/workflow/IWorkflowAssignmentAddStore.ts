/**
 * The Store interface to select the meta object for the configuration of business processes
 *
 * @author Sergey Shishigin
 * @date 2020-03-10
 */
import {WorkflowTypeCategory} from './WorkflowTypeCategory';

export interface IWorkflowAssignmentAddStore {
    loadAll: () => Promise<any>;
    initHashes: (entityHash: {[name: string]: any}) => {[name: string]: any};
    filterMetaObject: (name: string) => boolean;
    filterProcessTypes: (category: WorkflowTypeCategory) => boolean;
    filterTriggerTypes: (code: string) => boolean;
    hasWorkflow: (processesHashByCode: {[name: string]: any[]}) => boolean;
}
