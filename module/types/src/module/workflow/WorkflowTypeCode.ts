/**
 * Workflow type codes
 *
 * @author Sergey Shishigin
 * @date 2019-07-18
 */
export enum WorkflowTypeCode {
    RECORD_EDIT = 'RECORD_EDIT',
    RECORD_DELETE = 'RECORD_DELETE',
    CLASSIFIER_EDIT = 'CLASSIFIER_EDIT'
}
