/**
 * Workflow type categories
 *
 * @author Sergey Shishigin
 * @date 2019-07-18
 */

export enum WorkflowTypeCategory {
    RECORD = 'RECORD',
    CLASSIFIER = 'CLASSIFIER'
}
