/**
 * Listing the date format
 *
 * @author Stebunov Vladimir
 * @date 2019-10-28
 */
export const enum DateGranularityMode {
    DATE = 'DATE',
    DATETIME = 'DATETIME',
    DATETIMEMILLIS = 'DATETIMEMILLIS'
}

