﻿/**
 * Security label editing page
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-15
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {Layout} from '@unidata/uikit';
import {SecurityLabelList} from './list/SecurityLabelList';
import {SecurityLabelsEditor} from './editor/SecurityLabelsEditor';
import {SecurityLabelStore} from './store';
import {i18n, Res, ResourceManager} from '@unidata/core-app';

interface IProps {}

@observer
export class SecurityLabelPage extends React.Component<IProps> {
    PAGE_CLASS: string = 'ud-page ud-page-with-sider ud-page-security-labels';

    store: SecurityLabelStore = new SecurityLabelStore();
    providerProps: any;

    constructor (props: IProps) {
        super(props);

        // the same props must be passed to the provider, otherwise mobs throws an exception
        // @see https://stackoverflow.com/questions/43550137/mobx-rerender-after-assign/43562824
        this.providerProps = this.store.props;
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>securitylabel');
    }

    render () {
        let readOnly: boolean = ResourceManager.isReadOnly([Res.ADMIN_SYSTEM_MANAGEMENT, Res.SECURITY_LABELS_MANAGEMENT]);

        return (
            <Provider {...this.providerProps}>
                <Layout className={this.PAGE_CLASS}>
                    <SecurityLabelList
                        readOnly={readOnly}
                        store={this.store}
                    />

                    <Layout.Content>
                        <SecurityLabelsEditor readOnly={readOnly}/>
                    </Layout.Content>
                </Layout>
            </Provider>
        );
    }
}
