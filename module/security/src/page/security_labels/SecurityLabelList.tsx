/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {i18n} from '@unidata/core-app';
import {CardPanel, EmptyText, Col, Row} from '@unidata/uikit';

interface IProps {
    securityLabels: any[];
}

export class SecurityLabelList extends React.Component<IProps> {

    render () {
        const {securityLabels} = this.props;

        return (
            <React.Fragment>
                {securityLabels.length > 0 && (
                    securityLabels.map((label: any) => {
                        return (
                            <CardPanel
                                key={`${label.modelId}-${label.name.getValue()}`}
                                title={label.displayName.getValue()}
                                internal
                            >
                                {label.attributes.items.map((item: any) =>
                                    <Row
                                        key={`${label.modelId}-${item.name.getValue()}`}
                                        style={{
                                            height: 32,
                                            margin: '18px 0px 0px 0px',
                                            fontFamily: '"Open Sans", serif',
                                            fontSize: 13
                                        }}
                                    >
                                        <Col span={8}>
                                            {item.name.getValue()}
                                        </Col>

                                        <Col span={14} style={{borderBottom: '1px solid #d7d7d7'}}>
                                            {item.value.getValue() || i18n.t('admin.labels>allowedAll')}
                                        </Col>
                                    </Row>
                                )}
                            </CardPanel>
                        );
                    })
                )}
                {securityLabels.length === 0 && (
                    <EmptyText text={i18n.t('admin.role>securityLabelsEmptyText')}/>
                )}
            </React.Fragment>
        );
    }
}
