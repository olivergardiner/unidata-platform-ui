/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-15
 */

import * as React from 'react';
import {action} from 'mobx';
import {observer} from 'mobx-react';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {SecurityLabel} from '../../../model/user/SecurityLabel';
import {Button, INTENT, SIZE, ButtonWithConfirmation, MenuSider, Confirm} from '@unidata/uikit';
import {SecurityLabelStore} from '../store';

interface IProps {
    readOnly?: boolean;
    store: SecurityLabelStore;
}

interface IState {
    isConfirmOpen: boolean;
    targetSecurityLabel: SecurityLabel;
}

const EmptySecurityLabel = new SecurityLabel({});

@observer
export class SecurityLabelList extends React.Component<IProps, IState> {
    selectedLabel: SecurityLabel | null = null;

    state = {
        isConfirmOpen: false,
        targetSecurityLabel: EmptySecurityLabel
    }

    get readOnly () {
        return this.props.readOnly;
    }

    get formattedDataForMenu () {
        return this.props.store.menuStore.items.map((item: SecurityLabel) => {
            return {
                displayName: item.displayName.getValue(),
                name: item.name.getValue(),
                phantom: item.phantom,
                model: item,
                key: item.modelId
            };
        });
    }

    get actions () {
        const canCreateLabel = (!this.readOnly &&
            ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.SECURITY_LABELS_MANAGEMENT], Right.CREATE));

        if (!canCreateLabel) {
            return null;
        }

        return this.props.store.isNeedConfirmToSwitchToNew() ?
            (<ButtonWithConfirmation
                isRound={true}
                leftIcon={'plus'}
                intent={INTENT.SECONDARY}
                key='button-add'
                name='createLabel'
                size={SIZE.LARGE}
                onConfirm={this.props.store.handleAddNewItemClick}
                title={i18n.t('admin.labels>addButtonText')}
                confirmationMessage={i18n.t('unsavedChanges')}
                data-qaid='addLabelButton'
            />) :
            (<Button
                isRound={true}
                leftIcon={'plus'}
                intent={INTENT.SECONDARY}
                key='button-add'
                name='createLabel'
                size={SIZE.LARGE}
                onClick={this.props.store.handleAddNewItemClick}
                title={i18n.t('admin.labels>addButtonText')}
                data-qaid='addLabelButton'
            />);
    }

    get menuColumns () {
        return [
            {
                accessor: 'displayName',
                id: 'displayName',
                minWidth: 90,
                Header: i18n.t('admin.labels>labelName')
            }
        ];
    }

    @action
    handleListClick = () => {
        this.setState({isConfirmOpen: false});
        this.selectedLabel = this.state.targetSecurityLabel;
        this.props.store.handleListClick(this.state.targetSecurityLabel);
    }

    onSwitchClose = () => {
        this.setState({isConfirmOpen: false});
    }

    checkSwitch = (target: SecurityLabel) => {
        if (this.props.store.isNeedConfirmToSwitch(target)) {
            this.setState({isConfirmOpen: true, targetSecurityLabel: target});

            return;
        }

        this.setState({targetSecurityLabel: target}, this.handleListClick);
    }

    render () {
        return (
            <React.Fragment>
                <MenuSider
                    title={i18n.t('admin.labels>labelListTitle')}
                    data={this.formattedDataForMenu}
                    columns={this.menuColumns}
                    hideHeader={true}
                    currentItem={this.props.store.menuStore.selectedLabel}
                    onClick={this.checkSwitch}
                    actions={this.actions}
                    data-qaid='securityLabelMenuSider'
                />
                <Confirm
                    onConfirm={this.handleListClick}
                    confirmationMessage={i18n.t('unsavedChanges')}
                    isOpen={this.state.isConfirmOpen}
                    onClose={this.onSwitchClose}
                    data-qaid='addLabelConfirmWindow'
                />
            </React.Fragment>
        );
    }
}
