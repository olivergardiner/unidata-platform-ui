/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {action} from 'mobx';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {getErrorMessage} from '@unidata/core-app';
import {Button, Field, IOption, Col, Row} from '@unidata/uikit';
import {SecurityLabelAttributeRole} from '../../../model/user/SecurityLabelAttributeRole';

interface IProps {
    item: any;
    entityName: string;
    onDestroy: (item: any) => void;
    metaRecordOptions: IOption[];
    readOnly?: boolean;
}

@observer
export class AttributeRow extends React.Component<IProps> {

    @action
    handleOnNameChange = (name: string, value: string) => {
        this.props.item.name.setValue(value);
    }

    @action
    handleOnAttributeSelect = (name: string, value: string) => {
        this.props.item.path.setValue(`${this.props.entityName}.${value}`);
    }

    handleOnAttributeDestroy = () => {
        this.props.onDestroy(this.props.item);
    }

    render () {
        const {item, metaRecordOptions, readOnly} = this.props;

        const getValue = (attr: SecurityLabelAttributeRole) => {
            return attr.path.getValue() ? attr.path.getValue().split('.')[1] : undefined;
        };

        return (
            <Row className='attribute-row' data-qaid={getValue(item) + 'Row'}>
                <Col
                    className='name-column'
                    span={11}
                >
                    <Field.Input
                        name='name'
                        placeholder={i18n.t('admin.labels>namePlaceholder')}
                        label={i18n.t('admin.labels>name')}
                        labelWidth={20}
                        inputWidth={80}
                        disabled={readOnly}
                        defaultValue={item.name.getValue()}
                        onChange={this.handleOnNameChange}
                        error={getErrorMessage(item, 'name')}
                        data-qaid='nameField'
                    />
                </Col>
                <Col
                    className='action-column'
                    span={1}
                >
                </Col>
                <Col
                    className='attribute-column'
                    span={11}
                >
                    <Field.Select
                        name='item'
                        placeholder={i18n.t('admin.labels>itemPlaceholder')}
                        label={i18n.t('admin.labels>attribute')}
                        labelWidth={20}
                        inputWidth={80}
                        disabled={readOnly}
                        options={metaRecordOptions}
                        value={getValue(item)}
                        onSelect={this.handleOnAttributeSelect}
                        error={getErrorMessage(item, 'path')}
                        data-qaid='itemField'
                    />
                </Col>
                <Col
                    className='action-column-delete'
                    span={1}
                >
                    {!readOnly && (
                        <Button
                            leftIcon={'delete'}
                            name='removeAttribute'
                            isMinimal={true}
                            onClick={this.handleOnAttributeDestroy}
                            data-qaid='destroyButton'
                        />
                    )}
                </Col>
            </Row>
        );
    }
}
