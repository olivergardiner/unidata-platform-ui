/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-07-09
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {Button, CardPanel, Editor, INTENT, PLACEMENT, ButtonWithConfirmation} from '@unidata/uikit';
import {SecurityLabelForm} from './SecurityLabelForm';
import {AttributesForm} from './AttributesForm';
import {SecurityLabelsEditorStore} from '../store/editorStore';

interface IProps {
    readOnly?: boolean;
}

interface IInjectProps extends IProps {
    editorStore: any;
    handleItemDestroy: () => void;
}

@inject('editorStore', 'handleItemDestroy')
@observer
export class SecurityLabelsEditor extends React.Component<IProps> {

    get injected () {
        return this.props as IInjectProps;
    }

    get store (): SecurityLabelsEditorStore {
        return this.injected.editorStore;
    }

    get readOnly () {
        return this.props.readOnly;
    }

    get canEdit () {
        if (this.readOnly) {
            return false;
        }

        if (this.store.currentLabel && this.store.currentLabel.phantom) {
            return ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.SECURITY_LABELS_MANAGEMENT], Right.CREATE);
        }

        return ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.SECURITY_LABELS_MANAGEMENT], Right.UPDATE);
    }

    get extraButtons () {
        const canDeleteLabel = (!this.props.readOnly &&
            ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.SECURITY_LABELS_MANAGEMENT], Right.DELETE));

        return (
            <React.Fragment>
                {this.canEdit && (
                    <>
                        <Button
                            leftIcon={'save'}
                            name='saveSecurityLabel'
                            isRound={true}
                            intent={INTENT.PRIMARY}
                            isDisabled={!this.store.canSave}
                            onClick={this.store.handleSave}
                            data-qaid='saveButton'
                        >
                            {i18n.t('admin.labels>saveButtonText')}
                        </Button>
                        &nbsp;
                        &nbsp;
                    </>
                )}
                {canDeleteLabel && (
                    <ButtonWithConfirmation
                        name='removeSecurityLabel'
                        isDisabled={!this.store.canDestroy}
                        onConfirm={this.injected.handleItemDestroy}
                        title={i18n.t('admin.labels>removeButtonText')}
                        tooltipPlacement={PLACEMENT.BOTTOM_END}
                        leftIcon={'trash2'}
                        isRound={true}
                        confirmationMessage={i18n.t('admin.labels>destroyConfirmation', {name: this.store.currentLabel?.name.getValue()})}
                        data-qaid='destroyButton'
                    />
                )}
            </React.Fragment>
        );
    }

    render () {
        const readOnly = !this.canEdit;

        if (!this.store.currentLabel) { return null; }

        return (
            <Editor
                title={i18n.t('admin.labels>editorTitle')}
                groupSectionTitle={i18n.t('page.header>administration')}
                extraButtons={this.extraButtons}
                itemTitle={this.store.currentLabel.displayName.getValue()}
                iconType='flag'
            >
                <CardPanel title={i18n.t('admin.labels>securityLabelPanelTitle')}>
                    <SecurityLabelForm
                        item={this.store.currentLabel}
                        onChange={this.store.handleLabelChange}
                        readOnly={readOnly}
                        onTreeChange={this.store.handleTreeChange}
                        entityName={this.store?.selectedEntityName || undefined}
                    />

                    <AttributesForm
                        onCreate={this.store.handleAttributeAdd}
                        onDestroy={this.store.handleAttributeDestroy}
                        readOnly={readOnly}
                        attributes={this.store?.attributes?.items || []}
                        entityName={this.store?.selectedEntityName || ''}
                        metaRecordOptions={this.store.formattedMetaRecordOptions}
                    />
                </CardPanel>
            </Editor>
        );
    }
}
