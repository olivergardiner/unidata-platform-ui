/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {Button, CardPanel, INTENT, IOption} from '@unidata/uikit';
import {AttributeRow} from './AttributeRow';
import {i18n} from '@unidata/core-app';

interface IProps {
    attributes: any[];
    metaRecordOptions: IOption[];
    entityName: string;
    onCreate: () => void;
    onDestroy: (attribute: any) => void;
    readOnly?: boolean;
}

interface IState {
    visibleRows: number;
    total: number;
}

@observer
export class AttributesForm extends React.Component<IProps, IState> {
    timer: number | null;

    state = {
       visibleRows: 100,
       total: 0
    };

    componentDidMount () {
        this.setState({total: this.props.attributes.length});
    }

    componentDidUpdate (prevProps: IProps) {
        if (this.state.total !== this.props.attributes.length) {
            this.setState({total: this.props.attributes.length});
            this.updateVisibleRow();
        }
    }

    get extraButtons () {
        if (this.props.readOnly) {
            return undefined;
        }

        return (
            <Button
                intent={INTENT.PRIMARY}
                isMinimal={true}
                leftIcon={'plus-circle'}
                onClick={this.handleOnClick}
                name='addSecurityLabel'
                data-qaid='addSecurityAttributeButton'
            >
                {i18n.t('admin.labels>addSecurityLabel')}
            </Button>
        );
    }

    updateVisibleRow = () => {
        this.timer = window.setTimeout(() => {
            if (this.state.visibleRows < this.state.total) {
                this.setState((prevState) => ({
                    visibleRows: prevState.visibleRows + 100
                }));
            }

            this.updateVisibleRow();
        }, 100);
    }

    handleOnClick = () => {
        this.props.onCreate();
    }

    render () {
        const {metaRecordOptions, entityName, attributes, readOnly} = this.props,
            {visibleRows} = this.state;

        return (
            <CardPanel
                internal
                title={i18n.t('admin.labels>attributeSecurityLabelPanelTitle')}
                extraButtons={this.extraButtons}
                data-qaid='attributeSecurityLabelPanel'
            >

                <div style={{paddingLeft: '100px'}}>
                    {attributes.slice(0, visibleRows).map((item) =>
                        <AttributeRow
                            key={item.id.getValue()}
                            item={item}
                            readOnly={readOnly}
                            onDestroy={this.props.onDestroy}
                            metaRecordOptions={metaRecordOptions}
                            entityName={entityName} />
                    )}
                </div>
            </CardPanel>
        );
    }
}
