/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {Field} from '@unidata/uikit';

import {getErrorMessage} from '@unidata/core-app';
import {EntitySelect, EntityTreeType} from '@unidata/meta';

interface IProps {
    item: any;
    onChange: (name: string, value: string) => void;
    entityName?: string;
    onTreeChange: (item: EntityTreeType) => void;
    readOnly?: boolean;
}

@observer
export class SecurityLabelForm extends React.Component<IProps> {

    render () {
        const {item, onChange, onTreeChange, entityName, readOnly} = this.props;

        return (
            <React.Fragment>
                <Field.Input
                    name='name'
                    label={i18n.t('admin.labels>name')}
                    disabled={readOnly || !item.phantom}
                    defaultValue={item.name.getValue()}
                    onChange={onChange}
                    error={getErrorMessage(item, 'name')}
                    data-qaid='nameInput'
                />

                <Field.Input
                    name='displayName'
                    label={i18n.t('admin.labels>displayName')}
                    disabled={readOnly}
                    defaultValue={item.displayName.getValue()}
                    onChange={onChange}
                    error={getErrorMessage(item, 'displayName')}
                    data-qaid='displayNameInput'
                />

                <Field.Textarea
                    name='description'
                    label={i18n.t('admin.labels>description')}
                    disabled={readOnly}
                    defaultValue={item.description.getValue()}
                    onChange={onChange}
                    error={getErrorMessage(item, 'description')}
                    data-qaid='descriptionTextarea'
                />

                <Field
                    label={i18n.t('admin.labels>entity')}
                    error={getErrorMessage(item, 'entityName')}
                    data-qaid='entityField'
                >
                    <EntitySelect
                        disabled={readOnly}
                        placeholder={''}
                        onEntitySelect={onTreeChange}
                        value={entityName}
                    />
                </Field>

            </React.Fragment>
        );
    }
}
