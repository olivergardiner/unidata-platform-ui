/**
 * Arguments for the remove label event
 *
 * @author Denis Makarov
 * @date 2019-02-05
 */
export class RemoveCurrentLabelArgs<T> {
    private readonly internalValue: T;

    constructor (value: T) {
        this.internalValue = value;
    }

    public get value (): T {
        return this.internalValue;
    }
}
