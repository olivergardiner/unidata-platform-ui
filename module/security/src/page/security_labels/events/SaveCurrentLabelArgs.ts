/**
 * Arguments for the save label event
 *
 * @author Denis Makarov
 * @date 2019-02-05
 */
export class SaveCurrentLabelArgs<T> {
    private readonly internalValue: T;

    constructor (value: T) {
        this.internalValue = value;
    }

    public get value (): T {
        return this.internalValue;
    }
}
