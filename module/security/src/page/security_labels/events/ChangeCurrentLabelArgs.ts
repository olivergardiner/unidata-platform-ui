/**
 * Arguments for the change label event
 *
 * @author Denis Makarov
 * @date 2019-02-05
 */
export class ChangeCurrentLabelArgs<T> {
    private readonly internalValue: T;

    constructor (value: T) {
        this.internalValue = value;
    }

    public get value (): T {
        return this.internalValue;
    }
}
