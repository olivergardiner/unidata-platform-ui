/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import {observe, action} from 'mobx';
import {SecurityLabelsListStore} from './menuStore';
import {SecurityLabelsEditorStore} from './editorStore';
import {MenuStoreType} from '../../../types/SecurityLabelType';
import {SecurityLabel} from '../../../model/user/SecurityLabel';

export class SecurityLabelStore {
    public menuStore: MenuStoreType = new SecurityLabelsListStore();
    public editorStore: any = new SecurityLabelsEditorStore();

    constructor () {
        let me = this;

        this.editorStore.eventCurrentLabelSave.subscribe(this.handleOnCurrentLabelChanged, this);
        this.editorStore.eventCurrentLabelRemove.subscribe(this.handleOnCurrentLabelChanged, this);

        const menuDisposer = observe(this.menuStore, 'selectedLabel', (change) => {
            if (change.newValue) {
                me.editorStore.setCurrentLabelLoading(true);

                if (change.newValue.phantom) {
                    me.editorStore.createNewLabel();
                } else {
                    me.editorStore.loadLabel(change.newValue.name.getValue());
                }
            }

            return change;
        });

        const editorDisposer = observe(this.editorStore, 'currentLabel', (change) => {
            if (change.newValue && me.menuStore.selectedLabel) {
                for (let property of ['displayName']) {
                    change.newValue[property].bind(me.menuStore.selectedLabel[property]);
                }
            }

            return change;
        });
    }

    public get props () {
        return {
            menuStore: this.menuStore,
            editorStore: this.editorStore,
            handleListClick: this.handleListClick,
            handleAddNewItemClick: this.handleAddNewItemClick,
            handleItemDestroy: this.handleItemDestroy,
            isNeedConfirmToSwitchToNew: this.isNeedConfirmToSwitchToNew
        };
    }

    public handleOnCurrentLabelChanged = () => {
        this.menuStore.loadLabelList();
    }

    @action
    public handleListClick = (label: SecurityLabel) => {
        return this.menuStore.setSelectedLabel(label);
    }

    @action
    public handleAddNewItemClick = () => {
        return this.menuStore.createLabel();
    }

    public isNeedConfirmToSwitchToNew = (): boolean => {
        return this.editorStore.canSave;
    }

    @action
    public handleItemDestroy = () => {
        return this.editorStore.handleDestroy().then(() => {
            this.menuStore.setSelectedLabel(null);
            this.menuStore.loadLabelList();
        });
    }

    public isNeedConfirmToSwitch = (target: SecurityLabel): boolean => {
        return this.editorStore.canSave &&
            this.menuStore.selectedLabel.modelId !== target.modelId;
    }
}
