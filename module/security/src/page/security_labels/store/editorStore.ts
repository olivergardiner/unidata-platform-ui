/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import {action, observable} from 'mobx';
import {SecurityLabel} from '../../../model/user/SecurityLabel';
import {
    AbstractAttribute,
    Catalog,
    CatalogEntity,
    CatalogLookupEntity,
    CodeAttribute,
    Entity, EntityStoreManager,
    EntityTreeType,
    LookupEntity,
    MetaRecordService
} from '@unidata/meta';
import {EventChannel, NegativeSequence, ReactiveProp} from '@unidata/core';
import {SecurityLabelAttributeRole} from '../../../model/user/SecurityLabelAttributeRole';
import {RemoveCurrentLabelArgs} from '../events/RemoveCurrentLabelArgs';
import {SaveCurrentLabelArgs} from '../events/SaveCurrentLabelArgs';
import {AttributeTypeCategory, EntityType} from '@unidata/types';
import {Dialog, i18n} from '@unidata/core-app';
import {MetaAttributeUtil} from '../../../utils/MetaAttributeUtil';
import {SecurityLabelService} from '../../../service/SecurityLabelService';
import {IOption} from '@unidata/uikit';

export class SecurityLabelsEditorStore {

    @observable public currentLabel: SecurityLabel | null;
    @observable public selectedEntityMetaRecordAttributes: IOption[] = [];
    @observable public currentLabelIsLoading: boolean = false;
    @observable public isSaving: boolean = false;

    @observable
    public selectedEntityName: string | null;
    public selectedEntityMetaRecord: Entity | LookupEntity | null;

    public eventCurrentLabelSave = new EventChannel<SecurityLabelsEditorStore, SaveCurrentLabelArgs<SecurityLabel>>();
    public eventCurrentLabelRemove = new EventChannel<SecurityLabelsEditorStore, RemoveCurrentLabelArgs<string>>();

    constructor () {
    }

    private updateSelectedEntityName () {
        if (this.currentLabel && this.currentLabel.attributes) {
            let firstAttribute = this.currentLabel.attributes.items.length > 0 && this.currentLabel.attributes.items[0] || null;

            if (firstAttribute && firstAttribute.path) {
                this.selectedEntityName = firstAttribute.path.getValue() && firstAttribute.path.getValue().split('.')[0] || null;

                if (this.selectedEntityName) {
                    EntityStoreManager.getCatalogList(false, true)
                        .then(function () {
                            this.loadMetaRecord(
                                EntityStoreManager.findEntity(this.selectedEntityName, false)
                            );
                        }.bind(this));
                }
            }
        } else {
            this.selectedEntityName = null;
        }
    }

    public get attributes () {
        if (this.currentLabel) {
            return this.currentLabel.attributes;
        }

        return null;
    }

    public loadMetaRecord (record: CatalogEntity | CatalogLookupEntity | null) {
        let me = this,
            entityType,
            promise;

        if (!record) {
            me.selectedEntityMetaRecord = null;

            return;
        }

        entityType = record instanceof CatalogLookupEntity ? EntityType.LookupEntity : EntityType.Entity;

        promise = MetaRecordService.getMetaRecord(entityType, record.name.getValue());
        promise.then(
            action((result: Entity | LookupEntity) => {
                me.selectedEntityMetaRecord = result;
                me.updateSelectedEntityMetaRecordAttributes();
            })
        );
    }

    public updateSelectedEntityMetaRecordAttributes () {
        let attributes: AbstractAttribute[] = [];

        const filterSimpleAttributesByDataType = ((item: any) => {
            let typeCategory = MetaAttributeUtil.getTypeCategory(item);

            if (typeCategory === AttributeTypeCategory.simpleDataType) {
                return ['Integer', 'String'].includes(item.simpleDataType.getValue());
            } else if (typeCategory === AttributeTypeCategory.lookupEntityType) {
                return true;
            }

            return false;
        });

        if (!this.selectedEntityMetaRecord) {
            this.selectedEntityMetaRecordAttributes = [];

            return;
        }

        attributes = [...this.selectedEntityMetaRecord.simpleAttributes.items.slice().filter(filterSimpleAttributesByDataType)];

        if (this.selectedEntityMetaRecord instanceof LookupEntity) {
            attributes = [
                ...attributes.slice(),
                (this.selectedEntityMetaRecord.codeAttribute as CodeAttribute),
                ...this.selectedEntityMetaRecord.aliasCodeAttributes.items.slice()
            ];
        }

        this.selectedEntityMetaRecordAttributes = attributes.map((attribute: AbstractAttribute) => {
            return {
                key: attribute.name.getValue(),
                title: attribute.displayName.getValue(),
                value: attribute.name.getValue()
            };
        });
    }

    /* API */

    public setCurrentLabel (label: SecurityLabel | null) {
        this.currentLabel = label;
        this.selectedEntityName = null;
        this.updateSelectedEntityName();

        if (this.currentLabel) {
            this.currentLabel.setReactiveCascade([ReactiveProp.DIRTY, ReactiveProp.LAST_VALIDATION_RESULT]);
        }

        this.setCurrentLabelLoading(false);
    }

    public get formattedMetaRecordOptions () {
       return this.selectedEntityMetaRecordAttributes;
    }

    public get canSave () {
        if (this.isSaving) {
            return false;
        }

        return (this.currentLabel && this.currentLabel.getDirty());
    }

    public get canDestroy () {
        if (this.isSaving) {
            return false;
        }

        return (this.currentLabel !== undefined);
    }

    @action
    public setSavingFalse () {
        this.isSaving = false;
    }

    @action
    public handleLabelChange = (name: string, value: string) => {
        if (this.currentLabel) {
            (this.currentLabel as any)[name].setValue(value);
        }
    }

    @action
    public handleTreeChange = (entity: EntityTreeType) => {
        if (entity instanceof Catalog) {
            return;
        }

        this.selectedEntityName = entity.name.getValue();
        this.loadMetaRecord(entity);

        if (this.currentLabel) {
            this.currentLabel.attributes.removeAll();
            this.handleAttributeAdd();
        }
    }

    @action
    public handleAttributeAdd = (count?: number) => {
        if (this.currentLabel) {
            if (count) {
                for (let i = 0; i < count; ++i) {
                    this.currentLabel.attributes.add(new SecurityLabelAttributeRole({id: NegativeSequence.getId()}));
                }
            } else {
                let attribute = new SecurityLabelAttributeRole({id: NegativeSequence.getId()});

                this.currentLabel.attributes.add(attribute);
            }
        }
    }

    @action
    public handleAttributeDestroy = (attribute: SecurityLabelAttributeRole) => {
        if (this.currentLabel && attribute) {
            this.currentLabel.attributes.remove(attribute);
        }
    }

    @action
    public setCurrentLabelLoading (flag: boolean) {
        this.currentLabelIsLoading = flag;
    }

    @action
    public createNewLabel () {
        this.setCurrentLabel(new SecurityLabel({}));
    }

    @action
    public loadLabel (name: string) {
        return new Promise((resolve: any) => {
            let promise = SecurityLabelService.getSecurityLabel(name),
                me = this;

            me.setCurrentLabelLoading(true);

            promise.then(action(function (label: SecurityLabel) {
                me.setCurrentLabel(label);
                resolve();
            }));
        });
    }

    @action
    public handleSave = () => {
        let me = this;
        let apiMethod: any;
        let params: any[];

        this.setSaving(true);

        this.currentLabel!.validate();

        if (this.currentLabel!.attributes.size() === 0) {
            this.currentLabel!.lastValidationResult.set('entityName', ['validation>required>attributes']);
        }

        if (this.currentLabel!.lastValidationResult.size > 0) {
            this.showError();

            return;
        }

        apiMethod = this.currentLabel!.getPhantom() ? SecurityLabelService.createSecurityLabel : SecurityLabelService.updateSecurityLabel;

        params = this.currentLabel!.getPhantom() ?
            [this.currentLabel!.serialize()] :
            [this.currentLabel!.name.getValue(), this.currentLabel!.serialize()];

        return apiMethod(...params).then(
            function () {
                me.currentLabel!.commit();
                me.eventCurrentLabelSave.getDispatcher().dispatch(this, new SaveCurrentLabelArgs<SecurityLabel>(me.currentLabel!));
                me.handleRequestSuccess();
            },
            this.showError
        );
    }

    @action
    public handleDestroy = () => {
        let me = this;

        this.setSaving(true);

        let labelName = this.currentLabel!.name.getValue();

        const promise = SecurityLabelService.destroySecurityLabel(labelName).then(
            action(() => {
                me.eventCurrentLabelRemove.getDispatcher().dispatch(this, new RemoveCurrentLabelArgs<string>(labelName));
                me.setCurrentLabel(null);
                me.handleRequestSuccess();
            }),
            this.showError
        );

        return promise;
    }

    @action
    private handleRequestSuccess = () => {
        this.setSaving(false);

        Dialog.showMessage('', i18n.t('admin.labels>saveSuccessText'));

        return Promise.resolve();
    }

    @action
    private showError = (er?: Error) => {
        let message: string;

        this.setSaving(false);

        if (er instanceof Array) {
            message = er.map((error) => error.userMessageDetails || error.userMessage).join(' ');
        } else {
            message = i18n.t('admin.labels>saveFailureText');
        }

        Dialog.showError(message, i18n.t('admin.labels>saveFailureTitle'));
    }

    @action
    public setSaving = (isSaving: boolean) => {
        this.isSaving = isSaving;
    }
}
