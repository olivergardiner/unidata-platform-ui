/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import {action, observable} from 'mobx';
import {SecurityLabel} from '../../../model/user/SecurityLabel';
import {ModelCollection, Nullable} from '@unidata/core';
import {SecurityLabelService} from '../../../service/SecurityLabelService';

export class SecurityLabelsListStore {
    @observable
    public labelList: ModelCollection<SecurityLabel> = ModelCollection.create(SecurityLabel);

    @observable
    public selectedLabel: Nullable<SecurityLabel>;

    constructor () {
        this.loadLabelList();
    }

    public get items () {
        return this.labelList.items || [];
    }

    @action
    public createLabel = () => {
        let label: (SecurityLabel | undefined) = this.labelList.items.find((label) => label.phantom);

        if (!label) {
            label = new SecurityLabel({});
            this.labelList.add(label);
        }

        this.setSelectedLabel(label);
    }

    @action
    public setSelectedLabel (label: SecurityLabel) {
        this.selectedLabel = label;
    }

    @action
    public loadLabelList () {
        let self = this;
        let currentLabelName = self.selectedLabel ? self.selectedLabel.name.getValue() : null;

        self.labelList.removeAll();

        return SecurityLabelService.getSecurityLabels().then(
            action((result: SecurityLabel[]) => {
                self.labelList.replaceAll(result);

                if (result && currentLabelName) {
                    const label: any[] = result.filter((item) => item.name.getValue() === currentLabelName);

                    if (label.length > 0) {
                        self.setSelectedLabel(label[0]);
                    }
                }
            }),
            function (result) {
                throw new Error('Error during fetching security labels');
            });
    }
}
