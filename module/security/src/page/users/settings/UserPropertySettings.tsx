/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {inject, observer, Observer} from 'mobx-react';
import {Button, CardPanel, Checkbox, Input, INTENT, PageHeader, Table} from '@unidata/uikit';
import {UserProperty} from '../../../model/user/UserProperty';
import {SettingsStore} from '../store/settingsStore';

import * as styles from './userPropertySettings.m.scss';

interface IProps {
    readOnly: boolean;
}

interface IInjectProps extends IProps {
    settingsStore: SettingsStore;
    handleSaveSettings: any;
}

@inject('settingsStore', 'handleSaveSettings')
@observer
export class UserPropertySettings extends React.Component<IProps> {
    columns = [
        {
            accessor: 'required',
            Header: () => {
                return (
                    //we use <Observer> because after updating React => 16.9, an error occurs when trying to pass observer( ) for Header or Cell
                    <Observer render={() => (
                        i18n.t('admin.user.settings>requiredColumnText')
                    )}/>
                );
            },
            Cell: ({original}: { original: UserProperty }) => {
                return (
                    <Observer render={() => (
                        <Checkbox
                            name='required'
                            disabled={this.readOnly || this.store.isItemRemoved(original) || original.readOnly.getValue()}
                            checked={original.required.getValue()}
                            onChange={({target}: { target: any }) => {
                                this.store.handleItemChange(original, 'required', target.checked);
                            }}
                        />
                    )}/>
                );
            }
        },
        {
            accessor: 'readOnly',
            Header: () => {
                return (
                    // we use <Observer> because after updating React => 16.9, an error occurs when trying to pass observer( ) for Header or Cell
                    <Observer render={() => (
                        i18n.t('admin.user.settings>readOnlyColumnText')
                    )}/>
                );
            },
            Cell: ({original}: { original: UserProperty }) => {
                return (
                    <Observer render={() => (
                        <Checkbox
                            name='readOnly'
                            disabled={this.readOnly || this.store.isItemRemoved(original) || original.required.getValue()}
                            checked={original.readOnly.getValue()}
                            onChange={({target}: { target: any }) => {
                                this.store.handleItemChange(original, 'readOnly', target.checked);
                            }}
                        />
                    )}/>
                );
            }
        },
        {
            accessor: 'name',
            Header: i18n.t('admin.user.settings>nameColumnText'),
            Cell: ({original}: { original: UserProperty }) => {
                return original.name.getValue();
            }
        },
        {
            accessor: 'displayName',
            Header: i18n.t('admin.user.settings>displayNameColumnText'),
            Cell: ({original}: { original: UserProperty }) => {
                return original.displayName.getValue();
            }
        }
    ] as any[];

    renderInput = (key: 'name' | 'displayName') => ({original, onEdit}: {original: UserProperty; onEdit: (value: string) => void}) => {
        return (
            <Input
                name={key}
                defaultValue={original[key].getValue()}
                autoFocus={true}
                onBlur={(e) => {
                    const val = e.target.value;

                    onEdit(val);
                }}
                disabled={this.readOnly || this.store.isItemRemoved(original)}
            />
        );
    };

    editComponents = {
        displayName: this.renderInput('displayName'),
        name: this.renderInput('name')
    };

    onDeleteRow = (rowIndex: number) => {
        const original = this.store.actualPropertyList[rowIndex];

        this.store.handleRemoveItem(original);

    };

    get injected () {
        return this.props as IInjectProps;
    }

    get store () {
        return this.injected.settingsStore;
    }

    get items () {
        return this.store.userProperties;
    }

    get readOnly () {
        let resources = [Res.ADMIN_SYSTEM_MANAGEMENT, Res.USER_MANAGEMENT];

        return this.props.readOnly ? true : !ResourceManager.userHasAnyResourceRight(resources, Right.UPDATE);
    }

    get extraButtons () {
        if (this.readOnly) {
            return undefined;
        }

        return (
            <Button
                intent={INTENT.PRIMARY}
                leftIcon={'save'}
                isDisabled={!this.store.canSave}
                onClick={this.injected.handleSaveSettings}
                isRound={true}
            >
                {i18n.t('admin.user.settings>saveButtonText')}
            </Button>
        );
    }

    onCellEdit = (rowIndex: number, cellId: string, value: string) => {
        const original = this.store.actualPropertyList[rowIndex];

        this.store.handleItemChange(original, cellId, value);
    };

    render () {
        return (
            <>
                <PageHeader
                    sectionTitle={i18n.t('admin.user.settings>editorTitle')}
                    groupSectionTitle={i18n.t('page.header>administration')}
                    itemTitle={i18n.t('admin.user.settings>editorItemTitle')}
                    iconType='user'
                    extraButtons={this.extraButtons}
                />
                <div className={styles.cardContainer}>
                    <CardPanel
                        title={i18n.t('admin.user.settings>settingsPanelTitle')}
                    >
                        <div className={styles.tableContainer}>
                            <Table
                                hasHeaderBg={false}
                                columns={this.columns}
                                data={this.store.actualPropertyList.slice()}
                                editComponents={this.editComponents}
                                isRowsDeletable={!this.readOnly}
                                onRowDelete={this.onDeleteRow}
                                onAllDelete={this.store.handleRemoveAll}
                                isEditable={true}
                                viewRowsCount={this.store.actualPropertyList.slice().length}
                                onCellEdit={this.onCellEdit}
                            />
                        </div>

                        <div style={{
                            textAlign: 'center',
                            paddingTop: 10,
                            paddingBottom: 10
                        }}>
                            {!this.readOnly && (
                                <Button
                                    onClick={this.store.handleAddItem}
                                    intent={INTENT.PRIMARY}
                                    isMinimal={true}
                                    leftIcon={'plus-circle'}
                                >
                                    {i18n.t('admin.role.settings>addSettings')}
                                </Button>
                            )}
                        </div>
                    </CardPanel>
                </div>
            </>
        );
    }
}
