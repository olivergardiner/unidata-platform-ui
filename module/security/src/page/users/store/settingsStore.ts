/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import {action, computed, observable} from 'mobx';
import {IServerError, ModelCollection, OperationError, ReactiveProp} from '@unidata/core';
import {UserProperty} from '../../../model/user/UserProperty';
import {Dialog} from '@unidata/core-app';
import {i18n} from '@unidata/core-app';
import {UserService} from '../../../service/UserService';

export class SettingsStore {
    @observable public isSaving: boolean = false;
    public userProperties: ModelCollection<UserProperty> = ModelCollection.create(UserProperty);
    @observable private deletedIds: number[] = [];

    constructor () {
        this.userProperties.setReactive([ReactiveProp.DIRTY]);
        this.loadUserProperties();
    }

    private loadUserProperties () {
        UserService.getUserProperties().then(action((response: any[]) => {
            this.userProperties.replaceAll(response);
        }));
    }

    @action
    public handleAddItem = (): void => {
        this.userProperties.add(new UserProperty({}));
    }

    @action
    public handleRemoveItem = (item: UserProperty) => {
        const index: number = this.deletedIds.indexOf(item.modelId);

        if (index > -1) {
            this.deletedIds = this.deletedIds.filter((id) => id !== item.modelId);
        } else {
           this.deletedIds.push(item.modelId);
        }
    }

    @action
    public handleRemoveAll = () => {
        if (this.isAllRemoved()) {
            this.deletedIds = [];
        } else {
            for (let item of this.userProperties.items) {
                if (!this.isItemRemoved(item)) {
                    this.deletedIds.push(item.modelId);
                }
            }
        }
    }

    @action
    public handleItemChange = (model: UserProperty, name: string, value: any) => {
        (model as any)[name].setValue(value);
    }

    @computed
    public get canSave (): boolean {
        const isHaveDeleted: boolean = this.deletedIds.length > 0 && Boolean(this.deletedIds),
            userPropertyIsDirty = this.userProperties.getDirty();

        if (this.isSaving) {
            return false;
        }

        return isHaveDeleted || userPropertyIsDirty;
    }

    public handleSave = (): Promise<void> => {
        this.setSaving(true);

        const promisses = this.userProperties.items.filter((item) => {
            return item.getDirty() || this.isItemRemoved(item);
        }).map((item) => {
            if (this.isItemRemoved(item)) {
                if (item.getPhantom()) {
                    return null;
                }

                return UserService.deleteUserProperty(item.id.getValue());
            }

            if (item.getPhantom()) {
                return UserService.createUserProperty(item.serialize());
            }

            return UserService.updateUserProperty(item.id.getValue(), item.serialize());
        });

        return Promise.all(promisses).then(
            this.handleSettingsRequestSuccess,
            this.handleSettingsRequestFailed
        );
    }

    public isItemRemoved = (item: UserProperty): boolean => {
        return this.deletedIds.slice().includes(item.modelId);
    }

    public isAllRemoved = (): boolean => {
        return (this.userProperties.items.length === this.deletedIds.length);
    }

    @action
    private handleSettingsRequestSuccess = () => {
        this.deletedIds = [];
        this.loadUserProperties();
        this.setSaving(false);

        Dialog.showMessage('', i18n.t('admin.user.settings>saveSuccessText'));

        return Promise.resolve();
    }

    private handleSettingsRequestFailed = (er?: any) => {
        let message: string;

        this.setSaving(false);

        if (er instanceof OperationError) {
            const errors: IServerError[] = er.getServerError();

            message = errors.map((error) => error.userMessageDetails || error.userMessage).join(' ');
        } else {
            message = i18n.t('admin.user.settings>saveFailureText');
        }

        Dialog.showError(message, i18n.t('admin.user>userSaveFailureTitle'));
    }

    @action
    private setSaving = (isSaving: boolean) => {
        this.isSaving = isSaving;
    }

    public get actualPropertyList () {
        const me = this;

        return this.userProperties.items.filter(function (item) {
            return me.isItemRemoved(item) === false;
        });
    }
}
