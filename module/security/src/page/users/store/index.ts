﻿/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import {action, observable, observe} from 'mobx';
import {ListStore} from './list';
import {EditorStore} from './editor';
import {SettingsStore} from './settingsStore';
import {Pages} from '@unidata/types';
import {User} from '../../../model/user/User';
import {UserManager, App} from '@unidata/core-app';

export class UserPageStore {

    @observable public currentPage: Pages = Pages.Editor;

    public listStore: any = new ListStore();
    public editorStore: any = new EditorStore();
    public settingsStore: any = new SettingsStore();

    constructor () {
        let me = this;

        const listDisposer = observe(this.listStore, 'currentItem', (change) => {
            if (change.newValue) {
                if (change.newValue.phantom) {
                    me.editorStore.createNewItem();
                    me.currentPage = Pages.Editor;
                } else {
                    me.editorStore.loadItem(change.newValue.login.getValue());
                }
            }

            return change;
        });

        const editorDisposer = observe(this.editorStore, 'currentItem', (change) => {
            if (change.newValue && me.listStore.currentItem) {
                for (let property of ['firstName', 'lastName', 'login']) {
                    change.newValue[property].bind(me.listStore.currentItem[property]);
                }
            }

            return change;
        });
    }

    public get props () {
        let methods = Object.assign(
            {
                handleEditorSave: this.handleEditorSave,
                handleListClick: this.handleListClick,
                openSettingsPage: this.openSettingsPage,
                handleAddNewItemClick: this.handleAddNewItemClick,
                handleSaveSettings: this.handleSaveSettings,
                openEditorPage: this.openEditorPage,
                isNeedConfirmToSwitchToNew: this.isNeedConfirmToSwitchToNew,
                isNeedConfirmToSwitch: this.isNeedConfirmToSwitch
            }
        );

        for (let key of Object.keys(methods)) {
            methods[key] = methods[key].bind(this);
        }

        return Object.assign(this.stores, methods);
    }

    public get stores () {
        return {
            listStore: this.listStore,
            editorStore: this.editorStore,
            settingsStore: this.settingsStore
        };
    }

    @action
    public handleListClick = (user: User) => {
        return this.listStore.setCurrentItem(user);
    }

    public handleEditorSave = () => {
        return this.editorStore.handleSave().then(() => {
            if (UserManager.getUserLogin() === this.editorStore.currentItem.login.getValue()) {
                App.deauthenticate();
            }

            this.listStore.loadItems();
        });
    }

    public handleAddNewItemClick = () => {
        return this.listStore.createNewItem();
    }

    public isNeedConfirmToSwitchToNew = (): boolean => {
        return this.editorStore.canSave;
    }

    @action
    public openEditorPage = () => {
        this.currentPage = Pages.Editor;
    }

    @action
    public openSettingsPage = () => {
        this.currentPage = Pages.Settings;
    }

    public handleSaveSettings = () => {
        return this.settingsStore.handleSave().then(() => {
            this.editorStore.reload();
        });
    }

    public isNeedConfirmToSwitch = (target: User): boolean => {
        return Boolean(this.editorStore.canSave) &&
            this.listStore.currentItem.modelId !== target.modelId;
    }
}
