/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import {action, computed, observable} from 'mobx';
import {ModelCollection} from '@unidata/core';
import {User} from '../../../model/user/User';
import remove from 'lodash/remove';
import sortBy from 'lodash/sortBy';
import lodashFilter from 'lodash/filter';
import {UserListItem} from '../../../model/user/UserListItem';
import {i18n} from '@unidata/core-app';
import {UserService} from '../../../service/UserService';

type CustomFilter = {
    filterId: string;
    fn: (user: User) => boolean;
};

export class ListStore {
    @observable public users: ModelCollection<User> = ModelCollection.create(User);
    @observable public currentItem: (User | null) = null;
    @observable public filterMode: string = 'all';
    @observable.shallow private customFilters: CustomFilter[] = [];

    constructor () {
        this.loadItems();
    }

    public filterModes = [
        {
            key: 'all',
            getTitle: () => i18n.t('admin.user>all')
        },
        {
            key: 'active',
            getTitle: () => i18n.t('admin.user>actives')
        },
        {
            key: 'inactive',
            getTitle: () => i18n.t('admin.user>inactives')
        }
    ];

    @action
    public addCustomFilter (fiterId: string, fn: (user: User) => boolean) {
        this.removeCustomFilter(fiterId);

        this.customFilters.push({
            filterId: fiterId,
            fn: fn
        });
    }

    @action
    public removeCustomFilter (fiterId: string) {
        remove(this.customFilters, function (item) {
            return item.filterId === fiterId;
        });
    }

    @computed
    public get items () {
        let activeFlag: boolean;
        let items: User[];

        if (this.filterMode === 'all') {
            items = sortBy(this.users.items, function (userListItem: UserListItem): boolean {
                return !userListItem.active.getValue();
            }) as User[];
        } else {
            if (this.filterMode === 'active') {
                activeFlag = true;
            } else if (this.filterMode === 'inactive') {
                activeFlag = false;
            }

            items = lodashFilter(this.users.items, function (userListItem: UserListItem): boolean {
                return userListItem.active.getValue() === activeFlag;
            }) as User[];
        }

        for (let filter of this.customFilters) {
            items = lodashFilter(items, filter.fn) as User[];
        }

        return items;
    }

    public loadItems () {
        UserService.getUsers().then((users: User[]) => {
            this.setUsers(users);
        }).catch((err) => {
            console.log('UserError', err);
        });
    }

    @action
    public changeFilterMode (mode: string) {
        this.filterMode = mode;
    }

    @action
    private setUsers = (users: User[]) => {
        this.users.replaceAll(users);
    }

    @action
    public createNewItem = () => {
        let user: (User | undefined) = this.users.items.find((user) => user.phantom);

        if (!user) {
            user = new User({});
            this.users.add(user);
        }

        this.setCurrentItem(user);
    }

    @action
    public setCurrentItem = (user: User) => {
        this.currentItem = user;
    }
}
