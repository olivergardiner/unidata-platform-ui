/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import {action, observable} from 'mobx';
import {IStringKeyMap, ModelCollection, PasswordConfirm, ReactiveProp, StringField} from '@unidata/core';
import {User} from '../../../model/user/User';
import {UserEndpoint} from '../../../model/user/UserEndpoint';
import {UserProperty} from '../../../model/user/UserProperty';
import {Role} from '../../../model/user/Role';
import {SecurityLabelRole} from '../../../model/user/SecurityLabelRole';
import {SecurityLabelUser} from '../../../model/user/SecurityLabelUser';
import {Dialog} from '@unidata/core-app';
import {i18n} from '@unidata/core-app';
import {EndpointService} from '../../../service/EndpointService';
import {SecurityLabelService} from '../../../service/SecurityLabelService';
import {UserService} from '../../../service/UserService';
import {UserAuthSourcesService} from '../../../service/UserAuthSourcesService';
import {RoleService} from '../../../service/RoleService';
import {IOption} from '@unidata/uikit';

export class EditorStore {
    @observable public currentItem: (User | null) = null;
    @observable public userRoles: ModelCollection<Role> = ModelCollection.create(Role);
    @observable public isSaving: boolean = false;
    @observable public isLoading: boolean = false;
    @observable public passwordConfirmation: StringField;

    @observable public dirty = false;

    public labelList: ModelCollection<SecurityLabelRole> = ModelCollection.create(SecurityLabelRole);
    public endpointList: ModelCollection<UserEndpoint> = ModelCollection.create(UserEndpoint);
    public formattedEndpoints: IOption[] = [];
    public roleList: ModelCollection<Role> = ModelCollection.create(Role);
    public formattedRoles: IOption[] = [];
    public userProperties: UserProperty[] = [];
    public userAuthSources: Array<{[key: string]: string}> = [];

    private removedLabels: any = {};

    constructor () {
        this.loadRoleList();
        this.loadLabelList();
        this.loadEndpointList();
        this.loadUserProperties();
        this.loadUserAuthSource();
    }

    public loadRoleList () {
        return RoleService.getRoleList().then((result: Role[]) => {
            this.formattedRoles = result.map((role: Role) => {
                return {
                    title: role.displayName.getValue(),
                    value: role.name.getValue()
                };
            });

            this.roleList.replaceAll(result);
        });
    }

    public loadLabelList () {
        let self = this;

        return SecurityLabelService.getSecurityLabels().then(
            function (result: SecurityLabelRole[]) {
                self.labelList.replaceAll(result);
            },
            function (result) {
                throw new Error('Error during fetching security labels');
            }
        );
    }

    private loadEndpointList () {
        this.formattedEndpoints = [];

        EndpointService.getEndpointList().then((result: UserEndpoint[]) => {
            this.formattedEndpoints = result.map((endpoint: UserEndpoint) => {
                return {
                    title: endpoint.displayName.getValue(),
                    value: endpoint.name.getValue()
                };
            });

            this.endpointList.replaceAll(result);
        });
    }

    private loadUserProperties () {
        UserService.getUserProperties().then(action((response: any[]) => {
            this.userProperties = response;
        }));
    }

    private loadUserAuthSource () {
        UserAuthSourcesService.getUserAuthSources().then(action((response: any[]) => {
            this.userAuthSources = response;
        }));
    }

    public loadItem = (login: string) => {
        let me = this;
        let result: {user?: User; userProperties?: UserProperty[]} = {};

        this.setLoading(true);

        this.removedLabels = {};

        let promiseUser = UserService.getUser(login).then((user: User) => {
            result.user = user;

            return Promise.resolve();
        });

        // loading additional user properties
        let promiseUserProperties = UserService.getUserProperties()
            .then(action((response: any[]) => {
                result.userProperties = response;

                return Promise.resolve();
            }));

        Promise.all([promiseUser, promiseUserProperties])
            .then(function (data: any) {
                me.setCurrentItem(result.user!);
                me.userProperties = result.userProperties!;

                me.setLoading(false);
            });
    }

    @action
    private setCurrentItem = (user: User | null) => {
        this.currentItem = user;

        if (this.currentItem !== null) {
            this.currentItem.setReactiveCascade([ReactiveProp.DIRTY, ReactiveProp.LAST_VALIDATION_RESULT]);
        }

        this.loadUserRoles();

        if (this.currentItem) {
            this.updateUserProperties(this.currentItem);
            this.initPasswordConfirmationField(this.currentItem);
        }
    }

    @action
    private updateUserProperties = (user: User) => {
        let allPropertiesMap: IStringKeyMap = {};
        let userOwnProperties: UserProperty[] = user.properties.items;

        this.userProperties.forEach(function (property: UserProperty) {
            allPropertiesMap[property.name.getValue()] = property;
        });

        userOwnProperties.forEach(function (userProperty: UserProperty) {
            let required = allPropertiesMap[userProperty.name.getValue()].required.getValue();

            userProperty.required.setValue(required, true);
        });
    }

    private initPasswordConfirmationField (user: User) {
        this.passwordConfirmation = new StringField('');
        this.passwordConfirmation.addValidator({
            rule: PasswordConfirm,
            message: 'admin.user>passwordsNotMatch',
            model: user,
            args: ['password']
        });
    }

    @action
    private loadUserRoles = () => {
        this.userRoles.removeAll();

        if (this.currentItem && this.currentItem.roles.getValue()) {
            for (let roleName of this.currentItem.roles.getValue()) {
                RoleService.getRole(roleName).then((role: Role) => {
                    this.addUserRole(role);
                });
            }
        }
    }

    @action
    private addUserRole = (role: Role) => {
        this.userRoles.add(role);
    }

    @action
    public createNewItem () {
        const newUser: User = new User({
            roles: [],
            properties: this.userProperties.map((item) => item.serialize())
        });

        this.setCurrentItem(newUser);
    }

    @action
    public handleCurrentItemChange = (name: string, value: any) => {
        if (name === 'passwordConfirmation') {
            this.passwordConfirmation.setValue(value);
        } else {
            (this.currentItem as any)[name].setValue(value);

            if (name === 'email' && value.length === 0) {
                this.currentItem!.emailNotification.setValue(false);
            }

            if (name === 'external') {
                if (value && this.userAuthSources.length === 1) {
                    this.currentItem!.securityDataSource.setValue(this.userAuthSources[0].name);
                } else if (!value) {
                    this.currentItem!.securityDataSource.setValue(null);
                }
            }
        }
    }

    @action
    public handleEndpointChange = (endpointName: string) => {
        const endpoint: UserEndpoint | undefined = this.endpointList!.items.find((item: UserEndpoint) =>
            item.name.getValue() === endpointName
        );

        const userEndpoint: UserEndpoint | undefined = this.currentItem!.endpoints.items.find((item: UserEndpoint) =>
            item.name.getValue() === endpointName
        );

        if (userEndpoint) {
            this.currentItem!.endpoints.remove(userEndpoint);
        } else if (endpoint) {
            this.currentItem!.endpoints.add(new UserEndpoint(endpoint.serialize()));
        }
    }

    @action
    public handleRoleChange = (roleName: string) => {
        const userRoles: string[] = this.currentItem!.roles.getValue().slice();
        const userRole: string | undefined = userRoles.find((item: string) => item === roleName);

        if (userRole) {
            this.currentItem!.roles.setValue(userRoles.filter((item: string) => item !== roleName));
        } else {
            this.currentItem!.roles.setValue([...userRoles, roleName]);
        }

        this.loadUserRoles();
    }

    @action
    public handleCheckSecurityLabel = (item: SecurityLabelUser) => {
        const userLabelName = item.name.getValue();

        const results = this.currentItem!.securityLabels.items.filter((label: SecurityLabelUser) =>
            label.name.getValue() === userLabelName
        );

        if (results.length > 0) {
            if (!this.removedLabels[userLabelName]) {
                this.removedLabels[userLabelName] = [];
            }

            for (let securityLabel of results) {
                this.handleRemoveAttributes(securityLabel);
            }

        } else {
            this.handleAddAttributes(item);
        }
    }

    @action
    public handleAddAttributes = (label: SecurityLabelUser) => {
        const userLabelName = label.name.getValue();
        const prevLabel: SecurityLabelUser[] = this.removedLabels[userLabelName];

        if (prevLabel && prevLabel.length > 0) {
            for (let l of prevLabel) {
                l.revert();
            }

            this.currentItem!.securityLabels.addItems(prevLabel);
            this.removedLabels[label.name.getValue()] = [];
        } else {
            this.currentItem!.securityLabels.add(new SecurityLabelUser(label.serialize()));
        }
    }

    @action
    public handleRemoveAttributes = (label: SecurityLabelUser) => {
        if (!label.getPhantom()) {
            if (!this.removedLabels[label.name.getValue()]) {
                this.removedLabels[label.name.getValue()] = [];
            }

            this.removedLabels[label.name.getValue()].push(label);
        }

        this.currentItem!.securityLabels.remove(label);
    }

    public get canSave () {
        if (this.isSaving) {
            return false;
        }

        return (this.currentItem && this.currentItem.getDirty());
    }

    @action
    public handleSave = (): Promise<void> => {
        let me = this;
        let apiMethod: any;
        let params: any [];

        this.setSaving(true);

        this.passwordConfirmation!.validate();
        this.currentItem!.validate();

        if (this.currentItem!.lastValidationResult.size > 0 ||
            this.passwordConfirmation!.lastValidationResult.length > 0
        ) {
            return this.handleRequestFailed();
        }

        apiMethod = this.currentItem!.getPhantom() ? UserService.createUser : UserService.updateUser;

        params = this.currentItem!.getPhantom() ?
            [this.currentItem!.serialize()] :
            [this.currentItem!.login.getValue(), this.currentItem!.serialize()];

        return apiMethod(...params).then(
            function () {
                me.currentItem!.commit();
                me.handleRequestSuccess();
            },
            this.handleRequestFailed
        );
    }

    @action
    private handleRequestSuccess = () => {
        this.setSaving(false);
        this.removedLabels = {};

        Dialog.showMessage('', i18n.t('admin.user>userSaveSuccessTitle'));

        return Promise.resolve();
    }

    private handleRequestFailed = (er?: Error) => {
        let message: string;

        this.setSaving(false);

        if (er instanceof Array) {
            message = er.map((error) => error.userMessageDetails || error.userMessage).join(' ');
        } else {
            message = i18n.t('admin.user>userSaveFailureText');
        }

        Dialog.showError(message, i18n.t('admin.user>userSaveFailureTitle'));

        return Promise.reject(er);
    }

    @action
    public setSaving = (isSaving: boolean) => {
        this.isSaving = isSaving;
    }

    @action
    public setLoading = (isLoading: boolean) => {
        this.isLoading = isLoading;
    }

    public reload = () => {
        if (this.currentItem) {
            this.loadUserProperties();
            this.loadItem(this.currentItem.login.getValue());
        }
    }
}
