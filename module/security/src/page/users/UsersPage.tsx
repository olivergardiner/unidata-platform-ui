﻿/**
 * The users page
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {Layout} from '@unidata/uikit';
import {UserList} from './list/UserList';
import {UserEditor} from './editor/UserEditor';
import {UserPropertySettings} from './settings/UserPropertySettings';
import {UserPageStore} from './store';
import {Pages} from '@unidata/types';
import {i18n, Res, ResourceManager} from '@unidata/core-app';

interface IProps {}

interface IInjectProps extends IProps {
}

@observer
export class UsersPage extends React.Component<IProps> {
    componentCls: string = 'ud-page ud-page-with-sider ud-page-user';

    store: UserPageStore = new UserPageStore();
    providerProps: any;

    constructor (props: IProps) {
        super(props);

        // the same props must be passed to the provider, otherwise mobs throws an exception
        // @see https://stackoverflow.com/questions/43550137/mobx-rerender-after-assign/43562824
        this.providerProps = this.store.props;
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>user');
    }

    get injected () {
        return this.props as IInjectProps;
    }

    get currentPage () {
        return this.store.currentPage;
    }

    render () {
        let readOnly: boolean = ResourceManager.isReadOnly([Res.ADMIN_SYSTEM_MANAGEMENT, Res.USER_MANAGEMENT]);

        return (
            <Provider {...this.providerProps}>
                <Layout className={this.componentCls}>
                    <UserList store={this.store} readOnly={readOnly} />

                    <Layout.Content>
                        {this.currentPage === Pages.Editor && <UserEditor readOnly={readOnly}/> }
                        {this.currentPage === Pages.Settings && <UserPropertySettings readOnly={readOnly}/> }
                    </Layout.Content>
                </Layout>
            </Provider>
        );
    }
}
