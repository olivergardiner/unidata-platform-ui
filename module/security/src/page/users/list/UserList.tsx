/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {ButtonWithConfirmation, Button, INTENT, SIZE, MenuSider, Confirm, Select} from '@unidata/uikit';
import {User} from '../../../model/user/User';
import {UserPageStore} from '../store';
import {CustomUserListFilter} from './CustomUserListFilter';

interface IProps {
    readOnly?: boolean;
    store: UserPageStore;
}

interface IState {
    isConfirmOpen: boolean;
    targetUser: User;
}

const EmptyUser = new User({});

@observer
export class UserList extends React.Component<IProps, IState> {
    selectedUser: User | null = null;

    state = {
        isConfirmOpen: false,
        targetUser: EmptyUser
    }

    get readOnly () {
        return this.props.readOnly;
    }

    onAddUserButtonClick = () => {
        this.props.store.openEditorPage();
        this.props.store.handleAddNewItemClick();
    }

    get createUserButton () {
        return this.props.store.isNeedConfirmToSwitchToNew() ?
            (
                <ButtonWithConfirmation
                    size={SIZE.LARGE}
                    key='button-add'
                    isRound={true}
                    intent={INTENT.SECONDARY}
                    onConfirm={this.onAddUserButtonClick}
                    title={i18n.t('admin.user>create')}
                    leftIcon={'plus'}
                    confirmationMessage={i18n.t('unsavedChanges')}
                />
            ) : (
                <Button
                    size={SIZE.LARGE}
                    key='button-add'
                    isRound={true}
                    intent={INTENT.SECONDARY}
                    onClick={this.onAddUserButtonClick}
                    title={i18n.t('admin.user>create')}
                    leftIcon={'plus'}
                />
            );
    }

    get actions () {
        const canCreateUser = (!this.readOnly &&
            ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.USER_MANAGEMENT], Right.CREATE));

        return (
            <React.Fragment>
                {canCreateUser && this.createUserButton}
                <Button
                    size={SIZE.LARGE}
                    key='button-settings'
                    isRound={true}
                    intent={INTENT.SECONDARY}
                    onClick={this.props.store.openSettingsPage}
                    title={i18n.t('admin.user>settings')}
                    leftIcon={'cog'}
                />
            </React.Fragment>
        );
    }

    get formattedDataForList () {
        return this.props.store.listStore.items.map((item: User) => {

            return {
                firstName: item.firstName.getValue(),
                lastName: item.lastName.getValue(),
                login: item.login.getValue(),
                phantom: item.phantom,
                model: item,
                key: item.modelId
            };
        });
    }

    get listColumns () {
        return [
            {
                accessor: 'firstName',
                id: 'firstName',
                Header: i18n.t('admin.user>firstName')
            },
            {
                accessor: 'lastName',
                id: 'lastName',
                Header: i18n.t('admin.user>lastName')
            },
            {
                accessor: 'login',
                id: 'login',
                Header: i18n.t('admin.user>login')
            }
        ];
    }

    get filters () {
        return [
            <Select
                value={this.props.store.listStore.filterMode}
                key={'users-filter'}
                style={{width: '100%'}}
                onChange={this.props.store.listStore.changeFilterMode.bind(this.props.store.listStore)}
                options={this.props.store.listStore.filterModes.map(function (mode: any) {
                    return {
                        value: mode.key,
                        title: mode.getTitle()
                    };
                })}
            />,
            <CustomUserListFilter
                key={'custom-users-filter'}
            />
        ];
    }

    handleListClick = () => {
        this.setState({isConfirmOpen: false});
        this.props.store.openEditorPage();
        this.selectedUser = this.state.targetUser;
        this.props.store.handleListClick(this.state.targetUser);
    }

    onSwitchClose = () => {
        this.setState({isConfirmOpen: false});
    }

    checkSwitch = (target: User) => {
        if (this.props.store.isNeedConfirmToSwitch(target)) {
            this.setState({isConfirmOpen: true, targetUser: target});

            return;
        }

        this.setState({targetUser: target}, this.handleListClick);
    }

    render () {

        return (
            <React.Fragment>
                <MenuSider
                    title={i18n.t('admin.user>userListTitle')}
                    data={this.formattedDataForList}
                    columns={this.listColumns}
                    currentItem={this.props.store.listStore.currentItem}
                    onClick={this.checkSwitch}
                    filters={this.filters}
                    actions={this.actions}
                />
                <Confirm
                    onConfirm={this.handleListClick}
                    confirmationMessage={i18n.t('unsavedChanges')}
                    isOpen={this.state.isConfirmOpen}
                    onClose={this.onSwitchClose}
                />
            </React.Fragment>
        );
    }
}
