/**
 * Component of additional filters for customer users
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {ListStore} from '../store/list';
import {
    Nullable,
    ueModuleManager,
    UeModuleReactComponent,
    UeModuleTypeComponent
} from '@unidata/core';
import {User} from '../../../model/user/User';

interface IProps {
}

interface IInjectProps extends IProps {
    listStore: ListStore;
}

@inject('listStore')
@observer
export class CustomUserListFilter extends React.Component<IProps> {
    get injected () {
        return this.props as IInjectProps;
    }

    onFilterChange (filterId: string, fn: Nullable<(user: User) => boolean>) {
        let store = this.injected.listStore;

        if (fn) {
            store.addCustomFilter(filterId, fn);
        } else {
            store.removeCustomFilter(filterId);
        }
    }

    render () {
        const self = this;

        let ueComponents = ueModuleManager.getModulesByType(UeModuleTypeComponent.USER_LIST_FILTER) || [];

        return (
            <div>
                {
                    ueComponents.map((ueComponent: UeModuleReactComponent<UeModuleTypeComponent.USER_LIST_FILTER>) => {
                        const Component = ueComponent.default.component;
                        const moduleId = ueComponent.default.moduleId;

                        return (
                            <Component
                                key={moduleId}
                                onFilterChange={self.onFilterChange.bind(self, moduleId)}
                            />
                        );
                    })
                }
            </div>
        );
    }
}
