/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {SecurityLabelList} from '../../security_labels/SecurityLabelList';
import {ModelCollection} from '@unidata/core';
import {Role} from '../../../model/user/Role';

interface IProps {
    roles: ModelCollection<Role>;
    readOnly: boolean;
}

@observer
export class RoleSecurityLabelList extends React.Component<IProps> {

    get userRolesSecurityLabels () {
        const securityLabels = this.props.roles.items.map((role: Role) => {
            return role.securityLabels.items.slice();
        }) as any[];

        return [].concat.apply([], securityLabels).filter((securityLabel: any) => {
            return securityLabel.attributes.items.length > 0;
        });
    }

    render () {

        return (
            <>
                <div style={{
                    textTransform: 'uppercase',
                    fontSize: '12px',
                    fontWeight: 'bold'
                }}>{i18n.t('admin.user>roleLabels')}</div>
                <div style={{minHeight: '138px'}}>
                    <SecurityLabelList securityLabels={this.userRolesSecurityLabels}/>
                </div>
            </>
        );
    }
}
