/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {action} from 'mobx';
import {i18n} from '@unidata/core-app';
import {CardPanel, Field} from '@unidata/uikit';
import {getErrorMessage} from '@unidata/core-app';
import {UdComponent} from '@unidata/uikit';
import {UserProperty} from '../../../model/user/UserProperty';

interface IProps {
    properties: any;
    readOnly: boolean;
    metaProperties: UserProperty[];
}

interface IInjectedProps {
}

interface IState {
}

@observer
export class PropertyForm extends UdComponent<IInjectedProps, IProps, IState> {

    @action
    onChange = (name: string, value: any) => {
        let property = this.props.properties.find((property: any) => property.name.getValue() === name);

        property.value.setValue(value);
    }

    userPropertyReadOnly (name: string): boolean {
        let property = this.props.metaProperties.find((property: any) => property.name.getValue() === name);

        if (property && property.readOnly.getValue()) {
            return true;
        }

        return false;
    }

    render () {
        const {properties, readOnly} = this.props;

        return (
            <CardPanel internal title={i18n.t('admin.user>info')}>
                {properties.items.map((property: any) =>
                    <Field.Input
                        key={property.name.getValue()}
                        type='text'
                        name={property.name.getValue()}
                        disabled={readOnly || this.userPropertyReadOnly(property.name.getValue())}
                        label={property.displayName.getValue() + (property.required.getValue() ? '*' : '')}
                        defaultValue={property.value.getValue()}
                        onChange={this.onChange}
                        error={getErrorMessage(property, 'value')}
                    />

                )}
            </CardPanel>
        );
    }
}
