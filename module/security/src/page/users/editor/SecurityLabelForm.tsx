/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';

import {SecurityLabelRole} from '../../../model/user/SecurityLabelRole';
import {SecurityLabelUser} from '../../../model/user/SecurityLabelUser';
import {SecurityLabelsList} from '../../roles/editor/security_labels_list/SecurityLabelsList';
import {ModelCollection} from '@unidata/core';

interface IProps {
    roleSecurityLabels: ModelCollection<SecurityLabelUser>;
    allSecurityLabels: SecurityLabelRole[];
    handleCheckSecurityLabel: (item: SecurityLabelUser) => void;
    handleAddAttributes: (item: SecurityLabelUser) => void;
    handleRemoveAttributes: (item: SecurityLabelUser) => void;
    readOnly: boolean;
}

@observer
export class SecurityLabelForm extends React.Component<IProps> {

    render () {
        const {
            roleSecurityLabels,
            allSecurityLabels,
            handleCheckSecurityLabel,
            handleAddAttributes,
            handleRemoveAttributes,
            readOnly
        } = this.props;

        return (
            <>
                <div style={{
                    textTransform: 'uppercase',
                    fontSize: '12px',
                    fontWeight: 'bold'
                }}>{i18n.t('admin.user>userLabels')}</div>
                <div style={{minHeight: '138px'}}>
                    <SecurityLabelsList
                        allSecurityLabels={allSecurityLabels}
                        roleSecurityLabels={roleSecurityLabels}
                        handleCheckSecurityLabel={handleCheckSecurityLabel}
                        handleAddAttributes={handleAddAttributes}
                        handleRemoveAttributes={handleRemoveAttributes}
                        readOnly={readOnly}
                    />
                </div>
            </>
        );
    }
}
