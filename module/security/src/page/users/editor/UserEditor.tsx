/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {Button, CardPanel, Editor, INTENT, PageSpinner, Col, Row} from '@unidata/uikit';
import {AccountForm} from './AccountForm';
import {PropertyForm} from './PropertyForm';
import {SecurityLabelForm} from './SecurityLabelForm';
import {RoleSecurityLabelList} from './RoleSecurityLabelList';
import {EditorStore} from '../store/editor';
import {SecurityLabelRole} from '../../../model/user/SecurityLabelRole';

interface IProps {
    readOnly: boolean;
}

interface IInjectProps extends IProps {
    editorStore: EditorStore;
    handleEditorSave: () => Promise<void>;
}

@inject('editorStore', 'handleEditorSave')
@observer
export class UserEditor extends React.Component<IProps> {

    get injected () {
        return this.props as IInjectProps;
    }

    get store () {
        return this.injected.editorStore;
    }

    get canEdit () {
        if (this.props.readOnly) {
            return false;
        }

        if (this.store.currentItem && this.store.currentItem.phantom) {
            return ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.USER_MANAGEMENT], Right.CREATE);
        }

        return ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.USER_MANAGEMENT], Right.UPDATE);
    }

    get allSecurityLabels () {
        let result: SecurityLabelRole[] = [];

        const allLabelList = this.store.labelList.items.slice();

        const hasLabelInResult = (labelName: string) => {
            return result.find((label) => label.name.getValue() === labelName);
        };

        for (let role of this.store.userRoles.items) {
            for (let label of role.securityLabels.items) {
                let secLabel = allLabelList.find((metaLabel: SecurityLabelRole) => metaLabel.name.getValue() === label.name.getValue());

                if (secLabel && !hasLabelInResult(secLabel.name.getValue())) {
                    result.push(secLabel);
                }
            }
        }

        return result;
    }

    get extraButtons () {
        if (!this.canEdit) {
            return undefined;
        }

        return (
            <Button
                leftIcon={'save'}
                isRound={true}
                intent={INTENT.PRIMARY}
                isDisabled={!this.store.canSave}
                onClick={this.injected.handleEditorSave}
            >
                {i18n.t('admin.user>saveButtonText')}
            </Button>
        );
    }

    render () {
        let readOnly = !this.canEdit;

        if (this.store.isLoading) {
            return <PageSpinner/>;
        }

        if (!this.store.currentItem) {
            return null;
        }

        return (
            <Editor
                title={i18n.t('admin.user>editorTitle')}
                groupSectionTitle={i18n.t('page.header>administration')}
                iconType='user'
                isWaiting={this.store.isSaving}
                itemTitle={this.store.currentItem.fullName.getValue()}
                extraButtons={this.extraButtons}
            >
                <CardPanel title={i18n.t('admin.user>settings')}>
                    <Row gutter={40}>
                        <Col span={12}>
                            <AccountForm
                                user={this.store.currentItem}
                                endpoints={this.store.formattedEndpoints}
                                onUserChange={this.store.handleCurrentItemChange}
                                handleEndpointChange={this.store.handleEndpointChange}
                                readOnly={readOnly}
                                roles={this.store.formattedRoles}
                                handleRoleChange={this.store.handleRoleChange}
                                passwordConfirmation={this.store.passwordConfirmation}
                                authSources={this.store.userAuthSources}
                                userFields={this.store.currentItem.properties}
                            />
                        </Col>

                        <Col span={12}>
                            <PropertyForm
                                readOnly={readOnly}
                                properties={this.store.currentItem.properties}
                                metaProperties={this.store.userProperties}
                            />
                        </Col>
                    </Row>
                </CardPanel>

                <div style={{height: '12px'}}></div>

                <CardPanel title={i18n.t('admin.user>securityLabelSettings')}>
                    <Row gutter={40}>
                        <Col span={12}>
                            <SecurityLabelForm
                                roleSecurityLabels={this.store.currentItem.securityLabels}
                                allSecurityLabels={this.allSecurityLabels}
                                readOnly={readOnly}
                                handleCheckSecurityLabel={this.store.handleCheckSecurityLabel}
                                handleAddAttributes={this.store.handleAddAttributes}
                                handleRemoveAttributes={this.store.handleRemoveAttributes}
                            />
                        </Col>

                        <Col span={12}>
                            <RoleSecurityLabelList readOnly={readOnly} roles={this.store.userRoles}/>
                        </Col>
                    </Row>
                </CardPanel>
            </Editor>
        );
    }
}
