/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n, Locales, UserManager} from '@unidata/core-app';
import {CardPanel, Field, IOption, Divider, Switcher} from '@unidata/uikit';
import {User} from '../../../model/user/User';
import {StringField, UeModuleTypeComponent, ueModuleManager} from '@unidata/core';
import {getErrorMessage} from '@unidata/core-app';

interface IProps {
    user: User;
    endpoints: IOption[];
    onUserChange: (name: string, value: any) => void;
    handleEndpointChange: (endpointName: string) => void;
    roles: IOption[];
    authSources: Array<{[key: string]: string}>;
    handleRoleChange: (roleName: string) => void;
    passwordConfirmation: StringField;
    readOnly?: boolean;
    userFields: any;
}

@observer
export class AccountForm extends React.Component<IProps> {

    get rolesDefaultValue () {
        if (this.props.user.roles.getValue()) {
            return this.props.user.roles.getValue().slice();
        }

        return [];
    }

    get renderTitle () {
        const checked: boolean = this.props.user.active.getValue();
        const labelKey: string = checked ? 'active' : 'unactive';
        const readOnly = this.props.readOnly;

        return (
            <div>
                <Switcher
                    size='small'
                    checked={checked}
                    disabled={readOnly}
                    onChange={this.handleActiveChange}
                />
                <span
                    style={{
                        padding: '0 5px',
                        display: 'inline-block'
                    }}
                >
                    {i18n.t(`admin.user>${labelKey}`)}
                </span>
            </div>
        );
    }

    get passwordConfirmationError () {
        const {passwordConfirmation} = this.props,
            validationResult = passwordConfirmation.lastValidationResult,
            isHaveFirstElement = (validationResult && validationResult.length > 0);

        return isHaveFirstElement ? i18n.t(validationResult[0]) : undefined;
    }

    get authSourceOptions () {
        return this.props.authSources.map((item) => {
            return {title: item.description, value: item.name};
        });
    }

    get roleCustomizationButton (): JSX.Element | undefined {
        //TODO: fix type
        const externals = ueModuleManager.getModulesByType(UeModuleTypeComponent.ROLE_CUSTOMIZER) as any;
        const RoleCustomizationComponent = externals[0] ? externals[0].default.component : null;

        return !RoleCustomizationComponent ? undefined : (<RoleCustomizationComponent
                    user={this.props.user}
                    complete={this.props.roles}
                    selected={this.rolesDefaultValue}
                />
        );
    }

    handleSelectChange = (name: string, value: string) => {
        if (name === 'endpoint') {
            this.props.handleEndpointChange(value);
        } else if (name === 'role') {
            this.props.handleRoleChange(value);
        } else {
            this.props.onUserChange(name, value || null);
        }
    }

    handleActiveChange = (checked: boolean) => {
        this.props.onUserChange('active', checked);
    }


    render () {
        const {user, endpoints, roles, passwordConfirmation, onUserChange, readOnly} = this.props;

        return (
            <CardPanel
                internal
                title={this.renderTitle}
            >
                <Field.Input
                    label={i18n.t('admin.user>firstNameAndLastName') + '*'}
                    onChange={onUserChange}
                    items={[
                        {
                            name: 'firstName',
                            inputWidth: 47,
                            disabled: readOnly,
                            defaultValue: user.firstName.getValue(),
                            error: getErrorMessage(user, 'firstName')
                        },
                        {
                            name: 'lastName',
                            inputWidth: 47,
                            disabled: readOnly,
                            defaultValue: user.lastName.getValue(),
                            error: getErrorMessage(user, 'lastName')
                        }
                    ]}
                />

                <Field.Input name='login'
                    label={i18n.t('admin.user>login') + '*'}
                    disabled={readOnly || !user.phantom}
                    defaultValue={user.login.getValue()}
                    onChange={onUserChange}
                    error={getErrorMessage(user, 'login')}
                />

                <Field.Input
                    name='email'
                    label={i18n.t('admin.user>email')}
                    disabled={readOnly}
                    defaultValue={user.email.getValue()}
                    onChange={onUserChange}
                    error={getErrorMessage(user, 'email')}
                />

                <Field.Checkbox
                    name='emailNotification'
                    inputLabel={i18n.t('admin.user>emailNotification')}
                    defaultChecked={user.emailNotification.getValue()}
                    onChange={onUserChange}
                    error={getErrorMessage(user, 'emailNotification')}
                    disabled={readOnly || !user.email.getValue()}
                    // offset={9}
                />

                <Field.Input
                    type='password'
                    name='password'
                    label={i18n.t('admin.user>newPassword') + '*'}
                    defaultValue={user.password.getValue()}
                    disabled={readOnly}
                    onChange={onUserChange}
                    error={getErrorMessage(user, 'password')}
                />

                <Field.Input
                    type='password'
                    name='passwordConfirmation'
                    error={this.passwordConfirmationError}
                    label={i18n.t('admin.user>confirmPassword') + '*'}
                    defaultValue={passwordConfirmation.getValue()}
                    disabled={readOnly}
                    onChange={onUserChange}
                />

                <Divider dashed/>

                <Field.Checkbox
                    label={i18n.t('admin.user>rights')}
                    onChange={onUserChange}
                    items={[
                        {
                            name: 'admin',
                            disabled: readOnly || !UserManager.isUserAdmin(),
                            defaultChecked: user.admin.getValue(),
                            inputWidth: 50,
                            inputLabel: i18n.t('admin.user>admin')
                        },
                        {
                            name: 'external',
                            disabled: readOnly,
                            defaultChecked: user.external.getValue(),
                            inputWidth: 47,
                            inputLabel: i18n.t('admin.user>external')
                        }
                    ]}
                />

                {user.external.getValue() &&
                    <Field.Select
                        name='securityDataSource'
                        label={i18n.t('admin.user>securityDataSource')}
                        options={this.authSourceOptions}
                        value={user.securityDataSource.getValue()}
                        allowClear={true}
                        onSelect={this.handleSelectChange}
                        onChange={this.handleSelectChange}
                        disabled={readOnly}
                        error={getErrorMessage(user, 'securityDataSource')}
                    />
                }

                <Field.Select
                    name='locale'
                    label={i18n.t('admin.user>localization')}
                    onSelect={this.handleSelectChange}
                    value={user.locale.getValue() || ' '}
                    disabled={readOnly}
                    error={getErrorMessage(user, 'locale')}
                    options={[
                        {
                            title: i18n.t(`app.locales>${Locales.Ru}`),
                            value: Locales.Ru
                        },
                        {
                            title: i18n.t(`app.locales>${Locales.En}`),
                            value: Locales.En
                        }
                    ]}
                />

                <Field.Select
                    name='endpoint'
                    mode='multiple'
                    label={i18n.t('admin.user>endpoint') + '*'}
                    error={getErrorMessage(user, 'endpoints')}
                    value={user.endpoints.items.map((point) => point.name.getValue())}
                    options={endpoints}
                    disabled={readOnly}
                    onSelect={this.handleSelectChange}
                    onDeselect={this.handleSelectChange}
                />

                <Field.Select
                    name='role'
                    mode='multiple'
                    label={i18n.t('admin.user>roles')}
                    disabled={readOnly}
                    onSelect={this.handleSelectChange}
                    onDeselect={this.handleSelectChange}
                    options={roles}
                    value={this.rolesDefaultValue}
                    overlapButton={this.roleCustomizationButton}
                />

            </CardPanel>
       );
    }
}
