﻿/**
 * Role editing page
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {Layout} from '@unidata/uikit';
import {RoleList} from './list/RoleList';
import {RoleEditor} from './editor/RoleEditor';
import {RolePropertySettings} from './settings/RolePropertySettings';
import {RolePageStore} from './store/RolePageStore';
import {Pages} from '@unidata/types';
import {i18n, Res, ResourceManager, UserManager} from '@unidata/core-app';

interface IProps {}

@observer
export class RolePage extends React.Component<IProps> {
    static PAGE_CLASS: string = 'ud-page ud-page-with-sider ud-page-roles';

    store: RolePageStore = new RolePageStore();
    providerProps: any;

    constructor (props: IProps) {
        super(props);

        // the same props must be passed to the provider, otherwise mobs throws an exception
        // @see https://stackoverflow.com/questions/43550137/mobx-rerender-after-assign/43562824
        this.providerProps = this.store.props;
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>role');
    }

    get currentPage () {
        return this.store.currentPage;
    }

    render () {
        let readOnly: boolean = ResourceManager.isReadOnly([Res.ADMIN_SYSTEM_MANAGEMENT, Res.ROLE_MANAGEMENT]);
        let roles = UserManager.getUserRoles();

        return (
            <Provider {...this.providerProps}>
                <Layout className={RolePage.PAGE_CLASS}>
                    <RoleList
                        readOnly={readOnly}
                        readOnlyRoles={roles}
                        store={this.store}
                    />

                    <Layout.Content>
                        {this.currentPage === Pages.Editor && <RoleEditor readOnly={readOnly} readOnlyRoles={roles} /> }
                        {this.currentPage === Pages.Settings && <RolePropertySettings readOnly={readOnly}/> }
                    </Layout.Content>
                </Layout>
            </Provider>
        );
    }
}
