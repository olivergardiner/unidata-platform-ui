/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {action} from 'mobx';
import {observer} from 'mobx-react';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {Role} from '../../../model/user/Role';
import {Button, ButtonWithConfirmation, INTENT, SIZE, MenuSider, Confirm} from '@unidata/uikit';
import {RolePageStore} from '../store/RolePageStore';

interface IProps {
    readOnly?: boolean;
    readOnlyRoles: string[]; // Roles that the user can't edit
    store: RolePageStore;
}

interface IState {
    isConfirmOpen: boolean;
    targetRole: Role;
}

const EmptyRole = new Role({});

@observer
export class RoleList extends React.Component<IProps, IState> {
    selectedRole: Role | null = null;

    state = {
        isConfirmOpen: false,
        targetRole: EmptyRole
    }

    get readOnly () {
        return this.props.readOnly;
    }

    get formattedDataForMenu () {
        return this.props.store.menuStore.items.map((item: Role) => {
            return {
                displayName: item.displayName.getValue(),
                name: item.name.getValue(),
                phantom: item.phantom,
                model: item,
                key: item.name.getValue()
            };
        });
    }

    onCreateRole = () => {
        this.props.store.openEditorPage();
        this.props.store.handleAddNewItemClick();
    }

    get createRoleButton () {
        return this.props.store.isNeedConfirmToSwitchToNew() ?
            (
                <ButtonWithConfirmation
                    size={SIZE.LARGE}
                    intent={INTENT.SECONDARY}
                    isRound={true}
                    leftIcon={'plus'}
                    onConfirm={this.onCreateRole}
                    title={i18n.t('admin.role>create')}
                    confirmationMessage={i18n.t('unsavedChanges')}
                />
            ) : (
                <Button
                    size={SIZE.LARGE}
                    intent={INTENT.SECONDARY}
                    isRound={true}
                    leftIcon={'plus'}
                    onClick={this.onCreateRole}
                    title={i18n.t('admin.role>create')}
                />
            );
    }

    get actions () {
        const canCreateRole = (!this.readOnly &&
            ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.ROLE_MANAGEMENT], Right.CREATE));

        return (
            <React.Fragment>
                {canCreateRole && this.createRoleButton}
                <Button
                    size={SIZE.LARGE}
                    intent={INTENT.SECONDARY}
                    isRound={true}
                    leftIcon={'cog'}
                    onClick={this.props.store.openSettingsPage}
                    title={i18n.t('admin.role>settings')}
                />
            </React.Fragment>
        );
    }

    get menuColumns () {
        const {readOnlyRoles} = this.props;

        return [{
            accessor: 'displayName',
            id: 'displayName',
            // Header: i18n.t('admin.role>displayName'),
            Cell: (cell: any) => {
                const readOnly = readOnlyRoles.indexOf(cell.original.name) > -1;

                return (
                    <div className={'ud-menu-cell-container'}>
                        <div className="ud-menu-name">{cell.value}</div>
                        <div className="ud-menu-readonly">{readOnly ? i18n.t('admin.role.settings>readOnlyColumnText') : '' }</div>
                    </div>
                );
            }
        }];
    }

    @action
    handleListClick = () => {
        this.setState({isConfirmOpen: false});
        this.props.store.openEditorPage();
        this.selectedRole = this.state.targetRole;
        this.props.store.handleListClick(this.state.targetRole);
    }

    onSwitchClose = () => {
        this.setState({isConfirmOpen: false});
    }

    checkSwitch = (target: Role) => {
        if (this.props.store.isNeedConfirmToSwitch(target)) {
            this.setState({isConfirmOpen: true, targetRole: target});

            return;
        }

        this.setState({targetRole: target}, this.handleListClick);
    }

    render () {
        return (
            <React.Fragment>
                <MenuSider
                    title={i18n.t('admin.role>roleListTitle')}
                    data={this.formattedDataForMenu}
                    columns={this.menuColumns}
                    currentItem={this.props.store.menuStore.selectedRole}
                    onClick={this.checkSwitch}
                    actions={this.actions}
                    hideHeader={true}
                />
                <Confirm
                    onConfirm={this.handleListClick}
                    confirmationMessage={i18n.t('unsavedChanges')}
                    isOpen={this.state.isConfirmOpen}
                    onClose={this.onSwitchClose}
                />
            </React.Fragment>
        );
    }
}
