/**
 * Section for editing additional Role parameters
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {inject, observer, Observer} from 'mobx-react';
import {Button, CardPanel, Checkbox, Input, INTENT, PageHeader, Table} from '@unidata/uikit';
import {RoleProperty} from '../../../model/user/RoleProperty';

import * as styles from './../../users/settings/userPropertySettings.m.scss';

interface IProps {
    readOnly?: boolean;
}

interface IInjectProps extends IProps {
    settingsStore: any;
}

@inject('settingsStore')
@observer
export class RolePropertySettings extends React.Component<IProps> {

    columns = [
        {
            id: 'required',
            accessor: 'required',
            Header: () => {
                // we use <Observer> because after updating React => 16.9, an error occurs when trying to pass observer( ) for Header or Cell
                return (
                    <Observer render={() => (
                        i18n.t('admin.role.settings>requiredColumnText')
                    )}/>
                );
            },
            Cell: ({original}: { original: RoleProperty }) => {
                return (
                    <Observer render={() => (
                        <Checkbox
                            name='required'
                            disabled={this.readOnly || this.store.isItemRemoved(original) || original.readOnly.getValue()}
                            checked={original.required.getValue()}
                            onChange={({target}: { target: any }) => {
                                this.store.handleItemChange(original, 'required', target.checked);
                            }}
                        />
                    )}/>
                );
            }
        },
        {
            id: 'readOnly',
            accessor: 'readOnly',
            Header: () => {
                // we use <Observer> because after updating React => 16.9, an error occurs when trying to pass observer( ) for Header or Cell
                return (
                    <Observer render={() => (
                        i18n.t('admin.role.settings>readOnlyColumnText')
                    )}/>
                );
            },
            Cell: ({original}: { original: RoleProperty }) => {
                return (
                    <Observer render={() => (
                        <Checkbox
                            name='readOnly'
                            disabled={this.readOnly || this.store.isItemRemoved(original) || original.required.getValue()}
                            checked={original.readOnly.getValue()}
                            onChange={({target}: { target: any }) => {
                                this.store.handleItemChange(original, 'readOnly', target.checked);
                            }}
                        />
                    )}/>
                );
            }
        },
        {
            id: 'name',
            accessor: 'name',
            Header: i18n.t('admin.role.settings>nameColumnText'),
            Cell: ({original}: { original: RoleProperty }) => {
                return original.name.getValue();
            }
        },
        {
            id: 'displayName',
            accessor: 'displayName',
            Header: i18n.t('admin.role.settings>displayNameColumnText'),
            Cell: ({original}: { original: RoleProperty }) => {
                return original.displayName.getValue();
            }
        }
    ] as any[];

    renderInput = (key: 'name' | 'displayName') => ({original, onEdit}: {original: RoleProperty; onEdit: (value: string) => void}) => {
        return (
            <Input
                name={key}
                defaultValue={original[key].getValue()}
                autoFocus={true}
                onBlur={(e) => {
                    const val = e.target.value;

                    onEdit(val);
                }}
                disabled={this.readOnly || this.store.isItemRemoved(original)}
            />
        );
    };

    editComponents = {
        displayName: this.renderInput('displayName'),
        name: this.renderInput('name')
    };

    onDeleteRow = (rowIndex: number) => {
        const original = this.store.actualPropertyList[rowIndex];

        this.store.handleRemoveItem(original);
    };

    get injected () {
        return this.props as IInjectProps;
    }

    get store () {
        return this.injected.settingsStore;
    }

    get items () {
        return this.store.items;
    }

    get readOnly () {
        let resources = [Res.ADMIN_SYSTEM_MANAGEMENT, Res.ROLE_MANAGEMENT];

        return this.props.readOnly || !ResourceManager.userHasAnyResourceRight(resources, Right.UPDATE);
    }

    get extraButtons () {
        if (this.readOnly) {
            return undefined;
        }

        return (
            <Button
                leftIcon={'save'}
                isRound={true}
                intent={INTENT.PRIMARY}
                isDisabled={!this.store.canSave}
                onClick={this.store.handleSave}
            >
                {i18n.t('admin.role.settings>saveButtonText')}
            </Button>
        );
    }

    onCellEdit = (rowIndex: number, cellId: string, value: string) => {
        const original = this.store.actualPropertyList[rowIndex];

        this.store.handleItemChange(original, cellId, value);
    };

    render () {
        return (
            <>
                <PageHeader
                    sectionTitle={i18n.t('admin.role.settings>editorTitle')}
                    itemTitle={i18n.t('admin.role.settings>editorItemTitle')}
                    groupSectionTitle={i18n.t('page.header>administration')}
                    iconType='users'
                    extraButtons={this.extraButtons}
                />
                <div className={styles.cardContainer}>
                    <CardPanel
                        title={i18n.t('admin.role.settings>settingsTitle')}
                    >
                        <div className={styles.tableContainer}>
                            <Table
                                hasHeaderBg={false}
                                columns={this.columns}
                                data={this.store.actualPropertyList.slice()}
                                editComponents={this.editComponents}
                                isRowsDeletable={!this.readOnly}
                                onRowDelete={this.onDeleteRow}
                                onAllDelete={this.store.handleRemoveAll}
                                isEditable={true}
                                viewRowsCount={this.store.actualPropertyList.slice().length}
                                onCellEdit={this.onCellEdit}
                            />
                        </div>

                        <div style={{
                            textAlign: 'center',
                            paddingTop: 10,
                            paddingBottom: 10
                        }}>
                            {!this.readOnly && (
                                <Button
                                    onClick={this.store.handleAddItem}
                                    intent={INTENT.PRIMARY}
                                    isMinimal={true}
                                    leftIcon={'plus-circle'}
                                >
                                    {i18n.t('admin.role.settings>addSettings')}
                                </Button>
                            )}
                        </div>
                    </CardPanel>
                </div>
            </>
        );
    }
}
