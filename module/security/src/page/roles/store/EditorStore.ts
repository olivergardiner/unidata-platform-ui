/**
 * Store of the role editor
 *
 * @author Ivan Marshalkin
 * @date 2019-05-22
 */

import {action, observable} from 'mobx';
import {Role} from '../../../model/user/Role';
import {Right} from '../../../model/user/Right';
import {SecurityLabel} from '../../../model/user/SecurityLabel';
import {SecurityLabelRole} from '../../../model/user/SecurityLabelRole';
import {buildTreeNode, buildTreeNodeList} from './SecurityRuleTreeHelper';
import {Dialog, i18n} from '@unidata/core-app';
import {RightTabEnum} from '../editor/type/RightTabEnum';
import {
    IStringKeyMap,
    Nullable,
    ReactiveProp,
    ueModuleManager,
    UeModuleReactComponent,
    UeModuleTypeComponent
} from '@unidata/core';
import {CrudRights} from '../../../types/CrudRights';
import {UserProperty} from '../../../model/user/UserProperty';
import {RoleProperty} from '../../../model/user/RoleProperty';
import {SecurityLabelService} from '../../../service/SecurityLabelService';
import {RolePropertyService} from '../../../service/RolePropertyService';
import {RoleService} from '../../../service/RoleService';
import {Resource} from '../../../model/user/Resource';
import {ResourceTreeItem} from '../../../types/ResourceTreeItem';
import {RightCoherenceStore} from './RightCoherenceStore';
import keyBy from 'lodash/keyBy';

export class EditorStore {
    @observable public currentRole: Role | null;
    @observable public isLoading: boolean = false;
    @observable public isSaving: boolean = false;
    @observable public isExpand: boolean = false;
    @observable public rights: Right[] = [];
    @observable public activeRightTab: RightTabEnum = RightTabEnum.right;
    @observable public securedResourcesTree: ResourceTreeItem[] = [];
    @observable public securedResourcesFlat: ResourceTreeItem[] = [];

    public changeExpandFunctions: Function[] = [];
    public securityLabels: SecurityLabel[] = [];
    public allRoleProperties: RoleProperty[] = [];
    public rightsBySecuredResourceMap: Map<string, Right> = new Map();
    public rightCoherenceStore: RightCoherenceStore;

    private removedLabels: any = {};

    private readonly ueList: Nullable<Array<UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>>>;

    constructor () {
        let promises: Array<Promise<any>> = [];

        // eslint-disable-next-line max-len
        this.ueList = ueModuleManager.getModulesByType(UeModuleTypeComponent.ROLE_SECURITY) as Nullable<Array<UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>>>;

        this.loadAllSecurityResources();
        this.loadAllSecurityLabels();
        this.loadAllRoleProperties();
    }

    public getUEList (): Nullable<Array<UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>>> {
        return this.ueList;
    }

    private loadAllSecurityResources (): Promise<void> {
        let me = this;

        return RoleService.getRoleSecurityResourceList().then(action((resources: Resource[]) => {
            me.setLoading(false);

            me.securedResourcesTree = buildTreeNode(resources);
            me.securedResourcesFlat = buildTreeNodeList(me.securedResourcesTree);

            return Promise.resolve();
        }));
    }


    @action
    private setAllSecurityResources = (resources: any[]) => {
        this.securedResourcesTree = buildTreeNode(resources);
    }

    @action
    public loadAllSecurityLabels () {
        let me = this;

        SecurityLabelService.getSecurityLabels().then(
            action((result: SecurityLabel[]) => {
                me.securityLabels = result;
            }),
            function (result) {
                throw new Error('Error during fetching security labels');
            });
    }

    @action
    private loadAllRoleProperties () {
        RolePropertyService.getRolePropertyList().then(action((response: any[]) => {
            this.allRoleProperties = response;
        }));
    }

    public get canSave () {
        if (this.isSaving) {
            return false;
        }

        return (this.currentRole && (this.currentRole.getDirty() || this.computeUECanSave()));
    }

    private computeUECanSave () {
        const ueList = this.getUEList();

        let canSave = true;

        if (ueList) {
            canSave = ueList.reduce((result: boolean, item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                return !item.default.meta.canSave || item.default.meta.canSave();
            }, true);
        }

        return canSave;
    }

    public get canDestroy () {
        if (this.isSaving) {
            return false;
        }

        return (this.currentRole !== undefined);
    }

    @action
    public createNewRole () {
        let role = new Role({
            properties: [...this.allRoleProperties.map((property) => property.serialize())]
        });

        this.UEResetAll();
        this.updateCurrentRole(role);
    }

    @action
    public loadRole (name: string) {
        let me = this;
        let result: {role?: Role; roleProperties?: RoleProperty[]} = {};

        this.removedLabels = {};

        this.setLoading(true);

        // loading the role
        let promiseRole = RoleService.getRole(name)
            .then(action((role: Role) => {
                result.role = role;

                me.prepareRole(role);

                return Promise.resolve();
            }));

        // loading the role rights
        let promiseUEItems = this.UELoadAll(name);

        let promiseRoleProperties = RolePropertyService.getRolePropertyList()
            .then(action((roleProperties: RoleProperty []) => {
                result.roleProperties = roleProperties;

                return Promise.resolve();
            }));

        Promise.all([promiseRole, promiseRoleProperties].concat(promiseUEItems))
            .then(function (data: any) {
                me.allRoleProperties = result.roleProperties!;
                me.updateCurrentRole(result.role!);

                me.setLoading(false);
            });
    }

    /**
     * Performs the upload function for all UE
     * @param roleName
     */
    private UELoadAll (roleName: string): Array<Promise<any>> {
        const ueList = this.getUEList();

        let promises: Array<Promise<any>> = [];

        if (ueList) {
            promises = ueList
                .filter((item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                    return Boolean(item.default.meta.load);
                })
                .map((item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                    return item.default.meta.load(roleName);
                });
        }

        return promises;
    }

    /**
     * Performs the save function for all UE
     * @param roleName
     */
    private UESaveAll (roleName: string): Array<Promise<any>> {
        const ueList = this.getUEList();

        let promises: Array<Promise<any>> = [];

        if (ueList) {
            promises = ueList
                .filter((item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                    return Boolean(item.default.meta.save);
                })
                .map((item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                    return item.default.meta.save(roleName);
                });
        }

        return promises;
    }

    /**
     * Performs the reset function for all UE
     * @param roleName
     */
    private UEResetAll (): void {
        const ueList = this.getUEList();

        if (ueList) {
            ueList
                .filter((item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                    return Boolean(item.default.meta.reset);
                })
                .forEach((item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                    return item.default.meta.reset();
                });
        }
    }

    /**
     * Preparing the role for operation
     *
     * @param role
     */
    private prepareRole (role: Role) {
        let securityLabels: SecurityLabelRole[] = [],
            processed: string[] = [];

        securityLabels = role.securityLabels.items.slice();

        // adding fake security tags without attributes, it is used to indicate " security tag enabled"
        securityLabels.forEach(function (item) {
            if (processed.indexOf(item.name.getValue()) === -1) {
                role.securityLabels.add(new SecurityLabelRole({
                    name: item.name.getValue(),
                    displayName: item.displayName.getValue(),
                    attributes: [],
                    description: ''
                }));
            }
        });

        // since we have rewritten the model we need to commit it
        role.securityLabels.commit();
    }

    @action
    private updateRoleProperties = (role: Role) => {
        let allPropertiesMap: IStringKeyMap = {};
        let roleOwnProperties: UserProperty[] = role.properties.items;

        this.allRoleProperties.forEach(function (property: UserProperty) {
            allPropertiesMap[property.name.getValue()] = property;
        });

        roleOwnProperties.forEach(function (roleProperty: UserProperty) {
            let required = allPropertiesMap[roleProperty.name.getValue()].required.getValue();

            roleProperty.required.setValue(required, true);
        });
    }

    @action
    public updateCurrentRole = (role: Role | null) => {
        this.currentRole = role;

        if (this.currentRole) {
            this.currentRole.setReactive([ReactiveProp.DIRTY]);
            this.updateRoleProperties(this.currentRole);
            this.updateRightsBySecuredResources(this.currentRole);
            this.setRights();

            this.updateRightCoherenceStore();
        }
    }

    @action
    private setRights () {
        this.rights = [];
        this.changeExpandFunctions = [];

        const roleRights = this.currentRole!.rights.items.slice();

        const toggleExpand = (node: any) => (isExpand: boolean) => {
            node.expand = isExpand;
        };

        const checkRights = (resources: any) => {
            for (let node of resources) {
                if (node.roleRight !== undefined) {
                    let find: boolean = false;

                    for (let right of roleRights) {
                        if ((right.securedResource as Resource).name.getValue() === node.name) {
                            this.rights.push(right);
                            node.roleRight = right;
                            find = true;
                            break;
                        }
                    }

                    if (!find) {
                        let right: Right = new Right({
                            securedResource: node,
                            [CrudRights.Read]: false,
                            [CrudRights.Create]: false,
                            [CrudRights.Update]: false,
                            [CrudRights.Delete]: false
                        });

                        this.rights.push(right);
                        node.roleRight = right;
                    }
                }

                if (node.children) {
                    checkRights(node.children);
                    this.changeExpandFunctions.push(toggleExpand(node));
                }
            }
        };

        checkRights(this.securedResourcesTree);
    }

    @action
    public handleRoleChange = (name: string, value: string) => {
        if (this.currentRole) {
            (this.currentRole as any)[name].setValue(value);
        }
    }

    @action
    public handleRuleChange = (right: Right, crudAction: string, value: boolean, unpermittedActions: CrudRights[], treeNode: any) => {
        const allRules: CrudRights[] = [CrudRights.Create, CrudRights.Read, CrudRights.Update, CrudRights.Delete],
              availableActions: CrudRights[] = allRules.filter((right) => unpermittedActions.indexOf(right) === -1);

        /* right setting value block */
        if (crudAction === 'all') {
            for (let action of availableActions) {
                right[action].setValue(value);
            }
        } else if (crudAction === CrudRights.Read && !value) {
            if (treeNode.type === 'SYSTEM' || (treeNode.parentNode && treeNode.parentNode.nodeType !== 'SECURED_RESOURCE_NODE')) {
                // we forbid disabling read if there are any other rights
                if (!right[CrudRights.Create].getValue() && !right[CrudRights.Update].getValue() && !right[CrudRights.Delete].getValue()) {
                    right[CrudRights.Read].setValue(value);
                }
            } else {
                right[CrudRights.Read].setValue(value);
            }
        } else {
            //  when any permission is set, read is automatically active
            if (crudAction !== CrudRights.Read && value === true) {
                right[CrudRights.Read].setValue(value);
            }

            (right as any)[crudAction].setValue(value);
        }

        /* role update block */
        if (value) {
            if (!this.findUserRight(right)) {
                this.currentRole!.rights.add(right);
            }
        } else {
            if (right.getPhantom() &&
                !right[CrudRights.Read].getValue() &&
                !right[CrudRights.Create].getValue() &&
                !right[CrudRights.Update].getValue() &&
                !right[CrudRights.Delete].getValue()) {
                this.currentRole!.rights.remove(right);
            }
        }
     }

     private findUserRight = (right: any) => {
         return this.currentRole!.rights.find((item: any) => {
             return item.securedResource.name.getValue() === right.securedResource.name.getValue();
         });
     }

     @action
     public handleToggleExpand = () => {
         this.isExpand = !this.isExpand;

         for (let func of this.changeExpandFunctions) {
             func(this.isExpand);
         }
     }

     @action
     public handleCheckSecurityLabel = (label: SecurityLabel) => {
         const name = label.name.getValue();

         const results = this.currentRole!.securityLabels.items.filter((item: SecurityLabelRole) =>
             item.name.getValue() === name
         );

         if (results.length > 0) {
            if (!this.removedLabels[name]) {
                this.removedLabels[name] = [];
            }

            for (let securityLabel of results) {
                this.handleRemoveAttributes(securityLabel);
            }
         } else {
             let params: any = label.serialize();

             params.attributes = [];

             this.handleAddAttributes(label, params);
         }
     }

     @action
    public handleAddAttributes = (label: SecurityLabel, labelParams?: {}) => {
         const name = label.name.getValue();
         const prevLabel: SecurityLabelRole[] = this.removedLabels[name];

         if (prevLabel && prevLabel.length > 0) {
             for (let l of prevLabel) {
                 l.revert();
             }

             this.currentRole!.securityLabels.addItems(prevLabel);
             this.removedLabels[name] = [];
         } else {
             this.currentRole!.securityLabels.add(new SecurityLabelRole(labelParams || label.serialize()));
         }
     }

     @action
     public handleRemoveAttributes = (label: SecurityLabelRole) => {
         if (!label.getPhantom()) {
             if (!this.removedLabels[label.name.getValue()]) {
                 this.removedLabels[label.name.getValue()] = [];
             }

             this.removedLabels[label.name.getValue()].push(label);
         }

         this.currentRole!.securityLabels.remove(label);
     }

     @action
     public handleSave = (): Promise<void> => {
         let me = this;
         let roleName: string = this.currentRole!.name.getValue();
         let promiseRole: Promise<any>;
         let uePromises: Array<Promise<any>>;
         let saveSuccessTitle = i18n.t('admin.role>roleSaveSuccessTitle');
         let saveFailureText = i18n.t('admin.role>roleSaveFailureText');
         let saveFailureTitle = i18n.t('admin.role>roleSaveFailureTitle');

         this.setSaving(true);

         this.currentRole!.validate();

         if (this.currentRole!.lastValidationResult.size > 0) {
             this.handleRequestFailed(saveFailureText, saveFailureTitle);

             return Promise.reject();
         }

         uePromises = this.UESaveAll(roleName);

         if (this.currentRole!.getPhantom()) {
             promiseRole = RoleService.createRole(this.currentRole!);

             return promiseRole
                 .then(
                     function () {
                         return Promise.all(uePromises);
                     }
                 )
                 .then(
                     function () {
                         me.currentRole!.commit();
                         me.handleRequestSuccess('', saveSuccessTitle);
                     }
                 )
                 .catch(this.handleRequestFailed.bind(this, saveFailureText, saveFailureTitle));
         } else {
             promiseRole = RoleService.updateRole(this.currentRole!);

             return Promise.all([promiseRole].concat(uePromises)).then(
                 function () {
                     me.currentRole!.commit();
                     me.handleRequestSuccess('', saveSuccessTitle);
                 },
                 this.handleRequestFailed.bind(this, saveFailureText, saveFailureTitle)
             );
         }
     }

     @action
     public handleDestroy = (): Promise<void> => {
         let destroySuccessTitle = i18n.t('admin.role>roleDestroySuccessTitle'),
             destroyFailureText = i18n.t('admin.role>roleDestroyFailureText'),
             destroyFailureTitle = i18n.t('admin.role>roleDestroyFailureTitle');

         this.setSaving(true);

         if (this.currentRole!.getPhantom()) {
             this.updateCurrentRole(null);

             return this.handleRequestSuccess('', destroySuccessTitle);
         }

         return RoleService.deleteRole(this.currentRole!.name.getValue()).then(
             () => {

                 this.updateCurrentRole(null);
                 this.handleRequestSuccess('', destroySuccessTitle);
             },
             this.handleRequestFailed.bind(this, destroyFailureText, destroyFailureTitle)
         );
     }

     private handleRequestSuccess = (text: string, title: string) => {
         this.setSaving(false);
         this.removedLabels = {};

         Dialog.showMessage(text, title);

         return Promise.resolve();
     }

     private handleRequestFailed = (text: string, title: string, er?: Error) => {
         let message: string;

         this.setSaving(false);

         if (er instanceof Array) {
             message = er.map((error) => error.userMessageDetails || error.userMessage).join(' ');
         } else {
             message = text;
         }

         Dialog.showError(message, title);
     }

     @action
     public setSaving = (isSaving: boolean) => {
         this.isSaving = isSaving;
     }

     @action
     public setLoading = (isLoading: boolean) => {
         this.isLoading = isLoading;
     }

     /**
      * Installation method
      * @param tabKey
      */
    public setActiveRightTab (tabKey: RightTabEnum) {
        this.activeRightTab = tabKey;
    }

    private updateRightsBySecuredResources (currentRole: Role) {
        let securedResourcesMap: IStringKeyMap<ResourceTreeItem> = keyBy(this.securedResourcesFlat,
            (res: ResourceTreeItem) => res.name
        );

        currentRole.rights.getRange().forEach((right) => {
            if (right.securedResource) {
                this.rightsBySecuredResourceMap.set(right.securedResource.name.getValue(), right);
            }
        });

        this.securedResourcesFlat.forEach((securedResource: ResourceTreeItem) => {
            const securedResourceName = securedResource.name;

            let right = this.rightsBySecuredResourceMap.get(securedResourceName);

            if (!right) {
                securedResource = securedResourcesMap[securedResourceName];
                right = new Right({
                    securedResource: securedResource,
                    [CrudRights.Read]: false,
                    [CrudRights.Create]: false,
                    [CrudRights.Update]: false,
                    [CrudRights.Delete]: false
                });

                right.commit();

                this.rightsBySecuredResourceMap.set(securedResource.name, right);
            }
        });
    }

    private updateRightCoherenceStore () {
        this.rightCoherenceStore = new RightCoherenceStore(this.currentRole,
            this.rightsBySecuredResourceMap,
            this.securedResourcesFlat);
    }
}
