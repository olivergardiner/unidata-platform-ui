﻿/**
 * Store the role page
 *
 * @author Ivan Marshalkin
 * @date 2019-05-22
 */

import {action, observable, observe} from 'mobx';
import {RoleListStore} from './RoleListStore';
import {EditorStore} from './EditorStore';
import {SettingsStore} from './SettingsStore';
import {Pages} from '@unidata/types';
import {Role} from '../../../model/user/Role';

export class RolePageStore {

    @observable public currentPage: Pages = Pages.Editor;

    public menuStore: RoleListStore = new RoleListStore();
    public editorStore: EditorStore = new EditorStore();
    public settingsStore: SettingsStore = new SettingsStore();

    constructor () {
        let me = this;

        const menuDisposer = observe(this.menuStore, 'selectedRole', (change) => {
            if (change.newValue) {
                if (change.newValue.phantom) {
                    me.editorStore.createNewRole();
                    me.currentPage = Pages.Editor;
                } else {
                    me.editorStore.loadRole(change.newValue.name.getValue());
                }
            }

            return change;
        });

        const editorDisposer = observe(this.editorStore, 'currentRole', (change) => {
            if (change.newValue && me.menuStore.selectedRole) {
                for (let property of ['displayName']) {
                    (change.newValue as any)[property].bind((me.menuStore.selectedRole as any)[property]);
                }
            }

            return change;
        });
    }

    public get props () {
        let methods = Object.assign(
            {
                handleEditorSave: this.handleEditorSave,
                handleListClick: this.handleListClick,
                openSettingsPage: this.openSettingsPage,
                handleAddNewItemClick: this.handleAddNewItemClick,
                handleItemDestroy: this.handleItemDestroy,
                openEditorPage: this.openEditorPage,
                isNeedConfirmToSwitchToNew: this.isNeedConfirmToSwitchToNew,
                isNeedConfirmToSwitch: this.isNeedConfirmToSwitch
            }
        );

        for (let key of Object.keys(methods)) {
            methods[key] = methods[key].bind(this);
        }

        return Object.assign(this.stores, methods);
    }

    public get stores () {
        return {
            menuStore: this.menuStore,
            editorStore: this.editorStore,
            settingsStore: this.settingsStore
        };
    }

    @action
    public handleListClick = (role: Role) => {
        return this.menuStore.setSelectedRole(role);
    }

    @action
    public handleEditorSave = () => {
        this.editorStore.setLoading(true);

        this.editorStore.handleSave()
            .then(() => {
                return this.menuStore.loadRoleList();
            })
            .then(() => {
                return this.editorStore.rightCoherenceStore.checkRightCoherence();
            })
            .finally(() => {
                this.editorStore.setLoading(false);
            });
    };

    @action
    public handleAddNewItemClick = () => {
        return this.menuStore.createRole();
    }

    @action
    public handleItemDestroy = () => {
        return this.editorStore.handleDestroy().then(() => {
            this.menuStore.setSelectedRole(null);
            this.menuStore.loadRoleList();
        });
    }

    @action
    public openSettingsPage = () => {
        this.currentPage = Pages.Settings;
    }

    @action
    public openEditorPage = () => {
        this.currentPage = Pages.Editor;
    }

    public isNeedConfirmToSwitchToNew = (): boolean => {
        return Boolean(this.editorStore.canSave);
    }

    public isNeedConfirmToSwitch = (target: Role): boolean => {
        return Boolean(this.editorStore.canSave) &&
            this.menuStore.selectedRole !== null &&
            (this.menuStore.selectedRole.modelId !== target.modelId);
    }
}
