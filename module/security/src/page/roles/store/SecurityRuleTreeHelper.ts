/**
 * empty comment line Viktor Magarlamov
 *
 * @author: Viktor Magarlamov
 * @date: 2019-04-15
 */

import {i18n} from '@unidata/core-app';
import chain from 'lodash/chain';
import {observable} from 'mobx';
import {CrudRights} from '../../../types/CrudRights';
import {ResourceTreeItem} from '../../../types/ResourceTreeItem';
import {IStringKeyMap} from '@unidata/core';
import {Resource} from '../../../model/user/Resource';

const UNPERMITTED_ACTIONS: {[key: string]: CrudRights[]} = {
    USER_MANAGEMENT: [
        CrudRights.Delete
    ],
    PLATFORM_PARAMETERS_MANAGEMENT: [
        CrudRights.Create,
        CrudRights.Delete
    ],
    AUDIT_ACCESS: [
        CrudRights.Create,
        CrudRights.Update,
        CrudRights.Delete
    ],
    EXECUTE_DATA_OPERATIONS: [
        CrudRights.Create,
        CrudRights.Update,
        CrudRights.Delete
    ],
    BULK_OPERATIONS_OPERATOR: [
        CrudRights.Create,
        CrudRights.Update,
        CrudRights.Delete
    ],
    DATA_HISTORY_EDITOR: [
        CrudRights.Create,
        CrudRights.Delete
    ],
    BULK_WIPE_REMOVE_RECORDS: [
        CrudRights.Create,
        CrudRights.Update,
        CrudRights.Delete
    ]
};

const CATEGORY_UNPERMITTED_ACTIONS: {[key: string]: CrudRights[]} = {
    CLASSIFIER: [
        CrudRights.Create,
        CrudRights.Update,
        CrudRights.Delete
    ]
};

export function buildTreeNodeList (resourceTree: ResourceTreeItem []) {
    let nodeList: ResourceTreeItem [] = [];

    const collectRecursively = (resourceTree: ResourceTreeItem [] | null, result: ResourceTreeItem []) => {
        if (!resourceTree) {
            return;
        }

        resourceTree.forEach(function (resource: ResourceTreeItem) {
            result.push(resource);

            collectRecursively(resource.children, result);
        });
    };

    collectRecursively(resourceTree, nodeList);

    return nodeList;
}

/**
 * @deprecated
 * @param securityResources
 */
export function buildTreeNode (securityResources: Resource[]): ResourceTreeItem[] {
    const calculateUnpermittedActions = (item: ResourceTreeItem) => {
        let byName =  UNPERMITTED_ACTIONS[item.name] || [];
        let byCategory = CATEGORY_UNPERMITTED_ACTIONS[item.category] || [];
        let unpermitted = chain([] as CrudRights []).concat(byName, byCategory).uniq().value();

        return unpermitted;
    };

    const convertTreeNodes = (items: ResourceTreeItem [], parentNode: ResourceTreeItem | null): ResourceTreeItem[] => {
        let result: ResourceTreeItem[] = [];

        for (let item of items) {
            let treeNode: ResourceTreeItem = {
                name: item.name,
                category: item.category,
                type: item.type,
                displayName: item.displayName,
                expand: false,
                roleRight: null,
                unpermittedActions: calculateUnpermittedActions(item),
                nodeType: 'SECURED_RESOURCE_NODE',
                parentNode: observable.object(parentNode, {}, {deep: false}),
                children: null
            };

            treeNode.children = (item.children ? convertTreeNodes(item.children, treeNode) : null);

            result.push(treeNode);
        }

        return result.sort((n1: ResourceTreeItem, n2: ResourceTreeItem) => n1.displayName.localeCompare(n2.displayName));
    };

    const groupByCategory = (result: any, item: Resource) => {
        let category: Partial<ResourceTreeItem> = result[item.category.getValue()];

        if (!category) {
            category = {
                name: item.category.getValue(),
                displayName: i18n.t(`admin.role>ruleCategories>${item.category.getValue()}`),
                expand: false,
                nodeType: 'GROUP_NODE',
                children: []
            };
        }

        const treeItem: Partial<ResourceTreeItem> = {
            name: item.name.getValue(),
            category: item.category.getValue(),
            type: item.type.getValue(),
            displayName: item.displayName.getValue()
        };

        category.children = [...category.children as any, treeItem];
        category.children = convertTreeNodes(category.children, null);

        result[item.category.getValue()] = category;

        return result;
    };

    const categories: IStringKeyMap<ResourceTreeItem> = securityResources.reduce(groupByCategory, {});

    return Object.values(categories);
}
