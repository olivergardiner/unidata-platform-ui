/**
 * Store for working with rights consistency highlighting
 *
 * @author: Denis Makarov
 * @date: 2020-03-05
 */

import {action} from 'mobx';
import difference from 'lodash/difference';
import union from 'lodash/union';
import {Role} from '../../../model/user/Role';
import {Right} from '../../../model/user/Right';
import {Nullable} from '@unidata/core';
import {ResourceTreeItem} from '../../../types/ResourceTreeItem';
import {
    ConnectedVertexes,
    Edge,
    EdgeDirection,
    MetaDependencyGraph,
    RightCoherenceWarning,
    Vertex
} from '@unidata/meta';
import {Dialog, i18n} from '@unidata/core-app';
import {MetaDependencyGraphService} from '@unidata/meta';
import {RelType} from '@unidata/types';
import {MetaType} from '@unidata/meta';

type EntityLink = {from: Nullable<string>; to: Nullable<string>}

export class RightCoherenceStore {
    public allSecurityResources: ResourceTreeItem[] = [];
    public rightCoherenceWarningsMap: Map<string, Map<string, RightCoherenceWarning>> = new Map();
    public metaDependencyGraph: MetaDependencyGraph;
    public rightsBySecuredResourceMap: Map<string, Right>;
    public securedResourcesNameMappings: Map<string, string> = new Map();
    private currentRole: Role | null;

    constructor (
        currentRole: Role | null,
        rightsBySecuredResourceMap: Map<string, Right>,
        securityResources: ResourceTreeItem [],
    ) {
        this.setCurrentRole(currentRole);
        this.setRightsBySecuredResourceMap(rightsBySecuredResourceMap);
        this.setAllSecurityResources(securityResources);
        this.securedResourcesNameMappings = this.buildNameMappings(securityResources);
    }

    public static relationsWithLocales: {[key: string]: {locale: string}} = {
        [RelType.References]: {locale: 'relation>referenceRelation'},
        [RelType.Contains]: {locale: 'relation>containsRelation'},
        [RelType.ManyToMany]: {locale: 'relation>manyToManyRelation'}
    };

    public static getRelationTypeDisplayName (relationType: string) {
        let relTypes = Object.values(RelType);
        let found;

        found = relTypes.find((item: RelType) => {
            return item === relationType;
        });

        if (!found) {
            return relationType;
        }

        const locale = RightCoherenceStore.relationsWithLocales[found].locale;

        return i18n.t(locale);
    }

    private buildNameMappings (securedResourcesFlat: ResourceTreeItem[]) {
        return securedResourcesFlat.reduce(
            (result, item) => result.set(item.name, item.displayName), new Map<string, string>()
        );
    }

    @action
    public setAllSecurityResources (securityResources: ResourceTreeItem []) {
        this.allSecurityResources = securityResources;
    }

    @action
    public setCurrentRole (role: Role | null) {
        this.currentRole = role;
    }

    @action
    public setRightsBySecuredResourceMap (rightsBySecuredResourceMap: Map<string, Right>) {
        this.rightsBySecuredResourceMap = rightsBySecuredResourceMap;
    }

    private calculateAllRightsCoherence () {
        this.rightCoherenceWarningsMap.clear();

        this.allSecurityResources.forEach((resource) => {
            this.calculateRightCoherence(resource);
        });
    }

    private buildCoherenceWarnings (): string [] {
        const messages: string [] = [];

        this.rightCoherenceWarningsMap.forEach(
            (v) => {
                v.forEach((warning) => {
                        messages.push(warning.message);
                    }
                );
            });

        return messages;
    }

    private calculateRightCoherence (resource: ResourceTreeItem) {
        const securedResourceName = resource.name;
        const securedResourceCategory = resource.category;

        if (securedResourceCategory !== 'META_MODEL') {
            return;
        }

        const rightWarnings: any = this.findRightWarningsByEntityName(securedResourceName);

        this.updateRightWarningMap(rightWarnings);
    }

    private updateRightWarningMap (rightWarnings: RightCoherenceWarning []) {
        rightWarnings.forEach((rightWarning) => {
            const rightWarningMap = this.rightCoherenceWarningsMap,
                rights = rightWarning.rights,
                securedResourceName = rightWarning.securedResource,
                relatedSecuredResourceName = rightWarning.relatedSecuredResource,
                securedResourceMap = rightWarningMap.get(securedResourceName);

            if (!securedResourceMap) {
                rightWarningMap.set(securedResourceName, new Map());
            }

            if (securedResourceMap) {
                if (rights) {
                    securedResourceMap.set(relatedSecuredResourceName, rightWarning);
                } else {
                    securedResourceMap.delete(relatedSecuredResourceName);
                }
            }
        });
    }

    /**
     * Loading a dependency graph for building consistency warnings
     */
    public loadDependencyGraph () {
        const forTypes = [
            MetaType.LOOKUP,
            MetaType.ENTITY,
            MetaType.RELATION,
            MetaType.NESTED_ENTITY
        ];

        const skipTypes = [
            MetaType.NESTED_ENTITY
        ];

        return new Promise(resolve => {
            MetaDependencyGraphService.getMetaDependencyGraph(forTypes, skipTypes)
                .then(action((metaDependencyGraph: MetaDependencyGraph) => {
                        this.setMetaDependencyGraph(metaDependencyGraph);

                        return resolve(metaDependencyGraph);
                    }),
                    () => {
                        Dialog.showError(i18n.t('admin.role.coherence>cannotCheckRightsCoherence'));
                    });
        });
    }

    /**
     * Getting all consistency warnings by registry name
     */
    private findRightWarningsByEntityName (entityName: string) {
        let rightWarnings: RightCoherenceWarning [] = [],
            vertex: Vertex | null,
            vertexType: string,
            inboundVertexes: ConnectedVertexes,
            outboundVertexes: ConnectedVertexes;

        vertex = this.findMetaModelVertex(entityName) || null;

        if (!vertex) {
            return rightWarnings;
        }

        vertexType = vertex.type.getValue();

        inboundVertexes = {
            relation: [],
            lookup: []
        };

        outboundVertexes = {
            relation: [],
            lookup: []
        };

        if (vertexType === MetaType.ENTITY) {
            inboundVertexes.relation = this.findInboundVertexesByRelation(entityName);
            outboundVertexes.relation = this.findOutboundVertexesByRelation(entityName);
            outboundVertexes.lookup = this.findOutboundVertexesByLookup(entityName);

            rightWarnings = rightWarnings.concat(
                inboundVertexes.relation.map(this.calcRightWarningsByRelation.bind(this, EdgeDirection.INBOUND, entityName))
            );
            rightWarnings = rightWarnings.concat(
                outboundVertexes.relation.map(this.calcRightWarningsByRelation.bind(this, EdgeDirection.OUTBOUND, entityName))
            );
            rightWarnings = rightWarnings.concat(
                outboundVertexes.lookup.map(this.calcRightWarningsByLookup.bind(this, EdgeDirection.OUTBOUND, entityName))
            );
        } else if (vertexType === MetaType.LOOKUP) {
            inboundVertexes.lookup = this.findInboundVertexesByLookup(entityName);
            outboundVertexes.lookup = this.findOutboundVertexesByLookup(entityName);

            rightWarnings = rightWarnings.concat(
                inboundVertexes.lookup.map(this.calcRightWarningsByLookup.bind(this, EdgeDirection.INBOUND, entityName))
            );
            rightWarnings = rightWarnings.concat(
                outboundVertexes.lookup.map(this.calcRightWarningsByLookup.bind(this, EdgeDirection.OUTBOUND, entityName))
            );
        }

        rightWarnings = rightWarnings.filter((rightWarning: any) => {
            return Boolean(rightWarning); // filtering out null
        });

        return rightWarnings;
    }

    /**
     * Find by vertex name of type ENTITY or LOOKUP in metaDependencyGraph
     */
    private findMetaModelVertex (name: string) {
        const metaDependencyGraph = this.metaDependencyGraph,
            vertexes = metaDependencyGraph.vertexes.items;

        let found: Vertex | undefined,
            index: number;

        index = vertexes.findIndex((vertex: Vertex) => {
            const id = vertex.id.getValue();
            const type = vertex.type.getValue();

            return id === name && (type === MetaType.ENTITY || type === MetaType.LOOKUP);
        });

        if (index > -1) {
            found = vertexes[index];
        }

        return found;
    }

    /**
     * Calculate the inconsistency of rights for the corresponding relationship (specified by the relation Vertex)
     */
    private calcRightWarningsByRelation (direction: EdgeDirection, entityName: string, relationVertex: Vertex) {
        let rightWarning: Nullable<RightCoherenceWarning>,
            entityNamePair: EntityLink,
            propName: string;

        entityNamePair = {
            from: null,
            to: null
        };

        if (direction !== EdgeDirection.INBOUND && direction !== EdgeDirection.OUTBOUND) {
            throw new Error('Edge direction is not defined');
        }

        if (direction === EdgeDirection.INBOUND) {
            propName = 'FROM';
            entityNamePair.from = relationVertex.getCustomPropValue(propName);
            entityNamePair.to = entityName;
        } else {
            propName = 'TO';
            entityNamePair.from = entityName;
            entityNamePair.to = relationVertex.getCustomPropValue(propName);
        }

        if (!entityNamePair.to || !entityNamePair.from) {
            return null;
        }

        rightWarning = this.compareRelationRights(entityNamePair.from, entityNamePair.to, relationVertex);

        return rightWarning;
    }

    /**
     * Calculate the inconsistency of permissions for the corresponding link (set by lookupVertex)
     */
    private calcRightWarningsByLookup (direction: EdgeDirection, entityName: string, lookupVertex: Vertex) {
        let lookupVertexId = lookupVertex.id.getValue(),
            rightWarning: Nullable<RightCoherenceWarning>,
            entityNamePair: EntityLink;

        entityNamePair = {
            from: null,
            to: null
        };

        if (direction !== EdgeDirection.INBOUND && direction !== EdgeDirection.OUTBOUND) {
            throw new Error('Edge direction is not defined');
        }

        if (direction === EdgeDirection.INBOUND) {
            entityNamePair.from = lookupVertexId;
            entityNamePair.to = entityName;
        } else {
            entityNamePair.from = entityName;
            entityNamePair.to = lookupVertexId;
        }

        rightWarning = this.compareLookupRights(entityNamePair.from, entityNamePair.to);

        return rightWarning;
    }

    /**
     * Compare the rights of related directories
     */
    private compareRelationRights (fromEntityName: string, toEntityName: string, relationVertex: Vertex) {
        let fromRights: Right | undefined = this.findRight(fromEntityName),
            toRights: Right | undefined = this.findRight(toEntityName),
            relationName = relationVertex.displayName.getValue(),
            relationType = relationVertex.getCustomPropValue('REL_TYPE'),
            relationTypeDisplayName: string = '',
            rightListSubtraction: string [] = [],
            fromEntityVertex: Vertex | undefined,
            fromEntityDisplayName: string = '',
            rightWarning: RightCoherenceWarning | null,
            rightDiff1: string [],
            rightDiff2: string [];

        if (relationType) {
            relationTypeDisplayName = RightCoherenceStore.getRelationTypeDisplayName(relationType);
        }

        rightWarning = null;

        if (!fromRights || !toRights) {
            return rightWarning;
        }

        if (fromEntityName) {
            fromEntityVertex = this.findMetaModelVertex(fromEntityName);
        }

        if (fromEntityVertex) {
            fromEntityDisplayName = fromEntityVertex.displayName.getValue() ||
                this.securedResourcesNameMappings.get(fromEntityVertex.id.getValue()) || '';
        }

        if (relationType === RelType.ManyToMany || relationType === RelType.References) {
            if (fromRights.read.getValue() && !toRights.read.getValue()) {
                rightWarning = {
                    securedResource: toEntityName,
                    rights: 'read',
                    relatedSecuredResource: fromEntityName,
                    message: i18n.t('admin.role.coherence>groupsNotApprovedWithEntity', {name: fromEntityDisplayName}) + ' ' +
                        i18n.t('admin.role.coherence>isRelationWithName', {name: relationName}) + ' ' +
                        i18n.t('admin.role.coherence>typeWithName', {name: relationTypeDisplayName})

                };
            }
        } else if (relationType === RelType.Contains) {
            if (Right.anyRightExists(fromRights) && !Right.rightsEqual(fromRights, toRights)) {
                rightDiff1 = difference(fromRights.rightsList, toRights.rightsList);
                rightDiff2 = difference(toRights.rightsList, fromRights.rightsList);
                rightListSubtraction = union(rightDiff1, rightDiff2);
                rightWarning = {
                    securedResource: toEntityName,
                    rights: rightListSubtraction,
                    relatedSecuredResource: fromEntityName,
                    message: i18n.t('admin.role.coherence>groupsNotApprovedWithEntiry', {name: fromEntityDisplayName}) + ' ' +
                        i18n.t('admin.role.coherence>isRelationWithName', {name: relationName}) + ' ' +
                        i18n.t('admin.role.coherence>typeWithName', {name: relationTypeDisplayName})
                };
            }
        }

        return rightWarning;
    }

    private findInboundVertexesByRelation (toId: string) {
        return this.findVertexesByRelation(toId, EdgeDirection.INBOUND);
    }

    private findOutboundVertexesByRelation (fromId: string) {
        return this.findVertexesByRelation(fromId, EdgeDirection.OUTBOUND);
    }

    private findInboundVertexesByLookup (toId: string) {
        return this.findVertexesByLookup(toId, EdgeDirection.INBOUND);
    }

    private findOutboundVertexesByLookup (fromId: string) {
        return this.findVertexesByLookup(fromId, EdgeDirection.OUTBOUND);
    }

    /**
     * Compare the rights of a registry / directory linked to another directory by clicking the link
     *
     */
    private compareLookupRights (fromEntityName: string, toEntityName: string) {
        let fromRights: Right | undefined = this.findRight(fromEntityName),
            toRights: Right | undefined = this.findRight(toEntityName),
            fromEntityVertex: Vertex | undefined,
            fromEntityDisplayName: string | undefined,
            toEntityDisplayName: string = '',
            rightWarning: RightCoherenceWarning | null;

        if (fromEntityName) {
            fromEntityVertex = this.findMetaModelVertex(fromEntityName);
        }

        // getting displayName for source
        if (fromEntityVertex) {
            fromEntityDisplayName = fromEntityVertex.displayName.getValue() ||
                this.securedResourcesNameMappings.get(fromEntityVertex.id.getValue()) || '';
        }

        // getting displayName for target
        if (toEntityName) {
            toEntityDisplayName = this.securedResourcesNameMappings.get(toEntityName) || '';
        }

        rightWarning = null;

        if (!fromRights || !toRights) {
            return rightWarning;
        }

        if (fromRights.read.getValue() && !toRights.read.getValue()) {
            rightWarning = {
                securedResource: toEntityName,
                rights: 'read',
                relatedSecuredResource: fromEntityName,
                message: i18n.t('admin.role.coherence>groupsNotApprovedWithEntity', {name: fromEntityDisplayName}) + ' ' +
                    i18n.t('admin.role.coherence>lookupLink', {name: toEntityDisplayName})
            };
        }

        return rightWarning;
    }

    private findRight (resourceName: string) {
        return this.rightsBySecuredResourceMap.get(resourceName);
    }

    private findVertexesByRelation (entityName: string, direction: EdgeDirection) {
        let metaDependencyGraph = this.metaDependencyGraph,
            vertexes = metaDependencyGraph.vertexes.getRange(),
            vertexesByRelation: Vertex [],
            propName: string;

        if (direction !== EdgeDirection.INBOUND && direction !== EdgeDirection.OUTBOUND) {
            throw new Error('Edge direction is not defined');
        }

        propName = (direction === EdgeDirection.INBOUND) ? 'TO' : 'FROM';

        vertexesByRelation = vertexes.filter((vertex: Vertex) => {
            return vertex.type.getValue() === MetaType.RELATION && vertex.getCustomPropValue(propName) === entityName;
        }, this);

        return vertexesByRelation;
    }

    private findVertexesByLookup (entityName: string, direction: EdgeDirection) {
        let metaDependencyGraph = this.metaDependencyGraph,
            edges = metaDependencyGraph.edges.getRange(),
            edgesByLookup: Edge [],
            vertexesByLookup: Vertex [];

        if (direction !== EdgeDirection.INBOUND && direction !== EdgeDirection.OUTBOUND) {
            throw new Error('Edge direction is not defined');
        }

        edgesByLookup = edges.filter((edge: Edge) => {
            let fromVertex = edge.from,
                toVertex = edge.to,
                fromVertexType = fromVertex.type.getValue(),
                toVertexType = toVertex.type.getValue(),
                toVertexId = toVertex.id.getValue(),
                fromVertexId = fromVertex.id.getValue(),
                typesCondition,
                idCondition;

            typesCondition = (fromVertexType === MetaType.ENTITY || fromVertexType === MetaType.LOOKUP) &&
                (toVertexType === MetaType.LOOKUP);

            if (direction === EdgeDirection.INBOUND) {
                idCondition = toVertexId === entityName;
            } else {
                idCondition = fromVertexId === entityName;
            }

            return idCondition && typesCondition;
        }, this);

        vertexesByLookup = edgesByLookup.map((edge: Edge) => {
            return direction === EdgeDirection.INBOUND ? edge.from : edge.to;
        });

        return vertexesByLookup;
    }

    public checkRightCoherence (): Promise<void> {
        return new Promise((resolve) => {
            this.loadDependencyGraph().then(() => {
                this.calculateAllRightsCoherence();
                const messages: string [] = this.buildCoherenceWarnings();

                if (messages.length > 0) {
                    Dialog.showWarning(messages, '', {}, true);
                }

                resolve();
            });
        });
    }

    @action
    private setMetaDependencyGraph (metaDependencyGraph: MetaDependencyGraph) {
        this.metaDependencyGraph = metaDependencyGraph;
    }
}
