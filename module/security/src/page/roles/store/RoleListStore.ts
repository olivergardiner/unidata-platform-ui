/**
 * Store of the list of roles
 *
 * @author Ivan Marshalkin
 * @date 2019-05-22
 */

import {action, observable} from 'mobx';
import {ModelCollection} from '@unidata/core';
import {Role} from '../../../model/user/Role';
import {RoleService} from '../../../service/RoleService';

export class RoleListStore {
    @observable public roleList: ModelCollection<Role> = ModelCollection.create(Role);

    @observable
    public selectedRole: Role | null;

    constructor () {
        this.loadRoleList();
    }

    public get items () {
        return this.roleList.items || [];
    }

    @action
    public createRole = () => {
        let role: (Role | undefined) = this.roleList.items.find((role) => role.phantom);

        if (!role) {
            role = new Role({});
            this.roleList.add(role);
        }

        this.setSelectedRole(role);
    }

    @action
    public setSelectedRole (role: Role | null) {
        this.selectedRole = role;
    }

    @action
    public loadRoleList () {
        let self = this;
        let currentRoleName = self.selectedRole ? self.selectedRole.name.getValue() : null;

        self.roleList.removeAll();

        return RoleService.getRoleList().then(
            action((result: Role[]) => {
                self.roleList.replaceAll(result);

                if (result && currentRoleName) {
                    const role: any[] = result.filter((item) => item.name.getValue() === currentRoleName);

                    if (role.length > 0) {
                        self.setSelectedRole(role[0]);
                    }
                }
            }),
            function (result) {
                throw new Error('Error during fetching security roles');
            });
    }
}
