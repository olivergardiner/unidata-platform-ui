/**
 * Store of the additional parameters editor
 *
 * @author Ivan Marshalkin
 * @date 2019-05-22
 */

import {action, computed, observable} from 'mobx';
import {IServerError, ModelCollection, OperationError} from '@unidata/core';
import {RoleProperty} from '../../../model/user/RoleProperty';
import {Dialog, i18n} from '@unidata/core-app';
import {RolePropertyService} from '../../../service/RolePropertyService';

export class SettingsStore {
    @observable public isSaving: boolean = false;
    @observable public items: ModelCollection<RoleProperty> = ModelCollection.create(RoleProperty);
    @observable private deletedIds: number[] = [];

    constructor () {
        this.loadRoleProperties();
    }

    private loadRoleProperties () {
        RolePropertyService.getRolePropertyList().then(action((response: any[]) => {
            this.items.replaceAll(response);
        }));
    }

    @action
    public handleAddItem = () => {
        this.items.add(new RoleProperty({}));
    }

    @action
    public handleRemoveItem = (item: RoleProperty) => {
        const index: number = this.deletedIds.indexOf(item.modelId);

        if (index > -1) {
            this.deletedIds = this.deletedIds.filter((id) => id !== item.modelId);
        } else {
           this.deletedIds.push(item.modelId);
        }
    }

    @action
    public handleRemoveAll = () => {
        if (this.isAllRemoved()) {
            this.deletedIds = [];
        } else {
            for (let item of this.items.items) {
                if (!this.isItemRemoved(item)) {
                    this.deletedIds.push(item.modelId);
                }
            }
        }
    }

    @action
    public handleItemChange = (model: RoleProperty, name: string, value: any) => {
        (model as any)[name].setValue(value);
    }

    @computed
    public get canSave () {
        if (this.isSaving) {
            return false;
        }

        return this.deletedIds.length > 0 || this.items.items.slice().some((item: RoleProperty) => item.getDirty());
    }

    public handleSave = (): Promise<void> => {
        this.setSaving(true);

        const promisses = this.items.items.filter((item) => {
            return item.getDirty() || this.isItemRemoved(item);
        }).map((item) => {
            if (this.isItemRemoved(item)) {
                if (item.getPhantom()) {
                    return null;
                }

                return RolePropertyService.deleteRoleProperty(item.id.getValue());
            }

            if (item.getPhantom()) {
                return RolePropertyService.createRoleProperty(item);
            }

            return RolePropertyService.updateRoleProperty(item);
        });

        return Promise.all(promisses).then(
            this.handleSaveSettingsSuccess,
            this.handleSaveSettingsFailed
        );
    }

    public isItemRemoved = (item: RoleProperty): boolean => {
        return this.deletedIds.slice().includes(item.modelId);
    }

    public isAllRemoved = (): boolean => {
        return (this.items.items.length === this.deletedIds.length);
    }

    @action
    private handleSaveSettingsSuccess = () => {
        this.deletedIds = [];
        this.loadRoleProperties();
        this.setSaving(false);

        Dialog.showMessage('', i18n.t('admin.role.settings>saveSuccessText'));
    }

    private handleSaveSettingsFailed = (er?: Error) => {
        let message: string;

        this.setSaving(false);

        if (er instanceof OperationError) {
            const errors: IServerError[] = er.getServerError();

            message = errors.map((error: IServerError) => error.userMessageDetails || error.userMessage).join(' ');
        } else {
            message = i18n.t('admin.role.settings>saveFailureText');
        }

        Dialog.showError(message, i18n.t('admin.role.settings>saveFailureTitle'));
    }

    @action
    private setSaving = (isSaving: boolean) => {
        this.isSaving = isSaving;
    }

    public get actualPropertyList () {
        const me = this;

        return this.items.items.filter(function (item) {
            return me.isItemRemoved(item) === false;
        });
    }
}
