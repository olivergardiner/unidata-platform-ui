/**
 * Editor for a specific user role.
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {i18n, Res, ResourceManager, Right} from '@unidata/core-app';
import {Button, CardPanel, Editor, INTENT, PageSpinner, PLACEMENT, TabBar, ButtonWithConfirmation, Col, Menu, Row} from '@unidata/uikit';
import {MainSettingsForm} from './MainSettingsForm';
import {AdditionalInfoForm} from './AdditionalInfoForm';
import {RulesTreeTable} from './rules_tree_table/RulesTreeTable';
import {SecurityLabelsList} from './security_labels_list/SecurityLabelsList';
import {RightTabEnum} from './type/RightTabEnum';
import {action} from 'mobx';
import {ueModuleManager, UeModuleReactComponent, UeModuleTypeComponent} from '@unidata/core';

interface IProps {
    readOnly?: boolean;
    readOnlyRoles: string[]; // Roles that the user can't edit
}

interface IInjectProps extends IProps {
    editorStore: any;
    handleEditorSave: () => Promise<void>;
    handleItemDestroy: () => Promise<void>;
}

@inject('editorStore', 'handleEditorSave', 'handleItemDestroy')
@observer
export class RoleEditor extends React.Component<IProps> {

    get injected () {
        return this.props as IInjectProps;
    }

    get store () {
        return this.injected.editorStore;
    }

    get roleProperties () {
        if (this.store.currentRole.properties) {
            return this.store.currentRole.properties.items;
        }

        return null;
    }

    get securityLabels () {
        return this.store.currentRole.securityLabels;
    }

    get securedResourcesTree () {
        if (this.store.securedResourcesTree) {
            return this.store.securedResourcesTree;
        }

        return null;
    }

    get canEdit () {
        if (this.props.readOnly) {
            return false;
        }

        if (this.store.currentRole && this.props.readOnlyRoles.indexOf(this.store.currentRole.name.getValue()) > -1) {
            return false;
        }

        if (this.store.currentRole && this.store.currentRole.phantom) {
            return ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.ROLE_MANAGEMENT], Right.CREATE);
        }

        return ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.ROLE_MANAGEMENT], Right.UPDATE);
    }

    get editorExtraButtons () {
        const canDeleteRole = (!this.props.readOnly &&
            ResourceManager.userHasAnyResourceRight([Res.ADMIN_SYSTEM_MANAGEMENT, Res.ROLE_MANAGEMENT], Right.DELETE)),
            roleName = this.store.currentRole ? this.store.currentRole.name.getValue() : '';


        return (
            <React.Fragment>
                {this.canEdit && (
                    <>
                        <Button
                            leftIcon={'save'}
                            intent={INTENT.PRIMARY}
                            isRound={true}
                            isDisabled={!this.store.canSave}
                            onClick={this.injected.handleEditorSave}
                        >
                            {i18n.t('admin.role>saveButtonText')}
                        </Button>
                        &nbsp;
                        &nbsp;
                    </>
                )}
                {canDeleteRole && (
                    <ButtonWithConfirmation
                        isRound={true}
                        leftIcon={'trash2'}
                        isDisabled={!this.store.canDestroy}
                        onConfirm={this.injected.handleItemDestroy}
                        title={i18n.t('admin.role>deleteRole')}
                        tooltipPlacement={PLACEMENT.BOTTOM_START}
                        confirmationMessage={i18n.t('admin.role>destroyConfirmation', {name: roleName})}
                    />
                )}
            </React.Fragment>
        );
    }

    isActiveTab (menuKey: string) {
        return this.store.activeRightTab === menuKey;
    }

    get entityRightTabVisible (): boolean {
        return this.store.activeRightTab === RightTabEnum.right;
    }

    get entityCollapseButtonText (): string {
        if (this.store.isExpand) {
            return i18n.t('admin.role>collapseAllRights');

        }

        return i18n.t('admin.role>expandAllRights');
    }

    /**
     * Tab click handler
     * @param e
     */
    @action
    handleTabBarClick = (e: any) => {
        this.store.setActiveRightTab(e.key);
    }

    render () {
        const readOnly = !this.canEdit;
        const ueListOfSecurityTabs = ueModuleManager.getModulesByType(UeModuleTypeComponent.ROLE_SECURITY);

        if (this.store.isLoading) {
           return <PageSpinner/>;
        }

        if (!this.store.currentRole) { return null; }

        return (
            <Editor
                title={i18n.t('admin.role>editorTitle')}
                groupSectionTitle={i18n.t('page.header>administration')}
                itemTitle={this.store.currentRole.displayName.getValue()}
                iconType='users'
                isWaiting={this.store.isSaving}
                extraButtons={this.editorExtraButtons}
            >
                <Row
                    gutter={12}
                >
                    <Col span={12}>
                        <CardPanel
                            title={i18n.t('admin.role>roleInformation')}
                            scrollY={true}
                        >
                            <div
                                style={
                                    {
                                        maxHeight: 420
                                    }
                                }
                            >
                                <MainSettingsForm
                                    item={this.store.currentRole}
                                    onChange={this.store.handleRoleChange}
                                    readOnly={readOnly}
                                />
                                <AdditionalInfoForm
                                    items={this.roleProperties}
                                    readOnly={readOnly}
                                    metaProperties={this.store.allRoleProperties}
                                />
                            </div>
                        </CardPanel>
                    </Col>

                    <Col span={12}>
                        <CardPanel
                            title={i18n.t('admin.role>securityLabel')}
                            scrollY={true}
                        >
                            <div
                                style={
                                    {
                                        maxHeight: 420,
                                        minHeight: 138,
                                        position: 'relative'
                                    }
                                }
                            >
                                <SecurityLabelsList
                                    roleSecurityLabels={this.securityLabels}
                                    allSecurityLabels={this.store.securityLabels}
                                    handleCheckSecurityLabel={this.store.handleCheckSecurityLabel}
                                    handleAddAttributes={this.store.handleAddAttributes}
                                    handleRemoveAttributes={this.store.handleRemoveAttributes}
                                    readOnly={readOnly}
                                />
                            </div>
                        </CardPanel>
                    </Col>
                </Row>

                <div style={{height: '12px'}}></div>

                <Row gutter={12}>
                    <Col span={24}>
                        <CardPanel
                            title={i18n.t('admin.role>accessPermission')}
                            noBodyPadding={true}
                        >

                            <TabBar
                                onClick={this.handleTabBarClick}
                                defaultSelectedKeys={this.store.activeRightTab}
                            >
                                <Menu.Item key={RightTabEnum.right}>
                                    {i18n.t('admin.role>resourceRightTabText')}
                                </Menu.Item>

                                {ueListOfSecurityTabs &&
                                ueListOfSecurityTabs.map((item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                                    let menuKey = item.default.meta.menuKey,
                                        menuText = item.default.meta.menuText as any;

                                    return (
                                        <Menu.Item key={menuKey}>
                                            {menuText()}
                                        </Menu.Item>
                                    );
                                })}

                                <div
                                    key={'rfloatcontainer'}
                                    style={{'float': 'right', marginRight: '4px'}}
                                >
                                    {this.entityRightTabVisible &&
                                    <Button
                                        intent={INTENT.PRIMARY}
                                        isMinimal={true}
                                        onClick={this.store.handleToggleExpand}
                                    >
                                        {this.entityCollapseButtonText}
                                    </Button>
                                    }
                                    {ueListOfSecurityTabs &&
                                    // eslint-disable-next-line max-len
                                    ueListOfSecurityTabs.map((item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                                        const headerItems = item.default.meta.headerItems,
                                            menuKey = item.default.meta.menuKey;

                                        if (headerItems) {
                                            if (this.isActiveTab(menuKey)) {
                                                return headerItems;
                                            }
                                        }

                                        return null;
                                    })}
                                </div>
                            </TabBar>

                            <div style={{padding: '12px 24px'}}>
                                {this.isActiveTab(RightTabEnum.right) &&
                                <RulesTreeTable
                                    readOnly={readOnly}
                                    nodes={this.securedResourcesTree}
                                />
                                }

                                {ueListOfSecurityTabs &&
                                ueListOfSecurityTabs.map((item: UeModuleReactComponent<UeModuleTypeComponent.ROLE_SECURITY>) => {
                                    let obj = item.default,
                                        menuKey = obj.meta.menuKey;

                                    if ((!obj.resolver || obj.resolver() === true) && obj.component) {
                                        const Component: any = obj.component;

                                        if (this.isActiveTab(menuKey)) {
                                            return (
                                                <Component
                                                    key={menuKey}
                                                    readOnly={readOnly}/>
                                            );
                                        }
                                    }

                                    return null;
                                })}
                            </div>
                        </CardPanel>
                    </Col>
                </Row>
            </Editor>
        );
    }
}
