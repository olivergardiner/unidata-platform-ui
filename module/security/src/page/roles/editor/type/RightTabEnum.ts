/**
 * DELETE AFTER CREATING A UE FOR THE CLASSIFIER
/**
 *  List of access rights tabs
 *
 * @author Ivan Marshalkin
 * @date 2019-05-22
 */

export enum RightTabEnum {
    right = 'right'

    // Previously, there were also classifiers,
    // but after cutting into modules, they went to a separate module
    // classifier = 'classifier'
}
