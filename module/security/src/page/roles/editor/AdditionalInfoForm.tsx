/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {CardPanel} from '@unidata/uikit';
import {PropertyRow} from './PropertyRow';
import {RoleProperty} from '../../../model/user/RoleProperty';

interface IProps {
    items: any[];
    readOnly?: boolean;
    metaProperties: RoleProperty[];
}

@observer
export class AdditionalInfoForm extends React.Component<IProps> {

    rolePropertyReadOnly (name: string): boolean {
        let property = this.props.metaProperties.find((property: any) => property.name.getValue() === name);

        if (property && property.readOnly.getValue()) {
            return true;
        }

        return false;
    }

    render () {
        const {items, readOnly} = this.props;

        // the panel is displayed only if there is an additional one.properties
        if (!items.length) {
            return null;
        }

        return (
            <CardPanel
                internal
                title={i18n.t('admin.role>additionalPanelTitle')}
            >
                {items.map((property) =>
                    <PropertyRow
                        key={property.id.getValue()}
                        item={property}
                        readOnly={readOnly || this.rolePropertyReadOnly(property.name.getValue())}
                    />
                )}
            </CardPanel>
        );
    }
}
