/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {SecurityLabelRole} from '../../../../model/user/SecurityLabelRole';
import {SecurityLabelUser} from '../../../../model/user/SecurityLabelUser';
import {i18n} from '@unidata/core-app';
import {Button, CardPanel, INTENT, Switcher} from '@unidata/uikit';
import {AttributeList} from './AttributeList';

interface IProps {
    label: SecurityLabelRole | SecurityLabelUser;
    roleSecurityLabels: Array<SecurityLabelRole | SecurityLabelUser>;
    onCheck: (label: SecurityLabelRole | SecurityLabelUser) => void;
    handleAddAttributes: (label: SecurityLabelRole | SecurityLabelUser) => void;
    handleRemoveAttributes: (label: SecurityLabelRole | SecurityLabelUser) => void;
    readOnly?: boolean;
}

@observer
export class SecurityLabelForm extends React.Component<IProps> {

    get rolesWithAttributes () {
        return this.props.roleSecurityLabels.filter((l) => l.attributes.items.length > 0);
    }

    get extraButtons () {
        const readOnly = this.props.readOnly;
        const checked: boolean = this.props.roleSecurityLabels.length > 0;

        return (
            <Switcher
                leftText={checked ? i18n.t('on') : i18n.t('off')}
                size={'small'}
                disabled={readOnly}
                checked={checked}
                onChange={this.handleUseCheck}
            />
        );
    }

    handleUseCheck = () => {
        this.props.onCheck(this.props.label);
    }

    handleAddAttributes = () => {
        this.props.handleAddAttributes(this.props.label);
    }

    render () {
        const {
            label,
            roleSecurityLabels,
            handleRemoveAttributes,
            readOnly
        } = this.props;

        return (
            <React.Fragment>
                <CardPanel
                    internal
                    title={label.displayName.getValue()}
                    extraButtons={this.extraButtons}
                >
                    {this.rolesWithAttributes.map((roleLabel, index) =>
                        <AttributeList
                            key={`attr-list-${roleLabel.modelId}`}
                            roleLabel={roleLabel}
                            readOnly={readOnly}
                            roleLabelAttributes={roleLabel.attributes.items}
                            handleRemoveAttributes={handleRemoveAttributes}
                            isLast={index === this.rolesWithAttributes.length - 1}
                        />
                    )}

                    {roleSecurityLabels.length > 0 &&
                    <div style={{textAlign: 'center'}}>
                        {!readOnly && (
                            <Button
                                onClick={this.handleAddAttributes}
                                intent={INTENT.PRIMARY}
                                isMinimal={true}
                                leftIcon={'plus-circle'}
                            >
                                {i18n.t('admin.role>securityLabelAddValue')}
                            </Button>
                        )}
                    </div>
                    }
                </CardPanel>
                <div style={{height: 10}}></div>
            </React.Fragment>
        );
    }
}
