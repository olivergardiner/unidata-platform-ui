/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {SecurityLabelRole} from '../../../../model/user/SecurityLabelRole';
import {SecurityLabelUser} from '../../../../model/user/SecurityLabelUser';
import {SecurityLabelForm} from './SecurityLabelForm';
import {ModelCollection, Nullable} from '@unidata/core';
import {EmptyText} from '@unidata/uikit';

interface IProps {
    roleSecurityLabels: Nullable<ModelCollection<SecurityLabelRole | SecurityLabelUser>>;
    allSecurityLabels: SecurityLabelRole[];
    handleCheckSecurityLabel: (item: SecurityLabelRole | SecurityLabelUser) => void;
    handleAddAttributes: (item: SecurityLabelRole | SecurityLabelUser) => void;
    handleRemoveAttributes: (item: SecurityLabelRole | SecurityLabelUser) => void;
    readOnly?: boolean;
}

@observer
export class SecurityLabelsList extends React.Component<IProps> {

    filterRoleSecurityLabelsByName = (labelName: string) => {
        let roleSecurityLabels = this.props.roleSecurityLabels;

        if (roleSecurityLabels) {
            return roleSecurityLabels.items.filter((roleLabel: (SecurityLabelRole | SecurityLabelUser)) => {
                return roleLabel.name.getValue() === labelName;
            });
        }

        return [];
    }

    render () {
        const {
            allSecurityLabels,
            handleCheckSecurityLabel,
            handleAddAttributes,
            handleRemoveAttributes,
            readOnly
        } = this.props;

        return (
            <React.Fragment>
                {allSecurityLabels.length > 0 && (
                    allSecurityLabels.map((label: (SecurityLabelUser | SecurityLabelRole), index: number) =>
                        <SecurityLabelForm
                            key={`${index}-${label.modelId}`}
                            label={label}
                            roleSecurityLabels={this.filterRoleSecurityLabelsByName(label.name.getValue())}
                            onCheck={handleCheckSecurityLabel}
                            readOnly={readOnly}
                            handleAddAttributes={handleAddAttributes}
                            handleRemoveAttributes={handleRemoveAttributes}
                        />
                    )
                )}
                {allSecurityLabels.length === 0 && (
                    <EmptyText text={i18n.t('admin.role>securityLabelsEmptyText')}/>
                )}
            </React.Fragment>
        );
    }
}
