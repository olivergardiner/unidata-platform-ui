/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {action} from 'mobx';
import {SecurityLabelRole} from '../../../../model/user/SecurityLabelRole';
import {SecurityLabelUser} from '../../../../model/user/SecurityLabelUser';
import {SecurityLabelAttributeRole} from '../../../../model/user/SecurityLabelAttributeRole';
import {i18n} from '@unidata/core-app';
import {INTENT, Button, Field, Col, Divider, Row} from '@unidata/uikit';

interface IProps {
    roleLabel: SecurityLabelRole | SecurityLabelUser;
    roleLabelAttributes: SecurityLabelAttributeRole[];
    handleRemoveAttributes: (item: SecurityLabelRole | SecurityLabelUser) => void;
    isLast?: boolean;
    readOnly?: boolean;
}

@observer
export class AttributeList extends React.Component<IProps> {

    get attributes () {
        return this.props.roleLabelAttributes.sort((attr1, attr2) => Number(attr1.id) - Number(attr2.id));
    }

    handleRemoveAttributes = () => {
        this.props.handleRemoveAttributes(this.props.roleLabel);
    }

    @action
    handleOnChange = (attr: SecurityLabelAttributeRole) => (name: string, value: any) => {
        attr.value.setValue(value);
    }

    render () {
        const {isLast, readOnly} = this.props;

        return (
            <Row>
                <Col span={22}>
                    <React.Fragment>
                        {this.attributes.map((attr: SecurityLabelAttributeRole) =>
                            <Field.Input
                                key={attr.modelId}
                                name={attr.name.getValue()}
                                label={attr.name.getValue()}
                                disabled={readOnly}
                                placeholder={i18n.t('admin.role>securityLabelAllowAllValue')}
                                defaultValue={attr.value.getValue()}
                                onChange={this.handleOnChange(attr)}
                            />
                        )}
                        {isLast ||
                            <Col className='ud-sucurity-labels-list-divider' offset={9}>
                                <Divider dashed>
                                    {i18n.t('or').toLowerCase()}
                                </Divider>
                            </Col>
                        }
                    </React.Fragment>
                </Col>

                <Col
                    span={2}
                    style={{paddingTop: '8px'}}
                >
                    {this.attributes.length > 0 && !readOnly && (
                        <Button
                            leftIcon={'delete'}
                            isMinimal={true}
                            onClick={this.handleRemoveAttributes}
                        />
                    )}
                </Col>
            </Row>
        );
    }
}
