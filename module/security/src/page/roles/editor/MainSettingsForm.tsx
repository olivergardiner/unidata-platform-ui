/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {getErrorMessage, i18n} from '@unidata/core-app';
import {CardPanel, Field} from '@unidata/uikit';
import {Role} from '../../../model/user/Role';

type HandleOnChangeType = (name: string, value: string) => void;

interface IProps {
    item: Role;
    onChange: HandleOnChangeType;
    readOnly?: boolean;
}

@observer
export class MainSettingsForm extends React.Component<IProps> {

    render () {
        const {item, onChange, readOnly} = this.props;

        return (
            <CardPanel
                internal
                title={i18n.t('admin.role>mainSettingsPanelTitle')}
            >
                <Field.Input
                    name='name'
                    disabled={readOnly || !item.phantom}
                    label={i18n.t('admin.role>name') + '*'}
                    defaultValue={item.name.getValue()}
                    onChange={onChange}
                    error={getErrorMessage(item, 'name')}
                />

                <Field.Input
                    name='displayName'
                    label={i18n.t('admin.role>displayName') + '*'}
                    defaultValue={item.displayName.getValue()}
                    disabled={readOnly}
                    onChange={onChange}
                    error={getErrorMessage(item, 'displayName')}
                />
            </CardPanel>
        );
    }
}
