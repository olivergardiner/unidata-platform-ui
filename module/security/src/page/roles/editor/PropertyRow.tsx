/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {getErrorMessage} from '@unidata/core-app';
import {Field} from '@unidata/uikit';

interface IProps {
    item: any;
    readOnly?: boolean;
}

@observer
export class PropertyRow extends React.Component<IProps> {

    handleOnChange = (name: string, value: string) => {
        this.props.item.value.setValue(value);
    }

    render () {
        const {item, readOnly} = this.props;

        return (
            <React.Fragment>
                <Field.Input
                    name='value'
                    label={item.displayName.getValue() + (item.required.getValue() ? '*' : '')}
                    defaultValue={item.value.getValue()}
                    disabled={readOnly}
                    onChange={this.handleOnChange}
                    error={getErrorMessage(item, 'value')}
                />
            </React.Fragment>
        );
    }
}
