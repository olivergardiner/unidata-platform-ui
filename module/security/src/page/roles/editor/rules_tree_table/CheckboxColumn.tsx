/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {Checkbox} from '@unidata/uikit';

interface IProps {
    name?: string;
    disabled?: boolean;
    hidden?: boolean;
    checked: boolean;
    indeterminate?: boolean; // indicator when not everything is selected
    hideDisabled?: boolean;
    onChange: (e: any) => void;
    style?: React.CSSProperties;
}

const defaultProps = {
    hideDisabled: false as boolean
};

@observer
export class CheckboxColumn extends React.Component<IProps> {
    static defaultProps = defaultProps;

    render () {
        const {name, hidden, disabled, indeterminate, checked, onChange} = this.props;

        return (
            <div className='ud-rule-tree_column'>
                {
                    hidden || <Checkbox name={name}
                                                disabled={disabled}
                                                checked={checked}
                                                indeterminate={indeterminate}
                                                onChange={onChange}/>
                }
            </div>
        );
    }
}
