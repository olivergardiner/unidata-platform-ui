/**
 * empty comment line Viktor Magarlamov
 *
 * @author: Viktor Magarlamov
 * @date: 2019-06-20
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {CheckboxColumn} from './CheckboxColumn';
import {CrudRights} from '../../../../types/CrudRights';

interface IProps {
    item: any;
    expand?: boolean;
    level: number;
    onClick?: (e: any) => void;
    category?: string;
    readOnly?: boolean;
    isLast: boolean;
}

interface IInjectProps extends IProps {
    editorStore: any;
}

@inject('editorStore')
@observer
export class Row extends React.Component<IProps> {
    get injected () {
        return this.props as IInjectProps;
    }

    get expandSymbol () {
        const {onClick, expand} = this.props;

        let content;

        if (onClick) {
            content = expand ? 'open' : 'close';
        }

        return content;
    }

    get allChecked () {
        const {item} = this.props,
            allRules: CrudRights[] = [CrudRights.Create, CrudRights.Read, CrudRights.Update, CrudRights.Delete],
            availableRules: CrudRights[] = allRules.filter((right) => item.unpermittedActions.indexOf(right) === -1);

        let itemRules: boolean[] = [];

        if (!item.roleRight) {
            return false;
        }

        itemRules = availableRules.reduce((result: boolean[], right: CrudRights) => {
            result.push(item.roleRight[right] ? item.roleRight[right].getValue() : false);

            return result;
        }, []);

        return itemRules.indexOf(false) === -1;
    }

    get createChecked () {
        return this.currentRight.create.getValue();
    }

    get readChecked () {
        return this.currentRight.read.getValue();
    }

    get updateChecked () {
        return this.currentRight.update.getValue();
    }

    get deleteChecked () {
        return this.currentRight.delete.getValue();
    }

    get currentRight () {
        return this.props.item.roleRight;
    }

    handleOnChange = (crudAction: string) => ({target}: {target: any}) => {
        this.injected.editorStore.handleRuleChange(
            this.currentRight,
            crudAction,
            target.checked,
            this.props.item.unpermittedActions,
            this.props.item
        );
    }

    isCheckboxHidden = (rule: ('all' | CrudRights)) => {
        const {item} = this.props;

        return item.unpermittedActions.indexOf(rule) > -1;
    }

    render () {
        const {onClick, level, item, isLast} = this.props;

        const firstColumn = {
            fontWeight: onClick ? 'bold' : 'normal'
        } as {[key: string]: string};

        const levels = Array.from(new Array(level).keys());

        return (
            <React.Fragment>
                <div className='ud-rule-tree_first-column' style={firstColumn} onClick={onClick}>
                    {levels.map((i, index) =>
                        <div className={`v-dot-line ${(isLast && index === levels.length - 1) ? 'v-dot-line-last' : ''}`} key={i} />
                    )}

                    <div className='h-dot-line' />

                    {this.expandSymbol && <div className={this.expandSymbol} /> }

                    <span title={item.name}>{item.displayName}</span>
                </div>

                {item.roleRight &&
                    <React.Fragment>
                        <CheckboxColumn
                            name={`#{item.name}_all`}
                            checked={this.allChecked}
                            disabled={this.props.readOnly}
                            onChange={this.handleOnChange('all')}
                            hidden={this.isCheckboxHidden('all')}
                        />
                        <CheckboxColumn
                            name={`#{item.name}_${CrudRights.Create}`}
                            checked={this.createChecked}
                            disabled={this.props.readOnly}
                            onChange={this.handleOnChange(CrudRights.Create)}
                            hidden={this.isCheckboxHidden(CrudRights.Create)}
                        />
                        <CheckboxColumn
                            name={`#{item.name}_${CrudRights.Read}`}
                            checked={this.readChecked}
                            disabled={this.props.readOnly}
                            onChange={this.handleOnChange(CrudRights.Read)}
                            hidden={this.isCheckboxHidden(CrudRights.Read)}
                        />
                        <CheckboxColumn
                            name={`#{item.name}_${CrudRights.Update}`}
                            checked={this.updateChecked}
                            disabled={this.props.readOnly}
                            onChange={this.handleOnChange(CrudRights.Update)}
                            hidden={this.isCheckboxHidden(CrudRights.Update)}
                        />
                        <CheckboxColumn
                            name={`#{item.name}_${CrudRights.Delete}`}
                            checked={this.deleteChecked}
                            disabled={this.props.readOnly}
                            onChange={this.handleOnChange(CrudRights.Delete)}
                            hidden={this.isCheckboxHidden(CrudRights.Delete)}
                        />
                    </React.Fragment>
                }
            </React.Fragment>
        );
    }
}
