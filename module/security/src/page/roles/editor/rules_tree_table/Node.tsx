/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {action} from 'mobx';
import {observer} from 'mobx-react';
import {Row} from './Row';

interface IProps {
    level: number;
    item: any;
    readOnly?: boolean;
    isLast: boolean;
}

@observer
export class Node extends React.Component<IProps> {

    @action
    onExpandClick = (e: Event) => {
        this.props.item.expand = !this.props.item.expand;
    }

    render () {
        const {item, level, readOnly, isLast} = this.props;
        const nextLevel = level + 1;

        return (
            <React.Fragment>
                <div className='ud-rule-tree_container'>
                    <Row
                        level={level}
                        item={item}
                        readOnly={readOnly}
                        onClick={this.onExpandClick}
                        expand={item.expand}
                        category={item.category}
                        isLast={isLast}
                    />

                </div>
                {item.expand && item.children.map((node: any, index: number) =>
                    <React.Fragment key={node.name}>
                        {node.children === null ?
                            <div className='ud-rule-tree_container'>
                                  <Row
                                    item={node}
                                    level={nextLevel}
                                    readOnly={readOnly}
                                    isLast={index === item.children.length - 1}
                                  />
                            </div> :
                            <Node
                                item={node}
                                level={nextLevel}
                                readOnly={readOnly}
                                isLast={index === item.children.length - 1}
                            />
                        }
                    </React.Fragment>
                )}
            </React.Fragment>
        );
    }
}
