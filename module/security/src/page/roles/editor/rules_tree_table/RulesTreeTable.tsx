/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {Node} from './Node';
import {i18n} from '@unidata/core-app';

interface IProps {
    nodes: any[];
    readOnly?: boolean;
}

/**
 * @deprecated Tree is used only for roles in the long term to replace the Tree
 * @see {Tree}
 */
export class RulesTreeTable extends React.Component<IProps> {
    render () {
        const {nodes, readOnly} = this.props;

        return (
            <div className='ud-rule-tree'>

                <div className='ud-rule-tree_container ud-rule-tree_header'>
                    <div className='ud-rule-tree_first-column'>
                        {i18n.t('admin.role>resourceColumnText')}
                    </div>
                    <div className='ud-rule-tree_column'>
                        {i18n.t('admin.role>fullColumnText')}
                    </div>
                    <div className='ud-rule-tree_column'>
                        {i18n.t('admin.role>createColumnText')}
                    </div>
                    <div className='ud-rule-tree_column'>
                        {i18n.t('admin.role>readColumnText')}
                    </div>
                    <div className='ud-rule-tree_column'>
                        {i18n.t('admin.role>updateColumnText')}
                    </div>
                    <div className='ud-rule-tree_column'>
                        {i18n.t('admin.role>deleteColumnText')}
                    </div>
                </div>

                <div className='ud-rule-tree_body'>
                    {nodes.map((rule) =>
                        <Node
                            key={rule.name}
                            readOnly={readOnly}
                            level={0}
                            item={rule}
                            isLast={false}
                        />
                    )}
                </div>
            </div>
        );
    }
}
