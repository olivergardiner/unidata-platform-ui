/**
 * DELETE AFTER CREATING A UE FOR THE CLASSIFIER
 */
/**
 * empty comment line Viktor Magarlamov
 *
 * @author: Viktor Magarlamov
 * @date: 2019-04-01
 */

export type MenuStoreType = {
    selectedLabel: any;
    items: any[];
    loadLabelList: () => void;
    createLabel: () => void;
    setSelectedLabel: (model: any) => void;
};
