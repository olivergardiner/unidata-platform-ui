/**
 * DELETE AFTER CREATING A UE FOR THE CLASSIFIER
 */
export enum CrudRights {
    Create = 'create',
    Read = 'read',
    Update = 'update',
    Delete = 'delete'
}
