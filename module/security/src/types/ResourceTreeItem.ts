import {CrudRights} from './CrudRights';

export type ResourceTreeItem = {
    name: string;
    category: string;
    type: string;
    displayName: string;
    children: ResourceTreeItem [] | null;
    expand: boolean;
    nodeType?: string;
    parentNode?: ResourceTreeItem | null;
    unpermittedActions?: CrudRights [];
    roleRight?: any;
}
