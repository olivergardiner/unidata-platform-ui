/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-09-04
 */

import {
    AbstractModel,
    hasMany,
    IAssociationDescriptorMap,
    IFieldValidator,
    LatinOnly,
    ModelCollection,
    ModelMetaData,
    Required,
    stringField,
    StringField
} from '@unidata/core';
import {IKeyValueMap, observable} from 'mobx';
import {SecurityLabelAttributeRole} from './SecurityLabelAttributeRole';

export class SecurityLabelRole extends AbstractModel {
    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

    @observable
    @stringField()
    public description: StringField;

    @observable
    @hasMany()
    public attributes: ModelCollection<SecurityLabelAttributeRole>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                attributes: SecurityLabelAttributeRole
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public static validators: IKeyValueMap<IFieldValidator[]> = {
        name: [
            {
                rule: Required,
                message: 'validation>required>name'
            },
            {
                rule: LatinOnly,
                message: 'validation>latinOnly'
            }
        ],
        displayName: [
            {
                rule: Required,
                message: 'validation>required>displayName'
            }
        ]
    };
}
