/**
 * empty comment line Viktor Magarlamov
 *
 * @author: Viktor Magarlamov
 * @date: 2019-05-27
 */

import {
    AbstractModel,
    hasMany,
    IAssociationDescriptorMap,
    IFieldValidator,
    LatinOnly,
    ModelCollection,
    ModelMetaData,
    Required,
    stringField,
    StringField
} from '@unidata/core';
import {IKeyValueMap, observable} from 'mobx';
import {SecurityLabelAttributeRole} from './SecurityLabelAttributeRole';

export class SecurityLabelUser extends AbstractModel {
    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

    @observable
    @hasMany()
    public attributes: ModelCollection<SecurityLabelAttributeRole>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                attributes: SecurityLabelAttributeRole
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public static validators: IKeyValueMap<IFieldValidator[]> = {
        name: [
            {
                rule: Required,
                message: 'validation>required>name'
            },
            {
                rule: LatinOnly,
                message: 'validation>latinOnly'
            }
        ],
        displayName: [
            {
                rule: Required,
                message: 'validation>required>displayName'
            }
        ]
    };
}
