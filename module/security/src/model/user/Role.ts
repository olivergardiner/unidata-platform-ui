/**
 * Role model
 *
 * @author Alexandr Bavin
 * @date 2018-18-12
 */

import {
    AbstractModel,
    dateField,
    DateField,
    hasMany,
    IAssociationDescriptorMap,
    IFieldValidator,
    LatinOnly,
    ModelCollection,
    ModelMetaData,
    Required,
    stringField,
    StringField
} from '@unidata/core';
import {IKeyValueMap, observable} from 'mobx';
import {SecurityLabelRole} from './SecurityLabelRole';
import {Right} from './Right';
import {RoleProperty} from './RoleProperty';

export class Role extends AbstractModel {

    @observable
    @stringField({primaryKey: true})
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

    @observable
    @dateField()
    public createdAt: DateField;

    @observable
    @stringField()
    public createdBy: StringField;

    @observable
    @dateField()
    public updatedAt: DateField;

    @observable
    @stringField()
    public updatedBy: StringField;

    @observable
    @stringField({defaultValue: 'USER_DEFINED'})
    public type: StringField;

    @observable
    @hasMany()
    public properties: ModelCollection<RoleProperty>;

    @observable
    @hasMany()
    public rights: ModelCollection<Right>;

    @observable
    @hasMany()
    public securityLabels: ModelCollection<SecurityLabelRole>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                properties: RoleProperty,
                rights: Right,
                securityLabels: SecurityLabelRole
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public static validators: IKeyValueMap<IFieldValidator[]> = {
        name: [
            {
                rule: Required,
                message: 'validation>required>name'
            },
            {
                rule: LatinOnly,
                message: 'validation>onlyAlphaAndSlashes'
            }
        ],
        displayName: [
            {
                rule: Required,
                message: 'validation>required>displayName'
            }
        ]
    };
}
