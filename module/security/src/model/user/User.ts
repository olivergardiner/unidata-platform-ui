/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-01-27
 */

import {
    AbstractModel,
    arrayField,
    ArrayField,
    booleanField,
    BooleanField,
    dateField,
    DateField,
    Email,
    hasMany,
    IAssociationDescriptorMap,
    IFieldValidator,
    LatinOnly,
    ModelCollection,
    ModelMetaData,
    Required,
    RequiredIfPhantom,
    stringField,
    StringField
} from '@unidata/core';
import {IKeyValueMap} from 'mobx';
import {SecurityLabelUser} from './SecurityLabelUser';
import {UserEndpoint} from './UserEndpoint';
import {UserProperty} from './UserProperty';
import {Locales} from '@unidata/core-app';

export class User extends AbstractModel {

    @booleanField({defaultValue: true})
    public active: BooleanField;

    @booleanField({defaultValue: false})
    public admin: BooleanField;

    @booleanField({defaultValue: false})
    public external: BooleanField;

    @booleanField({defaultValue: false})
    public emailNotification: BooleanField;

    @stringField({defaultValue: ''})
    public email: StringField;

    @stringField()
    public firstName: StringField;

    @stringField()
    public fullName: StringField;

    @stringField()
    public lastName: StringField;

    @stringField()
    public password: StringField;

    @stringField({defaultValue: Locales.Ru})
    public locale: StringField;

    @stringField()
    public securityDataSource: StringField;

    @stringField({primaryKey: true})
    public login: StringField;

    @arrayField()
    public roles: ArrayField;

    @dateField()
    public createdAt: DateField;

    @stringField()
    public createdBy: StringField;

    @dateField()
    public updatedAt: DateField;

    @stringField()
    public updatedBy: StringField;

    @stringField({defaultValue: 'USER_DEFINED'})
    public type: StringField;

    @hasMany()
    public properties: ModelCollection<UserProperty>;

    @hasMany()
    public securityLabels: ModelCollection<SecurityLabelUser>;

    @hasMany()
    public endpoints: ModelCollection<UserEndpoint>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                properties: UserProperty,
                securityLabels: SecurityLabelUser,
                endpoints: UserEndpoint
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public static validators: IKeyValueMap<IFieldValidator[]> = {
        login: [
            {
                rule: Required
            },
            {
                rule: LatinOnly,
                message: 'validation>onlyAlphaAndSlashes'
            }
        ],
        firstName: [
            {
                rule: Required
            }
        ],
        lastName: [
            {
                rule: Required
            }
        ],
        email: [
            {
                rule: Email,
                message: 'validation>malformedEmail'
            }
        ],
        password: [
            {
                rule: RequiredIfPhantom,
                message: 'validation>required>password'
            }
        ],
        locale: [
            {
                rule: Required
            }
        ],
        endpoints: [
            {
                rule: Required
            }
        ]
    };

}
