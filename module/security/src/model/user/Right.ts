/**
 * Rights model
 *
 * @author Alexandr Bavin
 * @date 2018-18-12
 */

import {
    AbstractModel,
    booleanField,
    BooleanField,
    dateField,
    DateField,
    hasOne,
    IAssociationDescriptorMap,
    ModelMetaData,
    Nullable,
    stringField,
    StringField
} from '@unidata/core';
import {observable} from 'mobx';
import {Resource} from './Resource';

export class Right extends AbstractModel {

    @observable
    @booleanField()
    public create: BooleanField;

    @observable
    @booleanField()
    public read: BooleanField;

    @observable
    @booleanField()
    public update: BooleanField;

    @observable
    @booleanField()
    public delete: BooleanField;

    @observable
    @dateField()
    public createdAt: DateField;

    @observable
    @stringField()
    public createdBy: StringField;

    @observable
    @dateField()
    public updatedAt: DateField;

    @observable
    @stringField()
    public updatedBy: StringField;

    @observable
    @stringField()
    public type: StringField;

    @observable
    @hasOne()
    public securedResource: Nullable<Resource>;

    public static rightsEqual (rightModel1: Right, rightModel2: Right) {
        if (!rightModel1 || !rightModel2) {
            return false;
        }

        return rightModel1.create.getValue() === rightModel2.create.getValue() &&
            rightModel1.read.getValue() === rightModel2.read.getValue() &&
            rightModel1.update.getValue() === rightModel2.update.getValue() &&
            rightModel1.delete.getValue() === rightModel2.delete.getValue();
    }

    public static anyRightExists (rightModel: Right) {
        let rightExists;

        if (!rightModel) {
            return false;
        }

        rightExists = rightModel.create.getValue() ||
            rightModel.read.getValue() ||
            rightModel.update.getValue() ||
            rightModel.delete.getValue();

        return rightExists;
    }

    public get rightsList () {
        let rights = ['create', 'read', 'update', 'delete'],
            rightList: string [] = [];

        rights.forEach((right: string) => {
            if ((this as any)[right]) {
                rightList.push(right);
            }
        });

        return rightList;
    }

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasOne: {
                securedResource: Resource
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

}
