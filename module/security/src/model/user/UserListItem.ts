/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-01-27
 */

import {
    AbstractModel,
    booleanField,
    BooleanField,
    dateField,
    DateField,
    hasMany,
    IAssociationDescriptorMap,
    ModelCollection,
    ModelMetaData,
    stringField,
    StringField
} from '@unidata/core';
import {observable} from 'mobx';
import {SecurityLabelRole} from './SecurityLabelRole';
import {UserProperty} from './UserProperty';

export class UserListItem extends AbstractModel {
    @observable
    @booleanField({defaultValue: false})
    public active: BooleanField;

    @observable
    @booleanField({defaultValue: false})
    public admin: BooleanField;

    @observable
    @booleanField({defaultValue: false})
    public external: BooleanField;

    @observable
    @stringField()
    public email: StringField;

    @observable
    @stringField()
    public firstName: StringField;

    @observable
    @stringField()
    public fullName: StringField;

    @observable
    @stringField()
    public lastName: StringField;

    @observable
    @stringField()
    public locale: StringField;

    @observable
    @stringField()
    public securityDataSource: StringField;

    @observable
    @stringField({primaryKey: true})
    public login: StringField;

    @observable
    @dateField()
    public createdAt: DateField;

    @observable
    @stringField()
    public createdBy: StringField;

    @observable
    @dateField()
    public updatedAt: DateField;

    @observable
    @stringField()
    public updatedBy: StringField;

    @observable
    @stringField({defaultValue: 'USER_DEFINED'})
    public type: StringField;

    @observable
    @hasMany()
    public properties: ModelCollection<UserProperty>;

    @observable
    @hasMany()
    public securityLabels: ModelCollection<SecurityLabelRole>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                properties: UserProperty,
                securityLabels: SecurityLabelRole
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
