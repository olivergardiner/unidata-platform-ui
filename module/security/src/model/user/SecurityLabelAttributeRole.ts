/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-09-04
 */

import {
    AbstractModel,
    IFieldValidator,
    IntegerField,
    integerField,
    Required,
    StringField,
    stringField
} from '@unidata/core';
import {IKeyValueMap, observable} from 'mobx';

export class SecurityLabelAttributeRole extends AbstractModel {

    @observable
    @integerField()
    public id: IntegerField;

    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public value: StringField;

    @observable
    @stringField()
    public path: StringField;

    public static validators: IKeyValueMap<IFieldValidator[]> = {
        name: [
            {
                rule: Required
            }
        ],
        path: [
            {
                rule: Required
            }
        ]
    };

}
