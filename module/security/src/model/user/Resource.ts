/**
 * Rights model
 *
 * @author Alexandr Bavin
 * @date 2018-18-12
 */

import {AbstractModel, dateField, DateField, StringField, stringField} from '@unidata/core';
import {observable} from 'mobx';

export class Resource extends AbstractModel {

    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

    @observable
    @dateField()
    public createdAt: DateField;

    @observable
    @stringField()
    public createdBy: StringField;

    @observable
    @dateField()
    public updatedAt: DateField;

    @observable
    @stringField()
    public updatedBy: StringField;

    @observable
    @stringField()
    public type: StringField;

    @observable
    @stringField()
    public category: StringField;
}
