/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-01-27
 */

import {AbstractModel, stringField, StringField} from '@unidata/core';
import {observable} from 'mobx';

export class UserEndpoint extends AbstractModel {
    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

    @observable
    @stringField()
    public description: StringField;
}
