/**
 * Model the list of roles
 *
 * @author Alexandr Bavin
 * @date 2018-18-12
 */

import {AbstractModel, stringField, StringField} from '@unidata/core';
import {observable} from 'mobx';

export class RoleListItem extends AbstractModel {

    @observable
    @stringField({primaryKey: true})
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

}
