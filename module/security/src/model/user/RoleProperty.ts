/**
 * Model for additional role properties
 *
 * @author Alexandr Bavin
 * @date 2018-18-12
 */

import {
    AbstractModel,
    booleanField,
    BooleanField,
    IFieldValidationParams,
    IFieldValidator,
    NumberField,
    numberField,
    stringField,
    StringField
} from '@unidata/core';
import {IKeyValueMap, observable} from 'mobx';

export class RoleProperty extends AbstractModel {

    @observable
    @numberField({primaryKey: true})
    public id: NumberField;

    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public displayName: StringField;

    @observable
    @booleanField()
    public required: BooleanField;

    @observable
    @booleanField()
    public readOnly: BooleanField;

    @observable
    @stringField()
    public value: StringField;

    public static validators: IKeyValueMap<IFieldValidator[]> = {
        value: [
            {
                message: 'validation>fieldRequired',
                validate: function (params: IFieldValidationParams) {
                    let model = params.model as RoleProperty;
                    let required = model.required.getValue();

                    if (required) {
                        return Boolean(model.value.getValue());
                    }

                    return true;
                }
            }
        ]
    };

}
