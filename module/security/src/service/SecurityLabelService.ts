/**
 * Service for working with security tags
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {ReadSecurityLabelOp} from './op/securitylabels/ReadSecurityLabelOp';
import {ReadSecurityLabelListOp} from './op/securitylabels/ReadSecurityLabelListOp';
import {CreateSecurityLabelOp} from './op/securitylabels/CreateSecurityLabelOp';
import {UpdateSecurityLabelOp} from './op/securitylabels/UpdateSecurityLabelOp';
import {DeleteSecurityLabelOp} from './op/securitylabels/DeleteSecurityLabelOp';

export class SecurityLabelService {

    public static getSecurityLabel (name: string): Promise<any> {
        let op = new ReadSecurityLabelOp(name);

        return op.execute();
    }

    public static getSecurityLabels (): Promise<any> {
        let op = new ReadSecurityLabelListOp();

        return op.execute();
    }

    public static createSecurityLabel (data: any) {
        let op = new CreateSecurityLabelOp(data);

        return op.execute();
    }

    public static updateSecurityLabel (id: string | number, data: any) {
        let op = new UpdateSecurityLabelOp(id, data);

        return op.execute();
    }

    public static destroySecurityLabel (id: string | number) {
        let op = new DeleteSecurityLabelOp(id);

        return op.execute();
    }
}
