/**
 * Operation for getting data sources
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {ReadUserAuthSourceListOp} from './op/user/ReadUserAuthSourceListOp';

export class UserAuthSourcesService {
    public static getUserAuthSources (): Promise<any> {
        let op = new ReadUserAuthSourceListOp();

        return op.execute();
    }
}
