/**
 * Role service
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {ReadRoleOp} from './op/role/ReadRoleOp';
import {Role} from '../model/user/Role';
import {DeleteRoleOp} from './op/role/DeleteRoleOp';
import {ReadRoleListOp} from './op/role/ReadRoleListOp';
import {CreateRoleOp} from './op/role/CreateRoleOp';
import {UpdateRoleOp} from './op/role/UpdateRoleOp';
import {ReadRoleSecurityResourceListOp} from './op/role/ReadRoleSecurityResourceListOp';

export class RoleService {
    /**
     * Gets a role
     *
     * @param roleName
     */
    public static getRole (roleName: string): Promise<Role> {
        let op = new ReadRoleOp(roleName);

        return op.execute();
    }

    /**
     * Retrieves the list of roles
     */
    public static getRoleList (): Promise<Role[]> {
        let op = new ReadRoleListOp();

        return op.execute();
    }

    /**
     * Deletes a role
     *
     * @param roleName
     */
    public static deleteRole (roleName: string): Promise<null> {
        let op = new DeleteRoleOp(roleName);

        return op.execute();
    }

    /**
     * Creates a new role
     *
     * @param role
     */
    public static createRole (role: Role): Promise<null> {
        let op = new CreateRoleOp(role.serialize());

        return op.execute();
    }

    /**
     * Updates an existing role
     *
     * @param role
     */
    public static updateRole (role: Role): Promise<null> {
        let op = new UpdateRoleOp(role.name.getValue(), role.serialize());

        return op.execute();
    }

    /**
     * Saves a role
     *
     * @param role
     */
    public static saveRole (role: Role): Promise<null> {
        if (role.getPhantom()) {
            return this.createRole(role);
        }

        return this.updateRole(role);
    }

    /**
     * Retrieves the full list of security resources
     */
    public static getRoleSecurityResourceList () {
        let op = new ReadRoleSecurityResourceListOp();

        return op.execute();
    }
}
