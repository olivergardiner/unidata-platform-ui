/**
 * Service for working with Users
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {ReadUserListOp} from './op/user/ReadUserListOp';
import {ReadUserOp} from './op/user/ReadUserOp';
import {CreateUserOp} from './op/user/CreateUserOp';
import {UpdateUserOp} from './op/user/UpdateUserOp';
import {ReadUserPropertyListOp} from './op/user/ReadUserPropertyListOp';
import {CreateUserPropertyOp} from './op/user/CreateUserPropertyOp';
import {UpdateUserPropertyOp} from './op/user/UpdateUserPropertyOp';
import {DeleteUserPropertyOp} from './op/user/DeleteUserPropertyOp';

export class UserService {
    public static getUsers (): Promise<any> {
        let op = new ReadUserListOp();

        return op.execute();
    }

    public static getUser (login: string): Promise<any> {
        let op = new ReadUserOp(login);

        return op.execute();
    }

    public static createUser (user: any): Promise<any> {
        let op = new CreateUserOp(user);

        return op.execute();
    }

    public static updateUser (login: string, user: any): Promise<any> {
        let op = new UpdateUserOp(login, user);

        return op.execute();
    }

    public static getUserProperties (): Promise<any> {
        let op = new ReadUserPropertyListOp();

        return op.execute();
    }

    public static createUserProperty (userProperty: any): Promise<any> {
        let op = new CreateUserPropertyOp(userProperty);

        return op.execute();
    }

    public static updateUserProperty (id: number, userProperty: any): Promise<any> {
        let op = new UpdateUserPropertyOp(id, userProperty);

        return op.execute();
    }

    public static deleteUserProperty (id: number): Promise<any> {
        let op = new DeleteUserPropertyOp(id);

        return op.execute();
    }

}
