/**
 * Service endpoint
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {ReadEndpointListOp} from './op/endpoint/ReadEndpointListOp';
import {UserEndpoint} from '../model/user/UserEndpoint';

export class EndpointService {
    /**
     * Retrieves the endpoint list
     */
    public static getEndpointList (): Promise<UserEndpoint[]> {
        let op = new ReadEndpointListOp();

        return op.execute();
    }
}
