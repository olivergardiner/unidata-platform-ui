/**
 * Additional role properties service
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {ReadRolePropertyListOp} from './op/roleproperty/ReadRolePropertyListOp';
import {RoleProperty} from '../model/user/RoleProperty';
import {CreateRoleProperty} from './op/roleproperty/CreateRolePropertyOp';
import {UpdateRolePropertyOp} from './op/roleproperty/UpdateRolePropertyOp';
import {DeleteRolePropertyOp} from './op/roleproperty/DeleteRolePropertyOp';

export class RolePropertyService {
    /**
     * Retrieves a list of additional properties for the role
     */
    public static getRolePropertyList (): Promise<RoleProperty[]> {
        let op = new ReadRolePropertyListOp();

        return op.execute();
    }

    /**
     * Creates a new additional property
     *
     * @param roleProperty
     */
    public static createRoleProperty (roleProperty: RoleProperty): Promise<null> {
        let op = new CreateRoleProperty(roleProperty);

        return op.execute();
    }

    /**
     * Updates an existing additional property
     *
     * @param roleProperty
     */
    public static updateRoleProperty (roleProperty: RoleProperty): Promise<null> {
        let op = new UpdateRolePropertyOp(roleProperty);

        return op.execute();
    }

    /**
     * Saves an additional property
     *
     * @param roleProperty
     */
    public static saveRoleProperty (roleProperty: RoleProperty): Promise<null> {
        if (roleProperty.getPhantom()) {
            return this.createRoleProperty(roleProperty);
        }

        return this.updateRoleProperty(roleProperty);
    }

    /**
     * Deletes an additional property
     *
     * @param rolePropertyId
     */
    public static deleteRoleProperty (rolePropertyId: number): Promise<null> {
        let op = new DeleteRolePropertyOp(rolePropertyId);

        return op.execute();
    }
}
