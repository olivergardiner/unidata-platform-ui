/**
 * Operation to get the endpoint list
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {UserEndpoint} from '../../../model/user/UserEndpoint';
import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class ReadEndpointListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/security/user/user-api/list',
        method: 'get',
        model: UserEndpoint,
        rootProperty: 'content'
    };
}
