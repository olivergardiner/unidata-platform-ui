/**
 * Operation for getting a list of roles
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {RoleListItem} from '../../../model/user/RoleListItem';
import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp} from '@unidata/core-app';

export class ReadRoleListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/security/role',
        method: 'get',
        model: RoleListItem,
        rootProperty: 'content'
    };
}
