/**
 * Operation for creating a new role
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class CreateRoleOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/security/role/',
        method: 'post'
    };

    constructor (data: any) {
        super();

        this.config.data = data;
    }
}
