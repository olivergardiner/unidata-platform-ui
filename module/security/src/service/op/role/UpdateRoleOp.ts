/**
 * Operation to save a role
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {AppHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class UpdateRoleOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/security/role/',
        method: 'put'
    };

    private roleName: string;

    constructor (roleName: string, data: any) {
        super();

        this.roleName = roleName;

        this.config.data = data;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/core/security/role/${this.roleName}`);
    }
}
