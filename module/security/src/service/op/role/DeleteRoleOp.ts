/**
 * Operation to delete a role
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {AppHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class DeleteRoleOp extends AppHttpOp {
    protected config: IOpConfig = {
        method: 'delete',
        rootProperty: 'content'
    };

    private roleName: string;

    constructor (roleName: string) {
        super();

        this.roleName = roleName;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/core/security/role/${this.roleName}`);
    }
}
