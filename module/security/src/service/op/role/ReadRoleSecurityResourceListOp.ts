/**
 * Operation for getting security resources
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {Resource} from '../../../model/user/Resource';
import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp} from '@unidata/core-app';

// TODO: we need to find out if it is needed and why it is part of RoleService
// Upd: the operation is definitely needed, the question is whether a separate service is needed
export class ReadRoleSecurityResourceListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/security/role/get-all-secured-resources',
        method: 'get',
        rootProperty: 'content',
        model: Resource
    };

    constructor () {
        super();
    }
}

