
/**
 * Operation for creating a security Tag
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class CreateSecurityLabelOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/security/label',
        method: 'post'
    };

    constructor (data: any) {
        super();

        this.config.data = data;
    }
}
