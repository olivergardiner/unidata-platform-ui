/**
 * Operation for getting a Security Tag
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {SecurityLabel} from '../../../model/user/SecurityLabel';
import {AppModelHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class ReadSecurityLabelOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        url: 'core/security/role/get-all-security-labels',
        method: 'get',
        model: SecurityLabel,
        rootProperty: 'content'
    };

    private labelName: string;

    constructor (labelName: string) {
        super();

        this.labelName = labelName;
    }

    protected processOperationData (data: any): any {
        const classModel = this.config.model;

        let result: any = [];
        let me = this;

        data.forEach(function (item: any) {
            if (me.labelName === item.name) {
                result = new classModel(item);
            }
        });

        return result;
    }
}
