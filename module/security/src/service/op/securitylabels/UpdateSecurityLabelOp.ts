/**
 * Operation for saving a Security Tag
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {AppHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class UpdateSecurityLabelOp extends AppHttpOp {
    protected config: IOpConfig = {
        method: 'put'
    };

    private id: string | number;

    constructor (id: string | number, data: any) {
        super();

        this.id = id;
        this.config.data = data;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/core/security/label/${this.id}`);
    }
}
