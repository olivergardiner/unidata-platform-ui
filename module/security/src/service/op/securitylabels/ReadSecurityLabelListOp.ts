/**
 * Operation for getting a list of Security Tags
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {SecurityLabel} from '../../../model/user/SecurityLabel';
import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class ReadSecurityLabelListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: 'core/security/role/get-all-security-labels',
        method: 'get',
        model: SecurityLabel,
        rootProperty: 'content'
    };
}
