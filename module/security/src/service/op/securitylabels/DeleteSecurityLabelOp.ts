/**
 * Operation to delete a security Label
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {AppHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class DeleteSecurityLabelOp extends AppHttpOp {
    protected config: IOpConfig = {
        method: 'delete'
    };

    private id: string | number;

    constructor (id: string | number) {
        super();

        this.id = id;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/core/security/label/${this.id}`);
    }
}
