/**
 * Operation for getting a list of additional role properties
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {RoleProperty} from '../../../model/user/RoleProperty';
import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp} from '@unidata/core-app';

export class ReadRolePropertyListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: 'core/security/role/role-properties/list',
        method: 'get',
        model: RoleProperty,
        rootProperty: 'content'
    };
}

