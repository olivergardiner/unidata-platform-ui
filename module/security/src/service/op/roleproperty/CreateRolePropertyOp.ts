/**
 * Operation for creating an additional role property
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {AppHttpOp} from '@unidata/core-app';
import {RoleProperty} from '../../../model/user/RoleProperty';
import {IOpConfig} from '@unidata/core';

export class CreateRoleProperty extends AppHttpOp {
    // TODO: why is the put method here??? you need to change it to post for unification
    protected config: IOpConfig = {
        url: 'core/security/role/role-properties/',
        method: 'put'
    };

    constructor (roleProperty: RoleProperty) {
        super();

        this.config.data = roleProperty.serialize();
    }
}

