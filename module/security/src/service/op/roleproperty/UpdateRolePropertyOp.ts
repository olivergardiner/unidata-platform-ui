/**
 * Operation for updating an additional role property
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {AppHttpOp, ServerUrlManager} from '@unidata/core-app';
import {RoleProperty} from '../../../model/user/RoleProperty';
import {IOpConfig} from '@unidata/core';

export class UpdateRolePropertyOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: 'core/security/role/role-properties/',
        method: 'put'
    };

    private rolePropertyId: number;

    constructor (roleProperty: RoleProperty) {
        super();

        this.rolePropertyId = roleProperty.id.getValue();
        this.config.data = roleProperty.serialize();
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`core/security/role/role-properties/${this.rolePropertyId}`);
    }
}

