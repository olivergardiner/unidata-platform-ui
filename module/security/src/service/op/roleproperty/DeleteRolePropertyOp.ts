/**
 * Operation to delete an additional role property
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {AppHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class DeleteRolePropertyOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: 'core/security/role/role-properties/',
        method: 'delete'
    };

    private rolePropertyId: number;

    constructor (rolePropertyId: number) {
        super();

        this.rolePropertyId = rolePropertyId;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/core/security/role/role-properties/${this.rolePropertyId}`);
    }
}
