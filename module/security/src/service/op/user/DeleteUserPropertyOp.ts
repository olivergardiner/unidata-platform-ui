/**
 * Operation to delete additional user properties
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {AppHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class DeleteUserPropertyOp extends AppHttpOp {
    protected config: IOpConfig = {
        method: 'delete'
    };

    private userPropertyId: number;

    constructor (rolePropertyId: number) {
        super();

        this.userPropertyId = rolePropertyId;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/core/security/user/user-properties/${this.userPropertyId}`);
    }
}
