/**
 * Operation for getting a user
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {User} from '../../../model/user/User';
import {AppModelHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class ReadUserOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/security/user',
        method: 'get',
        model: User,
        rootProperty: 'content'
    };

    private login: string;

    constructor (login: string) {
        super();
        this.login = login;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/core/security/user/${this.login}`);
    }
}
