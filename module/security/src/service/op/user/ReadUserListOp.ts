/**
 * Operation for getting a list of users
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {User} from '../../../model/user/User';
import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp} from '@unidata/core-app';

export class ReadUserListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/security/user',
        method: 'get',
        model: User,
        rootProperty: 'content'
    };
}
