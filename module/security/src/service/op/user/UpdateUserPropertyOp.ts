/**
 * Operation for saving additional user properties
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {AppHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class UpdateUserPropertyOp extends AppHttpOp {
    protected config: IOpConfig = {
        method: 'put'
    };

    private userPropertyId: number;

    constructor (userPropertyId: number, data: any) {
        super();

        this.userPropertyId = userPropertyId;
        this.config.data = data;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`core/security/user/user-properties/${this.userPropertyId}`);
    }
}
