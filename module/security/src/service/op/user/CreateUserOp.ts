/**
 * Operation for creating a new user
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class CreateUserOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/security/user/',
        method: 'post'
    };

    constructor (data: any) {
        super();

        this.config.data = data;
    }
}
