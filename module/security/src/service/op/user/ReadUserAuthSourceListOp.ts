/**
 * The operation to retrieve additional user properties
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ReadUserAuthSourceListOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: 'core/security/user/auth-sources/list',
        method: 'get'
    };
}
