/**
 * Operation for creating a new add-on.user properties
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class CreateUserPropertyOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/core/security/user/user-properties',
        method: 'put'
    };

    constructor (data: any) {
        super();

        this.config.data = data;
    }
}
