/**
 * Operation for getting a role
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {Role} from '../../../model/user/Role';
import {AppModelHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';

export class ReadRoleOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        method: 'get',
        model: Role,
        rootProperty: 'content'
    };

    private roleName: string;

    constructor (roleName: string) {
        super();

        this.roleName = roleName;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/core/security/role/${this.roleName}`);
    }
}
