/**
 * Operation for getting data sources
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {UserProperty} from '../../../model/user/UserProperty';
import {IModelOpConfig} from '@unidata/core';
import {AppModelListHttpOp} from '@unidata/core-app';

export class ReadUserPropertyListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/core/security/user/user-properties/list',
        model: UserProperty,
        method: 'get'
    };
}
