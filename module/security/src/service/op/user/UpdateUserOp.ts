/**
 * Operation save the user
 *
 * @author Denis Makarov
 * @date 2019-08-22
 */

import {AppHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class UpdateUserOp extends AppHttpOp {
    protected config: IOpConfig = {
        method: 'put'
    };

    private login: string;

    constructor (login: string, data: any) {
        super();

        this.login = login;
        this.config.data = data;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/core/security/user/${this.login}`);
    }
}
