/**
 * Utility class for working with the record model
 *
 * @author Ivan Marshalkin
 * @date 2019-07-03
 */

import {AttributeTypeCategory} from '@unidata/types';
import {Nullable} from '@unidata/core';

export class MetaAttributeUtil {
    public static getTypeCategory (metAttribute: any): Nullable<AttributeTypeCategory> {
        let typeCategory: Nullable<AttributeTypeCategory> = null;

        if (metAttribute.simpleDataType && Boolean(metAttribute.simpleDataType.getValue())) {
            typeCategory = AttributeTypeCategory.simpleDataType;
        } else if (metAttribute.enumDataType && Boolean(metAttribute.enumDataType.getValue())) {
            typeCategory = AttributeTypeCategory.enumDataType;
        } else if (metAttribute.lookupEntityType && Boolean(metAttribute.lookupEntityType.getValue())) {
            typeCategory = AttributeTypeCategory.lookupEntityType;
        } else if (metAttribute.linkDataType && Boolean(metAttribute.linkDataType.getValue())) {
            typeCategory = AttributeTypeCategory.linkDataType;
        }

        return typeCategory;
    }

}
