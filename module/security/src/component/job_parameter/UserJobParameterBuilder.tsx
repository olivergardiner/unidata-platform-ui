/**
 * UE for output select with users
 *
 * @author: Vladimir Stebunov
 * @date: 2020-02-18
 */

import * as React from 'react';
import {ModelCollection, UeModuleTypeCallBack} from '@unidata/core';
import {Field} from '@unidata/uikit';
import {User} from '../../model/user/User';
import {observable} from 'mobx';
import {Observer} from 'mobx-react';
import {UserService} from '../../service/UserService';

// TODO: fix types
export default {
    'default': {
        type: UeModuleTypeCallBack.JOB_PARAMETER,
        moduleId: 'userJobParameter',
        active: true,
        system: false,
        fn: function (
            props: {key: string; name: string; label: string},
            jobParameter: any,
            handleChange: (name: string, value: string) => void
        ) {
            // launching and making a request
            const users: ModelCollection<User> = observable(ModelCollection.create(User));

            UserService.getUsers().then((result: User[]) => {
                users.replaceAll(result);
            });

            const buildArrayValue = (value: string, delimiter: string): string[] => {
                if (value && value.length) {
                    return value.split(delimiter);
                }

                return [];
            };

            // redefining the Builder in the current osprey
            const Component = (
                props: {key: string; name: string; label: string},
                jobParameter: any,
                handleChange: (name: string, value: string) => void
            ) => {
                let value = buildArrayValue(jobParameter.value.getValue(), '|');

                const userList = users.items.map(function (user: User) {
                    return {
                        title: user.fullName.getValue(),
                        value: user.login.getValue()
                    };
                });

                return (
                    <Observer>
                        {
                            () => userList && <Field.Select {...props}
                                                mode='tags'
                                                name='userselect'
                                                data-qaid='userselect'
                                                options={userList}
                                                disabled={false}
                                                onSelect={handleChange}
                                                onDeselect={handleChange}
                                                allowClear={true}
                                                value={value}/>
                        }
                    </Observer>
                );
            };

            Component.displayName = 'UserJobParameterBuilder';

            this.fn = Component;

            return this.fn(props, jobParameter, handleChange);
        },
        resolver: () => true,
        meta: {
            metaParameterName: 'usersSelector'
        }
    }
};
