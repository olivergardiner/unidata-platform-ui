/**
 * UE for displaying a column with the full user name
 *
 * @author: Vladimir Stebunov
 * @date: 2020-07-07
 */

import * as React from 'react';
import {Observer} from 'mobx-react';
import {ModelCollection, UeModuleTypeCallBack} from '@unidata/core';
import {i18n, App} from '@unidata/core-app';
import {Badge, PLACEMENT, Select, Tooltip} from '@unidata/uikit';
import {UserListItem} from '../../model/user/UserListItem';
import {UserService} from '../../service/UserService';
import {observable, IObservableArray} from 'mobx';

// TODO: fix types
export default {
    'default': {
        type: UeModuleTypeCallBack.AUDIT_HEADER,
        moduleId: 'fullnameAuditColumn',
        active: true,
        system: false,
        fn: function (
            tableHeader: any,
            headerList: any,
            extension: {               // extension for additional properties
                storeSearchField: any; // store the selected values in the filter
                defaultRenderer: any;  // renderer by default
                userTooltip: any;      // tooltip to display
            }) {

            const users: ModelCollection<UserListItem> = observable(ModelCollection.create(UserListItem));

            UserService.getUsers().then((result) => {
                users.replaceAll(result as UserListItem[]);
            });

            const integrationMode: boolean =  App.hasExtJsApplication();

            // adding columns to the list
            headerList.columnNameMapper['fullname'] = () => i18n.t('admin.audit>fullname');
            headerList.allColumns.push('fullname');
            headerList.adminModeColumns['fullname'] = 'fullname';

            const getUsersBadgeText = (userFilterOpt: IObservableArray | undefined) => {
                let filteredUsersLength;

                if (!userFilterOpt) {
                    return '';
                }

                filteredUsersLength = userFilterOpt.length;

                return filteredUsersLength < 1 ? '' : `${filteredUsersLength}`;

            };

            const withTooltip = (value: string, key: string) => {
                return (
                    <Tooltip placement={PLACEMENT.TOP_START} mouseEnterDelay={500} overlay={value}>
                        <span data-qaid={key}>{value}</span>
                    </Tooltip>
                );
            };

            const userHeader = (headerText: string) => {
                const userFilterOpt = extension.storeSearchField.get('login');
                const badgeText = getUsersBadgeText(userFilterOpt);

                return (
                    <Badge count={badgeText} overflowCount={9}>
                        {withTooltip(headerText, 'fullname')}
                    </Badge>
                );
            };

            const header = () => userHeader(i18n.t('admin.audit>fullname'));

            const getFullnameBy = (login: string, users: ModelCollection<UserListItem>): string => {
                const user = users.find((user) => user.login.getValue() === login);

                if (user) {
                    return user.fullName.getValue() || '';
                }

                return '';
            };

            const renderFullname = (cell: any) => {
                const drawCell = (users: ModelCollection<UserListItem>) => {
                    const fullname = getFullnameBy(cell.value, users);

                    if (this.integrationMode) {
                        return (
                            <Observer>
                                {
                                    () => <span
                                        onMouseOver={
                                            (e) => this.userTooltip.showTooltip(cell.value, e.target)
                                        }
                                        onMouseLeave={
                                            () => this.userTooltip.hideTooltip()
                                        }
                                    >
                                        {fullname}
                                    </span>
                                }
                            </Observer>
                        );
                    } else {
                        return (<Observer>{() => extension.defaultRenderer({value: fullname})}</Observer>);
                    }
                };

                return (<Observer>{() => drawCell(users)}</Observer>);
            };

            const filter = (filterProps: any) => {
                let userFilterOpt = extension.storeSearchField.get('login');

                return (
                    <Observer>
                        { () =>
                            <React.Fragment>
                                <Select key='fullname'
                                        mode={'multiple'}
                                        placeholder={i18n.t('admin.audit>select')}
                                        onChange={(users: any) => {
                                            filterProps.onChange(users);
                                        }}
                                        style={{width: '100%'}}
                                        allowClear
                                        value={userFilterOpt && userFilterOpt.slice()}
                                        dropdownMatchSelectWidth={false}
                                        showArrow={true}
                                        data-qaid={'fullname_select'}
                                        options={users.items.map(function (userItem: UserListItem) {
                                            return {
                                                value: userItem.login.getValue(),
                                                title: userItem.fullName.getValue(),
                                                'data-qaid': userItem.login.getValue() + '_fullname_item'
                                            };
                                        })}
                                />
                            </React.Fragment>
                        }
                    </Observer>
                );
            };

            tableHeader.unshift({
                Header: header,
                accessor: 'login',
                id: 'login',
                minWidth: 180,
                Cell: renderFullname.bind(this),
                Filter: filter
            });

        },
        resolver: () => true,
        meta: {}
    }
};
