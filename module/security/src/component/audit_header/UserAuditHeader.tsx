/**
 * UE for displaying a column with users
 *
 * @author: Vladimir Stebunov
 * @date: 2020-02-18
 */

import * as React from 'react';
import {Observer} from 'mobx-react';
import {ModelCollection, UeModuleTypeCallBack} from '@unidata/core';
import {i18n, App} from '@unidata/core-app';
import {Badge, PLACEMENT, Select, Tooltip} from '@unidata/uikit';
import {UserListItem} from '../../model/user/UserListItem';
import {UserService} from '../../service/UserService';
import {observable, IObservableArray} from 'mobx';

// TODO: fix types
export default {
    'default': {
        type: UeModuleTypeCallBack.AUDIT_HEADER,
        moduleId: 'userAuditColumn',
        active: true,
        system: false,
        fn: function (
            tableHeader: any,
            headerList: any,
            extension: {               // extension for additional properties
                storeSearchField: any; // store the selected values in the filter
                defaultRenderer: any;  // renderer by default
                userTooltip: any;      // tooltip to display
            }) {

            const users: ModelCollection<UserListItem> = observable(ModelCollection.create(UserListItem));

            UserService.getUsers().then((result) => {
                users.replaceAll(result as UserListItem[]);
            });

            const integrationMode: boolean =  App.hasExtJsApplication();

            // adding columns to the list
            headerList.columnNameMapper['user'] = () => i18n.t('admin.audit>initiator');
            headerList.allColumns.push('login');
            headerList.adminModeColumns['login'] = 'login';

            const getUsersBadgeText = (userFilterOpt: IObservableArray | undefined) => {
                let filteredUsersLength;

                if (!userFilterOpt) {
                    return '';
                }

                filteredUsersLength = userFilterOpt.length;

                return filteredUsersLength < 1 ? '' : `${filteredUsersLength}`;

            };

            const withTooltip = (value: string, key: string) => {
                return (
                    <Tooltip placement={PLACEMENT.TOP_START} mouseEnterDelay={500} overlay={value}>
                        <span data-qaid={key}>{value}</span>
                    </Tooltip>
                );
            };

            const userHeader = (headerText: string) => {
                const userFilterOpt = extension.storeSearchField.get('user');
                const badgeText = getUsersBadgeText(userFilterOpt);

                return (
                    <Badge count={badgeText} overflowCount={9}>
                        {withTooltip(headerText, 'initiator')}
                    </Badge>
                );
            };

            const header = () => userHeader(i18n.t('admin.audit>initiator'));

            const renderUser = (cell: any) => {
                if (integrationMode) {
                    return (
                        <span
                            onMouseOver={
                                (e) => extension.userTooltip.showTooltip(cell.value, e.target)
                            }
                            onMouseLeave={
                                () => extension.userTooltip.hideTooltip()
                            }
                        >
                    {cell.value}
                </span>
                    );
                } else {
                    return extension.defaultRenderer(cell);
                }
            };

            const filter = (filterProps: any) => {
                let userFilterOpt = extension.storeSearchField.get('login');

                return (
                    <Observer>
                        { () =>
                            <React.Fragment>
                                <Select key='user'
                                        mode={'multiple'}
                                        placeholder={i18n.t('admin.audit>select')}
                                        onChange={(users: any) => {
                                            filterProps.onChange(users);
                                        }}
                                        style={{width: '100%'}}
                                        allowClear
                                        value={userFilterOpt && userFilterOpt.slice()}
                                        dropdownMatchSelectWidth={false}
                                        showArrow={true}
                                        data-qaid={'initiator_select'}
                                        options={users.items.map(function (userItem: UserListItem) {
                                            return {
                                                value: userItem.login.getValue(),
                                                title: userItem.login.getValue(),
                                                'data-qaid': userItem.login.getValue() + '_item'
                                            };
                                        })}
                                />
                            </React.Fragment>
                        }
                    </Observer>
                );
            };

            tableHeader.unshift({
                Header: header,
                accessor: 'login',
                id: 'login',
                minWidth: 180,
                Cell: renderUser.bind(this),
                Filter: filter
            });

        },
        resolver: () => true,
        meta: {}
    }
};
