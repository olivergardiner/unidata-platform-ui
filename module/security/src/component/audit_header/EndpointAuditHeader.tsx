/**
 * UE for output of a column with endpoint
 *
 * @author: Vladimir Stebunov
 * @date: 2020-02-18
 */

import * as React from 'react';
import {Observer} from 'mobx-react';
import {ModelCollection, UeModuleTypeCallBack} from '@unidata/core';
import {i18n} from '@unidata/core-app';
import {PLACEMENT, Select, Tooltip} from '@unidata/uikit';
import {observable, ObservableMap} from 'mobx';
import {UserEndpoint} from '../../model/user/UserEndpoint';
import {EndpointService} from '../../service/EndpointService';

interface IExtension {
    storeSearchField: ObservableMap;    // store the selected values in the filter
    defaultRenderer: JSX.Element;       // renderer by default
    userTooltip: any;                   // tooltip to display
}

export default {
    'default': {
        type: UeModuleTypeCallBack.AUDIT_HEADER,
        moduleId: 'endpointAuditColumn',
        active: true,
        system: false,
        fn: function (
            tableHeader: any,
            headerList: any,
            extension?: IExtension              // extension for additional properties
        ) {

            if (!extension) {
                return;
            }

            const endpoints: ModelCollection<UserEndpoint> = observable(ModelCollection.create(UserEndpoint));

            EndpointService.getEndpointList().then((result) => {
                endpoints.replaceAll(result);
            });
            // adding columns to the list
            headerList.columnNameMapper['endpoint'] = () => i18n.t('admin.labels>interface');
            headerList.allColumns.push('endpoint');

            headerList.adminModeColumns['endpoint'] = 'endpoint';
            headerList.operatorModeColumns['endpoint'] = 'endpoint';

            const withTooltip = (value: string, key: string) => {
                return (
                    <Tooltip placement={PLACEMENT.TOP_START} mouseEnterDelay={500} overlay={value}>
                        <span data-qaid={key}>{value}</span>
                    </Tooltip>
                );
            };

            const filter = (filterProps: any) => (
                <Observer>
                    {
                        () =>
                            <React.Fragment>
                                <Select
                                    key='endpoint2'
                                    onChange={(action: any) => {
                                        filterProps.onChange(action);
                                    }}
                                    style={{width: '100%'}}
                                    showSearch
                                    allowClear
                                    placeholder={i18n.t('admin.audit>select')}
                                    value={extension.storeSearchField.get('endpoint')}
                                    dropdownMatchSelectWidth={false}
                                    data-qaid='endpoint_filter'
                                    options={endpoints.items.map(function (endpoint: UserEndpoint) {
                                        return {
                                            value: endpoint.name.getValue(),
                                            'data-qaid': endpoint.name + '_item',
                                            title: endpoint.displayName.getValue()
                                        };

                                    })}
                                />
                            </React.Fragment>
                    }
                </Observer>
            );

            tableHeader.splice(5, 0, {
                Header: () => withTooltip(i18n.t('admin.labels>interface'), 'interface'),
                accessor: 'endpoint',
                minWidth: 100,
                id: 'endpoint',
                Cell: extension.defaultRenderer,
                Filter: filter
            });

        },
        resolver: () => true,
        meta: {}
    }
};
