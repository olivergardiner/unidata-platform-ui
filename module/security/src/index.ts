/**
 * Module for working with security tags, roles, and users
 *
 * @author: Vladimir Stebunov
 * @date: 2020-02-11
 */
import {IModule, UeModuleTypeComponent, UeModuleTypeCallBack} from '@unidata/core';

import {RolePage} from './page/roles/RolePage';
import {UsersPage} from './page/users/UsersPage';
import {SecurityLabelPage} from './page/security_labels/SecurityLabelPage';
import UserAuditHeader from './component/audit_header/UserAuditHeader';
import EndpointAuditHeader from './component/audit_header/EndpointAuditHeader';
import UserJobParameterBuilder from './component/job_parameter/UserJobParameterBuilder';
import {AppTypeManager} from '@unidata/core-app';
import FullnameAuditHeader from './component/audit_header/FullnameAuditHeader';

const menuItemSecurity = {
    'default': {
        type: UeModuleTypeCallBack.MENU_ITEM,
        moduleId: 'securityLabelMenuItem',
        active: true,
        system: false,
        fn: () => {},
        resolver: () => true,
        meta: {
            groupName: 'administration',
            order: 'last',
            name: 'security',
            route: '/security/list',
            icon: 'flag'
        }
    }
};


const menuItemRole = {
    'default': {
        type: UeModuleTypeCallBack.MENU_ITEM,
        moduleId: 'roleMenuItem',
        active: true,
        system: false,
        fn: () => {},
        resolver: () => true,
        meta: {
            groupName: 'administration',
            name: 'roles',
            route: '/roles',
            icon: 'users',
            pinned: true,
            order: 'last'
        }
    }
};

const menuItemUser = {
    'default': {
        type: UeModuleTypeCallBack.MENU_ITEM,
        moduleId: 'userMenuItem',
        active: true,
        system: false,
        fn: () => {},
        resolver: () => true,
        meta: {
            groupName: 'administration',
            order: 'last',
            name: 'users',
            route: '/users/list',
            icon: 'user'
        }
    }
};

const securityPage = {
    'default': {
        type: UeModuleTypeComponent.PAGE,
        moduleId: 'securityLabelPage',
        active: true,
        system: false,
        component: SecurityLabelPage,
        resolver: () => {
            return AppTypeManager.isSystemAdmin();
        },
        meta: {
            route: '/security/list'
        }
    }
};

const rolePage = {
    'default': {
        type: UeModuleTypeComponent.PAGE,
        moduleId: 'rolePage',
        active: true,
        system: false,
        component: RolePage,
        resolver: () => {
            return AppTypeManager.isSystemAdmin();
        },
        meta: {
            route: '/roles'
        }
    }
};

const userPage = {
    'default': {
        type: UeModuleTypeComponent.PAGE,
        moduleId: 'userPage',
        active: true,
        system: false,
        component: UsersPage,
        resolver: () => {
            return AppTypeManager.isSystemAdmin();
        },
        meta: {
            route: '/users/list'
        }
    }
};

export function init (): IModule {
    return {
        id: 'security',
        uemodules: [
            menuItemSecurity,
            menuItemRole,
            menuItemUser,
            securityPage,
            rolePage,
            userPage,
            UserAuditHeader,
            EndpointAuditHeader,
            UserJobParameterBuilder,
            FullnameAuditHeader
        ]
    };
}
