/**
 * Draft model
 *
 * @author Ivan Marshalkin
 * @date 2020-04-13
 */

import {AbstractModel, dateField, DateField, stringField, StringField} from '@unidata/core';

export class Draft extends AbstractModel {
    @stringField({primaryKey: true})
    public draftId: StringField;

    @stringField()
    public type: StringField;

    @stringField()
    public entityId: StringField;

    @stringField()
    public owner: StringField;

    @stringField()
    public ownerFullName: StringField;

    @stringField()
    public displayName: StringField;

    @dateField()
    public updateDate: DateField;

    @dateField()
    public createDate: DateField;
}
