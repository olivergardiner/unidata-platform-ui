import {IModule} from '@unidata/core';
import * as DraftList from './uemodule/draft_list/DraftList';
import * as GetDraftInfo from './uemodule/GetDraftInfo';
import {DraftWidget} from './uemodule/draft_widget/DraftWidget';

export * from './component/draft_list/DraftListWnd';

export {
    DraftWidget
};

export function init (): IModule {
    console.log('init module draft');

    return {
        id: 'draft',
        uemodules: [
            DraftList,
            GetDraftInfo
        ]
    };
}
