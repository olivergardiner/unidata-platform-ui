/**
 * UE that returns draft information
 *
 * @author Aleksandr Bavin
 * @date 2020-05-12
 */

import {Nullable, UeModuleTypeCallBack} from '@unidata/core';
import {DraftService} from '../service/DraftService';

export default {
    type: UeModuleTypeCallBack.GET_DRAFT_INFO,
    moduleId: 'get_draft_info',
    active: true,
    system: false,
    resolver: () => true,
    fn: (type: string, entityId: Nullable<string>, draftId: string) => {
        return DraftService
            .getDraftList({type, entityId})
            .then(function (drafts) {
                let draft = drafts.find(function (draft) {
                    return draft.draftId.getValue().toString() === draftId.toString();
                });

                if (draft) {
                    return draft.serialize();
                }

                return null;
            });
    },
    meta: {
    }
};

