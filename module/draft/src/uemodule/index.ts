import {UeModuleTypeComponent} from '@unidata/core';
import {DraftWidget} from './draft_widget/DraftWidget';

// todo Brauer Ilya - temporarily leaving the UE, since it is likely to be used.
export const draftUE = [
    {
        'default': {
            type: UeModuleTypeComponent.DATA_PAGE_WIDGET,
            moduleId: 'draft_widget',
            active: true,
            system: false,
            resolver: () => true,
            meta: {
                dataGrid: DraftWidget.dataGrid
            },
            component: DraftWidget
        }
    }
];
