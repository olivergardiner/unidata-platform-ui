/**
 * UI UE that returns the draft list window
 *
 * @author Ivan Marshalkin
 * @date 2020-04-20
 */

import {IComponentPropsByUEType, UeModuleTypeComponent} from '@unidata/core';
import * as React from 'react';
import {DraftListWnd} from '../../component/draft_list/DraftListWnd';

export default {
    type: UeModuleTypeComponent.CMP_DRAFT_LIST_WND,
    moduleId: 'draft_list_wnd',
    active: true,
    system: false,
    resolver: () => true,
    meta: {},
    component: class Classifier extends React.Component<IComponentPropsByUEType[UeModuleTypeComponent.CMP_DRAFT_LIST_WND]> {
        render () {
            const props = this.props;

            return (
                <DraftListWnd
                    isOpen = {props.isOpen}
                    type = {props.type}
                    entityId = {props.entityId}
                    onDraftOpen = {props.onDraftOpen}
                    onClose = {props.onClose}
                />
            );
        }
    }
};

