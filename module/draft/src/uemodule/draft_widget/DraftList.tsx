/**
 * Widget that displays a list of drafts
 *
 * @author Brauer Ilya
 * @date 2020-04-22
 */

import * as React from 'react';
import {Spinner, Table, RowInfo} from '@unidata/uikit';
import {Draft} from '../../model/Draft';
import {commonColumns} from './commonColumns';
import {DraftService} from '../../service/DraftService';
import {DraftType} from './DraftWidget';

interface IProps {
    draftType: DraftType;
    entityId?: string;
}

interface IState {
    draftList: Draft[];
    page: number;
    isLoading: boolean;
}

export class DraftList extends React.Component<IProps, IState> {
    limit = 10;

    state: IState = {
        draftList: [],
        page: 1,
        isLoading: true
    };

    componentDidMount (): void {
        this.fetchDrafts();
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        if (prevProps.entityId !== this.props.entityId) {
            this.setState({page: 1}, this.fetchDrafts);
        }
    }

    get columns () {
        return commonColumns;
    }

    /**
     * Getting a list of drafts
     */
    fetchDrafts = () => {
        this.setState({isLoading: true});

        return DraftService.getDraftList({
            type: this.props.draftType,
            entityId: this.props.entityId === undefined ?
                null :
                this.props.entityId,
            limit: this.limit,
            start: (this.state.page - 1) * this.limit
        })
            .then((result: Draft[]) => {
                this.setState({draftList: result});
            })
            .finally(() => {
                this.setState({isLoading: false});
            });
    };

    /**
     * Change the page
     */
    onChangePage = (page: number) => {
        this.setState({page});
    };

    /**
     * Props for table row
     */
    getTrProps = (state: any, rowInfo: RowInfo) => {
        if (rowInfo === undefined) {
            return {};
        }

        const record: Draft = rowInfo.original;
        const classifierName = record.entityId.getValue();
        const draftId = record.draftId.getValue();

        const onClick = () => {
            if (this.props.draftType === 'classifier') {
                return window.open(`#/classifiers?name=${classifierName}&draftId=${draftId}`);
            }

            return null;
        };

        return {
            onClick
        };
    };

    render () {
        const draftLength = this.state.draftList.length;
        // 11 experimentally defined number of rows to fit on the draft widget
        const viewRowsCount = draftLength > 11 ? draftLength : 11;

        return (
            <Spinner isShow={this.state.isLoading}>
                <Table
                    columns={this.columns}
                    data={this.state.draftList}
                    viewRowsCount={viewRowsCount}
                    NoDataComponent={() => <Spinner/>}
                    getTrProps={this.getTrProps}
                />
                <Table.Pagination
                    hidePages={true}
                    showPageJump={false}
                    onPageChange={this.onChangePage}
                    page={this.state.page}
                    pages={Infinity}
                />
            </Spinner>
        );
    }
}
