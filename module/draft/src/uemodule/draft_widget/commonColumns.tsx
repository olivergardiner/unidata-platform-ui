/**
 * List of columns for the draft table. (Name, owner, date of update)
 *
 * @author Brauer Ilya
 *
 * @date 2020-05-06
 */
import {DateTimeUtil, i18n} from '@unidata/core-app';
import {CellInfo, Column} from '@unidata/uikit';
import {Draft} from '../../model/Draft';

export const commonColumns: Column[] = [
    {
        accessor: 'displayName',
        Header: () => {
            return i18n.t('module.draft>draftColumnNameTitle');
        },
        sortable: false,
        resizable: false,
        Cell: (props: CellInfo) => {
            const record: Draft = props.original;

            return record.displayName.getValue();
        }
    },
    {
        accessor: 'owner',
        Header: () => {
            return i18n.t('module.draft>draftColumnOwnerTitle');
        },
        sortable: false,
        resizable: false,
        Cell: (props: CellInfo) => {
            const record: Draft = props.original;

            return record.ownerFullName.getValue();
        }
    },
    {
        accessor: 'updateDate',
        Header: () => {
            return i18n.t('module.draft>draftColumnUpdateDateTitle');
        },
        sortable: false,
        resizable: false,
        Cell: (props: CellInfo) => {
            const record: Draft = props.original;

            return DateTimeUtil.format(record.updateDate.getValue(), 'DD-MM-YYYY');
        }
    }
];
