/**
 * Widget that displays a list of registries with drafts
 *
 * @author Brauer Ilya
 * @date 2020-04-22
 */

import * as React from 'react';
import {CardPanel, GroupInput, Icon, IOption, Select, InitTable} from '@unidata/uikit';
import {DraftList} from './DraftList';

import * as styles from './draftWidget.m.scss';

import {Catalog, EntitySelect, EntityTreeType} from '@unidata/meta';
import {
    ModuleManager,
    ueModuleManager,
    UeModuleTypeComponent
} from '@unidata/core';
import {i18n} from '@unidata/core-app';

const dataGrid = {
    i: 'draftWidget',
    x: 0,
    y: 3,
    w: 12,
    h: 3
};

interface IProps {
}

export type DraftType = 'data' | 'classifier';

interface IState {
    draftType: DraftType;
    selectedEntityName?: string;

    classifierList: IOption[];
    selectedClassifierName?: string;
}

export class DraftWidget extends React.Component<IProps, IState> {
    static dataGrid = dataGrid;

    selectOptions: IOption[] = [
        {title: i18n.t('module.draft>initTable>Entity'), value: 'data'},
        {title: i18n.t('module.draft>initTable>Classifiers'), value: 'classifier'}
    ];

    hasClassifier: boolean = ModuleManager.hasModule('classifier');

    draftDataUe = ueModuleManager.getComponentById<UeModuleTypeComponent.DRAFT_DATA_LIST>('draftDataList');
    clsfOptionsUe = ueModuleManager.getModuleById('get_classifier_options');

    entitySelectRef: EntitySelect | null = null;

    state: IState = {
        draftType: 'data',
        classifierList: []
    };

    /**
     * Change the draft type - data or classifier
     */
    onChangeType = (value: DraftType) => {
        // So far, the first time you switch to clsf, we make a request to the list of classifiers and save it
        // todo Brauer Ilya to consider how to update the list when adding a new clsf
        // probably need to finish caching in UE get_classifier_options
        if (value === 'classifier' && this.clsfOptionsUe !== null && this.state.classifierList.length === 0) {
            (this.clsfOptionsUe.default as any).fn().then((classifierList: any) => {
                this.setState({classifierList});
            });
        }

        this.setState({draftType: value, selectedEntityName: undefined, selectedClassifierName: undefined});
    };

    /**
     * Select a registry / directory to display only its drafts
     */
    onEntitySelect = (entity: EntityTreeType) => {
        if (!(entity instanceof Catalog)) {
            this.setState({selectedEntityName: entity.name.getValue()});
        }
    };

    /**
     * Select a classifier to display only its drafts
     */
    onClassifierSelect = (clsf: string) => {
        this.setState({selectedClassifierName: clsf});
    };

    setEntitySelectVisible = () => {
        if (this.entitySelectRef !== null) {
            this.entitySelectRef.setDropdownVisible(true);
            this.entitySelectRef.focus();
        }
    };

    renderDraftDataList () {
        if (this.state.selectedEntityName === undefined) {
            return (
                <div className={styles.initDrafts}>
                    <InitTable
                        mainText={i18n.t('module.draft>initTable>choose')}
                        mainTextBlue={i18n.t('module.draft>initTable>entity')}
                        subText={i18n.t('module.draft>initTable>subText')}
                        onClick={this.setEntitySelectVisible}
                    />
                </div>
            );
        }

        const DraftDataList = this.draftDataUe;

        return DraftDataList !== null && (
            <DraftDataList entityName={this.state.selectedEntityName}/>
        );
    }

    renderDraftClassifierList () {
        if (this.state.selectedClassifierName === undefined) {
            return (
                <InitTable
                    mainText={i18n.t('module.draft>initTable>choose')}
                    mainTextBlue={i18n.t('module.draft>initTable>classifier')}
                    subText={i18n.t('module.draft>initTable>subText')}
                />
            );
        }

        return (
            <DraftList draftType={'classifier'} entityId={this.state.selectedClassifierName}/>
        );
    }

    setRef = (el: EntitySelect) => {
        this.entitySelectRef = el;
    };

    render () {
        return (
            <CardPanel
                title={(
                    <>
                        <Icon name={'register'}/> {i18n.t('module.draft>draftList')}
                    </>
                )}
                isCollapsed={false}
            >
                <CardPanel
                    internal={true}
                    extraButtons={(
                        <div className={styles.extraButtons}>
                            <GroupInput>
                                {this.hasClassifier && (
                                    <Select
                                        options={this.selectOptions}
                                        value={this.state.draftType}
                                        onChange={this.onChangeType}
                                    />
                                )}
                                {this.state.draftType === 'data' && (
                                    <EntitySelect
                                        ref={this.setRef}
                                        onEntitySelect={this.onEntitySelect}
                                    />
                                )}
                                {this.state.draftType === 'classifier' && (
                                    <Select
                                        value={this.state.selectedClassifierName}
                                        placeholder={i18n.t('module.draft>initTable>Classifiers')}
                                        options={this.state.classifierList}
                                        onSelect={this.onClassifierSelect}
                                    />
                                )}
                            </GroupInput>
                        </div>
                    )}
                >
                    <div className={styles.container}>
                        {this.state.draftType === 'data' && this.renderDraftDataList()}
                        {this.state.draftType === 'classifier' && this.renderDraftClassifierList()}
                    </div>
                </CardPanel>
            </CardPanel>
        );
    }
}
