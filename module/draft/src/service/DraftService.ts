/**
 * Generalized drafts service
 *
 * @author Ivan Marshalkin
 * @date 2020-04-08
 */

import {IDraftListRequest, ReadDraftListOp} from './op/draft/ReadDraftListOp';
import {Nullable} from '@unidata/core';
import {Draft} from '../model/Draft';
import {CreateDraftOp} from './op/draft/CreateDraftOp';
import {UpdateDraftOp} from './op/draft/UpdateDraftOp';
import {PublishDraftOp} from './op/draft/PublishDraftOp';
import {DeleteDraftOp} from './op/draft/DeleteDraftOp';

export class DraftService {
    public static getDraftList (params: IDraftListRequest): Promise<Draft[]> {
        let op = new ReadDraftListOp(params);

        return op.execute();
    }

    public static saveDraft (draft: Draft): Promise<null> {
        if (draft.phantom) {
            return this.createDraft(draft);
        }

        return this.updateDraft(draft);
    }

    private static createDraft (draft: Draft): Promise<null> {
        let op = new CreateDraftOp(draft);

        return op.execute();
    }

    private static updateDraft (draft: Draft): Promise<null> {
        let op = new UpdateDraftOp(draft);

        return op.execute();
    }

    public static publishDraft (draft: Draft): Promise<null> {
        let op = new PublishDraftOp(draft);

        return op.execute();
    }

    public static deleteDraft (draft: Draft): Promise<null> {
        let op = new DeleteDraftOp(draft);

        return op.execute();
    }
}
