/**
 * Operation for getting a list of drafts
 *
 * @author Ivan Marshalkin
 * @date 2020-04-08
 */

import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig, Nullable} from '@unidata/core';
import {Draft} from '../../../model/Draft';

export interface IDraftListRequest {
    type: string;
    entityId: Nullable<string>;
    start?: number;
    limit?: number;
}

export class ReadDraftListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/draft/drafts?start={{start}}&limit={{limit}}',
        method: 'post',
        model: Draft,
        rootProperty: 'content'
    }

    constructor (params: IDraftListRequest) {
        super();

        this.config.data = {
            type: params.type,
            entityId: params.entityId
        };

        this.urlContext = {
            start: params.start !== undefined ? params.start : 0,
            limit: params.limit !== undefined ? params.limit : 100
        };
    }
}
