/**
 * The publishing of the draft
 *
 * @author Ivan Marshalkin
 * @date 2020-04-08
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {Draft} from '../../../model/Draft';

export class PublishDraftOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/draft/publish',
        method: 'post'
    };

    constructor (draft: Draft) {
        super();

        this.config.data = draft.serialize();
    }
}
