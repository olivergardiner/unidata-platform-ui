/**
 * Operation to delete a draft
 *
 * @author Ivan Marshalkin
 * @date 2020-04-08
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {Draft} from '../../../model/Draft';

export class DeleteDraftOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/draft/remove',
        method: 'post'
    };

    constructor (draft: Draft) {
        super();

        this.config.data = {
            type: draft.type.getValue(),
            entityId: draft.entityId.getValue(),
            draftId: draft.draftId.getValue()
        };
    }
}
