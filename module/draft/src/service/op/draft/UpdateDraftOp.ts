/**
 * Operation to save a draft
 *
 * @author Ivan Marshalkin
 * @date 2020-04-08
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {Draft} from '../../../model/Draft';

export class UpdateDraftOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/draft/upsert',
        method: 'post'
    };

    constructor (draft: Draft) {
        super();

        this.config.data = {
            type: draft.type.getValue(),
            entityId: draft.entityId.getValue(),
            draftId: draft.draftId.getValue(),
            displayName: draft.displayName.getValue()
        };
    }
}
