/**
 * Window for working with the list of drafts
 *
 * @author Ivan Marshalkin
 * @date 2020-04-17
 */

import * as React from 'react';
import {ReactNode} from 'react';
import {Button, INTENT, Modal} from '@unidata/uikit';
import {DraftList} from './DraftList';
import * as style from './DraftListWnd.m.scss';
import {i18n} from '@unidata/core-app';
import {action, observable} from 'mobx';
import {Draft} from '../../model/Draft';
import {observer} from 'mobx-react';

// props from @inject mobx
interface IInjectedProps {
}

interface IProps {
    children?: ReactNode;
    isOpen: boolean;
    type: string;
    entityId: string;
    onDraftOpen: (draftId: string) => void;
    onClose: () => void;
}

interface IState {
}

@observer
export class DraftListWnd extends React.Component<IProps, IState> {
    @observable
    selectedDraft: Draft | null = null;

    @action
    setSelectedDraft (draft: Draft | null) {
        this.selectedDraft = draft;
    }

    get injectedProps (): IInjectedProps {
        return this.props as IInjectedProps;
    }

    onDraftSelect (draft: Draft) {
        this.setSelectedDraft(draft);
    }

    openDraft () {
        if (this.selectedDraft) {
            this.props.onDraftOpen(this.selectedDraft.draftId.getValue());
            this.setSelectedDraft(null);
        }
    }

    render () {
        const self = this;
        const props = this.props;

        return (
            <Modal
                isOpen={props.isOpen}
                noBodyPadding={true}
                header={i18n.t('module.draft>draftListWindowTitle')}
                onClose={function () {
                    props.onClose();
                }}
            >
                <div className={style.wrapper}>
                    <div className={style.list}>
                        <DraftList
                            type={props.type}
                            entityId={props.entityId}
                            onDraftOpen={self.openDraft.bind(this)}
                            onDraftSelect={self.onDraftSelect.bind(this)}
                        >
                        </DraftList>
                    </div>

                    <div className={style.footer}>
                        <Button
                            intent={INTENT.PRIMARY}
                            onClick={self.openDraft.bind(this)}
                            isDisabled={self.selectedDraft ? false : true}
                            data-qaid={'openDraft'}
                        >
                            {i18n.t('module.draft>openDraftButtonTitle')}
                        </Button>
                    </div>
                </div>
            </Modal>
        );
    }
}
