/**
 *
 *
 * @author Ivan Marshalkin
 * @date 2020-04-09
 */
import * as React from 'react';
import {ReactNode} from 'react';
import * as style from './DraftList.m.scss';
import {DraftListStore} from './store/DraftListStore';
import {observer, Provider} from 'mobx-react';
import {Draft} from '../../model/Draft';
import {DraftListFooter} from './component/DraftListFooter';
import {DraftListTable} from './component/DraftListTable';

interface IProps {
    type: string;
    entityId: string;
    onDraftOpen: () => void;
    onDraftSelect: (draft: Draft) => void;
    children?: ReactNode;
}

interface IState {
}

@observer
export class DraftList extends React.Component<IProps, IState> {
    store = new DraftListStore();

    componentDidMount (): void {
        const store = this.store;

        store.setType(this.props.type);
        store.setEntityId(this.props.entityId);

        store.loadDraftList();
    }

    render () {
        const props = this.props;

        return (
            <Provider store={this.store}>
                <div className={style.wrapper}>
                    <div className={style.list}>
                        <DraftListTable
                            onDraftOpen={props.onDraftOpen}
                            onDraftSelect={props.onDraftSelect}
                        />
                    </div>

                    <div className={style.footer}>
                        <DraftListFooter/>
                    </div>
                </div>
            </Provider>
        );
    }
}
