/**
 * Store of the draft list
 *
 * @author Ivan Marshalkin
 * @date 2020-04-09
 */

import {action, computed, observable} from 'mobx';
import {Draft} from '../../../model/Draft';
import {Nullable} from '@unidata/core';
import {DraftService} from '../../../service/DraftService';
import remove from 'lodash/remove';
import {DateTimeUtil, i18n, UserLocale} from '@unidata/core-app';

export class DraftListStore {
    @observable
    public type: string;

    @observable
    public entityId: string;

    @observable.ref
    public drafts: Draft[] = [];

    @observable.shallow
    public checkedDrafts: string[] = [];

    @observable
    public editableDraft: Draft | null = null;

    @observable
    public selectedDraft: Draft | null = null;

    @observable
    public hoveredDraft: Draft | null = null;

    @action
    public setType (type: string) {
        this.type = type;
    }

    @action
    public setEntityId (entityId: string) {
        this.entityId = entityId;
    }

    public loadDraftList () {
        const self = this;

        const promise = DraftService.getDraftList({
            type: this.type,
            entityId: this.entityId
        });

        promise.then(
            action(function (drafts: Draft[]) {
                self.drafts = drafts;
            }),
            function () {
            }
        );

        return promise;
    }

    @action
    public checkDraft (draftId: string) {
        this.checkedDrafts.push(draftId);
    }

    @action
    public decheckDraft (draftId: string) {
        let index: number = this.checkedDrafts.indexOf(draftId);

        if (index !== -1) {
            this.checkedDrafts.splice(index, 1);
        }
    }

    @action
    public clearCheckedDrafts () {
        this.checkedDrafts = [];
    }

    @action
    public selectAllDrafts () {
        const self = this;

        this.drafts.forEach(function (draft) {
            self.checkDraft(draft.draftId.getValue());
        });
    }

    @action
    public toggleDraftCheck (draftId: string) {
        if (this.isDraftChecked(draftId)) {
            this.decheckDraft(draftId);
        } else {
            this.checkDraft(draftId);
        }
    }

    @action
    public toggleAllCheckedDrafts () {
        if (this.allDraftsChecked) {
            this.clearCheckedDrafts();
        } else {
            this.selectAllDrafts();
        }
    }

    @computed
    public get allDraftsChecked (): boolean {
        const self = this;

        let selected: boolean = true;

        this.drafts.forEach(function (draft) {
            if (self.checkedDrafts.indexOf(draft.draftId.getValue()) === -1) {
                selected = false;
            }
        });

        return selected;
    }

    @computed
    public get hasCheckedDrafts (): boolean {
        return this.checkedDrafts.length > 0 ? true : false;
    }

    public isDraftChecked (draftId: string) {
        let index: number = this.checkedDrafts.indexOf(draftId);

        if (index !== -1) {
            return true;
        }

        return false;
    }

    @action
    public removeCheckedDrafts () {
        const self = this;

        let promises: Array<Promise<any>> = [];

        this.checkedDrafts.forEach(function (draftId) {
            const draft = self.getDraftById(draftId);

            if (draft) {
                promises.push(DraftService.deleteDraft(draft));
            }
        });

        return Promise.all(promises);
    }

    @action
    public createDraft () {
        const title = i18n.t('module.draft>defaultNewClassifierDraftText');
        const datetime: string = DateTimeUtil.format(new Date(), UserLocale.getDateTimeFormat());
        const displayName = `${title} ${datetime}`;

        let draft: Draft = new Draft({
            type: this.type,
            entityId: this.entityId,
            displayName: displayName
        });

        this.drafts = [draft, ...this.drafts];

        this.setEditableDraft(draft);
        this.setSelectedDraft(draft);
    }

    private getDraftById (draftId: string): Nullable<Draft> {
        let result;

        result = this.drafts.find(function (draft) {
            return draft.draftId.getValue() === draftId;
        });

        return result || null;
    }

    public indexOf (draft: Draft) {
        return this.drafts.indexOf(draft);
    }

    @action
    public setDraftAtIndex (index: number, draft: Draft) {
        this.drafts = this.drafts.reduce<Draft[]>((result, draftItem, ind) => {

            if (index === ind) {
                result.push(draft);
            } else {
                result.push(draftItem);
            }

            return result;
        }, []);
    }

    @action
    public setEditableDraft (draft: Draft) {
        this.editableDraft = draft;
    }

    @action
    public clearEditableDraft () {
        this.editableDraft = null;
    }

    public saveDraft (draft: Draft): Promise<any> {
        return DraftService.saveDraft(draft);
    }

    @action
    public removeDraft (draft: Draft) {
        remove(this.drafts, draft);

        this.drafts = this.drafts.slice();
    }

    @action
    public setSelectedDraft (draft: Draft | null) {
        this.selectedDraft = draft;
    }

    @action
    public clearSelectedDraft () {
        this.selectedDraft = null;
    }
}

