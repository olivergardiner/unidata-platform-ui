/**
 * List of drafts
 *
 * @author Ivan Marshalkin
 * @date 2020-04-17
 */

import * as React from 'react';
import {ReactNode} from 'react';
import {inject, observer} from 'mobx-react';
import {Input, Table, CellInfo} from '@unidata/uikit';
import {DraftListStore} from '../store/DraftListStore';
import {DateTimeUtil, Dialog, hasPrintableChars, i18n, UserLocale} from '@unidata/core-app';
import {Draft} from '../../../model/Draft';

// props from @inject mobx
interface IInjectedProps {
    store: DraftListStore;
}

interface IProps {
    children?: ReactNode;
    onDraftOpen: () => void;
    onDraftSelect: (draft: Draft | null) => void;
}

interface IState {
}


@inject('store')
@observer
export class DraftListTable extends React.Component<IProps, IState> {
    get injectedProps (): IInjectedProps {
        return this.props as any as IInjectedProps;
    }

    get store () {
        return this.injectedProps.store;
    }

    get columns () {
        return [
            {
                accessor: 'displayName',
                Header: () => {
                    return i18n.t('module.draft>draftColumnNameTitle');
                },
                Cell: (props: CellInfo) => {
                    const record: Draft = props.original;

                    return record.displayName.getValue();
                }
            },
            {
                accessor: 'ownerFullName',
                Header: () => {
                    return i18n.t('module.draft>draftColumnOwnerTitle');
                },
                Cell: (props: CellInfo) => {
                    const record: Draft = props.original;

                    return record.ownerFullName.getValue();
                }
            },
            {
                accessor: 'updateDate',
                Header: () => {
                    return i18n.t('module.draft>draftColumnUpdateDateTitle');
                },
                Cell: (props: CellInfo) => {
                    const record: Draft = props.original;

                    return (
                        <React.Fragment>
                            {DateTimeUtil.format(record.updateDate.getValue(), UserLocale.getDateTimeFormat())}
                        </React.Fragment>
                    );
                }
            }
        ];
    }

    editComponents = {
        displayName: (props: any) => {
            const record = props.original;
            const self = this;

            return (
                <Input
                    defaultValue={record.displayName.getValue()}
                    onBlur={function (event) {
                        props.onEdit(event.currentTarget.value);
                    }}
                    onKeyDown={function (event) {
                        switch (event.keyCode) {
                            case 13:
                                props.onEdit(event.currentTarget.value);
                                break;
                            case 27:
                                event.stopPropagation();
                                self.cancelEdit(record);
                                break;
                        }
                    }}
                    autoFocus={true}
                />
            );
        }
    };

    get data () {
        return this.store.drafts.slice();
    }

    cancelEdit (draft: Draft) {
        const store = this.store;

        draft.displayName.revert();
        store.clearEditableDraft();
    }

    saveDraft (draft: Draft) {
        const store = this.store;

        if (!draft.phantom && !draft.displayName.getDirty()) {
            store.clearEditableDraft();

            return;
        }

        if (!hasPrintableChars(draft.displayName.getValue())) {
            if (draft.phantom) {
                store.removeDraft(draft);
            } else {
                draft.displayName.revert();
                store.clearEditableDraft();
            }

            return;
        }

        const promise = store.saveDraft(draft);
        const self = this;

        promise.then(
            function (data) {
                if (draft.phantom) {
                    let draftIndex = store.indexOf(draft);

                    draft = new Draft(data);

                    store.setDraftAtIndex(draftIndex, draft);
                }

                store.clearEditableDraft();
                store.setSelectedDraft(draft);
                self.props.onDraftSelect(draft);
                Dialog.showMessage(i18n.t('module.draft>saveDraftSuccessMessage'));
            }
        );
    }

    onRowCheck = (rowIndex: number, isCheck: boolean) => {
        const store = this.store;
        const record = store.drafts[rowIndex];

        if (isCheck === true) {
            store.checkDraft(record.draftId.getValue());
        } else {
            store.decheckDraft(record.draftId.getValue());
        }

        store.clearSelectedDraft();
        this.props.onDraftSelect(null);
    };

    onRowClick = (rowIndex: number) => {
        const store = this.store;
        const draft: Draft = this.data[rowIndex];

        if (store.hasCheckedDrafts) {
            return;
        }

        store.setSelectedDraft(draft);
        this.props.onDraftSelect(draft);
    };

    onRowDBClick = () => {
        const store = this.store;

        if (store.selectedDraft) {
            this.props.onDraftOpen();
        }
    };

    onCellEdit = (rowIndex: number, columnId: string, value: string) => {
        const draft = this.store.drafts[rowIndex];

        draft.displayName.setValue(value);

        this.saveDraft(draft);
    };

    render () {
        const store = this.store;

        let checkedRows = undefined;

        if (store.selectedDraft !== null) {
            const index = store.drafts.findIndex((draft) => {
                return store.selectedDraft?.draftId.getValue() === draft.draftId.getValue();
            });

            if (index > -1) {
                checkedRows = [index];
            }
        }

        return (
            <Table
                columns={this.columns}
                data={store.drafts}
                viewRowsCount={0}
                hasPagination={false}

                editableCell={store.editableDraft !== null ? {id: 'displayName', index: 0} : undefined}
                editComponents={this.editComponents}
                isEditable={true}
                onCellEdit={this.onCellEdit}

                isRowsCheckable={true}
                checkedRows={checkedRows}
                onAllCheck={store.toggleAllCheckedDrafts.bind(store)}
                onRowCheck={this.onRowCheck}

                onRowClick={this.onRowClick}
                onRowDoubleClick={this.onRowDBClick}
            />
        );
    }
}
