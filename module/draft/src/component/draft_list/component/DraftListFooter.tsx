/**
 * Footer of the draft list
 *
 * @author Ivan Marshalkin
 * @date 2020-04-10
 */

import * as React from 'react';
import {ReactNode} from 'react';
import {Button, Confirm, INTENT} from '@unidata/uikit';
import {capitalize, Dialog, i18n} from '@unidata/core-app';
import {inject, observer} from 'mobx-react';
import {DraftListStore} from '../store/DraftListStore';

// props from @inject mobx
interface IInjectedProps {
    store: DraftListStore;
}

interface IProps {
    children?: ReactNode;
}

interface IState {
    isConfirmOpen: boolean;
}

@inject('store')
@observer
export class DraftListFooter extends React.Component<IProps, IState> {
    state = {
        isConfirmOpen: false
    }

    get store () {
        return this.injectedProps.store;
    }

    get injectedProps (): IInjectedProps {
        return this.props as IInjectedProps;
    }

    onAddDraftButtonClick = () => {
        const store = this.store;

        store.createDraft();
    }

    onDeleteDraftsButtonClick = () => {
        this.setState({
            isConfirmOpen: true
        });
    }

    onConfirmClose = () => {
        this.setState({
            isConfirmOpen: false
        });
    }

    onConfirm = () => {
        const self = this;
        const store = this.store;

        store.removeCheckedDrafts()
             .then(
                 function (result) {
                     Dialog.showMessage(
                         i18n.t('module.draft>deleteDraftSuccessMessage', {context: result.length.toString()})
                     );

                     return store.loadDraftList();
                 }
             )
             .then(
                 function () {
                     store.clearCheckedDrafts();

                     self.setState({
                         isConfirmOpen: false
                     });
                 }
             );
    }

    getDeleteTypeText () {
        let type = this.store.type;

        if (!type) {
            return null;
        }

        // Classifier | Data
        return i18n.t('module.draft>bulkDeleteType' + capitalize(type));
    }

    render () {
        let deleteTypeText = this.getDeleteTypeText();

        return (
            <>
                {!this.store.hasCheckedDrafts && this.store.editableDraft === null && (
                    <Button
                        leftIcon={'plus-circle'}
                        intent={INTENT.PRIMARY}
                        isMinimal={true}
                        onClick={this.onAddDraftButtonClick}
                        data-qaid={'createDraft'}
                        isDisabled={this.store.editableDraft !== null}
                    >
                        {i18n.t('module.draft>createNewDraftButtonText')}
                    </Button>
                )}

                {
                    this.store.hasCheckedDrafts ?
                        <Button
                            leftIcon={'trash2'}
                            intent={INTENT.DANGER}
                            isMinimal={true}
                            onClick={this.onDeleteDraftsButtonClick}
                            data-qaid={'deleteDraft'}
                        >
                            {i18n.t('module.draft>deleteDraftButtonText')}
                        </Button> :
                        null
                }

                {
                    this.store.type ?
                        <Confirm
                            confirmationMessage={i18n.t('module.draft>bulkDeleteDraftText', {type: deleteTypeText})}
                            header={i18n.t(`module.draft>bulkDeleteDraftTitle`, {type: deleteTypeText})}
                            isOpen={this.state.isConfirmOpen}
                            onClose={this.onConfirmClose}
                            onConfirm={this.onConfirm}
                        /> :
                        null
                }
            </>
        );
    }
}
