const path = require('path');
const {TypedCssModulesPlugin} = require('typed-css-modules-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    stories: ['../src/**/*.stories.tsx'],
    addons: ['@storybook/addon-knobs/register'],
    webpackFinal: async config => {
        config.resolve.alias = {
            '@unidata/core-app': path.resolve(__dirname, '..', '..', 'core-app', 'src'),
            '@unidata/icon': path.resolve(__dirname, '..', '..', 'icon', 'src')
        };
        config.module.rules = [
            {
                test: /\.(ts|tsx)$/,
                use: [
                    {
                        loader: require.resolve('ts-loader')
                    },
                    // Optional
                    {
                        loader: require.resolve('react-docgen-typescript-loader')
                    }
                ]
            },
            {
                test: /\.m\.css$/,
                use: [
                    {
                        loader: require.resolve('style-loader')
                    },
                    {
                        loader: require.resolve('css-loader'),
                        options: {
                            importLoaders: 1,
                            modules: {
                                localIdentName: '[local]__[hash:base64:6]'
                            },
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                exclude: /\.m\.css$/,
                use: [
                    {
                        loader: require.resolve('style-loader')
                    },
                    {
                        loader: require.resolve('css-loader'),
                        options: {
                            importLoaders: 1,
                            // modules: {
                            //     localIdentName: '[local]__[hash:base64:6]'
                            // },
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.m\.scss$/,
                use: [
                    {
                        loader: require.resolve('style-loader')
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: {
                                localIdentName: '[local]__[hash:base64:6]',
                            },
                            sourceMap: true,
                        },
                    },
                    'sass-loader'
                ],
            },
            {
                test: /\.scss$/,
                exclude: /\.m\.scss$/,
                use: [
                    {
                        loader: require.resolve('style-loader')
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            sourceMap: true,
                        },
                    },
                    'sass-loader'
                ],
            },
            {
                test: /\.(ttf|woff|woff2)$/,
                use: [
                    {
                        loader: require.resolve('file-loader')
                    }
                ],
                include: path.resolve(__dirname, '..', '..', '..', 'react-resources', 'fonts')
            }
        ];

        config.resolve.extensions.push('.ts', '.tsx');

        config.plugins.push(new TypedCssModulesPlugin({globPattern: 'src/**/*.css'}));

        config.plugins.push(
            new CopyWebpackPlugin({
                patterns: [{
                    from: path.resolve(__dirname, '..', '..', '..', 'react-resources'), to: 'react-resources'
                }]
            })
        );

        return config;
    }
};
