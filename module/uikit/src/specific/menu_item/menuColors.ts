/**
 * Constants that define the possible background colors of menu items. Needed for typing.
 * Must be kept up-to-date with styles (./menuItem.m. css - $section-colors)
 *
 * @author Brauer Ilya
 * @date 2020-04-15
 */

export const green = 'green';

export const blue = 'blue';
