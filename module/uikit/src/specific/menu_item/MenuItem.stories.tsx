import * as React from 'react';

import {withInfo} from '@storybook/addon-info';
import {boolean, select, text, withKnobs} from '@storybook/addon-knobs';
import {storyLayout} from '../../index.stories';
import {MenuItem} from './MenuItem';
import {iconMap, menuColorMap} from '../../utils/storybookEnums';
import {Button} from '../../index';

export default {
    title: 'MenuItem',
    decorators: [withInfo, storyLayout, withKnobs]
};

const readonlyGroup = 'Required';

export const pinnedMenu = () => {
    const [isShow, setVisible] = React.useState(false);

    return (
        <>
            <Button onClick={() => setVisible(!isShow)}>show</Button>
            <br/>
            <br/>
            <br/>
            <div style={{
                border: '1px solid red',
                width: '34px',
                height: '34px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'left'
            }}>

                <MenuItem
                    isShow={isShow}
                    icon={select('icon', iconMap(false), 'docs')}
                    title={text('title', 'Menu title')}
                    isMinimal={boolean('isMinimal', true, readonlyGroup)}
                    isActive={boolean('isActive', false)}
                    isDisabled={boolean('isDisabled', false)}
                    menuColor={select('menuColor', menuColorMap(true), undefined)}
                />

            </div>
        </>
    );
};

export const fullMenu = () => {
    return (

        <div style={{
            border: '1px solid red',
            width: '88px',
            height: '81px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'left'
        }}>

            <MenuItem
                icon={select('icon', iconMap(false), 'docs')}
                title={text('title', 'Menu title')}
                isMinimal={boolean('isMinimal', false, readonlyGroup)}
                isActive={boolean('isActive', false)}
                isDisabled={boolean('isDisabled', false)}
                menuColor={select('menuColor', menuColorMap(true), undefined)}
            />

        </div>
    );
};
