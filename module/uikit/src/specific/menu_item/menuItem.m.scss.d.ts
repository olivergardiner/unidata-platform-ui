export const container: string;
export const icon: string;
export const title: string;
export const isMinimal: string;
export const isActive: string;
export const isDisabled: string;
export const initHide: string;

export * from './menuColors';
