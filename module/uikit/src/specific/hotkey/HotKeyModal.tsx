/**
 * Modal window that opens for hockey
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-17
 */
import * as React from 'react';
import {GlobalHotKeyManager} from '@unidata/core-app';
import {Modal} from '../../component/modal/Modal';
import {IProps as IModalProps} from '../../component/modal/Modal.types';
import {SIZE} from '../../constants';

interface IProps extends Partial<IModalProps> {
    header: string;
    letterKey: string;
    ctrlKey?: boolean;
    altKey?: boolean;
    shiftKey?: boolean;
    isOpen?: boolean;
    onOpen?: () => void;
}

interface IState {
    isOpen: boolean;
}

export class HotKeyModal extends React.Component<IProps, IState> {

    constructor (props: IProps) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.onClose = this.onClose.bind(this);
        this.onOpen = this.onOpen.bind(this);
        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);

        let {letterKey, ctrlKey, altKey, shiftKey} = props;

        GlobalHotKeyManager.register(
            {letterKey, ctrlKey, altKey, shiftKey},
            this.show,
            true
        );
    }

    onClose () {
        this.hide();

        if (this.props.onClose) {
            this.props.onClose();
        }
    }

    onOpen () {
        if (this.props.onOpen) {
            this.props.onOpen();
        }
    }

    setOpen (open: boolean) {
        this.setState({isOpen: open});

        if (open) {
            this.onOpen();
        }
    }

    show () {
        this.setOpen(true);
    }

    hide () {
        this.setOpen(false);
    }

    render () {
        let {letterKey, ctrlKey, altKey, shiftKey, onOpen, ...restProps} = this.props;

        return (
            <Modal
                isOpen={this.state.isOpen}
                onClose={this.onClose}
                size={SIZE.MIDDLE}
                {...restProps}
            >
                {this.props.children}
            </Modal>
        );
    }

}
