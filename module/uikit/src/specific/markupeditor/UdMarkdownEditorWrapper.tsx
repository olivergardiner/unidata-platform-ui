/**
 *  Wrapper for the markdown editor component
 *
 * Storing value inside the wrapper, configuring the Converter
 *
 * @author Sergey Shishigin
 * @date 2019-11-22
 */
import * as React from 'react';
import {ReactMdeProps} from 'react-mde';
import {UdMarkdownEditor} from './UdMarkdownEditor';
import {MarkdownConverterFactory} from './markdown/MarkdownConverterFactory';
import {ConverterExtensionType} from './markdown/MarkdownConverterExtension';

interface IProps {
    defaultValue?: string;
}

interface IState {
    value: string;
}

const defaultProps = {
    selectedTab: 'write' as 'write'|'preview',
    onTabChange: () => {},
    onChange: () => {},
    value: undefined,
    generateMarkdownPreview: () => {}
};

export class UdMarkdownEditorWrapper extends React.Component<IProps & ReactMdeProps, IState> {
    static defaultProps = defaultProps;

    state: IState = {
        value: this.props.defaultValue || ''
    }

    constructor (props: IProps & ReactMdeProps) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.generateMarkdownPreview = this.generateMarkdownPreview.bind(this);
    }

    handleChange (value: string) {
        this.setState({
            value: value
        });

        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    private generateMarkdownPreview (markdown: string): Promise<React.ReactNode> {
        return Promise.resolve(
            MarkdownConverterFactory.buildConverter(
                {},
                [ConverterExtensionType.BUSINESS_TERM_LINK, ConverterExtensionType.CUSTOM_HTML_ATTRIBUTES])
                .makeHtml(markdown));
    }

    render () {
        let {value, onChange, generateMarkdownPreview, ...udMarkdownEditorProps} = this.props;

        return (
            <UdMarkdownEditor
                value={this.state.value}
                onChange={this.handleChange}
                generateMarkdownPreview={this.generateMarkdownPreview}
                {...udMarkdownEditorProps}/>
        );
    }
}
