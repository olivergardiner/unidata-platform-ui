import * as React from 'react';
import {UdMarkdownEditorWrapper} from './../../index';

import {withInfo} from '@storybook/addon-info';
import {withKnobs} from '@storybook/addon-knobs';
import {useState} from 'react';

export default {
    title: 'MarkdownEditor',
    decorators: [withInfo, withKnobs]
};

export const simpleMarkdownEditor = () => {

    return (
        <UdMarkdownEditorWrapper
            minEditorHeight={400}
            maxEditorHeight={400}
            minPreviewHeight={400}
        />
    );
};
