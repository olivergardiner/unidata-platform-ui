/**
 * Markup output area showdown (https://github.com/showdownjs/showdown)
 *
 * @author Sergey Shishigin
 * @date 2019-10-23
 */
import * as showdown from 'showdown';
import {ConverterOptions} from 'showdown';
import * as React from 'react';
import {ConverterExtensionType} from './MarkdownConverterExtension';
import {MarkdownConverterFactory} from './MarkdownConverterFactory';

interface IProps {
    className?: string;
    text: string;
    options?: ConverterOptions;
    extensionNames?: ConverterExtensionType[];
}

export class MarkdownArea extends React.Component<IProps> {
    private converter: showdown.Converter;

    private readonly myRef: any;

    constructor (props: IProps) {
        super(props);

        this.converter = MarkdownConverterFactory.buildConverter(this.props.options, this.props.extensionNames);
        this.myRef = React.createRef();
    }

    componentDidMount (): void {
        this.initEventListeners();
    }

    componentDidUpdate (prevProps: Readonly<IProps>): void {
        if (this.props.text !== prevProps.text) {
            this.initEventListeners();
        }
    }

    private initEventListeners () {
        let nodes: HTMLElement[];

        nodes = this.myRef.current.querySelectorAll('a[href="#"]');
        nodes.forEach(function (node: HTMLElement) {
            node.addEventListener('click', function (e: Event) {
                e.preventDefault();
            });
        });
    }

    get className () {
        const {className} = this.props;

        let classNameNew = 'ud-markdown-area';

        if (className) {
            classNameNew = classNameNew + ' ' + className;
        }

        return classNameNew;
    }

    render () {
        return <div
            ref={this.myRef}
            className={this.className}
            dangerouslySetInnerHTML={{__html: this.converter.makeHtml(this.props.text)}}
        />;
    }
}
