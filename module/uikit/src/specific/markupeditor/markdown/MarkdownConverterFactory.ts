/**
 * Converter generation factory
 *
 * @author Sergey Shishigin
 * @date 2019-11-22
 */
import * as showdown from 'showdown';
import {ConverterOptions, ShowdownExtension} from 'showdown';
import {MarkdownConverterExtension, ConverterExtensionType} from './MarkdownConverterExtension';
import uniq from 'lodash/uniq';

export class MarkdownConverterFactory {
    public static buildConverter (options?: ConverterOptions, extensionNames?: ConverterExtensionType[]): showdown.Converter {
        let converter: showdown.Converter;

        return new showdown.Converter(MarkdownConverterFactory.buildConverterOptions(options, extensionNames));
    }

    public static buildConverterOptions (options?: ConverterOptions, extensionNames?: ConverterExtensionType[]): ConverterOptions {
        let newOptions: ConverterOptions = {},
            extensions: ShowdownExtension[] = [];

        if (extensionNames) {
            extensions = extensionNames.map((typeName: ConverterExtensionType): ShowdownExtension => {
                return MarkdownConverterExtension.getExtension(typeName);
            });
        }

        newOptions = Object.assign(newOptions, options);
        newOptions.extensions = (newOptions.extensions && newOptions.extensions.length) ? newOptions.extensions : [];
        newOptions.extensions = uniq(newOptions.extensions.concat(extensions));

        return newOptions;
    }

}
