/**
 * Extensions for showdown (https://github.com/showdownjs/showdown)
 *
 * @author Sergey Shishigin
 * @date 2019-10-23
 */
import {ShowdownExtension} from 'showdown';

/**
 * Type of Converter extensions
 *
 * @author Sergey Shishigin
 * @date 2019-10-23
 */
export enum ConverterExtensionType {
    BUSINESS_TERM_LINK = 'BUSINESS_TERM_LINK',
    CUSTOM_HTML_ATTRIBUTES = 'CUSTOM_HTML_ATTRIBUTES'
}

export class MarkdownConverterExtension {
    public static getExtension (type: ConverterExtensionType): ShowdownExtension {
        switch (type) {
            case ConverterExtensionType.BUSINESS_TERM_LINK:
                return this.getBusinessTermLinkExtension();
            case ConverterExtensionType.CUSTOM_HTML_ATTRIBUTES:
                return this.getCustomHtmlAttributesExtension();
        }
    }

    /**
     * Showdown extension
     * replace etalonId type [Fis|594cc896-e03a-11e9-9864-87ba01ce01e0]
     * to [Fis](#main?section=businessglossary|etalon?etalonId=594cc896-e03a-11e9-9864-87ba01ce01e0)(data-tip="594cc896-e03a-11e9-9864-87ba01ce01e0" data-for="businessTerm")
     *
     * This is a markdown link + description of the attributes of the a tag
     */
    private static getBusinessTermLinkExtension (): ShowdownExtension {
        return {
            type: 'lang',
            regex: /\[(.+?)\|([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})\]/g,
            replace: '[$1](#main?section=businessglossary|etalon?etalonId=$2)(data-tip="$2" data-for="businessTerm")'
        };
    }

    /**
     * Showdown extension
     * Inserts the properties of tag a, described as (data-tip="594cc896-e03a-11e9-9864-87ba01ce01e0" data-for="businessTerm"), inside the a tag
     *
     * see https://guides.codechewing.com/add-custom-attributes-to-anchor-html-tag-showdown
     */
    private static getCustomHtmlAttributesExtension (): ShowdownExtension {
        return {
            type: 'output',
            // regex: /(<a.+?<\/a>)\((.+=".+" ?)+\)/g,
            regex: /(<a.+?)>(.+?)(<\/a>)\((.+?=".+?" ?)\)/g,
            replace: '$1 $4>$2$3'
        };
    }
}
