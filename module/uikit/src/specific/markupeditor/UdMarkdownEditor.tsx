/**
 *  Markdown component-editor
 *
 * @author Sergey Shishigin
 * @date 2019-11-22
 */
import * as React from 'react';
import ReactMde, {L18n, ReactMdeProps} from 'react-mde';
import {i18n} from '@unidata/core-app';
import 'react-mde/lib/styles/css/react-mde-all.css';

interface IProps {
    className?: string;
}

interface IState {
    selectedTab: 'write'|'preview';
}

const defaultProps = {
    selectedTab: 'write' as 'write'|'preview',
    onTabChange: () => {}
};

export class UdMarkdownEditor extends React.Component<IProps & ReactMdeProps, IState> {
    static defaultProps = defaultProps;

    baseCls: string = 'ud-markdown-editor';

    state: IState = {
        selectedTab: defaultProps.selectedTab
    };

    constructor (props: IProps & ReactMdeProps) {
        super(props);
        this.handleTabChange = this.handleTabChange.bind(this);
    }

    handleTabChange (tab: 'write'|'preview') {
        this.setState({
            selectedTab: tab
        });
    }

    get l18n (): L18n {
        return {
            preview: i18n.t('markdown>preview'),
            write: i18n.t('markdown>write')
        };
    }

    get className (): string {
        let className: string = this.baseCls;

        if (this.props.className) {
            className += ' ' + this.props.className;
        }

        return className;
    }

    render () {
        let {selectedTab, onTabChange, l18n, className, ...reactMdeProps} = this.props;

        return (
            <ReactMde
                selectedTab={this.state.selectedTab}
                onTabChange={this.handleTabChange}
                l18n={this.l18n}
                className={this.className}
                {...reactMdeProps}/>
        );
    }
}
