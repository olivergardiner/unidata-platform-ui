/**
 * Test base class of unidata components
 *
 * @author Ivan Marshalkin
 * @date 2019-05-23
 */

import * as React from 'react';

/**
 * IP interface of properties embedded (inject) in the component
 * P-interface for properties of transmitted components
 * S-component state interface
 */
/**
 * @deprecated
 */
export class UdComponent<IP = {}, P = {}, S = {}> extends React.Component<P, S> {
    get ownProps (): P {
        return this.props as P;
    }

    get injectedProps (): IP {
        return this.props as any;
    }
}
