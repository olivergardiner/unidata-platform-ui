export enum INTENT {
    DEFAULT = 'default',
    PRIMARY = 'primary',
    INFO = 'info',
    SUCCESS = 'success',
    WARNING = 'warning',
    DANGER = 'danger',
    SECONDARY = 'secondary'
}

export enum SIZE {
    EXTRA_SMALL = 'xs',
    SMALL = 's',
    MIDDLE = 'm',
    LARGE = 'l',
    EXTRA_LARGE = 'xl'
}

export enum ALIGN {
    LEFT = 'left',
    CENTER = 'center',
    RIGHT = 'right',
}

export enum TRIGGER {
    FOCUS = 'focus',
    CONTEXT_MENU = 'contextMenu',
    CLICK = 'click',
    CLICK_TARGET_ONLY = 'clickTargetOnly',
    CLICK_TARGET_ONCE = 'clickTargetOnce',
    HOVER = 'hover',
    HOVER_TARGET_ONLY = 'hoverTargetOnly'
}

export enum PLACEMENT {
    AUTO = 'auto',
    AUTO_START = 'auto-start',
    AUTO_END = 'auto-end',
    TOP = 'top',
    TOP_START = 'top-start',
    TOP_END = 'top-end',
    LEFT = 'left',
    LEFT_START = 'left-start',
    LEFT_END = 'left-end',
    RIGHT = 'right',
    RIGHT_START = 'right-start',
    RIGHT_END = 'right-end',
    BOTTOM = 'bottom',
    BOTTOM_START = 'bottom-start',
    BOTTOM_END = 'bottom-end',
}

export enum GROUP_TYPE {
    HORIZONTAL = 'horizontal',
    VERTICAL = 'vertical'
}

export type Size = 'small' | 'large' | 'middle' | undefined;

export type SpinSize = 'small' | 'large' | 'default' | undefined;

export enum POPOVER_BOUNDARIES {
    SCROLL_PARENT = 'scrollParent',
    VIEWPORT = 'viewport',
    WINDOW = 'window',
}
