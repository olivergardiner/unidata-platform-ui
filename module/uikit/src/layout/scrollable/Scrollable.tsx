/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-04-26
 */

import * as React from 'react';
import Scrollbars, {ScrollbarProps} from 'react-custom-scrollbars';

export class Scrollable extends React.Component<ScrollbarProps> {

    render () {
        return (
            <Scrollbars {...this.props}>
                {this.props.children}
            </Scrollbars>
        );
    }
}
