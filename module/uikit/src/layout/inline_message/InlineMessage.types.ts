/**
 * Enumeration for message types
 * @author Sergey Shishigin
 * @date 2019-07-17
 */

export enum UdInlineMessageType {
    DEFAULT = 'default',
    INFO = 'info',
    WARNING = 'warning',
    ERROR = 'error'
}
