import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {select, withKnobs} from '@storybook/addon-knobs';
import {storyLayout} from '../../index.stories';
import {InlineMessage, INTENT, UdInlineMessageType} from './../../index';
import {iconMap} from '../../utils/storybookEnums';

export default {
    title: 'InlineMessage',
    decorators: [withInfo, storyLayout, withKnobs]
};

const typeMap = Object.entries(UdInlineMessageType).reduce((result: any, [key, value]) => {
    result[key] = value;

    return result;
}, {});

export const simpleInlineMessage = () => {

    return (
        <InlineMessage
            type={select('type', typeMap, UdInlineMessageType.DEFAULT)}
            iconName={select('iconName', iconMap(true), undefined)}
        >
            InlineMessage - some data
        </InlineMessage>
    );
};
