/**
 * The Component "Message"
 *
 * @author Sergey Shishigin
 * @date 2019-07-17
 *
 * In explanation Semantic React UI Message
 * https://react.semantic-ui.com/collections/message/
 * https://github.com/Semantic-Org/Semantic-UI-React/blob/master/src/collections/Message/Message.js
 */
import * as React from 'react';
import {CSSProperties} from 'react';
import {UdInlineMessageType} from './InlineMessage.types';
import {Icon, SIZE} from '../../index';
import {IconName, IconIntent} from '@unidata/icon';

import './inlineMessage.scss';

interface IProps {
    type: UdInlineMessageType;
    // This property is not used yet, but it will be useful in the future-the panel header for message
    // title?: string;
    iconName?: IconName;
    iconSize?: SIZE;
    style?: CSSProperties;
}

const defaultProps = {
    type: UdInlineMessageType.DEFAULT,
    iconSize: SIZE.LARGE
};

export {
    UdInlineMessageType
};

export class InlineMessage extends React.PureComponent<IProps> {
    static defaultProps = defaultProps;

    readonly baseCls: string = 'ud-inline-message';
    readonly clsDelimiter: string = '-';

    get iconCls () {
        let baseIconCls = this.buildCls('icon');
        let clsSuffix = this.props.iconSize;

        return `${baseIconCls} ${baseIconCls}_${clsSuffix}`;

    }

    get contentCls () {
        return this.buildCls('content');
    }

    get componentClass () {
        let {type, iconName} = this.props,
            className: string[] = [this.baseCls];

        if (type !== UdInlineMessageType.DEFAULT) {
            className.push(this.buildCls('type' + this.clsDelimiter + type));
        }

        if (type === UdInlineMessageType.DEFAULT && !iconName) {
            className.push(this.buildCls('noicon'));
        }

        return className.join(' ');
    }

    buildCls (name: string) {
        return [this.baseCls, name].join(this.clsDelimiter);
    }

    getIconNameByType (type: UdInlineMessageType): IconName | undefined {
        switch (type) {
            case UdInlineMessageType.INFO:
                return 'notification-circle';
            case UdInlineMessageType.WARNING:
                return 'warning';
            case UdInlineMessageType.ERROR:
                return 'warning';
            default:
                return undefined;
        }
    }

    render () {
        const {type, children, /*title,*/ iconName, style, iconSize} = this.props;

        let iconName1: IconName | undefined;

        iconName1 = iconName || this.getIconNameByType(type);

        return (
            <div className={this.componentClass} style={style}>
                {iconName1 && (
                    <div className={this.iconCls}>
                        <Icon
                            name={iconName1}
                            size={iconSize}
                            intent={(type === UdInlineMessageType.INFO || type === UdInlineMessageType.ERROR) ?
                                IconIntent.GHOST :
                                IconIntent.DEFAULT
                            }
                        />
                    </div>
                )}
                <div className={this.contentCls}>
                    {children}
                </div>
            </div>
        );
    }
}
