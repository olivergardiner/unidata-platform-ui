export const sider: string;

export const siderContainer: string;

export const siderHeader: string;

export const headerText: string;

export const rotated: string;

export const siderList: string;

export const siderListContainer: string;

export const tableHeader: string;

export const hidden: string;

export const tableRow: string;

export const selectedRow: string;

export const inactiveRow: string;
