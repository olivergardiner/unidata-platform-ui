import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {boolean, text, withKnobs} from '@storybook/addon-knobs';
import {storyLayout} from '../../index.stories';
import {MenuSider} from './../../index';

export default {
    title: 'MenuSider',
    decorators: [withInfo, storyLayout, withKnobs]
};

export const simpleMenuSider = () => {

    return (
        <MenuSider
            title={'menu title'}
            onClick={() => console.log('click')}
            columns={[]}
            data={[]}
        />
    );
};
