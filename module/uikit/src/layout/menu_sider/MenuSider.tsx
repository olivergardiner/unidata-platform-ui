/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-04-23
 */

import * as React from 'react';
import {ReactNode} from 'react';
import {Layout} from 'antd';
import ReactTable from 'react-table';
import {ActionsPanel} from './actions_panel/ActionsPanel';
import {FiltersPanel} from './filters_panel/FiltersPanel';
import {Button, INTENT, ScrollableTableBody} from '../../index';

import * as styles from './menuSider.m.scss';

export interface IMenuSiderProps {
    title: string;
    data: any[];
    columns: any[];
    hideHeader?: boolean;
    currentItem?: any;
    width?: number;
    collapsible?: boolean;
    collapsedWidth?: number;
    siderHeaderHeight?: number;
    onClick: (rowInfo: any) => void;
    onAddClick?: () => void;
    filters?: any | JSX.Element[];
    actions?: ReactNode;
    'data-qaid'?: string;
}

interface IState {
    collapsed: boolean;
}

const defaultProps = {
    hideHeader: false,
    width: 340 as number,
    collapsedWidth: 34 as number,
    collapsible: true as boolean
};

export class MenuSider extends React.Component<IMenuSiderProps, IState> {

    static defaultProps = defaultProps;

    state = {
        collapsed: false
    };

    get siderProps () {
        const {width, collapsible, collapsedWidth} = this.props,
            {collapsed} = this.state;

        return {
            width: width,
            collapsedWidth: collapsedWidth,
            collapsible: collapsible,
            collapsed: collapsed,
            trigger: null,
            className: `ud-page-sider ${styles.sider}`
        };
    }

    get tableProps () {
        const {data, columns, onClick, currentItem} = this.props;

        return {
            className: styles.siderList,
            multiSort: false,
            minRows: 0,
            style: {display: 'block'},
            resizable: false,
            showPagination: false,
            pageSize: data.length,
            columns: columns,
            data: data,
            getTrProps: (tableState: any, rowInfo: any) => {
                return {
                    onClick: (e: any) => {
                        onClick(rowInfo.original.model);
                    },
                    className: [
                        styles.tableRow,
                        (rowInfo.original.model === currentItem) ? styles.selectedRow : styles.inactiveRow
                    ].join(' ')
                };
            },
            getTheadThProps: () => {
                return {
                    className: [
                        styles.tableHeader,
                        this.props.hideHeader ? styles.hidden : ''
                    ].join(' ')
                };
            },
            TbodyComponent: ScrollableTableBody
        };
    }

    get headerClass () {
        return [
            styles.siderHeader,
            this.state.collapsed ? styles.rotated : ''
        ].join(' ');
    }

    handleOnCollapseClick = () => {
        this.setState((prevState) => ({
            collapsed: !prevState.collapsed
        }));
    }

    render () {
        const {title, collapsible, filters, actions, siderHeaderHeight} = this.props;
        const {collapsed} = this.state;

        return (
            <Layout.Sider {...this.siderProps}>
                <div className={styles.siderContainer} data-qaid={this.props['data-qaid']}>
                    <div className={this.headerClass} style={{height: siderHeaderHeight}}>
                        {!collapsed && (
                            <span className={styles.headerText}>
                                {title}
                            </span>
                        )}
                        {Boolean(collapsible) && (
                            <Button
                                isGhost={true}
                                isMinimal={true}
                                isRound={true}
                                onClick={this.handleOnCollapseClick}
                                leftIcon={collapsed ? 'chevron-right-circle' : 'chevron-left-circle'}
                            />
                        )}
                        {collapsed && (
                            <span className={[styles.headerText, styles.rotated].join(' ')}>
                                {title}
                            </span>
                        )}
                    </div>

                    {!collapsed && (
                        <>
                            {filters && <FiltersPanel filters={filters} />}

                            <div
                                className={`ud-sider-list-container ${styles.siderListContainer}`}
                                data-qaid={this.props['data-qaid'] + 'Table'}
                            >
                                <ReactTable
                                    NoDataComponent={() => null}
                                    {...this.tableProps}
                                />
                            </div>
                            {actions && <ActionsPanel actions={actions} />}
                        </>
                    )}
                </div>
            </Layout.Sider>
        );
    }
}
