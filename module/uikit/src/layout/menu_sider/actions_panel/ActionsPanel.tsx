/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-04-23
 */

import * as React from 'react';
import styles from './actionsPanel.m.css';

interface IActionsPanelProps {
    actions: React.ReactNode;
    panelHeight?: number;
}

const defaultProps = {
    panelHeight: 68
};

export class ActionsPanel extends React.Component<IActionsPanelProps> {
    static defaultProps = defaultProps;

    render () {
        const {actions, panelHeight} = this.props;

        return (
            <div style={{height: panelHeight}}>
                <div className={styles.actionsGroup}>
                    {actions}
                </div>
            </div>
        );
    }
}
