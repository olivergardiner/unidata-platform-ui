/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-04-26
 */

import * as React from 'react';
import styles from './filtersPanel.m.css';

interface IFiltersPanelProps {
    filters: any[] | Element;
}


export class FiltersPanel extends React.Component<IFiltersPanelProps> {

    render () {
        const {filters} = this.props;

        return (
            <div className={styles.filtersPanel}>
                {/*<div className='ud-sider-filters-group'>*/}
                    {filters}
                {/*</div>*/}
            </div>
        );
    }
}
