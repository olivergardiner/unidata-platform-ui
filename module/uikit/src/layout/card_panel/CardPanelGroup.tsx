import * as React from 'react';

import {GROUP_TYPE} from '../../constants';

import * as styles from './cardPanel.m.scss';

import {joinClassNames} from '../../utils/joinClassNames';

type IProps = {
    groupType: GROUP_TYPE;
};

export class CardPanelGroup extends React.Component<IProps> {
    render () {
        const className = joinClassNames(
            styles.cardContainer,
            [styles.horizontalGroup, this.props.groupType === GROUP_TYPE.HORIZONTAL],
            [styles.verticalGroup, this.props.groupType === GROUP_TYPE.VERTICAL]
        );

        return (
            <div className={className}>
                {this.props.children}
            </div>
        );
    }
}
