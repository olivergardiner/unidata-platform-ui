/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-26
 */

import * as React from 'react';
import {Icon, joinClassNames} from '../../index';

import styles from './cardPanel.m.scss';
import {CardPanelGrid} from './CardPanelGrid';
import {CardPanelGroup} from './CardPanelGroup';

type CardPanelTheme = 'default' | 'light';

export interface IProps {
    children?: React.ReactNode;
    title?: React.ReactNode;
    internal?: boolean; // Minimalistic header
    isCollapsed?: boolean;
    scrollY?: boolean;
    hideHeader?: boolean;
    extraButtons?: React.ReactNode;
    noBodyPadding?: boolean;
    contentAutoHeight?: boolean;
    'data-qaid'?: string;
    onToggleCollapse?: (isCollapsed: boolean) => void;
    mountOnExpand?: boolean; // we mount children only when we open the panel
    theme?: CardPanelTheme;
}

interface IState {
    isCollapsed: boolean;
}

const defaultProps = {
    hideHeader: false,
    scrollY: true
};

export class CardPanel extends React.Component<IProps, IState> {
    static Group = CardPanelGroup;
    static Grid = CardPanelGrid;
    static defaultProps = defaultProps;

    state = {
        isCollapsed: true
    };

    get drawHeader () {
        return this.props.title || this.props.extraButtons;
    }

    get extraButtons () {
        const {extraButtons, isCollapsed} = this.props;

        return (
            <React.Fragment>
                {extraButtons && extraButtons}
                {isCollapsed !== undefined &&
                <div
                    className={styles.collapseIcon}
                    onClick={this.handleCollapseClick}
                    style={{transform: this.state.isCollapsed ? 'rotate(-90deg)' : ''}}
                >
                    <Icon name='down'/>
                </div>
                }
            </React.Fragment>
        );
    }

    get renderContent () {
        const {noBodyPadding, contentAutoHeight, children, title, extraButtons, mountOnExpand} = this.props;

        const classNames = joinClassNames(
            styles.cardContent,
            [styles.internal, this.props.internal === true],
            [styles.noBodyPadding, noBodyPadding === true],
            [styles.contentAutoHeight, contentAutoHeight === true],
            [styles.isCollapsed, this.state.isCollapsed === true],
            [styles.withoutHeader, !title && !extraButtons],
        );

        return (
            <div className={classNames}>
                {children}
            </div>
        );
    }

    get cardStyle () {
        const cardStyle = joinClassNames(
            styles.cardPanel,
            [styles.internal, this.props.internal === true],
            [styles.contentAutoHeight, this.props.contentAutoHeight === true]
        );

        return cardStyle;
    }

    get cardHeaderStyle () {
        const cardHeaderStyle = joinClassNames(
            styles.cardHeader,
            [styles.themeDefault, !this.props.theme || this.props.theme === 'default'],
            [styles.themeLight, this.props.theme === 'light'],
            [styles.isCollapsed, this.state.isCollapsed === true]
        );

        return cardHeaderStyle;
    }

    componentDidMount () {
        if (this.props.isCollapsed === undefined || this.props.isCollapsed === false) {
            this.setState({isCollapsed: false});
        }
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        if (
            prevProps.isCollapsed !== undefined &&
            prevState.isCollapsed !== this.props.isCollapsed &&
            this.props.isCollapsed !== undefined
        ) {
            this.setState({isCollapsed: this.props.isCollapsed});
        }
    }

    handleCollapseClick = (e: React.SyntheticEvent<HTMLDivElement>) => {
        e.stopPropagation();
        this.setState((prevState) => ({
            isCollapsed: !prevState.isCollapsed
        }), () => {
            if (this.props.onToggleCollapse) {
                this.props.onToggleCollapse(this.state.isCollapsed);
            }
        });
    };

    render () {
        const {title, extraButtons, isCollapsed} = this.props;

        return (
            <div
                className={this.cardStyle}
                data-qaid={this.props['data-qaid']}
            >
                {this.drawHeader && <div
                    className={this.cardHeaderStyle}
                    onClick={isCollapsed !== undefined ? this.handleCollapseClick : undefined}
                >
                    {(title || extraButtons) && (
                        <>
                            <div className={styles.cardTitle}>{title}</div>
                            <div className={styles.extraButtonsContainer}>{this.extraButtons}</div>
                        </>
                    )}
                </div>
                }
                {this.props.mountOnExpand === true && this.state.isCollapsed === true ? null : this.renderContent}
            </div>
        );
    }
}
