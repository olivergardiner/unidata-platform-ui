/**
 * Container component for displaying cardPanel in a dynamic grid
 * allows you to configure the width, height, and position of the cardPanel, which are its descendants
 *
 * @author Brauer Ilya
 * @date 2020-04-15
 */

import * as React from 'react';
import {ReactElement} from 'react';
import * as GridLayout from 'react-grid-layout';

import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';

import * as styles from './cardPanel.m.scss';

import {Layout} from 'react-grid-layout';
import {propsGetter} from '../../utils/propsGetter';
import {joinClassNames} from '../../index';

type IProps = {
    isDraggable?: boolean;
    isResizable?: boolean;
} & Partial<DefaultProps>;

const defaultProps = {
    rowHeight: 150,
    isDraggable: true,
    isResizable: true
};

type DefaultProps = Readonly<typeof defaultProps>;

const getProps = propsGetter(defaultProps);

interface IState {
    width: number;
}

export class CardPanelGrid extends React.Component<IProps, IState> {

    static defaultProps = defaultProps;

    gridLayoutContainer = React.createRef<HTMLDivElement>();

    state = {
        width: 0
    };

    componentDidMount (): void {
        this.calculateGridWidth(this.state.width);
        window.addEventListener('resize', this.onResize);
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        this.calculateGridWidth(prevState.width);
    }

    componentWillUnmount (): void {
        window.removeEventListener('resize', this.onResize);
    }

    onResize = () => {
        if (this.gridLayoutContainer.current) {
            const width = this.gridLayoutContainer.current.clientWidth;

            this.setState({width});
        }
    }

    calculateGridWidth (prevWidth: number) {
        if (this.gridLayoutContainer.current) {
            const width = this.gridLayoutContainer.current.clientWidth;

            if (width !== prevWidth) {
                this.setState({width});
            }
        }
    }

    render () {
        const {rowHeight} = getProps(this.props);

        const containerClassName = joinClassNames(
            [styles.gridLayout, Boolean(this.props.isDraggable || this.props.isResizable)]
        );

        return (
            <div ref={this.gridLayoutContainer} className={containerClassName}>
                <GridLayout
                    width={this.state.width}
                    rowHeight={rowHeight}
                    isDraggable={this.props.isDraggable}
                    isResizable={this.props.isResizable}
                >
                    {React.Children.map(this.props.children, (Child: ReactElement<{ dataGrid?: Layout; 'data-grid'?: Layout }>) => {

                        if (React.isValidElement(Child)) {
                            const itemProp = Child.props.dataGrid || Child.props['data-grid'] || {};

                            return (
                                <div data-grid={{...itemProp}}>
                                    {Child}
                                </div>
                            );
                        }

                        return null;
                    })}
                </GridLayout>
            </div>
        );

    }
}
