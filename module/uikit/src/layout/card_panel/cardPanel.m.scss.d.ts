declare const styles: {
  readonly "cardContainer": string;
  readonly "verticalGroup": string;
  readonly "gridLayout": string;
  readonly "cardPanel": string;
  readonly "cardHeader": string;
  readonly "noOverflowY": string;
  readonly "isCollapsed": string;
  readonly "horizontalGroup": string;
  readonly "noBodyPadding": string;
  readonly "internal": string; // no background and borders
  readonly "contentAutoHeight": string;
  readonly "cardTitle": string;
  readonly "extraButtonsContainer": string;
  readonly "cardContent": string;
  readonly "withoutHeader": string;
  readonly "collapseIcon": string;
  readonly "themeDefault": string;
  readonly "themeLight": string;
};
export = styles;

