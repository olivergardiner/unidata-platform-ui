import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {boolean, select, text, withKnobs} from '@storybook/addon-knobs';
import {storyLayout} from '../../index.stories';
import {Button, CardPanel, GROUP_TYPE} from './../../index';
import {groupTypeMap} from '../../utils/storybookEnums';

export default {
    title: 'CardPanel',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const simpleCardPanel = () => {

    return (
        <CardPanel
            title={text('title', 'Card title')}
            internal={boolean('internal', false)}
            isCollapsed={boolean('isCollapsed', false)}
            scrollY={boolean('scrollY', false)}
            noBodyPadding={boolean('noBodyPadding', false)}
            extraButtons={(<Button leftIcon={'plus-circle'}/>)}
        >
            body - list of data
        </CardPanel>
    );
};

export const cardPanelGroup = () => {
    return (
        <CardPanel.Group groupType={select('groupType', groupTypeMap(false), GROUP_TYPE.HORIZONTAL)}>
            <CardPanel
                title={text('title', 'Card title1')}
                internal={boolean('internal', false)}
                isCollapsed={boolean('isCollapsed', false)}
                scrollY={boolean('scrollY', false)}
                noBodyPadding={boolean('noBodyPadding', false)}
                extraButtons={(<Button leftIcon={'plus-circle'}/>)}
            >
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
            </CardPanel>
            <CardPanel
                title={text('title2', 'Card title2')}
                internal={boolean('internal', false)}
                isCollapsed={boolean('isCollapsed', false)}
                scrollY={boolean('scrollY', false)}
                noBodyPadding={boolean('noBodyPadding', false)}
                extraButtons={(<Button leftIcon={'plus-circle'}/>)}
            >
                <div>body - list of data</div>
                <div>body - list of data</div>
            </CardPanel>
            <CardPanel
                title={text('title3', 'Card title3')}
                internal={boolean('internal', false)}
                isCollapsed={boolean('isCollapsed', false)}
                scrollY={boolean('scrollY', false)}
                noBodyPadding={boolean('noBodyPadding', false)}
                extraButtons={(<Button leftIcon={'plus-circle'}/>)}
            >
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
                <div>body - list of data</div>
            </CardPanel>
        </CardPanel.Group>
    );
};

export const cardPanelGrid = () => {
    return (
        <CardPanel.Grid>
            <React.Fragment data-grid={{
                i: 'firstBlock',
                x: 0,
                y: 0,
                w: 6,
                h: 5,
                minW: 3
            }}>
                <CardPanel
                    title={text('title', 'Card title1')}
                    internal={boolean('internal', false)}
                    isCollapsed={boolean('isCollapsed', false)}
                    scrollY={boolean('scrollY', false)}
                    noBodyPadding={boolean('noBodyPadding', false)}
                    extraButtons={(<Button leftIcon={'plus-circle'}/>)}
                >
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                </CardPanel>
            </React.Fragment>
            <React.Fragment data-grid={{
                i: 'secondBlock',
                x: 6,
                y: 0,
                w: 6,
                h: 2,
                minW: 3
            }}>
                <CardPanel
                    title={text('title2', 'Card title2')}
                    internal={boolean('internal', false)}
                    isCollapsed={boolean('isCollapsed', false)}
                    scrollY={boolean('scrollY', false)}
                    noBodyPadding={boolean('noBodyPadding', false)}
                    extraButtons={(<Button leftIcon={'plus-circle'}/>)}
                >
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                </CardPanel>
            </React.Fragment>
            <React.Fragment data-grid={{
                i: 'thirdBlock',
                x: 6,
                y: 2,
                w: 6,
                h: 2,
                minW: 3
            }}>
                <CardPanel
                    title={text('title3', 'Card title3')}
                    internal={boolean('internal', false)}
                    isCollapsed={boolean('isCollapsed', false)}
                    scrollY={boolean('scrollY', false)}
                    noBodyPadding={boolean('noBodyPadding', false)}
                    extraButtons={(<Button leftIcon={'plus-circle'}/>)}
                >
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                    <div>body - list of data</div>
                </CardPanel>
            </React.Fragment>
            <React.Fragment data-grid={{
                i: 'forthBlock',
                x: 0,
                y: 5,
                w: 6,
                h: 2,
                minW: 3
            }}>
                <CardPanel
                    title={text('title4', 'Card title4')}
                    internal={boolean('internal', false)}
                    isCollapsed={boolean('isCollapsed', false)}
                    scrollY={boolean('scrollY', false)}
                    noBodyPadding={boolean('noBodyPadding', false)}
                    extraButtons={(<Button leftIcon={'plus-circle'}/>)}
                >
                    body - list of data
                </CardPanel>
            </React.Fragment>
            <React.Fragment data-grid={{
                i: 'forthBlock',
                x: 6,
                y: 5,
                w: 6,
                h: 2,
                minW: 3
            }}>
                <CardPanel
                    title={text('title5', 'Card title5')}
                    internal={boolean('internal', false)}
                    isCollapsed={boolean('isCollapsed', false)}
                    scrollY={boolean('scrollY', false)}
                    noBodyPadding={boolean('noBodyPadding', false)}
                    extraButtons={(<Button leftIcon={'plus-circle'}/>)}
                >
                    body - list of data
                </CardPanel>
            </React.Fragment>
        </CardPanel.Grid>
    );
};
