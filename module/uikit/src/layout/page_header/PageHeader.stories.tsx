import * as React from 'react';
import {Button, INTENT, PageHeader} from './../../index';
import {boolean, select, text, withKnobs} from '@storybook/addon-knobs';
import {withInfo} from '@storybook/addon-info';
import {iconMap} from '../../utils/storybookEnums';

export default {
    title: 'PageHeader',
    decorators: [withInfo, withKnobs]
};

export const simplePageHeader = () => {
    return (
        <PageHeader
            iconType={select('iconType', iconMap(false), 'setting')}
            groupSectionTitle={text('groupSectionTitle', 'groupTitle')}
            sectionTitle={text('sectionTitle', 'section')}
            extraButtons={(
                <>
                    <Button intent={INTENT.SECONDARY}>Button 1</Button>
                    <Button intent={INTENT.PRIMARY}>Button 1</Button>
                </>
            )}
            // horizontalMenu={(
            //     <>
            //         <div>qwe</div>
            //     </>
            // )}
        />
    );
};
