declare const styles: {
  readonly "pageHeader": string;
  readonly "withHorizontalMenu": string;
  readonly "rowTitle": string;
  readonly "buttonsContainer": string;
  readonly "titleBlock": string;
  readonly "iconContainer": string;
  readonly "groupTitle": string;
  readonly "titleContainer": string;
  readonly "primaryTitle": string;
  readonly "secondaryTitle": string;
  readonly "horizontalContainer": string;
};
export = styles;

