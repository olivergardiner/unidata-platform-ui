/**
 * empty comment line Sergey Shishigin
 *
 * @author: Sergey Shishigin
 * @date: 2019-04-23
 */

import * as React from 'react';
import {Layout} from 'antd';
import {Icon, SIZE} from '../..';
import {IconName} from '@unidata/icon';

import styles from './pageHeader.m.css';

interface IProps {
    groupSectionTitle: string;
    sectionTitle: string;
    itemTitle?: React.ReactNode;
    horizontalMenu?: JSX.Element;
    extraButtons?: JSX.Element;
    iconType?: IconName;
    // cls?: string[] | string;
}

export class PageHeader extends React.Component<IProps> {

    get className () {
        const {horizontalMenu} = this.props;

        let classes: string[] = [styles.pageHeader];

        // if (cls instanceof Array) {
        //     classes.push(...cls);
        // } else if (cls) {
        //     classes.push(cls);
        // }

        if (horizontalMenu) {
            classes.push(styles.withHorizontalMenu);
        }

        return classes.join(' ');
    }

    render () {

        const {groupSectionTitle, sectionTitle, itemTitle, extraButtons, iconType, horizontalMenu} = this.props;

        return (
            <Layout.Header className={this.className}>
                <div className={styles.rowTitle}>
                    <div className={styles.titleBlock}>
                        {iconType !== undefined && (
                            <div className={styles.iconContainer}>
                                <Icon name={iconType} size={SIZE.EXTRA_LARGE}/>
                            </div>
                        )}
                        <div>
                            <div className={styles.groupTitle}>{groupSectionTitle}</div>
                            <div className={styles.titleContainer}>
                                <span className={styles.primaryTitle}>{sectionTitle}</span>
                                {itemTitle && (
                                    <>
                                    <span className='ud-page-header-item-title'> / </span>
                                    <span className={styles.secondaryTitle}>{itemTitle}</span>
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className={styles.buttonsContainer}>
                        {extraButtons}
                    </div>
                </div>
                {/* TODO: it is not clear how to make responsive design for a variable set of buttons */}
                {/* TODO: tab buttons are not implemented yet, as in the layout */}
                {horizontalMenu &&
                <div className={styles.horizontalContainer}>
                    {horizontalMenu}
                </div>
                }
            </Layout.Header>
        );
    }
}
