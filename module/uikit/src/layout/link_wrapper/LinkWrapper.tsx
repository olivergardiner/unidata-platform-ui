/**
 * Component for displaying links on the main page of the Data module
 *
 * @author Brauer Ilya
 * @date 2020-04-15
 */

import * as React from 'react';
import * as styles from './linkWrapper.m.scss';

interface IProps {
    text?: React.ReactNode;
    link: React.ReactNode;
}

export class LinkWrapper extends React.Component<IProps> {
    render () {
        const {text, link} = this.props;

        if (link === undefined) {
            return null;
        }

        return (
            <div className={styles.item}>
                {text !== undefined && (
                    <>
                        <span>
                            {text || ''}
                        </span>
                        &nbsp;
                        &gt;
                        &nbsp;
                    </>
                )}
                {this.props.link}
            </div>
        );
    }
}
