import * as React from 'react';

import {withInfo} from '@storybook/addon-info';
import {text, withKnobs} from '@storybook/addon-knobs';
import {storyLayout} from '../../index.stories';
import {LinkWrapper} from './LinkWrapper';

export default {
    title: 'LinkWrapper',
    decorators: [withInfo, storyLayout, withKnobs]
};


export const simpleLinkWrapper = () => {

    return (
        <LinkWrapper
            text={text('text', 'simple text')}
            link={<a href={''}>{text('link', 'link text')}</a>}
        />
    );
};
