import * as React from 'react';

import styles from './emptyText.m.css';

interface IProps {
    text: string;
}

export class EmptyText extends React.Component<IProps> {
    render () {
        return (
            <span className={styles.emptyText}>{this.props.text}</span>
        );
    }
}
