/**
 * Button with a standard confirmation window
 *
 * @author: Vladimir Stebunov
 * @date: 2020-04-13
 */

import * as React from 'react';
import {Confirm} from '../../component/modal/Confirm';
import {Button} from '../../component/button/Button';
import {Props as ButtonProps} from '../../component/button/Button.types';

interface IProps {
    confirmationMessage: string;
    header?: string;
    onConfirm: () => void;
    silent?: boolean;
}

interface IState {
    isConfirmOpen: boolean;
}

export class ButtonWithConfirmation extends React.Component<IProps & ButtonProps, IState> {

    state = {
        isConfirmOpen: false
    }

    onClick = () => {
        if (this.props.silent) {
            this.props.onConfirm();

            return;
        }

        this.setState({isConfirmOpen: true});
    }

    onClose = () => {
        this.setState({isConfirmOpen: false});
    }

    onConfirm = () => {
        this.setState({isConfirmOpen: false});
        this.props.onConfirm();
    }

    render () {

        const {confirmationMessage, header, onConfirm, ...props} = this.props;

        return (
            <React.Fragment>
                <Button
                    {...props}
                    onClick={this.onClick}
                />
                <Confirm
                    isOpen={this.state.isConfirmOpen}
                    header={header}
                    confirmationMessage={confirmationMessage}
                    onConfirm={this.onConfirm}
                    onClose={this.onClose}
                />
            </React.Fragment>
        );
    }
}
