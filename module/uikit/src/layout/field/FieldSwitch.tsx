/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {Field, IFieldProps} from './Field';
import {Switcher, ISwitcherProps} from '../..';

type IFieldSwitchProps = IFieldProps & {
    name: string;
    defaultChecked?: any;
    disabled: boolean;
    onChange: (name: string, value: any) => void;
    inputLabel?: string;
    error?: string;
}

const defaultProps = {
    disabled: false as boolean
};

export class FieldSwitch extends React.PureComponent<IFieldSwitchProps> {

    static defaultProps = defaultProps;

    inputProps = (
        {
            name,
            defaultChecked,
            disabled,
            inputLabel
        }: IFieldSwitchProps
    ) => {
        let props: ISwitcherProps = {
            name,
            size: 'small',
            checked: defaultChecked,
            onChange: this.onChange,
            disabled,
            rightText: inputLabel
        };

        return props;
    }

    onChange = (checked: boolean) => {
        this.props.onChange(this.props.name, checked);
    }

    render () {
        const {label, sublabel, error, inputLabel} = this.props;

        return (
            <Field label={label} sublabel={sublabel} error={error}>
                <Switcher {...this.inputProps(this.props)} />
            </Field>
        );
    }
}
