/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {Field, IFieldProps} from './Field';

import {Input, ITextAreaProps} from '../..';

type IFieldTextAreaProps = IFieldProps & {
    name: string;
    rows?: number;
    value?: string | number | boolean | null;
    defaultValue?: string | number;
    disabled: boolean;
    onChange: (name: string, value: string) => void;
    error?: string | null;
    autoSize?: boolean;
}

const defaultProps = {
    rows: 4 as number,
    disabled: false as boolean
};

export class FieldTextArea extends React.PureComponent<IFieldTextAreaProps> {

    static defaultProps = defaultProps;

    get inputProps () {
        let props: ITextAreaProps = {
            name: this.props.name,
            rows: this.props.rows,
            value: this.props.defaultValue,
            autoSize: this.props.autoSize,
            onChange: this.onChange,
            disabled: this.props.disabled,
            'data-qaid': this.props['data-qaid']
        };

        const inputErrorCls = 'ud-field-input_error';

        if (this.props.error) {
            props['className'] = props['className'] ? props['className'] + ' ' + inputErrorCls : inputErrorCls;
        }

        return props;
    }

    onChange = ({target}: {target: any}) => {
        this.props.onChange(this.props.name, target.value);
    }

    render () {
        const {label, sublabel, error, labelWidth, inputWidth} = this.props;

        return (
            <Field
                isMultiLine={true}
                label={label}
                sublabel={sublabel}
                error={error}
                labelWidth={labelWidth}
                inputWidth={inputWidth}
            >
                <Input.TextArea {...this.inputProps} />
            </Field>
        );
    }
}
