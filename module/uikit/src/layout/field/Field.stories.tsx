import * as React from 'react';
import {Field} from './index';

import {withInfo} from '@storybook/addon-info';
import {storyLayout} from '../../index.stories';

export default {
    title: 'Field',
    decorators: [withInfo, storyLayout]
};

export const fields = () => {
    return (
        <React.Fragment>
            label {<Field label={'Label'} sublabel={'SubLabel'}>INPUT</Field>}
            <br/>
            input {<Field.Input
                label={'Label'}
                sublabel={'SubLabelsublabelsublabelaslsfldskfksdfkdlkfnalhflahbflsadfhadlksfhbl'}
                disabled={false}
                onChange={(name: any, value: any) => {console.log(name, value);}}
            />}
            <br/>
            select {<Field.Select
                name={'select'}
                label={'SubLabelsublabelsublabe laslsfldskfksdfkdlkfnalhflahbflsadfhadlksfhbl'}
                sublabel={'SubLabelsublabelsublabelaslsfldskfksdfkdlkfnalhflahbflsadfhadlksfhbl'}
                disabled={false}
                options={[]}
                onSelect={(name: any, value: any) => {console.log(name, value);}}
            />}
            <br/>
            checkbox {<Field.Checkbox
                label={'SubLabelsublabelsublabelaslsfldskfksdfkdlkfnalhflahbflsadfhadlksfhbl'}
                sublabel={'SubLabelsublabelsublabelaslsfldskfksdfkdlkfnalhflahbflsadfhadlksfhbl'}
                disabled={false}
                onChange={(name: any, value: any) => {console.log(name, value);}}
            />}
            <br/>
            switch {<Field.Switcher
                name={'switcher'}
                label={'Label'}
                sublabel={'SubLabel'}
                disabled={false}
                onChange={(name: any, value: any) => {console.log(name, value);}}
            />}
        </React.Fragment>
    );
};
