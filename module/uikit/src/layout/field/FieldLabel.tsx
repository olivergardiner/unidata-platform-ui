import * as React from 'react';
import styles from './field.m.css';

interface IProps {
    label?: string;
    sublabel?: string;
}

export class FieldLabel extends React.PureComponent<IProps> {

    render () {
        const {label, sublabel} = this.props;

        if (!label) {
            return null;
        }

        return (
            <>
                <div className={styles.fieldLabel}>
                    {label}
                </div>
                {sublabel &&
                <div className={styles.fieldSubLabel }>{sublabel}</div>
                }
            </>
        );
    }
}
