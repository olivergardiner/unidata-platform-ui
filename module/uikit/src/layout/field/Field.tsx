/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {FieldLabel} from './FieldLabel';
import styles from './field.m.css';
import {FieldInput} from './FieldInput';
import {FieldCheckbox} from './FieldCheckbox';
import {FieldSelect} from './FieldSelect';
import {FieldTextArea} from './FieldTextArea';
import {FieldSwitch} from './FieldSwitch';
import {FieldTreeSelect} from './FieldTreeSelect';

export interface IFieldProps {
    label?: string;
    sublabel?: string;
    error?: string | null;
    labelWidth?: number;
    inputWidth?: number;
    isMultiLine?: boolean;
    // offset: number;
    // gutter: number;
    // className?: string;
    'data-qaid'?: string;
}

const defaultProps = {
    labelWidth: 40,
    inputWidth: 60
    // offset: 0,
    // gutter: 16
};

export class Field extends React.PureComponent<IFieldProps> {

    static defaultProps = defaultProps;

    static Label = FieldLabel;
    static Input = FieldInput;
    static Checkbox = FieldCheckbox;
    static Select = FieldSelect;
    static Textarea = FieldTextArea;
    static Switcher = FieldSwitch;
    static TreeSelect = FieldTreeSelect;

    render () {
        const props = this.props;

        const {
            label,
            sublabel,
            error,
            labelWidth,
            inputWidth,
            isMultiLine
            // offset,
            // gutter,
            // className
        } = props;

        const fieldStyles = [
            styles.field,
            (isMultiLine === true) ? styles.multiLine : ''
        ].join(' ');

        return (
            <div
                className={fieldStyles}
                data-qaid={props['data-qaid']}
            >
                <div style={{width: `${labelWidth}%`}} className={styles.fieldLabelContainer}>
                    <FieldLabel
                        label={label}
                        sublabel={sublabel}
                    />
                </div>
                <div style={{width: `${inputWidth}%`}} className={styles.fieldInputContainer}>
                    {props.children}

                    {error &&
                    <div className={styles.fieldError}>
                        {error}
                    </div>
                    }
                </div>
            </div>
        );
    }
}
