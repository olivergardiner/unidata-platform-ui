/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';

import {Field, IFieldProps} from './Field';
import styles from './field.m.css';
import {ICheckboxProps, Checkbox} from '../..';

type IFieldCheckboxProps = IFieldProps & {
    name?: string;
    defaultChecked?: boolean;
    disabled: boolean;
    onChange: (name: string, value: boolean) => void;
    inputLabel?: string;
    error?: string;
    items?: any[];
}

const defaultProps = {
    disabled: false as boolean
};

export class FieldCheckbox extends React.PureComponent<IFieldCheckboxProps> {

    static defaultProps = defaultProps;

    get checkboxChildren () {
        if (this.props.items) {
            return (
                <div className={styles.itemsContainer}>

                    {this.props.items.map((item) => {
                        return (
                            <div key={item.name} style={{width: `${item.inputWidth}%`}} className={styles.fieldInputContainer}>
                                <Checkbox {...this.inputProps(item)}>
                                    {item.inputLabel}
                                </Checkbox>
                            </div>
                        );
                    })}
                </div>
            );
        }

        return (
            <Checkbox {...this.inputProps(this.props)}>
                {this.props.inputLabel}
            </Checkbox>
        );
    }

    inputProps = ({
                      name,
                      defaultChecked,
                      disabled
    }: IFieldCheckboxProps) => {
        let props: ICheckboxProps = {
            name,
            checked: (defaultChecked === undefined) ? false : defaultChecked,
            onChange: this.onChange,
            disabled
        };

        return props;
    }

    onChange = ({target}: {target: any}) => {
        this.props.onChange(target.name, target.checked);
    }

    render () {
        const props = this.props;
        const {label, sublabel, error} = props;

        return (
            <Field
                label={label}
                sublabel={sublabel}
                error={error}
                data-qaid={props['data-qaid']}
            >
                {this.checkboxChildren}
            </Field>
        );
    }
}
