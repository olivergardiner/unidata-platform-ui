/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {Field, IFieldProps} from './Field';
import {ISelectProps, Select, IOption, Value as SelectValue} from '../..';

type IFieldSelectProps = IFieldProps & {
    name: string;
    isOpen?: boolean;
    options: IOption[];
    value?: SelectValue;
    disabled: boolean;
    onSelect: (name: string, value: SelectValue) => void;
    onDeselect?: (name: string, value: string) => void;
    onSearch?: (name: string, value: any) => void;
    onChange?: (name: string, value: SelectValue) => void;
    loading?: boolean;
    error?: string | null;
    showSearch?: boolean;
    dropdownRender?: any;
    placeholder?: string;
    mode?: 'default' | 'multiple' | 'tags';
    allowClear: boolean;
    overlapButton?: JSX.Element;
    'data-qaid'?: string;
    onDropdownVisibleChange?: (open: boolean) => void;
}

const defaultProps = {
    disabled: false as boolean,
    allowClear: false as boolean
};

export class FieldSelect extends React.Component<IFieldSelectProps> {

    static defaultProps = defaultProps;

    get inputProps () {
        let props: ISelectProps = {
            options: this.props.options,
            isOpen: this.props.isOpen,
            name: this.props.name,
            showSearch: this.props.showSearch,
            dropdownRender: this.props.dropdownRender,
            placeholder: this.props.placeholder,
            onSelect: this.handleOnSelect,
            onSearch: this.handleOnSearch,
            onDeselect: this.handleOnDeselect,
            onChange: this.handleOnChange,
            loading: this.props.loading,
            // style: {width: '100%'},
            allowClear: this.props.allowClear,
            // error: this.props.error,
            // className: this.props.className,
            onDropdownVisibleChange: this.props.onDropdownVisibleChange,
            disabled: this.props.disabled,
            value: this.props.value,
            mode: this.props.mode === undefined ? 'default' : this.props.mode
        };

        const inputErrorCls = 'ud-field-input_error';

        // if (this.props.error) {
        //     props['className'] = props['className'] ? props['className'] + ' ' + inputErrorCls : inputErrorCls;
        // }

        return props;
    }

    handleOnSelect = (value: SelectValue) => {
        this.props.onSelect(this.props.name, value);
    }

    handleOnSearch = (value: string | number | boolean) => {
        if (this.props.onSearch) {
            this.props.onSearch(this.props.name, value);
        }
    }

    handleOnDeselect = (value: string) => {
        if (this.props.onDeselect) {
            return this.props.onDeselect(this.props.name, value);
        }
    }

    handleOnChange = (value: SelectValue) => {
        if (this.props.onChange) {
            this.props.onChange(this.props.name, value);
        }
    }

    render () {
        const {label, sublabel, error, labelWidth, inputWidth, overlapButton} = this.props;

        return (
            <Field
                label={label}
                sublabel={sublabel}
                error={error}
                labelWidth={labelWidth}
                inputWidth={inputWidth}
                data-qaid={this.props['data-qaid']}
            >
                {overlapButton ?
                    <div>
                        <Select {...this.inputProps}/>
                        {overlapButton}
                    </div> : <Select {...this.inputProps}/>
                }
            </Field>
        );
    }
}
