/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {Field, IFieldProps} from './Field';
import styles from './field.m.css';
import {IInputProps, Input} from '../..';


type IFieldInputProps = IFieldProps & {
    name?: string;
    type: 'text' | 'number' | 'password';
    disabled: boolean;
    onChange: (name: string, value: string) => void;
    onEnter?: () => void;
    defaultValue?: string | number;
    error?: any;
    placeholder?: string;
    items?: any[];
    suffix?: any;
    // inputClassName?: string;
    // className?: string;
    // style?: string;
}

const defaultProps = {
    type: 'text' as 'text' | 'number' | 'password',
    disabled: false as boolean
};

const InputByType = (props: any) => {
    const {label, sublabel, inputWidth, labelWidth, ...inputProps} = props;

    if (props.type === 'password') {
        return (
            <Input.Password {...inputProps} />
        );
    } else {
        return (
            <Input {...inputProps} />
        );
    }
};

export class FieldInput extends React.PureComponent<IFieldInputProps> {

    static defaultProps = defaultProps;

    get inputChildren () {

        if (this.props.items) {
            return (
                <div className={styles.itemsContainer}>

                    {this.props.items.map((item) => {
                        return (
                            <div key={item.name} style={{width: `${item.inputWidth}%`}} className={styles.fieldInputContainer}>
                                <InputByType {...this.inputProps(item)} />
                            </div>
                        );
                    })}
                </div>
            );
        }

        return (
            <InputByType {...this.inputProps(this.props)} />
        );
    }

    inputProps = ({
                      name,
                      type,
                      defaultValue,
                      placeholder,
                      disabled,
                      onEnter,
                      suffix,
                      error
                  }: IFieldInputProps) => {
        let props: IInputProps = {
            name,
            type,
            placeholder,
            value: defaultValue,
            disabled,
            onChange: this.onChange,
            onKeyDown: (onEnter !== undefined) ? this.onKeyDown : undefined,
            suffix
        };

        const inputErrorCls = 'ud-field-input_error';

        if (error) {
            props['className'] = props['className'] ? props['className'] + ' ' + inputErrorCls : inputErrorCls;
        }

        return props;
    }

    onChange = ({target}: { target: any }) => {
        this.props.onChange(target.name, target.value);
    }

    onKeyDown = (e: any) => {
        if (e.key === 'Enter' && this.props.onEnter) {
            this.props.onEnter();
        }
    }

    render () {
        const {label, sublabel, error, labelWidth, inputWidth} = this.props;

        return (
            <Field
                label={label}
                sublabel={sublabel}
                error={error}
                labelWidth={labelWidth}
                inputWidth={inputWidth}
                data-qaid={this.props['data-qaid']}
            >
                {this.inputChildren}
            </Field>
        );
    }
}
