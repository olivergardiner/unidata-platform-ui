declare const styles: {
  readonly "field": string;
  readonly "multiLine": string;
  readonly "fieldLabel": string;
  readonly "fieldError": string;
  readonly "fieldLabelContainer": string;
  readonly "fieldInputContainer": string;
  readonly "fieldSubLabel": string;
  readonly "itemsContainer": string;
};
export = styles;

