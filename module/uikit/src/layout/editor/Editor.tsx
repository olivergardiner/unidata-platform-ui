/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2019-04-19
 */

import * as React from 'react';
import {PageHeader, Scrollable, Spinner} from '../../index';
import {IconName} from '@unidata/icon';

import styles from './editor.m.css';

interface IProps {
    title: string;
    groupSectionTitle: string;
    itemTitle?: string;
    iconType?: IconName;
    children?: JSX.Element | JSX.Element[];
    extraButtons?: JSX.Element;
    horizontalMenu?: JSX.Element;
    padded?: 'basic' | 'very' | 'small';
    isWaiting?: boolean; // whether to show the spinner

    // todo Brauer Ilya - you do not need to provide this option for expansion, it is not supported
    // headerCls?: string[] | string;
    // contentCls?: string;
    // contentStyle?: any;
    // style?: any;
}

const defaultProps = {
    padded: 'basic',
    isWaiting: false
};

export class Editor extends React.Component<IProps> {

    static defaultProps = defaultProps;

    render () {
        const {
            title,
            itemTitle,
            children,
            iconType,
            horizontalMenu,
            extraButtons,
            padded,
            groupSectionTitle
        } = this.props;

        const scrollableStyle = {
            height: (horizontalMenu) ? 'calc(100vh - 55px - 92px)' : 'calc(100vh - 55px - 54px)'
        };

        const editorClassName = [styles.editor];

        if (padded) {
            editorClassName.push(styles[padded]);
        }

        return (
            <Spinner isShow={this.props.isWaiting}>
                <PageHeader
                    groupSectionTitle={groupSectionTitle}
                    sectionTitle={title}
                    iconType={iconType as IconName}
                    itemTitle={itemTitle}
                    horizontalMenu={horizontalMenu}
                    extraButtons={extraButtons}
                />

                <div style={scrollableStyle}>
                    <Scrollable>
                        <div className={editorClassName.join(' ')}>
                            {children}
                        </div>
                    </Scrollable>
                </div>
            </Spinner>
        );
    }
}
