/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-06-03
 */

import * as React from 'react';
import {Spinner} from '../../index';

import styles from './pageSpinner.m.css';

interface ISpinnerProps {}

export class PageSpinner extends React.PureComponent<ISpinnerProps> {

    render () {
        return (
            <div className={styles.pageSpinner}>
                <Spinner  />
            </div>
        );
    }
}
