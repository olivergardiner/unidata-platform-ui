import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {boolean, select, text, withKnobs} from '@storybook/addon-knobs';
import {Button} from './Button';
import {alignMap, intentMap, sizeMap} from '../../utils/storybookEnums';
import {iconData} from '@unidata/icon';
import {ALIGN, INTENT, SIZE} from '../../constants';
import {storyLayout} from '../../index.stories';

export default {
    title: 'Button',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const simpleButton = () => {
    return (
        <>
            <Button
                size={select('size', sizeMap(true), SIZE.MIDDLE)}
                intent={select('intent', intentMap(true), INTENT.DEFAULT)}
                align={select('align', alignMap(true), ALIGN.CENTER)}
                isRound={boolean('isRound', false)}
                isActive={boolean('isActive', false)}
                isMinimal={boolean('isMinimal', false)}
                isGhost={boolean('isGhost', false)}
                isFilled={boolean('isFilled', false)}
                isDisabled={boolean('isDisabled', false)}
                leftIcon={text('leftIcon', '')}
                rightIcon={text('rightIcon', '')}
            >
                {text('buttonText', 'button text')}
            </Button>
        </>
    );
};

const ButtonDef = (props: any) => {
    return (
        <Button
            size={SIZE.MIDDLE}
            leftIcon={text('leftIcon', '')}
            align={ALIGN.CENTER}
            intent={props.intent}
            isMinimal={props.isMinimal}
            isActive={props.isActive}
            isRound={props.isRound}
            isFilled={props.isFilled}
            isDisabled={props.isDisabled}
            isGhost={props.isGhost}
        >
            {text('buttonText', 'button text')}
        </Button>
    );
};

export const everyButtons = () => {
    const isGhost = boolean('isGhost', false);

    return (
        <React.Fragment>
            {['', 'default', 'isActive', 'isMinimal', 'isDisabled'].map((type) => {
                return (
                    <div
                        key={type}
                        style={{display: 'flex', justifyContent: 'space-between', width: '80%', marginBottom: '10px'}}
                    >
                        <div style={{width: '80px', flexShrink: 0}}>{type}:</div>
                        {Object.values(INTENT).map((intent) => {
                            if (type === '') {
                                return (
                                    <div style={{width: '140px', flexShrink: 0, textAlign: 'left', margin: '0 10px'}}>
                                        {intent} {isGhost === true ? ' + ghost' : ''}
                                    </div>
                                );
                            }

                            return (
                                <div key={`${type}_${intent}`}
                                     style={{width: '140px', flexShrink: 0, margin: '0 10px'}}>
                                    <ButtonDef
                                        intent={intent}
                                        isMinimal={type === 'isMinimal'}
                                        isActive={type === 'isActive'}
                                        isRound={boolean('isRound', false)}
                                        isFilled={boolean('isFilled', false)}
                                        isGhost={isGhost}
                                        isDisabled={type === 'isDisabled'}
                                    />
                                </div>
                            );
                        })}
                    </div>
                );
            })}
        </React.Fragment>
    );
};

export const buttonWithIcons = () => {
    return Object.keys(iconData).map((iconKey) => {
        return (
            <>
                <Button
                    size={select('size', sizeMap(true), SIZE.MIDDLE)}
                    intent={select('intent', intentMap(true), INTENT.DEFAULT)}
                    align={select('align', alignMap(true), ALIGN.CENTER)}
                    isRound={boolean('isRound', false)}
                    isActive={boolean('isActive', false)}
                    isMinimal={boolean('isMinimal', false)}
                    isFilled={boolean('isFilled', false)}
                    isDisabled={boolean('isDisabled', false)}
                    leftIcon={iconKey}
                >
                    {text('buttonText', 'button text')}
                </Button>
                <br/>
                <br/>
            </>
        );
    });
};
