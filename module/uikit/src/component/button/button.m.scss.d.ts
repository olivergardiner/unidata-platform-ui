declare const styles: {
  readonly "button": string;
  readonly "buttonAlignLeft": string;
  readonly "buttonHasSpace": string;
  readonly "buttonAlignCenter": string;
  readonly "buttonAlignRight": string;
  readonly "buttonText": string;
  readonly "buttonAnchor": string;
  readonly "buttonSizeXs": string;
  readonly "buttonHasText": string;
  readonly "buttonHasLeftIcon": string;
  readonly "buttonHasRightIcon": string;
  readonly "buttonIsRound": string;
  readonly "buttonLeftIconContainer": string;
  readonly "buttonWaitingIconContainer": string;
  readonly "buttonSizeS": string;
  readonly "buttonRightIconContainer": string;
  readonly "buttonSizeM": string;
  readonly "buttonSizeL": string;
  readonly "buttonSizeXl": string;
  readonly "buttonIntentDefault": string;
  readonly "buttonIsDisabled": string;
  readonly "buttonIsActive": string;
  readonly "buttonIsMinimal": string;
  readonly "buttonIntentPrimary": string;
  readonly "buttonIntentInfo": string;
  readonly "buttonIntentDanger": string;
  readonly "buttonLeftIcon": string;
  readonly "iconIntentDanger": string;
  readonly "buttonRightIcon": string;
  readonly "buttonWaitingIcon": string;
  readonly "buttonIntentSecondary": string;
  readonly "buttonIsFilled": string;
  readonly "isGhost": string;
};
export = styles;

