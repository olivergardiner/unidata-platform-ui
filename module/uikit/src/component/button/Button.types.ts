import * as React from 'react';
import {ALIGN, INTENT, PLACEMENT, SIZE} from '../../constants';
import {IconName} from '@unidata/icon';

export type IntentClassNames =
    | 'buttonIntentDefault'
    | 'buttonIntentPrimary';

export type SizeClassNames = 'buttonSizeXs' | 'buttonSizeS' | 'buttonSizeM' | 'buttonSizeL' | 'buttonSizeXl';

export type AlignClassNames = 'buttonAlignLeft' | 'buttonAlignCenter' | 'buttonAlignRight';

export type Props = Omit<React.ButtonHTMLAttributes<HTMLButtonElement>, 'disabled'> & {
    intent?: INTENT; // Type
    size?: SIZE;
    align?: ALIGN;
    leftIcon?: React.ReactNode | IconName;
    rightIcon?: React.ReactNode | IconName;

    // flags
    isRound?: boolean;
    isActive?: boolean;
    isMinimal?: boolean;
    isDisabled?: boolean;
    isFilled?: boolean; // filling the entire container
    isWaiting?: boolean; // (spinner to the left of the text)
    isGhost?: boolean; // minimalistic button with the save framework

    tooltipPlacement?: PLACEMENT; // location of the tooltip
}
