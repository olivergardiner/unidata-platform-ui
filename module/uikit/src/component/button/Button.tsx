/**
 * Main button
 *
 * @author Ilya Braujer
 * @date 2019-12-26
 */
import * as React from 'react';
import {AlignClassNames, IntentClassNames, Props, SizeClassNames} from './Button.types';
import {createHtmlPropsFilter} from '../../utils/createHtmlPropsFilter';
import {getSizeThemeKey} from '../../utils/getSizeThemeKey';
import {getIntentThemeKey} from '../../utils/getIntentThemeKey';
import {getAlignThemeKey} from '../../utils/getAlignThemeKey';
import {ALIGN, INTENT, PLACEMENT, SIZE} from '../../constants';
import {joinClassNames} from '../../utils/joinClassNames';
import * as styles from './button.m.scss';
import {renderIcon} from '../../utils/renderIcon';
import {Tooltip} from '../..';

const filterProps = createHtmlPropsFilter<Props, React.ButtonHTMLAttributes<HTMLButtonElement>>([
    'leftIcon',
    'rightIcon',
    'size',
    'intent',
    'children',
    'isActive',
    'isMinimal',
    'isGhost',
    'isDisabled',
    'isFilled',
    'isWaiting',
    'align',
    'isRound',
    'tooltipPlacement',
    'title'
]);

const spanStyle = {
    display: 'inline-block',
    cursor: 'not-allowed'
};

export class Button extends React.Component<Props> {

    renderButton () {
        const props = this.props;

        const {
            size = SIZE.MIDDLE,
            intent = INTENT.DEFAULT,
            align = ALIGN.CENTER,
            isDisabled
        } = props;

        const sizeThemeKey = getSizeThemeKey<SizeClassNames>('button', size);
        const intentThemeKey = getIntentThemeKey<IntentClassNames>('button', intent);
        const alignThemeKey = getAlignThemeKey<AlignClassNames>('button', align);

        const className = joinClassNames(
            styles.button,
            styles[sizeThemeKey],
            styles[intentThemeKey],
            styles[alignThemeKey],
            [styles.buttonIsRound, Boolean(props.isRound)],
            [styles.buttonIsActive, Boolean(props.isActive)],
            [styles.buttonIsMinimal, Boolean(props.isMinimal)],
            [styles.isGhost, Boolean(props.isGhost)],
            [styles.buttonIsDisabled, Boolean(props.isDisabled || props.isWaiting)],
            [styles.buttonIsFilled, Boolean(props.isFilled)],
            [styles.buttonHasLeftIcon, Boolean(props.leftIcon)],
            [styles.buttonHasRightIcon, Boolean(props.rightIcon)],
            [styles.buttonHasText, Boolean(props.children)],
        );

        const {onClick, ...buttonHtmlProps} = filterProps(props);

        const buttonProps = {
            ...buttonHtmlProps,
            onClick: isDisabled === true ? () => null : onClick,
            className
        };

        return (
            <button {...buttonProps}>
                {Boolean(props.isWaiting) && (renderIcon(
                    'loading',
                    {
                        intent: intent,
                        size
                    },
                    {className: styles.buttonLeftIconContainer}
                ))}
                {Boolean(props.leftIcon) && !Boolean(props.isWaiting) && (renderIcon(
                    props.leftIcon,
                    {
                        intent: intent,
                        size
                    },
                    {className: styles.buttonLeftIconContainer}
                ))}
                {props.children && (<span className={styles.buttonText}>{props.children}</span>)}
                {Boolean(props.rightIcon) && (renderIcon(
                    props.rightIcon,
                    {
                        intent: intent,
                        size
                    },
                    {className: styles.buttonRightIconContainer}
                ))}
            </button>
        );
    }

    render () {
        const {title, isDisabled, tooltipPlacement = PLACEMENT.TOP} = this.props;

        return (
            <React.Fragment>
                {(title &&
                    <Tooltip
                        overlay={title}
                        placement={tooltipPlacement}
                        mouseEnterDelay={100}
                    >
                        {(!isDisabled && this.renderButton()) || <span style={spanStyle}>{this.renderButton()}</span>}
                    </Tooltip>) ||
                this.renderButton()}
            </React.Fragment>
        );
    }
}
