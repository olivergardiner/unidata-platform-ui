/**
 * Date inputs
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {DatePicker} from 'antd';
import {DatePickerProps, RangePickerProps} from 'antd/lib/date-picker';

const {RangePicker} = DatePicker;

export class DateRangeInput extends React.Component<RangePickerProps> {
    render () {

        return (
            <RangePicker {...this.props} data-qaid='dateRangeInput'/>
        );
    }
}

export class DateInput extends React.Component<DatePickerProps> {
    render () {
        return (
            <DatePicker {...this.props} data-qaid='datePickerInput' />
        );
    }
}

