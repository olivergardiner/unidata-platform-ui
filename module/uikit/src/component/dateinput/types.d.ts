import {Size} from '../../constants';
import * as moment from 'moment';
import {TimePickerProps} from 'antd/lib/time-picker';
import * as React from 'react';

interface ITimePickerLocale {
    placeholder: string;
}

interface ITimePickerProps {
    className?: string;
    size?: 'large' | 'default' | 'small';
    value?: moment.Moment;
    defaultValue?: moment.Moment | moment.Moment[];
    open?: boolean;
    format?: string;
    onChange?: (time: moment.Moment, timeString: string) => void;
    onOpenChange?: (open: boolean) => void;
    onAmPmChange?: (ampm: 'AM' | 'PM') => void;
    disabled?: boolean;
    placeholder?: string;
    prefixCls?: string;
    hideDisabledOptions?: boolean;
    disabledHours?: () => number[];
    disabledMinutes?: (selectedHour: number) => number[];
    disabledSeconds?: (selectedHour: number, selectedMinute: number) => number[];
    style?: React.CSSProperties;
    getPopupContainer?: (triggerNode: HTMLElement) => HTMLElement;
    addon?: Function;
    use12Hours?: boolean;
    focusOnOpen?: boolean;
    hourStep?: number;
    minuteStep?: number;
    secondStep?: number;
    allowEmpty?: boolean;
    allowClear?: boolean;
    inputReadOnly?: boolean;
    clearText?: string;
    defaultOpenValue?: moment.Moment;
    popupClassName?: string;
    popupStyle?: React.CSSProperties;
    suffixIcon?: React.ReactNode;
    clearIcon?: React.ReactNode;
    locale?: ITimePickerLocale;
}

declare const DatePickerModes: ['time', 'date', 'month', 'year', 'decade'];
type DatePickerMode = (typeof DatePickerModes)[number];

interface IPickerProps {
    id?: string;
    name?: string;
    prefixCls?: string;
    inputPrefixCls?: string;
    format?: string | string[];
    disabled?: boolean;
    allowClear?: boolean;
    className?: string;
    suffixIcon?: React.ReactNode;
    style?: React.CSSProperties;
    popupStyle?: React.CSSProperties;
    dropdownClassName?: string;
    locale?: any;
    size?: Size;
    getCalendarContainer?: (triggerNode: Element) => HTMLElement;
    open?: boolean;
    onOpenChange?: (status: boolean) => void;
    disabledDate?: (current: moment.Moment | undefined) => boolean;
    dateRender?: (current: moment.Moment, today: moment.Moment) => React.ReactNode;
    autoFocus?: boolean;
}

interface ISinglePickerProps {
    value?: moment.Moment;
    defaultValue?: moment.Moment;
    defaultPickerValue?: moment.Moment;
    placeholder?: string;
    renderExtraFooter?: (mode: DatePickerMode) => React.ReactNode;
    onChange?: (date: moment.Moment | null, dateString: string) => void;
}

export interface IDatePickerProps extends IPickerProps, ISinglePickerProps {
    showTime?: ITimePickerProps | boolean;
    showToday?: boolean;
    open?: boolean;
    disabledTime?: (current: moment.Moment | undefined) => {
        disabledHours?: () => number[];
        disabledMinutes?: () => number[];
        disabledSeconds?: () => number[];
    };
    onOpenChange?: (status: boolean) => void;
    onPanelChange?: (value: moment.Moment | undefined, mode: DatePickerMode) => void;
    onOk?: (selectedTime: moment.Moment) => void;
    mode?: DatePickerMode;
}

export declare type RangePickerValue =
    [moment.Moment | null, moment.Moment | null] | null | undefined

export declare type DefaultRangePickerValue =
    [moment.Moment, moment.Moment]


export declare type RangePickerPresetRange = RangePickerValue | (() => RangePickerValue);

/*
* @deprecated use RangePickerProps from antd instead
* */
export interface IRangePickerProps extends IPickerProps {
    className?: string;
    value?: RangePickerValue;
    defaultValue?: RangePickerValue;
    defaultPickerValue?: DefaultRangePickerValue;
    onChange?: (dates: RangePickerValue, dateStrings: [string, string]) => void;
    onCalendarChange?: (dates: RangePickerValue, dateStrings: [string, string]) => void;
    onOk?: (selectedTime: RangePickerPresetRange) => void;
    showTime?: TimePickerProps | boolean;
    showToday?: boolean;
    ranges?: {
        [range: string]: RangePickerPresetRange;
    };
    placeholder?: [string, string];
    mode?: string | string[];
    separator?: React.ReactNode;
    disabledTime?: (current: moment.Moment | undefined, type: string) => {
        disabledHours?: () => number[];
        disabledMinutes?: () => number[];
        disabledSeconds?: () => number[];
    };
    onPanelChange?: (value?: RangePickerValue, mode?: string | string[]) => void;
    renderExtraFooter?: () => React.ReactNode;
}
