import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {boolean, withKnobs} from '@storybook/addon-knobs';
import {DateInput, DateRangeInput} from './../../index';
import {storyLayout} from '../../index.stories';

export default {
    title: 'DateInput',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const dateInput = () => {
    return (
        <>
            <DateInput

            />
        </>
    );
};

export const dateRangeInput = () => {
    return (
        <>
            <DateRangeInput

            />
        </>
    );
};
