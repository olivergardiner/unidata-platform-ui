/**
 * Loading indicator
 *
 * @author: Denis Makarov
 * @date: 2020-08-19
 */

import * as React from 'react';
import {BarLoader} from 'react-spinners';

interface IProps {
    height?: number | string;
    width?: number | string;
}

const defaultProps =  {
    height: 'auto',
    width: 'auto'
};

export class LoadingIndicator extends React.Component<IProps> {
    static defaultProps = defaultProps;

    render () {
        return (
            <BarLoader width={this.props.width}
                       height={this.props.height}/>
        );
    }
}
