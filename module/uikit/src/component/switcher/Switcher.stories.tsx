import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {boolean, withKnobs} from '@storybook/addon-knobs';
import {Switcher} from './../../index';
import {useState} from 'react';
import {storyLayout} from '../../index.stories';

export default {
    title: 'Switcher',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const simpleSwitcher = () => {
    const [checked, setState] = useState(false);

    return (
        <>
            <Switcher
                checked={boolean('checked', checked)}
                onChange={(e) => setState(e)}
            />
        </>
    );
};
