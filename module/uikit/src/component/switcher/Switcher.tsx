import * as React from 'react';
import {Switch} from 'antd';

type SwitcherChange = (checked: boolean, event: MouseEvent) => void;

export interface ISwitcherProps {
    name?: string;
    prefixCls?: string;
    className?: string;
    size?: 'small' | 'default';
    checked?: boolean;
    defaultChecked?: boolean;
    onChange?: SwitcherChange;
    onClick?: SwitcherChange;
    checkedChildren?: React.ReactNode;
    unCheckedChildren?: React.ReactNode;
    disabled?: boolean;
    loading?: boolean;
    autoFocus?: boolean;
    style?: React.CSSProperties;
    title?: string;
    leftText?: React.ReactNode;
    rightText?: React.ReactNode;
    'data-qaid'?: string;
}

const spanStyle = {
    fontSize: '12px',
    lineHeight: '12px',
    fontFamily: 'Open Sans',
    paddingRight: '4px'
};

export class Switcher extends React.Component<ISwitcherProps> {
    render () {
        const {leftText, rightText, ...otherProps} = this.props;

        return (
            <>
                {leftText && <span style={spanStyle}>{leftText}</span>}
                <Switch {...otherProps}/>
                {rightText && <span style={spanStyle}>{rightText}</span>}
            </>
        );
    }
}
