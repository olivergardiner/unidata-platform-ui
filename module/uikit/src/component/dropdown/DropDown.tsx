/**
 * Drop-down list component
 *
 * @author Brauer Ilya
 * @date 28-02-2020
 */

import * as React from 'react';
import {PLACEMENT, Popover, TRIGGER} from '../../index';
import {Header} from './Header';
import {Item} from './Item';
import {Splitter} from './Splitter';
import {IProps as PopoverProps} from '../popover/Popover.types';

type IProps = PopoverProps & {
    isDisabled?: boolean;
    'data-qaid'?: string;
}

export class DropDown extends React.Component<IProps, any> {

    static defaultProps = {
        trigger: TRIGGER.CLICK,
        placement: PLACEMENT.BOTTOM_START,
        hasShadow: true,
        hasArrow: false,
        hasPaddings: false,
        modifiers: {
            popperOffset: {
                enabled: true,
                offset: 4
            }
        },
        mouseEnterDelay: 10,
        mouseLeaveDelay: 10
    };

    static Header = Header;
    static Item = Item;
    static Splitter = Splitter;

    render () {

        const {isDisabled, isOpen, onVisibleChange, ...otherProps} = this.props;

        return (
            <Popover
                {...otherProps}
                isOpen={isDisabled === true ? false : isOpen}
                onVisibleChange={isDisabled === true ? undefined : onVisibleChange}
            >
                <div data-qaid={this.props['data-qaid']}>
                    {this.props.children}
                </div>
            </Popover>
        );
    }
}
