/**
 * Title element in the drop down list
 *
 * @author Brauer Ilya
 * @date 2020-03-27
 */

import * as React from 'react';
import * as styles from './dropdown.m.scss';
import {joinClassNames} from '../../utils/joinClassNames';

type IProps = React.HTMLAttributes<HTMLDivElement>

export class Header extends React.Component<IProps, any> {
    render () {
        const className = joinClassNames(
            styles.ddHeader
        );

        const ItemProps = {
            className
        };

        return (
            <div {...ItemProps}>
                {this.props.children}
            </div>
        );
    }
}
