import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {select, withKnobs} from '@storybook/addon-knobs';
import {DropDown} from './DropDown';
import {placementMap} from '../../utils/storybookEnums';
import {PLACEMENT} from '../../constants';
import {storyLayout} from '../../index.stories';
import {Button} from '../../index';

export default {
    title: 'DropDown',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const simpleDropDown = () => {
    return (
        <>
            <DropDown
                target={(
                    <Button>button</Button>
                )}

                placement={select('placement', placementMap(true), PLACEMENT.BOTTOM_START)}
            >
                <>
                    <DropDown.Item>first</DropDown.Item>
                    <DropDown.Item isMinimal={true}>second</DropDown.Item>
                    <DropDown.Splitter/>
                    <DropDown.Item>last</DropDown.Item>
                </>
            </DropDown>

            <DropDown
                target={(
                    <div>button</div>
                )}

                placement={select('placement', placementMap(true), PLACEMENT.BOTTOM_START)}
            >
                <>
                    <DropDown.Item>first</DropDown.Item>
                    <DropDown.Item isMinimal={true}>second</DropDown.Item>
                    <DropDown.Splitter/>
                    <DropDown.Item>last</DropDown.Item>
                </>
            </DropDown>
        </>
    );
};


