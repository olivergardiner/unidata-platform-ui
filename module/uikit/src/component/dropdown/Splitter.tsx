/**
 * Separator in the drop-down list
 *
 * @author Brauer Ilya
 * @date 2020-03-02
 */

import * as React from 'react';
import * as styles from './dropdown.m.scss';

export class Splitter extends React.Component<any, any> {
    render () {
        return (
            <div className={styles.ddSplitter}/>
        );
    }
}
