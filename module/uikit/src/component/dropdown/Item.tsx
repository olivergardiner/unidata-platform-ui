/**
 * Drop-down list item
 *
 * @author Brauer Ilya
 * @date 2020-03-02
 */

import * as React from 'react';
import * as styles from './dropdown.m.scss';
import {renderIcon} from '../../utils/renderIcon';
import {createHtmlPropsFilter} from '../../utils/createHtmlPropsFilter';
import {joinClassNames} from '../../utils/joinClassNames';

type IProps = React.HTMLAttributes<HTMLDivElement> & {
    isMinimal?: boolean;
    isDisabled?: boolean;
    isNoHover?: boolean;
    rightIcon?: React.ReactNode;
}

const filterProps = createHtmlPropsFilter<IProps, React.HTMLAttributes<HTMLDivElement>>([
    'isDisabled',
    'isMinimal',
    'isNoHover',
    'rightIcon',
    'children'
]);

export class Item extends React.Component<IProps, any> {
    render () {
        const className = joinClassNames(
            styles.ddItem,
            [styles.ddItemMinimal, Boolean(this.props.isMinimal)],
            [styles.ddItemNoHover, Boolean(this.props.isNoHover)],
            [styles.ddItemDisabled, Boolean(this.props.isDisabled)]
        );

        const ItemProps = {
            ...filterProps(this.props),
            className
        };

        return (
            <div {...ItemProps}>
                {this.props.children}
                {this.props.rightIcon !== undefined && (renderIcon(
                    this.props.rightIcon,
                    {}, {
                        className: styles.ddItemIconContainer
                    }
                ))}
            </div>
        );
    }
}
