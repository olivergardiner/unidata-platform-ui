/**
 * Component for displaying extjs widgets
 *
 * @author Ivan Marshalkin
 * @date 2020-02-13
 */

import * as React from 'react';
import {RefObject} from 'react';

interface IProps {
    autoDestroy: boolean;
    createWidget: () => any;
    destroyWidget?: (widget: any) => any;
    async?: boolean;
    style?: React.CSSProperties;
}

interface IState {}

export class ExtWidget  extends React.Component<IProps, IState> {
    widgetRef: RefObject<HTMLDivElement> = React.createRef();
    widget: any;

    componentDidMount (): void {
        const self = this;

        self.widget = self.props.createWidget();

        this.renderWidget();

        window.Ext.on('resize', self.onResize, self);
    }

    componentWillUnmount (): void {
        const self = this;
        const props = self.props;
        const widget = self.widget;
        const destroyFn = this.props.destroyWidget;

        if (destroyFn) {
            destroyFn(widget);
        }

        if (props.autoDestroy && widget && !widget.isDestroyed &&  !widget.destroying) {
            widget.destroy();
        }

        window.Ext.un('resize', self.onResize, self);
    }

    renderWidget () {
        if (this.widget) {
            if (this.props.async) {
                setTimeout(function () {
                    this.widget.render(this.widgetRef.current);
                }.bind(this), 0);
            } else {
                this.widget.render(this.widgetRef.current);
            }
        }
    }

    onResize () {
        if (this.widget) {
            this.widget.updateLayout();
        }
    }

    getWidget () {
        return this.widget;
    }

    render () {
        return (
            <div
                style={this.props.style}
                ref={this.widgetRef}
            >
            </div>
        );
    }
}
