import * as React from 'react';

import {withInfo} from '@storybook/addon-info';
import {storyLayout} from '../../index.stories';
import {Wizard} from './Wizard';
import {Footer} from './footer/Footer';
import {IStringKeyMap} from '../../type';
import {Navigation} from './navigation/Navigation';
import {Button, Modal} from '..';
import {useState} from 'react';
import {StepSchema, WizardContextProps, WizardStep} from '../../type/WizardTypes';

export default {
    title: 'Wizard',
    decorators: [withInfo, storyLayout]
};

interface IStepComponentProps {
    title: string;
}

class StepComponent extends React.Component<Partial<WizardContextProps> & IStepComponentProps> {
    constructor (props: IStepComponentProps) {
        super(props);
    }

    get wizardProps () {
        return this.props as WizardContextProps;
    }

    render () {
        return (
            <>
                <Navigation allowNext={true}
                />
                {this.props.title}
                <Button onClick={() => this.wizardProps.moveToStep('step4')}>Add more step</Button>
                <Footer rightElement={
                    (<div>
                        <Button onClick={() => alert('extra button')}>Alert</Button>
                        <Button onClick={this.wizardProps.onSubmit.bind(this)}>Submit</Button>
                    </div>)
                }
                />
            </>
        );
    }
}

const wizardRef: React.Ref<any> = React.createRef();

const stepsSchema: IStringKeyMap<StepSchema> = {
    step1: {next: 'step2', prev: null},
    step2: {next: 'step3', prev: 'step1'},
    step3: {next: null, prev: 'step2'},
    step4: {next: null, prev: 'step3'}
};

const steps: WizardStep [] = [
    {
        name: 'step1', label: 'step 1', component: <Step/>
    },
    {
        name: 'step2', label: 'step 2', component: <Step2 key={'step2'}/>
    },
    {
        name: 'step3', label: 'step 3', component: <StepComponent title={'Step3'}/>
    },
    {
        name: 'step4', label: 'step 4', component: <div>
            <Navigation/>
            <div>Sorry, step is under construction</div>
            <Button onClick={(() => {
                wizardRef.current.moveToStep('step1');
            })}>To step 1</Button>
        </div>
    }
];


function Step () {
    const [count, setCount] = useState<number>(0);

    return (
        <>
            <Navigation allowNext={count > 0}
            />
            <button onClick={() => {
                setCount(count + 1);
            }}>
                Count {count}
            </button>
            <div>Increase counter to pass next step</div>
            <Footer allowPrev={true}
                    allowNext={count > 0}
            />
        </>
    );
}

function Step2 (props: Partial<WizardContextProps>) {
    const [modalOpen, setModalOpen] = useState<boolean>(false);

    function stepProps (): WizardContextProps {
        return props as WizardContextProps;
    }

    function beforeNextStep () {
        setModalOpen(true);
    }

    return (
        <>
            <Modal isOpen={modalOpen}>
                <div>Just press the button
                </div>
                <div>
                    <Button onClick={() => stepProps().moveNextStep()}>Continue</Button>
                </div>
            </Modal>
            <Navigation allowNext={true} beforeNextStep={beforeNextStep}
            />
            Step 2
            <Footer allowPrev={true}
                    beforeNextStep={beforeNextStep}
                    allowNext={true}
            />
        </>
    );
}

export const wizard = () => {
    return (
        <Wizard ref={wizardRef} stepsSchema={stepsSchema} style={{width: 400}} steps={steps} onSubmit={() => {
            alert('Submit Success!');
        }} firstStep={'step1'}/>
    );
};


