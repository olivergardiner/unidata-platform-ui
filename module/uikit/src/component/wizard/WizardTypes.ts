import {IStringKeyMap} from '../../type';
import {ReactElement} from 'react';

export type StepSchema = {
    next: string | null;
    prev: string | null;
}

export type WizardContextProps = {
    moveNextStep: () => void;
    movePrevStep: () => void;
    moveToStep: (step: string) => void;
    onSubmit: () => void;
    steps: WizardStep [];
    stepsSchema?: IStringKeyMap<StepSchema>;
    currentStep: string;
    getPrevStep: () => string | null;
    getNextStep: () => string | null;
}

export type WizardStep = {
    name: string;
    label: string;
    component: ReactElement;
}

