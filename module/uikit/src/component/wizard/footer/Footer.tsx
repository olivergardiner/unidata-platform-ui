/**
 * Footer for the Step component used inside Wizard
 * Represents buttons for navigating through the steps of the wizard
 * Works with Wizardcontent for easy access to the state and methods of the wizard
 * You need to connect this component inside the Step where navigation is required
 *
 * @author: Denis Makarov
 * @date: 2020-02-12
 */
import * as React from 'react';
import {ReactElement, ReactNode} from 'react';
import {WizardContext} from '../Wizard';
import {WizardContextProps} from '../../../type/WizardTypes';
import {Button} from '../../index';
import {INTENT} from '../../../constants';

import * as styles from './footer.m.scss';
import {joinClassNames} from '../../../utils/joinClassNames';
import {i18n} from '@unidata/core-app';

interface IProps {
    allowPrev?: boolean;
    allowNext?: boolean;
    allowSubmit?: boolean;
    isSubmitStep?: boolean; // custom flag for determining whether onSubmit will be called
    leftElement?: ReactNode; // custom controls that use custom handlers
    rightElement?: ReactNode;
    beforePrevStep?: () => void; // custom handler for clicking the "back" button"
    beforeNextStep?: () => void; // custom handler for clicking the "next" button"
    onSubmit?: () => void; //overrides the behavior of WizardContext.onSubmit
}

interface IDefaultProps {
    allowPrev: boolean;
    allowNext: boolean;
    allowSubmit: boolean;
}

export class Footer extends React.Component<IProps> {
    static defaultProps: IDefaultProps = {
        allowPrev: true,
        allowNext: true,
        allowSubmit: true
    };

    onNextStep (wizardProps: WizardContextProps) {
        if (!this.props.allowNext) {
            return;
        }

        if (this.props.beforeNextStep) {
            this.props.beforeNextStep();
        } else {
            wizardProps.moveNextStep();
        }
    }

    onPrevStep (wizardProps: WizardContextProps) {
        if (!this.props.allowPrev) {
            return;
        }

        if (this.props.beforePrevStep) {
            this.props.beforePrevStep();
        } else {
            wizardProps.movePrevStep();
        }
    }

    onSubmit (wizardProps: WizardContextProps) {
        if (!this.props.allowSubmit) {
            return;
        }

        wizardProps.onSubmit();
    }

    renderDefaultLeftElement (wizardProps: WizardContextProps) {
        const buttonText = (typeof this.props.leftElement === 'string') ? this.props.leftElement : i18n.t('wizard>prevStep');

        return <Button isGhost={true}
                       data-qaid={'prevbutton'}
                       isRound
                       onClick={this.onPrevStep.bind(this, wizardProps)}
                       isDisabled={!this.props.allowPrev}>
            {buttonText}
        </Button>;
    }

    renderDefaultRightElement (wizardProps: WizardContextProps, isSubmitStep: boolean) {
        let rightElementText;

        if (typeof this.props.rightElement === 'string') {
            rightElementText = this.props.rightElement;
        } else {
            rightElementText = isSubmitStep ? i18n.t('wizard>submitStep') : i18n.t('wizard>nextStep');
        }

        return (
            isSubmitStep ?
                <Button intent={INTENT.PRIMARY}
                        data-qaid={'nextbutton'}
                        isRound
                        onClick={this.onSubmit.bind(this, wizardProps)}
                        isDisabled={!this.props.allowSubmit}>
                    {rightElementText}
                </Button> :
                <Button isGhost={true}
                        data-qaid={'nextbutton'}
                        isRound
                        onClick={this.onNextStep.bind(this, wizardProps)}
                        isDisabled={!this.props.allowNext}>
                    {rightElementText}
                </Button>
        );
    }

    renderLeftElement (wizardProps: WizardContextProps) {
        const CustomLeftElement: ReactElement | null = React.isValidElement(this.props.leftElement) ? this.props.leftElement : null;
        const showLeftElement = Boolean(CustomLeftElement) || Boolean(wizardProps.getPrevStep());

        let leftElement = null;

        if (showLeftElement) {
            leftElement = CustomLeftElement ||
                this.renderDefaultLeftElement(wizardProps);
        }

        return <div className={joinClassNames(styles.footerControls, styles.leftContainer)}>
            {leftElement}
        </div>;
    }

    renderRightElement (wizardProps: WizardContextProps) {
        const CustomRightElement: ReactElement | null = React.isValidElement(this.props.rightElement) ? this.props.rightElement : null;
        const nextStep = wizardProps.getNextStep();

        let isSubmitStep: boolean = false;
        let showRightElement: boolean = false;

        isSubmitStep = this.props.isSubmitStep === undefined ? Boolean(!nextStep) : this.props.isSubmitStep;

        if (isSubmitStep) {
            showRightElement = true;
        } else {
            showRightElement = Boolean(CustomRightElement) || Boolean(nextStep);
        }

        let rightElement = null;

        if (showRightElement) {
            rightElement = CustomRightElement ||
                this.renderDefaultRightElement(wizardProps, isSubmitStep);
        }

        return <div className={joinClassNames(styles.footerControls, styles.rightContainer)}>
            {rightElement}
        </div>;
    }

    render () {
        return (
            <WizardContext.Consumer>
                {(wizardProps: WizardContextProps) => {
                    return (
                        <div className={styles.footer}>
                            {this.renderLeftElement(wizardProps)}
                            {this.renderRightElement(wizardProps)}
                        </div>
                    );
                }}
            </WizardContext.Consumer>
        );
    }

}
