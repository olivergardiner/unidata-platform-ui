import {SIZE, TRIGGER} from '../../constants';
import {IconName, IconIntent} from '@unidata/icon';

export type Props = React.SVGAttributes<SVGElement> & {
    name: IconName;
    size?: SIZE;
    intent?: IconIntent;
    title?: React.ReactNode;
    titleTrigger?: TRIGGER.HOVER_TARGET_ONLY | TRIGGER.HOVER;
}
