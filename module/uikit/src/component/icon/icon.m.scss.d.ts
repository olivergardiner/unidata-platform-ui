declare const styles: {
  readonly "icon": string;
  readonly "iconSizeXs": string;
  readonly "iconSizeS": string;
  readonly "iconSizeM": string;
  readonly "iconSizeL": string;
  readonly "iconSizeXl": string;
  readonly "iconIntentDefault": string;
  readonly "iconIntentPrimary": string;
  readonly "iconIntentInfo": string;
  readonly "iconIntentSuccess": string;
  readonly "iconIntentWarning": string;
  readonly "iconIntentDanger": string;
  readonly "iconIntentGhost": string;
  readonly "iconIntentSecondary": string;
};
export = styles;

