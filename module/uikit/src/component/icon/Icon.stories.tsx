import * as React from 'react';
import {Icon} from './Icon';
import {select, withKnobs} from '@storybook/addon-knobs';
import {withInfo} from '@storybook/addon-info';
import {iconColorMap, sizeMap} from '../../utils/storybookEnums';
import {iconData, IconName, IconIntent} from '@unidata/icon';
import {SIZE} from '../../constants';

export default {
    title: 'Icon',
    decorators: [withInfo, withKnobs]
};

export const simpleIcon = () => {
    return (
        <div style={{width: '100%', display: 'flex', flexWrap: 'wrap'}}>
            {Object.entries(iconData).map(([name]) => {
                return (
                    <div key={name} style={{margin: '10px', border: '1px solid green', padding: '10px'}}>
                        <Icon
                            name={name as IconName}
                            size={select('size', sizeMap(true), SIZE.MIDDLE)}
                            intent={select('intent', iconColorMap(true), IconIntent.DEFAULT)}
                        />
                        <br/>
                        {name}
                    </div>
                );
            })}
        </div>
    );
};
