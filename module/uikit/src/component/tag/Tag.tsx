/**
 * Tag component
 *
 * @author Ivan Marshalkin
 * @date 2020-05-28
 */

import * as React from 'react';
import {Tag as AntdTag} from 'antd';
import {ReactNode} from 'react';

interface IProps {
    children?: ReactNode;
    color: string;
}

export class Tag extends React.Component<IProps, any> {
    render () {
        const props = this.props;

        return (
            <AntdTag color={props.color}>
                {props.children}
            </AntdTag>
        );
    }
}
