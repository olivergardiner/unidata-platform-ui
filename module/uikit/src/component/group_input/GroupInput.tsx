/**
 * Component for displaying a group of inputs (input, select, date Input, number Input) in a string.
 * Changes display styles by removing rounding between elements
 *
 * @author Brauer Ilya
 *
 * @date 2020-05-06
 */

import * as React from 'react';

import * as styles from './groupInput.m.scss';

export class GroupInput extends React.Component<any, any> {

    render () {
        return (
            <div className={styles.groupContainer}>
                {this.props.children}
            </div>
        );
    }
}
