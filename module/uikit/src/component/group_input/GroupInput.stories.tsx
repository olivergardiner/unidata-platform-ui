import * as React from 'react';
import {GroupInput} from './GroupInput';
import {DateInput, Icon, Input, Select} from './../../index';

import {withInfo} from '@storybook/addon-info';
import {storyLayout} from '../../index.stories';
import {useState} from 'react';

export default {
    title: 'GroupInput',
    decorators: [withInfo, storyLayout]
};

export const simpleGroup = () => {

    const [value, setValue] = useState('title2');

    return (
        <GroupInput>
            <Select
                value={value}
                onChange={(value) => setValue(value as string)}
                options={[
                    {title: 'title1', value: 'value1'},
                    {title: 'title2', value: 'value2'},
                    {title: 'title3', value: 'value3'},
                    {title: 'title4', value: 'value4'}
                ]}
            />
            <Input title={'hello'} value={'Hello Button'}/>
            <Select
                value={value}
                onChange={(value) => setValue(value as string)}
                options={[
                    {title: 'title1', value: 'value1'},
                    {title: 'title2', value: 'value2'},
                    {title: 'title3', value: 'value3'},
                    {title: 'title4', value: 'value4'}
                ]}
            />
            <Input title={'hello'} value={'Hello Button'} suffix={(<Icon name={'plus'}/>)}/>

        </GroupInput>
    );
};
