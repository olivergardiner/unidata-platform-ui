import * as React from 'react';
import {IStringKeyMap} from '../../type';

type NodeKey = string;

export interface ITreeNode {
    key: NodeKey; // member key
    expanded: boolean; // the flag is folded/deployed
    parent?: ITreeNode; // the parent node
    children: ITreeNode [] | null; // descendants
    row: {
        [key: string]: any;
    }; // data to display
    path?: string; // path to the element todo Brauer Ilya as keys array from root
    type?: string; // todo Brauer Ilya what it is?
    icon?: string;
    cls?: string;
    root?: boolean; // is the node the parent node
    // path?: string;
    iconCls?: string;
    disabled?: boolean; // flag for displaying the node as " disabled"
    selected?: boolean; // flag for displaying the node as " selected"

    //todo Brauer Ilya: for what?
    readOnly?: boolean;
}


export interface ITreeColumn {
    name: string;
    displayName: string;
    cls?: string;
    width?: number | string;
    renderer?: (node: ITreeNode) => React.ReactNode;
}

export interface IFlatListItem<T> {
    nodeId: string;
    name: string;
    parentId: string;
    model: T;
}
