/**
 * empty comment line Viktor Magarlamov
 *
 * @author: Viktor Magarlamov
 * @date: 2019-06-20
 */

import * as React from 'react';
import {ITreeColumn, ITreeNode} from './TreeType';

interface IProps {
    item: ITreeNode;
    expanded?: boolean;
    level: number;
    onExpandClick?: (e: any) => void;
    category?: string;
    columns: ITreeColumn [];
    readOnly?: boolean;
    isLast: boolean;
    cls?: string;
    selected?: boolean;
    disabled?: boolean;
    onNodeSelect: any;
    onBeforeNodeSelect?: (node: ITreeNode) => boolean;
    searchValue?: string;
    isScrollToMe?: boolean;
}

export class Row extends React.Component<IProps> {

    ref: React.RefObject<HTMLDivElement> = React.createRef();

    get expandSymbol () {
        const {expanded, item} = this.props;

        let content;

        if (item.children) {
            content = expanded ? 'open' : 'close';
        }

        return content;
    }

    onRowClick = (e: React.SyntheticEvent<HTMLDivElement>) => {
        e.stopPropagation();

        if (!this.props.disabled) {
            if (this.props.onBeforeNodeSelect && !this.props.onBeforeNodeSelect(this.props.item)) {
                return;
            }

            this.props.onNodeSelect(this.props.item);
        }
    };

    buildCells = (node: ITreeNode) => {
        let me = this,
            cells,
            clsName;

        cells = this.props.columns.map((column: ITreeColumn, index) => {
            let value,
                style;

            if (column.renderer) {
                node.selected = me.props.selected;
                node.disabled = me.props.disabled;
                value = column.renderer(node);
            } else if (node.row) {
                value = node.row[column.name];
            }

            // for filters
            // if (this.props.searchValue && value instanceof String) {
            //
            // }

            style = {
                width: column.width || ''
            };

            clsName = index === 0 ? 'ud-table-tree_first-column' : 'ud-table-tree_column';

            return <div onClick={this.onRowClick} className={`${clsName} ${me.props.cls || ''}`} style={style} key={column.name}><span>{value}</span></div>;
        });

        return cells;
    };

    shouldComponentUpdate (nextProps: Readonly<IProps>, nextState: Readonly<{}>, nextContext: any): boolean {
        if (nextProps.item !== this.props.item) {
            return true;
        }

        if (nextProps.selected !== this.props.selected) {
            return true;
        }

        if (nextProps.disabled !== this.props.disabled) {
            return true;
        }

        return nextProps.expanded !== this.props.expanded;
    }

    buildRowClassName (): string {
        const {selected, disabled, item} = this.props;

        let selectable: boolean = Boolean(item.path);
        let className = 'ud-table-tree_row ';

        if (selectable && !disabled) {
            className += 'ud-table-tree_row__selectable ';
        }

        if (selected) {
            className += 'ud-table-tree_row__active ';
        }

        if (disabled) {
            className += 'ud-table-tree_row__disabled ';
        }

        return className;

    }

    componentDidMount (): void {
        if (this.props.item.row.match && this.props.item.row.isScrollToMe && this.ref.current) {
            this.ref.current.scrollIntoView();
        }
    }

    render () {
        const {onExpandClick, level, item, isLast, expanded, selected, disabled} = this.props;
        const cells = this.buildCells(item);

        const firstColumn = {
            fontWeight: onExpandClick ? 'bold' : 'normal'
        } as {[key: string]: string};

        const levels = Array.from(new Array(level).keys());

        let className = this.buildRowClassName();

        return (
            <div className={className} ref={this.ref}>
                <div className='ud-table-tree_expand-column' style={firstColumn} onClick={onExpandClick}>
                    {levels.map((i, index) =>
                        <div className={`v-dot-line ${(isLast && index === levels.length - 1 && !expanded) ? 'v-dot-line-last' : ''}`} key={i} />
                    )}

                    <div className='h-dot-line' />

                    {this.expandSymbol && <div className={this.expandSymbol} /> }
                </div>
                {cells}
            </div>
        );
    }
}
