/**
 * empty comment line Ivan Marshalkin
 *
 * TODO: navigation using the keyboard
 *
 * @author: Ivan Marshalkin
 * @date: 2019-05-27
 */

import * as React from 'react';
import keyBy from 'lodash/keyBy';
import {Node} from './Node';
import {ITreeColumn, ITreeNode, IFlatListItem} from './TreeType';
import {Skeleton} from 'antd';
import {IStringKeyMap} from '../../type';

export interface ITreeTableProps {
    nodes: ITreeNode[];
    isLoading?: boolean;
    readOnly?: boolean;
    columns: ITreeColumn[];
    onNodeSelect: (node: ITreeNode) => void;
    onBeforeNodeSelect?: (node: ITreeNode) => boolean;
    selectedNodes: string[];
    disabledNodes?: string[];
    colorScheme?: ColorScheme;
    searchText?: string;
    style?: React.CSSProperties;
    getRowCls?: (item: ITreeNode) => string;
}

interface IState {
    selectedItems: IStringKeyMap;
    disabledItems: IStringKeyMap;
}

export enum ColorScheme {
    light = 'theme-light',
    dark = 'theme-dark'
}

const defaultProps = {
    colorScheme: ColorScheme.light,
    disabled: []
};

export {
    ITreeNode,
    ITreeColumn,
    IFlatListItem
};

export {TreeUtil} from './TreeUtil';

export class Tree extends React.Component<ITreeTableProps, IState> {
    static defaultProps = defaultProps;

    constructor (props: ITreeTableProps) {
        super(props);

        this.state = {
            selectedItems: keyBy(this.props.selectedNodes),
            disabledItems: keyBy(this.props.disabledNodes)
        };
    }

    static getDerivedStateFromProps (props: ITreeTableProps) {
        return {
            selectedItems: keyBy(props.selectedNodes),
            disabledItems: keyBy(props.disabledNodes)
        };
    }

    render () {
        const {nodes, colorScheme} = this.props;

        return (
            <Skeleton active loading={this.props.isLoading}>
                <div style={this.props.style} className={`ud-table-tree-wrapper ${colorScheme}`}>
                    <div className={`ud-table-tree`}>
                        <div className='ud-table-tree_body'>
                            {nodes && nodes.map((node) =>
                                <Node
                                    key={node.key}
                                    readOnly={node.readOnly}
                                    expanded={node.expanded}
                                    columns={this.props.columns}
                                    searchText={this.props.searchText}
                                    onNodeSelect={this.props.onNodeSelect}
                                    onBeforeNodeSelect={this.props.onBeforeNodeSelect}
                                    selectedItems={this.state.selectedItems}
                                    disabledItems={this.state.disabledItems}
                                    level={0}
                                    item={node}
                                    isLast={false}
                                    getRowCls={this.props.getRowCls}
                                />
                            )}
                        </div>
                    </div>
                </div>
            </Skeleton>
        );
    }
}
