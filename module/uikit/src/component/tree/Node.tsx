import * as React from 'react';

import {Row} from './Row';
import {ITreeColumn, ITreeNode} from './TreeType';
import {IStringKeyMap} from '../../type';

interface IProps {
    level: number;
    item: ITreeNode;
    readOnly?: boolean;
    isLast: boolean;
    expanded: boolean;
    onNodeSelect: any;
    onBeforeNodeSelect?: (node: ITreeNode) => boolean;
    columns: ITreeColumn [];
    selectedItems: IStringKeyMap;
    disabledItems: IStringKeyMap;
    searchText?: string;
    getRowCls?: (item: ITreeNode) => string;
}

interface IState {
    expanded: boolean;
}

export class Node extends React.Component<IProps, IState> {
    constructor (props: IProps) {
        super(props);

        this.state = {
            expanded: this.props.expanded
        };

        this.getRowCls = this.getRowCls.bind(this);
    }

    getRowCls (item: ITreeNode, level: number): string {
        let rowCls = '';

        if (this.props.getRowCls) {
            rowCls = this.props.getRowCls(item);
        }

        if (level === 0 && item.children) {
            rowCls = rowCls + ' ' + 'ud-table-tree_root-node';
        }

        return rowCls;
    }

    onExpandClick = (e: Event) => {
        e.stopPropagation();

        this.setState({expanded: !this.state.expanded});
    };

    render () {
        const {item, level, readOnly, isLast} = this.props;
        const nextLevel = level + 1;
        const me = this;

        return (
            <React.Fragment>
                <div className='ud-table-tree_container'>
                    <Row
                        level={level}
                        item={item}
                        readOnly={readOnly}
                        searchValue={this.props.searchText}
                        onNodeSelect={me.props.onNodeSelect}
                        onBeforeNodeSelect={me.props.onBeforeNodeSelect}
                        onExpandClick={me.onExpandClick}
                        expanded={me.state.expanded}
                        selected={Boolean(me.props.selectedItems[item.key])}
                        disabled={Boolean(me.props.disabledItems[item.key])}
                        cls={this.getRowCls(item, level)}
                        columns={me.props.columns}
                        isLast={isLast}
                    />

                </div>
                {me.state.expanded && item.children && item.children.map(
                    function (childItem: ITreeNode, index: number) {
                        let isLast;

                        if (!item.children) {
                            isLast = false;
                        } else {
                            isLast = (index === item.children.length - 1);
                        }

                        return (
                            <React.Fragment key={childItem.key}>
                                {childItem.children === null ?
                                    <div className='ud-table-tree_container'>
                                        <Row
                                            item={childItem}
                                            level={nextLevel}
                                            onNodeSelect={me.props.onNodeSelect}
                                            onBeforeNodeSelect={me.props.onBeforeNodeSelect}
                                            selected={Boolean(me.props.selectedItems[childItem.key])}
                                            disabled={Boolean(me.props.disabledItems[childItem.key])}
                                            readOnly={readOnly}
                                            cls={me.getRowCls(childItem, nextLevel)}
                                            columns={me.props.columns}
                                            isLast={isLast}
                                        />
                                    </div> :
                                    <Node
                                        key={childItem.key}
                                        item={childItem}
                                        level={nextLevel}
                                        readOnly={readOnly}
                                        onNodeSelect={me.props.onNodeSelect}
                                        onBeforeNodeSelect={me.props.onBeforeNodeSelect}
                                        columns={me.props.columns}
                                        selectedItems={me.props.selectedItems}
                                        disabledItems={me.props.disabledItems}
                                        expanded={childItem.expanded}
                                        isLast={isLast}
                                    />
                                }
                            </React.Fragment>
                        );
                    })
                }
            </React.Fragment>
        );
    }
}
