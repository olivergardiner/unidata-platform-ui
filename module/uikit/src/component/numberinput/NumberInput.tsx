import * as React from 'react';

import {InputNumber} from 'antd';
import * as styles from './../input/input.m.css';
import {Size} from '../../constants/constants';

interface IProps extends Omit<React.InputHTMLAttributes<HTMLInputElement>, 'defaultValue' | 'onChange' | 'size'> {
    isInteger?: boolean;
    isOnlyPositive?: boolean;
    isOnlyNegative?: boolean;

    forwardRef?: React.RefObject<any>;

    min?: number;
    max?: number;
    value?: number;
    step?: number | string;
    defaultValue?: number;
    tabIndex?: number;
    onChange?: (value: number | undefined) => void;
    disabled?: boolean;
    size?: Size;
    formatter?: (value: number | string | undefined) => string;
    parser?: (displayValue: string | undefined) => number | string;
    decimalSeparator?: string;
    placeholder?: string;
    style?: React.CSSProperties;
    precision?: number;
    onPressEnter?: React.KeyboardEventHandler<HTMLInputElement>;
    'data-qaid'?: string;
    hasError?: boolean;
    errorMessage?: string;
}

export class NumberInput extends React.Component<IProps> {
    parser = (displayValue: string | undefined) => {
        if (this.props.parser) {
            return this.props.parser(displayValue);
        }

        if (displayValue === undefined) {
            return '';
        }

        if (this.props.isInteger) {
            if (/^-?\d*$/.test(displayValue)) {
                return displayValue;
            }
        } else {
            if (/^-?\d*\.?\d*(?:e-?\d*)?$/.test(displayValue)) {
                return displayValue;
            }
        }

        return displayValue.slice(0, -1);
    };

    render () {
        const {forwardRef, hasError, errorMessage, ...props} = this.props;

        return (
            <div className={styles.container}>
                <InputNumber
                    {...props}
                    ref={forwardRef}
                    parser={this.parser}
                    data-qaid={props['data-qaid'] ? props['data-qaid'] : 'inputNumber'}
                    className={hasError ? styles.error : ''}
                />
                {hasError === true && (
                    <div className={styles.errContainer}>
                        {errorMessage}
                    </div>
                )}
            </div>
        );
    }
}
