import * as React from 'react';
import {NumberInput} from './../../index';

import {withInfo} from '@storybook/addon-info';
import {storyLayout} from '../../index.stories';

export default {
    title: 'NumberInput',
    decorators: [withInfo, storyLayout]
};

export const simpleNumberInput = () => <NumberInput title={'hello'} value={12}/>;
