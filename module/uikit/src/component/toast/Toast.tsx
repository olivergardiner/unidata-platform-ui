/**
 * The notification POPs up at the bottom and has three types of design
 *
 * @author Stebunov Vladimir
 * @date 2020-03-19
 */

import * as React from 'react';
import {ReactElement} from 'react';
import * as styles from './toast.m.scss';
import {INTENT, SIZE} from '../../constants';
import {Button} from '../button/Button';
import {Icon} from '../icon/Icon';
import {IconName} from '@unidata/icon';
import {i18n, ServerErrorSeverity, ToastMessage, ToastTypeEnum} from '@unidata/core-app';
import {joinClassNames} from '../../utils/joinClassNames';

interface IProps {
    isOpen: boolean;
    onClose: (e: any) => void;
    showModal?: (e: any) => void;
    message: ToastMessage;
    isDissapeared: boolean;
}

export class Toast extends React.Component<IProps> {

    blockStyle () {
        const {type, severity} = this.props.message;

        return joinClassNames(
            styles.block,
            [styles.infoBlock, type === ToastTypeEnum.Info],
            [styles.warningBlock, type === ToastTypeEnum.Warning],
            [styles.lowSeverityBlock, severity === ServerErrorSeverity.LOW],
            [styles.disappear, this.props.isDissapeared]
        );
    }

    descriptionStyle () {
        return joinClassNames(
            styles.description
        );
    }

    renderIcon () {
        const IconAssoc = new Map<ToastTypeEnum, IconName>();

        IconAssoc.set(ToastTypeEnum.Error, 'notification-circle');
        IconAssoc.set(ToastTypeEnum.Warning, 'question-circle');
        IconAssoc.set(ToastTypeEnum.Info, 'checkmark-circle');

        return (<Icon
            name={IconAssoc.get(this.props.message.type) || 'checkmark-circle'}
        />);
    }

    render () {
        const {message, onClose, showModal} = this.props,
            blockStyle = this.blockStyle(),
            descriptionStyle = this.descriptionStyle();

        let description: string | ReactElement[];

        if (Array.isArray(message.description)) {
            description = message.description.map((line, index) => (
                <div key={index}>{line}</div>
            ));
        } else {
            description = message.description;
        }

        return (
            <div className={blockStyle} data-qaid='toast'>
                <div className={styles.icon}>
                    {this.renderIcon()}
                </div>
                {message.title ? <div className={styles.title}>{message.title}</div> : null}
                <div className={styles.closeButton}>
                    <Icon name='close' onClick={onClose} />
                </div>
                <div className={descriptionStyle}>{description}</div>
                {message.detail ?
                    <div className={styles.buttonContainer}>
                        <Button
                            data-qaid={'errorDetailButton'}
                            size={SIZE.MIDDLE}
                            isMinimal={true}
                            isFilled={false}
                            intent={INTENT.SECONDARY}
                            onClick={showModal}
                        >{i18n.t('util>errorDetails')}</Button>
                    </div> : null
                }
            </div>
        );
    }
}
