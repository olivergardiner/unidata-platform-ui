/**
 * Story for output of the notifier
 *
 * @author: Stebunov Vladimir
 * @date: 2020-03-19
 */

import * as React from 'react';

import {Toast} from './Toast';
import {ServerErrorSeverity, ToastTypeEnum} from '@unidata/core-app';
import {storyLayout} from '../../index.stories';
import {boolean, select, text, withKnobs} from '@storybook/addon-knobs';
import {ToastTypeMap, SeverityTypeMap} from '../../utils/storybookEnums';

interface IState {
    isOpen: boolean;
}

export default {
    title: 'Toast',
    decorators: [storyLayout, withKnobs]
};

class SimpleToast extends React.Component<any, IState> {

    render () {
        return (
            <Toast
                isOpen={true}
                onClose={() => {}}
                showModal={() => {}}
                isDissapeared={false}
                message={{
                    id: 'test_id',
                    type: select('type', ToastTypeMap(true), ToastTypeEnum.Info),
                    title: text('title', 'Title'),
                    description: text('description', 'description '.repeat(50)),
                    detail: {userMessage: 'userMessage'},
                    severity: select('severity', SeverityTypeMap(true), ServerErrorSeverity.NORMAL)
                }}
            />
        );
    }
}

export const simpleToast = () => {
    return (
        <SimpleToast/>
    );
};
