/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-06-03
 */

import * as React from 'react';
import {Spin} from 'antd';
import {Size, SpinSize} from '../../constants';

import styles from './spinner.m.scss';

interface ISpinnerProps {
    tip?: string; // text after spinner
    size?: SpinSize;
    isShow?: boolean;
}

export class Spinner extends React.Component<ISpinnerProps> {
    static defaultProps = {
        isShow: true
    };

    renderSpinnerWrapper () {
        const {isShow, ...spinnerProps} = this.props;

        if (isShow) {
            return (
                <Spin className={styles.spinnerWrapper} {...spinnerProps}>
                    {this.props.children}
                </Spin>
            );
        }

        return this.props.children;
    }

    renderSpinner () {
        const {isShow, ...spinnerProps} = this.props;

        if (!isShow) {
            return null;
        }

        return (
            <div className={styles.spinner}>
                <Spin {...spinnerProps}/>
            </div>
        );
    }

    render () {
        if (this.props.children) {
            return this.renderSpinnerWrapper();
        } else {
            return this.renderSpinner();
        }
    }
}
