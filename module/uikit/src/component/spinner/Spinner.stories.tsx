import * as React from 'react';
import {Spinner} from './../../index';

import {withInfo} from '@storybook/addon-info';
import {withKnobs} from '@storybook/addon-knobs';
import {storyLayout} from '../../index.stories';

export default {
    title: 'Spinner',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const simpleSpinner = () => {
    return (
        <Spinner/>
    );
};
