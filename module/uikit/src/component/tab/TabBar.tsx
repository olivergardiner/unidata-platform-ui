/**
 *  WTF class ... or interface... or type ...
 *
 * @author Ivan Marshalkin
 * @date 2019-05-30
 */

import * as React from 'react';
import {ReactNode} from 'react';
import {Menu} from 'antd';

import styles from './tabBar.m.css';

interface IProps {
    children?: ReactNode;
    // intent?: INTENT; // UI type of Tambov. Currently only default is supported
    noDefaultUi?: boolean;                    // whether or not the default ui is applied to the component
    onClick?: (param: any) => void;
    defaultSelectedKeys?: string[];      // the active tab by default
}

interface IState {
}

export class TabBar extends React.Component<IProps, IState> {
    render () {
        return (
            <Menu
                mode='horizontal'
                onClick={this.props.onClick}
                className={styles.tabBar}
                selectedKeys={this.props.defaultSelectedKeys}
            >
                {this.props.children}
            </Menu>
        );
    }
}
