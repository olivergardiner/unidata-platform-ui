/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-07-22
 */

import * as React from 'react';
import {CheckboxChangeEvent} from 'antd/es/checkbox';
import {Checkbox as CheckboxAntd} from 'antd';

export interface ICheckboxProps {
    name?: string;
    disabled?: boolean;
    checked: boolean;
    indeterminate?: boolean; // indicator when not everything is selected
    className?: string; // @deprecated
    onChange: (e: CheckboxChangeEvent) => void;
    onClick?: React.MouseEventHandler<HTMLElement>;
    style?: React.CSSProperties;
}

export {
    CheckboxChangeEvent
};

export class Checkbox extends React.PureComponent<ICheckboxProps> {

    render () {
        const {name, disabled, indeterminate, checked, onChange, onClick, className, style, children} = this.props;

        return (
            <CheckboxAntd
                name={name}
                disabled={disabled}
                className={className}
                checked={checked}
                indeterminate={indeterminate}
                style={style}
                onClick={onClick}
                onChange={onChange}>
                {children}
            </CheckboxAntd>
        );
    }
}
