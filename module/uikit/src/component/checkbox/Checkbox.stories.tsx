import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {boolean, withKnobs} from '@storybook/addon-knobs';
import {Checkbox} from './../../index';
import {useState} from 'react';
import {storyLayout} from '../../index.stories';

export default {
    title: 'Checkbox',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const simpleCheckbox = () => {
    const [checked, setState] = useState(false);

    return (
        <>
            <Checkbox
                checked={boolean('checked', checked)}
                onChange={(e) => setState(e.target.checked)}
            />
        </>
    );
};
