import * as React from 'react';

import debounce from 'lodash/debounce';
import cloneDeep from 'lodash/cloneDeep';
import {Tree, ITreeColumn, ITreeNode} from '../../index';
import {Select, IOption, Value} from './Select';
import * as $ from 'jquery';

interface IProps {
    searchFields: string [];
    mode?: 'default' | 'multiple' | 'tags';
    onSelectNode: (node: ITreeNode) => void;
    onChange?: (value: Value) => void;
    value?: Value;
    options: IOption[];
    nodes: ITreeNode[];
    columns: ITreeColumn[];
    selectedNodes?: string[];
    disabledNodes?: string[];
    allowClear?: boolean;
    showSearch?: boolean;
    getRowCls?: (item: ITreeNode) => string;
    placeholder?: string;
}

interface IState {
    searchText: string;
    isOpen: boolean;
}

const defaultProps = {
    searchFields: ['name', 'displayName'],
    mode: 'default',
    allowClear: true
};

// ToDo fix search
export class SelectTree extends React.Component<IProps, IState> {
    static defaultProps = defaultProps;
    readonly MIN_SEARCH_STRING_LENGTH = 3;

    constructor (props: IProps) {
        super(props);

        this.select = this.select.bind(this);
        this.resetSearch = this.resetSearch.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.renderTable = this.renderTable.bind(this);
        this.updateSearchText = debounce(this.updateSearchText, 300);

        this.state = {
            searchText: '',
            isOpen: false
        };
    }

    componentDidMount (): void {
        document.body.addEventListener('click', this.handleClickForCloseDropDown);
    }
    componentWillUnmount (): void {
        document.body.removeEventListener('click', this.handleClickForCloseDropDown);
    }

    // todo Ilya Brauer: Hack for forcibly hiding a select dropout when clicking outside of it
    handleClickForCloseDropDown = (e: any) => {
        // the Html element also has closest, but IE11 does not support It
        const element = $(e.target).closest('.ant-select-dropdown')[0];

        if (!element || !element.contains(e.target)) {
            this.setState({isOpen: false});
        }
    };

    onDropdownVisibleChange = (open: boolean) => {
        if (open === true) {
            this.setState({isOpen: true});
        }
    };

    select (item: ITreeNode) {
        this.props.onSelectNode(item);

        if (item.path) {
            this.setState({isOpen: false});
        }
    }

    resetSearch () {
        this.setState({searchText: ''});
    }

    onSearch (text: string) {
        if (text.length >= this.MIN_SEARCH_STRING_LENGTH) {
            this.updateSearchText(text);
        } else {
            if (text.length === 0) {
                this.resetSearch();
            }
        }
    }

    searchFieldsContainValue (node: ITreeNode, value: string): boolean {
        let found = false;

        for (let searchField of this.props.searchFields) {
            let field: string = (node.row as any)[searchField];

            if (field && field.indexOf(value) > -1) {
                found = true;
            }
        }

        return found;
    }

    filterRecursive (data: ITreeNode [], text: string): ITreeNode [] {
        let me = this;
        let childrenContainValue: boolean = false;

        if (text.trim() === '') {
            return data;
        }

        let filteredNodes = data.filter(function (node) {
            if (node.children) {

                node.children = me.filterRecursive(node.children, text);
            }

            childrenContainValue = Boolean(node.children && node.children.length > 0);

            return childrenContainValue || me.searchFieldsContainValue(node, text);
        });

        return filteredNodes;
    }

    updateSearchText (text: string) {
        if (text !== this.state.searchText) {
            this.setState({searchText: text});
        }
    }

    renderTable () {
        let nodes = this.filterRecursive(cloneDeep(this.props.nodes), this.state.searchText);

        return <div style={{maxHeight: 400}}>
            <Tree selectedNodes={this.props.selectedNodes || []}
                       disabledNodes={this.props.disabledNodes}
                       columns={this.props.columns}
                       style={{height: 400}}
                       onNodeSelect={this.select}
                       nodes={nodes}
                       searchText={this.state.searchText}
                       getRowCls={this.props.getRowCls}
            />
        </div>;
    }

    mockOnChange = () => {
        return;
    };

    render () {
        return (
            <Select
                onChange={this.props.onChange || this.mockOnChange}
                options={this.props.options}

                isOpen={this.state.isOpen}
                placeholder={this.props.placeholder}
                mode={this.props.mode}
                value={this.props.value}
                showSearch={this.props.showSearch}
                allowClear={this.props.allowClear}
                onSearch={this.onSearch}

                dropdownRender={this.renderTable}
                onDropdownVisibleChange={this.onDropdownVisibleChange}
            />
        );
    }
}
