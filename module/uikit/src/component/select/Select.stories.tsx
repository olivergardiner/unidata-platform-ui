import * as React from 'react';
import {Select} from './../../index';

import {withInfo} from '@storybook/addon-info';
import {withKnobs} from '@storybook/addon-knobs';
import {useState} from 'react';
import {storyLayout} from '../../index.stories';

export default {
    title: 'Select',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const simpleSelect = () => {

    const [value, setValue] = useState('title2');

    return (
        <Select
            value={value}
            onChange={(value) => setValue(value as string)}
            options={[
                {title: 'title1', value: 'value1'},
                {title: 'title2', value: 'value2'},
                {title: 'title3', value: 'value3'},
                {title: 'title4', value: 'value4'}
            ]}
        />
    );
};
