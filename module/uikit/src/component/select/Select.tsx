import * as React from 'react';
import {Select as SelectAntd} from 'antd';

import styles from './select.m.css';

export type Value = string | string[] | number | number[];

export interface IOption {
    disabled?: boolean;
    title: string;
    value: string | number;
    className?: string;
}

export interface IGroupOption {
    title: string;
    value: string | number; // groups key
    options: IOption[]; // options for group
}

export interface ISelectProps {
    value?: Value;
    options: IOption[];

    onChange?: (value: Value) => void;

    name?: string;

    size?: 'default' | 'large' | 'small';
    showSearch?: boolean;
    showArrow?: boolean;
    placeholder?: string | React.ReactNode;
    mode?: 'default' | 'multiple' | 'tags';
    disabled?: boolean;
    defaultActiveFirstOption?: boolean;
    onDropdownVisibleChange?: (open: boolean) => void;
    isOpen?: boolean;
    style?: React.CSSProperties;
    notFoundContent?: React.ReactNode;
    dropdownRender?: () => React.ReactNode;
    allowClear?: boolean;
    onSearch?: (searchText: string) => void;
    onSelect?: (value: Value) => void;
    onDeselect?: (value: Value) => void;
    loading?: boolean;
    dropdownMatchSelectWidth?: boolean;
    containerRef?: React.RefObject<HTMLElement>; // todo Brauer Ilya: temporary param, remove it when the Select will be in our popover
}

export class Select extends React.Component<ISelectProps> {
    static defaultProps = {
        size: 'default',
        showSearch: false,
        showArrow: true,
        defaultActiveFirstOption: false
    };

    selectElement: SelectAntd | null = null;

    get selectProps () {
        const props: any = {
            loading: this.props.loading,
            showSearch: this.props.showSearch,
            showArrow: this.props.showArrow,
            style: this.props.style,
            className: styles.select,
            size: this.props.size,
            placeholder: this.props.placeholder,
            value: this.props.value,
            onChange: this.props.onChange,
            onDropdownVisibleChange: this.props.onDropdownVisibleChange,
            mode: this.props.mode,
            notFoundContent: this.props.notFoundContent,
            dropdownRender: this.props.dropdownRender,
            allowClear: this.props.allowClear,
            onSearch: this.props.onSearch,
            onDeselect: this.props.onDeselect,
            onSelect: this.props.onSelect,
            dropdownMatchSelectWidth: this.props.dropdownMatchSelectWidth,
            getPopupContainer: this.getContainer
        };

        if (this.props.isOpen !== undefined) {
            props.open = this.props.isOpen;
        }

        return props;
    }

    getContainer = () => {
        if (!this.props.containerRef) {
            return document.body;
        }

        return this.props.containerRef.current || document.body;
    };

    setRef = (el: SelectAntd) => {
        this.selectElement = el;
    };

    focus () {
        if (this.selectElement !== null) {
            this.selectElement.focus();
        }
    }

    render () {
        return (
            <SelectAntd {...this.selectProps} data-qaid={'select'} ref={this.setRef}>
                {this.props.options.map((option) => {
                    return (
                        <SelectAntd.Option
                            key={`${this.props.name}_${option.value.toString()}`}
                            value={option.value}
                            disabled={option.disabled}
                            data-qaid={`${this.props.name}_item`}
                        >
                            {option.title}
                        </SelectAntd.Option>
                    );
                })}
            </SelectAntd>
        );
    }
}
