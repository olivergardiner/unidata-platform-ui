import PopperJs, {ModifierFn} from 'popper.js';

/**
 * Add an offset for the popup window from the arrow side
 */
export const popperOffset: ModifierFn = (data: any, {offset = 0}: any) => {
    const popper = data.offsets.popper;
    const basePlacement = data.placement.split('-')[0];

    if (basePlacement === 'left') {
        popper.left -= offset;
    } else if (basePlacement === 'right') {
        popper.left += offset;
    } else if (basePlacement === 'top') {
        popper.top -= offset;
    } else if (basePlacement === 'bottom') {
        popper.top += offset;
    }

    data.popper = popper;

    return data;
};

/**
 * Hide a pop-up window when going out of bounds
 */
export const hideOutOfBoundaries: ModifierFn = (data) => {
    const popperInstance = data.instance as any;

    const preventOverflowModifier = popperInstance.modifiers.find(
        ({name}: {name: string}) => name === 'preventOverflow',
    );

    if (preventOverflowModifier && preventOverflowModifier.enabled) {
        const {boundaries} = preventOverflowModifier;
        const {popper} = data.offsets;

        const isOutOfBoundaries =
            boundaries.left > popper.left ||
            boundaries.top > popper.top ||
            boundaries.right < popper.left + popper.width ||
            boundaries.bottom < popper.top + popper.height;

        if (data.hide || isOutOfBoundaries) {
            data.styles.visibility = 'hidden';
            data.styles.position = 'fixed';
        }

        return data;
    }

    return data;
};

/**
 * Factory of a modifier that forces all PopperJS instances to be updated, the target object of Which is
 * current popper (or its child elements)
 *
 * Temporary fix for a pop-up window display error with the target object
 * there is another pop-up window when scrolling (see http://jira.taskdata.com/browse/RR-5213).
 *
 * TODO: Think of a better solution to the Popover-dependent scroll problem (see http://jira.taskdata.com/browse/RR-5213).
 *
 * @param poppers registry Of all active popperjs instances
 * @param paper Key Unique identifier for the Popover instance (and the Paper JS created by It)
 */
type CreateUpdateChildPoppersModifier = (poppers: {[poperKey: string]: PopperJs}, popperKey: string) => ModifierFn;

export const createUpdateChildPoppersModifier: CreateUpdateChildPoppersModifier = (poppers, popperKey) => {
    return (data) => {
        poppers[popperKey] = data.instance;
        Object.values(poppers)
            .filter((popper) => {
                return (
                    data.instance.popper.contains(popper.reference as Node) || data.instance.popper === popper.reference
                );
            })
            .forEach((popper) => {
                popper.scheduleUpdate();
            });

        return data;
    };
};

/**
 * Hide if visibility: hidden is set for the target object
 *
 * Temporary fix for a pop-up window display error with the target object
 * there is another pop-up window when scrolling.
 */
export const hideWhenReferenceHidden: ModifierFn = (data) => {
    const referenceVisibility = getComputedStyle(data.instance.reference as Element).visibility;

    if (referenceVisibility === 'hidden') {
        data.styles.visibility = 'hidden';
    }

    return data;
};
