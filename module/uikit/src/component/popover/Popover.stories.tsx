import * as React from 'react';
import {PLACEMENT, Popover, TRIGGER} from './../../index';

import {withInfo} from '@storybook/addon-info';
import {select, withKnobs, boolean, text} from '@storybook/addon-knobs';
import {placementMap, triggerMap} from '../../utils/storybookEnums';
import {storyLayout} from '../../index.stories';
import {Button} from './../../index';

export default {
    title: 'Popover',
    decorators: [withInfo, withKnobs, storyLayout]
};

import * as styles from './popover.stories.m.scss';

export const simplePopover = () => {
    const containerRef = React.useRef<HTMLDivElement>(null);
    const elementRef = React.useRef<HTMLDivElement>(null);

    const scrollToCenter = React.useCallback(() => {
        if (containerRef.current && elementRef.current) {
            containerRef.current.scrollTo(
                (containerRef.current.scrollWidth - 400) / 2,
                (containerRef.current.scrollHeight - 400) / 2,
            );
        }
    }, [containerRef, elementRef]);

    React.useEffect(() => {
        scrollToCenter();
    }, []);

    return (
        <div className={styles.scrollWrapper}>
            <div className={styles.scrollContainer} ref={containerRef}>
                <div className={styles.scrollElement} ref={elementRef}>
                    <Popover
                        trigger={select('trigger', triggerMap(false), TRIGGER.HOVER)}
                        placement={select('placement', placementMap(false), PLACEMENT.BOTTOM_END)}
                        boundaries={undefined}
                        hasBackdrop={boolean('hasBackdrop', false)}
                        hasArrow={boolean('hasArrow', false)}
                        hasPaddings={boolean('hasPaddings', false)}
                        canFlip={boolean('canFlip', false)}
                        shouldHideOutOfBoundaries={boolean('shouldHideOutOfBoundaries', false)}
                        shouldKeepInBoundaries={boolean('shouldKeepInBoundaries', false)}
                        target={<div className={styles.target} />}
                        mouseLeaveDelay={parseInt(text('mouseLeaveDelay', '100'), 2)}
                        mouseEnterDelay={parseInt(text('mouseEnterDelay', '100'), 2)}
                    >
                        <div className={styles.content} />
                    </Popover>
                </div>
            </div>
            <Button leftIcon="filterCenterFocus" onClick={scrollToCenter}>В центр</Button>
        </div>
    );
};
