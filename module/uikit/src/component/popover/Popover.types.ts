import * as React from 'react';
import {Modifiers} from 'popper.js';
import {POPOVER_BOUNDARIES, TRIGGER, PLACEMENT} from '../../constants/constants';

export type PalcementThemeKeys =
    | 'popoverPlacementTop'
    | 'popoverPlacementTopEnd'
    | 'popoverPlacementTopStart'
    | 'popoverPlacementLeft'
    | 'popoverPlacementLeftEnd'
    | 'popoverPlacementLeftStart'
    | 'popoverPlacementRight'
    | 'popoverPlacementRightEnd'
    | 'popoverPlacementRightStart'
    | 'popoverPlacementBottom'
    | 'popoverPlacementBottomEnd'
    | 'popoverPlacementBottomStart';

/**
 * Properties required for the goal element
 */
export type ITargetProps = {
    ref?: React.Ref<any>;
    onClick?: React.MouseEventHandler<any>;
    onMouseEnter?: React.MouseEventHandler<any>;
    onMouseLeave?: React.MouseEventHandler<any>;
} | {
    forwardedRef?: React.Ref<any>;
    onClick?: React.MouseEventHandler<any>;
    onMouseEnter?: React.MouseEventHandler<any>;
    onMouseLeave?: React.MouseEventHandler<any>;
}

export interface IProps extends React.HTMLAttributes<HTMLDivElement> {
    /**
     * Is the pop-up window open?
     *
     * If the Boolean type, the component goes into managed mode and opens/closes
     * only when this property is changed from the outside. If undefined, the component is in an unmanaged state
     * mode, and opens/closes according to the internal state.
     */
    isOpen?: boolean;
    /**
     * The window is open by default?
     *
     * Specifies whether the window should have been opened when creating it. This property is only relevant in unmanaged mode.
     */
    isDefaultOpen?: boolean;
    /**
     * ID for overlay
     *
     * use only in tests
     */
    overlayId?: string;
    /**
     * Type of interaction with the goal.
     *
     * Defines how and when events will open/close the pop-up window.
     */
    trigger?: TRIGGER;
    /**
     * The goal that the popup window will be attached to.
     *
     * Can be an element or can be set via the Render Props pattern.
     */
    target: ((renderProps: Required<ITargetProps>) => React.ReactNode) | React.ReactElement<ITargetProps>;
    /**
     *  The delay after which the popup window is hidden.
     */
    mouseLeaveDelay?: number;
    /**
     *  The delay after which the pop-up window is displayed.
     */
    mouseEnterDelay?: number;
    /**
     * Modifiers
     *
     * See the documentation for popper.js: https://popper.js.org/popper-documentation.html
     * Modifiers should be redefined with extreme care, before learning how they are configured by default
     */
    modifiers?: Modifiers;
    /**
     * Location of the popup window relative to the target.
     */
    placement?: PLACEMENT;
    /**
     * The boundaries of which will be a pop-up window
     *
     * Can be a DOM element.
     *
     */
    boundaries?: POPOVER_BOUNDARIES | Element;
    /**
     * Is there a background?
     */
    hasBackdrop?: boolean;
    /**
     * Are there internal margins in the popup window?
     */
    hasPaddings?: boolean;
    /**
     * Whether to display the arrow
     */
    hasArrow?: boolean;
    /**
     * Whether to display the shadow
     */
    hasShadow?: boolean;
    /**
     * Flag for displaying dark Popover
     */
    isDark?: boolean;
    /**
     * Can the pop-up window to change position when approaching the borders?
     */
    canFlip?: boolean;
    /**
     * Should I hide the pop-up window when going out of bounds?
     *
     * This property only works when the shouldKeepInBoundaries property is disabled
     */
    shouldHideOutOfBoundaries?: boolean;
    /**
     * Should I open a pop-up window from the goal and leave it within the border?
     *
     * This property only works when the hasArrow property is disabled
     */
    shouldKeepInBoundaries?: boolean;
    /**
     * Handler for the popup window opening event
     */
    onOpen?: (e: React.SyntheticEvent<Element>) => void;
    /**
     * Handler for the popup window closing event
     */
    onClose?: (e: React.SyntheticEvent<Element> | Event) => void;
    /**
     * Handler for changing the visibility of a window
     */
    onVisibleChange?: (visible: boolean) => void;
}

type DefaultPropsKeys =
    | 'boundaries'
    | 'placement'
    | 'trigger'
    | 'modifiers'
    | 'mouseLeaveDelay'
    | 'mouseEnterDelay'
    | 'canFlip'
    | 'isDark'
    | 'hasBackdrop'
    | 'hasPaddings'
    | 'hasArrow'
    | 'hasShadow'
    | 'shouldKeepInBoundaries'
    | 'shouldHideOutOfBoundaries';

export type DefaultProps = Required<Pick<IProps, DefaultPropsKeys>>;

export type PropsWithDefault = IProps & DefaultProps;

export interface IState {
    /**
     * Unique key for managing mounting/unmounting of the Popper component
     *
     * The key is updated every time properties that change modifiers are updated.
     * This remounts the Popper component, which does not update modifiers when updating properties. And what
     * to update them, you must forcibly remount the component. This was done in order to make it possible
     * change modifiers in Basic Popover via properties without dismantling It.
     */
    popperKey: string;

    /**
     * Flag for opening the popup window.
     *
     * Used for unmanaged pop-up window opening/closing mode.
     */
    isOpen: boolean;
    /**
     * Is the component in managed mode?
     *
     * Managed mode - opening/closing is determined from outside the component
     */
    isControlledMode: boolean;
}

export type GetPlacementThemeKey = (placement: PLACEMENT) => PalcementThemeKeys;

export type GetDerivedStateFromProps = (nextProps: IProps, prevState: Pick<IState, 'popperKey' | 'isOpen'>) => IState;
