export const scrollContainer: string;
export const scrollWrapper: string;
export const scrollElement: string;
export const target: string;
export const content: string;
