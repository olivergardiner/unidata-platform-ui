/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2019-05-08
 */

import * as React from 'react';
import {ReactTableDefaults} from 'react-table';
import {Scrollable} from '../../index';

interface IProps {
    autoHeight: boolean;
}

interface IState {
}

export class ScrollableTableBody extends React.Component<IProps, IState> {

    render () {
        return (
            <Scrollable autoHeight={this.props.autoHeight}>
                <ReactTableDefaults.TbodyComponent>
                    {this.props.children}
                </ReactTableDefaults.TbodyComponent>
            </Scrollable>
        );
    }
}
