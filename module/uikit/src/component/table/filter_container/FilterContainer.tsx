import * as React from 'react';

import {i18n} from '@unidata/core-app';

import {Button, INTENT, PLACEMENT, Popover, POPOVER_BOUNDARIES, SIZE, TRIGGER} from '../../../index';

import * as styles from './filterContainer.m.scss';

export interface IFilterData {
    component: React.ComponentType<any>;
    type: 'Input' | 'DatePicker' | 'Select' | 'NumberInput';
}

type IProps = IFilterData & {
    isChecked?: boolean;
    onSearch: () => void;
    onClear: () => void;
}

interface IState {
    isOpen: boolean;
}

export class FilterContainer extends React.PureComponent<IProps, IState> {

    containerElement = React.createRef<HTMLDivElement>();

    state: IState = {
        isOpen: false
    };

    getContainer = () => {
        return this.containerElement.current || document.body;
    };

    get Component () {
        const FilterComponent = this.props.component;

        if (this.props.type === 'DatePicker') {
            return <FilterComponent getCalendarContainer={this.getContainer}/>;
        }

        if (this.props.type === 'Select') {
            return <FilterComponent containerRef={this.containerElement}/>;
        }

        return <FilterComponent/>;
    }

    onVisibleChange = (isVisible: boolean) => {
        this.setState({isOpen: isVisible});
    };

    closePopover = () => {
        this.onVisibleChange(false);
        this.props.onClear();
    };

    onSearch = () => {
        this.onVisibleChange(false);
        this.props.onSearch();
    };

    render () {
        return (
            <Popover
                target={(
                    <div
                        onClick={(e) => {
                            e.stopPropagation();
                        }}
                    >
                        <Button
                            isMinimal={true}
                            size={SIZE.SMALL}
                            leftIcon={this.props.isChecked ? 'filledFilter' : 'filter'}
                            intent={this.props.isChecked ? INTENT.PRIMARY : INTENT.DEFAULT}
                        />
                    </div>
                )}
                placement={PLACEMENT.BOTTOM}
                trigger={TRIGGER.CLICK}
                boundaries={POPOVER_BOUNDARIES.VIEWPORT}
                isOpen={this.state.isOpen}
                onVisibleChange={this.onVisibleChange}
                hasShadow={true}
            >
                <div ref={this.containerElement} className={styles.container}>
                    {this.Component}

                    <div className={styles.buttonsContainer}>
                        <Button size={SIZE.SMALL} isFilled={true} onClick={this.closePopover} isMinimal={true}>
                            {i18n.t('common:cancel')}
                        </Button>
                        <Button intent={INTENT.PRIMARY} size={SIZE.SMALL} isFilled={true} onClick={this.onSearch}>
                            {i18n.t('common:apply')}
                        </Button>
                    </div>
                </div>
            </Popover>
        );
    }
}
