/**
 * The "stub" component. Currently used for displaying in empty tables, it contains a call to start searching.
 *
 * @author Brauer Ilya
 *
 * @date 2020-05-06
 */

import * as React from 'react';
import * as styles from './initTable.m.scss';

interface IProps {
    mainText?: React.ReactNode;
    mainTextBlue?: React.ReactNode;
    subText?: React.ReactNode;
    svg?: React.ReactNode;
    onClick?: () => void;
}

export class InitTable extends React.PureComponent<IProps, {}> {
    renderIcon () {
        const svg = this.props.svg;

        if (svg !== undefined) {
            return svg;
        }

        return (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.7 34.96">
                <rect className="cls-1" x="11" y="13" width="9" height="1"/>
                <rect className="cls-1" x="5" y="16" width="15" height="1"/>
                <rect className="cls-1" x="5" y="19" width="15" height="1"/>
                <rect className="cls-1" x="5" y="13" width="3" height="1"/>
                <path className="cls-1" d="M16.45,22H5v1H15.54A8.64,8.64,0,0,1,16.45,22Z"/>
                {/* eslint-disable-next-line max-len */}
                <path d="M28.46,33.66h0l-2.61-2.61.28-.42a5.49,5.49,0,0,0,.93-3.07,5.56,5.56,0,1,0-2.72,4.78l.41-.25,2.64,2.64h0a.77.77,0,0,0,.55.23.75.75,0,0,0,.76-.75A.74.74,0,0,0,28.46,33.66Zm-7-2a4,4,0,1,1,4.05-4.05A4.06,4.06,0,0,1,21.5,31.61Z"/>
                {/* eslint-disable-next-line max-len */}
                <path d="M14.79,31H3.21A2.21,2.21,0,0,1,1,28.79V3.21A2.21,2.21,0,0,1,3.21,1H17V6.5A2.5,2.5,0,0,0,19.5,9H25V20.89a7.06,7.06,0,0,1,1,.64v-13h0a.45.45,0,0,0-.13-.38l-8-8A.49.49,0,0,0,17.49,0V0H3.21A3.21,3.21,0,0,0,0,3.21V28.79A3.21,3.21,0,0,0,3.21,32h12.2A7.93,7.93,0,0,1,14.79,31ZM18,1.71,24.29,8H19.5A1.5,1.5,0,0,1,18,6.5Z"/>
            </svg>
        );
    }

    render () {
        const {mainText = '', mainTextBlue = '', subText = '', onClick} = this.props;

        return (
            <div className={styles.init}>
                {this.renderIcon()}
                <div className={styles.mainText}>
                    {mainText}
                    &nbsp;
                    <span className={styles.blue} onClick={onClick}>{mainTextBlue}</span>
                </div>
                <div className={styles.subText}>{subText}</div>
            </div>
        );
    }
}
