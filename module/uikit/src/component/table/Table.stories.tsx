import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {boolean, withKnobs} from '@storybook/addon-knobs';
import {
    CardPanel,
    DateRangeInput,
    Input,
    LinkWrapper,
    Modal,
    Select,
    Table,
    Value
} from './../../index';

import {storyLayout} from '../../index.stories';

const columns = [
    {
        Header: 'Last name',
        accessor: 'lastName'
    },
    {
        Header: 'First name',
        accessor: 'firstName',
        // eslint-disable-next-line react/display-name
        Cell: (props: any) => {
            return (<div style={{color: 'red'}}>{props.value}</div>);
        }
    },
    {
        Header: 'Birthday',
        accessor: 'birthday'
    },
    {
        Header: 'Documents',
        accessor: 'documents',
        columns: [
            {
                Header: 'Document type',
                accessor: 'docType'
            },
            {
                Header: 'Number',
                accessor: 'docNumber'
            }
        ]
    },
    {
        Header: 'Contacts',
        accessor: 'contacts',
        columns: [
            {
                Header: 'Value',
                accessor: 'contactValue'
            }
        ]
    }
];

const simpleColumns = [
    {
        Header: 'Last name',
        accessor: 'lastName'
    },
    {
        Header: 'First name',
        accessor: 'firstName',
        // eslint-disable-next-line react/display-name
        Cell: (props: any) => {
            return (<div style={{color: 'red'}}>{props.value}</div>);
        }
    },
    {
        Header: 'Birthday',
        accessor: 'birthday'
    },
    {
        Header: 'Document type',
        accessor: 'docType'
    },
    {
        Header: 'Number',
        accessor: 'docNumber'
    }
];

const data = [
    {
        lastName: 'Косицин',
        firstName: 'Арс',
        birthday: '01.08.1986',
        docType: 'Паспорт гражданина РФ',
        docNumber: '123123',
        contactValue: '968 4562845'
    },
    {
        lastName: 'Косицин',
        firstName: 'Арсений',
        birthday: '01.08.1986',
        docType: 'Паспорт гражданина РФ',
        docNumber: '123123',
        contactValue: '968 4562845'
    },
    {
        lastName: 'Косицин',
        firstName: 'Арсений',
        birthday: '01.08.1986',
        docType: 'Паспорт гражданина РФ',
        docNumber: '123123',
        contactValue: '968 4562845'
    },
    {
        lastName: 'Косицин',
        firstName: 'Арсений',
        birthday: '01.08.1986',
        docType: 'Паспорт гражданина РФ',
        docNumber: '123123',
        contactValue: '968 4562845'
    }
];

const expandedData = [
    {
        key1: 'value1',
        key2: 'value1',
        key3: 'value1'
    },
    {
        key1: 'value2',
        key2: 'value2',
        key3: 'value2'
    }
];

const subRows = [
    (<div key={'value1'}>
        <LinkWrapper link={'link1'} text={'some text1'}/>
        <LinkWrapper link={'link2'} text={'some text2'}/>
        <LinkWrapper link={'link3'} text={'some text3'}/>
    </div>)
];

export default {
    title: 'Table',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const simpleTable = () => {

    const [selectFilter, setSelectFilter] = React.useState<Value>('');
    const [sort, setSort] = React.useState<undefined | 'ASC' | 'DESC'>(undefined);
    const sortData: any = {};

    if (sort !== undefined) {
        sortData.lastName = sort;
    }

    return (
        <Table
            columns={columns}
            data={data}

            sort={sortData}

            onSort={(key: string) => {
                if (key === 'lastName') {
                    switch (sortData.lastName) {
                        case undefined: {
                            setSort('ASC');
                            break;
                        }
                        case 'ASC': {
                            setSort('DESC');
                            break;
                        }
                        case 'DESC': {
                            setSort(undefined);
                            break;
                        }
                    }
                }
            }}

            hasHeaderBg={boolean('hasHeaderBg', false)}
            isStriped={boolean('isStriped', false)}
            isRowsCheckable={boolean('isRowsCheckable', false)}
            isRowsDeletable={boolean('isRowsDeletable', false)}
            isEditable={boolean('isEditable', false)}

            editComponents={{
                // eslint-disable-next-line react/display-name
                lastName: (props: any) => {
                    return (
                        <Input
                            defaultValue={props.value}
                            autoFocus={true}
                            onBlur={(e) => {
                                const value = e.target.value;

                                props.onEdit(value);
                            }}
                        />
                    );
                }
            }}

            filters={{
                lastName: {
                    // eslint-disable-next-line react/display-name
                    component: () => <Input/>,
                    type: 'Input'
                },
                firstName: {
                    // eslint-disable-next-line react/display-name
                    component: () => <Input/>,
                    type: 'Input'
                },
                birthday: {
                    // eslint-disable-next-line react/display-name
                    component: (props: any) => <DateRangeInput {...props}/>,
                    type: 'DatePicker'
                },
                docType: {
                    // eslint-disable-next-line react/display-name
                    component: (props: any) => {
                        return (
                            <Select
                                {...props}
                                options={[
                                    {title: 'Паспорт 1', value: 'passport1'},
                                    {title: 'Паспорт 2', value: 'passport2'},
                                    {title: 'Паспорт 3', value: 'passport3'},
                                    {title: 'Паспорт 4', value: 'passport4'},
                                    {title: 'Паспорт 5', value: 'passport5'}
                                ]}
                                value={selectFilter}
                                onChange={(value) => setSelectFilter(value)}
                            />
                        );
                    },
                    type: 'Select'
                }
            }}
        />
    );
};

export const fullScreenTable = () => {
    return (
        <Table
            columns={columns}
            data={data}

            hasHeaderBg={true}
            isRowsCheckable={true}
        />
    );
};

export const tableInPanel = () => {
    return (
        <div style={{width: '80%', height: '500px', margin: '20px'}}>
            <CardPanel title={'Таблица настроек'}>
                <Table
                    columns={simpleColumns}
                    data={data}

                    isRowsDeletable={true}
                    isStriped={true}
                />
            </CardPanel>
        </div>
    );
};

export const tableInModal = () => {
    return (
        <Modal
            isOpen={true}
            noBodyPadding={true}
            header={'Таблица в модельном окне'}
        >
            <div style={{height: '300px'}}>
                <Table
                    columns={simpleColumns}
                    data={data}

                    hasHeaderBg={true}
                    isRowsCheckable={true}
                    isStriped={true}
                    isEditable={true}
                />
            </div>
        </Modal>
    );
};

export const tableWithExpand = () => {
    return (
        <Table
            viewRowsCount={0}
            columns={columns}
            data={expandedData}
            subRows={subRows}
        />
    );
};
