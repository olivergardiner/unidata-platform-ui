/**
 * Component for displaying pagination rows for tables
 *
 * @author: Denis Makarov
 * @date: 2019-05-08
 */

import * as React from 'react';

import styles from './pagination.m.scss';

export class PaginationContainer extends React.Component {
    render () {
        return (
            <div
                data-qaid={'paginationContainer'}
                className={styles.paginationContainer}
            >
                {this.props.children}
            </div>
        );
    }
}
