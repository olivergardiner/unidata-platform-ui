/**
 * Component for displaying pagination rows for tables
 *
 * @author: Denis Makarov
 * @date: 2019-05-08
 */

import * as React from 'react';

import styles from './pagination.m.scss';
import {Button, INTENT} from '../../../index';

export interface IPaginationProps {
    page: number;
    pages: number;
    onPageChange: (page: number) => void;
    ofText: string;
    showPageJump: boolean;
    canPrevious: boolean;
    canNext: boolean;
    hidePages?: boolean;
    isDisabled?: boolean;
}

export class Pagination extends React.Component<IPaginationProps, any> {
    static defaultProps = {
        ofText: 'from',
        showPageJump: true,
        canPrevious: true,
        canNext: true
    };

    inputEl = React.createRef<HTMLInputElement>();

    componentDidMount (): void {
        if (this.inputEl.current) {
            this.inputEl.current.value = this.props.page.toString();
        }
    }

    componentDidUpdate () {
        if (this.inputEl.current) {
            this.inputEl.current.value = this.props.page.toString();
        }
    }

    getSafePage = (page?: number) => {
        if (!page || Number.isNaN(page)) {
            page = this.props.page;
        }

        return Math.min(Math.max(page, 1), this.props.pages);
    };

    setPage = (page: number) => {
        page = this.getSafePage(page);

        if (this.props.page !== page) {
            this.props.onPageChange(page);
        } else {
            this.forceUpdate();
        }
    };

    changePage = (page: number) => {
        return (e: React.SyntheticEvent<HTMLButtonElement>) => {
            e.stopPropagation();

            this.setPage(page);
        };
    };

    onBlur = () => {
        if (this.inputEl.current) {
            this.setPage(Number(this.inputEl.current.value));
        }
    };

    onKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            this.onBlur();
        }
    };

    render () {
        const {
            pages,
            page,
            showPageJump,
            canPrevious,
            canNext,
            ofText,
            isDisabled
        } = this.props;

        return (
            <div className={styles.buttonsContainer}>
                <Button
                    onClick={this.changePage(1)}
                    isDisabled={!canPrevious || isDisabled || page === 1}
                    leftIcon={'double-left'}
                    isGhost={true}
                    isMinimal={true}
                    data-qaid='firstPage'
                />
                <Button
                    onClick={this.changePage(page - 1)}
                    isDisabled={!canPrevious || isDisabled || page === 1}
                    leftIcon={'left'}
                    isGhost={true}
                    isMinimal={true}
                    data-qaid='previousPage'
                />
                <span className={styles.pagesContainer}>
                    {showPageJump ? (
                        <input
                            type={'number'}
                            ref={this.inputEl}
                            onBlur={this.onBlur}
                            onKeyPress={this.onKeyPress}
                            data-qaid='pageNumber'
                            disabled={isDisabled || pages === 1}
                            min={1}
                            max={pages}
                        />
                    ) : (
                        <span>{page}</span>
                    )}{' '}
                    {!this.props.hidePages && <React.Fragment>
                        {ofText} <span>{pages || 1}</span>
                    </React.Fragment>}
                </span>

                <Button
                    onClick={this.changePage(page + 1)}
                    isDisabled={!canNext || isDisabled || page === pages}
                    leftIcon={'right'}
                    isGhost={true}
                    isMinimal={true}
                    data-qaid='nextPage'
                />
                <Button
                    onClick={this.changePage(pages + 1)}
                    isDisabled={!canNext || isDisabled || page === pages}
                    leftIcon={'double-right'}
                    isGhost={true}
                    isMinimal={true}
                    data-qaid='lastPage'
                />
            </div>
        );
    }
}
