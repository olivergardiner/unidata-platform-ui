export const paginationContainer: string;

export const pageSizeContainer: string;

export const buttonsContainer: string;

export const updateContainer: string;

export const pagesContainer: string;

export const tileFooterContainer: string;
