declare const styles: {
  readonly "paginationContainer": string;
  readonly "buttonsContainer": string;
  readonly "pagesContainer": string;
  readonly "tileFooterContainer": string;
  readonly "ud-button": string;
  readonly "ud-button-noborder": string;
  readonly "ant-btn-ghost": string;
};
export = styles;

