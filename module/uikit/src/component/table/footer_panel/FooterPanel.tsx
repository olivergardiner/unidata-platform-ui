import * as React from 'react';

import {i18n} from '@unidata/core-app';
import {Button, INTENT} from '../../../index';
import {Pagination} from '../pagination/Pagination';
import {PageSize} from '../page_size/PageSize';
import {PaginationContainer} from '../pagination/PaginationContainer';

interface IProps {
    currentPage: number;
    pageSize: number;
    totalPages: number;
    totalRecords: number;
    reload: () => void;
    setPage: (page: number) => void;
    setPageSize: (pageSize: number) => void;
    isDisabled?: boolean;
}

export class FooterPanel extends React.Component<IProps> {
    render () {
        const {
            currentPage,
            pageSize,
            totalPages,
            totalRecords,
            reload,
            setPage,
            setPageSize,
            isDisabled
        } = this.props;

        const start = (currentPage - 1) * pageSize + 1;
        const end = (currentPage === totalPages) ? totalRecords : start + pageSize - 1;

        return (
            <PaginationContainer>
                <div style={{marginLeft: '5px'}}>
                    <Button
                        onClick={reload}
                        leftIcon={'reload'}
                        isGhost={true}
                        data-qaid='refreshButton'
                    >
                        {i18n.t('common:refresh')}
                    </Button>
                </div>
                <Pagination
                    page={currentPage}
                    pages={totalPages}
                    onPageChange={setPage}
                    isDisabled={isDisabled}
                />
                <div style={{marginRight: '10px'}}>
                    &nbsp;
                    {i18n.t('module.data>search.table>displayRecords')}:
                    &nbsp;
                    <PageSize
                        value={`${start} - ${end}`}
                        onChangePageSize={setPageSize}
                        pageSizeOptions={[20, 50, 100]}
                        rowsText={i18n.t('common:page')}
                        isDisabled={isDisabled}
                    />
                </div>
            </PaginationContainer>
        );
    }
}
