/**
 * The component for displaying a table is a wrapper over the react-table that defines the appearance of the table
 *
 * @author: Ilya Brauer
 * @date: 2019-10-08
 */

import * as React from 'react';
import ReactTable, {CellInfo, Column as RTColumn, RowInfo, TableProps} from 'react-table';

import styles from './table.m.scss';
import 'react-table/react-table.css';
import {FooterPanel} from '../footer_panel/FooterPanel';
import {PageSize} from '../page_size/PageSize';
import {Pagination} from '../pagination/Pagination';
import {
    Button,
    Checkbox, CheckboxChangeEvent,
    Icon,
    INTENT,
    joinClassNames,
    SIZE
} from '../../../index';
import {FilterContainer, IFilterData} from '../filter_container/FilterContainer';
import {safeInvoke} from '../../../utils/safeInvoke';

export type Column<T = any> = Omit<RTColumn<T>, 'accessor'> & {
    accessor: string;
}

type IProps = {
    columns: Column[];
    data: Array<{[key: string]: any}>;
    TheadComponent?: React.ReactType;

    filters?: {
        [key: string]: IFilterData;
    };
    filterValues?: {
        [key: string]: any;
    };

    editComponents?: {
        [key: string]: React.ComponentType<any>;
    };
    editableCell?: {
        index: number;
        id: string;
    };

    sort?: {[key: string]: 'ASC' | 'DESC' | undefined};
    checkedRows?: number[];

    onCellEdit?: (rowIndex: number, cellId: string, newValue: string | number) => void;
    onRowCheck?: (rowIndex: number, isCheck: boolean) => void;
    onAllCheck?: (isCheck: boolean) => void;

    onAllDelete?: () => void;
    onRowDelete?: (rowIndex: number) => void;

    onSort?: (columnId: string) => void;

    onFilter?: (columnId: string, isClear?: boolean) => void;

    onRowClick?: (rowIndex: number) => void;
    onRowDoubleClick?: (rowIndex: number) => void;
    onRowMouseEnter?: (rowIndex: number) => void;
    onRowMouseLeave?: (rowIndex: number) => void;

} & Partial<IDefProps>;

interface IDefProps {
    viewRowsCount: number;
    columnMinWidth: number;
    NoDataComponent?: React.ReactType;
    getTrProps?: TableProps['getTrProps'];
    subRows?: React.ReactNode[];

    hasHeaderBg: boolean;
    isStriped: boolean;
    isRowsCheckable: boolean;
    isRowsDeletable: boolean;
    isEditable: boolean;
    hasPagination: boolean;
}

interface IResizeItem {
    id: string;
    value: number;
}

interface IState {
    resized: IResizeItem[];
    selectedRow: number | undefined;
    selectedCell: string | undefined;
    checkedRows: number[];
    editableCell?: {
        index: number;
        id: string;
    };
}

export {
    IFilterData
};

export class Table extends React.Component<IProps & IDefProps, IState> {
    static defaultProps = {
        viewRowsCount: 20,
        columnMinWidth: 50,
        hasHeaderBg: true,
        isStriped: true,
        isRowsCheckable: false,
        isRowsDeletable: false,
        isEditable: false,
        hasPagination: true
    };

    static FooterPanel = FooterPanel;
    static Pagination = Pagination;
    static PageSize = PageSize;

    state: IState = {
        resized: [],
        selectedRow: undefined,
        selectedCell: undefined,
        checkedRows: [],
        editableCell: undefined
    };

    onResizedChange = (data: IResizeItem[]) => {
        const columnMinWidth = this.props.columnMinWidth as number;

        this.setState({
            resized: data.map((item) => {
                return {
                    id: item.id,
                    value: (item.value < columnMinWidth) ? columnMinWidth : item.value
                };
            })
        });
    };

    /**
     * Drawing the substring hide/reveal icon
     */
    renderExpanderComponent = (props: CellInfo) => {
        if (this.props.subRows && this.props.subRows[props.index]) {
            const className = joinClassNames(
                styles.expandIcon,
                [styles.expandIconRotate, props.isExpanded]
            );

            return (
                <div className={className}>
                    <Icon name={'chevron-right-circle'}/>
                </div>
            );
        }

        return null;
    };

    /**
     * Drawing a substring
     */
    renderSubComponent = (props: RowInfo) => {
        if (this.props.subRows && this.props.subRows[props.index]) {
            return (
                <div>
                    {this.props.subRows[props.index]}
                </div>
            );
        }

        return null;
    };

    onCheckboxClick = (e: React.MouseEvent<HTMLElement>) => {
        e.stopPropagation();
    };

    onCheckboxChange = (rowIndex: number) => (e: CheckboxChangeEvent) => {
        const type = e.target.checked;

        if (type === true) {
            this.setState((prevState) => {
                return {
                    checkedRows: prevState.checkedRows.concat(rowIndex)
                };
            });
        } else {
            this.setState((prevState) => {
                return {
                    checkedRows: prevState.checkedRows.reduce<number[]>((result, item) => {
                        if (item !== rowIndex) {
                            result.push(item);
                        }

                        return result;
                    }, [])
                };
            });
        }

        safeInvoke(this.props.onRowCheck, rowIndex, type);
    };

    get CheckboxColumn (): Column<any> {
        const checkedRows = this.state.checkedRows;
        const data = this.props.data;

        return {
            Header: (
                <Checkbox
                    checked={checkedRows.length > 0 && checkedRows.length === data.length}
                    onChange={(e) => {
                        const type = e.target.checked;

                        if (type === true) {
                            this.setState({
                                checkedRows: data.map((_item, index) => index)
                            });
                        } else {
                            this.setState({checkedRows: []});
                        }

                        safeInvoke(this.props.onAllCheck, type);
                    }}
                />
            ),
            resizable: false,
            accessor: '__checked',
            Cell: (props: any) => {
                if (props.index === this.state.selectedRow || checkedRows.length > 0) {
                    return (
                        <Checkbox
                            checked={checkedRows.includes(props.index)}
                            onClick={this.onCheckboxClick}
                            onChange={this.onCheckboxChange(props.index)}
                        />
                    );
                }

                return null;
            },
            width: 26,
            className: styles.checkboxCell
        };
    }

    onAllDelete = () => {
        safeInvoke(this.props.onAllDelete);
    };

    onRowDelete = (rowIndex: number) => (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        event.stopPropagation();
        safeInvoke(this.props.onRowDelete, rowIndex);
    };

    get DeleteColumn (): Column<any> {
        return {
            Header: <Button leftIcon={'delete'} size={SIZE.SMALL} isMinimal={true} onClick={this.onAllDelete}/>,
            accessor: '__delete',
            Cell: (props: any) => {
                if (props.index === this.state.selectedRow) {

                    return (
                        <Button
                            leftIcon={'delete'}
                            intent={INTENT.DANGER}
                            size={SIZE.SMALL}
                            isMinimal={true}
                            onClick={this.onRowDelete(props.index)}
                        />
                    );
                }

                return null;
            },
            width: 28,
            resizable: false,
            className: styles.deleteCell
        };
    }

    onSearch = (id: string, isClear: boolean = false) => () => {
        safeInvoke(this.props.onFilter, id, isClear);
    };

    Filter = (id: string) => {
        const filter = this.props.filters![id];

        return (
            <FilterContainer
                {...filter}
                isChecked={this.props.filterValues![id] ?? false}
                onSearch={this.onSearch(id)}
                onClear={this.onSearch(id, true)}
            />
        );
    };

    onClickHeader = (id: string) => () => {
        if (this.props.sort) {
            safeInvoke(this.props.onSort, id);
        }
    };

    renderHeader = (columnDef: Column<any>, props: CellInfo, col: any) => {
        const headComponent = typeof columnDef.Header === 'function' ? columnDef.Header(props, col) : columnDef.Header;
        const sortType = this.props.sort && this.props.sort[columnDef.accessor as string];

        const headerClassName = joinClassNames(
            styles.headerTextContainer,
            [styles.sortable, this.props.sort !== undefined]
        );

        return (
            <div className={styles.headContainer}>
                <div className={headerClassName} onClick={this.onClickHeader(columnDef.accessor as string)}>
                    <div className={styles.headerText}>{headComponent}</div>
                    {this.props.sort && sortType && (
                        <Icon name={sortType === 'ASC' ? 'sort-amount-asc' : 'sort-amount-desc'}/>
                    )}
                </div>
                {this.props.filters && this.props.filters[columnDef.accessor as string] && this.Filter(columnDef.accessor as string)}
            </div>
        );
    };

    columnPatch = (columnDef: Column) => {
        const column: Column<any> = {
            ...columnDef
        };

        if (columnDef.columns) {
            column.columns = columnDef.columns.map(this.columnPatch);
        }

        if (this.props.isEditable && this.props.editComponents && this.props.editComponents[columnDef.accessor as string]) {
            column.Cell = (props: CellInfo, col: any) => {
                const editableCell = this.state.editableCell || this.props.editableCell;

                if (editableCell?.id === props.column.id && editableCell?.index === props.index) {
                    const EditComponent = this.props.editComponents![columnDef.accessor as string];

                    return (
                        <div className={styles.inputContainer}>
                            <EditComponent
                                value={props.value}
                                original={props.original}
                                onEdit={(value: any) => {
                                    this.setState({editableCell: undefined});

                                    safeInvoke(this.props.onCellEdit, props.index, props.column.id!, value);
                                }}
                            />
                        </div>
                    );
                }

                const cellComponent = typeof columnDef.Cell === 'function' ? columnDef.Cell(props, col) : props.value;

                return (
                    <div className={styles.editableContainer}>
                        <span>{cellComponent}</span>
                        {props.index === this.state.selectedRow && props.column?.id === this.state.selectedCell && (
                            <Button
                                leftIcon={'pencil-line'}
                                size={SIZE.SMALL}
                                isMinimal={true}
                                onClick={(e) => {
                                    e.stopPropagation();
                                    this.setState({editableCell: {id: props.column.id as string, index: props.index}});
                                }}
                            />
                        )}
                    </div>
                );
            };
        } else {
            column.Cell = (props: CellInfo, col: any) => {
                const cellComponent = typeof columnDef.Cell === 'function' ? columnDef.Cell(props, col) : props.value;

                return (
                    <div className={styles.editableContainer}>
                        <span>{cellComponent}</span>
                    </div>
                );
            };
        }

        if (!column.columns) {
            column.Header = (props: CellInfo, col: any) => {
                return this.renderHeader(columnDef, props, col);
            };
        }

        return column;
    };

    get columns () {
        let columns: Column[] = [];

        if (this.props.isRowsCheckable === true) {
            columns.push(this.CheckboxColumn);
        }

        this.props.columns.forEach((columnDef) => {
            const column = this.columnPatch(columnDef);

            columns.push(column);
        });

        if (this.props.isRowsDeletable === true) {
            columns.push(this.DeleteColumn);
        }

        return columns;
    }

    getTrProps = (finalState: any, rowInfo?: RowInfo, column?: Column, instance?: any) => {
        const checkedRows = (this.props.checkedRows || []).concat(this.state.checkedRows);

        if (checkedRows && checkedRows.length > 0 && typeof rowInfo?.index === 'number' && checkedRows.includes(rowInfo?.index)) {
            return {
                className: styles.checkedRow
            };
        }

        return {};
    };

    getTdProps = (finalState: any, rowInfo?: RowInfo, column?: Column, instance?: any) => {
        const rowIndex: number | undefined = rowInfo?.index;
        const cellKey: string | undefined = column?.id;

        return {
            onMouseEnter: () => {
                this.setState({selectedRow: rowIndex, selectedCell: cellKey});
            },
            onMouseLeave: () => {
                this.setState({selectedRow: undefined, selectedCell: undefined});
            },
            onClick: (event: any) => {
                if (rowIndex !== undefined) {
                    safeInvoke(this.props.onRowClick, rowIndex);
                }
            },
            onDoubleClick: () => {
                if (rowIndex !== undefined) {
                    safeInvoke(this.props.onRowDoubleClick, rowIndex);
                }
            }
        };
    };

    NoDataComponent = () => null;

    componentDidUpdate (prevProps: Readonly<IProps & IDefProps>, prevState: Readonly<IState>, snapshot?: any) {
        if (prevProps.data !== this.props.data) {
            this.setState({
                selectedRow: undefined,
                selectedCell: undefined,
                checkedRows: [],
                editableCell: undefined
            });
        }
    }

    render () {
        // You must add a div with the headerTitle class to correctly draw grouped columns
        const columns = this.columns.map((column: Column) => {
            if (Array.isArray(column.columns) && column.columns.length > 0) {
                return {
                    ...column,
                    Header: () => {
                        if (typeof column.Header === 'string') {
                            return <div className={styles.headerTitle}>{column.Header}</div>;
                        }

                        return column.Header;
                    }
                };
            }

            return column;
        });

        const pageSize = (this.props.data.length !== 0 && this.props.data.length < this.props.viewRowsCount) ?
            this.props.viewRowsCount :
            this.props.data.length;

        const isExpandable = this.props.subRows && this.props.subRows.length > 0;

        const className = joinClassNames(
            styles.udTableCommon,
            [styles.hasHeaderBg, this.props.hasHeaderBg === true],
            [styles.isStriped, this.props.isStriped === true],
            [styles.hasPagination, this.props.hasPagination === true]
        );

        return (
            <ReactTable
                className={className}
                pageSize={pageSize}
                columns={columns}
                data={this.props.data}
                sortable={false}
                showPagination={false}
                resized={this.state.resized}
                onResizedChange={this.onResizedChange}
                NoDataComponent={this.NoDataComponent}
                getTrProps={this.getTrProps}
                getTdProps={this.getTdProps}
                ExpanderComponent={isExpandable === true ? this.renderExpanderComponent : undefined}
                SubComponent={isExpandable === true ? this.renderSubComponent : undefined}
                TheadComponent={this.props.TheadComponent}
            />
        );
    }
}
