declare const styles: {
  readonly "udTableCommon": string;
  readonly "greyHref": string;
  readonly "headerTitle": string;
  readonly "expandIcon": string;
  readonly "expandIconRotate": string;
  readonly "hasHeaderBg": string;
  readonly "isStriped": string;
  readonly "checkboxCell": string;
  readonly "deleteCell": string;
  readonly "inputContainer": string;
  readonly "editableContainer": string;
  readonly "checkedRow": string;
  readonly "headContainer": string;
  readonly "headerTextContainer": string;
  readonly "headerText": string;
  readonly "sortable": string;
  readonly "hasPagination": string;
};
export = styles;

