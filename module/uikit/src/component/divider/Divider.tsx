/**
 * Separator
 *
 * @author Ivan Marshalkin
 * @date 2020-05-28
 */

import * as React from 'react';
import {Divider as AntdDivider} from 'antd';

export class Divider extends React.Component<any, any> {
    render () {
        return (
            <AntdDivider>
                {this.props.children}
            </AntdDivider>
        );
    }
}
