/**
 * A standard confirmation dialog box for testing convenience
 *
 * @author: Vladimir Stebunov
 * @date: 2020-04-13
 */
import * as React from 'react';

import {Modal} from './Modal';
import {Button} from '../button/Button';
import {INTENT} from '../../constants/index';
import {i18n} from '@unidata/core-app';
import * as styles from './Confirm.m.scss';

interface IConfrim {
    onConfirm: () => void;
    confirmationMessage: React.ReactNode;
    header?: string;
    isOpen: boolean;
    onClose: () => void;
    confirmText?: string;
    cancelText?: string;
    isDisableConfirm?: boolean;
}

export class Confirm extends React.PureComponent<IConfrim> {

    get cancelButtonText (): string {
        const props = this.props;

        if (props.cancelText) {
            return props.cancelText;
        }

        return i18n.t('common:cancel_noun');
    }

    get confirmButtonText (): string {
        const props = this.props;

        if (props.confirmText) {
            return props.confirmText;
        }

        return i18n.t('common:continue');
    }

    renderFooter = () => {
        const {
            onClose,
            onConfirm,
            isDisableConfirm
        } = this.props;

        return (
            <div className={styles.modalFooter}>
                <Button
                    isRound={true}
                    isGhost={true}
                    onClick={onClose}
                    data-qaid='cancelConfirm'
                >
                    {this.cancelButtonText}
                </Button>
                <Button
                    isRound={true}
                    intent={INTENT.PRIMARY}
                    onClick={onConfirm}
                    data-qaid='confirm'
                    isDisabled={isDisableConfirm}
                >
                    {this.confirmButtonText}
                </Button>
            </div>
        );
    }

    render () {

        const {confirmationMessage, header, isOpen, onClose} = this.props,
            realHeader = header ? header : i18n.t('attention');

        return (
            <Modal
                isOpen={isOpen}
                shouldCloseOnClickOutside={false}
                shouldCloseOnEsc={false}
                header={realHeader}
                footer={this.renderFooter()}
                onClose={onClose}
                data-qaid='confirmbox'
            >
                <div>{confirmationMessage}</div>
        </Modal>
        );
    }
}
