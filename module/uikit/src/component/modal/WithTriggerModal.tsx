/**
 * todo Brauer Ilya - I'm not sure that this component is necessary (21.01.2020)
 */

import * as React from 'react';

import {IProps as IModalProps} from './Modal.types';
import {Modal} from './Modal';

interface IProps extends IModalProps {
    trigger: React.ReactNode;
}

interface IState {
    isOpen: boolean;
}

export class WithTriggerModal extends React.Component<Omit<IProps, 'isOpen'>, IState> {
    state = {
        isOpen: false
    };

    private toggleModal = () => {
        this.setState((prevState) => {
            return {
                isOpen: !prevState.isOpen
            };
        });
    };

    render () {
        const {trigger, ...otherProps} = this.props;

        return (
            <React.Fragment>
                <span onClick={this.toggleModal}>
                    {this.props.trigger}
                </span>
                <Modal isOpen={this.state.isOpen} onClose={this.toggleModal} {...otherProps}>
                    {this.props.children}
                </Modal>
            </React.Fragment>
        );
    }
}
