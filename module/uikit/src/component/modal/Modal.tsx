/**
 * Modal window
 *
 * @author: Ilya Braujer
 * @date: 2019-12-26
 */
import * as React from 'react';

import {Overlay} from './core/overlay/Overlay';
import {joinClassNames} from '../../utils/joinClassNames';
import {filterHtmlProps} from '../../utils/filterHtmlProps';
import {getSizeThemeKey} from '../../utils/getSizeThemeKey';

import {Icon} from '../../index';

import {DefaultProps, IProps, PropsWithDefault, SizeClassNames} from './Modal.types';

import * as theme from './modal.m.scss';

const DEFAULT_BODY_PADDING = 16; // Default padding for the modal body is 16px (see model.m. scs)

export class Modal extends React.PureComponent<IProps> {

    static defaultProps: DefaultProps = {
        shouldCloseOnClickOutside: true,
        shouldCloseOnEsc: true,
        hasCloseIcon: true,
        'data-qaid': 'modal'
    };

    static excludingProps: Array<keyof IProps> = [
        'isOpen',
        'size',
        'header',
        'footer',
        'noBodyPadding',
        'shouldCloseOnClickOutside',
        'shouldCloseOnEsc',
        'hasCloseIcon',
        'onClose',
        'styles'
    ];

    headerRef = React.createRef<HTMLDivElement>();
    bodyRef = React.createRef<HTMLDivElement>();

    componentDidMount (): void {
        this.setHeaderWidth();
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<{}>, snapshot?: any): void {
        this.setHeaderWidth();
    }

    setHeaderWidth = () => {
        if (this.bodyRef.current && this.headerRef.current) {
            const bodyChildren = this.bodyRef.current.children.item(0);
            const headerElement = this.headerRef.current;

            if (bodyChildren) {
                const bodyWidth = bodyChildren.clientWidth;
                const bodyPadding = this.props.noBodyPadding === true ? 0 : DEFAULT_BODY_PADDING;

                if (bodyWidth !== headerElement.offsetWidth) {
                    headerElement.style.width = `${bodyWidth + (bodyPadding * 2)}px`;
                }
            }
        }
    };

    render () {
        const {isOpen, header, size, styles, shouldCloseOnClickOutside, shouldCloseOnEsc, hasCloseIcon, onClose} = this
            .props as PropsWithDefault;

        const style = styles && styles.modal;
        const headerElement = this.renderHeader();
        const footerElement = this.renderFooter();

        let sizeClassName;

        if (size) {
            const sizeThemeKey = getSizeThemeKey<SizeClassNames>('modal', size);

            sizeClassName = theme[sizeThemeKey];
        }

        const className = joinClassNames(
            theme.modal,
            sizeClassName,
            [theme.modalHasHeaderContent, Boolean(header)],
            [theme.modalHasCloseIcon, Boolean(hasCloseIcon)],
            [theme.modalHasHeader, Boolean(headerElement)],
            [theme.modalHasFooter, Boolean(footerElement)],
        );

        const htmlProps = {
            ...filterHtmlProps<IProps, React.HTMLAttributes<HTMLDivElement>>(this.props, Modal.excludingProps),
            style,
            className
        };

        return (
            <Overlay
                isOpen={isOpen}
                hasBackdrop={true}
                shouldCloseOnClickOutside={shouldCloseOnClickOutside}
                shouldCloseOnEsc={shouldCloseOnEsc}
                onClose={onClose}
            >
                <div {...htmlProps} data-qaid={this.props['data-qaid']}>
                    <div className={theme.container}>
                        {headerElement}
                        {this.renderBody()}
                        {footerElement}
                    </div>
                </div>
            </Overlay>
        );
    }

    private renderCloseIcon () {
        if (!this.props.hasCloseIcon) {
            return null;
        }

        const {onClose, styles} = this.props;
        const iconElement: React.ReactNode = <Icon name={'close'} className={theme.closeIcon}/>;
        const style = styles && styles.closeIconContainer;

        return (
            <div
                data-qaid={'tools-close'}
                style={style}
                className={theme.closeIconContainer}
                onClick={onClose}
            >
                {iconElement}
            </div>
        );
    }

    private renderHeader () {
        const {header, styles} = this.props;

        let headerContentElement = null;

        if (header) {
            const className = joinClassNames(theme.headerContent, [
                theme.headerContentHasText,
                typeof header === 'string'
            ]);

            const style = styles && styles.headerContent;

            headerContentElement = (
                <div style={style} className={className}>
                    {header}
                </div>
            );
        }

        const closeIconElement = this.renderCloseIcon();

        if (headerContentElement || closeIconElement) {
            const style = styles && styles.header;

            return (
                <div style={style} className={theme.header} ref={this.headerRef}>
                    {headerContentElement}
                    {closeIconElement}
                </div>
            );
        }

        return null;
    }

    private renderBody () {
        const {styles, noBodyPadding} = this.props;
        const style = styles && styles.body;

        const className = joinClassNames(
            theme.body, [theme.noPadding, Boolean(noBodyPadding)]
        );

        return (
            <div style={style} className={className} ref={this.bodyRef}>
                {this.props.children}
            </div>
        );
    }

    private renderFooter () {
        const {styles, footer} = this.props;

        if (footer) {
            const style = styles && styles.footer;

            return (
                <div style={style} className={theme.footer}>
                    {footer}
                </div>
            );
        }

        return null;
    }
}
