import * as React from 'react';
import {Button, Modal, SIZE} from './../../index';

import {withInfo} from '@storybook/addon-info';
import {storyLayout} from '../../index.stories';

export default {
    title: 'Modal',
    decorators: [withInfo, storyLayout]
};

interface IState {
    isOpen: boolean;
}

class SimpleModal extends React.Component<any, IState> {
    state: IState = {
        isOpen: false
    };

    toggleModal = () => {
        this.setState((prevState) => {
            return {
                isOpen: !prevState.isOpen
            };
        });
    };

    render () {
        return (
            <>
                <Button onClick={this.toggleModal}>Open modal</Button>

                <Modal
                    isOpen={this.state.isOpen}
                    onClose={this.toggleModal}
                    header={'HEADER'}
                    footer={'FOOTER'}
                    size={SIZE.MIDDLE}
                    shouldCloseOnClickOutside={false}
                >
                    Body of Modal
                </Modal>
            </>
        );
    }
}

export const simpleModal = () => {
    return (
        <SimpleModal/>
    );
};
