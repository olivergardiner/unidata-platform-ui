import * as React from 'react';

/**
 * Element in the stack of layers
 */
export interface IStackItem {
    /**
     * Unique ID for each layer
     */
    id: string;
    /**
     * Does the layer have a background
     */
    hasBackdrop: boolean;
}

/**
 * Add a layer to the stack
 */
export type AddToStack = (stackItem: IStackItem) => void;

/**
 * Update a layer in the stack
 */
export type UpdateInStack = (stackItem: IStackItem) => void;

/**
 * Remove a layer from the stack
 */
export type RemoveFromStack = (id: string) => void;

export interface IRenderProps {
    stack: IStackItem[];
    add: AddToStack;
    update: UpdateInStack;
    remove: RemoveFromStack;
}

export type MapRenderProps<MP> = (renderProps: IRenderProps) => MP;

type ExcludeProps<P1, P2> = Pick<P1, Exclude<keyof P1, keyof P2>>;

export type WithOverlayStack = <P extends MP, MP>(
    mapProps: MapRenderProps<MP>,
) => (Child: React.ComponentType<P>) => React.FC<ExcludeProps<P, MP>>;
