/**
 * Synchronization component <body> with a stack of layers.
 *
 * Adds / removes corresponding classes in <body> if there are open layers.
 */

import * as React from 'react';

import * as theme from './bodySynchronizer.m.scss';

interface IBodySyncProps {
    hasOverlays: boolean;
    hasBackdropOverlays: boolean;
}

export class BodySynchronizer extends React.PureComponent<IBodySyncProps> {
    private addClassName = (className?: string) => {
        if (className) {
            document.body.classList.add(className);
        }
    };

    private removeClassName = (className?: string) => {
        if (className) {
            document.body.classList.remove(className);
        }
    };

    componentDidMount () {
        this.addClassName(theme.body);

        if (this.props.hasOverlays) {
            this.addClassName(theme.bodyHasOverlays);
        }

        if (this.props.hasBackdropOverlays) {
            this.addClassName(theme.bodyHasBackdropOverlays);
        }
    }

    componentDidUpdate () {
        if (this.props.hasOverlays) {
            this.addClassName(theme.bodyHasOverlays);
        } else {
            this.removeClassName(theme.bodyHasOverlays);
        }

        if (this.props.hasBackdropOverlays) {
            this.addClassName(theme.bodyHasBackdropOverlays);
        } else {
            this.removeClassName(theme.bodyHasBackdropOverlays);
        }
    }

    componentWillUnmount () {
        this.removeClassName(theme.body);
        this.removeClassName(theme.bodyHasOverlays);
        this.removeClassName(theme.bodyHasBackdropOverlays);
    }

    render () {
        return null;
    }
}
