import {IStackItem} from '../overlays/OverlayStack.types';

type IsContainsInUpperOverlays = (id: string, element: Element, stack: IStackItem[]) => boolean;

export const isContainsInUpperOverlays: IsContainsInUpperOverlays = (id, element, stack) => {
    const isLast = stack[stack.length - 1].id === id;

    if (isLast) {
        return false;
    }

    const index = stack.findIndex((stackItem) => stackItem.id === id);

    if (index >= 0) {
        const stackSlice = stack.slice(index + 1);

        return stackSlice.some((stackItem) => {
            const overlayElement = document.getElementById(stackItem.id);

            if (overlayElement) {
                return element === overlayElement || overlayElement.contains(element);
            }

            return false;
        });
    }

    return false;
};

type UniqueId = () => string;

export const uniqueId: UniqueId = () => {
    return Math.random()
        .toString(36)
        .substr(2, 9);
};
