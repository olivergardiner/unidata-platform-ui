import * as React from 'react';

import {IRenderProps} from '../overlays/OverlayStack.types';

/**
 * Incoming properties
 */
export interface IInputProps {
    /**
     * You can only get external IDS in tests.
     */
    id?: string;
    isOpen: boolean;
    hasBackdrop?: boolean;
}

export interface IProps extends IInputProps {
    overlayStack: IRenderProps;
}

/**
 * Additional properties passed to the overlay
 */
export interface IMixedProps {
    /**
     * Unique layer ID
     */
    id: string;
    /**
     * Is this the last layer on the stack?
     */
    isLast: boolean;
    /**
     * Is this the last layer with a background in the stack?
     */
    isLastBackdrop: boolean;
    /**
     * Is the DOM element contained in the layers above?
     */
    isContainsInUpperOverlays: (targetElement: Element) => boolean;
}

type DefaultPropsKeys = 'hasBackdrop';

export type DefaultProps = Required<Pick<IProps, DefaultPropsKeys>>;

export type PropsWithDefault = IProps & DefaultProps;

type ExcludeProps<P1, P2> = Pick<P1, Exclude<keyof P1, keyof P2>>;

export type WithStackSynchronyzer = any;
// export type WithStackSynchronyzer = <P extends MixedProps & InputProps>(
//     Child: React.ComponentType<P>,
// ) => React.FC<ExcludeProps<P, MixedProps>>;
