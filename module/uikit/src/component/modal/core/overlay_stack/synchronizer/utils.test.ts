import {isContainsInUpperOverlays} from './utils';

const getStackItem = (id: string) => {
    const element = document.createElement('div');

    element.id = id;

    return {
        id,
        element,
        hasBackdrop: false
    };
};

test('Is last stack item', () => {
    const id = '1';
    const targetElement = document.createElement('div');
    const stack = [getStackItem(id)];

    expect(isContainsInUpperOverlays(id, targetElement, stack)).toBe(false);
});

test('Invalid id', () => {
    const id = '1';
    const targetElement = document.createElement('div');
    const stack = [getStackItem(id)];

    expect(isContainsInUpperOverlays('2', targetElement, stack)).toBe(false);
});

test('Target element is upper overlay', () => {
    const id = '1';
    const stackItem = getStackItem('2');

    document.body.appendChild(stackItem.element);
    const stack = [getStackItem(id), stackItem];

    expect(isContainsInUpperOverlays(id, stackItem.element, stack)).toBe(true);
    document.body.removeChild(stackItem.element);
});

test('Target element contains in upper overlay', () => {
    const id = '1';
    const targetElement = document.createElement('div');
    const stackItem = getStackItem('2');

    stackItem.element.appendChild(targetElement);
    document.body.appendChild(stackItem.element);
    const stack = [getStackItem(id), stackItem];

    expect(isContainsInUpperOverlays(id, targetElement, stack)).toBe(true);
    document.body.removeChild(stackItem.element);
});

test('Can\'t find dom element with id', () => {
    const id = '1';
    const targetElement = document.createElement('div');
    const stackItem = getStackItem('2');

    stackItem.element.appendChild(targetElement);
    const stack = [getStackItem(id), stackItem];

    expect(isContainsInUpperOverlays(id, targetElement, stack)).toBe(false);
});
