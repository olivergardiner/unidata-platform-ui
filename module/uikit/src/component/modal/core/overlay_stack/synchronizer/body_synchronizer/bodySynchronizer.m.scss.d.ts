export const body: string;

export const bodyHasOverlays: string;

export const bodyHasBackdropOverlays: string;
