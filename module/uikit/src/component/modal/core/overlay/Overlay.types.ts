import * as React from 'react';

export interface IStyles {
    overlay?: React.CSSProperties;
    backdrop?: React.CSSProperties;
}

export interface IOverlayStackProps {
    id: string;
    isLastBackdrop: boolean;
    isLast: boolean;
    isContainsInUpperOverlays: (targetElement: Element) => boolean;
}

export interface IProps extends React.HTMLAttributes<HTMLDivElement> {
    isOpen: boolean;
    hasBackdrop?: boolean;
    shouldCloseOnClickOutside?: boolean;
    shouldCloseOnEsc?: boolean;
    styles?: IStyles;
    onClose?: (e: Event | React.SyntheticEvent<HTMLDivElement>) => void;
}

type DefaultPropsKeys = 'hasBackdrop' | 'shouldCloseOnClickOutside' | 'shouldCloseOnEsc';

export type DefaultProps = Required<Pick<IProps, DefaultPropsKeys>>;

export type PropsWithDefault = IProps & DefaultProps;

export type FullProps = IProps & IOverlayStackProps;

export type FullPropsWithDefault = PropsWithDefault & IOverlayStackProps;
