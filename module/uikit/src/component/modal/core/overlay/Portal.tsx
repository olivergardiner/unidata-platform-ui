import * as React from 'react';
import * as ReactDOM from 'react-dom';

interface IProps {
    container: HTMLElement;
    defaultContainerId: string;
}

export class Portal extends React.PureComponent<IProps> {
    static defaultProps = {
        container: document.createElement('div'),
        defaultContainerId: 'portal'
    };

    componentDidMount () {
        const {container, defaultContainerId} = this.props;

        if (defaultContainerId) {
            container.id = defaultContainerId;
        }

        if (!document.body.contains(container)) {
            document.body.appendChild(container);
        }
    }

    render () {
        const {container} = this.props;

        return ReactDOM.createPortal(this.props.children, container);
    }
}
