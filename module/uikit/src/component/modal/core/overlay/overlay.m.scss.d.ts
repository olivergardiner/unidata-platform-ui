export const overlay: string;
export const overlayHasBackdrop: string;
export const overlayIsLast: string;
export const overlayIsLastBackdrop: string;
export const backdrop: string;
