import * as React from 'react';
import {SIZE} from '../../constants';

export type SizeClassNames = 'modalSizeXs' | 'modalSizeS' | 'modalSizeM' | 'modalSizeL' | 'modalSizeXl';

export interface IStyles {
    modal?: React.CSSProperties;
    container?: React.CSSProperties;
    header?: React.CSSProperties;
    headerContent?: React.CSSProperties;
    closeIconContainer?: React.CSSProperties;
    closeIcon?: React.CSSProperties;
    body?: React.CSSProperties;
    footer?: React.CSSProperties;
}

export interface IProps extends React.HTMLAttributes<HTMLDivElement> {
    isOpen: boolean;
    header?: React.ReactNode;
    footer?: React.ReactNode;
    size?: SIZE;
    shouldCloseOnClickOutside?: boolean;
    shouldCloseOnEsc?: boolean;
    hasCloseIcon?: boolean;
    noBodyPadding?: boolean;
    styles?: IStyles;
    onClose?: () => void;
    'data-qaid'?: string;
}

type DefaultPropsKeys = 'shouldCloseOnClickOutside' | 'shouldCloseOnEsc' | 'hasCloseIcon' | 'data-qaid';

export type DefaultProps = Required<Pick<IProps, DefaultPropsKeys>>;

export type PropsWithDefault = IProps & DefaultProps;
