import * as React from 'react';
import {InputMask, IInputMaskProps} from './InputMask';
import {IInputProps} from './Input';
import {i18n} from '@unidata/core-app';
import * as styles from './input.m.css';

type IProps = IInputProps & Partial<IInputMaskProps>;

interface IState {
    hasError: boolean;
    errorMessage: string;
}

export class InputTime extends React.Component<IProps, IState> {
    state: IState = {
        hasError: false,
        errorMessage: ''
    };

    onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;

        if (value !== '__:__:__' && !/^(([01][0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]$/.test(value)) {
            this.setState({hasError: true, errorMessage: i18n.t('validation>notATime')});
        } else {
            this.setState({hasError: false, errorMessage: ''});
        }

        if (this.props.onChange) {
            this.props.onChange(e);
        }
    };

    render () {
        const {onChange, ...props} = this.props,
            hasError = this.state.hasError || this.props.hasError,
            errorMessage = this.state.errorMessage || this.props.errorMessage;

        return (
            <InputMask
                mask={'99:99:99'}
                allowClear={true}
                {...props}
                onChange={this.onChange}
                hasError={hasError}
                errorMessage={errorMessage}
                className={hasError ? styles.error : ''}
            />
        );
    }
}
