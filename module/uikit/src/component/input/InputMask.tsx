import * as React from 'react';
import ReactInputMask from 'react-input-mask';
import {IInputProps, Input} from './Input';

export type IInputMaskProps = IInputProps & {
    mask: string | Array<(string | RegExp)>;
    maskPlaceholder?: null | string;
}

export class InputMask extends React.Component<IInputMaskProps, any> {

    render () {
        const {forwardRef, mask, value, onChange, maskPlaceholder, ...props} = this.props;

        return (
            <ReactInputMask mask={mask} value={value} onChange={onChange} maskChar={maskPlaceholder}>
                {(inputProps: any) => {
                    return (
                        <Input
                            {...props}
                            ref={forwardRef}
                            autoComplete={this.props.autoComplete ? this.props.autoComplete : 'off'}
                            {...inputProps}
                        />
                    );
                }}
            </ReactInputMask>
        );
    }
}
