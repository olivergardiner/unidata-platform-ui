/**
 * The main input element
 *
 * @author Ilya Braujer
 * @date 2019-12-26
 */
import * as React from 'react';

import {Input as InputAntd} from 'antd';
import {Size} from '../../constants';

import * as styles from './input.m.css';
import {InputMask} from './InputMask';
import {InputTime} from './InputTime';

export interface ITextAreaProps extends Omit<React.TextareaHTMLAttributes<HTMLTextAreaElement>, 'size'> {
    prefixCls?: string;
    autosize?: boolean;
    autoSize?: boolean;
    onPressEnter?: React.KeyboardEventHandler<HTMLTextAreaElement>;
    'data-qaid'?: string;
}

export interface IInputProps extends Omit<React.InputHTMLAttributes<HTMLInputElement>, 'size' | 'prefix'> {
    onPressEnter?: React.KeyboardEventHandler<HTMLInputElement>;
    addonBefore?: React.ReactNode;
    addonAfter?: React.ReactNode;
    prefix?: React.ReactNode;
    suffix?: React.ReactNode;
    allowClear?: boolean;
    forwardRef?: React.RefObject<InputAntd>;
    size?: Size;
    rows?: number;
    autosize?: boolean | object;
    hasError?: boolean;
    errorMessage?: React.ReactNode;
}

// for ref
export {InputAntd};

export class Input extends React.Component<IInputProps> {
    static TextArea = InputAntd.TextArea;
    static Password = InputAntd.Password;
    static Mask = InputMask;
    static Time = InputTime;

    render () {
        const {forwardRef, hasError, errorMessage, ...props} = this.props;

        return (
            <div className={styles.container}>
                <InputAntd
                    {...props}
                    ref={forwardRef}
                    autoComplete={this.props.autoComplete ? this.props.autoComplete : 'off'}
                />
                {hasError === true && (
                    <div className={styles.errContainer}>
                        {errorMessage}
                    </div>
                )}
            </div>
        );
    }
}
