declare const styles: {
  readonly "container": string;
  readonly "errContainer": string;
  readonly "error": string;
};
export = styles;

