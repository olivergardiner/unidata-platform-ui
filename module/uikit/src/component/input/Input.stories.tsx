import * as React from 'react';
import {Input} from './../../index';

import {withInfo} from '@storybook/addon-info';
import {storyLayout} from '../../index.stories';

export default {
    title: 'Input',
    decorators: [withInfo, storyLayout]
};

export const simpleInput = () => <Input title={'hello'} value={'Hello Button'} hasError={true} errorMessage={'some err'}/>;

export const maskInput = () => {
    const [value, setValue] = React.useState('');

    return (
        <Input.Mask
            title={'hello'}
            mask={'99:99:99'}
            value={value}
            onChange={(e) => setValue(e.currentTarget.value)}
            maskPlaceholder={'0'}
        />
    );
};
