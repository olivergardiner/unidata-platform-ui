import * as React from 'react';
import {withInfo} from '@storybook/addon-info';
import {withKnobs} from '@storybook/addon-knobs';
import {PLACEMENT, Tooltip, TRIGGER} from './../../index';
import {storyLayout} from '../../index.stories';

export default {
    title: 'Tooltip',
    decorators: [withInfo, withKnobs, storyLayout]
};

export const simpleTooltip = () => {

    return (
        <Tooltip
            placement={PLACEMENT.BOTTOM_START}
            trigger={TRIGGER.CLICK}
            overlay={(<span>overlay</span>)}
        >
            <div>children</div>
        </Tooltip>
    );
};
