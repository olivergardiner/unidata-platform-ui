declare const styles: {
  readonly "tooltip": string;
  readonly "tooltipIntentGhost": string;
};
export = styles;

