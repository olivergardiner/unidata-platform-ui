import * as React from 'react';

type FilterHtmlProps = <P extends A, A extends React.DOMAttributes<Element>>(
    allComponentProps: P,
    excludingProps: string[],
) => A;

export const filterHtmlProps: FilterHtmlProps = (allComponentProps, excludingProps) => {
    return Object.entries(allComponentProps).reduce<any>((htmlProps, [key, value]) => {
        if (!excludingProps.includes(key)) {
            htmlProps[key] = value;
        }

        return htmlProps;
    }, {});
};
