/**
 * The function implements a measure of css module classes by combining them separated by a space
 *
 * @author: Aleksandr Bavin
 * @date: 2020-02-20
 */

type ClassNameCollection = {[key: string]: string | undefined};
type ClassNameCollectionOrUndefined = ClassNameCollection | undefined;

/**
 * @deprecated
 * @param classNameCollections
 */
export function mergeClassNamesCollections<T extends ClassNameCollectionOrUndefined> (...classNameCollections: T[]): T {
    let result: {[key: string]: string} = {};

    for (let classNameCollection of classNameCollections) {
        for (let key in classNameCollection) {
            if (Object.prototype.hasOwnProperty.call(classNameCollection, key)) {
                let value = classNameCollection[key];

                if (key in result) {
                    result[key] += ' ' + value;
                } else if (value) {
                    result[key] = value;
                }
            }
        }
    }

    return result as T;
}
