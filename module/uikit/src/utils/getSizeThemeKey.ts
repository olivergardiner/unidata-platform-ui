import {SIZE} from '../constants';
import {capitalize} from './capitalize';

type GetSize = <K extends string>(prefix: string, size: SIZE) => K;

export const getSizeThemeKey: GetSize = (prefix, size) => `${prefix}Size${capitalize(size)}` as any;
