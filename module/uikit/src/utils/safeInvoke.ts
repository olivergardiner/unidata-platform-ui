/**
 * Wrapper for safely calling the callback function.
 *
 * Useful for calling event handlers that may be 'undefined'.
 * The main reason for using this function is to reduce the number of branches in the code,
 * and reduce the number of tests to cover.
 */

type ArgumentTypes<F extends Function> = F extends (...args: infer A) => any ? A : never;

export type SafeInvoke = <F extends Function>(fn?: F, ...args: ArgumentTypes<F>) => void;

export const safeInvoke: SafeInvoke = (fn, ...args) => {
    if (fn) {
        fn(...args);
    }
};
