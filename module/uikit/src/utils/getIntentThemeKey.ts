import {INTENT} from '../constants';
import {capitalize} from './capitalize';
import {IconIntent} from '@unidata/icon';

type GetIntent = <K extends string>(prefix: string, intent: INTENT | IconIntent) => K;

export const getIntentThemeKey: GetIntent = (prefix, intent) => `${prefix}Intent${capitalize(intent)}` as any;
