import * as React from 'react';

import {composeEventHandlers} from './composeEventHandlers';

let e: React.SyntheticEvent;
let handlerOne: jest.Mock;
let handlerTwo: jest.Mock;

beforeEach(() => {
    e = {defaultPrevented: false} as React.SyntheticEvent;
    handlerOne = jest.fn();
    handlerTwo = jest.fn();
});

test('Should call first handler', () => {
    composeEventHandlers(handlerOne, handlerTwo)(e);
    expect(handlerOne).toHaveBeenCalledTimes(1);
});

test('Should call second handler', () => {
    composeEventHandlers(handlerOne, handlerTwo)(e);
    expect(handlerTwo).toHaveBeenCalledTimes(1);
});

test('Should call first handler if not set second handler', () => {
    composeEventHandlers(handlerOne)(e);
    expect(handlerOne).toHaveBeenCalledTimes(1);
});

test('Should call second handler if not set first handler', () => {
    composeEventHandlers(undefined, handlerTwo)(e);
    expect(handlerTwo).toHaveBeenCalledTimes(1);
});

test('Should not call second handler if in first handler set defaultPrevented for event', () => {
    const handlerOne: React.ReactEventHandler = (e) => {
        e.defaultPrevented = true;
    };

    composeEventHandlers(handlerOne, handlerTwo)(e);
    expect(handlerTwo).toHaveBeenCalledTimes(0);
});
