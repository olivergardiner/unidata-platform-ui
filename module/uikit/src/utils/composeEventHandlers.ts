/**
 * Method for compose event handlers and execute them all or stop on 'defaultPrevented'
 */

import * as React from 'react';

import {safeInvoke} from './safeInvoke';

export type ComposeEventHandlers = <E extends React.SyntheticEvent<Element> = React.SyntheticEvent<Element>>(
    externalHandler?: React.EventHandler<E>,
    internalHandler?: React.EventHandler<E>,
) => React.EventHandler<E>;

export const composeEventHandlers: ComposeEventHandlers = (externalHandler, internalHandler) => {
    return (e) => {
        safeInvoke(externalHandler, e);

        if (!e.defaultPrevented) {
            safeInvoke(internalHandler, e);
        }
    };
};
