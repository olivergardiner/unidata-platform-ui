/**
 * Function that receives default props and returns the function,
 * which returns correctly typed props inside React. Component
 *
 * Example
 ==============
 const defaultProps = {
    size: 10
 };

 type DefaultProps = Readonly<typeof defaultProps>;

 type IProps = {
    name: string; // required props
    color?: string; // and not required
 } & Partial<DefaultProps>; // add default

 class SomeComponent extends React.Component<IProps> {
    static defaultProps = defaultProps;
    render () {
        const {name, size, color} = propsGetter(defaultProps)(this.props);

        return (<div>{name} {size} {color || ''}</div>); // in render use every without control
    }
 }

 // Usage
 <SomeComponent name={'name'}/> // required only one props
 ==============
 *
 * @author Brauer Ilya
 * @date 2020-04-10
 */

export const propsGetter = <DP extends object>(defaultProps: DP) => {
    return <P extends Partial<DP>>(props: P) => {
        type PropsExcludingDefaults = Pick<P, Exclude<keyof P, keyof DP>>;

        type RecomposedProps = DP & PropsExcludingDefaults;

        return (props as any) as RecomposedProps;
    };
};
