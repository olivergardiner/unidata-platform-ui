import * as React from 'react';

export type CreateHtmlPropsFilter = <P extends F, F extends React.DOMAttributes<Element>>(
    excludingKeys: Array<keyof P>,
) => (props: P) => F;

export const createHtmlPropsFilter: CreateHtmlPropsFilter = (excludingKeys) => {
    return (props) => {
        type Key = keyof typeof props;

        return Object.entries(props).reduce<any>((htmlProps, [key, value]) => {
            if (!excludingKeys.includes(key as Key)) {
                htmlProps[key] = value;
            }

            return htmlProps;
        }, {});
    };
};
