type ClassName = string | undefined;

type ClassNameDef = ClassName | [ClassName, boolean];

type JoinClassNames = (...classNameDefs: ClassNameDef[]) => string | undefined;

export const joinClassNames: JoinClassNames = (...classNameDefs) => {
    const classNames = classNameDefs.reduce<string[]>((classNames, classNameDef) => {
        if (Array.isArray(classNameDef)) {
            const [className, shouldAdd] = classNameDef;

            if (className && shouldAdd) {
                classNames.push(className);
            }
        } else if (classNameDef) {
            classNames.push(classNameDef);
        }

        return classNames;
    }, []);

    return classNames.join(' ') || undefined;
};
