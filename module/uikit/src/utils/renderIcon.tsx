import * as React from 'react';

import {Icon} from '../component/icon/Icon';

export type RenderIcon = (
    icon: React.ReactNode,
    iconProps: any,
    containerProps: React.HTMLAttributes<HTMLSpanElement>,
) => React.ReactNode;

export const renderIcon: RenderIcon = (icon, iconProps, containerProps) => {
    let iconElement: React.ReactNode = null;

    if (typeof icon === 'string' && icon !== '') {
        iconElement = <Icon name={icon} {...iconProps} />;
    } else if (React.isValidElement(icon)) {
        iconElement = icon;
    }

    if (iconElement) {
        return <span {...containerProps}>{iconElement}</span>;
    }

    return null;
};
