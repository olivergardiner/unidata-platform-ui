import {safeInvoke} from './safeInvoke';

const argOne = 1;
const argTwo = 2;

test('Should call function with arguments', () => {
    const fn = jest.fn();

    safeInvoke(fn, argOne, argTwo);
    expect(fn).toHaveBeenCalledWith(argOne, argTwo);
});

test('Should return undefined if set function', () => {
    expect(safeInvoke(jest.fn(() => true))).toBeUndefined();
});

test('Should return undefined if set fn as undefined', () => {
    expect(safeInvoke(undefined)).toBeUndefined();
});
