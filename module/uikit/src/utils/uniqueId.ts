/**
 * Unique id in module uikit.
 *
 */

type UniqueId = () => string;

let index = 0;

export const uniqueId: UniqueId = () => {
    index = index + 1;

    return index.toString();
};
