import {ALIGN} from '../constants';
import {capitalize} from './capitalize';

type GetAlignThemeKey = <K extends string>(prefix: string, align: ALIGN) => K;

export const getAlignThemeKey: GetAlignThemeKey = (prefix, align) => `${prefix}Align${capitalize(align)}` as any;
