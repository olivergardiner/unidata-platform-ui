import {INTENT, SIZE, ALIGN, PLACEMENT, TRIGGER, GROUP_TYPE} from '../constants';
import {iconData, IconIntent} from '@unidata/icon';
import * as menuColors from '../specific/menu_item/menuColors';
import {ServerErrorSeverity, ToastTypeEnum} from '@unidata/core-app';

const optionalMap = () => ({
    '-- not set --': undefined
});

const enumToMap = (map: any, [key, value]: [string, string]) => {
    map[key] = value
        .split('_')
        .join(' ')
        .toLowerCase();

    return map;
};

export const placementMap = (isOptional = false) => {
    return Object.entries(PLACEMENT).reduce<any>(enumToMap, isOptional ? optionalMap() : {});
};

export const triggerMap = (isOptional = false) => {
    return Object.entries(TRIGGER).reduce<any>(enumToMap, isOptional ? optionalMap() : {});
};

export const enumMap = <K>(enumerate: K, isOptional = false) => {
    return Object.entries(enumerate).reduce<any>(enumToMap, isOptional ? optionalMap() : {});
};

export const intentMap = (isOptional = false) => {
    return Object.entries(INTENT).reduce<any>(enumToMap, isOptional ? optionalMap() : {});
};

export const iconColorMap = (isOptional = false) => {
    return Object.entries(IconIntent).reduce<any>(enumToMap, isOptional ? optionalMap() : {});
};

export const sizeMap = (isOptional = false) => {
    return Object.entries(SIZE).reduce<any>(enumToMap, isOptional ? optionalMap() : {});
};

export const alignMap = (isOptional = false) => {
    return Object.entries(ALIGN).reduce<any>(enumToMap, isOptional ? optionalMap() : {});
};

export const iconMap = (isOptional = false) => {
    return Object.keys(iconData).reduce<any>((iconMap, name) => {
        iconMap[name] = name;

        return iconMap;
    }, isOptional ? optionalMap() : {});
};

export const menuColorMap = (isOptional = false) => {
    return Object.keys(menuColors).reduce<any>((iconMap, name) => {
        iconMap[name] = name;

        return iconMap;
    }, isOptional ? optionalMap() : {});
};

export const groupTypeMap = (isOptional = false) => {
    return Object.entries(GROUP_TYPE).reduce<any>(enumToMap, isOptional ? optionalMap() : {});
};

export const ToastTypeMap = (isOptional = false) => {
    return Object.entries(ToastTypeEnum).reduce<any>(enumToMap, isOptional ? optionalMap() : {});
};

export const SeverityTypeMap = (isOptional = false) => {
    return Object.entries(ServerErrorSeverity).reduce<any>((map: any, [key, value]: [string, string]) => {
        map[key] = value
            .split('_')
            .join(' ');

        return map;
    }, isOptional ? optionalMap() : {});
};
