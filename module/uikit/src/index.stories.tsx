import * as React from 'react';

import '../../../node_modules/antd/dist/antd.min.css';
import {Overlays} from './component/modal/core/overlay_stack/overlays/Overlays';

import {UserLocale} from '@unidata/core-app';
import {Button, Switcher} from './component';
import {INTENT} from './constants';

UserLocale.apply('ru');
import './style.css';

const style: React.CSSProperties = {
    position: 'relative',
    width: '100%',
    height: '100vh',
    overflow: 'auto',
    backgroundSize: '60px 60px',
    backgroundPosition: '0 0, 0 30px, 30px -30px, -30px 0px'
};

export const storyLayout = (storyFn: any) => {
    const [theme, setTheme] = React.useState('light');
    const [hasChess, setChess] = React.useState(true);

    if (theme === 'dark') {
        style.backgroundColor = '#242424';

        if (hasChess) {
            // eslint-disable-next-line max-len
            style.backgroundImage = 'linear-gradient(45deg, #292929 25%, transparent 25%), linear-gradient(-45deg, #292929 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #292929 75%), linear-gradient(-45deg, transparent 75%, #292929 75%)';
        } else {
            style.backgroundImage = 'none';
        }
    } else {
        style.backgroundColor = '#fff';

        if (hasChess) {
            // eslint-disable-next-line max-len
            style.backgroundImage = 'linear-gradient(45deg, #f6f6f6 25%, transparent 25%), linear-gradient(-45deg, #f6f6f6 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #f6f6f6 75%), linear-gradient(-45deg, transparent 75%, #f6f6f6 75%)';
        } else {
            style.backgroundImage = 'none';
        }
    }

    return (
        <Overlays.Provider>
            <div style={{...style}}>
                <div style={{position: 'absolute', top: '10px', left: '10px'}}>
                    <Button intent={INTENT.PRIMARY} onClick={() => setTheme(theme === 'dark' ? 'light' : 'dark')}>toggle theme</Button>
                    <Switcher checked={hasChess} onChange={() => setChess(!hasChess)}/>
                </div>
                <div style={{margin: '100px 50px'}}>
                {storyFn()}
                </div>
            </div>
        </Overlays.Provider>
    );
};
