# Module description:

Module for shared components that are used in the application. Ideally, the components should not be specific.
