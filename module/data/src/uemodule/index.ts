/**
 * List OF UE data modules
 *
 * @author Brauer Ilya
 * @date 2020-04-15
 */

import {UeModuleTypeComponent, UeModuleTypeCallBack} from '@unidata/core';
import {BulkExportWizard} from './bulkwizard/export_record/BulkExportWizard';
import {BulkDeleteWizard} from './bulkwizard/remove_record/BulkDeleteWizard';
import {BulkDeleteRelationFromWizard} from './bulkwizard/remove_relation_from/BulkDeleteRelationFromWizard';
import {BulkModifyWizard} from './bulkwizard/edit_record/BulkModifyWizard';
import {BulkRestoreWizard} from './bulkwizard/restore_record/BulkRestoreWizard';
import {BulkRestorePeriodWizard} from './bulkwizard/restore_record_period/BulkRestorePeriodWizard';

import {LastDataWidget} from './last_data_widget/LastDataWidget';
import {CriteriaItem} from '../component/criteria_item/CriteriaItem';
import {SearchTerm} from '../component/search_term/SearchTerm';
import {DraftList} from './draft_list/DraftList';
import {FavoriteStore} from '../store/FavoriteStore';

export const ueModules = [
    {
        'default': {
            type: UeModuleTypeComponent.BULK_DATA_WIZARD,
            moduleId: 'bulkDataWizardExport',
            active: true,
            system: false,
            component: BulkExportWizard,
            resolver: () => true,
            meta: {
                wizardId: 'EXPORT_RECORDS_TO_XLS'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.BULK_DATA_WIZARD,
            moduleId: 'bulkDataWizardDelete',
            active: true,
            system: false,
            component: BulkDeleteWizard,
            resolver: () => true,
            meta: {
                wizardId: 'REMOVE_RECORDS'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.BULK_DATA_WIZARD,
            moduleId: 'bulkDataWizardDeleteRelFrom',
            active: true,
            system: false,
            component: BulkDeleteRelationFromWizard,
            resolver: () => true,
            meta: {
                wizardId: 'REMOVE_RELATIONS_FROM'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.BULK_DATA_WIZARD,
            moduleId: 'bulkDataWizardEditRecord',
            active: true,
            system: false,
            component: BulkModifyWizard,
            resolver: () => true,
            meta: {
                wizardId: 'MODIFY_RECORDS'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.BULK_DATA_WIZARD,
            moduleId: 'bulkDataWizardRestoreRecord',
            active: true,
            system: false,
            component: BulkRestoreWizard,
            resolver: () => true,
            meta: {
                wizardId: 'RESTORE_RECORDS'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.BULK_DATA_WIZARD,
            moduleId: 'bulkDataWizardRestorePeriod',
            active: true,
            system: false,
            component: BulkRestorePeriodWizard,
            resolver: () => true,
            meta: {
                wizardId: 'RESTORE_RECORD_PERIODS'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.DATA_PAGE_WIDGET,
            moduleId: 'lastDataWidget',
            active: true,
            system: false,
            component: LastDataWidget,
            resolver: () => true,
            meta: {
                dataGrid: LastDataWidget.dataGrid
            }
        }
    },

    // Abstract search criteria
    {
        'default': {
            type: UeModuleTypeComponent.ABSTRACT_SEARCH_CRITERIA,
            moduleId: 'abstractSearchCriteria',
            active: true,
            system: false,
            component: CriteriaItem,
            resolver: () => true,
            meta: {}
        }
    },

    // Abstract search term
    {
        'default': {
            type: UeModuleTypeComponent.ABSTRACT_SEARCH_TERM,
            moduleId: 'abstractSearchTerm',
            active: true,
            system: false,
            component: SearchTerm,
            resolver: () => true,
            meta: {}
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.DRAFT_DATA_LIST,
            moduleId: 'draftDataList',
            active: true,
            system: false,
            component: DraftList,
            resolver: () => true,
            meta: {}
        }
    },
    {
        'default': {
            type: UeModuleTypeCallBack.APP_DATA_LOADER,
            moduleId: 'favoriteStore',
            active: true,
            system: false,
            resolver: () => true,
            meta: {},
            fn: () => {
                const favoriteStore = new FavoriteStore();

                // todo Brauer Ilya - think about how to do better
                (window as any).UnidataReact.manager.FavoriteStore = favoriteStore;

                return favoriteStore.setFavoriteEtalonsMap();
            }
        }
    }
];
