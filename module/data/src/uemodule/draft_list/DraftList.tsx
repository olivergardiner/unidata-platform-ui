/**
 * UE for displaying a list of drafts for registries/directories
 *
 * @author Brauer Ilya
 *
 * @date 2020-05-06
 */

import * as React from 'react';
import {DraftDataService} from '../../service/DraftDataService';
import {Spinner, Table, CellInfo, Column, InitTable} from '@unidata/uikit';
import {MetaRecordStore} from '../../page/search/store/MetaRecordStore/MetaRecordStore';
import {SearchResultStore} from '../../page/search/store/SearchResultStore';
import {FavoriteStore} from '../../store/FavoriteStore';
import {DateTimeUtil, i18n, SearchHit, UserLocale, Uuid} from '@unidata/core-app';
import {SearchHitUtil} from '../../utils/SearchHitUtil';
import {DraftData} from '../../model/DraftData';
import {FormatUtil} from '../../page/search/utils/FormatUtil';
import {
    AliasCodeAttribute,
    ArrayAttribute,
    CodeAttribute,
    SimpleAttribute,
    SearchQuery,
    StringFormField,
    systemVariables
} from '@unidata/meta';
import * as styles from './draftList.m.scss';

interface IProps {
    entityName: string;
}

interface IState {
    records: SearchHit[];
    drafts: { [key: string]: DraftData[] };
    page: number;
    isLoading: boolean;
}

export class DraftList extends React.Component<IProps, IState> {
    limit = 1000;

    metaRecordStore = new MetaRecordStore(new FavoriteStore());
    searchResultStore = new SearchResultStore(this.metaRecordStore);

    state: IState = {
        records: [],
        drafts: {},
        page: 1,
        isLoading: true
    };

    constructor (props: IProps) {
        super(props);

        this.metaRecordStore.getEntityList();
    }

    componentDidMount (): void {
        this.metaRecordStore.setCurrentEntityByName(this.props.entityName)
            .then(this.metaRecordStore.getMetaRecordFull)
            .then(this.fetch);
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<any>, snapshot?: any): void {
        if (prevProps.entityName !== this.props.entityName) {
            this.metaRecordStore.setCurrentEntityByName(this.props.entityName)
                .then(this.metaRecordStore.getMetaRecordFull)
                .then(this.fetch);
        }
    }

    fetch = () => {
        this.setState({isLoading: true});

        let drafts: {[key: string]: DraftData[]} = {};

        DraftDataService.getDraftList({
            entityName: this.props.entityName,
            limit: this.limit,
            start: (this.state.page - 1) * this.limit
        })
            .then((result) => {
                drafts = result;
                const entityIds = Object.keys(result).filter((key) => key !== '');

                if (entityIds.length === 0) {
                    this.searchResultStore.clearStore();

                    return;
                }

                const searchBuilder = new SearchQuery({
                    entity: this.metaRecordStore.currentEntity.entityName,
                    attributes: this.metaRecordStore.getMainVisibleAttributes()
                });

                searchBuilder.add({formFields: this.makeEtalonFormFields(entityIds)});

                const query = searchBuilder.getSearchQuery();

                this.searchResultStore.setSearchQuery(query);

                return this.searchResultStore.getRecords();
            })
            .then(() => {
                const records = drafts[''] && drafts[''].length > 0 ?
                    // eslint-disable-next-line camelcase,@typescript-eslint/naming-convention
                    [new SearchHit({preview: [{field: '$etalon_id', value: '', values: ['']}]})]
                        .concat(this.searchResultStore.records.slice()) :
                    this.searchResultStore.records.slice();

                this.setState({
                    records,
                    drafts
                });
            })
            .finally(() => {
                this.setState({isLoading: false});
            });
    };

    makeEtalonFormFields = (etalons: string[]) => {
        return etalons.map((etalonItem) => {
            return new StringFormField({
                name: systemVariables.$etalonId,
                value: etalonItem
            });
        });
    };

    onChangePage = (page: number) => {
        this.setState({page});
    };

    renderColumns = () => {
        const metaRecordStore = this.metaRecordStore;

        const mainAttributes = metaRecordStore.getMainVisibleAttributes()
            .sort(metaRecordStore.columnsSort);

        const columns: Column[] = [
            {
                Header: `${i18n.t('module.data>draft>records')} (${mainAttributes.map((attr) => attr.displayName).join(', ')})`,
                accessor: 'record',
                Cell: (props: CellInfo) => {
                    const etalonId = SearchHitUtil.getFieldValues(props.original, '$etalon_id')[0];

                    if (props.index === 0 && etalonId === '') {
                        return (<b>{i18n.t('module.data>draft>newRecords')}</b>);
                    }

                    return (
                        <b>
                            {mainAttributes.map((mainAttr) => {
                                const values = SearchHitUtil.getFieldValues(props.original, mainAttr.name);

                                if (values.length === 0) {
                                    return null;
                                }

                                const metaAttribute = mainAttr.metaAttribute as
                                    (SimpleAttribute | CodeAttribute | AliasCodeAttribute | ArrayAttribute);

                                const firstValue = FormatUtil.formatByAttribute(values[0], metaAttribute);

                                const listValue = values.length > 1 ? ` (${values.slice(1).map((value) => {
                                    return FormatUtil.formatByAttribute(value, metaAttribute);
                                }).join(', ')})` : '';

                                let displayableValue = `${firstValue || ''}${listValue}`;

                                if (!displayableValue) {
                                    return null;
                                }

                                return (displayableValue);
                            }).join(', ')}
                        </b>
                    );
                }
            }
        ];

        return columns;
    };


    getSubColumns = () => {
        const columns: Column[] = [
            {
                width: 35,
                accessor: 'empty'
            },
            {
                accessor: 'displayName',
                Cell: (props: CellInfo) => {
                    const record: DraftData = props.original;
                    const etalonId = record.entityId.getValue();
                    const entityName = record.entityName.getValue();
                    const draftId = record.id.getValue();

                    return (
                        <a href={`#/dataview?section=dataview|dataSearch?entityName=${entityName}|etalon?etalonId=${etalonId}&draftId=${draftId}`} target={'_blank'}>
                            {record.displayName.getValue()}
                        </a>
                    );
                }
            },
            {
                accessor: 'updateDate',
                Cell: (props: CellInfo) => {
                    const record: DraftData = props.original;

                    return DateTimeUtil.format(record.updateDate.getValue(), UserLocale.getParseTimestampFormat());
                }
            }
        ];

        return columns;
    };

    renderSubtableSep = () => {
        return (<div style={{background: 'silver', height: '2px'}}/>);
    };

    renderSubRows = () => {
        const rows = this.state.records.reduce<React.ReactNode[]>((result, item: SearchHit) => {
            const etalonId = SearchHitUtil.getFieldValues<string>(item, '$etalon_id')[0];
            const draftData = this.state.drafts[etalonId];

            result.push(
                <Table
                    TheadComponent={this.renderSubtableSep}
                    columns={this.getSubColumns()}
                    data={draftData}
                    viewRowsCount={0}
                    NoDataComponent={() => null}
                />
            );

            return result;
        }, []);

        return rows;
    };

    render () {
        const draftLength = this.state.records.length;
        // 11 experimentally defined number of rows to fit on the draft widget
        const viewRowsCount = draftLength > 11 ? draftLength : 11;

        return (
            <Spinner isShow={this.state.isLoading}>
                {(this.state.records.length === 0 && this.state.isLoading === false) ?
                    (
                        <div className={styles.noDataContainer}>
                            <InitTable
                                svg={null}
                                subText={i18n.t('module.data>draft>noRecords')}
                            />
                        </div>
                    ) :
                    (
                        <>
                            <Table
                                columns={this.renderColumns()}
                                data={this.state.records}
                                subRows={this.renderSubRows()}
                                viewRowsCount={viewRowsCount}
                                NoDataComponent={() => null}
                            />
                            <Table.Pagination
                                hidePages={true}
                                showPageJump={false}
                                onPageChange={this.onChangePage}
                                page={this.state.page}
                                pages={Infinity}
                            />
                        </>
                    )}
            </Spinner>
        );
    }
}
