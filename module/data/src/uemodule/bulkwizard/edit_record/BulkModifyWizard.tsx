/**
 * Wizard for batch record editing operations
 *
 * @author Ivan Marshalkin
 * @date 2020-03-23
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {IEtalonIds} from '../type/EtalonIds';
import * as styles from './BulkModifyWizard.m.scss';
import {ExtWidget} from '@unidata/uikit';
import {EntityType} from '@unidata/types';

export interface IProps {
    entityName: string;
    entityType: EntityType;
    etalonIds: IEtalonIds[];
    query: any;
    queryCount: number;
    classifiers: string[];
    runOperation: (cfg: any) => void;
    operationTitle: string;
    onFinish: () => void;
}

@observer
export class BulkModifyWizard extends React.Component<IProps> {
    render () {
        const self = this;
        const props = this.props;

        return (
            <div className={styles.wizardContainer}>
                <ExtWidget
                    autoDestroy={true}
                    async={true}
                    createWidget={function () {
                        let view;

                        view = window.Ext.widget({
                            xtype: 'steward.search.bulk.reactwizard',
                            height: 500,
                            width: '100%',

                            entityName: props.entityName,
                            entityType: props.entityType,
                            queryParams: props.query,
                            useQueryCount: props.queryCount,
                            operationType: 'MODIFY_RECORDS',
                            operationName: props.operationTitle
                        });

                        view.on('operationfinish', function () {
                            self.props.onFinish();
                        }, this);

                        return view;
                    }}
                    style={{
                        width: '100%',
                        height: '500px'
                    }}
                >
                </ExtWidget>
            </div>
        );
    }
}
