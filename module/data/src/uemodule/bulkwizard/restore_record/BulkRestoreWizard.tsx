/**
 * Wizard batch operation to restore deleted records
 *
 * @author Ivan Marshalkin
 * @date 2020-03-26
 */

import {Wizard} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {IEtalonIds} from '../type/EtalonIds';
import {ConfirmStep} from '../common/step/ConfirmStep';
import * as styles from './BulkRestoreWizard.m.scss';
import {RestoreStep} from './step/RestoreStep';

export interface IProps {
    entityName: string;
    etalonIds: IEtalonIds[];
    query: any;
    queryCount: number;
    classifiers: string[];
    runOperation: (cfg: any) => void;
    operationTitle: string;
    recordCount: number;
}

@observer
export class BulkRestoreWizard extends React.Component<IProps> {

    get steps () {
        const props = this.props;

        return [
            {
                name: 'settings',
                label: i18n.t('module.data>bulk.wizard>stepSettingsTitle'),
                component: <RestoreStep/>
            },
            {
                name: 'confirm',
                label: i18n.t('module.data>bulk.wizard>stepConfirmationTitile'),
                component: (
                    <ConfirmStep
                        operationTitle={props.operationTitle}
                        confirmText={i18n.t('module.data>bulk.wizard>modifyConfirmText', {
                            count: props.recordCount
                        })}
                    />
                )
            }
        ];
    }

    onSubmit () {
        this.runOperation();
    }

    runOperation () {
        let cfg: any = {};

        this.props.runOperation(cfg);
    }

    render () {
        return (
            <Provider>
                <div className={styles.wizardContainer}>
                    <Wizard
                        steps={this.steps}
                        onSubmit={this.onSubmit.bind(this)}
                    />
                </div>
            </Provider>
        );
    }
}
