/**
 * Step of selecting links to delete
 *
 * @author Ivan Marshalkin
 * @date 2020-03-06
 */

import React from 'react';
import {inject, Observer, observer} from 'mobx-react';
import {BulkDeleteRelationFromWizardStore} from '../store/BulkDeleteRelationFromWizardStore';
import {Checkbox, Table, Wizard, CellInfo} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import * as styles from './DeleteStep.m.scss';
import {BulkOperationLimitMessage} from '../../common/BulkOperationLimitMessage';

// props from @inject mobx
interface IInjectedProps {
    wizardStore: BulkDeleteRelationFromWizardStore;
}

interface IProps {
    entityName: string;
}

@inject('wizardStore')
@observer
export class DeleteStep extends React.Component<IProps> {
    get injectedProps () {
        return (this.props as unknown as IInjectedProps);
    }

    get wizardStore () {
        return this.injectedProps.wizardStore;
    }

    componentDidMount (): void {
        const wizardStore = this.wizardStore;

        if (!wizardStore.relationLoaded) {
            wizardStore.loadRelationFrom(this.props.entityName);
        }
    }

    get columns () {
        const self = this;
        const store = this.wizardStore;

        return [
            {
                Header: () => {
                    return (
                        <Observer render={() => (
                            <Checkbox
                                name='required'
                                checked={self.wizardStore.isAllRelationsChecked}
                                onClick={function () {
                                    self.wizardStore.toggleAllRelations();
                                }}
                                onChange={function () {
                                }}
                            />
                        )}/>
                    );
                },
                accessor: 'entityDisplayName',
                id: 'entityDisplayName',
                width: 30,
                sortable: false,
                resizable: false,
                Cell: (props: CellInfo) => {
                    const rowData = store.relationsData[props.index];

                    return (
                        <Observer render={() => (
                            <Checkbox
                                name='required'
                                checked={rowData.checked}
                                onClick={function () {
                                    self.wizardStore.toggleRelation(props.index);
                                }}
                                onChange={function () {
                                }}
                            />
                        )}/>
                    );
                }
            },
            {
                Header: () => {
                    return i18n.t('module.data>bulk.wizard>entityName');
                },
                accessor: 'entityDisplayName',
                id: 'entityDisplayName',
                sortable: false,
                resizable: false,
                Cell: (props: CellInfo) => {
                    const rowData = store.relationsData[props.index];

                    return rowData.entityDisplayNameFrom;
                }
            },
            {
                Header: () => {
                    return i18n.t('module.data>bulk.wizard>relationName');
                },
                accessor: 'relationDisplayName',
                id: 'relationDisplayName',
                sortable: false,
                resizable: false,
                Cell: (props: CellInfo) => {
                    const rowData = store.relationsData[props.index];

                    return rowData.relationDisplayName;
                }
            }
        ];
    }

    get data () {
        return this.wizardStore.relationsData.slice();
    }

    render () {
        const allowNext = this.wizardStore.hasSelectedRelation;

        return (
            <>
                <Wizard.Navigation allowNext={allowNext}/>

                <div className={styles.stepContainer}>
                    <div className={styles.stepBody}>
                        <Table
                            columns={this.columns}
                            data={this.data}
                            viewRowsCount={0}
                            NoDataComponent={() => null}
                        />
                    </div>

                    <BulkOperationLimitMessage/>
                </div>

                <Wizard.Footer allowNext={allowNext}/>
            </>
        );
    }
}
