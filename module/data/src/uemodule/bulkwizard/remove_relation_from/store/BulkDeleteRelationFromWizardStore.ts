/**
 * Store for a batch operation to delete links
 *
 * @author Ivan Marshalkin
 * @date 2020-03-10
 */
import {Entity, MetaRelationService} from '@unidata/meta';
import {action, computed, observable} from 'mobx';
import {RelationDirectionType} from '@unidata/types';
import {Dialog, i18n} from '@unidata/core-app';

export interface IDataRelations {
    checked: boolean;
    relationName: string;
    relationDisplayName: string;
    entityNameFrom: string;
    entityDisplayNameFrom: string;
}

export class BulkDeleteRelationFromWizardStore {
    @observable
    private entityName: string;

    @observable
    public entities: Entity[] = [];

    @observable
    public relationsData: IDataRelations[] = [];

    @action
    public loadRelationFrom (entityname: string) {
        const self = this;
        const promise = MetaRelationService.getRelatedEntities(RelationDirectionType.from, entityname);

        promise.then(
            function (relations) {
                self.setRelations(relations);
                self.relationLoaded = true;
            },
            function () {
                // TODO: add a pop-up
                Dialog.showError(i18n.t('module.data>bulk.wizard>loadRelationFailure'));
            }
        );
    }

    @observable
    public relationLoaded: boolean = false;

    @action
    private setRelations (entities: Entity[]) {
        const self = this;

        this.entities = entities;

        entities.forEach(function (metaRecord) {
            metaRecord.relations.forEach(function (metaRelation) {
                if (metaRelation.toEntity.getValue() === self.entityName && !metaRelation.required.getValue()) {
                    self.relationsData.push({
                        checked: false,
                        relationName: metaRelation.name.getValue(),
                        relationDisplayName: metaRelation.displayName.getValue(),
                        entityNameFrom: metaRecord.name.getValue(),
                        entityDisplayNameFrom: metaRecord.displayName.getValue()
                    });
                }
            });
        });
    }

    @action
    public setEntityName (entityName: string): void {
        this.entityName = entityName;
    }

    @action
    public toggleRelation (index: number) {
        let rowData = this.relationsData[index];

        if (!rowData) {
            return;
        }

        rowData.checked = !rowData.checked;
    }

    @action
    public toggleAllRelations () {
        const checked: boolean = this.isAllRelationsChecked ? false : true;

        this.relationsData.forEach(function (rowData) {
            rowData.checked = checked;
        });
    }

    @computed
    public get isAllRelationsChecked (): boolean {
        let selected: boolean = true;

        if (this.relationsData.length === 0) {
            return false;
        }

        this.relationsData.forEach(function (rowData) {
            if (!rowData.checked) {
                selected = false;
            }
        });

        return selected;
    }

    @computed
    public get hasSelectedRelation (): boolean {
        let selected: boolean = false;

        this.relationsData.forEach(function (rowData) {
            if (rowData.checked) {
                selected = true;
            }
        });

        return selected;
    }

    public getSelectedRelation (): string[] {
        let selected: string[] = [];

        this.relationsData.forEach(function (rowData) {
            if (rowData.checked) {
                selected.push(rowData.relationName);
            }
        });

        return selected;
    }
}

