/**
 * Wizard for a batch operation to delete a link
 *
 * @author Ivan Marshalkin
 * @date 2020-03-06
 */

import {Wizard} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {BulkDeleteRelationFromWizardStore} from './store/BulkDeleteRelationFromWizardStore';
import {DeleteStep} from './step/DeleteStep';
import {ConfirmStep} from '../common/step/ConfirmStep';
import * as styles from './BulkDeleteRelationFromWizard.m.scss';
import {IBulkWizardProps} from '../type/IBulkWizardProps';

type IProps = IBulkWizardProps;

@observer
export class BulkDeleteRelationFromWizard extends React.Component<IProps> {
    wizardStore: BulkDeleteRelationFromWizardStore = new BulkDeleteRelationFromWizardStore();

    componentDidMount (): void {
        this.wizardStore.setEntityName(this.props.entityName);
    }

    get steps () {
        const props = this.props;

        return [
            {
                name: 'settings',
                label: i18n.t('module.data>bulk.wizard>stepSettingsTitle'),
                component: (
                    <DeleteStep entityName={this.props.entityName}/>
                )
            },
            {
                name: 'confirm',
                label: i18n.t('module.data>bulk.wizard>stepConfirmationTitile'),
                component: (
                    <ConfirmStep
                        operationTitle={this.props.operationTitle}
                        confirmText={i18n.t('module.data>bulk.wizard>modifyConfirmText', {
                            count: props.recordCount
                        })}
                    />
                )
            }
        ];
    }

    onSubmit () {
        this.runOperation();
    }

    runOperation () {
        let cfg: any = {
            relationsNames: this.wizardStore.getSelectedRelation()
        };

        this.props.runOperation(cfg);
    }

    render () {
        return (
            <Provider wizardStore={this.wizardStore}>
                <div className={styles.wizardContainer}>
                    <Wizard
                        steps={this.steps}
                        onSubmit={this.onSubmit.bind(this)}
                    />
                </div>
            </Provider>
        );
    }
}
