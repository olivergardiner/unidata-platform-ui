/**
 * The initialization step
 *
 * @author Ivan Marshalkin
 * @date 2020-03-06
 */

import React from 'react';
import {BulkDeleteType} from '../type/BulkDeleteType';
import {i18n} from '@unidata/core-app';
import {inject} from 'mobx-react';
import {BulkDeleteWizardStore} from '../store/BulkDeleteWizardStore';
import {Wizard, Radio} from '@unidata/uikit';
import * as styles from './DeleteStep.m.scss';
import {BulkOperationLimitMessage} from '../../common/BulkOperationLimitMessage';

// props from @inject mobx
interface IInjectedProps {
    wizardStore: BulkDeleteWizardStore;
}

interface IProps {
}

@inject('wizardStore')
export class DeleteStep extends React.Component<IProps> {
    get injectedProps () {
        return this.props as IInjectedProps;
    }

    get wizardStore () {
        return this.injectedProps.wizardStore;
    }

    get deleteModes () {
        return [
            {
                type: BulkDeleteType.HARD,
                description: i18n.t('module.data>bulk.wizard>removeHard')
            },
            {
                type: BulkDeleteType.SOFT,
                description: i18n.t('module.data>bulk.wizard>removeSoft')
            }
        ];
    }

    onDeleteModeChange (e: any) {
        this.wizardStore.setDeleteMode(e.target.value);
    }

    render () {
        const self = this;
        const allowNext = this.wizardStore.allowNextStep;
        const wizardStore = this.wizardStore;

        return (
            <>
                <Wizard.Navigation allowNext={allowNext}/>

                <div className={styles.stepContainer}>
                    <div className={styles.stepBody}>
                        <Radio.Group
                            onChange={self.onDeleteModeChange.bind(self)}
                            value={wizardStore.deleteMode}
                        >
                            {
                                this.deleteModes.map(function (item) {
                                    return (
                                        <React.Fragment key={item.type}>
                                            <Radio
                                                key={item.type}
                                                value={item.type}

                                            >
                                                {item.description}
                                            </Radio>
                                            <br/>
                                        </React.Fragment>
                                    );
                                })
                            }
                        </Radio.Group>
                    </div>

                    <BulkOperationLimitMessage/>
                </div>
                <Wizard.Footer allowNext={allowNext}/>
            </>
        );
    }
}
