/**
 * Wizard for batch operation to delete records
 *
 * @author Ivan Marshalkin
 * @date 2020-03-06
 */

import {Wizard} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {ConfirmStep} from '../common/step/ConfirmStep';
import {BulkDeleteWizardStore} from './store/BulkDeleteWizardStore';
import {DeleteStep} from './step/DeleteStep';
import * as styles from './BulkDeleteWizard.m.scss';
import {IBulkWizardProps} from '../type/IBulkWizardProps';

type IProps = IBulkWizardProps;

@observer
export class BulkDeleteWizard extends React.Component<IProps> {
    wizardStore: BulkDeleteWizardStore = new BulkDeleteWizardStore();

    componentDidMount (): void {
        this.wizardStore.recordCount = this.props.recordCount;
    }

    get steps () {
        const self = this,
            props = this.props;

        return [
            {
                name: 'settings',
                label: i18n.t('module.data>bulk.wizard>stepSettingsTitle'),
                component: <DeleteStep/>
            },
            {
                name: 'confirm',
                label: i18n.t('module.data>bulk.wizard>stepConfirmationTitile'),
                component: (
                    <ConfirmStep
                        operationTitle={props.operationTitle}
                        confirmText={self.wizardStore.deleteModeConfirmText}
                    />
                )
            }
        ];
    }

    onSubmit () {
        this.runOperation();
    }

    runOperation () {
        let cfg: any = {
            wipe: this.wizardStore.getDataWipe()
        };

        this.props.runOperation(cfg);
    }

    render () {
        return (
            <Provider wizardStore={this.wizardStore}>
                <div className={styles.wizardContainer}>
                    <Wizard
                        steps={this.steps}
                        onSubmit={this.onSubmit.bind(this)}
                    />
                </div>
            </Provider>
        );
    }
}
