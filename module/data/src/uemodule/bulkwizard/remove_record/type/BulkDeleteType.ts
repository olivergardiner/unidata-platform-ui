/**
 * List of types of deleting records
 *
 * @author Ivan Marshalkin
 * @date 2020-03-13
 */

export enum BulkDeleteType {
    HARD = 'HARD',
    SOFT = 'SOFT'
}
