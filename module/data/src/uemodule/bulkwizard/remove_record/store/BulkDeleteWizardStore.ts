/**
 * Store wizard for batch operation to delete records
 *
 * @author Ivan Marshalkin
 * @date 2020-03-10
 */

import {action, computed, observable} from 'mobx';
import {BulkDeleteType} from '../type/BulkDeleteType';
import {i18n} from '@unidata/core-app';

export class BulkDeleteWizardStore {
    @observable
    public deleteMode: BulkDeleteType;

    @observable
    public deleteModeConfirmText: string;

    @observable
    public recordCount: number;

    @action
    public setDeleteMode (deleteMode: BulkDeleteType) {
        this.deleteMode = deleteMode;

        if (deleteMode === BulkDeleteType.SOFT) {
            this.deleteModeConfirmText = i18n.t('module.data>bulk.wizard>removeSoftConfirm', {
                count: this.recordCount
            });
        } else {
            this.deleteModeConfirmText = i18n.t('module.data>bulk.wizard>removeHardConfirm', {
                count: this.recordCount
            });
        }
    }

    public getDataWipe (): boolean {
        return this.deleteMode === BulkDeleteType.HARD ? true : false;
    }

    @computed
    public get allowNextStep (): boolean {
        if (this.deleteMode) {
            return true;
        }

        return false;
    }
}
