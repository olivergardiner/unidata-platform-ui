/**
 * Wizard batch operation for exporting records to excel
 *
 * @author Ivan Marshalkin
 * @date 2020-03-06
 */

import {Wizard} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {ExportStep} from './step/ExportStep';
import {ConfirmStep} from '../common/step/ConfirmStep';
import * as styles from './BulkExportWizard.m.scss';
import {IBulkWizardProps} from '../type/IBulkWizardProps';

type IProps = IBulkWizardProps;

@observer
export class BulkExportWizard extends React.Component<IProps> {
    get steps () {
        const props = this.props;

        return [
            {
                name: 'settings',
                label: i18n.t('module.data>bulk.wizard>stepSettingsTitle'),
                component: <ExportStep/>
            },
            {
                name: 'confirm',
                label: i18n.t('module.data>bulk.wizard>stepConfirmationTitile'),
                component: (
                    <ConfirmStep
                        operationTitle={props.operationTitle}
                        confirmText={i18n.t('module.data>bulk.wizard>exportConfirmText', {
                            count: props.recordCount
                        })}
                    />
                )
            }
        ];
    }

    onSubmit () {
        this.runOperation();
    }

    runOperation () {
        let cfg: any = {};

        this.props.runOperation(cfg);
    }

    render () {
        return (
            <Provider>
                <div className={styles.wizardContainer}>
                    <Wizard
                        steps={this.steps}
                        onSubmit={this.onSubmit.bind(this)}
                    />
                </div>
            </Provider>
        );
    }
}
