/**
 * Type for props for batch operations wizards
 *
 * @author Ivan Marshalkin
 * @date 2020-03-30
 */

import {IEtalonIds} from './EtalonIds';
import {SearchTermsRequest} from '../../../service/operation/search/ReadDataList/ReadDataListType';

export interface IBulkWizardProps {
    entityName: string;
    etalonIds: IEtalonIds[];
    query: SearchTermsRequest & {entity: string};
    queryCount: number;
    classifiers: string[];
    runOperation: (cfg: any) => void;
    operationTitle: string;
    recordCount: number;
}
