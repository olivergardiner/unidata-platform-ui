/**
 * List of selected reference periods for performing a batch operation
 *
 * @author Ivan Marshalkin
 * @date 2020-03-11
 */

import {Nullable} from '@unidata/core';

export interface IEtalonIds {
    etalonId: string;
    from: Nullable<string>;
    to: Nullable<string>;
}
