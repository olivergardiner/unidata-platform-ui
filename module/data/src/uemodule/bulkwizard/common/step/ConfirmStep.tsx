/**
 * The confirmation step of the operation
 *
 * @author Ivan Marshalkin
 * @date 2020-03-06
 */

import React from 'react';
import {i18n} from '@unidata/core-app';
import {Wizard} from '@unidata/uikit';
import * as styles from './ConfirmStep.m.scss';

interface IProps {
    operationTitle: string;
    confirmText?: string;
}

export class ConfirmStep extends React.Component<IProps> {
    render () {
        const allowNext = true;
        const props = this.props;

        let html: string = i18n.t('module.data>bulk.wizard>operationWillStart', {name: this.props.operationTitle});

        if (props.confirmText) {
            html = props.confirmText;
        }

        return (
            <>
                <Wizard.Navigation allowNext={allowNext}/>
                <div className={styles.stepContainer}>
                    <p dangerouslySetInnerHTML={{__html: html}}/>
                </div>
                <Wizard.Footer allowNext={allowNext} isSubmitStep={true}/>
            </>
        );
    }
}
