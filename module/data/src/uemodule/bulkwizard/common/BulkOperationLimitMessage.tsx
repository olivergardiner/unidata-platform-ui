/**
 * Component with a message about limiting the quantity in a batch operation
 *
 * @author Ivan Marshalkin
 * @date 2020-03-26
 */

import React from 'react';
import {InlineMessage, UdInlineMessageType} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';

interface IProps {
}

export class BulkOperationLimitMessage extends React.Component<IProps> {
    render () {
        return (
            <InlineMessage
                type={UdInlineMessageType.WARNING}
                iconName={'warning'}
            >
                {i18n.t('module.data>bulk.wizard>operationsLimitsWarning', {
                    limit: '50 000'
                })}
            </InlineMessage>
        );
    }
}
