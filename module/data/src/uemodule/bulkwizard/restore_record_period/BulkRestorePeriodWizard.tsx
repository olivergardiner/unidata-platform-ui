/**
 * Wizard batch operation to restore deleted write periods
 *
 * @author Ivan Marshalkin
 * @date 2020-03-26
 */

import {Wizard} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {ConfirmStep} from '../common/step/ConfirmStep';
import * as styles from './BulkRestorePeriodWizard.m.scss';
import {RestorePeriodStep} from './step/RestorePeriodStep';
import {BulkRestorePeriodWizardStore} from './store/BulkRestorePeriodWizardStore';
import {IBulkWizardProps} from '../type/IBulkWizardProps';

type IProps = IBulkWizardProps;

@observer
export class BulkRestorePeriodWizard extends React.Component<IProps> {
    wizardStore: BulkRestorePeriodWizardStore = new BulkRestorePeriodWizardStore();

    get steps () {
        const props = this.props;

        return [
            {
                name: 'settings',
                label: i18n.t('module.data>bulk.wizard>stepSettingsTitle'),
                component: <RestorePeriodStep/>
            },
            {
                name: 'confirm',
                label: i18n.t('module.data>bulk.wizard>stepConfirmationTitile'),
                component: (
                    <ConfirmStep
                        operationTitle={props.operationTitle}
                        confirmText={i18n.t('module.data>bulk.wizard>modifyConfirmText', {
                            count: props.recordCount
                        })}
                    />
                )
            }
        ];
    }

    onSubmit () {
        this.runOperation();
    }

    runOperation () {
        let cfg: any = {};

        this.props.runOperation(cfg);
    }

    render () {
        return (
            <Provider wizardStore={this.wizardStore}>
                <div className={styles.wizardContainer}>
                    <Wizard
                        steps={this.steps}
                        onSubmit={this.onSubmit.bind(this)}
                    />
                </div>
            </Provider>
        );
    }
}
