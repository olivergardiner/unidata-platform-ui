/**
 * Step of configuring the restore operation for deleted recording periods
 *
 * @author Ivan Marshalkin
 * @date 2020-03-26
 */

import React from 'react';
import {i18n} from '@unidata/core-app';
import {Wizard} from '@unidata/uikit';
import * as styles from './RestorePeriodStep.m.scss';
import {BulkOperationLimitMessage} from '../../common/BulkOperationLimitMessage';

interface IProps {
}

export class RestorePeriodStep extends React.Component<IProps> {
    render () {
        const allowNext = true;

        return (
            <>
                <Wizard.Navigation allowNext={allowNext}/>

                <div className={styles.stepContainer}>
                    <div className={styles.stepBody}>
                        <p>
                            {i18n.t('module.data>bulk.wizard>noOperationSettings')}
                        </p>
                    </div>

                    <BulkOperationLimitMessage/>
                </div>

                <Wizard.Footer allowNext={allowNext}/>
            </>
        );
    }
}
