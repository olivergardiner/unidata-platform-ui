import * as React from 'react';
import {CardPanel, Icon, InitTable, LinkWrapper, Skeleton} from '@unidata/uikit';
import {LastDataStore} from '../../page/dataview/store/LastDataStore';

import * as styles from './lastDataWidget.m.scss';
import {i18n, ModuleName} from '@unidata/core-app';

// ee_modules form webpack is array (see webpack/env/prod.env.js)
const modules: string[] = process.env.EE_MODULES as unknown as string[];

const dataGrid = {
    i: 'lastData',
    x: 0,
    y: 0,
    w: modules.includes(ModuleName.WORKFLOW) ? 7 : 12,
    h: 2
};

export class LastDataWidget extends React.Component<{}, {}> {
    static dataGrid = dataGrid;

    render () {
        const lastData = LastDataStore.get();

        return (
            <CardPanel
                internal={true}
                title={(
                    <>
                        <Icon name={'layoutH'}/> {i18n.t('module.data>search>widgets>actions>lastData')}
                    </>
                )}
            >
                <div className={styles.container}>
                    {lastData.map((dataItem) => {
                        return (
                            <LinkWrapper
                                key={dataItem.timestamp}
                                text={dataItem.currentEntity.entityName}
                                link={
                                    <a target={'_blank'} href={`#/dataview${dataItem.search}`}>
                                        {dataItem.mainDisplayable.join(' / ')}
                                    </a>
                                }
                            />
                        );
                    })}
                    {lastData.length === 0 && (
                        <InitTable
                            mainText={i18n.t('module.data>search>widgets>actions>section')}
                            mainTextBlue={i18n.t('module.data>search>widgets>actions>lastData')}
                            subText={i18n.t('module.data>search>widgets>actions>noLastData')}
                        />
                    )}
                </div>
            </CardPanel>
        );
    }
}
