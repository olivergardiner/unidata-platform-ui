/**
 * Options for getting date records
 *
 * @author Sergey Shishigin
 * @date 2019-10-24
 */
export interface IReadDataRecordOpOptions {
    inactive?: boolean;
    drafts?: boolean;
    diffToDraft?: boolean;
    operationId?: string;
}
