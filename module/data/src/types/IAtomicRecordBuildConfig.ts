/**
 * Config used for building AtomicDataRecord
 *
 * @author Denis Makarov
 * @date 2020-05-07
 */
import {RelationReferenceDiff} from '../model/relation/RelationReferenceDiff';
import {RelationContainsDiff} from '../model/relation/RelationContainsDiff';
import {DataRecord} from '../model';

export interface IAtomicRecordBuildConfig {
    dataRecord: DataRecord;
    relationReferenceDiff: RelationReferenceDiff;
    relationManyToManyDiff: RelationReferenceDiff;
    relationContainsDiff?: RelationContainsDiff;

}
