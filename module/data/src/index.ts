export * from './model';

export {getRefOptions} from './page/search/store/search_panel_store/lookupRefOptions';

import {IModule} from '@unidata/core';
import {hotkeys, menuItems, pages} from './page';
import {ueModules} from './uemodule';

export function init (): IModule {
    return {
        id: 'data',
        uemodules: [
            ...pages,
            ...menuItems,
            ...hotkeys,
            ...ueModules
        ]
    };
}
