/**
 * Star for working with your favorite records
 *
 * @author Brauer Ilya
 * @date 2020-03-03
 */

import {EntityMeta} from '../page/search/store/MetaRecordStore/MetaRecordStoreTypes';
import {action, observable} from 'mobx';
import {FavoriteService} from '../service/FavoriteService';
import {Catalog} from '@unidata/meta';
import {EntityType} from '@unidata/types';

export type FavoriteEtalonsMap = {
    [key: string]: string[];
};

export type FavoriteEntity = EntityMeta & {
    count: number;
};

export class FavoriteStore {
    private instance: FavoriteStore;

    private favoriteEtalonsMap: FavoriteEtalonsMap = {};

    @observable.shallow
    public favoriteEntities: FavoriteEntity[] = [];

    constructor () {
        if (this.instance) {
            return this.instance;
        }

        this.instance = this;
        window.addEventListener('storage', (e) => {
            if (e.key === 'favorite' && e.newValue !== null) {
                this.favoriteEtalonsMap = JSON.parse(e.newValue);
            }
        });
    }

    public setFavoriteEtalonsMap = () => {
        return FavoriteService.readEtalonsMap()
            .then((favoriteEtalonsMap) => {
                this.favoriteEtalonsMap = favoriteEtalonsMap;

                localStorage.setItem('favorite', JSON.stringify(this.favoriteEtalonsMap));
            })
            .catch((err: Error) => {
                console.error(err);
            });
    };

    @action
    public setFavoriteEntities = (entityList: Catalog[]) => {
        this.favoriteEntities = this.makeFavoriteEntityList(entityList, this.favoriteEtalonsMap);
    };

    /**
     * Checking whether an entry is a favorite
     * @param entityName
     * @param etalonId
     */
    public isFavorite (entityName: string, etalonId: string): boolean {
        return this.favoriteEtalonsMap[entityName] && this.favoriteEtalonsMap[entityName].includes(etalonId);
    }

    /**
     * Getting the etalon_id list for a single entity
     * @param entityName
     */
    public getFavoriteForEntity (entityName: string): string[] {
        return this.favoriteEtalonsMap[entityName] || [];
    }

    /**
     * Getting a list of etalon_ids grouped by entityName
     * @param entityName
     */
    public getFavoriteEtalonsMap (): FavoriteEtalonsMap {
        return this.favoriteEtalonsMap;
    }

    /**
     * Add an entry to favorites
     * @param entityName
     * @param etalonId
     */
    public addToFavorite (entityName: string, etalonId: string): Promise<void> {
        return FavoriteService.addToFavorite(entityName, [etalonId])
            .then(() => {
                if (!this.isFavorite(entityName, etalonId)) {
                    if (!this.favoriteEtalonsMap[entityName]) {
                        this.favoriteEtalonsMap[entityName] = [];
                    }

                    this.favoriteEtalonsMap[entityName].push(etalonId);

                    localStorage.setItem('favorite', JSON.stringify(this.favoriteEtalonsMap));
                }
            });
    }

    /**
     * Delete an entry from favorites
     * @param etalonId
     */
    public deleteFromFavorite (entityName: string, etalonId: string): Promise<void> {
        return FavoriteService.deleteFromFavorite(etalonId)
            .then(() => {
                if (this.isFavorite(entityName, etalonId)) {
                    const favoriteIndex = this.favoriteEtalonsMap[entityName].findIndex((item) => item === etalonId);

                    if (favoriteIndex !== -1) {
                        this.favoriteEtalonsMap[entityName].splice(favoriteIndex, 1);

                        localStorage.setItem('favorite', JSON.stringify(this.favoriteEtalonsMap));
                    }
                }
            });
    }

    private makeFavoriteEntityList = (entityList: Catalog[], favoriteEtalonsMap: FavoriteEtalonsMap): FavoriteEntity[] => {
        const favoriteKeys = Object.keys(favoriteEtalonsMap);
        const favoriteEntities: FavoriteEntity[] = [];

        entityList.forEach((entityItem) => {
            entityItem.entities.forEach((entity) => {
                const name = entity.name.getValue();

                if (favoriteKeys.includes(name)) {
                    favoriteEntities.push({
                        entityName: name,
                        entityDisplayName: entity.displayName.getValue(),
                        entityType: EntityType.Entity,
                        count: favoriteEtalonsMap[name].length
                    });
                }
            });
            entityItem.lookupEntities.forEach((lookup) => {
                const name = lookup.name.getValue();

                if (favoriteKeys.includes(name)) {
                    favoriteEntities.push({
                        entityName: name,
                        entityDisplayName: lookup.displayName.getValue(),
                        entityType: EntityType.LookupEntity,
                        count: favoriteEtalonsMap[name].length
                    });
                }
            });
        });

        return favoriteEntities;
    };
}
