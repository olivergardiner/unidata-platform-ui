/**
 * Array attribute formatter interface
 *
 * @author Denis Makarov
 * @date 2020-05-07
 */
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {AttributeArrayValue} from './ArrayAttributeValue';

export interface IDataArrayAttributeFormatter {
    format: (value: AttributeArrayValue, type: SIMPLE_DATA_TYPE, displayValue?: string []) => string [];
}
