/**
 * Simple Attribute value type
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {IStringKeyMap} from '@unidata/core';

// ToDo how to replace IStringKeyMap to specific value type
export type SimpleAttributeValue = string | boolean | number | IStringKeyMap | null

