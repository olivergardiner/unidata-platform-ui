/**
 * Simple attribute formatter interface
 *
 * @author Denis Makarov
 * @date 2020-05-07
 */
import {SimpleAttributeValue} from './SimpleAttributeValue';
import {SIMPLE_DATA_TYPE} from '@unidata/types';

export interface IDataAttributeFormatter {
    format: (value: SimpleAttributeValue, type: SIMPLE_DATA_TYPE, displayValue?: string) => string;
}
