/**
 * Value Formatter for array attribute
 *
 * @author Denis Makarov
 * @date 2020-08-26
 */

import {IDataArrayAttributeFormatter} from './type/IDataArrayAttributeFormatter';
import {IStringKeyMap} from '@unidata/uikit';
import {AttributeArrayValue} from './type/ArrayAttributeValue';
import {SIMPLE_DATA_TYPE} from '@unidata/types';

export class DefaultArrayAttributeFormatter implements IDataArrayAttributeFormatter {
    private dataTypeMappings: IStringKeyMap<Function> = {
        [SIMPLE_DATA_TYPE.LOOKUP]: this.formatLookup,
        [SIMPLE_DATA_TYPE.STRING]: this.formatString
    }

    public format (value: AttributeArrayValue, type: SIMPLE_DATA_TYPE, displayValue: string []): string [] {
        if (!value.length && !displayValue.length) {
            return [];
        }

        let fn = this.dataTypeMappings[type] || this.formatString;

        return fn(value, displayValue);
    }

    private formatString (value: string [], displayValue?: string []) {
        if (displayValue && displayValue.length > 0) {
            return displayValue;
        } else {
            return value;
        }
    }

    private formatLookup (value: string [], displayValue?: string []) {
        if (displayValue && displayValue.length > 0) {
            return displayValue;
        } else {
            return value;
        }
    }
}
