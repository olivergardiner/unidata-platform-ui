/**
 * Value Formatter for simple attribute
 *
 * @author Denis Makarov
 * @date 2020-08-26
 */

import moment from 'moment';
import {IDataAttributeFormatter} from './type/IDataAttributeFormatter';
import {IStringKeyMap} from '@unidata/uikit';
import {SimpleAttributeValue} from './type/SimpleAttributeValue';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {UserLocale} from '@unidata/core-app';

export class DefaultSimpleAttributeFormatter implements IDataAttributeFormatter {
   private dataTypeMappings: IStringKeyMap<Function> = {
        [SIMPLE_DATA_TYPE.STRING]: this.formatString,
        [SIMPLE_DATA_TYPE.BOOLEAN]: this.formatBoolean,
        [SIMPLE_DATA_TYPE.DATE]: this.formatDate,
        [SIMPLE_DATA_TYPE.BLOB]: this.formatFile,
        [SIMPLE_DATA_TYPE.CLOB]: this.formatFile,
        [SIMPLE_DATA_TYPE.TIME]: this.formatTime,
        [SIMPLE_DATA_TYPE.TIMESTAMP]: this.formatTimestamp
    }

    public format (value: SimpleAttributeValue, type: SIMPLE_DATA_TYPE, displayValue?: string): string {
        if (value === null) {
            return '';
        }

        let fn = this.dataTypeMappings[type] || this.formatString;

        return fn(value, displayValue);
    }

    private formatString (value: string, displayValue?: string) {
        return displayValue === undefined ? value : displayValue.toString();
    }

    private formatBoolean (value: boolean) {
        return value.toString();
    }

    private formatFile (value: IStringKeyMap) {
        return value.fileName;
    }

    private formatDate (value: string) {
        return moment(value).isValid() ? moment(value).format(UserLocale.getDateFormat()) : value;
    }

    private formatTimestamp (value: string) {
        return moment(value).isValid() ? moment(value).format(UserLocale.getDateTimeFormat()) : value;
    }

    private formatTime (value: string) {
        return moment(value).isValid() ? moment(value).format(UserLocale.getTimeFormat()) : value;
    }
}
