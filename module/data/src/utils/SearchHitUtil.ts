import {SearchHit, SearchPreview} from '@unidata/core-app';
import {IStringKeyMap} from '@unidata/core';
import {IExtendedValue} from '../service/operation/search/ReadDataList/ReadDataListType';

export class SearchHitUtil {
    /**
     * Returns true if there is a field in the preview with the value fieldName
     *
     * @param searchHit
     * @param fieldName
     */
    public static hasField (searchHit: SearchHit, fieldName: string): boolean {
        let preview: SearchPreview | undefined;

        preview = searchHit.preview.find(function (item) {
            return item.field.getValue() === fieldName;
        });

        if (preview) {
            return true;
        }

        return false;
    }

    /**
     * Returns an object where
     * key: values in the preview field.field
     * value: values in the preview.values field
     *
     * reimplementation of the Unidata.model.search.Searchit.map function To Object Values
     *
     * @param searchHit
     */
    public static toValuesMap (searchHit: SearchHit): IStringKeyMap {
        let result: IStringKeyMap = {};

        searchHit.preview.forEach(function (item) {
            result[item.field.getValue()] = item.values;
        });

        return result;
    }

    /**
     * Returns the value from the preview.values field by the preview value.field
     *
     * Unidata.model.search.SearchHit.getValuesFromObjectMap
     *
     * @param searchHit
     * @param fieldName
     */
    public static getFieldValues<T = any> (searchHit: SearchHit, fieldName: string): T[] {
        const preview = searchHit.preview.find((item) => {
            return item.field.getValue() === fieldName;
        });

        if (preview) {
            return  preview.values.getValue() as any;
        }

        return [];
    }

    /**
     * Returns Model Collection<Extended Value> by the preview value.field
     *
     * @param searchHit
     * @param fieldName
     */
    public static getFieldExtended (searchHit: SearchHit, fieldName: string): IExtendedValue[] | undefined {
        const preview = searchHit.preview.find((item) => {
            return item.field.getValue() === fieldName;
        });

        if (preview) {
            return preview.extendedValues.items;
        }

        return undefined;
    }
}
