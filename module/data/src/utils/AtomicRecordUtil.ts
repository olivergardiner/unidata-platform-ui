/**
 * AtomicRecord Utils
 *
 * @author Denis Makarov
 * @date 2020-05-07
 */

import {AtomicRecord} from '../model/AtomicRecord';
import {IAtomicRecordBuildConfig} from '../types/IAtomicRecordBuildConfig';

export class AtomicRecordUtil {
    public static buildAtomicRecord (config: IAtomicRecordBuildConfig) {
        const {dataRecord,
            relationReferenceDiff,
            relationContainsDiff,
            relationManyToManyDiff
        } = config;

        const atomicRecord = new AtomicRecord({});

        atomicRecord.dataRecord = dataRecord;
        atomicRecord.relationReference = relationReferenceDiff;
        atomicRecord.relationManyToMany = relationManyToManyDiff;

        if (relationContainsDiff) {
            atomicRecord.relationContains = relationContainsDiff;
        }

        return atomicRecord;
    }
}
