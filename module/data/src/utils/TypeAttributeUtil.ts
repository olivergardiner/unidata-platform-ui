/**
 Utility class for displaying / localizing the attribute type

 @author Vladimir Stebunov
 @date 2019-08-03
 */
import find from 'lodash/find';
import {i18n} from '@unidata/core-app';

export interface IAttributeDescription {
    name: string;
    displayName: string;
}

export class TypeAttributeUtil {

    public static getSimpleDataDisplayName (typeName: string): IAttributeDescription | undefined {
        const names = {
            typeCategories: ['simpleDataType', 'enumDataType', 'lookupEntityType', 'linkDataType'],
            simpleDataTypes: [
                {
                    name: 'String',
                    displayName: i18n.t('glossary>string')
                },
                {
                    name: 'Integer',
                    displayName: i18n.t('glossary>integer')
                },
                {
                    name: 'Number',
                    displayName: i18n.t('glossary>number')
                },
                {
                    name: 'Boolean',
                    displayName: i18n.t('glossary>boolean')
                },
                {
                    name: 'Date',
                    displayName: i18n.t('glossary>date')
                },
                {
                    name: 'Timestamp',
                    displayName: i18n.t('glossary>dateTime')
                },
                {
                    name: 'Time',
                    displayName: i18n.t('glossary>time')
                },
                {
                    name: 'Blob',
                    displayName: i18n.t('glossary>file')
                },
                {
                    name: 'Clob',
                    displayName: i18n.t('glossary>textFile')
                }
            ],
            arrayDataTypes: [
                {
                    name: 'String',
                    displayName: i18n.t('glossary>string')
                },
                {
                    name: 'Integer',
                    displayName: i18n.t('glossary>integer')
                },
                {
                    name: 'Number',
                    displayName: i18n.t('glossary>number')
                },
                {
                    name: 'Date',
                    displayName: i18n.t('glossary>date')
                },
                {
                    name: 'Timestamp',
                    displayName: i18n.t('glossary>dateTime')
                },
                {
                    name: 'Time',
                    displayName: i18n.t('glossary>time')
                }
            ],
            operationTypes: [
                {
                    name: 'DIRECT',
                    displayName: i18n.t('module.data>search>query.operationTypeDirect')
                },
                {
                    name: 'CASCADED',
                    displayName: i18n.t('module.data>search>query.operationTypeCascaded')
                },
                {
                    name: 'COPY',
                    displayName: i18n.t('module.data>search>query.operationTypeCopy')
                }
            ]
        };

        return find(names.simpleDataTypes, {name: typeName});
    }
}
