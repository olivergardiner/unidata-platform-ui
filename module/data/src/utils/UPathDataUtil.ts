/**
 * Utils for work with UPathData
 *
 * @author Denis Makarov
 * @date 2020-05-07
 */

import {
    DataAbstractAttribute,
    DataArrayAttribute,
    DataRecord,
    DataSimpleAttribute,
    DataComplexAttribute,
    DataCodeAttribute
} from '../model';
import {NestedRecord} from '../model/NestedRecord';
import {IStringKeyMap} from '@unidata/uikit';

export class UPathDataUtil {
    public static DELIMITER: string = '.';

    /**
     * Builds a map of attributes as a pair [uPathData]: DataAbstractAttribute
     */
    public static buildAttributesPathMap (record: DataRecord): IStringKeyMap<DataAbstractAttribute> {
        return this.buildAttributesMapRecursive(record, {}, '');
    }

    private static buildAttributesMapRecursive (record: DataRecord | NestedRecord, localAttributesMap: IStringKeyMap, localPath: string) {
        let prefix = '';

        if (localPath) {
            prefix = localPath + this.DELIMITER;
        }

        let attributesMap = {...localAttributesMap, ...this.buildSimpleAttributePathMap(record, localAttributesMap, localPath)};

        record.complexAttributes.getRange().forEach((attr, index) => {
            attributesMap = {...attributesMap, [prefix + attr.name.getValue()]: attr}; // adding complex attribute

            attr.nestedRecords.getRange().forEach((nested: NestedRecord, nestedIndex) => {
                attributesMap = {
                    ...attributesMap,
                    ...this.buildAttributesMapRecursive(nested, attributesMap, prefix + attr.name.getValue() + `[${nestedIndex}]`)
                };
            });
        });

        return attributesMap;
    }

    public static buildSimpleAttributePathMap (record: DataRecord | NestedRecord, localAttributesMap: IStringKeyMap, localPaths: string) {
        let allAttributes: DataAbstractAttribute [] = [
            ...record.simpleAttributes.getRange(),
            ...record.arrayAttributes.getRange()
        ];

        if (record instanceof DataRecord) {
            allAttributes.push(...record.codeAttributes.getRange());
        }

        let attributesMap = {
            ...localAttributesMap,
            ...this.buildNonComplexAttributesMap(allAttributes, localPaths)
        };

        return attributesMap;

    }

    private static buildNonComplexAttributesMap (attributes: DataAbstractAttribute [], localPath: string) {
        let prefix = '';

        if (localPath) {
            prefix = localPath + this.DELIMITER;
        }

        return attributes.reduce((result: IStringKeyMap, attr) => {
            result[prefix + attr.name.getValue()] = attr;

            return result;
        }, {});
    }

    public static buildAttributePaths (record: DataRecord | NestedRecord, rootPathParts: string []) {
        let paths: string [];
        let localPathParts: string [] = [];

        rootPathParts = rootPathParts || [];

        paths = this.buildSimpleAttributePaths(record, rootPathParts);

        record.complexAttributes.forEach((complexAttr) => {
            complexAttr.nestedRecords.getRange().forEach((nestedRecord) => {
                localPathParts = [...rootPathParts, complexAttr.name.getValue()];

                if (nestedRecord) {
                    paths = paths.concat(this.buildAttributePaths(nestedRecord, localPathParts));
                }
            });
        });

        return paths;
    }

    public static buildSimpleAttributePaths (record: DataRecord | NestedRecord, rootPathParts: string []) {
        let paths: string [] = [];
        let attributes = record.simpleAttributes;
        let localPathParts: string [] = [];

        rootPathParts = rootPathParts || [];

        attributes.forEach((attr) => {
            localPathParts = rootPathParts.concat(attr.name.getValue());
            paths.push(localPathParts.join(this.DELIMITER));
        });

        return paths;
    }

    public static findAttributesByPaths (record: DataRecord | NestedRecord, metaPaths: string []) {
        let attributes: DataAbstractAttribute [] = [];
        let attribute: DataAbstractAttribute | null;

        if (!metaPaths) {
            return attributes;
        }

        metaPaths.every((path: string) => {
            attribute = this.findFirstAttributeByPath(record, path);

            if (attribute) {
                attributes.push(attribute);
            }

            return Boolean(attribute);
        });

        return attributes;
    }

    public static findFirstAttributeByPath (record: DataRecord | NestedRecord, metaPaths: string | string []) {
        let pathParts: string [];
        let nestedRecord: DataRecord | NestedRecord = record;
        let foundAttribute: DataAbstractAttribute | null = null;
        let result;

        if (typeof metaPaths === 'string') {
            pathParts = metaPaths.split(this.DELIMITER);
        } else {
            pathParts = metaPaths;
        }

        if (pathParts) {
            result = pathParts.every((pathPart: string, index: number) => {
                if (index < pathParts.length - 1) {
                    let complexAttribute = this.findComplexAttributeByPath(nestedRecord, pathPart);

                    if (complexAttribute) {
                        foundAttribute = complexAttribute;
                        nestedRecord = complexAttribute.nestedRecords.get(0);
                    }
                } else {
                    let simpleAttribute = this.findSimpleAttributeByPath(nestedRecord, pathPart);

                    if (simpleAttribute) {
                        foundAttribute = simpleAttribute;
                    } else {
                        let arrayAttribute = this.findArrayAttributeByPath(nestedRecord, pathPart);

                        if (arrayAttribute) {
                            foundAttribute = arrayAttribute;

                            return true;
                        }

                        let codeAttribute = this.findCodeAttributeByPath(nestedRecord, pathPart);

                        if (codeAttribute) {
                            foundAttribute = codeAttribute;
                        } else {
                            let complexAttribute = this.findComplexAttributeByPath(nestedRecord, pathPart);

                            if (complexAttribute) {
                                foundAttribute = complexAttribute;
                            }
                        }
                    }
                }

                return nestedRecord && foundAttribute;
            });
        }

        return result ? foundAttribute : null;
    }

    private static findComplexAttributeByPath (record: DataRecord | NestedRecord, pathPart: string) {
        return record.complexAttributes.find((attribute: DataComplexAttribute) => {
            return attribute.name.getValue() === pathPart;
        }) || null;
    }

    private static findSimpleAttributeByPath (record: DataRecord | NestedRecord, pathPart: string) {
        return record.simpleAttributes.find((attribute: DataSimpleAttribute) => {
            return attribute.name.getValue() === pathPart;
        }) || null;
    }

    private static findArrayAttributeByPath (record: DataRecord | NestedRecord, pathPart: string) {
        return record.arrayAttributes.find((attribute: DataArrayAttribute) => {
            return attribute.name.getValue() === pathPart;
        }) || null;
    }

    private static findCodeAttributeByPath (record: DataRecord | NestedRecord, pathPart: string) {
        let codeAttributes: DataCodeAttribute [] = [];

        if ((record as DataRecord).codeAttributes) {
            codeAttributes = (record as DataRecord).codeAttributes.getRange();
        }

        return codeAttributes.find((attribute: DataCodeAttribute) => {
            return attribute.name.getValue() === pathPart;
        }) || null;
    }
}
