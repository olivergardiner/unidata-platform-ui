/**
 * General methods for working with paths
 *
 * @author Ivan Marshalkin
 * @date 2019-10-22
 */

export class UPath {
    public static DELIMITER: string = '.';

    /**
     * Returns true if the passed string is a path (contains a separator)
     * @param path
     */
    public static isComplexPath (path: string): boolean {
        if (path.indexOf(UPath.DELIMITER) !== -1) {
            return true;
        }

        return false;
    }
}
