/**
 * Auxiliary class for working with metaRecord attributes via the path mechanism
 *
 * @author Ivan Marshalkin
 * @date 2019-10-16
 */

import {
    AliasCodeAttribute,
    ArrayAttribute,
    CodeAttribute,
    ComplexAttribute,
    Entity,
    LookupEntity,
    NestedEntity,
    SimpleAttribute
} from '@unidata/meta';
import {Nullable} from '@unidata/core';
import {UPath} from './UPath';


export class UPathMeta {
    /**
     * Search for an attribute by name on the same level
     *
     * @param metaRecord
     * @param attributeName
     */
    // TODO: the method is commented out it should have limited use, it is not required at the moment
     public static findAttributeByName (
         metaRecord: Entity | LookupEntity | NestedEntity,
         attributeName: string
     ): Nullable<SimpleAttribute | CodeAttribute | AliasCodeAttribute | ComplexAttribute | ArrayAttribute> {
         if (metaRecord instanceof Entity) {
             return this.findSimpleAttributeByName(metaRecord, attributeName) ||
                 this.findComplexAttributeByName(metaRecord, attributeName) ||
                 this.findArrayAttributeByName(metaRecord, attributeName);
         } else if (metaRecord instanceof LookupEntity) {
             return this.findSimpleAttributeByName(metaRecord, attributeName) ||
                 this.findCodeAttributeByName(metaRecord, attributeName) ||
                 this.findAliasCodeAttributeByName(metaRecord, attributeName) ||
                 this.findArrayAttributeByName(metaRecord, attributeName);
         } else if (metaRecord instanceof NestedEntity) {
             return this.findSimpleAttributeByName(metaRecord, attributeName) ||
                 this.findComplexAttributeByName(metaRecord, attributeName) ||
                 this.findArrayAttributeByName(metaRecord, attributeName);
         }

         return null;
     }

    /**
     * Search for a code attribute by name
     *
     * @param metaRecord
     * @param attributeName
     */
    public static findCodeAttributeByName (metaRecord: LookupEntity, attributeName: string): Nullable<CodeAttribute> {
        if (metaRecord.codeAttribute && metaRecord.codeAttribute.name.getValue() === attributeName) {
            return metaRecord.codeAttribute;
        }

        return null;
    }

    /**
     * Search for an alternative code attribute by name
     *
     * @param metaRecord
     * @param attributeName
     */
    public static findAliasCodeAttributeByName (metaRecord: LookupEntity, attributeName: string): Nullable<AliasCodeAttribute> {
        let resultAttribute: Nullable<AliasCodeAttribute>;

        resultAttribute = metaRecord.aliasCodeAttributes.find2(function (attribute) {
            return attribute.name.getValue() === attributeName;
        });

        return resultAttribute;
    }

    /**
     * Search for a simple attribute by name
     *
     * @param metaRecord
     * @param attributeName
     */
    public static findSimpleAttributeByName (
        metaRecord: Entity | LookupEntity | NestedEntity,
        attributeName: string
    ): Nullable<SimpleAttribute> {
        let resultAttribute: Nullable<SimpleAttribute>;

        resultAttribute = metaRecord.simpleAttributes.find2(function (attribute) {
            return attribute.name.getValue() === attributeName;
        });

        return resultAttribute;
    }

    /**
     * Searching for a complex attribute by name
     *
     * @param metaRecord
     * @param attributeName
     */
    public static findComplexAttributeByName (metaRecord: Entity | NestedEntity, attributeName: string): Nullable<ComplexAttribute> {
        let resultAttribute: Nullable<ComplexAttribute>;

        resultAttribute = metaRecord.complexAttributes.find2(function (attribute) {
            return attribute.name.getValue() === attributeName;
        });

        return resultAttribute;
    }

    /**
     * Search for an attribute-array of an attribute by name
     *
     * @param metaRecord
     * @param attributeName
     */
    public static findArrayAttributeByName (
        metaRecord: Entity | LookupEntity | NestedEntity,
        attributeName: string
    ): Nullable<ArrayAttribute> {
        let resultAttribute: Nullable<ArrayAttribute>;

        resultAttribute = metaRecord.arrayAttributes.find2(function (attribute) {
            return attribute.name.getValue() === attributeName;
        });

        return resultAttribute;
    }

    /**
     * Searching for an attribute by path
     *
     * @param metaRecord
     * @param attributePath
     *
     * Example:
     * findAttributeByPath(meta, 'complAttr1.complAttr2.simpleAttr1');
     */
    public static findAttributeByPath (
        metaRecord: Entity | LookupEntity | NestedEntity,
        attributePath: string | string[]
    ): Nullable<SimpleAttribute | CodeAttribute | AliasCodeAttribute | ComplexAttribute | ArrayAttribute> {
        let path: string[],
            firstAttributeName: string,
            complexAttribute: Nullable<ComplexAttribute> = null,
            delimiter = UPath.DELIMITER;

        path = (typeof attributePath === 'string') ? attributePath.split(delimiter) : attributePath;

        if (!path.length) {
            return null;
        }

        firstAttributeName = path[0];

        if (metaRecord instanceof LookupEntity) {
            // LookupEntity do not have complex attributes
            if (path.length > 1) {
                return null;
            }

            return this.findAttributeByName(metaRecord, firstAttributeName);
        }

        if (path.length === 1) {
            return this.findAttributeByName(metaRecord, firstAttributeName);
        } else {
            complexAttribute = this.findComplexAttributeByName(metaRecord, firstAttributeName);

            if (!complexAttribute || !complexAttribute.nestedEntity) {
                return null;
            }

            // remove first firstAttributeName
            [, ...path] = path;

            return this.findAttributeByPath(complexAttribute.nestedEntity, path);
        }
    }

    /**
     * Search for multiple attributes by path
     *
     * @param metaRecord
     * @param attributePaths
     */
    public static findAttributesByPaths (
        metaRecord: Entity | LookupEntity | NestedEntity,
        attributePaths: Array<string | string[]>
    ): Array<Nullable<SimpleAttribute | CodeAttribute | AliasCodeAttribute | ComplexAttribute | ArrayAttribute>> {
        let self = this,
            result: Array<Nullable<SimpleAttribute | CodeAttribute | AliasCodeAttribute | ComplexAttribute | ArrayAttribute>> = [];

        return attributePaths.map(function (attributePath) {
            return self.findAttributeByPath(metaRecord, attributePath);
        });
    }
}
