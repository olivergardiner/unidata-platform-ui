/**
 * Utils for data record
 * @author: Sergey Shishigin
 * @date: 2019-09-26
 */
import {IStringKeyMap, ModelCollection} from '@unidata/core';
import {
    DataAbstractAttribute,
    DataArrayAttribute,
    DataArrayAttributeItem,
    DataSimpleAttribute,
    DataComplexAttribute,
    DataCodeAttribute
} from '../index';
import map from 'lodash/map';
import {AbstractAttribute, ArrayAttribute, CodeAttribute, ComplexAttribute, SimpleAttribute} from '@unidata/meta';

export class DataAttributeUtils {
    public static getAttributeByName (collection: ModelCollection<DataAbstractAttribute>, name: string): DataAbstractAttribute {
        let found: DataAbstractAttribute;

        found = collection.find((attribute: DataAbstractAttribute) => {
            return attribute.name.getValue() === name;
        });

        return found;
    }

    public static getSimpleAttributeValueByName (collection: ModelCollection<DataSimpleAttribute>, name: string): any {
        let attribute: DataSimpleAttribute,
            value: any = null;

        attribute = DataAttributeUtils.getAttributeByName(collection, name) as DataSimpleAttribute;

        if (attribute) {
            value = attribute.displayValue.getValue() || attribute.value.getValue();
        }

        return value;
    }

    public static getArrayAttributeValueByName (collection: ModelCollection<DataArrayAttribute>,  name: string): any[] {
        let attribute: DataArrayAttribute,
            value: any = null,
            arrayValues: DataArrayAttributeItem[];

        attribute = DataAttributeUtils.getAttributeByName(collection, name) as DataArrayAttribute;

        if (attribute) {
            arrayValues = attribute.value.items.slice();
            value = map(arrayValues, (item: DataArrayAttributeItem) => {
                return item.displayValue.getValue() || item.value.getValue();
            });
        }

        return value;
    }

    public static buildDataAttributeByMetaAttribute (metaAttribute: AbstractAttribute, config: IStringKeyMap = {}) {
        let dataAttribute: DataAbstractAttribute;

        config = {...config, name: metaAttribute.name.getValue()};

        if (metaAttribute instanceof SimpleAttribute) {
            dataAttribute = new DataSimpleAttribute(config);
        } else if (metaAttribute instanceof ArrayAttribute) {
            dataAttribute = new DataArrayAttribute({...config, value: []});
        } else if (metaAttribute instanceof ComplexAttribute) {
            dataAttribute = new DataComplexAttribute(config);
        } else if (metaAttribute instanceof CodeAttribute) {
            dataAttribute = new DataCodeAttribute(config);
        } else {
            throw new Error('MetaAttribute type is not supported!');
        }

        return dataAttribute;
    }

}
