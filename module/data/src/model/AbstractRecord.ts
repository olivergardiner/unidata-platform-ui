import {AbstractModel, booleanField, BooleanField, StringField, stringField} from '@unidata/core';
import {observable} from 'mobx';

export class AbstractRecord extends AbstractModel {
    @observable
    @stringField()
    public entityName: StringField;

    @observable
    @stringField()
    public version: StringField;

    @observable
    @booleanField()
    public modified: BooleanField;

    // replace with datetime field
    @observable
    @stringField()
    public validFrom: StringField;

    // replace with datetime field
    @observable
    @stringField()
    public validTo: StringField;

    // replace with datetime field
    @observable
    @stringField()
    public createDate: StringField;

    // replace with datetime field
    @observable
    @stringField()
    public updateDate: StringField;

    @observable
    @stringField()
    public status: StringField;

    @observable
    @stringField()
    public approval: StringField;
}
