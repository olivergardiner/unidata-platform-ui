/**
 * Data record
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {AbstractRecord} from './AbstractRecord';
import {hasMany, IAssociationDescriptorMap, ModelCollection, ModelMetaData, StringField, stringField} from '@unidata/core';
import {DataSimpleAttribute} from './attribute/DataSimpleAttribute';
import {DataComplexAttribute} from './attribute/DataComplexAttribute';
import {DataArrayAttribute} from './attribute/DataArrayAttribute';
import {DataCodeAttribute} from './attribute/DataCodeAttribute';

export class DataRecord extends AbstractRecord {
    @stringField({primaryKey: true})
    public etalonId: StringField;

    @hasMany()
    public simpleAttributes: ModelCollection<DataSimpleAttribute>;

    @hasMany()
    public complexAttributes: ModelCollection<DataComplexAttribute>;

    @hasMany()
    public arrayAttributes: ModelCollection<DataArrayAttribute>;

    @hasMany()
    public codeAttributes: ModelCollection<DataCodeAttribute>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                simpleAttributes: DataSimpleAttribute,
                complexAttributes: DataComplexAttribute,
                arrayAttributes: DataArrayAttribute,
                codeAttributes: DataCodeAttribute
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public getAllAttributes () {
        return [
            ...this.simpleAttributes.getRange(),
            ...this.arrayAttributes.getRange(),
            ...this.complexAttributes.getRange(),
            ...this.codeAttributes.getRange()
        ];
    }

}
