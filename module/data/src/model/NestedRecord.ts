import {AbstractModel, hasMany, IAssociationDescriptorMap, ModelCollection, ModelMetaData} from '@unidata/core';
import {DataSimpleAttribute} from './attribute/DataSimpleAttribute';
import {DataArrayAttribute} from './attribute/DataArrayAttribute';
import {DataComplexAttribute} from './attribute/DataComplexAttribute';
import {observable} from 'mobx';

export class NestedRecord extends AbstractModel {
    @observable
    @hasMany()
    public simpleAttributes: ModelCollection<DataSimpleAttribute>;

    @observable
    @hasMany()
    public complexAttributes: ModelCollection<DataComplexAttribute>;

    @observable
    @hasMany()
    public arrayAttributes: ModelCollection<DataArrayAttribute>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                simpleAttributes: DataSimpleAttribute,
                complexAttributes: DataComplexAttribute,
                arrayAttributes: DataArrayAttribute
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
