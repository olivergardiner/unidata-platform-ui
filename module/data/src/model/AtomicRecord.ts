/**
 * Atomic data record
 * Entity for data record save operation
 * Contains data record and relation diffs
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {observable} from 'mobx';
import {AbstractModel, hasOne, IAssociationDescriptorMap, ModelMetaData} from '@unidata/core';
import {DataRecord} from '../index';
import {RelationReferenceDiff} from './relation/RelationReferenceDiff';
import {RelationContainsDiff} from './relation/RelationContainsDiff';

export class AtomicRecord extends AbstractModel {
    @observable
    @hasOne()
    public dataRecord: DataRecord;

    @observable
    @hasOne()
    public relationReference: RelationReferenceDiff;

    @observable
    @hasOne()
    public relationContains: RelationContainsDiff;

    @observable
    @hasOne()
    public relationManyToMany: RelationReferenceDiff;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasOne: {
                dataRecord: DataRecord,
                relationReference: RelationReferenceDiff,
                relationContains: RelationContainsDiff,
                relationManyToMany: RelationReferenceDiff
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

}
