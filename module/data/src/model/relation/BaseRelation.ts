import {
    AbstractModel,
    hasMany,
    IAssociationDescriptorMap,
    ModelCollection,
    ModelMetaData,
    StringField,
    stringField
} from '@unidata/core';
import {observable} from 'mobx';
import {DataSimpleAttribute} from '../attribute/DataSimpleAttribute';
import {DataComplexAttribute} from '../attribute/DataComplexAttribute';
import {DataArrayAttribute} from '../attribute/DataArrayAttribute';
import {DqError} from '../DqError';

/**
 *  Base class for relations of the RelationManyToMany and RelationReference types
 *
 * Note: Relation Contains IS not inherited from this class, because it is still very different: it contains a record inside it
 *
 * @author Sergey Shishigin
 * @date 2019-12-11
 */

export class BaseRelation extends AbstractModel {
    @observable
    @stringField()
    public etalonId: StringField;

    @observable
    @stringField()
    public etalonIdTo: StringField;

    @observable
    @stringField()
    public etalonIdFrom: StringField;

    @observable
    @stringField()
    public etalonDisplayNameTo: StringField;

    @observable
    @stringField()
    public relName: StringField;

    // replace with datetime field
    @observable
    @stringField({allowNull: true})
    public validFrom: StringField;

    // replace with datetime field
    @observable
    @stringField({allowNull: true})
    public validTo: StringField;

    @observable
    @hasMany({ofType: DataSimpleAttribute})
    public simpleAttributes: ModelCollection<DataSimpleAttribute>;

    @observable
    @hasMany({ofType: DataComplexAttribute})
    public complexAttributes: ModelCollection<DataComplexAttribute>;

    @observable
    @hasMany({ofType: DataArrayAttribute})
    public arrayAttributes: ModelCollection<DataArrayAttribute>;

    @observable
    @hasMany({ofType: DqError})
    public dqErrors: ModelCollection<DqError>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                simpleAttributes: DataSimpleAttribute,
                complexAttributes: DataComplexAttribute,
                arrayAttributes: DataArrayAttribute,
                dqErrors: DqError
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
