/**
 * Diff for reference relation type
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {observable} from 'mobx';
import {AbstractModel, hasMany, IAssociationDescriptorMap, ModelMetaData} from '@unidata/core';
import {RelationReference} from './RelationReference';
import {RelationReferenceDelete} from './RelationReferenceDelete';

export class RelationReferenceDiff extends AbstractModel {
    @observable
    @hasMany()
    public toUpdate: RelationReference;

    @observable
    @hasMany()
    public toDelete: RelationReferenceDelete;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                toUpdate: RelationReference,
                toDelete: RelationReferenceDelete
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
