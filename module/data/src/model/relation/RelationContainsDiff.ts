/**
 * Diff for contains relation type
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {observable} from 'mobx';
import {AbstractModel, hasMany, IAssociationDescriptorMap, ModelMetaData} from '@unidata/core';
import {RelationContains} from './RelationContains';

export class RelationContainsDiff extends AbstractModel {
    @observable
    @hasMany()
    public toUpdate: RelationContains;

    @observable
    @hasMany()
    public toDelete: RelationContains;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                toUpdate: RelationContains,
                toDelete: RelationContains
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
