/**
 * The model ties the record
 *
 * @author: Ilya Brauer
 * @date: 2019-10-17
 */

import {
    AbstractModel,
    hasMany,
    IAssociationDescriptorMap,
    IntegerField,
    integerField,
    ModelCollection,
    ModelMetaData,
    stringField,
    StringField
} from '@unidata/core';
import {SearchRelationItem} from './SearchRelationItem';

export class SearchRelation extends AbstractModel {

    @hasMany()
    public data: ModelCollection<SearchRelationItem>;

    @stringField()
    public etalonId: StringField;

    @stringField()
    public relName: StringField;

    @integerField()
    public total: IntegerField;

    @stringField()
    public validFrom: StringField;

    @stringField()
    public validTo: StringField;

    public initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                data: SearchRelationItem
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
