/**
 * M2M communication model
 *
 * @author Sergey Shishigin
 * @date 2019-12-11
 */
import {BaseRelation} from './BaseRelation';

export class RelationManyToMany extends BaseRelation {
}
