/**
 * Model of a single link to display
 *
 * @author: Ilya Brauer
 * @date: 2019-10-17
 */

import {AbstractModel, StringField, stringField} from '@unidata/core';

export class SearchRelationItem extends AbstractModel {

    @stringField()
    public etalonDisplayNameTo: StringField;

    @stringField()
    public etalonIdFrom: StringField;

    @stringField()
    public etalonIdTo: StringField;

    @stringField()
    public relName: StringField;

    @stringField()
    public validFrom: StringField;

    @stringField()
    public validTo: StringField;
}
