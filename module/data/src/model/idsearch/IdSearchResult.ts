/**
 * Model for id search results
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-27
 */
import {
    AbstractModel,
    hasMany,
    IAssociationDescriptorMap,
    ModelCollection,
    ModelMetaData,
    StringField,
    stringField
} from '@unidata/core';
import {IdSearchOriginKey} from './IdSearchOriginKey';

export class IdSearchResult extends AbstractModel {

    @stringField()
    public etalonId: StringField;

    @hasMany()
    public originKeys: ModelCollection<IdSearchOriginKey>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                originKeys: IdSearchOriginKey
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

}
