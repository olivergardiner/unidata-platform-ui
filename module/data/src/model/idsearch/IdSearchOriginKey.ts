/**
 * Model for id search results
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-27
 */
import {AbstractModel, booleanField, BooleanField, StringField, stringField} from '@unidata/core';

export class IdSearchOriginKey extends AbstractModel {

    @stringField({primaryKey: true})
    public id: StringField;

    @stringField()
    public externalId: StringField;

    @stringField()
    public sourceSystem: StringField;

    @stringField()
    public entityName: StringField;

    @booleanField()
    public enrichment: BooleanField;

}

