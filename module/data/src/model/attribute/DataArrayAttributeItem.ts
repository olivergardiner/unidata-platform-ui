/**
 * Array attribute item
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {AbstractModel, anyField, AnyField, stringField, StringField} from '@unidata/core';
import {observable} from 'mobx';

export class DataArrayAttributeItem extends AbstractModel {
    @observable
    @stringField()
    public targetEtalonId: StringField;

    @observable
    @anyField()
    public value: AnyField;

    @observable
    @anyField()
    public displayValue: AnyField;
}
