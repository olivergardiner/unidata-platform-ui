/**
 * Abstract class for attribute with value
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {DataAbstractAttribute} from './DataAbstractAttribute';
import {DataArrayAttributeItem} from './DataArrayAttributeItem';
import {IDataAttributeFormatter} from '../../formatter/type/IDataAttributeFormatter';
import {IDataArrayAttributeFormatter} from '../../formatter/type/IDataArrayAttributeFormatter';
import {AnyField, EnumField, ModelCollection, stringField, AbstractField} from '@unidata/core';
import {SIMPLE_DATA_TYPE} from '@unidata/types';

export abstract class DataValuedAttribute extends DataAbstractAttribute {
    public abstract value: ModelCollection<DataArrayAttributeItem> | AbstractField<AnyField>;

    @stringField({defaultValue: SIMPLE_DATA_TYPE.STRING})
    public type: EnumField<SIMPLE_DATA_TYPE>;

    public getFormattedValue (formatter: IDataAttributeFormatter | IDataArrayAttributeFormatter) {
        let valueField = this.value;

        if (valueField && valueField instanceof AbstractField) {
            let value: any = valueField.getValue();

            // ToDo do it better? so far empty string for unknown type
            if (!(typeof value === 'string')) {
               value = '';
            }

            return formatter.format(value, this.type.getValue());
        }

        return '';
    }
}

