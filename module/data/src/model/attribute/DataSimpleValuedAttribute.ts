/**
 * Attribute with AnyField value
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {DataValuedAttribute} from './DataValuedAttribute';
import {observable} from 'mobx';
import {AnyField, anyField} from '@unidata/core';

export class DataSimpleValuedAttribute extends DataValuedAttribute {
    @observable
    @anyField()
    public value: AnyField;
}
