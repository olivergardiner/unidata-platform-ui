/**
 * Code attribute
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {anyField, AnyField} from '@unidata/core';
import {observable} from 'mobx';
import {DataSimpleValuedAttribute} from './DataSimpleValuedAttribute';

export class DataCodeAttribute extends DataSimpleValuedAttribute {
    @observable
    @anyField()
    public supplementary: AnyField;
}
