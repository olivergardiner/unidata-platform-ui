/**
 * Abstract attribute
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {AbstractModel, EnumField, stringField, StringField} from '@unidata/core';
import {observable} from 'mobx';
import {SIMPLE_DATA_TYPE} from '@unidata/types';

export class DataAbstractAttribute extends AbstractModel {
    @observable
    @stringField()
    public name: StringField;
}
