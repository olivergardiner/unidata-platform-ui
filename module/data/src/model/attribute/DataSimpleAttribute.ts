/**
 * Simple Attribute
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {anyField, AnyField, StringField, stringField} from '@unidata/core';
import {observable} from 'mobx';
import {IDataAttributeFormatter} from '../../formatter/type/IDataAttributeFormatter';
import {DataValuedAttribute} from './DataValuedAttribute';
import {DataSimpleValuedAttribute} from './DataSimpleValuedAttribute';

export class DataSimpleAttribute extends DataSimpleValuedAttribute {
    @observable
    @stringField({allowNull: true})
    public displayValue: StringField;

    @observable
    @stringField()
    public targetEtalonId: StringField;

    @observable
    @stringField()
    public valueId: StringField;

    @observable
    @stringField()
    public unitId: StringField;


    public getFormattedValue (formatter: IDataAttributeFormatter) {
        const attribute = this as DataSimpleAttribute;
        const displayValue = attribute.displayValue.getValue() as string | null;
        const value = attribute.value.getValue();

        if (displayValue === null) {
            return formatter.format(value, this.type.getValue());
        }

        return formatter.format(value, this.type.getValue(), displayValue);
    }

}
