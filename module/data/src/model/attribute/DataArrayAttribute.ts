/**
 * Array attribute
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {hasMany, IAssociationDescriptorMap, ModelCollection, ModelMetaData} from '@unidata/core';
import {observable} from 'mobx';
import {DataArrayAttributeItem} from './DataArrayAttributeItem';
import {IDataArrayAttributeFormatter} from '../../formatter/type/IDataArrayAttributeFormatter';
import {DataValuedAttribute} from './DataValuedAttribute';

export class DataArrayAttribute  extends DataValuedAttribute {
    @observable
    @hasMany()
    public value: ModelCollection<DataArrayAttributeItem>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                value: DataArrayAttributeItem
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }

    public getFormattedValue (formatter: IDataArrayAttributeFormatter): string [] {
        const attribute = this as DataArrayAttribute;

        let values;
        let displayValues;

        if (attribute.value) {
            values = attribute.value
                .getRange()
                .map((attr) => attr.value.getValue());

            displayValues = attribute.value
                .getRange()
                .map((attr) => attr.displayValue.getValue())
                .filter((v) => v !== null && v !== undefined);

            return formatter.format(values, this.type.getValue(), displayValues);
        }

        return [];
    }

}
