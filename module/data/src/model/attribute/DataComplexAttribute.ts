/**
 * Complex Attribute
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {DataAbstractAttribute} from './DataAbstractAttribute';
import {hasMany, IAssociationDescriptorMap, ModelCollection, ModelMetaData} from '@unidata/core';
import {observable} from 'mobx';
import {NestedRecord} from '../NestedRecord';

export class DataComplexAttribute extends DataAbstractAttribute {
    @observable
    @hasMany()
    public nestedRecords: ModelCollection<NestedRecord>;

    protected initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasMany: {
                nestedRecords: NestedRecord
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
