/**
 *  Model DQ Error
 *
 * @author Sergey Shishigin
 * @date 2019-10-25
 */
import {AbstractModel, stringField, StringField} from '@unidata/core';
import {observable} from 'mobx';

export class DqError extends AbstractModel {
    @observable
    @stringField()
    public severity: StringField;

    @observable
    @stringField()
    public category: StringField;

    @observable
    @stringField()
    public message: StringField;

    @observable
    @stringField()
    public ruleName: StringField;

    @observable
    @stringField({allowNull: true})
    public phase: StringField;
}
