/**
 * Model of the draft data.
 *
 * After adding draft indexing, you will need to redo this model (add preview attributes to the entity that the draft is used for)
 *
 * @author Brauer Ilya
 * @date 2020-05-12
 */

import {AbstractModel, dateField, DateField, stringField, StringField} from '@unidata/core';

export class DraftData extends AbstractModel {
    @stringField({primaryKey: true})
    public id: StringField;

    @stringField()
    public entityId: StringField;

    @stringField()
    public entityName: StringField;

    @stringField()
    public owner: StringField;

    @stringField()
    public displayName: StringField;

    @dateField()
    public updateDate: DateField;

    @dateField()
    public createDate: DateField;
}
