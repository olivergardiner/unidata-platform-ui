/**
 * Search service for the Id Search component
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-19
 */

import {IdSearchResult} from '../model/idsearch/IdSearchResult';
import {EtalonIdSearchOp} from './operation/idsearch/EtalonIdSearchOp';
import {OriginIdSearchOp} from './operation/idsearch/OriginIdSearchOp';
import {ExternalIdSearchOp} from './operation/idsearch/ExternalIdSearchOp';

export class DataRecordIdSearchService {

    public static etalonIdSearch (etalonId: string): Promise<IdSearchResult | null> {
        const op = new EtalonIdSearchOp(etalonId);

        return op.execute();
    }

    public static originIdSearch (originId: string): Promise<IdSearchResult | null> {
        const op = new OriginIdSearchOp(originId);

        return op.execute();
    }

    public static externalIdSearch (
        externalId: string,
        entityName: string,
        sourceSystem: string
    ): Promise<IdSearchResult | null> {
        const op = new ExternalIdSearchOp(externalId, entityName, sourceSystem);

        return op.execute();
    }

}
