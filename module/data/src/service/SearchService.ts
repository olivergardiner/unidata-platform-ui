/**
 * Record search service
 *
 * @author: Ilya Brauer
 * @date: 2019-10-08
 */

import {IResponseOp, ISearchRequest} from './operation/search/ReadDataList/ReadDataListType';
import {ReadDataListOp} from './operation/search/ReadDataList/ReadDataListOp';

export class SearchService {

    /** Getting the full list of entries */
    public static getSearchResult (searchQuery: ISearchRequest): Promise<IResponseOp> {
        const op = new ReadDataListOp(searchQuery);

        return op.execute();
    }
}
