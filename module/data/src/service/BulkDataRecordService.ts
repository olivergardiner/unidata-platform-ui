/**
 * Batch operations service
 *
 * @author Ivan Marshalkin
 * @date 2020-03-10
 */

import {BulkOperationType} from '../uemodule/bulkwizard/type/BulkOperationType';
import {IEtalonIds} from '../uemodule/bulkwizard/type/EtalonIds';
import {ExecuteBulkOperationOp, ExecuteBulkOperationQueryType} from './operation/bulkdatarecord/ExecuteBulkOperationOp';
import {
    BulkReadDataOperationsOp,
    IBulkDataRecordOperation,
    IReadBulkOperationsQuery
} from './operation/bulkdatarecord/BulkReadDataOperationsOp';

export class BulkDataRecordService {
    /**
     * Starts a batch operation
     *
     * @param operationType - type of operation to run
     * @ * @param entity Name-name of the registry / directory
     * @param ids-array of up-to-date periods
     * @param query - the search query
     * @param query Count - the number of entries in the search query
     * @param cfg-additional parameters to run
     */
    public static bulkOperation (
        operationType: BulkOperationType,
        entityName: string,
        ids: IEtalonIds[],
        query: ExecuteBulkOperationQueryType,
        queryCount: number,
        cfg: any
    ): Promise<null> {
        let op = new ExecuteBulkOperationOp(operationType, entityName, ids, query, queryCount, cfg);

        return op.execute();
    }

    /**
     * Returns a list of available batch operations
     *
     * @param query
     */
    public static getBulkDataRecordOperations (query: IReadBulkOperationsQuery): Promise<IBulkDataRecordOperation[]> {
        let op = new BulkReadDataOperationsOp(query);

        return op.execute();
    }
}
