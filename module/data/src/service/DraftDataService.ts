/**
 *  Service for drafts of registers/reference books
 *
 *
 * @author Brauer Ilya
 * @date 2020-05-12
 */
import {ReadDraftListOp, IDraftListRequest} from './operation/draft/ReadDraftListOp';


export class DraftDataService {
    /**
     * Getting a list of drafts
     */
    public static getDraftList (params: IDraftListRequest) {
        let op: ReadDraftListOp = new ReadDraftListOp(params);

        return op.execute();
    }
}
