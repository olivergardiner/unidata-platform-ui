/**
 * Favorite records service
 *
 * @author: Ilya Brauer
 * @date: 2019-10-08
 */

import {CreateFavoriteOp} from './operation/favorite/CreateFavoriteOp';
import {DeleteFavoriteOp} from './operation/favorite/DeleteFavoriteOp';
import {ReadFavoriteOp} from './operation/favorite/ReadFavoriteOp';
import {FavoriteEtalonsMap} from '../store/FavoriteStore';

export class FavoriteService {

    /** Getting a list of favorites */
    public static readEtalonsMap (): Promise<FavoriteEtalonsMap> {
        const op = new ReadFavoriteOp();

        return op.execute();
    }

    /** Adding to favorites */
    public static addToFavorite (entityName: string, etalonIds: string[]): Promise<any> {
        const op = new CreateFavoriteOp(entityName, etalonIds);

        return op.execute();
    }

    /** Deleting from favorites */
    public static deleteFromFavorite (etalonId: string): Promise<any> {
        const op = new DeleteFavoriteOp(etalonId);

        return op.execute();
    }
}
