/**
 *  A service for all types of relationships (see the api-docs relation_entities)
 *
 *
 * @author Sergey Shishigin
 * @date 2019-10-25
 */
import {ReadDataRelationBulkOp} from './operation/datarelation/ReadRelationBulkOp';
import {RelationManyToMany} from '../model/relation/RelationManyToMany';
import {Nullable} from '@unidata/core';
import {RelationDirectionType} from '@unidata/types';
import {IRelationQuery, IRelationResponseOp} from './operation/search/ReadRelationList/ReadRelationListType';
import {ReadRelationListOp} from './operation/search/ReadRelationList/ReadRelationListOp';

export class DataRelationService {
    /**
     * Loading m2m and reference links
     */
    public static getRelationBulk (
        dataRecordEtalonId: string,
        relName: string,
        dateFrom: Nullable<Date|string>,
        dateTo: Nullable<Date|string>,
        direction?: RelationDirectionType): Promise<RelationManyToMany[]> {
        let op: ReadDataRelationBulkOp = new ReadDataRelationBulkOp(dataRecordEtalonId, relName, dateFrom, dateTo, direction);

        return op.execute();
    }

    /** Getting links **/
    public static getResultRelation (relation: IRelationQuery): Promise<IRelationResponseOp> {
        const op = new ReadRelationListOp(relation);

        return op.execute();
    }
}
