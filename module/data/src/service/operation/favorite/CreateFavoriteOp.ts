/**
 * Operation for adding an entry to favorites
 *
 * @author Ilya Brauer
 * @date 2019-10-15
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class CreateFavoriteOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/ee/favorite-etalons',
        method: 'post'
    };

    constructor (entityName: string, etalonIds: string[]) {
        super();

        this.config.data = {
            favorites: {
                [entityName]: etalonIds
            }
        };
    }
}
