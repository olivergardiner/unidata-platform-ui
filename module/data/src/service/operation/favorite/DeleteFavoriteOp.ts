/**
 * Operation to delete an entry from favorites
 *
 * @author Ilya Brauer
 * @date 2019-10-15
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class DeleteFavoriteOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/ee/favorite-etalons',
        method: 'delete'
    };

    constructor (etalonId: string) {
        super();

        this.config.url = `${this.config.url}/${etalonId}`;
    }
}
