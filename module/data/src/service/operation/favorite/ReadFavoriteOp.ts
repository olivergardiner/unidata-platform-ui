/**
 * The operation to retrieve selected records
 *
 * @author Ilya Brauer
 * @date 2020-03-02
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ReadFavoriteOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/data/favorite-etalons/etalons-map?size=10000&page=1&start=0&limit=25',
        method: 'get'
    };
}
