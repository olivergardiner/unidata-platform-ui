/**
 * The search operation for etalonId
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-27
 */
import {AppModelHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {IdSearchResult} from '../../../model/idsearch/IdSearchResult';

export class EtalonIdSearchOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        url: '/internal/data/entities/keys/etalon/{{etalonId}}',
        rootProperty: 'content',
        model: IdSearchResult,
        method: 'get'
    };

    constructor (etalonId: string) {
        super();

        this.urlContext = {
            etalonId: etalonId
        };
    }
}
