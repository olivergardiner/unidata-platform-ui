/**
 * Search operation by origin Id
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-27
 */
import {AppModelHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {IdSearchResult} from '../../../model/idsearch/IdSearchResult';

export class OriginIdSearchOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        url: '/internal/data/entities/keys/origin/{{originId}}',
        rootProperty: 'content',
        model: IdSearchResult,
        method: 'get'
    };

    constructor (originId: string) {
        super();

        this.urlContext = {
            originId: originId
        };
    }
}
