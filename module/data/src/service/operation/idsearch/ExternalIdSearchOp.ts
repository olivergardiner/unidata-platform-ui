/**
 * Search operation by externalId
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-27
 */
import {AppModelHttpOp} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {IdSearchResult} from '../../../model/idsearch/IdSearchResult';

export class ExternalIdSearchOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        url: '/internal/data/entities/keys/external/{{externalId}}/{{sourceSystem}}/{{entityName}}',
        rootProperty: 'content',
        model: IdSearchResult,
        method: 'get'
    };

    constructor (externalId: string, entityName: string, sourceSystem: string) {
        super();

        this.urlContext = {
            externalId: externalId,
            entityName: entityName,
            sourceSystem: sourceSystem
        };
    }
}
