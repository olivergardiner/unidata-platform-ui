/**
 * Operation for getting a list of drafts
 *
 * @author Ilya Brauer
 * @date 2020-05-12
 */
import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {DraftData} from '../../../model/DraftData';

export interface IDraftListRequest {
    entityName: string;
    start?: number;
    limit?: number;
    groupByEtalon?: boolean;
    etalons?: string[];
}

export interface IDraftListResponse {
    [key: string]: DraftData[];
}

export class ReadDraftListOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/data/draft/entities/search?start={{start}}&limit={{limit}}',
        method: 'post',
        rootProperty: 'content',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    constructor (params: IDraftListRequest) {
        super();

        this.config.data = {
            entityName: params.entityName,
            groupByEtalon: params.groupByEtalon === false ? false : true,
            etalons: params.etalons
        };

        this.urlContext = {
            start: params.start !== undefined ? params.start : 0,
            limit: params.limit !== undefined ? params.limit : 100
        };
    }

    protected processOperationData (data: any) {
        if (data[this.config.data.entityName] !== undefined) {
            return Object.entries(data[this.config.data.entityName]).reduce<IDraftListResponse>((result, item) => {
                const [key, value] = item;

                if (value && Array.isArray(value)) {
                    result[key] = value.map((draftData: object) => new DraftData(draftData));
                }

                return result;
            }, {});
        }

        return {};
    }
}
