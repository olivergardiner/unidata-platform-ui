/**
 *  Link reading operation
 *
 * @author Sergey Shishigin
 * @date 2019-10-25
 */
import {AppModelListHttpOp, DateTimeUtil, TypeChecker} from '@unidata/core-app';
import {IModelOpConfig, Nullable} from '@unidata/core';
import {RelationManyToMany} from '../../../index';
import {RelationDirectionType} from '@unidata/types';

export class ReadDataRelationBulkOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/internal/data/relations/relation_bulk/{{id}}/{{relName}}/{{dateFrom}}/{{dateTo}}',
        rootProperty: 'content',
        model: RelationManyToMany,
        method: 'get'
    };

    constructor (
        etalonId: string,
        relName: string,
        dateFrom: Nullable<Date|string>,
        dateTo: Nullable<Date|string>,
        direction?: RelationDirectionType) {
        super();

        let dateFromStr = 'null',
            dateToStr = 'null';

        if (!direction) {
            direction = RelationDirectionType.to;
        }

        if (dateFrom) {
            if (TypeChecker.isDate(dateFrom)) {
                dateFromStr = DateTimeUtil.formatToServer(dateFrom as Date);
            } else {
                dateFromStr = dateFrom as string;
            }
        }

        if (dateTo) {
            if (TypeChecker.isDate(dateTo)) {
                dateToStr = DateTimeUtil.formatToServer(dateTo as Date);
            } else {
                dateToStr = dateTo as string;
            }
        }

        this.urlContext = {
            id: etalonId,
            relName: relName,
            dateFrom: dateFromStr,
            dateTo: dateToStr
        };

        this.config.params = {
            direction: direction
        };
    }
}
