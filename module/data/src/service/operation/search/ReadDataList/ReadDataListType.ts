/**
 * Operation for getting a list of records
 *
 * @author Ilya Brauer
 * @date 2019-10-08
 */

import {SearchHit} from '@unidata/core-app';
import {StringField} from '@unidata/core';
import {ARRAY_DATA_TYPE, CODE_DATA_TYPE, SIMPLE_DATA_TYPE} from '@unidata/types';
import {IFormField, ISupplementaryRequest, FACETS} from '@unidata/meta';

export enum ORDER {
    ASC = 'ASC',
    DESC = 'DESC'
}

export interface ISortField {
    field: string;
    order: ORDER;
    type: SIMPLE_DATA_TYPE | CODE_DATA_TYPE | ARRAY_DATA_TYPE;
}

export type SearchTermsRequest = {
    asOf?: string;
    returnFields: string[];
    searchFields: string[];
    formFields?: IFormField[];
    countOnly: boolean;
    fetchAll: boolean;
    facets?: FACETS[];
    text?: string;
    qtype?: 'FUZZY';
    supplementaryRequests?: ISupplementaryRequest[];
}

export type ISearchRequest = SearchTermsRequest & {
    entity: string;
    sortFields: ISortField[];
    page: number;
    start: number;
    count: number;
}

export interface IExtendedValue {
    displayValue: StringField;
    linkedEtalonId: StringField;
}

/**
 * Interface for recording search results
 */
export interface ISearchPreviewItem {
    field: string;
    value: string;
    values: Array<string | number | boolean | null>;
    extendedValues: IExtendedValue[];
}

/**
 * Search result interface
 */
export interface ISearchHit {
    modelId: number;
    source: any[];
    preview: ISearchPreviewItem[];
    status: string;
}

export interface IResponseOp {
    searchRecords: SearchHit[];
    totalCount: number;
}
