/**
 * Operation for getting a list of records§
 *
 * @author Ilya Brauer
 * @date 2019-10-08
 */

import {AppModelListHttpOp, SearchHit} from '@unidata/core-app';
import {IModelOpConfig} from '@unidata/core';
import {IResponseOp, ISearchRequest} from './ReadDataListType';

export class ReadDataListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/search',
        method: 'post',
        model: SearchHit,
        rootProperty: '',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    constructor (cfg: ISearchRequest) {
        super();

        this.config.data = cfg;
    }

    protected processOperationData (data: any): IResponseOp {
        const searchRecords: SearchHit[] = data.hits.map((item: any) => {
            return new SearchHit(item);
        });

        return {
            searchRecords,
            totalCount: data.total_count
        };
    }
}
