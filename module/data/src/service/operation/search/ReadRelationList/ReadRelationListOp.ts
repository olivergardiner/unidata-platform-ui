/**
 Loading links for search

 @author Vladimir Stebunov
 @date 2019-10-29
 */

import {SearchRelation} from '../../../../model/relation/SearchRelation';
import {AppModelListHttpOp} from '@unidata/core-app';
import {IModelOpConfig, Sequence} from '@unidata/core';
import {IRelationResponseOp, ISearchRelation} from './ReadRelationListType';

export class ReadRelationListOp extends AppModelListHttpOp {
    protected config: IModelOpConfig = {
        url: '/data/relations/search',
        method: 'post',
        model: SearchRelation,
        rootProperty: '',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    constructor (cfg: any) {
        super();

        this.config.data = cfg;
    }

    protected processOperationData (response: any): IRelationResponseOp {
        const relations: ISearchRelation[] = response.content.data.map((item: any) => {
            return {
                ...item,
                modelId: Sequence.getId()
            };
        });

        return {
            relations
        };
    }
}
