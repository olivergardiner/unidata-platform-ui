/**
 Loading links for search

 @author Vladimir Stebunov
 @date 2019-10-29
 */

export interface IEtalonRecord {
    etalonId: string;
    from: string | null;
    to: string | null;
}

/**
 * Interface for requesting links
 */
export interface IRelationQuery {
    entity: string;
    maxCount: number;
    relationNames: string[];
    fromEtalonIds: IEtalonRecord[];
}

export interface ISearchRelationItem {
    etalonDisplayNameTo: string;
    etalonIdFrom: string;
    etalonIdTo: string;
    relName: string;
    validFrom: string;
    validTo: string;
}

export interface ISearchRelation {
    data: ISearchRelationItem[];
    etalonId: string;
    relName: string;
    total: number;
    validFrom: string;
    validTo: string;
    modelId: number;
}

export interface IRelationResponseOp {
    relations: ISearchRelation[];
}
