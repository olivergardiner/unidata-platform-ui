/**
 * Launch bulk operation
 *
 * @author Ivan Marshalkin
 * @date 2020-03-11
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {BulkOperationType} from '../../../uemodule/bulkwizard/type/BulkOperationType';
import {IEtalonIds} from '../../../uemodule/bulkwizard/type/EtalonIds';
import {SearchTermsRequest} from '../search/ReadDataList/ReadDataListType';

export type ExecuteBulkOperationQueryType = SearchTermsRequest & {entity: string};

export class ExecuteBulkOperationOp extends AppHttpOp {
    protected config: IOpConfig = {
        method: 'post',
        url: 'bulk/data/bulk/run',
        successProperty: ''
    };

    constructor (
        operationType: BulkOperationType,
        entityName: string,
        ids: IEtalonIds[],
        query: ExecuteBulkOperationQueryType,
        queryCount: number,
        cfg: any
    ) {
        super();

        let data: any = {
            '@type': operationType,
            entityName: entityName
        };

        if (ids.length) {
            data['selectedByIds'] = ids;
        } else {
            data['selectedByIds'] = null;
            data['selectedByRequest'] = query;
            // UN-2513
            data['selectedByRequest']['count'] = queryCount;
        }

        Object.assign(data, cfg);

        this.config.data = data;
    }
}
