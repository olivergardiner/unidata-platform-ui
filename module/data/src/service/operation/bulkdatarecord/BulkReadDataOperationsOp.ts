/**
 * Operation to get a list of available operations on records
 *
 * @author Ivan Marshalkin
 * @date 2020-03-10
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {BulkOperationType} from '../../../uemodule/bulkwizard/type/BulkOperationType';
import {SearchTermsRequest} from '../search/ReadDataList/ReadDataListType';

export interface IBulkDataRecordOperation {
    description: string;
    allowed: boolean;
    type: BulkOperationType;
}

export interface IReadBulkOperationsQuery {
    searchComplexRO: SearchTermsRequest & {entity: string};
    customOperations: null | any;
}

export class BulkReadDataOperationsOp extends AppHttpOp {
    protected config: IOpConfig = {
        method: 'post',
        url: 'bulk/data/bulk'
    };

    constructor (query: IReadBulkOperationsQuery) {
        super();

        this.config.data = query;
    }
}
