/**
 * The data import operation
 *
 * @author: Brauer Ilya
 * @date: 2020-03-12
 */

import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';
import {UploadFile} from '@unidata/uikit';

export class ImportDataOp extends AppHttpOp {
    protected config: IOpConfig = {
        url: '/internal/import/data/xlsx',
        rootProperty: '',
        successProperty: '',
        method: 'post'
    };

    constructor (importParams: string, file: UploadFile) {
        super();

        const blob = new Blob([importParams], {type: 'application/json'});
        const data = new FormData();

        data.append('file', file as any);
        data.append('importParams', blob);
        this.config.data = data;
    }
}
