/**
 * The read operation date of record
 *
 * @author Sergey Shishigin
 * @date 2019-10-24
 */
import {AppModelHttpOp, ServerUrlManager} from '@unidata/core-app';
import {IReadDataRecordOpOptions} from '../../../types/IReadDataRecordOpOptions';
import {IModelOpConfig} from '@unidata/core';
import {DataRecord} from '../../../model';

export class ReadDataRecordOp extends AppModelHttpOp {
    protected config: IModelOpConfig = {
        method: 'get',
        model: DataRecord
    };

    private etalonId: string;
    private date: Date;
    private options: IReadDataRecordOpOptions;

    constructor (etalonId: string, date?: Date, options?: IReadDataRecordOpOptions) {
        super();
        this.etalonId = etalonId;

        if (date) {
            this.date = date;
        }

        this.options = options || {};

        this.config.params = options;
    }

    protected buildUrl (): string {
        let url: string = `/data/entities/${this.etalonId}`;

        // date not supported yet. correct formatting is required

        return ServerUrlManager.buildUrl(url);
    }
}
