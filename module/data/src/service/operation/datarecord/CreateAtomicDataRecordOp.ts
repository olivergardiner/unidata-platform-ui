/**
 * DataRecord creation operation
 *
 * @author Denis Makarov
 * @date 2020-05-07
 */

import {IModelOpConfig, IStringKeyMap, ModelHttpOp} from '@unidata/core';
import {ServerUrlManager} from '@unidata/core-app';
import {DataRecord} from '../../../model';

export class CreateAtomicDataRecordOp extends ModelHttpOp {
    protected config: IModelOpConfig = {
        rootProperty: 'content',
        model: DataRecord,
        successProperty: 'success',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    constructor (atomicRecordData: IStringKeyMap) {
        super();

        this.config.data = atomicRecordData;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/data/entities/atomic`);
    }
}
