/**
 * Write reindex operation
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-20
 */
import {AppHttpOp} from '@unidata/core-app';
import {IOpConfig} from '@unidata/core';

export class ReindexDataRecordOp extends AppHttpOp {

    protected config: IOpConfig = {
        url: '/internal/data/entities/reindex-etalon/{{etalonId}}',
        rootProperty: 'content',
        method: 'get'
    };

    constructor (etalonId: string) {
        super();

        this.urlContext = {
            etalonId: etalonId
        };
    }

}
