/**
 * DataRecord update operation
 *
 * @author Denis Makarov
 * @date 2020-05-07
 */

import {IModelOpConfig, IStringKeyMap, ModelHttpOp} from '@unidata/core';
import {ServerUrlManager} from '@unidata/core-app';
import {DataRecord} from '../../../model';

export class UpdateAtomicDataRecordOp extends ModelHttpOp {
    protected config: IModelOpConfig = {
        rootProperty: 'content',
        successProperty: 'success',
        model: DataRecord,
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    private etalonId: string;

    constructor (etalonId: string, atomicRecordData: IStringKeyMap) {
        super();

        this.etalonId = etalonId;
        this.config.data = atomicRecordData;
    }

    protected buildUrl (): string {
        return ServerUrlManager.buildUrl(`/data/entities/atomic/${this.etalonId}`);
    }
}
