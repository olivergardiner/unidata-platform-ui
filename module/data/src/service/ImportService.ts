/**
 * Data import service
 *
 * @author Brauer Ilya
 * @date 2020-03-12
 */

import {UploadFile} from '@unidata/uikit';
import {ImportDataOp} from './operation/import/ImportDataOp';
import {DownloadFileService} from '@unidata/core-app';

export class ImportService {
    public static importData (importParams: string, file: UploadFile) {
        let op = new ImportDataOp(importParams, file);

        return op.execute();
    }

    public static downloadTemplate (entityName: string) {
        const url = `/data/export/data/template/${entityName}`;

        DownloadFileService.byUrl(url);
    }
}
