/**
 * Data Record service
 *
 * @author Sergey Shishigin
 * @date 2019-10-24
 */
import {ReadDataRecordOp} from './operation/datarecord/ReadDataRecordOp';
import {IReadDataRecordOpOptions} from '../types/IReadDataRecordOpOptions';
import {DataRecord} from '../model/DataRecord';
import {ReindexDataRecordOp} from './operation/datarecord/ReindexDataRecordOp';
import {SerializedType} from '../../../core/src/data/model/type/SerializedType';
import {AtomicRecord} from '../model/AtomicRecord';
import {CreateAtomicDataRecordOp} from './operation/datarecord/CreateAtomicDataRecordOp';
import {UpdateAtomicDataRecordOp} from './operation/datarecord/UpdateAtomicDataRecordOp';

export class DataRecordService {

    public static getDataRecord (etalonId: string, date?: Date, options?: IReadDataRecordOpOptions): Promise<DataRecord> {
        let op = new ReadDataRecordOp(etalonId);

        return op.execute();
    }

    public static reindexDataRecord (etalonId: string): Promise<boolean> {
        let op = new ReindexDataRecordOp(etalonId);

        return op.execute();
    }

    public static saveDataRecord (etalonId: string, atomicRecord: AtomicRecord, create: boolean) {
        let op;

        const atomicRecordData = atomicRecord.serialize();

        if (create) {
            op = new CreateAtomicDataRecordOp(atomicRecordData);
        } else {
            op = new UpdateAtomicDataRecordOp(etalonId, atomicRecordData);
        }

        return op.execute();
    }

}
