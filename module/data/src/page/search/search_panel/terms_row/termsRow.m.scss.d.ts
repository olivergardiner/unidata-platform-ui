export const buttonContainer: string;

export const sep: string;

export const countBadge: string;

export const tagsRow: string;

export const tagsContainer: string;

export const isPin: string;

export const pinContainer: string;

export const tagListIsOpen: string;

export const tagsList: string;

export const tagsListOverflow: string;

export const open: string;

export const searchSaveContainer: string;
