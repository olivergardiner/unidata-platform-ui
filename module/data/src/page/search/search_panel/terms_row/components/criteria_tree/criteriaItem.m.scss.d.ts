
export const dropDownContainer: string;
export const ddGroupName: string;
export const ddGroupHover: string;
export const ddItem: string;
export const disabled: string;
export const checkIcon: string;
export const popoverContainer: string;
