/**
 * A search term of the Relation type, for searching related records
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {EntityType} from '@unidata/types';
import {EntityMeta} from '../../../../store/MetaRecordStore/MetaRecordStoreTypes';
import * as ddStyles from '../criteria_tree/criteriaItem.m.scss';
import {RelationModal} from './RelationModal';
import * as styles from './relation.m.scss';
import {IComponentPropsByUEType, UeModuleTypeComponent} from '@unidata/core';
import {i18n} from '@unidata/core-app';
import {SearchTerm} from '../../../../../../component/search_term/SearchTerm';
import {CriteriaItem} from '../../../../../../component/criteria_item/CriteriaItem';
import {RelationST, AbstractSearchTerm} from '@unidata/meta';
import {inject, observer} from 'mobx-react';

type IProps = IComponentPropsByUEType[UeModuleTypeComponent.RENDER_SEARCH_TERM] & {
    searchTerm: RelationST;
}

interface IState {
    isShowModal: boolean;
    relationFilters: AbstractSearchTerm[];
}

@inject('searchPanelStore')
@observer
export class Relation extends React.Component<IProps, IState> {

    state: IState = {
        isShowModal: false,
        relationFilters: []
    };

    toggleModal = () => {
        this.setState((prevState) => {
            return {isShowModal: !prevState.isShowModal};
        });
    };

    onChoose = (keys: string[], filters: AbstractSearchTerm[]) => {
        this.props.searchTerm.setRelationIds(keys);

        this.setState({
            relationFilters: filters
        });

        this.setState({isShowModal: false});
    };

    addTerm = (term: AbstractSearchTerm) => () => {
        term.setParentId(this.props.searchTerm.id);
        this.props.searchTerm.addItem(term);

        if (this.props.onAddTerm) {
            this.props.onAddTerm();
        }
    };

    deleteItem = (index: number) => () => {
        this.props.searchTerm.deleteItem(index);
    };

    render () {
        const searchTerm: RelationST = this.props.searchTerm;
        const relationCount = (searchTerm.relationIds || []).length;
        const label = searchTerm.displayName;
        const value = i18n.t('module.data>search.panel>relationCount', {count: relationCount});

        const termProps = {
            value,
            label
        };

        const entity: EntityMeta = {
            entityName: searchTerm.entity,
            entityType: EntityType.Entity,
            entityDisplayName: searchTerm.displayName
        };

        const valueKeys = searchTerm.itemsData.map((item) => item.key);

        return (
            <React.Fragment>
                <SearchTerm.Group
                    searchTerm={searchTerm}
                    searchTermList={this.props.searchTerm.itemsData}
                    onDelete={this.props.onDelete}
                    termProps={termProps}
                    isVisible={!this.state.isShowModal}
                >
                    <div className={ddStyles.ddItem} onClick={this.toggleModal}>
                        {i18n.t('module.data>search.panel>chooseRelationRecords')}
                    </div>
                    {searchTerm.attributes.length > 0 && (
                        <React.Fragment>
                            <div className={styles.attributesLabel}>{i18n.t('module.data>search.panel>attributes')}:</div>
                            {searchTerm.attributes.map((attr: AbstractSearchTerm) => {
                                const isChecked = valueKeys.includes(attr.key);

                                return (
                                    <CriteriaItem
                                        key={attr.key}
                                        title={attr.displayName}
                                        onClick={this.addTerm(attr)}
                                        isChecked={isChecked}
                                        data-qaid={attr.key}
                                    />
                                );
                            })}
                        </React.Fragment>
                    )}
                </SearchTerm.Group>
                {this.state.isShowModal === true && (
                    <RelationModal
                        filters={this.state.relationFilters}
                        ids={this.props.searchTerm.relationIds}
                        entity={entity}
                        onClose={this.toggleModal}
                        onChoose={this.onChoose}
                    />
                )}
                {searchTerm.itemsData.map((item, index) => {
                    return (
                        <SearchTerm.Group
                            key={item.key}
                            searchTerm={item}
                            searchTermList={searchTerm.itemsData}
                            onDelete={this.deleteItem(index)}
                        />
                    );
                })}
            </React.Fragment>
        );
    }
}
