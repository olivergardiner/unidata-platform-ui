import * as React from 'react';
import {TermGroup} from './TermGroup';
import {CriteriaItem} from '../../../../../../component/criteria_item/CriteriaItem';
import {GroupSearchTerm} from '../../../../store/search_panel_store/attributeTerms';
import {AbstractSearchTerm} from '@unidata/meta';

interface IProps {
    fields: GroupSearchTerm[];
    searchTerms: AbstractSearchTerm[]; // All selected attributes
    onAddTerm: (searchTerm: AbstractSearchTerm) => () => void;
    isMulti?: boolean; // multiple selection is possible (only for ISearchTerm fields)
}

export class Attributes extends React.Component<IProps> {
    render () {

        return this.props.fields.map((itemField) => {
            const childrenFields = itemField.children;

            if (childrenFields !== undefined && childrenFields.length > 0) {
                return (
                    <TermGroup
                        key={`group_${itemField.key}`}
                        popoverContent={(
                            <div
                                data-qaid={`popover_${itemField.key}`}
                            >
                                <Attributes
                                    fields={childrenFields}
                                    searchTerms={this.props.searchTerms}
                                    onAddTerm={this.props.onAddTerm}
                                />
                            </div>
                        )}
                        fieldsLength={childrenFields.length}
                        isVisible={true}
                        label={itemField.displayName}
                        data-qaid={`menu_${itemField.key}`}
                    />
                );
            }

            let isChecked;
            let counter;

            if (this.props.isMulti === true) {
                counter = this.props.searchTerms.filter((st) => st.key === itemField.key).length;
            } else {
                isChecked = this.props.searchTerms.find((st) => st.key === itemField.key);
            }

            return (
                <CriteriaItem
                    key={itemField.key}
                    title={itemField.displayName}
                    onClick={this.props.onAddTerm(itemField)}
                    isChecked={isChecked !== undefined}
                    counter={counter}
                    data-qaid={`item_${itemField.key}`}
                />
            );
        });
    }
}
