import * as React from 'react';
import {Popover, PLACEMENT, TRIGGER} from '@unidata/uikit';
import * as styles from '../criteria_tree/criteriaItem.m.scss';
import {CriteriaItem} from '../../../../../../component/criteria_item/CriteriaItem';

interface IProps {
    label: string;
    isVisible: boolean;
    popoverContent: React.ReactNode;
    fieldsLength: number;
    'data-qaid': string;
}

export class TermGroup extends React.Component<IProps, any> {
    render () {
        const udProps: any = {
            placement: PLACEMENT.RIGHT,
            trigger: TRIGGER.CLICK,
            hasPaddings: false,
            target: (
                <CriteriaItem
                    title={`${this.props.label} (${this.props.fieldsLength})`}
                    onClick={() => null}
                    isHeader={true}
                    hasChildren={this.props.fieldsLength !== 0}
                    data-qaid={this.props['data-qaid']}
                />
            )
        };

        if (this.props.isVisible === false) {
            udProps.visible = false;
        }

        return (
            <Popover
                {...udProps}
            >
                {this.props.popoverContent}
            </Popover>
        );
    }
}
