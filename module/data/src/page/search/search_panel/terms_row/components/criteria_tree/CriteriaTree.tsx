/**
 * A list of criteria by which you can search
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import * as styles from './criteriaItem.m.scss';
import {i18n} from '@unidata/core-app';
import {MetaRecordStore} from '../../../../store/MetaRecordStore/MetaRecordStore';
import {getAttributeTerms} from '../../../../store/search_panel_store/attributeTerms';
import {Attributes} from '../add_term/Attributes';
import {TermGroup} from '../add_term/TermGroup';

import {ueModuleManager, UeModuleReactComponent, UeModuleTypeComponent} from '@unidata/core';
import {observer} from 'mobx-react';
import {getSystemTerms} from '../../../../store/search_panel_store/systemTerms';

import {Button, DropDown, Input, INTENT, PLACEMENT, TRIGGER} from '@unidata/uikit';
import {getRelationTerms} from '../../../../store/search_panel_store/relationTerms';
import {SearchPanelStore} from '../../../../store/search_panel_store/SearchPanelStore';
import {FavoriteStore} from '../../../../../../store/FavoriteStore';
import {CriteriaItem} from '../../../../../../component/criteria_item/CriteriaItem';
import {AbstractSearchTerm, SEARCH_TERM_GROUP, SourceSystemService} from '@unidata/meta';

export function getGroupName (type: string) {
    switch (type) {
        case SEARCH_TERM_GROUP.ATTRIBUTE: {
            return i18n.t('module.data>search.panel>attributes');
        }
        case SEARCH_TERM_GROUP.SYSTEM: {
            return i18n.t('module.data>search.panel>system');
        }
        case SEARCH_TERM_GROUP.QUALITY: {
            return i18n.t('module.data>search.panel>quality');
        }
        case SEARCH_TERM_GROUP.RELATIONS: {
            return i18n.t('module.data>search.panel>relations');
        }
        case SEARCH_TERM_GROUP.CLASSIFY: {
            return i18n.t('module.data>search.panel>clsf');
        }
        default: {
            return '';
        }
    }
}

interface IProps {
    onAddTerm: (searchTerm: AbstractSearchTerm) => void;
    onFavoriteSearch: (favoriteEtalonsMap: string[]) => void;
    metaRecordStore: MetaRecordStore;
    searchPanelStore: SearchPanelStore;
    favoriteStore: FavoriteStore;
}

interface IState {
    isVisible: boolean;
    searchText: string;
}

@observer
export class CriteriaTree extends React.Component<IProps, IState> {
    state = {
        isVisible: false,
        searchText: ''
    };

    onVisibleChange = (isVisible: boolean) => {
        this.setState({isVisible});
    };

    addTerm = (searchTerm: AbstractSearchTerm) => () => {
        this.props.onAddTerm(searchTerm);
    };

    onSearch = (e: React.SyntheticEvent<HTMLInputElement>) => {
        const searchText = e.currentTarget.value;

        this.setState({searchText});
    };

    renderListOfCriteria = () => {
        const ueListOfCriteria = ueModuleManager.getModulesByType(UeModuleTypeComponent.RENDER_CRITERION);

        const fields = getAttributeTerms(this.props.metaRecordStore)
            .filter((field) => field.displayName.toLowerCase().indexOf(this.state.searchText.toLowerCase()) > -1);

        const attributes = [
            (<TermGroup
                key={'searchCriteriaAttributes'}
                popoverContent={(
                    <div data-qaid={'popover_attributes'}>
                        <Attributes
                            onAddTerm={this.addTerm}
                            fields={fields as any}
                            searchTerms={this.props.searchPanelStore.listST}
                        />
                    </div>
                )}
                fieldsLength={fields.length}
                isVisible={this.state.isVisible}
                label={getGroupName(SEARCH_TERM_GROUP.ATTRIBUTE)}
                data-qaid={'menu_attributes'}
            />)
        ];

        const relationFields = getRelationTerms(this.props.metaRecordStore)
            .filter((field) => field.displayName.toLowerCase().indexOf(this.state.searchText.toLowerCase()) > -1);

        const relationCriterias = [
            (<TermGroup
                key={'searchCriteriaRelations'}
                popoverContent={(
                    <div data-qaid={'popover_relations'}>
                        <Attributes
                            onAddTerm={this.addTerm}
                            fields={relationFields as any}
                            searchTerms={this.props.searchPanelStore.listST}
                            isMulti={true}
                        />
                    </div>
                )}
                fieldsLength={relationFields.length}
                isVisible={this.state.isVisible}
                label={getGroupName(SEARCH_TERM_GROUP.RELATIONS)}
                data-qaid={'menu_relations'}
            />)
        ];

        const systemCriterias = [
            <CriteriaItem
                key={'system_header'}
                title={getGroupName(SEARCH_TERM_GROUP.SYSTEM)}
                onClick={() => null}
                isHeader={true}
                data-qaid={'system_header'}
            />
        ].concat(getSystemTerms(this.props.metaRecordStore)
            .filter((field) => field.displayName.toLowerCase().indexOf(this.state.searchText.toLowerCase()) > -1)
            .map((item) => {
                const isChecked = this.props.searchPanelStore.listST.find((st) => st.key === item.key);

                return (
                    <CriteriaItem
                        key={`system_${item.key}`}
                        title={item.displayName}
                        onClick={this.addTerm(item as any)}
                        isChecked={isChecked !== undefined}
                        data-qaid={`item_${item.key}`}
                    />
                );
            })
        );

        return (ueListOfCriteria || []).reduce((result: any, item: UeModuleReactComponent<UeModuleTypeComponent.RENDER_CRITERION>) => {

            if ((item.default.resolver === undefined || item.default.resolver() === true) && item.default.component) {
                const Component: any = item.default.component;

                try {
                    result[item.default.meta.searchGroupType].push(
                        <Component
                            key={item.default.moduleId}
                            metaRecord={this.props.metaRecordStore.recordMetaData}
                            onAddTerm={(a: any) => {
                                this.addTerm(a)();
                            }}
                            searchText={this.state.searchText}
                            searchTerms={this.props.searchPanelStore.listST}
                            parentIsVisible={this.state.isVisible}
                        />
                    );
                } catch (e) {
                    console.error(`Could not use custom component with type '${UeModuleTypeComponent.RENDER_CELL}'`, e);
                }
            }

            return result;
        }, {
            [SEARCH_TERM_GROUP.ATTRIBUTE]: attributes,
            [SEARCH_TERM_GROUP.CLASSIFY]: [],
            [SEARCH_TERM_GROUP.RELATIONS]: relationCriterias,
            [SEARCH_TERM_GROUP.QUALITY]: [],
            [SEARCH_TERM_GROUP.SYSTEM]: systemCriterias
        });
    };

    showFavorite = () => {
        const favoriteEtalonsMap = this.props.favoriteStore.getFavoriteForEntity(this.props.metaRecordStore.currentEntity.entityName);

        if (favoriteEtalonsMap.length > 0) {
            this.setState({isVisible: false});
            this.props.onFavoriteSearch(favoriteEtalonsMap);
        }
    };

    render () {
        const metaRecordStore = this.props.metaRecordStore;
        const listOfCriteria = this.renderListOfCriteria();
        const attributes = metaRecordStore.attributes;
        const hasFavorites = this.props.favoriteStore.getFavoriteForEntity(metaRecordStore.currentEntity.entityName).length > 0;

        return (
            <React.Fragment>
                <DropDown
                    trigger={TRIGGER.CLICK}
                    placement={PLACEMENT.BOTTOM_START}
                    isDisabled={attributes.length === 0}
                    isOpen={this.state.isVisible}
                    onVisibleChange={this.onVisibleChange}
                    target={(
                        <span>
                            <Button
                                name={'addBadget'}
                                intent={INTENT.PRIMARY}
                                leftIcon={'plus'}
                                isRound={true}
                                isDisabled={attributes.length === 0}
                                data-qaid='addCriteria'
                                title={i18n.t('module.data>search.panel>newCriteriaTooltip')}
                            >
                                {i18n.t('module.data>search.panel>newCriteria')}
                            </Button>
                        </span>
                    )}
                >
                    <div className={styles.dropDownContainer} data-qaid={'criteriaTreeDropDownContainer'}>
                        <div style={{margin: '5px 10px'}}>
                            <Input
                                value={this.state.searchText}
                                onChange={this.onSearch} size={'small'}
                                placeholder={i18n.t('module.data>search.panel>search')}
                            />
                        </div>
                        {listOfCriteria[SEARCH_TERM_GROUP.ATTRIBUTE]}
                        {listOfCriteria[SEARCH_TERM_GROUP.CLASSIFY]}
                        {listOfCriteria[SEARCH_TERM_GROUP.RELATIONS]}
                        {listOfCriteria[SEARCH_TERM_GROUP.QUALITY]}
                        {listOfCriteria[SEARCH_TERM_GROUP.SYSTEM]}
                        {hasFavorites && (
                            <>
                                <DropDown.Splitter/>
                                <CriteriaItem
                                    title={i18n.t('module.data>search.panel>favorite>showFavorite')}
                                    onClick={this.showFavorite}
                                    isChecked={Boolean(this.props.searchPanelStore.listST.find((st) => st.key === '$etalon_id'))}
                                    data-qaid={'item_showFavorites'}
                                />
                            </>
                        )}
                    </div>
                </DropDown>
            </React.Fragment>
        );
    }
}
