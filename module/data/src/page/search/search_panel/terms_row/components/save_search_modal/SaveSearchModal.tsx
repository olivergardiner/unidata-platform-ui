/**
 * Component-a modal window for entering the name of saving a search query
 *
 * @author Brauer Ilya
 * @date 2020-04-13
 */

import * as React from 'react';
import {Button, Field, InlineMessage, INTENT, Modal, SIZE, Spinner, UdInlineMessageType} from '@unidata/uikit';
import {ILastSearchItem, SavedSearchStore} from '../../../../store/SavedSearchStore';
import {CurrentEntity} from '../../../../store/MetaRecordStore/MetaRecordStoreTypes';
import {SEARCH_TERM_GROUP, JsonData} from '@unidata/meta';

import isEqual from 'lodash/isEqual';
import omit from 'lodash/omit';
import {i18n} from '@unidata/core-app';

interface IProps {
    currentEntity: CurrentEntity;
    searchTerms: JsonData[];
    savedSearchStore: SavedSearchStore;
    onClose: () => void;
}

interface IState {
    isLoading: boolean;
    searchName: string;
    isSaving: boolean;
    hasCurrentSavedSearch: boolean;
    hasTooManySavedSearch: boolean;
}

const MAX_SAVED_SEARCH = 20; // maximum possible number of saved search queries

export class SaveSearchModal extends React.Component<IProps, IState> {
    state = {
        isLoading: true,
        searchName: '',
        isSaving: false,
        hasCurrentSavedSearch: false,
        hasTooManySavedSearch: false
    };

    componentDidMount (): void {
        this.props.savedSearchStore.getList()
            .then((result) => {

                if (result.length >= MAX_SAVED_SEARCH) {
                    this.setState({hasTooManySavedSearch: true});
                } else if (this.compareFilters(result)) {
                    this.setState({hasCurrentSavedSearch: true});
                }
            })
            .finally(() => {
                this.setState({isLoading: false});
            });
    }

    compareFilters = (savedList: ILastSearchItem[]): boolean => {
        return savedList.every((savedItem) => {
            let equal = true;

            for (let type of Object.values(SEARCH_TERM_GROUP)) {
                // id and measurement must not participate in the comparison
                // id is generated automatically, measurement may be undefined and not written to saved filters
                if (!isEqual(
                    savedItem.searchTerms.map((item) => omit(item, ['id', 'measurement'])),
                    this.props.searchTerms.map((item) => omit(item, ['id', 'measurement']))
                )) {
                    equal = false;
                    break;
                }
            }

            return equal;
        });
    };

    onSaveSearch = () => {
        if (this.state.hasTooManySavedSearch === false) {
            this.setState({isSaving: true});

            const dataToSave = {
                searchName: this.state.searchName,
                currentEntity: this.props.currentEntity,
                searchTerms: this.props.searchTerms
            };

            this.props.savedSearchStore.saveSearch(dataToSave)
                .then(() => {
                    this.props.onClose();
                });
        }
    };

    render () {
        const tooMatchHtml = {__html: i18n.t('module.data>search.panel>saveSearchModal>tooMatchSaved')};
        const hasTheSameHtml = {__html: i18n.t('module.data>search.panel>saveSearchModal>hasTheSame')};

        return (
            <Modal
                isOpen={true}
                onClose={this.props.onClose}
                header={i18n.t('module.data>search.panel>saveSearchModal>header')}
                size={SIZE.MIDDLE}
                footer={(
                    <div style={{textAlign: 'right'}}>
                        <Button
                            isGhost={true}
                            isRound={true}
                            onClick={this.props.onClose}
                        >
                            {i18n.t('common:cancel_noun')}
                        </Button>
                        &nbsp;
                        <Button
                            isWaiting={this.state.isSaving}
                            intent={INTENT.PRIMARY}
                            isRound={true}
                            onClick={this.onSaveSearch}
                            isDisabled={this.state.hasTooManySavedSearch || this.state.isLoading}
                        >
                            {i18n.t('common:save')}
                        </Button>
                    </div>
                )}
            >
                <Spinner isShow={this.state.isLoading}>
                    <Field.Input
                        label={i18n.t('module.data>search.panel>saveSearchModal>title')}
                        defaultValue={this.state.searchName}
                        onChange={((name, value) => this.setState({searchName: value}))}
                        disabled={this.state.hasTooManySavedSearch}
                    />
                    {this.state.hasTooManySavedSearch && (
                        <>
                            <br/>
                            <InlineMessage type={UdInlineMessageType.ERROR}>
                                <div dangerouslySetInnerHTML={tooMatchHtml}/>
                            </InlineMessage>
                        </>
                    )}
                    {this.state.hasCurrentSavedSearch && (
                        <>
                            <br/>
                            <InlineMessage type={UdInlineMessageType.WARNING}>
                                <div dangerouslySetInnerHTML={hasTheSameHtml}/>
                            </InlineMessage>
                        </>
                    )}
                </Spinner>
            </Modal>
        );
    }
}
