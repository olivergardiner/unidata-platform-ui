/**
 * Modal window for selecting related records
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {Button, INTENT, Modal, SIZE} from '@unidata/uikit';
import {MetaRecordStore} from '../../../../store/MetaRecordStore/MetaRecordStore';
import {EntityMeta} from '../../../../store/MetaRecordStore/MetaRecordStoreTypes';
import {SearchResultStore} from '../../../../store/SearchResultStore';
import {SearchPanelStore} from '../../../../store/search_panel_store/SearchPanelStore';
import {TermsRow} from '../../TermsRow';
import {SearchTable} from '../../../../search_table/SearchTable';

import {runInAction} from 'mobx';
import {observer} from 'mobx-react';

import * as modalStyles from './relation.m.scss';
import {FavoriteStore} from '../../../../../../store/FavoriteStore';
import {i18n} from '@unidata/core-app';
import {FindInput} from '../../../find_input/FindInput';
import {AbstractSearchTerm} from '@unidata/meta';
import {SearchHitUtil} from '../../../../../../utils/SearchHitUtil';

interface IProps {
    filters: AbstractSearchTerm[];
    ids: string[];
    entity: EntityMeta;
    onClose: () => void;
    onChoose: (keys: string[], filter: AbstractSearchTerm[]) => void;
}

@observer
export class RelationModal extends React.Component<IProps> {

    private metaRecordStore: MetaRecordStore;
    private searchResultStore: SearchResultStore;
    private searchPanelStore: SearchPanelStore;
    private favoriteStore: FavoriteStore;

    constructor (props: IProps) {
        super(props);

        this.searchPanelStore = new SearchPanelStore();
        this.metaRecordStore = this.searchPanelStore.metaRecordStore;
        this.searchResultStore = this.searchPanelStore.searchResultStore;
        this.favoriteStore = this.searchPanelStore.favoriteStore;

        this.searchResultStore.canChooseAll = false;

        this.metaRecordStore.setCurrentEntity(this.props.entity);
        this.metaRecordStore.getMetaRecordFull()
            .then(() => {
                return this.searchPanelStore.doRequest();
            })
            .then(() => {
                runInAction(() => {
                    this.searchResultStore.checkedRecords = this.props.ids.reduce((result: any, id: string) => {
                        result[id] = true;

                        return result;
                    }, {});
                });
            });
    }

    chooseItems = () => {
        const checkKeys = this.searchResultStore.checkedRecords.map((item) => {
            const record = this.searchResultStore.records[item];
            const etalonId = SearchHitUtil.getFieldValues<string>(record, '$etalon_id')[0];
            const from = SearchHitUtil.getFieldValues<string | null>(record, '$from')[0];
            const to = SearchHitUtil.getFieldValues<string | null>(record, '$to')[0];

            return [etalonId, from, to].join('__');
        });

        this.props.onChoose(checkKeys, this.searchPanelStore.listST);
    };

    render () {
        const metaRecordStore = this.metaRecordStore;
        const searchPanelStore = this.searchPanelStore;
        const searchResultStore = this.searchResultStore;
        const checkedRecordsCount = searchResultStore.checkedRecords.length;

        return (
            <Modal
                isOpen={true}
                shouldCloseOnClickOutside={false}
                onClose={this.props.onClose}
                hasCloseIcon={false}
                size={SIZE.EXTRA_LARGE}
                header={(
                    <React.Fragment>
                        {i18n.t('module.data>search.panel>referenceWith', {name: this.props.entity.entityDisplayName})}
                    </React.Fragment>
                )}
                footer={(
                    <div className={modalStyles.modalFooter}>
                        <div/>
                        <div>
                            <Button
                                intent={INTENT.PRIMARY}
                                isRound={true}
                                onClick={this.chooseItems}
                                data-qaid='choose'
                                isDisabled={checkedRecordsCount === 0}
                            >{i18n.t('select')}</Button>
                            &nbsp;
                            <Button
                                isGhost={true}
                                isRound={true}
                                onClick={this.props.onClose}
                                data-qaid='cancel'
                            >{i18n.t('cancel')}</Button>
                        </div>
                    </div>
                )}
            >
                <FindInput
                    metaRecordStore={metaRecordStore}
                    searchPanelStore={searchPanelStore}
                    searchResultStore={searchResultStore}
                />
                <TermsRow
                    metaRecordStore={metaRecordStore}
                    searchPanelStore={searchPanelStore}
                    favoriteStore={this.favoriteStore}
                />
                <div className={modalStyles.modalTableContainer}>
                        <SearchTable
                            searchResultStore={searchResultStore}
                            metaRecordStore={metaRecordStore}
                            withFavorite={false}
                        />
                </div>
            </Modal>
        );
    }
}
