/**
 * Component SourceSystem SearchTerm
 *
 * @author Brauer Ilya
 * @date 2020-07-27
 */

import * as React from 'react';
import {IComponentPropsByUEType, UeModuleTypeComponent} from '@unidata/core';
import {
    AbstractSearchTerm,
    RelationST,
    SEARCH_TERM_GROUP,
    SourceST,
    SourceSystemService,
    StringAttributeST
} from '@unidata/meta';
import {SearchTerm} from '../../../../../../component/search_term/SearchTerm';
import {CriteriaItem} from '../../../../../../component/criteria_item/CriteriaItem';
import {observer} from 'mobx-react';

type IProps = IComponentPropsByUEType[UeModuleTypeComponent.RENDER_SEARCH_TERM] & {
    searchTerm: SourceST;
}

@observer
export class SourceTerm extends React.Component<IProps, {}> {

    componentDidMount (): void {
        const searchTerm = this.props.searchTerm;

        SourceSystemService.getSourceSystemList().then((list) => {
            searchTerm.setAttributes(list.map((sourceItem) => {
                return new StringAttributeST({
                    key: sourceItem.name.getValue(),
                    displayName: sourceItem.name.getValue(),
                    termGroup: SEARCH_TERM_GROUP.SYSTEM
                });
            }));
            this.forceUpdate();
        });
    }

    addTerm = (term: AbstractSearchTerm) => () => {
        term.setParentId(this.props.searchTerm.id);
        this.props.searchTerm.addItem(term);

        if (this.props.onAddTerm) {
            this.props.onAddTerm();
        }
    };

    deleteItem = (index: number) => () => {
        this.props.searchTerm.deleteItem(index);
    };

    render () {
        const searchTerm: SourceST = this.props.searchTerm;
        const label = searchTerm.displayName;

        const termProps = {
            value: '',
            label
        };

        const valueKeys = searchTerm.itemsData.map((item) => item.key);

        return (
            <React.Fragment>
                <SearchTerm.Group
                    searchTerm={searchTerm}
                    searchTermList={searchTerm.itemsData}
                    onDelete={this.props.onDelete}
                    termProps={termProps}
                >
                    {searchTerm.attributes.length > 0 && (
                        <React.Fragment>
                            {searchTerm.attributes.map((attr: AbstractSearchTerm) => {
                                const isChecked = valueKeys.includes(attr.key);

                                return (
                                    <CriteriaItem
                                        key={attr.key}
                                        title={attr.displayName}
                                        onClick={this.addTerm(attr)}
                                        isChecked={isChecked}
                                        data-qaid={attr.key}
                                    />
                                );
                            })}
                        </React.Fragment>
                    )}
                </SearchTerm.Group>

                {searchTerm.itemsData.map((item, index) => {
                    return (
                        <SearchTerm.Group
                            key={item.key}
                            searchTerm={item}
                            searchTermList={searchTerm.itemsData}
                            onDelete={this.deleteItem(index)}
                        />
                    );
                })}
            </React.Fragment>
        );
    }
}
