export const relationTermItem: string;

export const modalTableContainer: string;

export const before: string;

export const after: string;

export const modalFooter: string;

export const attributesLabel: string;
