/**
 * Component container for the search terms
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {MetaRecordStore} from '../../store/MetaRecordStore/MetaRecordStore';
import {SearchPanelStore} from '../../store/search_panel_store/SearchPanelStore';
import {inject, observer} from 'mobx-react';

import * as styles from './termsRow.m.scss';
import {CriteriaTree} from './components/criteria_tree/CriteriaTree';
import {Button, INTENT, SIZE} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import {ueModuleManager, UeModuleReactComponent, UeModuleTypeComponent} from '@unidata/core';
import {Relation} from './components/relation/Relation';
import {FavoriteStore} from '../../../../store/FavoriteStore';
import {makeFavoriteSearchTerms} from '../../store/search_panel_store/favoriteTerms';
import {SaveSearchModal} from './components/save_search_modal/SaveSearchModal';
import {SavedSearchStore} from '../../store/SavedSearchStore';
import {SearchTerm} from '../../../../component/search_term/SearchTerm';
import {AbstractSearchTerm, SEARCH_TERM_GROUP, SourceST} from '@unidata/meta';
import {SourceTerm} from './components/source_term/SourceTerm';

interface IInjectedProps {
    savedSearchStore: SavedSearchStore;
}

type IProps = {
    metaRecordStore: MetaRecordStore;
    searchPanelStore: SearchPanelStore;
    favoriteStore: FavoriteStore;
} & Partial<IInjectedProps>;

type IState = {
    isOpen: boolean;
    isPin: boolean;
    isTagListOverflow: boolean;
    isOpenSaveSearchModal: boolean;
    isDisableSaveSearchButton: boolean;
}

const initState: IState = {
    isOpen: true,
    isPin: true,
    isTagListOverflow: false,
    isOpenSaveSearchModal: false,
    isDisableSaveSearchButton: false
};

@inject('savedSearchStore')
@observer
export class TermsRow extends React.Component<IProps, IState> {
    state: IState = initState;

    rows: number = 1;
    containerElement = React.createRef<HTMLDivElement>();
    hoverTimer: any;

    get propsWithInject () {
        return this.props as IProps & IInjectedProps;
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        let rows = this.rows;

        setTimeout(() => {
            if (this.containerElement.current !== null) {
                rows = this.containerElement.current.scrollHeight / 50; // 50 - Current height of the search bar
            }

            if (rows !== this.rows && rows > 1) {
                this.rows = rows;

                this.setState((prevState) => {
                    return {
                        isOpen: (this.state.isPin) ? prevState.isOpen : true
                    };
                });
            }
        }, 0);
    }

    componentDidMount (): void {
        document.body.addEventListener('click', this.handleClick);
    }

    componentWillUnmount (): void {
        document.body.removeEventListener('click', this.handleClick);
    }

    handleClick = (e: any) => {
        if (this.state.isPin === false) {
            const element = this.containerElement.current;
            const dropDownElement = document.querySelectorAll('.ant-dropdown');
            const datePickerElement = document.querySelectorAll('.ant-calendar-picker-container');

            if (element !== null && !element.contains(e.target)) {
                let isOpen = false;

                if (dropDownElement.length > 0) {
                    dropDownElement.forEach((element) => {
                        if (element.contains(e.target)) {
                            isOpen = true;

                            return;
                        }
                    });
                }

                if (datePickerElement.length > 0) {
                    datePickerElement.forEach((element) => {
                        if (element.contains(e.target)) {
                            isOpen = true;

                            return;
                        }
                    });
                }

                this.setState({isOpen});
            }
        }

    };

    onMouseEnter = () => {
        if (this.state.isPin === false) {
            if (this.hoverTimer) {
                clearTimeout(this.hoverTimer);
            }

            if (this.rows > 1) {
                this.hoverTimer = setTimeout(() => {
                    this.setState({isOpen: true});
                }, 400);
            }
        }
    };
    onMouseLeave = () => {
        if (this.hoverTimer) {
            clearTimeout(this.hoverTimer);
        }
    };

    toggleRow = () => {
        if (this.hoverTimer) {
            clearTimeout(this.hoverTimer);
        }

        this.setState((prevState: any) => {
            return {
                isOpen: !prevState.isOpen
            };
        });
    };

    onAddSubTerm = () => {
        // a small hack to redraw the entire line when adding a term to a group (for example. communication),
        // to display the height of the search bar correctly
        this.setState({});
    };

    onAddTerm = (searchTerm: AbstractSearchTerm) => {
        this.props.searchPanelStore.clearQueryTimer();

        this.setState({isDisableSaveSearchButton: false});

        this.props.searchPanelStore.addTerm(searchTerm);
    };

    onDelete = (term: AbstractSearchTerm) => () => {
        this.props.searchPanelStore.deleteTerm(term);
        this.onSubmit();
    };

    onRemoveAll = () => {
        this.rows = 1;
        this.props.searchPanelStore.deleteAllTerms();
        this.onSubmit();

        this.setState({
            isDisableSaveSearchButton: true
        });
    };

    onSubmit = () => {
        this.props.searchPanelStore.doRequest();
    };

    onClickRowSize = () => {
        if (this.state.isOpen === false) {
            setTimeout(() => {
                this.setState({
                    isTagListOverflow: true
                });
            }, 500);
        }

        this.setState((prevState: any) => {
            return {
                isOpen: !prevState.isOpen,
                isTagListOverflow: false
            };
        });
    };

    showFavorite = (favoriteEtalonsMap: string[]) => {
        const {etalonTerm, rangeTerm} = makeFavoriteSearchTerms(favoriteEtalonsMap);

        this.onAddTerm(etalonTerm);
        this.onAddTerm(rangeTerm);

        this.onSubmit();
    };

    toggleSaveSearchModal = () => {
        this.setState((prevState) => {
            return {
                isOpenSaveSearchModal: !prevState.isOpenSaveSearchModal
            };
        });
    };

    render () {
        const tagListContainer = [styles.tagsContainer];

        if (this.state.isOpen && this.rows > 1) {
            tagListContainer.push(styles.tagListIsOpen);
        }

        const {
            metaRecordStore,
            favoriteStore,
            searchPanelStore
        } = this.props;

        const searchTermLength = searchPanelStore.getAllTermsCount();
        const ueSearchTermList = ueModuleManager.getModulesByType(UeModuleTypeComponent.RENDER_SEARCH_TERM);

        let ClassifySearchTerm: any;
        let DqSearchTerm: any;

        if (ueSearchTermList !== null) {

            const classifierComponent = ueSearchTermList.find((item) => {
                return item.default.meta.searchGroupType === SEARCH_TERM_GROUP.CLASSIFY;
            });

            ClassifySearchTerm = classifierComponent !== undefined ?
                (classifierComponent as UeModuleReactComponent<UeModuleTypeComponent.RENDER_SEARCH_TERM>).default.component :
                undefined;

            const dqComponent = ueSearchTermList.find((item) => {
                return item.default.meta.searchGroupType === SEARCH_TERM_GROUP.QUALITY;
            });

            DqSearchTerm = dqComponent !== undefined ?
                (dqComponent as UeModuleReactComponent<UeModuleTypeComponent.RENDER_SEARCH_TERM>).default.component :
                undefined;
        }

        const tagsRow = [styles.tagsRow];

        if (this.state.isPin) {
            tagsRow.push(styles.isPin);
        }

        const tagsList = [styles.tagsList];

        if (this.state.isTagListOverflow) {
            tagsList.push(styles.tagsListOverflow);
        }

        return (
            <div className={tagsRow.join(' ')} onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
                <div className={tagListContainer.join(' ')} ref={this.containerElement}>
                    <div className={tagsList.join(' ')}>
                        <div className={styles.buttonContainer}>
                            <CriteriaTree
                                onAddTerm={this.onAddTerm}
                                onFavoriteSearch={this.showFavorite}
                                metaRecordStore={metaRecordStore}
                                searchPanelStore={searchPanelStore}
                                favoriteStore={favoriteStore}
                            />
                            <Button
                                isMinimal={true}
                                onClick={this.onRemoveAll}
                                isDisabled={searchTermLength === 0}
                                data-qaid='clearAllTerms'
                            >
                                {i18n.t('module.data>search.panel>clear')}
                                {searchTermLength > 0 && (<div className={styles.countBadge}>{searchTermLength}</div>)}
                            </Button>
                            <div className={styles.sep}/>
                        </div>
                        <SearchTerm.CommonGroup
                            searchTerms={searchPanelStore.mapST[SEARCH_TERM_GROUP.ATTRIBUTE]}
                            hasPrev={false}
                        >
                            {searchPanelStore.mapST[SEARCH_TERM_GROUP.ATTRIBUTE].map((searchTerm) => {
                                return (
                                    <SearchTerm.Attribute
                                        key={`attribute_${searchTerm.id}`}
                                        searchTerm={searchTerm as any}
                                        onDelete={this.onDelete(searchTerm)}
                                    />
                                );
                            })}
                        </SearchTerm.CommonGroup>
                        <SearchTerm.CommonGroup
                            searchTerms={searchPanelStore.mapST[SEARCH_TERM_GROUP.CLASSIFY]}
                            hasPrev={searchPanelStore.hasPrev(SEARCH_TERM_GROUP.CLASSIFY)}
                        >
                            {ClassifySearchTerm !== undefined && searchPanelStore.mapST[SEARCH_TERM_GROUP.CLASSIFY].map((searchTerm) => {
                                return (
                                    <ClassifySearchTerm
                                        key={`classify_${searchTerm.id}_${searchTerm.parentId || 'main'}`}
                                        searchTerm={searchTerm}
                                        searchTermList={searchPanelStore.mapST[SEARCH_TERM_GROUP.CLASSIFY]}
                                        onAddTerm={this.onAddSubTerm}
                                        onDelete={this.onDelete(searchTerm)}
                                    />
                                );
                            })}
                        </SearchTerm.CommonGroup>
                        <SearchTerm.CommonGroup
                            searchTerms={searchPanelStore.mapST[SEARCH_TERM_GROUP.QUALITY]}
                            hasPrev={searchPanelStore.hasPrev(SEARCH_TERM_GROUP.QUALITY)}
                        >
                            {DqSearchTerm !== undefined && searchPanelStore.mapST[SEARCH_TERM_GROUP.QUALITY].map((searchTerm) => {
                                return (
                                    <DqSearchTerm
                                        key={`quality_${searchTerm.id}`}
                                        searchTerm={searchTerm}
                                        searchTermList={searchPanelStore.mapST[SEARCH_TERM_GROUP.QUALITY]
                                            .concat(searchPanelStore.mapST[SEARCH_TERM_GROUP.CLASSIFY])}
                                        onAddTerm={this.onAddSubTerm}
                                        onDelete={this.onDelete(searchTerm)}
                                        onSubmit={this.onSubmit}
                                    />
                                );
                            })}
                        </SearchTerm.CommonGroup>
                        <SearchTerm.CommonGroup
                            searchTerms={searchPanelStore.mapST[SEARCH_TERM_GROUP.RELATIONS]}
                            hasPrev={searchPanelStore.hasPrev(SEARCH_TERM_GROUP.RELATIONS)}
                        >
                            {searchPanelStore.mapST[SEARCH_TERM_GROUP.RELATIONS].map((searchTerm) => {
                                return (
                                    <Relation
                                        key={`relation_${searchTerm.id}`}
                                        searchTerm={searchTerm}
                                        searchTermList={searchPanelStore.listST}
                                        onAddTerm={this.onAddSubTerm}
                                        onDelete={this.onDelete(searchTerm)}
                                    />
                                );
                            })}
                        </SearchTerm.CommonGroup>
                        <SearchTerm.CommonGroup
                            searchTerms={searchPanelStore.mapST[SEARCH_TERM_GROUP.SYSTEM]}
                            hasPrev={searchPanelStore.hasPrev(SEARCH_TERM_GROUP.SYSTEM)}
                        >
                            {searchPanelStore.mapST[SEARCH_TERM_GROUP.SYSTEM].map((searchTerm) => {
                                if (searchTerm instanceof SourceST) {
                                    return (
                                        <SourceTerm
                                            key={`system_${searchTerm.id}`}
                                            searchTerm={searchTerm}
                                            searchTermList={searchPanelStore.listST}
                                            onAddTerm={this.onAddSubTerm}
                                            onDelete={this.onDelete(searchTerm)}
                                        />
                                    );
                                }

                                return (
                                    <SearchTerm
                                        key={`system_${searchTerm.id}`}
                                        searchTerm={searchTerm}
                                        onDelete={this.onDelete(searchTerm)}
                                    />
                                );
                            })}
                        </SearchTerm.CommonGroup>
                        {searchPanelStore.listST.length > 0 && (
                            <div className={styles.searchSaveContainer}>
                                <Button
                                    onClick={this.onSubmit}
                                    intent={INTENT.SECONDARY} leftIcon={'search'}
                                    data-qaid='searchButton'
                                    title={i18n.t('module.data>search.panel>searchTooltip')}
                                >
                                    {i18n.t('module.data>search.panel>search')}
                                </Button>

                                <Button
                                    onClick={this.toggleSaveSearchModal}
                                    leftIcon={'save'}
                                    data-qaid='saveButton'
                                    isDisabled={this.state.isDisableSaveSearchButton}
                                    title={i18n.t('module.data>search.panel>saveButtonTooltip')}
                                />
                            </div>
                        )}
                    </div>
                    <div className={styles.pinContainer}>
                        <Button
                            size={SIZE.SMALL}
                            leftIcon={this.state.isPin ? 'filledPin' : 'pin'}
                            onClick={() => this.setState((prevState) => ({isPin: !prevState.isPin}))}
                            isMinimal={true}
                            data-qaid='pinButton'
                            title={i18n.t('module.data>search.panel>pinTooltip')}
                        />
                        {this.state.isPin && this.rows > 1 && (
                            <Button
                                size={SIZE.SMALL}
                                leftIcon={this.state.isOpen ? 'up' : 'down'}
                                onClick={this.onClickRowSize}
                                isMinimal={true}
                                data-qaid='openPinButton'
                            />
                        )}
                    </div>
                    {this.rows > 1 && !this.state.isPin && (<div className={styles.open} onClick={this.toggleRow}/>)}
                </div>
                {this.state.isOpenSaveSearchModal && (
                    <SaveSearchModal
                        currentEntity={metaRecordStore.currentEntity}
                        searchTerms={searchPanelStore.listSTJson}
                        savedSearchStore={this.propsWithInject.savedSearchStore}
                        onClose={this.toggleSaveSearchModal}
                    />
                )}
            </div>
        );
    }
}
