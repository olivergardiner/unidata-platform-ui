/**
 * Input for quick search
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import {Icon, Input, PLACEMENT, Tooltip, TRIGGER} from '@unidata/uikit';
import * as React from 'react';
import {observer} from 'mobx-react';
import {MetaRecordStore} from '../../store/MetaRecordStore/MetaRecordStore';
import {SearchPanelStore} from '../../store/search_panel_store/SearchPanelStore';
import {SearchResultStore} from '../../store/SearchResultStore';
import {i18n} from '@unidata/core-app';
import {SEARCH_TERM_GROUP} from '@unidata/meta';

interface IProps {
    metaRecordStore: MetaRecordStore;
    searchPanelStore: SearchPanelStore;
    searchResultStore: SearchResultStore;
}

@observer
export class FindInput extends React.Component<IProps> {
    onPressEnter = () => {
        const searchPanelStore = this.props.searchPanelStore;
        const searchResultStore = this.props.searchResultStore;
        const metaRecordStore = this.props.metaRecordStore;

        if (metaRecordStore.recordMetaData !== undefined) {
            const searchQuery = searchPanelStore.getQuery();

            searchResultStore.setSearchQuery(searchQuery);
            searchResultStore.getRecords();
        }
    };

    hasNotBlockingTerms = (): boolean => {
        const searchPanelStore = this.props.searchPanelStore;

        const result: boolean = [
            SEARCH_TERM_GROUP.ATTRIBUTE,
            SEARCH_TERM_GROUP.CLASSIFY,
            SEARCH_TERM_GROUP.QUALITY,
            SEARCH_TERM_GROUP.RELATIONS
        ].every((key: SEARCH_TERM_GROUP) => {
            return searchPanelStore.mapST[key].length === 0;
        });

        return result;
    };

    onChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
        const searchPanelStore = this.props.searchPanelStore;

        if (this.props.metaRecordStore.recordMetaData !== undefined && this.hasNotBlockingTerms()) {
            const value = e.currentTarget.value;

            searchPanelStore.setKeyword(value);
        }
    };

    render () {
        const metaRecordStore = this.props.metaRecordStore;
        const searchPanelStore = this.props.searchPanelStore;
        const isDisabled = metaRecordStore.currentEntity.entityName === '' || !this.hasNotBlockingTerms();

        const disabledText = metaRecordStore.currentEntity.entityName === '' ?
            i18n.t('module.data>search.panel>searchButtonTooltip_noEntity') :
            i18n.t('module.data>search.panel>searchButtonTooltip_existsSearchTerms');

        return (
            <Input
                placeholder={i18n.t('module.data>search.panel>search') + '...'}
                value={searchPanelStore.keyword}
                onChange={this.onChange}
                onPressEnter={this.onPressEnter}
                disabled={isDisabled}
                suffix={isDisabled ? (
                    <Tooltip
                        overlay={disabledText}
                        trigger={TRIGGER.HOVER}
                        placement={PLACEMENT.RIGHT}
                    >
                        <span>
                             <Icon name={'search'}/>
                        </span>
                    </Tooltip>
                ) : (
                    <Icon name={'search'}/>
                )}
                data-qaid='querySearch'
            />
        );
    }
}
