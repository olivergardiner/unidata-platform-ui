/**
 * Display component of the search string
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import * as styles from './searchPanel.m.scss';
import {GroupInput} from '@unidata/uikit';
import {SearchPanelStore} from '../store/search_panel_store/SearchPanelStore';
import {MetaRecordStore} from '../store/MetaRecordStore/MetaRecordStore';
import {inject, observer} from 'mobx-react';
import {FindInput} from './find_input/FindInput';
import {SearchResultStore} from '../store/SearchResultStore';
import {TermsRow} from './terms_row/TermsRow';
import {FavoriteStore} from '../../../store/FavoriteStore';
import {Catalog, EntitySelect, EntityTreeType} from '@unidata/meta';

interface IProps {
    metaRecordStore: MetaRecordStore;
    searchPanelStore: SearchPanelStore;
    searchResultStore: SearchResultStore;
    favoriteStore: FavoriteStore;
}

@inject('routing')
@observer
export class SearchPanel extends React.Component<IProps, any> {

    entitySelectRef: EntitySelect | null = null;

    onEntitySelect = (entity: EntityTreeType) => {
        if (!(entity instanceof Catalog)) {
            (this.props as any).routing.push(`/search/${entity.name.getValue()}`);
        }
    };

    setRef = (el: EntitySelect) => {
        this.entitySelectRef = el;
    };

    render () {
        const searchPanelStore = this.props.searchPanelStore;
        const metaRecordStore = this.props.metaRecordStore;
        const favoriteStore = this.props.favoriteStore;
        const searchResultStore = this.props.searchResultStore;

        const entitySelectValue: string | undefined = metaRecordStore.currentEntity.entityName === '' ?
            undefined :
            metaRecordStore.currentEntity.entityName;

        return (
            <React.Fragment>
                <div className={styles.searchContainer}>
                    <div className={styles.searchControls}>
                        <div className={styles.entityList}>
                            <GroupInput>
                                <EntitySelect
                                    value={entitySelectValue}
                                    onEntitySelect={this.onEntitySelect}
                                    ref={this.setRef}
                                />
                                <FindInput
                                    metaRecordStore={metaRecordStore}
                                    searchPanelStore={searchPanelStore}
                                    searchResultStore={searchResultStore}
                                />
                            </GroupInput>
                        </div>
                    </div>
                </div>
                {metaRecordStore.currentEntity.entityName !== '' && (
                    <TermsRow
                        metaRecordStore={metaRecordStore}
                        searchPanelStore={searchPanelStore}
                        favoriteStore={favoriteStore}
                    />
                )}
            </React.Fragment>
        );
    }
}
