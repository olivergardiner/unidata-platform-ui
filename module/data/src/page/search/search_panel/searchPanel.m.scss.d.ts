export const searchContainer: string;
export const searchControls: string;
export const entityList: string;
export const allFilters: string;
export const searchButton: string;
