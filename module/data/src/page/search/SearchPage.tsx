﻿/**
 * Container for the data search result.
 * Contains logic for getting MetaRecord and rendering the table search result
 *
 * @author: Ilya Brauer
 * @date: 2019-10-08
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {Layout} from '@unidata/uikit';
import {SearchTable} from './search_table/SearchTable';
import {i18n} from '@unidata/core-app';
import {MetaRecordStore} from './store/MetaRecordStore/MetaRecordStore';
import {SearchResultStore} from './store/SearchResultStore';
import {Header} from './header/Header';
import * as styles from './search.m.scss';

import {SearchPanel} from './search_panel/SearchPanel';
import {SearchPanelStore} from './store/search_panel_store/SearchPanelStore';
import {FavoriteStore} from '../../store/FavoriteStore';
import {RouteComponentProps} from 'react-router';
import {EntityType} from '@unidata/types';
import {ILastSearchItem, SavedSearchStore} from './store/SavedSearchStore';

interface IRouteProps {
    entityName?: string;
}

type IProps = RouteComponentProps<IRouteProps, {}, ILastSearchItem>

@observer
export class SearchPage extends React.Component<IProps> {
    private metaRecordStore: MetaRecordStore;
    private searchResultStore: SearchResultStore;
    private searchPanelStore: SearchPanelStore;
    private favoriteStore: FavoriteStore;
    private savedSearchStore: SavedSearchStore = new SavedSearchStore();

    searchPanelRef: SearchPanel | null = null;

    constructor (props: IProps) {
        super(props);

        this.searchPanelStore = new SearchPanelStore();
        this.metaRecordStore = this.searchPanelStore.metaRecordStore;
        this.searchResultStore = this.searchPanelStore.searchResultStore;
        this.favoriteStore = this.searchPanelStore.favoriteStore;

        this.metaRecordStore.getEntityList()
            .then(() => {
                if (props.match.params.entityName !== undefined) {
                    this.doSearch(props.match.params.entityName);
                }
            });
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>datasearch');
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<{}>, snapshot?: any): void {
        const metaRecordStore = this.metaRecordStore;
        const searchPanelStore = this.searchPanelStore;

        if (prevProps.match.params.entityName !== this.props.match.params.entityName) {
            if (this.props.match.params.entityName !== undefined) {
                this.doSearch(this.props.match.params.entityName);
            } else {
                metaRecordStore.setCurrentEntity({
                    entityName: '',
                    entityDisplayName: '',
                    entityType: EntityType.Entity
                });
                metaRecordStore.reloadFavorite();

                searchPanelStore.setKeyword('');
                searchPanelStore.clearSearch();
            }
        }
    }

    doSearch = (entityName: string) => {
        const searchPanelStore = this.searchPanelStore;
        const locationState: ILastSearchItem | undefined = this.props.location.state;

        searchPanelStore.setKeyword('');
        searchPanelStore.clearSearch();

        if (locationState !== undefined) {
            locationState.searchTerms.forEach((searchTerm) => {
                searchPanelStore.addTerm(searchTerm);
            });
        }

        this.metaRecordStore
            .setCurrentEntityByName(entityName)
            .then(this.metaRecordStore.getMetaRecordFull)
            .then(this.metaRecordStore.reloadFavorite)
            .then(searchPanelStore.doRequest);
    };

    setRefSearchPanel = (el: SearchPanel) => {
        this.searchPanelRef = el;
    };

    openEntitySelect = () => {
       this.searchPanelRef?.entitySelectRef?.setDropdownVisible(true);
       this.searchPanelRef?.entitySelectRef?.selectElement?.focus();
    };

    render () {
        return (
            <Provider
                searchResultStore={this.searchResultStore}
                metaRecordStore={this.metaRecordStore}
                savedSearchStore={this.savedSearchStore}
                searchPanelStore={this.searchPanelStore}
            >
                <div data-qaid='searchPage'>
                    <Header
                        metaRecordStore={this.metaRecordStore}
                        searchResultStore={this.searchResultStore}
                        searchPanelStore={this.searchPanelStore}
                    />

                    <SearchPanel
                        searchResultStore={this.searchResultStore}
                        metaRecordStore={this.metaRecordStore}
                        searchPanelStore={this.searchPanelStore}
                        favoriteStore={this.favoriteStore}
                        ref={this.setRefSearchPanel}
                    />
                    <Layout className={styles.layoutClass}>
                        <SearchTable
                            searchResultStore={this.searchResultStore}
                            metaRecordStore={this.metaRecordStore}
                            onClickToSelectEntity={this.openEntitySelect}
                        />
                    </Layout>
                </div>
            </Provider>
        );
    }
}
