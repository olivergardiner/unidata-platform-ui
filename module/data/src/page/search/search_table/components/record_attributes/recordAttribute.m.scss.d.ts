export const container: string;

export const title: string;

export const item: string;

export const itemWithSubTree: string;

export const icon: string;
