import * as React from 'react';
import {Checkbox, CheckboxChangeEvent, Icon, PLACEMENT, Popover, POPOVER_BOUNDARIES, TRIGGER} from '@unidata/uikit';
import {MetaRecordStore} from '../../../store/MetaRecordStore/MetaRecordStore';
import {observer} from 'mobx-react';
import get from 'lodash/get';
import * as styles from './recordAttribute.m.scss';
import {SearchResultStore} from '../../../store/SearchResultStore';
import {IDisplayAttr} from '@unidata/meta';

interface IProps {
    path: string;
    metaRecordStore: MetaRecordStore;
    searchResultStore: SearchResultStore;
}

@observer
export class RecordAttribute extends React.Component<IProps> {

    private changeShow = (recordItem: IDisplayAttr, path: string) => {
        return (e: CheckboxChangeEvent) => {
            const checked = e.target.checked;

            this.props.metaRecordStore.toggleAttributeVisible(path, checked);

            if (checked === true && recordItem.isRelation) {
                const relations = this.props.searchResultStore.relations.filter((item) => item.relName === recordItem.name);

                if (relations.length === 0) {
                    this.props.searchResultStore.getRelations([recordItem]);
                }
            }
        };
    };

    render () {
        const recordItem = get(this.props.metaRecordStore.attributes.slice(), this.props.path) as IDisplayAttr;

        if (!recordItem) {
            return null;
        }

        if (recordItem.children && recordItem.children.length > 0) {
            return (
                <Popover
                    placement={PLACEMENT.LEFT}
                    target={(
                        <div key={`columnItem_${recordItem.name}`} className={[styles.item, styles.itemWithSubTree].join(' ')}>
                            <div>
                                <Checkbox checked={recordItem.isShow} onChange={this.changeShow(recordItem, this.props.path)}/>
                                &nbsp; &nbsp;
                                {recordItem.displayName}
                            </div>
                            <div>
                                <Icon name={'caret-right'}/>
                            </div>
                        </div>
                    )}
                    trigger={TRIGGER.HOVER}
                    mouseLeaveDelay={100}
                    mouseEnterDelay={300}
                    boundaries={POPOVER_BOUNDARIES.VIEWPORT}
                >
                    <div style={{maxHeight: '400px', overflowY: 'auto'}}>
                        {recordItem.children.map((item, index) => {
                            return (
                                <RecordAttribute
                                    key={`columnItem_${item.name}`}
                                    metaRecordStore={this.props.metaRecordStore}
                                    searchResultStore={this.props.searchResultStore}
                                    path={`${this.props.path}.children[${index}]`}
                                />
                            );
                        })}
                    </div>
                </Popover>
            );
        }

        return (
            <div key={`columnItem_${recordItem.name}`} className={styles.item}>
                <Checkbox checked={recordItem.isShow} onChange={this.changeShow(recordItem, this.props.path)}/>
                &nbsp; &nbsp;
                {recordItem.displayName}
            </div>
        );
    }
}
