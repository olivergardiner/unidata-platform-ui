/**
 * List of displayed attributes
 *
 * @author: Ilya Braujer
 * @date: 2019-10-16
 */

import * as React from 'react';
import {MetaRecordStore} from '../../../store/MetaRecordStore/MetaRecordStore';
import {observer} from 'mobx-react';
import {RecordAttribute} from './RecordAttribute';
import {Checkbox, Popover, PLACEMENT, TRIGGER, DropDown, Icon, Tooltip} from '@unidata/uikit';
import {SearchResultStore} from '../../../store/SearchResultStore';
import * as styles from './recordAttribute.m.scss';
import {i18n} from '@unidata/core-app';

interface IProps {
    metaRecordStore: MetaRecordStore;
    searchResultStore: SearchResultStore;
}

@observer
export class RecordAttributes extends React.Component<IProps> {

    private checkAll = () => {
        const metaRecordStore = this.props.metaRecordStore;
        const isAllChecked = !metaRecordStore.attributes.some((attribute) => attribute.isShow === false);

        if (isAllChecked === true) {
            metaRecordStore.resetAttributesVisible();
        } else {
            metaRecordStore.setAllAttributesVisible();
        }
    };

    private toggle = (visible: boolean) => {
        if (visible === false) {
            this.props.metaRecordStore.saveVisibleSettings();
        }
    };

    render () {
        const {
            metaRecordStore,
            searchResultStore
        } = this.props;

        return (
            <Popover
                placement={PLACEMENT.BOTTOM_END}
                onVisibleChange={this.toggle}
                target={(
                    <span>
                        <Icon
                            name={'setting'}
                            className={styles.icon}
                            data-qaid='settings'
                            title={i18n.t('module.data>search.table>settingsTooltip')}
                        />
                    </span>
                )}
                trigger={TRIGGER.CLICK}
            >
                <div data-qaid='columnsVisible'>
                    <div className={styles.title}>
                        {i18n.t('module.data>search.table>columnsVisible')}
                    </div>
                    <div className={styles.item}>
                        <Checkbox
                            checked={!metaRecordStore.attributes.some((attribute) => attribute.isShow === false)}
                            onChange={this.checkAll}
                        /> {i18n.t('module.data>search.table>enableAll')}
                    </div>
                    <DropDown.Splitter />
                    <div style={{maxHeight: '400px', overflowY: 'auto'}}>
                        {metaRecordStore.attributes.map((item, index) => {
                            return (
                                <RecordAttribute
                                    key={`columnItem_${item.name}`}
                                    metaRecordStore={metaRecordStore}
                                    searchResultStore={searchResultStore}
                                    path={`[${index}]`}
                                />
                            );
                        })}
                    </div>
                </div>
            </Popover>
        );
    }
}
