import * as React from 'react';
import * as styles from './favoriteCheckbox.m.scss';
import {Icon, PLACEMENT, SIZE, Tooltip, TRIGGER} from '@unidata/uikit';
import {FavoriteStore} from '../../../../../store/FavoriteStore';
import {IconIntent} from '@unidata/icon';
import {i18n} from '@unidata/core-app';

interface IProps {
    favoriteStore: FavoriteStore;
    etalonId: string;
    entityName: string;
    isLarge?: boolean;
}

export class FavoriteCheckbox extends React.Component<IProps> {

    componentDidMount (): void {
        window.addEventListener('storage', this.handleFavoriteStoreUpdate);
    }

    componentWillUnmount (): void {
        window.removeEventListener('storage', this.handleFavoriteStoreUpdate);
    }

    handleFavoriteStoreUpdate = (e: StorageEvent) => {
        if (e.key === 'favorite' && e.newValue !== null) {
            this.forceUpdate();
        }
    };

    private handleClick = (isFavorite: boolean) => {
        return (e: React.SyntheticEvent<HTMLSpanElement>) => {
            e.stopPropagation();

            if (isFavorite) {
                this.props.favoriteStore.deleteFromFavorite(this.props.entityName, this.props.etalonId)
                    .then(() => {
                        this.forceUpdate();
                    });
            } else {
                this.props.favoriteStore.addToFavorite(this.props.entityName, this.props.etalonId)
                    .then(() => {
                        this.forceUpdate();
                    });
            }
        };
    }

    render () {
        const className = [styles.favoriteIcon];
        const {isLarge} = this.props;
        const isFavorite = this.props.favoriteStore.isFavorite(this.props.entityName, this.props.etalonId);

        if (isFavorite) {
            className.push(styles.isFavorite);
        }

        return (
            <span className={className.join(' ')} onClick={this.handleClick(isFavorite)}>
                <Icon
                    name={isFavorite ? 'filledFavorite' : 'favorite'}
                    size={isLarge ? SIZE.LARGE : SIZE.MIDDLE}
                    intent={isFavorite ? IconIntent.INFO : IconIntent.DEFAULT}
                    title={isFavorite ? i18n.t('module.data>search.table>favoriteTooltipCancel') :
                        i18n.t('module.data>search.table>favoriteTooltipAdd')}
                />
            </span>
        );
    }
}
