import * as React from 'react';
import {i18n, Res, ResourceManager, Right, UnidataConfig} from '@unidata/core-app';
import {observer} from 'mobx-react';
import {PaginationStore} from '../../store/PaginationStore';
import {SearchResultStore} from '../../store/SearchResultStore';
import {values} from 'mobx';

interface IProps {
    searchResultStore: SearchResultStore;
    paginationStore: PaginationStore;
}

@observer
export class HeadOfTable extends React.Component<IProps> {
    render () {
        const store = this.props.paginationStore;
        const searchResultStore = this.props.searchResultStore;
        const checkedRecordsCount = searchResultStore.checkedRecords.length; // values(searchResultStore.checkedRecords).filter((value) => value === true).length;
        const checked = searchResultStore.checkAll === true || checkedRecordsCount > 0;
        const count = searchResultStore.checkAll === true ? store.totalRecords : checkedRecordsCount;
        const isBulkOperator = ResourceManager.userHasRight(Res.BULK_OPERATIONS_OPERATOR, Right.READ);
        const maxBulkRecords = UnidataConfig.getBulkSelectionLimit();

        return (
            <React.Fragment>
                {i18n.t('module.data>search.table>result', {count: store.totalRecords})}
                {checked && (
                    <React.Fragment>
                        <span style={{fontWeight: 'normal'}}> | {i18n.t('module.data>search.table>find')}</span>
                        {count}
                        {isBulkOperator === false && count > maxBulkRecords && (
                            <span>&nbsp; ({i18n.t('module.data>search.table>max')} {maxBulkRecords})</span>
                        )}
                    </React.Fragment>
                )}
            </React.Fragment>
        );
    }
}
