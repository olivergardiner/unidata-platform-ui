/**
 * Column header with sorting
 *
 * @author: Ilya Braujer
 * @date: 2019-10-23
 */
import * as React from 'react';
import {IDisplayAttr} from '@unidata/meta';
import {observer} from 'mobx-react';
import {SearchResultStore} from '../../../../store/SearchResultStore';

import * as styles from './head.m.scss';
import {i18n} from '@unidata/core-app';
import {Icon, Tooltip} from '@unidata/uikit';
import {IconName} from '@unidata/icon';
import {RelType} from '@unidata/types';

interface IProps {
    searchResultStore: SearchResultStore;
    attributeSettings: IDisplayAttr;
}

const icons: {[key in RelType]: IconName} = {
    [RelType.ManyToMany]: 'site-map',
    [RelType.Contains]: 'share3',
    [RelType.References]: 'link2',
    [RelType.Incoming]: 'link'
};

const tooltipText = (relType: RelType) => {
    switch (relType) {
        case RelType.ManyToMany:
            return i18n.t('relation>m2mIconTooltipText');
        case RelType.Contains:
            return i18n.t('relation>containsIconTooltipText');
        case RelType.References:
            return i18n.t('relation>referenceIconTooltipText');
    }

    return '';
};

@observer
export class Head extends React.Component<IProps> {
    render () {
        const attributeSettings = this.props.attributeSettings;

        return (
            <div className={styles.headContainer} data-qaid={attributeSettings.name}>
                <div className={styles.text}>
                    {attributeSettings.isRelation === true && attributeSettings.relType !== undefined && (
                        <Tooltip overlay={tooltipText(attributeSettings.relType)}>
                            <span>
                                <Icon name={icons[attributeSettings.relType]}/>
                                &nbsp; &nbsp;
                            </span>
                        </Tooltip>
                    )}
                    {attributeSettings.lookupEntityType !== '' && (
                        <Tooltip overlay={i18n.t('module.data>search.table>relLink')}>
                            <span>
                                <Icon name={'link'}/>
                                &nbsp; &nbsp;
                            </span>
                        </Tooltip>
                    )}
                    {attributeSettings.displayName}
                </div>
            </div>
        );
    }
}
