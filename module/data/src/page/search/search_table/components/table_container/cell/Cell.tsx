import * as React from 'react';
import {SearchResultStore} from '../../../../store/SearchResultStore';
import {IDisplayAttr} from '@unidata/meta';
import {observer} from 'mobx-react';
import {SearchHitUtil} from '../../../../../../utils/SearchHitUtil';
import {CellRelation} from './CellRelation';
import {CellAttribute} from './CellAttribute';
import {CellLookup} from './lookup/CellLookup';
import {UeModuleReactComponent, UeModuleTypeComponent, ueModuleManager} from '@unidata/core';
import {SearchHit} from '@unidata/core-app';

interface IProps {
    entityName: string;
    attributeSettings: IDisplayAttr;
    searchResultStore: SearchResultStore;
    rowIndex: number;
}

@observer
export class Cell extends React.Component<IProps> {
    render () {
        const record: SearchHit = this.props.searchResultStore.records[this.props.rowIndex];
        /**
         * Customization for a cell
         */
        const ueCellList = ueModuleManager.getModulesByType(UeModuleTypeComponent.RENDER_CELL);

        const cellTransform = (ueCellList || []).find((item: UeModuleReactComponent<UeModuleTypeComponent.RENDER_CELL>) => {
            return item.default.resolver === undefined || item.default.resolver(this.props) === true;
        });

        if (cellTransform !== undefined) {
            const Component: any = (cellTransform as UeModuleReactComponent<UeModuleTypeComponent.RENDER_CELL>).default.component;

            try {
                return (
                    <Component
                        record={record}
                        attributeSettings={this.props.attributeSettings}
                        entityName={this.props.entityName}
                    />
                );
            } catch (e) {
                console.error(`Could not use custom component with type '${UeModuleTypeComponent.RENDER_CELL}'`, e);
            }
        }

        if (!record) {
            return null;
        }

        /**
         * Displaying relationship links
         */
        if (this.props.attributeSettings.isRelation === true) {
            return (
                <CellRelation
                    record={record}
                    attributeSettings={this.props.attributeSettings}
                    relations={this.props.searchResultStore.relations}
                />
            );
        }

        /**
         * lookupEntity
         */
        if (this.props.attributeSettings.lookupEntityType !== '') {
            const lookup = SearchHitUtil.getFieldExtended(record, this.props.attributeSettings.name);

            if (lookup && lookup.length && lookup[0]) {
                return (
                    <CellLookup
                        lookup={lookup}
                        record={record}
                        attributeSettings={this.props.attributeSettings}
                        entityName={this.props.entityName}
                    />
                );
            }
        }

        /**
         * Displaying simple attributes
         */
        return (
            <CellAttribute
                record={record}
                attributeSettings={this.props.attributeSettings}
                entityName={this.props.entityName}
            />
        );
    }
}
