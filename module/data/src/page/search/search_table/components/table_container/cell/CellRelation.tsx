import * as React from 'react';
import {SearchResultStore} from '../../../../store/SearchResultStore';
import {IDisplayAttr} from '@unidata/meta';
import {SearchHitUtil} from '../../../../../../utils/SearchHitUtil';
import {SearchHit} from '@unidata/core-app';
import {LinksCount} from './links_count/LinksCount';

interface IProps {
    relations: SearchResultStore['relations'];
    record: SearchHit;
    attributeSettings: IDisplayAttr;
}

export const CellRelation: React.FC<IProps> = (props) => {
    const etalonId = SearchHitUtil.getFieldValues<string>(props.record, '$etalon_id')[0];
    const from = SearchHitUtil.getFieldValues<string>(props.record, '$from')[0];
    const to = SearchHitUtil.getFieldValues<string>(props.record, '$to')[0];

    const relation = props.relations.find((item) => {
        return item.relName === props.attributeSettings.name &&
            item.etalonId === etalonId &&
            item.validFrom === from &&
            item.validTo === to;
    });

    if (relation === undefined) {
        return null;
    }

    const total = relation.total;

    return (
        <React.Fragment>
            {relation.data.map((item, index) => {
                return (
                        <a
                            key={`rel_item_${index}`}
                            href={`#/dataview?section=dataview|dataSearch?entityName=${item.relName}|etalon?etalonId=${item.etalonIdTo}`}
                            target={'_blank'}
                            onClick={(e: React.SyntheticEvent<HTMLAnchorElement>) => e.stopPropagation()}
                            data-qaid={etalonId}
                        >
                            {item.etalonDisplayNameTo}
                        </a>
                );
            })}
            {total > relation.data.length && (
                <LinksCount count={relation.data.length} total={total}/>
            )}
        </React.Fragment>
    );
};
