import * as React from 'react';
import {SearchResultStore} from '../../../store/SearchResultStore';
import {MetaRecordStore} from '../../../store/MetaRecordStore/MetaRecordStore';
import {Table, CellInfo, Column} from '@unidata/uikit';
import {setColumn, setFavoriteCheckbox} from './columnSettings';
import {i18n} from '@unidata/core-app';
import {FromToAttributeItem} from '../../../search_tile/attribute/FromToAttributeItem';
import {observer} from 'mobx-react';
import {DateGranularityMode} from '@unidata/types';
import {IDisplayAttr} from '@unidata/meta';

interface IProps {
    searchResultStore: SearchResultStore;
    metaRecordStore: MetaRecordStore;
    withFavorite?: boolean;
}

@observer
export class TableContainer extends React.Component<IProps> {

    private renderColumns = () => {
        const searchResultStore = this.props.searchResultStore;
        const metaRecordStore = this.props.metaRecordStore;
        const favoriteStore = metaRecordStore.getFavoriteStore();

        const columns = metaRecordStore.attributes
            .sort(metaRecordStore.columnsSort)
            .reduce((result: Column[], item) => {
                if (item.isShow) {
                    if (item.children && item.children.length > 0) {
                        result.push({
                            Header: item.displayName,
                            accessor: item.name,
                            id: item.name,
                            columns: item.children
                                .sort(metaRecordStore.columnsSort)
                                .reduce((result: Column[], childItem: IDisplayAttr) => {
                                    if (childItem.isShow) {
                                        result.push(setColumn(metaRecordStore.currentEntity.entityName, childItem, searchResultStore));
                                    }

                                    return result;
                                }, [])
                        });
                    } else {
                        result.push(setColumn(metaRecordStore.currentEntity.entityName, item, searchResultStore));
                    }
                }

                return result;
            }, [
                setFavoriteCheckbox(searchResultStore, metaRecordStore, favoriteStore, this.props.withFavorite !== false)
            ]);

        if (searchResultStore.hasPeriod) {
            columns.push({
                Header: () => {
                    return i18n.t('module.data>search.table>relevancyPeriod');
                },
                accessor: 'relevancyPeriod',
                id: 'relevancyPeriod',
                width: 300,
                sortable: false,
                resizable: false,
                Cell: (props: CellInfo) => {
                    const searchHit = searchResultStore.records[props.index];

                    return (
                        <FromToAttributeItem record={searchHit} granularity={DateGranularityMode.DATETIME}/>
                    );
                }
            });
        }

        return columns;
    };

    onRowChecked = (rowIndex: number, isChecked: boolean) => {
        const searchResultStore = this.props.searchResultStore;

        if (isChecked === true) {
            searchResultStore.checkRow(rowIndex);
        } else {
            searchResultStore.uncheckRow(rowIndex);
        }
    };

    onAllChecked = (isChecked: boolean) => {
        this.props.searchResultStore.toggleCheckAll(isChecked);
    };

    onSort = (columnId: string) => {
        const attribute = this.props.metaRecordStore.attributes.find((item) => item.name === columnId);

        if (attribute) {
            const type = attribute.valueType;

            if (attribute.isArray === true) {
                return;
            }

            if (type !== undefined) {
                this.props.searchResultStore.sortByField(columnId, type);
            }

            this.props.searchResultStore.getRecords();
        }
    };

    render () {

        const metaRecordStore = this.props.metaRecordStore;
        const searchResultStore = this.props.searchResultStore;

        if (metaRecordStore.attributes.length === 0) {
            return null;
        }

        const sort = searchResultStore.sortFields.reduce<any>((result, sortItem) => {
            result[sortItem.field] = sortItem.order;

            return result;
        }, {});

        return (
            <Table
                columns={this.renderColumns()}
                data={searchResultStore.records}
                hasHeaderBg={true}
                isStriped={true}
                isRowsCheckable={true}
                onRowCheck={this.onRowChecked}
                onAllCheck={this.onAllChecked}
                sort={sort}
                onSort={this.onSort}
            />
        );
    }
}
