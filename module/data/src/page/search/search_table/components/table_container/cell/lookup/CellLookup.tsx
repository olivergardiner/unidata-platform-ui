import * as React from 'react';
import {i18n, SearchHit} from '@unidata/core-app';
import {IDisplayAttr} from '@unidata/meta';
import {SearchHitUtil} from '../../../../../../../utils/SearchHitUtil';
import {IExtendedValue} from '../../../../../../../service/operation/search/ReadDataList/ReadDataListType';
import * as styles from './cellLookup.m.scss';
import {Tooltip, PLACEMENT, Icon} from '@unidata/uikit';
import {LinksCount} from '../links_count/LinksCount';

interface IProps {
    record: SearchHit;
    lookup: IExtendedValue[];
    attributeSettings: IDisplayAttr;
    entityName: string;
}

const maxCountView = 5;

export const CellLookup: React.FC<IProps> = (props) => {
    const etalonId = SearchHitUtil.getFieldValues<string>(props.record, '$etalon_id')[0];
    const from = SearchHitUtil.getFieldValues<string>(props.record, '$from')[0];
    const to = SearchHitUtil.getFieldValues<string>(props.record, '$to')[0];
    const total = props.lookup.length;

    return (
        <>
            {props.lookup.slice(0, maxCountView).map((lookupItem) => {
                const displayableValue = lookupItem.displayValue.getValue();

                let etalon = etalonId;
                let etalonLookup = lookupItem.linkedEtalonId.getValue();

                if (from) {
                    etalon = `${etalon}&from=${from}`;
                    etalonLookup = `${etalonLookup}&from=${from}`;
                }

                if (to) {
                    etalon = `${etalon}&to=${to}`;
                }

                const recordHref = `#/dataviewlight/${props.entityName}/${etalonId}`;
                const lookupHref = `#/dataview?section=dataview|dataSearch?entityName=${props.attributeSettings.lookupEntityType}|etalon?etalonId=${etalonLookup}`;

                return (
                    <div key={`${etalon}_${etalonLookup}`} className={styles.container} data-qaid={etalonId}>
                        <div className={styles.text}>
                            <Tooltip
                                placement={PLACEMENT.TOP_START}
                                mouseEnterDelay={600}
                                overlay={(
                                    <span>
                                {i18n.t('module.data>search.table>value')}: {displayableValue}
                                        <br/>
                                        {i18n.t('module.data>search.table>columnName')}: {props.attributeSettings.displayName}
                                        <br/>
                                        {i18n.t('module.data>search.table>description')}: {props.attributeSettings.description}
                                </span>
                                )}
                            >
                                <a
                                    target={'_blank'}
                                    href={recordHref}
                                    className={styles.greyHref}
                                >
                                    {displayableValue}
                                </a>
                            </Tooltip>
                        </div>
                        <Tooltip
                            placement={PLACEMENT.RIGHT}
                            mouseEnterDelay={200}
                            mouseLeaveDelay={0}
                            overlay={i18n.t('module.data>search.table>goToRelLink')}
                        >
                            <a
                                target={'_blank'}
                                href={lookupHref}
                                className={[styles.icon, styles.greyHref].join(' ')}
                            >
                                <Icon name={'launch'}/>
                            </a>
                        </Tooltip>
                    </div>
                );
            })}
            {total > maxCountView && (
                <LinksCount count={maxCountView} total={total}/>
            )}
        </>
    );
};
