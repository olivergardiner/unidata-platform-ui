import * as React from 'react';
import {i18n, SearchHit} from '@unidata/core-app';
import {greyHref} from './lookup/cellLookup.m.scss';
import {PLACEMENT, Tooltip, TRIGGER} from '@unidata/uikit';
import {SearchHitUtil} from '../../../../../../utils/SearchHitUtil';
import {FormatUtil} from '../../../../utils/FormatUtil';
import {AliasCodeAttribute, ArrayAttribute, CodeAttribute, SimpleAttribute, IDisplayAttr} from '@unidata/meta';
import * as styles from './cellAttribute.m.scss';

interface IProps {
    record: SearchHit;
    attributeSettings: IDisplayAttr;
    entityName: string;
}

export class CellAttribute extends React.Component<IProps> {

    containerElement = React.createRef<HTMLDivElement>();
    itemsContainerElement = React.createRef<HTMLDivElement>();
    moreElement = React.createRef<HTMLDivElement>();

    componentDidMount (): void {
        if (this.props.attributeSettings.metaAttribute instanceof ArrayAttribute) {
            this.calculateView();

            window.addEventListener('resize', this.calculateView);
        }
    }

    componentWillUnmount (): void {
        if (this.props.attributeSettings.metaAttribute instanceof ArrayAttribute) {
            window.removeEventListener('resize', this.calculateView);
        }
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<{}>, snapshot?: any): void {
        if (this.props.attributeSettings.metaAttribute instanceof ArrayAttribute) {
            this.calculateView();
        }
    }

    /**
     * Calculates each span for an array attribute"
     */
    calculateView = () => {
        if (this.itemsContainerElement.current && this.containerElement.current && this.moreElement.current) {
            let width =  this.containerElement.current.offsetWidth; // the total width of the cell
            let allowNext = true; // flag for displaying the next element

            const childrenLength = this.itemsContainerElement.current.children.length;

            // resets the text " more ..."
            this.moreElement.current.innerText = '';

            // passing through all values from the array
            for (let i = 0; i < childrenLength; i++) {
                const elem = this.itemsContainerElement.current.children.item(i) as HTMLDivElement;

                // make it visible for correct width calculation
                elem.style.display = 'block';
                elem.style.overflow = 'visible';

                // if you don't want to display it, hide it
                if (allowNext === false) {
                    elem.style.display = 'none';
                    continue;
                }

                const nextElem = this.itemsContainerElement.current.children.item(i + 1) as HTMLDivElement | null;

                width = width - elem.offsetWidth;

                // width 60-experimental value, this is the width of the "more..." label and part of the previous span.
                if (width < 60 || (nextElem !== null && width < nextElem.offsetWidth) || nextElem === null) {
                    // we determined that we are going beyond the width

                    // hide all subsequent elements, trim the current one (if necessary)
                    allowNext = false;
                    elem.style.overflow = 'hidden';
                    elem.style.textOverflow = 'ellipsis';

                    let moreCount = childrenLength - (i + 1);

                    // adding the label " more..."
                    if (moreCount > 0) {
                        this.moreElement.current.innerText = `${i18n.t('more')} ${moreCount}`;
                    }
                }
            }
        }
    };

    render () {
        const props = this.props;
        const etalonId = SearchHitUtil.getFieldValues<string>(props.record, '$etalon_id')[0];
        const from = SearchHitUtil.getFieldValues<string>(props.record, '$from')[0];
        const to = SearchHitUtil.getFieldValues<string>(props.record, '$to')[0];

        let etalon = etalonId;

        if (from) {
            etalon = `${etalon}&from=${from}`;
        }

        if (to) {
            etalon = `${etalon}&to=${to}`;
        }

        if (
            !(props.attributeSettings.metaAttribute instanceof SimpleAttribute ||
                props.attributeSettings.metaAttribute instanceof CodeAttribute ||
                props.attributeSettings.metaAttribute instanceof AliasCodeAttribute ||
                props.attributeSettings.metaAttribute instanceof ArrayAttribute)
        ) {
            console.error('try to render attribute without meta', props);

            return null;
        }

        const metaAttribute = props.attributeSettings.metaAttribute;
        const recordHref = `#/dataviewlight/${props.entityName}/${etalonId}`;
        const values = SearchHitUtil.getFieldValues(props.record, props.attributeSettings.name);

        if (values.length === 0) {
            return null;
        }

        const firstValue = FormatUtil.formatByAttribute(values[0], metaAttribute);

        const listValue = values.map((value) => {
            return FormatUtil.formatByAttribute(value, metaAttribute);
        });

        let displayableValue = metaAttribute instanceof ArrayAttribute ? `[ ${listValue.join(' | ')} ]` : firstValue;

        if (!displayableValue) {
            return null;
        }

        return (
            <Tooltip
                placement={PLACEMENT.TOP_START}
                mouseEnterDelay={600}
                mouseLeaveDelay={100}
                trigger={TRIGGER.HOVER_TARGET_ONLY}
                overlay={(
                    <span>
                    {i18n.t('module.data>search.table>value')}: {displayableValue}
                        <br/>
                        {i18n.t('module.data>search.table>columnName')}: {props.attributeSettings.displayName}
                        <br/>
                        {i18n.t('module.data>search.table>description')}: {props.attributeSettings.description}
                </span>
                )}
            >
                <a
                    className={greyHref}
                    target={'_blank'}
                    href={recordHref}
                    data-qaid={etalonId}
                >
                    {metaAttribute instanceof ArrayAttribute ?
                        (<div className={styles.container} ref={this.containerElement}>
                                <div className={styles.arrContainer} ref={this.itemsContainerElement}>
                                    {listValue.map((valueItem, index) => {
                                        return (
                                            <div className={styles.arrItem} key={index}>
                                                {valueItem}
                                            </div>
                                        );
                                    })}
                                </div>
                                <div ref={this.moreElement} className={styles.more}/>
                            </div>
                        ) :
                        firstValue}
                </a>
            </Tooltip>
        );
    }
}
