import * as React from 'react';
import {i18n} from '@unidata/core-app';

import * as styles from './linksCount.m.scss';

interface IProps {
    count: number;
    total: number;
}

export class LinksCount extends React.PureComponent<IProps> {
    render () {
        const {count, total} = this.props;

        return (
            <div className={styles.container}>
                {i18n.t('relation>showingOf', {count, total})}
            </div>
        );
    }
}
