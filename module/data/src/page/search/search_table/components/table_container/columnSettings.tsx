import * as React from 'react';
import {CellInfo, Column} from '@unidata/uikit';
import {MetaRecordStore} from '../../../store/MetaRecordStore/MetaRecordStore';
import {IDisplayAttr} from '@unidata/meta';
import {Cell} from './cell/Cell';
import {SearchResultStore} from '../../../store/SearchResultStore';
import {Head} from './head/Head';
import {SearchHitUtil} from '../../../../../utils/SearchHitUtil';
import {FavoriteCheckbox} from '../favorite_checkbox/FavoriteCheckbox';
import {SearchHit} from '@unidata/core-app';
import {FavoriteStore} from '../../../../../store/FavoriteStore';

export const setColumn = (entityName: string, item: IDisplayAttr, searchResultStore: SearchResultStore) => {
    const CellRender = (props: CellInfo) => {
        return <Cell
            entityName={entityName}
            attributeSettings={item}
            searchResultStore={searchResultStore}
            rowIndex={props.index}
        />;
    };

    return {
        Header: (<Head attributeSettings={item} searchResultStore={searchResultStore}/>),
        accessor: item.name,
        id: item.name,
        minWidth: (item.isRelation || item.lookupEntityType !== '') ? 250 : 100,
        Cell: CellRender
    };
};

export const setFavoriteCheckbox = (
    searchResultStore: SearchResultStore,
    metaRecordStore: MetaRecordStore,
    favoriteStore: FavoriteStore,
    isVisible?: boolean
): Column => {
    const CellFavoriteCheckboxRender = (props: CellInfo) => {
        const record: SearchHit = searchResultStore.records[props.index];

        if (!record) {
            return null;
        }

        const etalonId = SearchHitUtil.getFieldValues<string>(record, '$etalon_id')[0];

        return (
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', height: '25px'}}>
                <FavoriteCheckbox
                    favoriteStore={favoriteStore}
                    entityName={metaRecordStore.currentEntity.entityName}
                    etalonId={etalonId}
                />
            </div>
        );
    };

    return {
        accessor: 'favorite',
        width: 24,
        sortable: false,
        resizable: false,
        Cell: (isVisible === false) ? null : CellFavoriteCheckboxRender
    };
};
