import * as React from 'react';
import {SearchResultStore} from '../../../store/SearchResultStore';
import {observer} from 'mobx-react';
import * as styles from './tableLoading.m.scss';
import {Icon} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';

interface IProps {
    searchResultStore: SearchResultStore;
}

@observer
export class TableLoading extends React.Component<IProps> {
    render () {

        if (this.props.searchResultStore.isLoading) {
            return (
                <div className={styles.loading}>
                    {i18n.t('common:loading')}
                    &nbsp;
                    <Icon name="loading" />
                </div>
            );
        }

        return null;
    }
}
