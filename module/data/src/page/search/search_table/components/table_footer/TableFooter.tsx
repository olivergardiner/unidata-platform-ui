import * as React from 'react';

import {Table} from '@unidata/uikit';
import {observer} from 'mobx-react';
import {PaginationStore} from '../../../store/PaginationStore';
import {SearchResultStore} from '../../../store/SearchResultStore';

interface IProps {
    paginationStore: PaginationStore;
    searchResultStore: SearchResultStore;
}

@observer
export class TableFooter extends React.Component<IProps> {
    render () {
        const searchStore = this.props.searchResultStore;
        const store = this.props.paginationStore;

        return (
            <Table.FooterPanel
                currentPage={store.currentPage}
                pageSize={store.pageSize}
                totalPages={store.totalPages}
                totalRecords={store.totalRecords}
                reload={store.getList}
                setPage={store.setPage}
                setPageSize={store.setPageSize}
                isDisabled={searchStore.records.length === 0}
            />
        );
    }
}
