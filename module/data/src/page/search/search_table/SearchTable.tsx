/**
 * Component for displaying a table view of the data search result
 *
 * @author: Ilya Brauer
 * @date: 2019-10-08
 */

import * as React from 'react';
import {CardPanel, InitTable} from '@unidata/uikit';
import {SearchResultStore} from '../store/SearchResultStore';
import {MetaRecordStore} from '../store/MetaRecordStore/MetaRecordStore';
import {RecordAttributes} from './components/record_attributes/RecordAttributes';
import {TableFooter} from './components/table_footer/TableFooter';
import {HeadOfTable} from './components/HeadOfTable';
import {TableLoading} from './components/table_loading/TableLoading';

import * as styles from './searchTable.m.scss';
import {TableContainer} from './components/table_container/TableContainer';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';

interface IProps {
    searchResultStore: SearchResultStore;
    metaRecordStore: MetaRecordStore;
    withFavorite?: boolean;
    onClickToSelectEntity?: () => void;
}

@observer
export class SearchTable extends React.Component<IProps> {
    render () {
        const metaRecordStore = this.props.metaRecordStore;
        const searchStore = this.props.searchResultStore;

        if (metaRecordStore.currentEntity.entityName === '') {
            return (
                <InitTable
                    mainText={i18n.t('module.data>search.table>preSearch>choose')}
                    mainTextBlue={i18n.t('module.data>search.table>preSearch>entity')}
                    subText={i18n.t('module.data>search.table>preSearch>subText')}
                    onClick={this.props.onClickToSelectEntity}
                />
            );
        }

        return (
            <CardPanel
                title={<HeadOfTable paginationStore={searchStore.paginationStore} searchResultStore={searchStore}/>}
                noBodyPadding={true}
                extraButtons={(<RecordAttributes metaRecordStore={metaRecordStore}
                                                 searchResultStore={searchStore}/>)}
            >
                <div className={styles.tableLayout}>
                    <TableContainer
                        searchResultStore={searchStore}
                        metaRecordStore={metaRecordStore}
                        withFavorite={this.props.withFavorite}
                    />
                    <TableFooter
                        paginationStore={searchStore.paginationStore}
                        searchResultStore={searchStore}
                    />
                    <TableLoading searchResultStore={searchStore}/>
                </div>
            </CardPanel>
        );
    }
}
