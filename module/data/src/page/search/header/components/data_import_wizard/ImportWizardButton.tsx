/**
 * Data import wizard
 *
 * @author Brauer Ilya
 * @date 2020-03-12
 */

import * as React from 'react';
import {Button, INTENT, Modal, PLACEMENT, Wizard, WizardStep} from '@unidata/uikit';
import {Dialog, i18n} from '@unidata/core-app';
import {MetaRecordStore} from '../../../store/MetaRecordStore/MetaRecordStore';
import {Settings} from './step/Settings';
import {PrepareData} from './step/PrepareData';
import {Import} from './step/Import';
import {ImportWizardButtonStore} from './ImportWizardButtonStore';
import * as styles from './importWizardButton.m.scss';
import {ImportService} from '../../../../../service/ImportService';
import {UploadFile} from 'antd/es/upload/interface';
import {RouteComponentProps, withRouter} from 'react-router';

interface IRouteProps {
    entityName?: string;
}


type IProps = RouteComponentProps<IRouteProps> & {
    metaRecordStore: MetaRecordStore;
    isDisabled: boolean;
};

interface IState {
    isOpenModal: boolean;
}

class ImportWizardComponent extends React.Component<IProps, IState> {

    state: IState = {
        isOpenModal: false
    };

    importStore = new ImportWizardButtonStore();

    componentDidMount (): void {
        this.setEntityFromMetaRecordStore();

        this.importStore.loadSystemSources();
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<{}>, snapshot?: any): void {
        if (prevProps.match.params.entityName !== this.props.match.params.entityName) {
            this.setEntityFromMetaRecordStore();
        }
    }

    setEntityFromMetaRecordStore = () => {
        if (this.props.match.params.entityName !== undefined) {
            const metaRecordStore = this.props.metaRecordStore;

            metaRecordStore.setCurrentEntityByName(this.props.match.params.entityName)
                .then(() => {
                    const metaRecordStore = this.props.metaRecordStore;
                    const entityName = metaRecordStore.currentEntity.entityName;
                    const entityDisplayName = metaRecordStore.currentEntity.entityDisplayName;

                    if (entityName !== '') {
                        this.importStore.setEntityName(entityName);
                        this.importStore.setEntityDisplayName(entityDisplayName);
                    }
                });
        } else {
            this.importStore.setEntityName('');
            this.importStore.setEntityDisplayName('');
        }
    };

    toggleModal = () => {
        this.setState((prevState) => {
            return {
                isOpenModal: !prevState.isOpenModal
            };
        });
    };

    get steps () {
        const steps: WizardStep[] = [
            {
                name: 'setting',
                label: i18n.t('data>import>steps>settings>name'),
                component: <Settings
                    entityNodes={this.props.metaRecordStore.entityTree.slice()}
                    importStore={this.importStore}
                />
            },
            {
                name: 'prepareData',
                label: i18n.t('data>import>steps>prepareData>name'),
                component: <PrepareData
                    importStore={this.importStore}
                />
            },
            {
                name: 'import',
                label: i18n.t('data>import>steps>import>name'),
                component: <Import
                    importStore={this.importStore}
                />
            }
        ];

        return steps;
    }

    onSubmitWizard = () => {
        const importStore = this.importStore;

        const importParams = JSON.stringify({
            sourceSystem: importStore.sourceName,
            entityName: importStore.entityName,
            mergeWithPreviousVersion: importStore.isIgnoreEmpty
        });

        ImportService.importData(importParams, importStore.file as UploadFile)
            .then(() => {
                this.toggleModal();

                Dialog.showSimpleMessage(i18n.t('data>import>afterImportSuccess'));
            });

    };

    render () {
        return (
            <React.Fragment>
                <Button
                    leftIcon={'import'}
                    name={'import'}
                    title={i18n.t('module.data>search>import')}
                    isRound={true}
                    onClick={this.toggleModal}
                    tooltipPlacement={PLACEMENT.BOTTOM_END}
                    isDisabled={this.props.isDisabled}
                />
                <Modal
                    isOpen={this.state.isOpenModal}
                    shouldCloseOnClickOutside={false}
                    onClose={this.toggleModal}
                    header={i18n.t('data>import>modalHeader')}
                    noBodyPadding={true}
                >
                    <div className={styles.wizardContainer}>
                        <Wizard steps={this.steps} onSubmit={this.onSubmitWizard}/>
                    </div>
                </Modal>
            </React.Fragment>
        );
    }
}

export const ImportWizardButton = withRouter(ImportWizardComponent);
