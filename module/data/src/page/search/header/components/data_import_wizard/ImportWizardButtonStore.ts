/**
 * Store of data import wizard
 *
 * @author Brauer Ilya
 * @date 2020-03-12
 */

import {action, observable} from 'mobx';
import {SourceSystemService} from '@unidata/meta';
import {IOption} from '@unidata/uikit';
import {UploadFile} from 'antd/es/upload/interface';

export class ImportWizardButtonStore {
    @observable
    public entityName: string = '';

    @observable
    public entityDisplayName: string = '';

    @observable
    public adminSystemName: string = '';

    @observable
    public sourceName: string = '';

    @observable
    public isIgnoreEmpty: boolean = true;

    @observable
    public file: UploadFile | null = null;

    @observable.shallow
    public systemSources: IOption[] = [];

    @action
    public setEntityName = (entityName: string) => {
        this.entityName = entityName;
    };

    @action
    public setEntityDisplayName = (entityDisplayName: string) => {
        this.entityDisplayName = entityDisplayName;
    };

    @action
    public setIgnoreEmpty = (isIgnore: boolean) => {
        this.isIgnoreEmpty = isIgnore;
    };

    @action
    public handleUploadChange = (file: UploadFile | null) => {
        this.file = file;
    };

    @action
    public setSourceName = (sourceName: string) => {
        this.sourceName = sourceName;
    };

    @action
    public setAdminSystemName = (adminSystemName: string) => {
        this.adminSystemName = adminSystemName;
    };

    @action
    public setSystemSources = (systemSources: IOption[]) => {
        this.systemSources = systemSources;
    };

    public loadSystemSources = () => {
        return SourceSystemService.getSourceSystem()
            .then((sourceSystem) => {
                const options = sourceSystem.sourceSystem.map((sourceItem) => {
                    return {
                        value: sourceItem.name.getValue(),
                        title: sourceItem.name.getValue()
                    };
                });

                this.setSystemSources(options);
                this.setAdminSystemName(sourceSystem.adminSystemName);
            });
    };
}
