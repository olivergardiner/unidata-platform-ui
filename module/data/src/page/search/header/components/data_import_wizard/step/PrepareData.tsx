/**
 * Data import wizard-data preparation step
 *
 * @author Brauer Ilya
 * @date 2020-03-12
 */

import * as React from 'react';
import {Button, INTENT, Wizard} from '@unidata/uikit';
import * as styles from '../importWizardButton.m.scss';
import {ImportWizardButtonStore} from '../ImportWizardButtonStore';
import {observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {ImportService} from '../../../../../../service/ImportService';

interface IProps {
    importStore: ImportWizardButtonStore;
}

@observer
export class PrepareData extends React.Component<IProps> {
    downloadTemplate = () => {
        ImportService.downloadTemplate(this.props.importStore.entityName);
    };

    render () {
        return (
            <>
                <Wizard.Navigation/>
                <div className={styles.wizardBody}>
                    <p>
                        {i18n.t('data>import>steps>prepareData>text')}
                    </p>
                    <Button
                        intent={INTENT.PRIMARY}
                        data-qaid={'downloadfile'}
                        onClick={this.downloadTemplate}
                    >
                        {i18n.t('data>import>steps>prepareData>downloadTemplate')}
                    </Button>
                </div>
                <Wizard.Footer/>
            </>
        );
    }
}
