/**
 * Data import wizard-configuration step
 *
 * @author Brauer Ilya
 * @date 2020-03-12
 */

import * as React from 'react';
import {Field, SelectTree, ITreeNode, Wizard, IOption} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import {ImportWizardButtonStore} from '../ImportWizardButtonStore';
import {observer} from 'mobx-react';
import * as styles from '../importWizardButton.m.scss';

interface IProps {
    entityNodes: ITreeNode[];
    importStore: ImportWizardButtonStore;
}

@observer
export class Settings extends React.Component<IProps, any> {
    onSelectEntity = (node: ITreeNode) => {
        this.props.importStore.setEntityName(node.key);
        this.props.importStore.setEntityDisplayName(node.row.displayName);
    };

    onSelectSource = (name: string, value: string) => {
        this.props.importStore.setSourceName(value);

        if (value === this.props.importStore.adminSystemName) {
            this.props.importStore.setIgnoreEmpty(true);
        }
    };

    onChangeIgnore = (name: string, value: boolean) => {
        this.props.importStore.setIgnoreEmpty(value);
    };

    render () {
        const importStore = this.props.importStore;

        const options: IOption[] = [{
            value: importStore.entityName,
            title: importStore.entityDisplayName
        }];

        const allowNext = Boolean(importStore.entityName && importStore.sourceName);

        return (
            <>
                <Wizard.Navigation allowNext={allowNext}/>
                    <div className={styles.wizardBodyPadding}>
                        <Field
                            label={i18n.t('data>import>steps>settings>chooseEntity')}
                        >
                            <SelectTree
                                data-qaid={'entity'}
                                nodes={this.props.entityNodes}
                                value={importStore.entityName}
                                onSelectNode={this.onSelectEntity}
                                columns={[{name: 'displayName', displayName: ''}]}
                                options={options}
                                placeholder={i18n.t('module.data>search.panel>selectEntity')}
                                selectedNodes={[]}
                                allowClear={false}
                            />
                        </Field>

                        <Field.Select
                            data-qaid={'sourcesystem'}
                            label={i18n.t('data>import>steps>settings>chooseSource')}
                            name={'systemSources'}
                            value={importStore.sourceName}
                            options={importStore.systemSources}
                            onSelect={this.onSelectSource}
                        />

                        <Field.Checkbox
                            data-qaid={'ignore-emty-cell'}
                            label={i18n.t('data>import>steps>settings>isIgnore')}
                            defaultChecked={importStore.isIgnoreEmpty}
                            onChange={this.onChangeIgnore}
                            disabled={importStore.sourceName === importStore.adminSystemName}
                        />
                    </div>
                <Wizard.Footer allowNext={allowNext}/>
            </>
        );
    }
}
