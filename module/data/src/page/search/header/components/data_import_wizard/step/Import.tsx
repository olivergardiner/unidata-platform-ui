/**
 * Data import wizard-import step
 *
 * @author Brauer Ilya
 * @date 2020-03-12
 */

import * as React from 'react';
import {Button, INTENT, Wizard, UploadFile, Upload} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import {ImportWizardButtonStore} from '../ImportWizardButtonStore';
import * as styles from '../importWizardButton.m.scss';
import {observer} from 'mobx-react';

interface IProps {
    importStore: ImportWizardButtonStore;
}

@observer
export class Import extends React.Component<IProps> {
    render () {
        const importStore = this.props.importStore;

        const props = {
            onRemove: (): void => {
                importStore.handleUploadChange(null);
                this.setState({fileList: []});
            },
            beforeUpload: (file: UploadFile): boolean => {
                importStore.handleUploadChange(file);

                return false;
            },
            multiple: false,
            fileList: importStore.file ? [importStore.file] : [],
            accept: '.xlsx'
        };

        return (
            <>
                <Wizard.Navigation/>
                    <div className={styles.wizardBody}>
                        <p dangerouslySetInnerHTML={{__html: i18n.t('data>import>steps>import>text')}}/>
                        <Upload {...props}>
                            <Button
                                intent={INTENT.PRIMARY}
                                data-qaid={'selectfile'}
                                leftIcon={'upload'}
                            >
                                {i18n.t('selectFile')}
                            </Button>
                        </Upload>
                    </div>
                <Wizard.Footer isSubmitStep={true} allowSubmit={Boolean(importStore.file)}/>
            </>
        );
    }
}
