﻿/**
 * Dropdown component with bulk operations
 *
 * @author Brauer Ilya
 * @date 2020-03-24
 */

import * as React from 'react';
import {Button, DropDown, PLACEMENT, Tooltip, TRIGGER} from '@unidata/uikit';
import {BulkWizardWnd} from '../../../wizard/BulkWizardWnd';
import {BulkDataRecordService} from '../../../../../service/BulkDataRecordService';
import {i18n, Res, ResourceManager, Right, UnidataConfig} from '@unidata/core-app';
import {IBulkDataRecordOperation} from '../../../../../service/operation/bulkdatarecord/BulkReadDataOperationsOp';
import {BulkOperationType} from '../../../../../uemodule/bulkwizard/type/BulkOperationType';
import {observer} from 'mobx-react';
import {MetaRecordStore} from '../../../store/MetaRecordStore/MetaRecordStore';
import {SearchResultStore} from '../../../store/SearchResultStore';
import {SearchPanelStore} from '../../../store/search_panel_store/SearchPanelStore';
import {SearchTermsRequest} from '../../../../../service/operation/search/ReadDataList/ReadDataListType';
import {SearchHitUtil} from '../../../../../utils/SearchHitUtil';

interface IProps {
    allChecked: boolean;
    metaRecordStore: MetaRecordStore;
    searchResultStore: SearchResultStore;
    searchPanelStore: SearchPanelStore;
}

interface IState {
    isShowModal: boolean;
    operationList: IBulkDataRecordOperation[];
    operationType?: BulkOperationType;
    operationTitle?: string;
}

@observer
export class Operations extends React.Component<IProps, IState> {

    state: IState = {
        isShowModal: false,
        operationList: []
    };

    componentDidMount (): void {
        const query = {
            searchComplexRO: this.query,
            customOperations: null // todo Brauer Ilya - why is this in the executeBulkOp request?
        };

        if (this.canBulk()) {
            BulkDataRecordService.getBulkDataRecordOperations(query)
                .then((operationList) => {
                    this.setState({operationList});
                });
        }
    }

    get query (): SearchTermsRequest & {entity: string} {
        const {metaRecordStore, searchPanelStore} = this.props;

        return {
            entity: metaRecordStore.currentEntity.entityName,
            ...searchPanelStore.getQuery()
        };
    }

    get etalonIds () {
        const {searchResultStore} = this.props;

        return this.props.allChecked === true ? [] :
            searchResultStore.checkedRecords.map((item) => {
                const record = searchResultStore.records[item];
                const etalonId = SearchHitUtil.getFieldValues<string>(record, '$etalon_id')[0];
                const from = SearchHitUtil.getFieldValues<string | null>(record, '$from')[0];
                const to = SearchHitUtil.getFieldValues<string | null>(record, '$to')[0];

                return {
                    etalonId,
                    from,
                    to
                };
            });
    }

    toggleModal = () => {
        this.setState((prevState) => {
            return {
                isShowModal: !prevState.isShowModal
            };
        });
    };

    initOperation = (operation: IBulkDataRecordOperation) => () => {
        this.setState({
            isShowModal: true,
            operationTitle: operation.description,
            operationType: operation.type
        });
    };

    canBulk = () => {
        const searchResultStore = this.props.searchResultStore;
        const paginationStore = searchResultStore.paginationStore;
        const checkedRecordsCount = searchResultStore.checkedRecords.length;
        const count = searchResultStore.checkAll === true ? paginationStore.totalRecords : checkedRecordsCount;
        const isBulkOperator = ResourceManager.userHasRight(Res.BULK_OPERATIONS_OPERATOR, Right.READ);

        return isBulkOperator || count <= UnidataConfig.getBulkSelectionLimit();
    };

    render () {
        const {metaRecordStore, searchResultStore} = this.props;

        const classifiers = metaRecordStore.recordMetaData !== undefined ?
            (metaRecordStore.recordMetaData.classifiers.getValue() as string[]) :
            [];

        const queryCount = searchResultStore.paginationStore.totalRecords;

        if (!this.canBulk()) {
            return (
                <Tooltip
                    overlay={(
                        <span>
                            {i18n.t('module.data>search.table>bulkPermsError', {count: UnidataConfig.getBulkSelectionLimit()})}
                        </span>
                    )}
                >
                    <Button
                        isRound={true}
                        isDisabled={true}
                        rightIcon={'chevron-down'}
                        data-qaid='searchActions'
                    >
                        {i18n.t('module.data>search>actions')}
                    </Button>
                </Tooltip>
            );
        }

        return (
            <>
                <DropDown
                    placement={PLACEMENT.BOTTOM_END}
                    trigger={TRIGGER.CLICK}
                    data-qaid='operationList_dropdown'
                    target={(
                        <Button
                            isRound={true}
                            isDisabled={this.state.operationList.length === 0}
                            rightIcon={'chevron-down'}
                            data-qaid='searchActions'
                        >
                            {i18n.t('module.data>search>actions')}
                        </Button>
                    )}
                >
                    <React.Fragment>
                        {this.state.operationList.map((operationItem) => {
                            return (
                                <DropDown.Item
                                    key={operationItem.type}
                                    onClick={this.initOperation(operationItem)}
                                    data-qaid={operationItem.type + '_item'}
                                >
                                    {operationItem.description}
                                </DropDown.Item>
                            );
                        })}
                    </React.Fragment>
                </DropDown>

                {this.state.isShowModal &&
                this.state.operationType !== undefined &&
                this.state.operationTitle !== undefined && (
                    <BulkWizardWnd
                        entityName={metaRecordStore.currentEntity.entityName}
                        entityType={metaRecordStore.currentEntity.entityType}
                        etalonIds={this.etalonIds}
                        classifiers={classifiers}
                        query={this.query}
                        queryCount={queryCount}
                        isOpen={true}
                        onClose={this.toggleModal}
                        operationType={this.state.operationType}
                        operationTitle={this.state.operationTitle}
                    />
                )}
            </>
        );
    }
}
