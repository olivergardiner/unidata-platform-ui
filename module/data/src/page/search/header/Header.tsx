/**
 * Component for displaying the header of the search results page
 *
 * @author: Ilya Brauer
 * @date: 2019-11-08
 */

import * as React from 'react';
import {Button, INTENT, PageHeader, Tooltip} from '@unidata/uikit';
import {i18n, ResourceManager, Right} from '@unidata/core-app';
import {SearchResultStore} from '../store/SearchResultStore';
import {observer} from 'mobx-react';
import {values} from 'mobx';
import {Operations} from './components/operations/Operations';
import {MetaRecordStore} from '../store/MetaRecordStore/MetaRecordStore';
import {ImportWizardButton} from './components/data_import_wizard/ImportWizardButton';
import {SearchPanelStore} from '../store/search_panel_store/SearchPanelStore';

interface IProps {
    metaRecordStore: MetaRecordStore;
    searchResultStore: SearchResultStore;
    searchPanelStore: SearchPanelStore;
}

@observer
export class Header extends React.Component<IProps> {
    createRecord = () => {
        const {currentEntity} = this.props.metaRecordStore;

        window.open(`#/dataviewlight/${currentEntity.entityName}/new`);
    };

    render () {
        const {currentEntity} = this.props.metaRecordStore;
        const allChecked = this.props.searchResultStore.checkAll === true;
        const hasChecked = this.props.searchResultStore.checkedRecords.length > 0;
        const isEmptyCurrentEntity = (currentEntity.entityName === '');
        const isUserCanCreate = ResourceManager.userHasRight(currentEntity.entityName, Right.CREATE);
        const isUserCanUpdate = ResourceManager.userHasRight(currentEntity.entityName, Right.UPDATE);
        const createRecordDisabled = isEmptyCurrentEntity || !isUserCanCreate;
        const importDataDisabled = isEmptyCurrentEntity || !isUserCanCreate || !isUserCanUpdate;

        return (
            <PageHeader
                sectionTitle={i18n.t('module.data>search>title')}
                groupSectionTitle={i18n.t('module.data>search>groupTitle')}
                iconType='searchMainMenu'
                extraButtons={(
                    <React.Fragment>
                        {(hasChecked || allChecked) && (
                            <>
                                <Operations
                                    allChecked={allChecked}
                                    metaRecordStore={this.props.metaRecordStore}
                                    searchPanelStore={this.props.searchPanelStore}
                                    searchResultStore={this.props.searchResultStore}
                                />
                                &nbsp;
                                &nbsp;
                            </>
                        )}
                        {createRecordDisabled ?
                            (
                                <Tooltip
                                    overlay={i18n.t('module.data>search.panel>createDisabledText')}
                                >
                                    <Button
                                        leftIcon={'plus'}
                                        intent={INTENT.PRIMARY}
                                        isRound={true}
                                        data-qaid='createRecord'
                                        onClick={this.createRecord}
                                        isDisabled={true}
                                    >
                                        {i18n.t('module.data>search>createRecord')}
                                    </Button>
                                </Tooltip>
                            ) :
                            (
                                <Button
                                    leftIcon={'plus'}
                                    intent={INTENT.PRIMARY}
                                    isRound={true}
                                    data-qaid='createRecord'
                                    onClick={this.createRecord}
                                    title={i18n.t('module.data>search>createRecordTooltip')}
                                >
                                    {i18n.t('module.data>search>createRecord')}
                                </Button>
                            )
                        }
                        &nbsp;
                        &nbsp;
                        <ImportWizardButton
                            isDisabled={importDataDisabled}
                            metaRecordStore={this.props.metaRecordStore}
                        />
                    </React.Fragment>
                )}
            />
        );
    }
}
