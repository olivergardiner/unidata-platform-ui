/**
 * The wizard window with the batch operations
 *
 * @author Ivan Marshalkin
 * @date 2020-03-06
 */

import {Modal} from '@unidata/uikit';
import {Dialog, i18n} from '@unidata/core-app';
import * as React from 'react';
import {ReactNode} from 'react';
import {observer} from 'mobx-react';
import {BulkOperationType} from '../../../uemodule/bulkwizard/type/BulkOperationType';
import {IEtalonIds} from '../../../uemodule/bulkwizard/type/EtalonIds';
import {ueModuleManager, UeModuleReactComponent, UeModuleTypeComponent} from '@unidata/core';
import {BulkDataRecordService} from '../../../service/BulkDataRecordService';
import {EntityType} from '@unidata/types';
import {SearchTermsRequest} from '../../../service/operation/search/ReadDataList/ReadDataListType';

export interface IProps {
    entityName: string;
    entityType: EntityType;
    etalonIds: IEtalonIds[];
    classifiers: string[];
    query: SearchTermsRequest & {entity: string};
    queryCount: number;
    isOpen: boolean;
    onClose: () => void;
    operationType: BulkOperationType;
    operationTitle: string;
}

@observer
export class BulkWizardWnd extends React.Component<IProps> {
    closeModal () {
        let onClose = this.props.onClose;

        if (onClose) {
            onClose();
        }
    }

    runOperation = (cfg: any) => {
        const props = this.props;

        let promise = BulkDataRecordService.bulkOperation(
            props.operationType,
            props.entityName,
            props.etalonIds,
            props.query,
            props.queryCount,
            cfg
        );

        promise.then(
            function () {
                Dialog.showMessage(i18n.t('module.data>bulk.wizard>operationStart', {
                    name: props.operationTitle
                }));

                props.onClose();
            },
            function () {
                Dialog.showError(i18n.t('module.data>bulk.wizard>operationError'));
            }
        );
    }

    get recordCount (): number {
        const props = this.props;

        return props.etalonIds.length ? props.etalonIds.length : props.queryCount;
    }

    get operationWizard () {
        let wizard: ReactNode = null;

        const self = this;
        const props = this.props;
        const operationType = this.props.operationType;

        const modules = ueModuleManager.getModulesByType(UeModuleTypeComponent.BULK_DATA_WIZARD) as
            Array<UeModuleReactComponent<UeModuleTypeComponent.BULK_DATA_WIZARD>>;

        modules.forEach(function (module) {
            const wizardId = module.default.meta.wizardId;

            if (operationType === wizardId) {
                wizard = (
                    <module.default.component
                        entityName={props.entityName}
                        entityType={props.entityType}
                        etalonIds={props.etalonIds}
                        query={props.query}
                        queryCount={props.queryCount}
                        recordCount={self.recordCount}
                        onFinish={props.onClose}
                        classifiers={props.classifiers}
                        runOperation={self.runOperation}
                        operationTitle={props.operationTitle}
                    />
                );
            }
        });

        return wizard;
    }

    render () {
        const wndTitle = (
            <span>
                {i18n.t('module.data>bulk.wizard>wizardTitle')}: <span style={{fontWeight: 'normal'}}> {this.props.operationTitle}</span>
            </span>
        );

        return (
            <Modal
                isOpen={this.props.isOpen}
                noBodyPadding={true}
                header={wndTitle}
                onClose={this.closeModal.bind(this)}
            >
                {this.operationWizard}
            </Modal>
        );
    }
}
