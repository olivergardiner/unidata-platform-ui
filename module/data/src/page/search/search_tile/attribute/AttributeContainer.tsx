/**
 Selecting which attribute to show

 @author Vladimir Stebunov
 @date 2019-10-08
 */

import * as React from 'react';
import {UPathMeta} from '../../../../utils/UPathMeta';
import {SimpleAttributeItem} from './SimpleAttributeItem';
import {
    AliasCodeAttribute,
    ArrayAttribute,
    CodeAttribute,
    ComplexAttribute,
    Entity,
    LookupEntity,
    SimpleAttribute
} from '@unidata/meta';
import {SearchHit} from '@unidata/core-app';
import {SearchHitUtil} from '../../../../utils/SearchHitUtil';
import {UPath} from '../../../../utils/UPath';
import {ArrayAttributeItem} from './ArrayAttributeItem';

interface IAttributeContainerProps {
    searchHit: SearchHit;
    metaRecord: Entity | LookupEntity;
    isShowDisplayName: boolean;
    userExitTransformResultValues?: Array<{default:
        {fn: (metaAttribute: SimpleAttribute | CodeAttribute |AliasCodeAttribute, values: string[]) => string[]};
    }>;
}

export class AttributeContainer extends React.Component<IAttributeContainerProps> {
    render () {
        return (
            <span> {Object.keys(SearchHitUtil.toValuesMap(this.props.searchHit))
                          .sort((aKey: string, bKey: string) => {
                              const aAttr = UPathMeta.findAttributeByPath(this.props.metaRecord, aKey),
                                  bAttr = UPathMeta.findAttributeByPath(this.props.metaRecord, bKey),
                                  aOrder = aAttr ? aAttr.order.getValue() : Number.MAX_VALUE,
                                  bOrder = bAttr ? bAttr.order.getValue() : Number.MAX_VALUE;

                              return aOrder - bOrder;
                          })
                          .map((key: string) => {

                                  const metaAttribute = UPathMeta.findAttributeByPath(this.props.metaRecord, key);

                                  if (!metaAttribute) {
                                      return;
                                  }

                                  if (UPath.isComplexPath(key) || metaAttribute instanceof ComplexAttribute) {
                                      return;
                                  }

                                  if (!SearchHitUtil.getFieldValues(this.props.searchHit, key)) {
                                      return;
                                  }

                                  if (!metaAttribute.displayable.getValue()) {
                                      return;
                                  }

                                  if (SearchHitUtil.getFieldValues(this.props.searchHit, key).length === 0) {
                                      return;
                                  }

                                  if (metaAttribute instanceof ArrayAttribute) {
                                      return (
                                          <React.Fragment key={key}>
                                              <ArrayAttributeItem
                                                  metaAttribute={metaAttribute}
                                                  values={SearchHitUtil.getFieldValues(this.props.searchHit, key)}
                                                  isShowDisplayName={this.props.isShowDisplayName}
                                              />
                                          </React.Fragment>
                                      );
                                  } else {
                                      return (
                                          <React.Fragment key={key}>
                                              <SimpleAttributeItem
                                                  metaAttribute={metaAttribute}
                                                  values={SearchHitUtil.getFieldValues(this.props.searchHit, key)}
                                                  isShowDisplayName={this.props.isShowDisplayName}
                                                  userExitTransformResultValues={this.props.userExitTransformResultValues}
                                              />
                                          </React.Fragment>
                                      );
                                  }
                              }
                          )
            }
        </span>
        );
    }
}
