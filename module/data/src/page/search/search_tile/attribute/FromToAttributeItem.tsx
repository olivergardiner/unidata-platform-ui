/**
 A simple component that shows the recording interval

 @author Vladimir Stebunov
 @date 2019-10-16
 */

import * as React from 'react';
import * as moment from 'moment';
import {SearchHitUtil} from '../../../../utils/SearchHitUtil';
import {UserLocale, SearchHit} from '@unidata/core-app';

import * as styles from '../list/Tile.m.scss';
import {DateGranularityMode} from '@unidata/types';

interface IFromToAttributeItemProps {
    record: SearchHit;
    granularity: DateGranularityMode;
}

export class FromToAttributeItem extends React.Component<IFromToAttributeItemProps> {

    getFormatBy (granularityMode: DateGranularityMode): string {
        switch (granularityMode) {
            case DateGranularityMode.DATE: {
                return UserLocale.getDateFormat();
            }
            case DateGranularityMode.DATETIME: {
                return UserLocale.getDateTimeFormat();
            }
            case DateGranularityMode.DATETIMEMILLIS: {
                return UserLocale.getDateTimeMillsFormat();
            }
        }
    }

    render () {
        const
            granularity = this.props.granularity,
            record = this.props.record,
            format = this.getFormatBy(granularity),
            recordFrom = SearchHitUtil.getFieldValues<string>(record, '$from')[0],
            recordTo = SearchHitUtil.getFieldValues<string>(record, '$to')[0],
            from = recordFrom ? moment(recordFrom).format(format) : '-∞',
            to = recordTo ? moment(recordTo).format(format) : '+∞';

        return (<div className={styles.timeInterval}>{from}&nbsp;-&nbsp;{to}</div>);
    }
}
