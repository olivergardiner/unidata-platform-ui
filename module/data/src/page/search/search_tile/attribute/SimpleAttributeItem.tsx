/**
 The conclusion is simple attributes

 @author Vladimir Stebunov
 @date 2019-10-08
 */

import * as React from 'react';
import {AliasCodeAttribute, CodeAttribute, SimpleAttribute} from '@unidata/meta';
import {Tooltip} from '@unidata/uikit';
import * as styles from '../list/Tile.m.scss';
import {TooltipUtil} from '../../utils/TooltipUtil';
import {FormatUtil} from '../../utils/FormatUtil';

interface ISimpleAttributeItemProps {
    metaAttribute: SimpleAttribute | CodeAttribute | AliasCodeAttribute;
    values: any;
    isShowDisplayName: boolean;
    userExitTransformResultValues?: Array<{default:
        {fn: (metaAttribute: SimpleAttribute | CodeAttribute |AliasCodeAttribute, values: string[]) => string[]};
    }>;
}

export class SimpleAttributeItem extends React.Component<ISimpleAttributeItemProps> {

    renderTooltip () {
        const displayName = this.props.metaAttribute.displayName.getValue(),
            description = this.props.metaAttribute.description.getValue(),
            value = this.props.values,
            typeValueDisplay = TooltipUtil.buildDataTypeDisplayValue(this.props.metaAttribute);

        return (
            <div>
                <div>{displayName}: <i>{typeValueDisplay}</i></div>
                {value}
                {description ? <div>{description}</div> : ''}
            </div>
        );

    }

    renderValue () {
        const values = this.props.values;

        let formattedValues = values.map((value: string) => {
            return FormatUtil.formatByAttribute(value, this.props.metaAttribute);
        });

        formattedValues = this.props.userExitTransformResultValues && this.props.userExitTransformResultValues[0] ?
            this.props.userExitTransformResultValues[0].default.fn(this.props.metaAttribute, formattedValues.slice()) : formattedValues;

        return {
            __html: formattedValues
        };
    }

    render () {
        const isMain = this.props.metaAttribute.mainDisplayable.getValue(),
            displayName = this.props.metaAttribute.displayName.getValue(),
            tooltip = this.renderTooltip(),
            className = [styles.attribute, (isMain ? styles.attributeMain : '')].join(' ');

        return (
            <Tooltip overlay={tooltip}>
                <div className={className}>
                    {this.props.isShowDisplayName && <div className={styles.attributeHeader}>{displayName}</div>}
                    <div className={styles.attributeValue} dangerouslySetInnerHTML={this.renderValue()} />
                </div>
            </Tooltip>
        );
    }

}
