/**
 Output of arrays

 @author Vladimir Stebunov
 @date 2019-10-08
 */

import * as React from 'react';
import {createRef} from 'react';
import {Tooltip} from '@unidata/uikit';
import * as styles from '../list/Tile.m.scss';
import {ArrayAttribute} from '@unidata/meta';
import {AttributeTypeCategory} from '@unidata/types';
import {i18n} from '@unidata/core-app';
import {TooltipUtil} from '../../utils/TooltipUtil';

interface IArrayAttributeItemProps {
    metaAttribute: ArrayAttribute;
    values: any;
    isShowDisplayName: boolean;
}

interface IArrayAttributeItemState {
    isShowMore: boolean;
    removedElements: number;
}

export class ArrayAttributeItem extends React.Component<IArrayAttributeItemProps, IArrayAttributeItemState> {

    private containerRef = [];

    constructor (props: IArrayAttributeItemProps) {
        super(props);

        this.state = {
            isShowMore: false,
            removedElements: 0
        };
    }

    componentDidMount (): void {
        let firstTop = 0;
        let removedElements = 0;

        this.containerRef.forEach((value: any, index: number) => {
            if (!value) {
                return;
            }

            if (index === 0) {
                firstTop = value.current.offsetTop;

                return;
            }

            if (value.current.offsetTop !== firstTop) {
                removedElements++;
            }
        });
        this.setState({removedElements: removedElements});
    }

    renderTooltip (value: any): JSX.Element {
        const displayName = this.props.metaAttribute.displayName.getValue(),
            description = this.props.metaAttribute.description.getValue(),
            typeValueDisplay = TooltipUtil.buildDataTypeDisplayValue(this.props.metaAttribute);

        return (
            <div>
                <div>{displayName}: <i>{typeValueDisplay}</i></div>
                {value}
                {description ? <div>{description}</div> : ''}
            </div>
        );

    }

    renderMoreTooltip (hiddenValues: string[]): JSX.Element {
        return (
            <React.Fragment>
                {hiddenValues.map((value: string, index: number) => {
                    return (<div key={index}>{value}</div>);
                })}
            </React.Fragment>
        );
    }

    render () {
        const displayName = this.props.metaAttribute.displayName.getValue(),
            values = this.props.values;

        this.containerRef = values.map((val: any, index: number) => createRef());

        if (this.props.metaAttribute.typeCategory === AttributeTypeCategory.lookupEntityType) {
            //TODO:
            /*
            // reference in the form of a link
                    if (typeCategory === 'lookupEntityType') {
                        text = Ext.String.format(
                            '<span class="un-rel-to" data-etalonid="{0}">{1}</span>',
                            extendedValues[index].linkedEtalonId,
                            text
                        );
                    }
             */
        }

        let slicedValues = this.state.removedElements > 0 ? values.slice(0, values.length - this.state.removedElements) : values;
        let hiddenValues = this.state.removedElements > 0 ? values.slice(values.length - this.state.removedElements, values.length) : [];

        return (
            <div>
                {this.props.isShowDisplayName && <div className={styles.attributeHeader}>{displayName}</div>}
                <div className={styles.attributeArrayContainer}>
                    {slicedValues.map((value: any, index: number) => (
                        <Tooltip key={index} overlay={this.renderTooltip(value)}>
                            <div ref={this.containerRef[index]} className={styles.attributeArray}>{value}</div>
                        </Tooltip>
                    ))}
                    {this.state.removedElements > 0 && <Tooltip overlay={this.renderMoreTooltip(hiddenValues)}>
                        <div className={styles.attributeMore}>{i18n.t('more')} {this.state.removedElements}...
                        </div>
                    </Tooltip>}
                </div>
            </div>
        );
    }

}
