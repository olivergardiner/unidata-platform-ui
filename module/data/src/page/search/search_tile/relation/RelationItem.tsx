/**
 Output reference attributes

 @author Vladimir Stebunov
 @date 2019-10-08
 */

import * as React from 'react';
import {i18n} from '@unidata/core-app';

import * as styles from '../list/Tile.m.scss';
import {ISearchRelationItem} from '../../../../service/operation/search/ReadRelationList/ReadRelationListType';
import {MetaRelation} from '@unidata/meta';
import {Icon, Tooltip} from '@unidata/uikit';
import {IconName} from '@unidata/icon';

interface IRelationItemProps {
    metaRelation: MetaRelation;
    totalRelationsCount: number;
    relationsData: ISearchRelationItem[];
    hasShowName: boolean;
}

const icons: { [key: string]: IconName } = {
    ManyToMany: 'site-map',
    Contains: 'share3',
    References: 'link2'
};

export class RelationItem extends React.Component<IRelationItemProps> {
    renderTooltip (etalonName: string[]) {

        let type;

        switch (this.props.metaRelation.relType.getValue()) {
            case 'ManyToMany':
                type = i18n.t('relation>m2mIconTooltipText');
                break;
            case 'Contains':
                type = i18n.t('relation>containsIconTooltipText');
                break;
            case 'References':
                type = i18n.t('relation>referenceIconTooltipText');
                break;
        }

        return (
            <React.Fragment>
                <div>{this.props.metaRelation.displayName.getValue()} <i>{type}</i></div>
                {etalonName.map((etalonDisplayNameTo: string, index: number) =>
                    <div key={index}>{etalonDisplayNameTo}</div>
                )}
                {this.props.totalRelationsCount > this.props.relationsData.length &&
                <div>
                    {i18n.t('relation>showingOf', {
                        count: this.props.relationsData.length,
                        total: this.props.totalRelationsCount
                    })}
                </div>}
            </React.Fragment>
        );
    }

    render () {
        const {metaRelation, hasShowName, relationsData, totalRelationsCount} = this.props;
        const icon = icons[metaRelation.relType.getValue()];

        let etalonName: string[] = [];

        relationsData.forEach((item: ISearchRelationItem) => {
            etalonName.push(item.etalonDisplayNameTo);
        });

        return (
            <div>
                {hasShowName && <div className={styles.relationHeader}>
                    <Icon name={icon}/>
                    {metaRelation.displayName.getValue()}
                </div>}
                <Tooltip overlay={this.renderTooltip(etalonName)}>
                    <div className={styles.relationValue}>
                        {etalonName.map((etalonDisplayNameTo: string, index: number) =>
                            <div className={styles.attributeValue} key={index}>{etalonDisplayNameTo}</div>
                        )}
                    </div>
                </Tooltip>
                {totalRelationsCount > relationsData.length &&
                <div>
                    {i18n.t('relation>showingOf', {
                        count: relationsData.length,
                        total: totalRelationsCount
                    })}
                </div>}
            </div>
        );
    }
}
