/**
 The container contains all links

 @author Vladimir Stebunov
 @date 2019-10-08
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {Entity, LookupEntity, MetaRelation} from '@unidata/meta';
import {RelationItem} from './RelationItem';
import {SearchHitUtil} from '../../../../utils/SearchHitUtil';
import {SearchResultStore} from '../../store/SearchResultStore';
import {ISearchRelation} from '../../../../service/operation/search/ReadRelationList/ReadRelationListType';
import {SearchHit} from '@unidata/core-app';

interface IRelationContainerProps {
    searchHit: SearchHit;
    dataRelations: ISearchRelation[];
    metaRecord: Entity | LookupEntity;
    isShowDisplayName: boolean;
}

interface IInjectProps extends IRelationContainerProps {
    searchResultStore: SearchResultStore;
}

@inject('searchResultStore')
@observer
export class RelationContainer extends React.Component<IRelationContainerProps> {

    get injected () {
        return this.props as IInjectProps;
    }

    get store () {
        return this.injected.searchResultStore;
    }

    render () {

        if (this.props.metaRecord instanceof LookupEntity) {
            return (null);
        }

        let result: Array<{ metaRelation: MetaRelation; relationData: ISearchRelation }> = [];

        const dataRelations = this.injected.searchResultStore.relations;

        this.props.metaRecord.relations.forEach((metaRelation: MetaRelation) => {
            const metaRelationName = metaRelation.name.getValue();
            const etalonId = SearchHitUtil.getFieldValues(this.props.searchHit, '$etalon_id')[0];

            dataRelations.forEach((dataRelation: ISearchRelation) => {
                if (metaRelationName === dataRelation.relName && etalonId === dataRelation.etalonId) {
                    result.push({metaRelation: metaRelation, relationData: dataRelation});
                }
            });
        });

        return (
            <React.Fragment>
                {result.map(relation =>
                    <RelationItem
                        key={relation.relationData.modelId}
                        hasShowName={this.props.isShowDisplayName}
                        metaRelation={relation.metaRelation}
                        relationsData={relation.relationData.data}
                        totalRelationsCount={relation.relationData.total}/>
                )}
            </React.Fragment>
        );
    }
}
