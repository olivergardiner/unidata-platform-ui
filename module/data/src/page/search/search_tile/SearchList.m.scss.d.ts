export const slider: string;

export const header: string;

export const headerButton: string;

export const checkboxLabel: string;

export const tileListContainer: string;