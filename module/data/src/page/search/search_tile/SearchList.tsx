/**
 Flat list search component

 @author Vladimir Stebunov
 @date 2019-10-08
 */

import * as React from 'react';
import {observer} from 'mobx-react';

import {MetaRecordStore} from '../store/MetaRecordStore/MetaRecordStore';
import {SearchResultStore} from '../store/SearchResultStore';
import {SearchHitUtil} from '../../../utils/SearchHitUtil';
import {i18n, SearchHit} from '@unidata/core-app';
import {Confirm, Layout} from '@unidata/uikit';
import {TileList} from './list/TileList';

import * as styles from './SearchList.m.scss';
import {action, observable} from 'mobx';
import {AliasCodeAttribute, CodeAttribute, SimpleAttribute, FACETS} from '@unidata/meta';

const {Sider, Content} = Layout;

interface ITileListProps {
    metaRecordStore: MetaRecordStore;
    searchResultStore: SearchResultStore;
    showDataView: Function;
    destroyDataView: Function;
    dataViewHasChange: () => boolean;
    userExitTransformResultValues?: Array<{default:
            {fn: (metaAttribute: SimpleAttribute | CodeAttribute |AliasCodeAttribute, values: string[]) => string[]};
    }>;
}

@observer
export class SearchList extends React.Component<ITileListProps> {

    private cardView: {
        current: HTMLDivElement | null;
    };

    @observable private selected: SearchHit | null;
    @observable private newSelected: SearchHit | null;
    @observable isShowDisplayName: boolean;
    @observable isShowRelationData: boolean;
    @observable isConfirmOpen: boolean = false;

    constructor (props: ITileListProps) {
        super(props);

        this.cardView = React.createRef();
        this.isShowDisplayName = false;
        this.isShowRelationData = false;
        this.selected = null;
        this.newSelected = null;
    }

    componentWillUnmount (): void {
        this.props.destroyDataView();
    }

    private createShowViewArray (item: SearchHit) {
        const facets = this.props.searchResultStore.getSearchFacets(),
            isPendingOnly = Boolean(facets && (facets.indexOf(FACETS.PENDING_ONLY) > -1));

        return [
            SearchHitUtil.getFieldValues(item, '$etalon_id'), // string ID of the record
            null,                                                      // metaRecord
            null,                                                      // title
            null,                                                      // sourceRecord
            null,                                                      // saveCallback
            null,                                                      // operationId - flag for integration with pubsub
            isPendingOnly,                                // true if the search query specifies the pending_only facet example facets: ["pending_only"]
            SearchHitUtil.getFieldValues(item, 'from')
        ];
    }

    @action
    openNewSelectedCard () {
        this.isConfirmOpen = false;

        if (this.selected) {
            this.props.destroyDataView(this.cardView.current);
        }

        if (!this.newSelected) {
            return;
        }

        this.props.showDataView(this.cardView.current, this.createShowViewArray(this.newSelected));
        this.selected = this.newSelected;
    }

    @action
    changeShowDisplayName = () => {
        this.isShowDisplayName = !this.isShowDisplayName;
    }

    @action
    changeShowRelationData = () => {
        this.isShowRelationData = !this.isShowRelationData;
    };

    @action
    switchCard = (item: SearchHit) => {
        if (this.selected && this.selected === item) {
            return;
        }

        this.newSelected = item;

        if (this.props.dataViewHasChange()) {
            this.isConfirmOpen = true;

            return;
        }

        setTimeout(() => {
            this.openNewSelectedCard();
        });

    }

    @action
    onSwitchClose = () => {
        this.isConfirmOpen = false;
    }


    render () {

        const {searchResultStore, metaRecordStore} = this.props;

        const
            isShowDisplayName = this.isShowDisplayName,
            isShowRelationData = this.isShowRelationData;

        return (
            <React.Fragment>
                <Sider width={300} className={styles.slider}>
                    <TileList
                        metaRecordStore={metaRecordStore}
                        searchResultStore={searchResultStore}
                        showCard={this.switchCard}
                        isShowDisplayName={isShowDisplayName}
                        isShowRelationData={isShowRelationData}
                        changeShowDisplayName={this.changeShowDisplayName}
                        changeShowRelationData={this.changeShowRelationData}
                        selected={this.selected}
                        userExitTransformResultValues={this.props.userExitTransformResultValues}
                    />
                </Sider>
                <Layout>
                    <Content>
                        <div ref={this.cardView} style={{height: '100%'}}></div>
                    </Content>
                </Layout>
                <Confirm
                    onConfirm={this.openNewSelectedCard}
                    confirmationMessage={i18n.t('unsavedChanges')}
                    isOpen={this.isConfirmOpen}
                    onClose={this.onSwitchClose}
                />
            </React.Fragment>
        );
    }
}
