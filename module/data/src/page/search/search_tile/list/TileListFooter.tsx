/**
 List footer, contains pagination

 @author Vladimir Stebunov
 @date 2019-10-28
 */

import * as React from 'react';
import {Table} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';

import * as styles from './Tile.m.scss';
import {observer} from 'mobx-react';
import {PaginationStore} from '../../store/PaginationStore';

interface IProps {
    paginationStore: PaginationStore;
}

@observer
export class TileListFooter extends React.Component<IProps> {
    render () {
        return (
            <div className={styles.tileFooterContainer}>
                <Table.Pagination
                    page={this.props.paginationStore.currentPage}
                    pages={this.props.paginationStore.totalPages}
                    onPageChange={this.props.paginationStore.setPage}
                    hidePages={true}
                />
                <div style={{marginRight: '10px'}}>
                    <Table.PageSize
                        value={this.props.paginationStore.pageSize.toString()}
                        pageSizeOptions={[20, 50, 100]}
                        onChangePageSize={this.props.paginationStore.setPageSize}
                        rowsText={i18n.t('common:page')}
                    />
                </div>
            </div>
        );
    }
}
