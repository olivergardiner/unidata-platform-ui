export const tile: string;

export const tileControl: string;

export const attributeHeader: string;

export const attributeValue: string;

export const attributeArray: string;

export const attributeMain: string;

export const bulkCheck: string;

export const bulkCheckSelected: string;

export const favoriteIcon: string;

export const attribute: string;

export const attributeArrayContainer: string;

export const attributeMore: string;

export const favorite: string;

export const timeInterval: string;

export const relationHeader: string;

export const relationValue: string;

export const selected: string;

export const tileFooterContainer: string;
