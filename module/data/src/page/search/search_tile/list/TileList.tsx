/**
 The container for the tiles

 @author Vladimir Stebunov
 @date 2019-10-08
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {MetaRecordStore} from '../../store/MetaRecordStore/MetaRecordStore';
import {SearchResultStore} from '../../store/SearchResultStore';
import {Checkbox, Icon, PLACEMENT, Popover, SIZE, Table, TRIGGER} from '@unidata/uikit';
import {i18n, SearchHit} from '@unidata/core-app';
import {Tile} from './Tile';
import {TileListFooter} from './TileListFooter';

import * as styles from '../SearchList.m.scss';
import {TableLoading} from '../../search_table/components/table_loading/TableLoading';
import {AliasCodeAttribute, CodeAttribute, SimpleAttribute} from '@unidata/meta';

interface ITileListProps {
    metaRecordStore: MetaRecordStore;
    searchResultStore: SearchResultStore;
    showCard: (item: SearchHit) => void;
    isShowDisplayName: boolean;
    isShowRelationData: boolean;
    changeShowDisplayName: () => void;
    changeShowRelationData: () => void;
    selected: SearchHit | null;
    userExitTransformResultValues?: Array<{default:
        {fn: (metaAttribute: SimpleAttribute | CodeAttribute |AliasCodeAttribute, values: string[]) => string[]};
    }>;
}

@observer
export class TileList extends React.Component<ITileListProps> {

    render () {
        const {
            metaRecordStore,
            searchResultStore,
            isShowRelationData,
            isShowDisplayName,
            changeShowDisplayName,
            changeShowRelationData,
            showCard
        } = this.props;

        if (metaRecordStore.recordMetaData === undefined) {
            return null;
        }

        const metaRecord = metaRecordStore.recordMetaData,
            isAlwaysShowCheck = this.props.searchResultStore.isRecordsChecked();

        const columns = [{
            Header: () => (
                <div className={styles.header}>
                    <span>
                    {i18n.t('module.data>search.tile>results')} {searchResultStore.paginationStore.totalRecords > 0 ?
                        '(' + searchResultStore.paginationStore.totalRecords + ')' : ''
                    }
                    </span>
                    <span className={styles.headerButton}>
                        <Popover
                            trigger={TRIGGER.CLICK}
                            placement={PLACEMENT.BOTTOM_END}
                            target={(
                                <Icon
                                    name='cog2'
                                    size={SIZE.EXTRA_LARGE}
                                />
                            )}
                        >
                             <div>
                                    <div>
                                        <Checkbox checked={isShowDisplayName} onChange={changeShowDisplayName}/>
                                        <span className={styles.checkboxLabel}>{i18n.t('module.data>search.tile>showAttributes')}</span>
                                    </div>
                                    <div>
                                        <Checkbox checked={isShowRelationData} onChange={changeShowRelationData}/>
                                        <span className={styles.checkboxLabel}>{i18n.t('module.data>search.tile>showLinks')}</span>
                                    </div>
                                </div>
                        </Popover>
                    </span>
                </div>

            ),
            accessor: 'modelId', //TODO: remove
            sortable: false,
            resizable: false,
            Cell: (cell: any) => (
                <Tile
                    item={cell}
                    metaRecord={metaRecord}
                    isShowRelationData={isShowRelationData}
                    isShowDisplayName={isShowDisplayName}
                    isAlwaysShowCheck={isAlwaysShowCheck}
                    onClick={showCard}
                    store={searchResultStore}
                    metaRecordStore={metaRecordStore}
                    onCheckClick={() => this.forceUpdate()}
                    selected={this.props.selected}
                    userExitTransformResultValues={this.props.userExitTransformResultValues}
                />
            )
        }];

        if (metaRecordStore.attributes.length < 0 && !metaRecord) {
            return;
        }

        return (
            <div className={styles.tileListContainer}>
                <Table
                    columns={columns}
                    data={this.props.searchResultStore.records}
                    viewRowsCount={this.props.searchResultStore.paginationStore.pageSize}
                />
                <TileListFooter paginationStore={this.props.searchResultStore.paginationStore}/>
                <TableLoading searchResultStore={searchResultStore}/>
            </div>
        );

    }
}
