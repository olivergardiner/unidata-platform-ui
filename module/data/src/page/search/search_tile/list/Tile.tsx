/**
 Output the main search tile

 @author Vladimir Stebunov
 @date 2019-10-08
 */

import * as React from 'react';
import {AttributeContainer} from '../attribute/AttributeContainer';
import {RelationContainer} from '../relation/RelationContainer';
import {FromToAttributeItem} from '../attribute/FromToAttributeItem';
import {SearchHit} from '@unidata/core-app';
import {AliasCodeAttribute, CodeAttribute, Entity, LookupEntity, SimpleAttribute, FACETS} from '@unidata/meta';
import {FavoriteCheckbox} from '../../search_table/components/favorite_checkbox/FavoriteCheckbox';
import {SearchResultStore} from '../../store/SearchResultStore';
import {MetaRecordStore} from '../../store/MetaRecordStore/MetaRecordStore';
import {SearchHitUtil} from '../../../../utils/SearchHitUtil';
import * as styles from './Tile.m.scss';
import {Icon} from '@unidata/uikit';

interface ITileProps {
    item: {
        original: SearchHit;
    };
    metaRecord: Entity | LookupEntity;
    isShowRelationData: boolean;
    isShowDisplayName: boolean;
    onClick: Function;
    store: SearchResultStore;
    metaRecordStore: MetaRecordStore;
    isAlwaysShowCheck: boolean;
    onCheckClick: () => void;
    selected: SearchHit | null;
    userExitTransformResultValues?: Array<{default:
        {fn: (metaAttribute: SimpleAttribute | CodeAttribute |AliasCodeAttribute, values: string[]) => string[]};
    }>;
}

export class Tile extends React.Component<ITileProps> {

    render () {
        const isRemoved = this.props.item.original.status.getValue() === 'INACTIVE',
            metaRecordStore = this.props.metaRecordStore,
            granularity = this.props.metaRecord.getGranularityMode(),
            etalonId = SearchHitUtil.getFieldValues<string>(this.props.item.original, '$etalon_id')[0],
            from = SearchHitUtil.getFieldValues<string | null>(this.props.item.original, '$from')[0],
            to = SearchHitUtil.getFieldValues<string | null>(this.props.item.original, '$to')[0],
            isFavoriteRecord = metaRecordStore.getFavoriteStore().isFavorite(this.props.metaRecordStore.currentEntity.entityName, etalonId),
            isOpened = this.props.selected  ? this.props.selected.modelId === this.props.item.original.modelId : false,
            tileClassNames = [styles.tile, isFavoriteRecord ? styles.favorite : '', isOpened ? styles.selected : ''].join(' '),
            checkClassName = this.props.isAlwaysShowCheck ? styles.bulkCheckSelected : styles.bulkCheck,
            facets = this.props.store.getSearchFacets(),
            isShowAllPeriods = Boolean(facets && (facets.indexOf(FACETS.UN_RANGED) > -1));

        return (
            <div className={tileClassNames} onClick={() => {
                this.props.onClick(this.props.item.original);
            }}>
                <div>
                    {isRemoved && <Icon name={'trash2'}/>}
                    <AttributeContainer
                        isShowDisplayName={this.props.isShowDisplayName}
                        searchHit={this.props.item.original}
                        metaRecord={this.props.metaRecord}
                        userExitTransformResultValues={this.props.userExitTransformResultValues}
                    />
                    {isShowAllPeriods &&
                    <FromToAttributeItem record={this.props.item.original} granularity={granularity}/>}
                    {this.props.isShowRelationData &&
                    <RelationContainer
                        isShowDisplayName={this.props.isShowDisplayName}
                        searchHit={this.props.item.original}
                        metaRecord={this.props.metaRecord}
                        dataRelations={this.props.store.relations}
                    />
                    }
                </div>
                <div className={styles.tileControl}>
                    <div className={styles.favoriteIcon}>
                        <FavoriteCheckbox
                            entityName={this.props.metaRecordStore.currentEntity.entityName}
                            etalonId={etalonId}
                            favoriteStore={metaRecordStore.getFavoriteStore()}
                            isLarge={true}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

