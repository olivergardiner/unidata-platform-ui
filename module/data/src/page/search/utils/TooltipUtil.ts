/**
  Class for formatting the attribute for displaying the tooltip

  @author Vladimir Stebunov
  @date 2019-10-21
 */

import {AliasCodeAttribute, ArrayAttribute, CodeAttribute, ComplexAttribute, SimpleAttribute} from '@unidata/meta';
import {i18n} from '@unidata/core-app';
import {AttributeTypeCategory} from '@unidata/types';
import {TypeAttributeUtil} from '../../../utils/TypeAttributeUtil';
import isEmpty from 'lodash/isEmpty';


export class TooltipUtil {

    public static buildDataTypeDisplayValue (metaAttribute: SimpleAttribute | CodeAttribute | AliasCodeAttribute | ArrayAttribute) {
        let typeCategory, lookupEntityType, typeValueDisplay;

        //TODO: in theory we don't output a complex attribute but in the old code there is such a point in building a hint
        if (metaAttribute instanceof ComplexAttribute) {
            return i18n.t('metaFormatter>complex');
        }


        if (metaAttribute instanceof SimpleAttribute ||
            metaAttribute instanceof ArrayAttribute) {
            typeCategory  = metaAttribute.typeCategory;
            lookupEntityType = metaAttribute.lookupEntityType.getValue();
        }


        if (typeCategory === AttributeTypeCategory.lookupEntityType) {
            typeValueDisplay = lookupEntityType;
        } else {
            if (!(metaAttribute instanceof AliasCodeAttribute)) {
                let typeValue = metaAttribute.typeValue;
                let typeValueDescription = TypeAttributeUtil.getSimpleDataDisplayName(typeValue);

                typeValueDisplay = typeValueDescription ? typeValueDescription.displayName : '';
            }
        }

        if (typeCategory === AttributeTypeCategory.arrayDataType) {
            if (!isEmpty(lookupEntityType)) {
                typeValueDisplay = lookupEntityType;
            }

            typeValueDisplay += ', ' + i18n.t('glossary>array');
        }

        return typeValueDisplay;
    }

}
