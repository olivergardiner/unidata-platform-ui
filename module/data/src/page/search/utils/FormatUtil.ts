/**
  Class for formatting a value by its attribute

  @author Vladimir Stebunov
  @date 2019-10-21
 */

import {i18n} from '@unidata/core-app';
import {AliasCodeAttribute, ArrayAttribute, CodeAttribute, SimpleAttribute} from '@unidata/meta';
import {AttributeTypeCategory} from '@unidata/types';
import isArray from 'lodash/isArray';
import isBoolean from 'lodash/isBoolean';
import isString from 'lodash/isString';
import moment, {isDate} from 'moment';
import {UserLocale} from '@unidata/core-app';

export class FormatUtil {

    public static formatByAttribute (value: any, metaAttribute: SimpleAttribute | CodeAttribute | AliasCodeAttribute | ArrayAttribute) {
        if (!metaAttribute || value === null || value === undefined || (metaAttribute instanceof AliasCodeAttribute)) {
            return value;
        }

        const typeValue = metaAttribute.typeValue,
            typeCategory = metaAttribute.typeCategory;

        if (typeCategory === AttributeTypeCategory.enumDataType) {
            return value;
        }

        if (typeCategory === AttributeTypeCategory.arrayDataType && isArray(value)) {
            return '[' +
                value
                    .map((arrayValue: any) => {
                        return FormatUtil.formatValueByType(typeValue, arrayValue);
                    }).join(', ') +
                ']';
        }

        return FormatUtil.formatValueByType(typeValue, value);
    }

    public static formatValueByType (typeValue: string, value: string | boolean) {
        let dateFormat,
            dateParseFormat;

        if (typeValue === 'Date' || typeValue === 'Timestamp' || typeValue === 'Time') {
            switch (typeValue) {
                case 'Date':
                    dateParseFormat = UserLocale.getParseDateFormat();
                    dateFormat = UserLocale.getDateFormat();
                    break;
                case 'Timestamp':
                    dateParseFormat = UserLocale.getParseTimestampFormat();
                    dateFormat = UserLocale.getDateTimeFormat();
                    break;
                case 'Time':
                    dateParseFormat = UserLocale.getParseTimeFormat();
                    dateFormat = UserLocale.getTimeFormat();
                    break;
            }

            if (isString(value)) {
                value = moment(value, dateParseFormat).format(dateFormat);
            } else if (isDate(value)) {
                value = moment(value).format(dateFormat);
            }
        } else if (typeValue === 'Boolean') {
            if (isString(value)) {
                if (value === 'true') {
                    value = true;
                } else if (value === 'false') {
                    value = false;
                }
            }

            if (!isBoolean(value)) {
                value = '';
            } else {
                value = value ? i18n.t('yes').toLowerCase() : i18n.t('no').toLowerCase();
            }
        } else if (typeValue === 'Number') {
            return value.toString().replace('.', UserLocale.getDecimalSeparator());
        }

        return value;
    }

}
