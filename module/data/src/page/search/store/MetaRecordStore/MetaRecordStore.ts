/**
 * Storage for the metamodel to search for
 *
 * @author: Ilya Brauer
 * @date: 2019-10-09
 */

import {action, observable, runInAction, toJS} from 'mobx';
import {AttributeTypeCategory, EntityType, RelationDirectionType, SIMPLE_DATA_TYPE} from '@unidata/types';
import {ITreeNode, TreeUtil} from '@unidata/uikit';
import {
    AliasCodeAttribute,
    ArrayAttribute,
    Catalog,
    CodeAttribute,
    Entity,
    EntityGroupService,
    EntityGroupUtils,
    LookupEntity,
    MetaRecordService,
    MetaRelationService,
    NestedEntity,
    SimpleAttribute
} from '@unidata/meta';
import set from 'lodash/set';
import {BackendStorageService, IBackendStorageRecord, UdLogger, UserManager} from '@unidata/core-app';

import {CurrentEntity, EntityMeta, IDisplayAttrSettings} from './MetaRecordStoreTypes';
import {FavoriteStore} from '../../../../store/FavoriteStore';
import {IObservableArray} from 'mobx/lib/types/observablearray';
import {IDisplayAttr} from '@unidata/meta';

export class MetaRecordStore {

    private DELIMITER = '.';

    @observable
    public isLoading: boolean = false;

    @observable
    public currentEntity: CurrentEntity = {
        entityName: '',
        entityDisplayName: '',
        entityType: EntityType.Entity
    };

    @observable
    public newEntity?: CurrentEntity;

    @observable.shallow
    public recordMetaData?: Entity | LookupEntity;

    @observable.shallow
    public entityTree: ITreeNode[] = [];

    @observable.shallow
    public attributes: IObservableArray<IDisplayAttr> = observable([]);

    public relationsFrom: Entity[] = [];

    private entityList: Catalog[] = [];
    private readonly favoriteStore: FavoriteStore;
    private isLoadingEntitiesPromise: Promise<void> | undefined;

    constructor (favoriteStore: FavoriteStore) {
        this.favoriteStore = favoriteStore;
    }

    public getFavoriteStore = () => {
        return this.favoriteStore;
    };

    public reloadFavorite = () => {
        this.favoriteStore.setFavoriteEntities(this.entityList);
    };

    @action
    public getEntityList = () => {
        this.isLoadingEntitiesPromise = EntityGroupService.getEntityGroup().then((entityList) => {
            let entityTree: ITreeNode[] = [];

            const treeNodeData = EntityGroupUtils.buildTreeNodeModel(entityList);

            EntityGroupUtils.clearEmptyGroups(treeNodeData);

            if (treeNodeData.children) {
                entityTree = treeNodeData.children.sort(TreeUtil.sortTree);
            }

            this.favoriteStore.setFavoriteEntities(entityList);

            runInAction(() => {
                this.entityTree = entityTree;
            });

            this.entityList = entityList;
        });

        return this.isLoadingEntitiesPromise;
    };

    public setCurrentEntityByName = (entityName: string) => {
        let promise = Promise.resolve();

        if (this.isLoadingEntitiesPromise instanceof Promise) {
            promise = this.isLoadingEntitiesPromise;
        }

        return promise.then(() => {
            return new Promise((resolve, reject) => {
                for (let i = 0, length = this.entityList.length; i < length; i++) {
                    const entity = this.entityList[i].entities.find2((item) => item.name.getValue() === entityName);

                    if (entity !== null) {
                        this.setCurrentEntity({
                            entityName: entity.name.getValue(),
                            entityDisplayName: entity.displayName.getValue(),
                            entityType: EntityType.Entity
                        });
                        break;
                    }

                    const lookupEntity = this.entityList[i].lookupEntities.find2((item) => item.name.getValue() === entityName);

                    if (lookupEntity !== null) {
                        this.setCurrentEntity({
                            entityName: lookupEntity.name.getValue(),
                            entityDisplayName: lookupEntity.displayName.getValue(),
                            entityType: EntityType.LookupEntity
                        });
                        break;
                    }
                }

                if (this.currentEntity.entityName === '') {
                    UdLogger.error(`Try to go to unknown entity "${entityName}"`);
                } else {
                    resolve();
                }
            });
        });
    };

    @action
    public setCurrentEntity = (entityMeta: EntityMeta) => {
        this.currentEntity = entityMeta;
    };

    private settings: IBackendStorageRecord | undefined;

    /**
     * Obtaining of meta-information at the entity
     */
    public getMetaRecord = () => {
        const currentEntity = this.currentEntity;

        if (currentEntity !== undefined) {
            this.recordMetaData = undefined;

            return MetaRecordService.getMetaRecord(currentEntity.entityType, currentEntity.entityName)
                .then((data) => {
                    this.recordMetaData = data;

                    return data;
                });
        }

        return Promise.reject('Try to get metaData for undefined Entity');
    };

    /**
     * Receive full meta-information, including links
     */
    @action
    public getMetaRecordFull = () => {
        const currentEntity = this.currentEntity;

        if (currentEntity !== undefined) {
            this.isLoading = true;
            this.recordMetaData = undefined;
            this.attributes.clear();

            return MetaRecordService.getMetaRecord(currentEntity.entityType, currentEntity.entityName)
                .then((data) => {
                    return BackendStorageService.loadRecords(UserManager.getUserLogin() as string).then((backendStorageData) => {
                        this.settings = backendStorageData.find((item) => item.key === `${currentEntity.entityName}_visible_settings`);
                    })
                        .then(() => {
                            return Promise.all([
                                MetaRelationService.getRelatedEntities(RelationDirectionType.from, currentEntity.entityName)
                            ]).then(([relationFrom]) => {
                                this.relationsFrom = relationFrom;
                            });
                        })
                        .finally(() => {
                            runInAction(() => {
                                this.isLoading = false;
                                this.recordMetaData = data;
                                this.getDisplayAttrsToEntity(this.attributes, data);
                            });
                        });
                });
        }

        return Promise.reject('Try to get metaData for undefined Entity');
    };

    /**
     * A section on working with field visibility
     */

    @action
    public toggleAttributeVisible = (path: string, isShow: boolean) => {
        set(this.attributes, `${path}.isShow`, isShow);
    }

    @action
    public setAllAttributesVisible = () => {
        this.attributes.forEach((attribute) => {
            attribute.isShow = true;

            if (attribute.children && attribute.children.length > 0) {
                attribute.children.forEach((attributeCh) => {
                    attributeCh.isShow = true;
                });
            }
        });
    };

    @action
    public resetAttributesVisible = () => {
        this.attributes = observable([]);
        this.settings = undefined;

        if (this.recordMetaData !== undefined) {
            this.getDisplayAttrsToEntity(this.attributes, this.recordMetaData);
        }
    };

    public saveVisibleSettings = () => {
        const jsonValue = toJS(this.mapToVisibleSettings(this.attributes));
        const currentEntity = this.currentEntity;

        if (currentEntity !== undefined) {
            BackendStorageService.saveRecord(
                UserManager.getUserLogin() as string,
                `${currentEntity.entityName}_visible_settings`,
                JSON.stringify(jsonValue)
            );
        } else {
            console.warn('Try to save visible settings to undefined Entity');
        }
    };
    /** ===== */

    public columnsSort = (a: IDisplayAttr, b: IDisplayAttr) => {
        if (a.order === undefined) {
            return 1;
        } else if (b.order === undefined) {
            return 1;
        } else {
            return a.order - b.order;
        }
    };

    private mapToVisibleSettings = (attributes: IDisplayAttr[]): IDisplayAttrSettings[] => {
        return attributes.map((attribute) => {
            return {
                name: attribute.name,
                isShow: attribute.isShow,
                children: (attribute.children && attribute.children.length > 0) ? this.mapToVisibleSettings(attribute.children) : undefined
            };
        });
    };

    @action
    public getDisplayAttrsToEntity = (res: IDisplayAttr[], metaEntity: Entity | LookupEntity | NestedEntity, name?: string) => {
        let settings: IDisplayAttrSettings[] = [];

        if (this.settings !== undefined) {
            settings = JSON.parse((this.settings).value) as IDisplayAttrSettings[];
        }

        const pushAttribute = (recordMDItem: SimpleAttribute | ArrayAttribute | CodeAttribute | AliasCodeAttribute) => {
            const isLinkDataType = recordMDItem.typeCategory === AttributeTypeCategory.linkDataType;

            if (!isLinkDataType) {
                const key = (name) ? `${name}${this.DELIMITER}${recordMDItem.name.getValue()}` : recordMDItem.name.getValue();
                const settingItem = settings.find((s) => s.name === key);

                let isShow = recordMDItem.displayable.getValue();

                if (settingItem !== undefined) {
                    if (!name) {
                        isShow = settingItem.isShow;
                    } else if (settingItem.children !== undefined) {
                        const settingItemChild = settingItem.children.find((s) => s.name === recordMDItem.name.getValue());

                        if (settingItemChild !== undefined) {
                            isShow = settingItemChild.isShow;
                        }
                    }
                }

                if (recordMDItem instanceof ArrayAttribute) {
                    res.push(this.getDisplayAttrFromMeta(recordMDItem, key, isShow, true));
                } else {
                    res.push(this.getDisplayAttrFromMeta(recordMDItem, key, isShow, false));
                }
            }
        };

        if (metaEntity instanceof LookupEntity && metaEntity.codeAttribute !== null) {
            pushAttribute(metaEntity.codeAttribute);
        }

        if (metaEntity instanceof LookupEntity && metaEntity.aliasCodeAttributes !== null) {
            metaEntity.aliasCodeAttributes.items.forEach(pushAttribute);
        }

        metaEntity.simpleAttributes.items.forEach(pushAttribute);
        metaEntity.arrayAttributes.items.forEach(pushAttribute);

        if (metaEntity instanceof Entity || metaEntity instanceof NestedEntity) {
            metaEntity.complexAttributes.items.forEach((recordMDItem) => {
                const children: IDisplayAttr[] = [];
                const name = recordMDItem.name.getValue();
                const settingItem = settings.find((s) => s.name === name);

                if (recordMDItem.nestedEntity) {
                    this.getDisplayAttrsToEntity(children, recordMDItem.nestedEntity, recordMDItem.name.getValue());
                }

                res.push({
                    name,
                    displayName: recordMDItem.displayName.getValue(),
                    order: recordMDItem.order.getValue(),
                    description: recordMDItem.description.getValue(),
                    isShow: (settingItem !== undefined && settingItem.children !== undefined) ?
                        (settingItem.isShow === true && settingItem.children.some((item) => item.isShow === true)) :
                        children.some((item) => item.isShow === true),
                    children,
                    isRelation: false,
                    isArray: false,
                    lookupEntityType: '',
                    isSearchable: false
                });
            });
        }

        if (metaEntity instanceof Entity) {
            metaEntity.relations.items.forEach((recordMDItem) => {
                const name = recordMDItem.name.getValue();
                const settingItem = settings.find((s) => s.name === name);

                res.push({
                    name,
                    displayName: recordMDItem.displayName.getValue(),
                    description: recordMDItem.description.getValue(),
                    isShow: (settingItem !== undefined) ?
                        settingItem.isShow :
                        undefined !== recordMDItem.customProperties.find((prop) => {
                            return prop.name.getValue() === 'displayable' && prop.value.getValue() === 'true';
                        }),
                    isRelation: true,
                    isArray: false,
                    relType: recordMDItem.relType.getValue(),
                    lookupEntityType: '',
                    isSearchable: false
                });
            });
        }
    };

    /**
     * Getting a list of the main displayed attributes
     */
    public getMainVisibleAttributes = () => {

        const list: IDisplayAttr[] = [];

        const pushAttribute = (recordMDItem: SimpleAttribute | CodeAttribute) => {
            const key = (name) ? `${name}${this.DELIMITER}${recordMDItem.name.getValue()}` : recordMDItem.name.getValue();

            let isShow = recordMDItem.mainDisplayable.getValue();

            if (isShow) {
                list.push(this.getDisplayAttrFromMeta(recordMDItem, key, true, false));
            }
        };

        if (this.recordMetaData) {
            if (this.recordMetaData instanceof LookupEntity && this.recordMetaData.codeAttribute !== null) {
                pushAttribute(this.recordMetaData.codeAttribute);
            }

            this.recordMetaData.simpleAttributes.items.forEach(pushAttribute);
        }

        return list;
    }

    /**
     * Converting attribute meta data to IDisplayAttr for display in a table
     * @param recordMDItem
     * @param key
     * @param isShow
     */
    private getDisplayAttrFromMeta = (
        recordMDItem: SimpleAttribute | ArrayAttribute | CodeAttribute | AliasCodeAttribute,
        key: string,
        isShow: boolean,
        isArray: boolean
    ): IDisplayAttr => {
        return {
            name: key,
            displayName: recordMDItem.displayName.getValue(),
            order: recordMDItem.order.getValue(),
            description: recordMDItem.description.getValue(),
            isShow: isShow,
            isSearchable: recordMDItem.searchable.getValue(),
            isRelation: false,
            isArray: isArray,
            lookupEntityType: recordMDItem instanceof CodeAttribute || recordMDItem instanceof AliasCodeAttribute ?
                '' :
                recordMDItem.lookupEntityType.getValue(),
            metaAttribute: recordMDItem,
            valueType: recordMDItem.typeCategory === AttributeTypeCategory.enumDataType ?
                SIMPLE_DATA_TYPE.STRING :
                recordMDItem.typeValue,
            enumName: (recordMDItem instanceof SimpleAttribute && recordMDItem.typeCategory === AttributeTypeCategory.enumDataType) ?
                recordMDItem.typeValue :
                undefined
        };
    };
}
