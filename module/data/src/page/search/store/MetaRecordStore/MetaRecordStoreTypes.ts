/**
 * Types for the metamodel to search for
 *
 * @author: Ilya Brauer
 * @date: 2019-10-09
 */

import {
    EntityType
} from '@unidata/types';

export type EntityMeta = {
    entityName: string;
    entityDisplayName: string;
    entityType: EntityType;
}

export type CurrentEntity = EntityMeta;

export interface IDisplayAttrSettings {
    name: string;
    isShow: boolean;
    children?: IDisplayAttrSettings[];
}
