/**
 * Store for recent search queries
 *
 * @author Brauer Ilya
 * @date 2020-04-15
 */
import {BackendStorageKeys, BackendStorageService, UserManager} from '@unidata/core-app';
import {CurrentEntity} from './MetaRecordStore/MetaRecordStoreTypes';
import {action, observable} from 'mobx';
import {JsonData} from '@unidata/meta';

export type ILastSearchItem = {
    searchName: string;
    currentEntity: CurrentEntity;
    searchTerms: JsonData[];
}

export class SavedSearchStore {
    @observable
    public isLoading: boolean = false;

    @observable.shallow
    public savedSearch: ILastSearchItem[] = [];

    @action
    private setLoading (isLoading: boolean) {
        this.isLoading = isLoading;
    }

    @action
    private setSavedList (savedList: ILastSearchItem[]) {
        this.savedSearch = savedList;
    }

    public getList (): Promise<ILastSearchItem[]> {
        this.setLoading(true);

        return BackendStorageService.loadRecords(
            UserManager.getUserLogin() as string,
            BackendStorageKeys.SEARCH_TERMS
        )
            .then((result: any[]) => {
                if (result.length === 0) {
                    this.setSavedList([]);

                    return [];
                }

                try {
                    const list = JSON.parse(result[0].value);

                    this.setSavedList(list);

                    return list;
                } catch (err) {
                    this.setSavedList([]);

                    return [];
                }
            })
            .finally(() => {
                this.setLoading(false);
            });
    }

    /**
     * Saving the user's search query
     */
    public saveSearch (dataToSave: ILastSearchItem): Promise<any> {
        //TODO: now you can save searches with the same names
        // // but the output will fail. Either output correct or save
        this.setLoading(true);

        this.savedSearch.push(dataToSave);

        return BackendStorageService.saveRecord(
            UserManager.getUserLogin() as string,
            BackendStorageKeys.SEARCH_TERMS,
            JSON.stringify(this.savedSearch)
        )
            .finally(() => {
                this.setLoading(false);
            });
    }

    /**
     * remove from saved search
     *
     * @param search instance
     */
    @action
    public remove (search: ILastSearchItem): void {
        this.setLoading(true);

        const deleted = this.savedSearch.findIndex((saved) => {
            return search.searchName === saved.searchName;
        });

       if (deleted === -1) {
           return;
       }

        this.savedSearch.splice(deleted, 1);

        BackendStorageService.saveRecord(
            UserManager.getUserLogin() as string,
            BackendStorageKeys.SEARCH_TERMS,
            JSON.stringify(this.savedSearch)
                                    ).catch((e) => {
                                        console.warn(e);
                                    })
                                    .finally(() => {
                                        this.setLoading(false);
                                    });

    }
}
