/**
 * Getting a list of search attributes (for link attributes) to display in the search, in the list of criteria
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import {Entity, LookupEntity, MetaRelation, SimpleAttribute,
    SEARCH_TERM_GROUP,
    RelationST,
    AbstractSearchTerm, AbstractAttributeST
} from '@unidata/meta';
import {MetaRecordStore} from '../MetaRecordStore/MetaRecordStore';
import flatten from 'lodash/flatten';
import {getRefOptions} from '../search_panel_store/lookupRefOptions';

function createRelationST<T extends MetaRelation | Entity> (relationMeta: T): T extends MetaRelation ? RelationST : RelationST[]
function createRelationST (relationMeta: MetaRelation | Entity): RelationST | RelationST[] {

    const attributes = relationMeta.simpleAttributes.items.reduce((result: AbstractSearchTerm[], attribute: SimpleAttribute) => {
            if (attribute.searchable.getValue() === true) {
                result.push(AbstractAttributeST.createAttributeST(
                    attribute,
                    getRefOptions,
                    relationMeta.name.getValue() + '.' + attribute.name.getValue())
                );
            }

            return result;
        }, []);

    if (relationMeta instanceof Entity) {
        return relationMeta.relations.items.map((relationAttribute) => {
            return new RelationST({
                key: `$relFrom_${relationAttribute.name.getValue()}`, // A non-standard key, since it is necessary to distinguish this type of relationship
                displayName: `${relationMeta.displayName.getValue()} -> ${relationAttribute.displayName.getValue()}`,
                initDirect: 'false',
                initRelName: relationAttribute.name.getValue(),
                termGroup: SEARCH_TERM_GROUP.RELATIONS,
                entity: relationAttribute.toEntity.getValue(),
                attributes
            });
        });
    }

    return new RelationST({
        key: relationMeta.name.getValue(),
        displayName: relationMeta.displayName.getValue(),
        initDirect: 'true',
        initRelName: relationMeta.name.getValue(),
        termGroup: SEARCH_TERM_GROUP.RELATIONS,
        entity: relationMeta.fromEntity.getValue(),
        attributes
    });
}

export function getRelationTerms (metaRecordStore: MetaRecordStore): RelationST[] {
    if (!metaRecordStore.recordMetaData || metaRecordStore.recordMetaData instanceof LookupEntity) {
        return [];
    }

    return metaRecordStore.recordMetaData.relations.items.map((relItem) => createRelationST(relItem))
        .concat(flatten(metaRecordStore.relationsFrom.map((relItem) => {
            return createRelationST(relItem);
        })));

}
