/**
 * Store for the search bar
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import {MetaRecordStore} from '../MetaRecordStore/MetaRecordStore';
import {SearchResultStore} from '../SearchResultStore';

import {FavoriteStore} from '../../../../store/FavoriteStore';
import {SearchTermsRequest} from '../../../../service/operation/search/ReadDataList/ReadDataListType';
import {action, computed, IObservableArray, observable} from 'mobx';
import {
    AbstractGroupST,
    AbstractSearchTerm,
    AsOfST,
    BooleanAttributeST,
    CreateUpdateST,
    DateAttributeST,
    EnumAttributeST,
    IFormField,
    JsonData,
    NumberAttributeST,
    RefAttributeST,
    RelationST,
    SEARCH_TERM_GROUP,
    SearchQuery,
    SelectST,
    SourceST,
    StringAttributeST,
    systemVariables,
    TimeAttributeST
} from '@unidata/meta';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {UeModuleCallback, ueModuleManager, UeModuleTypeCallBack} from '@unidata/core';

export const dateTimeFormatToRequest = 'YYYY-MM-DDTHH:mm:ss.SSS';

export const dateFormatToRequest = 'YYYY-MM-DDT00:00:00.000';

export class SearchPanelStore {

    private queryTimer: number;
    private requestTimeout: number = 800;

    public favoriteStore: FavoriteStore = new FavoriteStore();
    public metaRecordStore: MetaRecordStore = new MetaRecordStore(this.favoriteStore);
    public searchResultStore: SearchResultStore = new SearchResultStore(this.metaRecordStore, this);

    private QualityST = ueModuleManager
        .getModuleById('dq_search_term_model') as UeModuleCallback<UeModuleTypeCallBack.SEARCH_TERM_MODEL> | null;
    private ClassifyST = ueModuleManager
        .getModuleById('clsf_search_term_model') as UeModuleCallback<UeModuleTypeCallBack.SEARCH_TERM_MODEL> | null;

    @observable
    public keyword: string = '';

    @observable.ref
    public searchTerms: IObservableArray<AbstractSearchTerm> = observable([]);

    public handleClear?: () => void;

    @action
    public setKeyword = (keyword: string) => {
        this.keyword = keyword;
    };

    @action
    public clearSearch = () => {
        this.searchResultStore.clearStore();

        this.searchTerms.clear();

        if (this.handleClear) {
            this.handleClear();
        }
    };

    @computed
    public get mapST () {

        return Object.values(SEARCH_TERM_GROUP).reduce<{[key in SEARCH_TERM_GROUP]: AbstractSearchTerm[]}>((result, key) => {
            result[key] = this.searchTerms.filter((item) => {
                return item.termGroup === key;
            });

            return result;
        }, {
            [SEARCH_TERM_GROUP.ATTRIBUTE]: [],
            [SEARCH_TERM_GROUP.CLASSIFY]: [],
            [SEARCH_TERM_GROUP.QUALITY]: [],
            [SEARCH_TERM_GROUP.RELATIONS]: [],
            [SEARCH_TERM_GROUP.SYSTEM]: []
        });
    }

    public get listSTJson () {
        return this.searchTerms.map((searchTerm) => searchTerm.getJsonData());
    }

    @computed
    public get listST (): AbstractSearchTerm[] {
        return this.searchTerms;
    }

    @action
    public addTerm = (term: AbstractSearchTerm | JsonData) => {
        if (term instanceof AbstractSearchTerm) {
            if (term.termGroup !== SEARCH_TERM_GROUP.SYSTEM) {
                this.keyword = '';
            }

            this.searchTerms.push(term);
        } else {
            try {
                const newTerm = this.createST(term);

                this.searchTerms.push(newTerm);
            } catch (e) {
                console.error(e);
            }
        }
    };

    private createST (term: JsonData): AbstractSearchTerm {
        const {formFields, facets, supplementaryRequest, constructorName, json, ...termData} = term;

        const ctorMap: any = {
            BooleanAttributeST: BooleanAttributeST,
            DateAttributeST: DateAttributeST,
            EnumAttributeST: EnumAttributeST,
            NumberAttributeST: NumberAttributeST,
            RefAttributeST: RefAttributeST,
            StringAttributeST: StringAttributeST,
            TimeAttributeST: TimeAttributeST,
            CreateUpdateST: CreateUpdateST
        };

        switch (constructorName) {
            case 'AsOfST': {
                const asOfST = new AsOfST({
                    ...termData,
                    type: SIMPLE_DATA_TYPE.DATE
                });

                const from = formFields.find((item) => item.name === systemVariables.$from);
                const to = formFields.find((item) => item.name === systemVariables.$to);
                const rangeFrom = from && from.range ? (from.range[1] || null) : null;
                const rangeTo = to && to.range ? (to.range[0] || null) : null;

                asOfST.changeItem({
                    ...formFields[0],
                    range: [rangeFrom, rangeTo]
                });

                return asOfST;
            }
            case 'SelectST': {
                return new SelectST({
                    ...termData,
                    value: facets.length > 0 ? facets :
                        formFields.map((formField: IFormField) => formField && formField.value ? formField.value.toString() : ''),
                    ...json
                });
            }
            case 'RelationST': {
                const {attributes, ...initData} = json;

                return new RelationST({
                        ...termData,
                        ...initData,
                        attributes: attributes.map((term: any) => this.createST(term))
                    });
            }
            case 'SourceST': {
                const {attributes, ...initData} = json;

                return new SourceST({
                    ...termData,
                    ...initData,
                    attributes: attributes.map((term: any) => this.createST(term))
                });
            }
            case 'ClassifyST': {
                if (this.ClassifyST && this.ClassifyST.default.meta.modelCtor) {
                    const {attributes, ...initData} = json;

                    const classifyST = new this.ClassifyST.default.meta.modelCtor({
                        ...termData,
                        ...initData
                    });

                    attributes.forEach((attribute: any, index: number) => {
                        classifyST.addItem(this.createST(attribute));
                    });

                    return classifyST;
                }

                throw new Error(`No constructor for ${constructorName}`);
            }
            case 'QualityST': {
                if (this.QualityST && this.QualityST.default.meta.modelCtor) {
                    const {attributes, ...initData} = json;

                    const qualityST = new this.QualityST.default.meta.modelCtor({
                        ...termData,
                        ...initData
                    });

                    attributes.forEach((attribute: any, index: number) => {
                        qualityST.addItem(this.createST(attribute));
                    });

                    return qualityST;
                }

                throw new Error(`No constructor for ${constructorName}`);
            }
            default: {
                if (ctorMap[constructorName] === undefined) {
                    throw new Error(`No constructor for ${constructorName}`);
                }

                return new ctorMap[constructorName]({
                    ...termData,
                    items: formFields,
                    options: json.options || []
                });
            }
        }
    }

    @action
    public deleteTerm = (term: AbstractSearchTerm) => {
        const index = this.searchTerms.findIndex((st) => st.id === term.id);

        this.searchTerms.splice(index, 1);
    };

    @action
    public deleteAllTerms = () => {
        this.searchTerms.clear();
    };

    public getAllTermsCount = () => {
        let count = this.searchTerms.length;

        this.searchTerms.forEach((searchTerm) => {
            if (searchTerm instanceof AbstractGroupST) {
                count = count + searchTerm.itemsData.length;
            }
        });

        return count;
    };

    public hasPrev = (key: SEARCH_TERM_GROUP) => {
        switch (key) {
            case SEARCH_TERM_GROUP.CLASSIFY: {
                return this.mapST[SEARCH_TERM_GROUP.ATTRIBUTE].length > 0;
            }
            case SEARCH_TERM_GROUP.QUALITY: {
                return this.mapST[SEARCH_TERM_GROUP.ATTRIBUTE].length > 0 || this.mapST[SEARCH_TERM_GROUP.CLASSIFY].length > 0;
            }
            case SEARCH_TERM_GROUP.RELATIONS: {
                return this.mapST[SEARCH_TERM_GROUP.ATTRIBUTE].length > 0 || this.mapST[SEARCH_TERM_GROUP.CLASSIFY].length > 0 ||
                    this.mapST[SEARCH_TERM_GROUP.QUALITY].length > 0;
            }
            case SEARCH_TERM_GROUP.SYSTEM: {
                return this.mapST[SEARCH_TERM_GROUP.ATTRIBUTE].length > 0 || this.mapST[SEARCH_TERM_GROUP.CLASSIFY].length > 0 ||
                    this.mapST[SEARCH_TERM_GROUP.QUALITY].length > 0 || this.mapST[SEARCH_TERM_GROUP.RELATIONS].length > 0;

            }
            default: {
                return false;
            }
        }
    };

    public doRequest = () => {
        const searchQuery = this.getQuery();

        if (this.metaRecordStore.currentEntity) {
            this.searchResultStore.setSearchQuery(searchQuery);

            return this.searchResultStore.getRecords();
        }

        return Promise.reject('There is no currentEntity in metaRecordStore');
    };

    public clearQueryTimer = () => {
        if (this.queryTimer) {
            clearTimeout(this.queryTimer);
        }
    };

    /**
     * building a query to the server based on terms
     */
    public getQuery = (): SearchTermsRequest => {
        const searchBuilder = new SearchQuery();

        searchBuilder.setKeyword(this.keyword);

        this.listST.forEach((searchTerm) => {
            const query = searchTerm.getQuery();

            if (searchTerm instanceof AsOfST) {
                searchBuilder.setAsOf(query);
            } else {
                searchBuilder.add(query);
            }
        });

        return {
            ...searchBuilder.getSearchQuery(),
            searchFields: [
                systemVariables.$etalonId,
                systemVariables.$from,
                systemVariables.$to
            ].concat(this.metaRecordStore.attributes.filter(item => item.isSearchable).map((item) => item.name)),
            returnFields: [
                systemVariables.$etalonId,
                systemVariables.$from,
                systemVariables.$to
            ].concat(this.metaRecordStore.attributes.reduce<string[]>((result, item) => {
                if (item.children) {
                    item.children.forEach((childItem) => {
                        result.push(childItem.name);
                    });
                }

                result.push(item.name);

                return result;
            }, []))
        };
    };

    public doRequestAwait = () => {
        this.clearQueryTimer();

        this.queryTimer = window.setTimeout(() => {
            this.doRequest();
        }, this.requestTimeout);
    };
}
