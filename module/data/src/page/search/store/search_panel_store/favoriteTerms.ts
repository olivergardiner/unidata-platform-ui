/**
 * Creating searchTerms of the Favorite (and Date_Range) type based on the passed etalon_id list
 *
 * @author Brauer Ilya
 * @date 2020-03-03
 */

import {SIMPLE_DATA_TYPE} from '@unidata/types';
import moment from 'moment';
import {dateTimeFormatToRequest} from './SearchPanelStore';
import {i18n} from '@unidata/core-app';
import {DateAttributeST, StringAttributeST, SEARCH_TERM_GROUP} from '@unidata/meta';

export function makeFavoriteSearchTerms (favoriteEtalonsMap: string[]) {
    const etalonTerm = new StringAttributeST({
        key: '$etalon_id',
        items: favoriteEtalonsMap.map((etalonItem) => {
            return {
                value: etalonItem
            };
        }),
        termGroup: SEARCH_TERM_GROUP.ATTRIBUTE,
        displayName: i18n.t('module.data>search.panel>favorite>favoriteRecords')
    });

    const now = moment().format(dateTimeFormatToRequest);

    const rangeTerm = new DateAttributeST({
        key: 'actual',
        type: SIMPLE_DATA_TYPE.DATE,
        termGroup: SEARCH_TERM_GROUP.SYSTEM,
        displayName: i18n.t('module.data>search.panel>systemDisplayName>actual'),
        items: [{
            range: [now, now]
        }]
    });

    return {etalonTerm, rangeTerm};
}
