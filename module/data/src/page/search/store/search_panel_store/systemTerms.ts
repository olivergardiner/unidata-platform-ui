/**
 * Getting a list of system attributes(date of creation/change, etc.) to display in the search, in the list of criteria
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import {DateGranularityMode, SIMPLE_DATA_TYPE} from '@unidata/types';

import {MetaRecordStore} from '../MetaRecordStore/MetaRecordStore';
import {i18n} from '@unidata/core-app';
import {
    AsOfST,
    CreateUpdateST,
    FACETS,
    SEARCH_TERM_GROUP,
    SelectST,
    SourceST
} from '@unidata/meta';

export function getSystemTerms (metaRecordStore: MetaRecordStore) {
    return [
        new AsOfST({
            key: 'actual',
            displayName: i18n.t('module.data>search.panel>systemDisplayName>actual'),
            termGroup: SEARCH_TERM_GROUP.SYSTEM,
            type: (metaRecordStore.recordMetaData?.getGranularityMode() === DateGranularityMode.DATETIME ||
                metaRecordStore.recordMetaData?.getGranularityMode() === DateGranularityMode.DATETIMEMILLIS) ?
                SIMPLE_DATA_TYPE.TIMESTAMP : SIMPLE_DATA_TYPE.DATE
        }),
        new CreateUpdateST({
            key: '$created_at',
            displayName: i18n.t('module.data>search.panel>systemDisplayName>createdAt'),
            type: SIMPLE_DATA_TYPE.DATE,
            termGroup: SEARCH_TERM_GROUP.SYSTEM
        }),
        new CreateUpdateST({
            key: '$updated_at',
            displayName: i18n.t('module.data>search.panel>systemDisplayName>updatedAt'),
            type: SIMPLE_DATA_TYPE.DATE,
            termGroup: SEARCH_TERM_GROUP.SYSTEM
        }),
        new SelectST({
            key: 'type',
            displayName: i18n.t('module.data>search.panel>systemDisplayName>type'),
            options: [
                {title: i18n.t('module.data>search.panel>attributesMeta>facets>direct'), value: FACETS.OPERATION_TYPE_DIRECT},
                {title: i18n.t('module.data>search.panel>attributesMeta>facets>cascaded'), value: FACETS.OPERATION_TYPE_CASCADED},
                {title: i18n.t('module.data>search.panel>attributesMeta>facets>copy'), value: FACETS.OPERATION_TYPE_COPY}
            ],
            value: [],
            termGroup: SEARCH_TERM_GROUP.SYSTEM
        }),
        new SelectST({
            key: 'periodAct',
            displayName: i18n.t('module.data>search.panel>systemDisplayName>periodAct'),
            options: [
                {title: i18n.t('module.data>search.panel>attributesMeta>facets>activePeriods'), value: FACETS.INCLUDE_ACTIVE_PERIODS},
                {title: i18n.t('module.data>search.panel>attributesMeta>facets>inactivePeriods'), value: FACETS.INCLUDE_INACTIVE_PERIODS}
            ],
            value: [],
            termGroup: SEARCH_TERM_GROUP.SYSTEM
        }),
        new SelectST({
            key: 'inactive',
            displayName: i18n.t('module.data>search.panel>systemDisplayName>records'),
            isMulti: true,
            options: [
                {title: i18n.t('module.data>search.panel>attributesMeta>facets>inactivePeriods'), value: FACETS.INACTIVE_ONLY},
                {title: i18n.t('module.data>search.panel>attributesMeta>facets>activePeriods'), value: FACETS.ACTIVE_ONLY}
            ],
            value: [],
            termGroup: SEARCH_TERM_GROUP.SYSTEM
        }),
        new SelectST({
            key: 'pending',
            displayName: i18n.t('module.data>search.panel>systemDisplayName>pending'),
            options: [
                {title: i18n.t('module.data>search.panel>attributesMeta>yes'), value: FACETS.PENDING_ONLY},
                {title: i18n.t('module.data>search.panel>attributesMeta>no'), value: ''}
            ],
            value: [],
            termGroup: SEARCH_TERM_GROUP.SYSTEM
        }),
        new SourceST({
            key: 'source',
            displayName: i18n.t('module.data>search.panel>systemDisplayName>sources'),
            termGroup: SEARCH_TERM_GROUP.SYSTEM,
            entity: metaRecordStore.currentEntity.entityName,
            attributes: []
        })
    ];
}
