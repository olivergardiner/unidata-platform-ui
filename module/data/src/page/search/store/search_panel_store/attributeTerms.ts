/**
 * Getting a list of search attributes to display in the search, in the list of criteria
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import {MetaRecordStore} from '../MetaRecordStore/MetaRecordStore';
import {AbstractSearchTerm, AbstractAttributeST} from '@unidata/meta';
import {getRefOptions} from './lookupRefOptions';

export type GroupSearchTerm = AbstractSearchTerm & {
    children?: AbstractSearchTerm[];
};

export function getAttributeTerms (metaRecordStore: MetaRecordStore): AbstractSearchTerm[] {
    if (!metaRecordStore.recordMetaData) {
        return [];
    }

    return metaRecordStore.attributes
        .sort(metaRecordStore.columnsSort)
        .reduce((result: GroupSearchTerm[], attribute) => {
            if (attribute.isSearchable && attribute.valueType !== undefined && attribute.metaAttribute !== undefined) {
                result.push(AbstractAttributeST.createAttributeST(attribute.metaAttribute, getRefOptions));
            } else if (attribute.children && attribute.children.length > 0 && attribute.metaAttribute !== undefined) {

                const groupTermData: GroupSearchTerm = AbstractAttributeST.createAttributeST(attribute.metaAttribute, getRefOptions);

                groupTermData.children = attribute.children
                    .sort(metaRecordStore.columnsSort)
                    .reduce((res: AbstractSearchTerm[], attribute) => {
                        if (attribute.isSearchable && attribute.valueType !== undefined && attribute.metaAttribute !== undefined) {
                            res.push(AbstractAttributeST.createAttributeST(attribute.metaAttribute, getRefOptions));
                        }

                        return res;
                    }, []);

                if (groupTermData.children && groupTermData.children.length > 0) {
                    result.push(groupTermData);
                }
            }

            return result;
        }, []);
}
