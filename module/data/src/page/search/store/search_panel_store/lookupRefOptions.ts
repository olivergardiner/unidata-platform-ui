/**
 * Getting a list for a select by lookup
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import {
    MetaRecordService,
    LookupEntity,
    CodeAttribute
} from '@unidata/meta';
import {SearchService} from '../../../../service/SearchService';
import {SearchPreview} from '@unidata/core-app';

/**
 * Getting a list for a select by lookup
 */
export function getRefOptions (lookupEntityType: string, lookupEntityDisplayAttributes: string[], lookupEntitySearchAttributes: string[]) {
    let returnFields: string[] = [];
    let searchFields: string[] = [];
    let codeField = '';

    return () => MetaRecordService.getLookupEntityMetaRecord(lookupEntityType)
        .then((result: LookupEntity) => {
            result.simpleAttributes.items.forEach((attribute) => {
                if (attribute.mainDisplayable.getValue() === true) {
                    returnFields.push(attribute.name.getValue());
                }

                if (attribute.searchable.getValue() === true) {
                    searchFields.push(attribute.name.getValue());
                }
            });

            if (lookupEntityDisplayAttributes.length > 0) {
                returnFields = lookupEntityDisplayAttributes;
            }

            if (lookupEntitySearchAttributes.length > 0) {
                searchFields = lookupEntitySearchAttributes;
            }

            codeField = (result.codeAttribute as CodeAttribute).name.getValue();

            return;
        })
        .then(() => {
            return SearchService.getSearchResult({
                entity: lookupEntityType,
                returnFields: returnFields.concat(codeField),
                searchFields: searchFields.concat(codeField),
                sortFields: [],
                countOnly: false,
                fetchAll: true,
                page: 1,
                start: 0,
                count: 50
            });
        })
        .then((list) => {
            const previewFields = returnFields.concat(codeField);

            return list.searchRecords.map((searchRecord) => {
                const currentCodeField = searchRecord.preview.items.find((preview: any) => preview.field.getValue() === codeField);

                let codeValue = '';

                if (currentCodeField) {
                    codeValue = currentCodeField.values.getValue()[0];
                }

                return {
                    title: searchRecord.preview.items.reduce<string[]>((result, preview: SearchPreview) => {
                        if (previewFields.includes(preview.field.getValue())) {
                            result.push(preview.values.getValue().join(', '));
                        }

                        return result;
                    }, []).join(' | '),
                    value: codeValue
                };
            });
        });
}
