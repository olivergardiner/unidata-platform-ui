/**
 * Store for pagination
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import {action, computed, observable} from 'mobx';

export interface IPagination {
    currentPage: number;
    pageSize: number;
    totalRecords: number;
    getList: () => void;
}

export class PaginationStore {

    private defaultPageSize = 20;

    @observable
    public currentPage: number;

    @observable
    public totalRecords: number;

    @observable
    public pageSize: number;

    public getList: () => void;

    constructor (options: IPagination) {
        this.currentPage = options.currentPage;
        this.pageSize = options.pageSize || this.defaultPageSize;
        this.totalRecords = options.totalRecords;
        this.getList = options.getList;
    }

    @computed
    public get totalPages () {
        return Math.ceil(this.totalRecords / this.pageSize);
    }

    @action
    public setPage = (page: number) => {
        this.currentPage = page;
        this.getList();
    }

    @action
    public setPageSize = (pageSize: number) => {
        this.pageSize = pageSize;
        this.getList();
    }
}
