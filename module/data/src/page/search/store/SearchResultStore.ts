/**
 * Storage for the results of the search according
 *
 * @author: Ilya Brauer
 * @date: 2019-10-08
 */

import {action, computed, get, IObservableArray, observable, runInAction, set, toJS} from 'mobx';

import {SearchService} from '../../../service/SearchService';
import {IStringKeyMap} from '@unidata/core';
import {SearchHitUtil} from '../../../utils/SearchHitUtil';
import {MetaRecordStore} from './MetaRecordStore/MetaRecordStore';
import {PaginationStore} from './PaginationStore';
import {ISortField, ORDER, SearchTermsRequest} from '../../../service/operation/search/ReadDataList/ReadDataListType';
import {ISearchRelation} from '../../../service/operation/search/ReadRelationList/ReadRelationListType';
import {ARRAY_DATA_TYPE, CODE_DATA_TYPE, SIMPLE_DATA_TYPE} from '@unidata/types';
import {Dialog, SearchHit} from '@unidata/core-app';
import {DataRelationService} from '../../../service/DataRelationService';
import {SearchPanelStore} from './search_panel_store/SearchPanelStore';
import {FACETS, IDisplayAttr} from '@unidata/meta';

export class SearchResultStore {

    private searchRequest: SearchTermsRequest;

    public canChooseAll: boolean = true;

    @observable
    public hasPeriod: boolean = false;

    @observable
    public isLoading: boolean = false;

    @observable
    public records: SearchHit[] = [];

    @observable.shallow
    public relations: ISearchRelation[] = [];

    @observable
    public checkedRecords: IObservableArray<number> = observable([]);

    @observable
    public checkAll: boolean = false;

    @observable
    public sortFields: IObservableArray<ISortField> = observable([]);

    private metaRecordStore: MetaRecordStore;
    private searchPanelStore: SearchPanelStore | undefined;

    constructor (metaRecordStore: MetaRecordStore, searchPanelStore?: SearchPanelStore) {
        this.metaRecordStore = metaRecordStore;
        this.searchPanelStore = searchPanelStore;
    }

    @action
    public getRecords = () => {
        this.isLoading = true;

        return SearchService.getSearchResult({
            ...this.searchRequest,
            entity: this.metaRecordStore.currentEntity.entityName,
            sortFields: toJS<ISortField[]>(this.sortFields),
            page: this.paginationStore.currentPage,
            count: this.paginationStore.pageSize,
            start: (this.paginationStore.currentPage - 1) * this.paginationStore.pageSize
        })
            .then((data) => {
                runInAction(() => {
                    // master record
                    this.records = data.searchRecords;
                    this.paginationStore.totalRecords = data.totalCount;
                });
            })
            .then(() => {
                if (this.records.length > 0) {
                    return this.getRelations();
                }

                return Promise.resolve();
            })
            .catch((err: Error) => {
                Dialog.showError(err.message, err.name); //TODO: pass all the necessary values and change the type
                runInAction(() => {
                    this.paginationStore.totalRecords = 0;
                });
            })
            .finally(() => {
                runInAction(() => {
                    this.isLoading = false;
                });
            });
    };

    /**
     * Method for updating the table from the pagination row.
     */
    private reload = () => {
        // if there is a store with search terms, you must re-create the search query
        if (this.searchPanelStore !== undefined) {
            this.searchPanelStore.doRequest();
        } else {
            this.getRecords();
        }
    };

    @observable
    public paginationStore = new PaginationStore({currentPage: 1, pageSize: 20, totalRecords: 0, getList: this.reload});

    public getRelations = (attributes: IDisplayAttr[] = this.metaRecordStore.attributes) => {
        const relationNames = (attributes || [])
            .filter((attribute) => attribute.isRelation === true && attribute.isShow === true)
            .map((attribute) => attribute.name);

        if (relationNames.length === 0 || !this.metaRecordStore.currentEntity.entityName) {
            return;
        }

        const fromEtalonIds = this.records.map((searchItem) => {
            return {
                etalonId: SearchHitUtil.getFieldValues<string>(searchItem, '$etalon_id')[0],
                from: SearchHitUtil.getFieldValues<string>(searchItem, '$from')[0] || null,
                to: SearchHitUtil.getFieldValues<string>(searchItem, '$to')[0] || null
            };
        });

        const relation = {
            entity: this.metaRecordStore.currentEntity.entityName,
            maxCount: 5,
            relationNames,
            fromEtalonIds: fromEtalonIds
        };

        return DataRelationService.getResultRelation(relation)
            .then((result) => {
                runInAction(() => {
                    this.relations = result.relations;
                });
            });
    };

    @computed
    public get searchHit (): IStringKeyMap[] {
        return this.records;
    }

    @action
    public checkRow = (rowIndex: number) => {
        this.checkedRecords.push(rowIndex);
    };

    @action
    public uncheckRow = (rowIndex: number) => {
        this.checkedRecords.remove(rowIndex);
        this.checkAll = false;
    };

    @action
    public toggleCheckAll = (isChecked: boolean) => {

        this.checkAll = isChecked;

        if (this.checkAll === false) {
            this.checkedRecords.clear();
        } else {
            this.records.forEach((record, index) => {
                this.checkedRecords.push(index);
            });
        }
    };

    public isRecordsChecked (): boolean {
        for (let checkedRecordsKey in this.checkedRecords) {
            if (this.checkedRecords[checkedRecordsKey]) {

                return true;
            }
        }

        return false;
    }

    @action
    public clearStore = () => {
        this.records = [];
        this.sortFields.clear();
        this.checkedRecords.clear();
        this.checkAll = false;
    };

    @action
    public setSearchQuery (searchQuery: SearchTermsRequest) {
        this.searchRequest = searchQuery;
        this.hasPeriod = Boolean(searchQuery.facets && searchQuery.facets.find((facet: FACETS) => FACETS.UN_RANGED) !== undefined);
        this.records = [];
        this.relations = [];
    }

    public getSearchFacets () {
        return this.searchRequest ? this.searchRequest.facets : [];
    }

    /**
     * Sortable by clicking on the table header for the field
     *
     * @param field field name
     * @param type type of data to send to the server
     */
    @action
    public sortByField = (field: string, type: SIMPLE_DATA_TYPE | ARRAY_DATA_TYPE | CODE_DATA_TYPE) => {
        const sortIndex = this.sortFields.findIndex((item) => item.field === field);

        let order = ORDER.DESC;

        if (sortIndex === -1) {
            this.sortFields.push({field, order, type});
        } else {
            const currentSort = this.sortFields[sortIndex];

            if (currentSort.order === ORDER.DESC) {
                order = ORDER.ASC;
                this.sortFields[sortIndex].order = order;
            } else {
                this.sortFields.remove(currentSort);
            }
        }
    }
}
