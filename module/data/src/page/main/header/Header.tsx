/**
 * Component for displaying the header of the main page
 *
 * @author: Ilya Brauer
 * @date: 2020-08-07
 */

import * as React from 'react';
import {Button, INTENT, PageHeader, Tooltip} from '@unidata/uikit';
import {i18n, ResourceManager, Right} from '@unidata/core-app';
import {observer} from 'mobx-react';
import {MetaRecordStore} from '../../search/store/MetaRecordStore/MetaRecordStore';

interface IProps {
    metaRecordStore: MetaRecordStore;
}

@observer
export class Header extends React.Component<IProps> {
    createRecord = () => {
        // todo Brauer Ilya: create record without currentEntity on main page
    };

    render () {
        const {currentEntity} = this.props.metaRecordStore;
        const isEmptyCurrentEntity = (currentEntity.entityName === '');
        const isUserCanCreate = ResourceManager.userHasRight(currentEntity.entityName, Right.CREATE);
        const createRecordDisabled = isEmptyCurrentEntity || !isUserCanCreate;

        return (
            <PageHeader
                sectionTitle={i18n.t('module.data>mainPage>title')}
                groupSectionTitle={i18n.t('module.data>mainPage>groupTitle')}
                iconType={'home'}
                extraButtons={(
                    <Button
                        leftIcon={'plus'}
                        intent={INTENT.PRIMARY}
                        isRound={true}
                        data-qaid='createRecord'
                        onClick={this.createRecord}
                        isDisabled={createRecordDisabled}
                        title={i18n.t('module.data>mainPage>createRecordTooltip')}
                    >
                        {i18n.t('module.data>search>createRecord')}
                    </Button>
                )}
            />
        );
    }
}
