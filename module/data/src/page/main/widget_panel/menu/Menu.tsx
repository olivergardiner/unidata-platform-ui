import * as React from 'react';
import * as styles from './menu.m.scss';
import {
    i18n,
    MenuManager,
    UserManager,
    MagicProperties,
    IMenuGroup,
    AppTypeManager,
    IMenuItem
} from '@unidata/core-app';
import {MenuItem, Scrollable} from '@unidata/uikit';
import {Link} from 'react-router-dom';
import {UrlRoutes} from '../../../index';

export class Menu extends React.Component<{}, {}> {

    userIsDataStewardOnly () {
        return AppTypeManager.isDataSteward() && !AppTypeManager.isDataAdmin() && !AppTypeManager.isSystemAdmin();
    }

    isShowMenu = () => {
        if (this.userIsDataStewardOnly()) {
            return true;
        }

        const user = UserManager.getUser();

        if (user !== null) {
            const propKey = MagicProperties.SHOW_MENU_ON_START;
            const propResolver = MagicProperties.resolvers[propKey];
            const showMenuOnStartProp = user.properties.find((prop) => prop.name === propKey);

            return showMenuOnStartProp !== undefined ? propResolver(showMenuOnStartProp.value) : false;
        }

        return false;
    };

    filterMainPage = (menuItem: IMenuItem): boolean => {
        return menuItem.route !== UrlRoutes.MainPage;
    };

    renderLongList = (menuList: IMenuGroup[]) => {
        return (
            <div
                className={styles.menuContainer}
            >
                <Scrollable
                    autoHeight={true}
                    autoHide={true}
                    // Hide delay in ms
                    autoHideTimeout={1000}
                    // Duration for hide animation in ms.
                    autoHideDuration={200}
                >
                    <div
                        className={styles.itemsContainer}
                    >
                        {menuList.map((menuGroup, index) => {
                            return (
                                <div key={menuGroup.name} className={styles.groupContainer}>
                                    <div className={styles.groupTitle}>
                                        {i18n.t(`page.header>${menuGroup.name}`)}
                                    </div>
                                    {index > 0 && (<div className={styles.groupSep}/>)}
                                    {menuGroup.items.filter(this.filterMainPage).map((menuItem) => {
                                        return (
                                            <div key={menuItem.name} className={styles.itemContainer}>
                                                <Link to={menuItem.route}>
                                                    <MenuItem
                                                        icon={menuItem.icon as any}
                                                        title={i18n.t(`app.menu>${menuItem.name}`)}
                                                        menuColor={menuItem.menuColor as any}
                                                    />
                                                </Link>

                                            </div>
                                        );
                                    })}
                                </div>
                            );
                        })}
                    </div>
                </Scrollable>
                <div className={styles.leftGradient}/>
                <div className={styles.rightGradient}/>
            </div>
        );
    };

    renderShortList = (menuList: IMenuGroup[]) => {
        return (
            <div
                className={styles.menuContainer}
            >
                <div
                    className={styles.itemsContainer}
                >
                    {menuList.map((menuGroup, index) => {
                        return (
                            <div key={menuGroup.name} className={styles.groupContainer}>
                                <div className={styles.groupTitle}>
                                    {i18n.t(`page.header>${menuGroup.name}`)}
                                </div>
                                {index > 0 && (<div className={styles.groupSep}/>)}
                                {menuGroup.items.filter(this.filterMainPage).map((menuItem) => {
                                    return (
                                        <Link key={menuItem.name} to={menuItem.route} className={styles.buttonMenu}>
                                            <MenuItem
                                                icon={menuItem.icon as any}
                                                menuColor={menuItem.menuColor as any}
                                            />
                                            <span>{i18n.t(`app.menu>${menuItem.name}`)}</span>
                                        </Link>
                                    );
                                })}
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    };

    render () {
        const menuList = MenuManager.getMenuList();
        const showMenu = this.isShowMenu();

        if (!showMenu) {
            return null;
        }

        return this.userIsDataStewardOnly() ? this.renderShortList(menuList) : this.renderLongList(menuList);
    }
}
