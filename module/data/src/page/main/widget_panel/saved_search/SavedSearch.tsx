/**
 * A widget that contains a saved search queries
 *
 * @author Brauer Ilya
 * @date 2020-03-02
 */

import * as React from 'react';
import {CardPanel, LinkWrapper, Spinner, Icon, Confirm} from '@unidata/uikit';
import {ILastSearchItem, SavedSearchStore} from '../../../search/store/SavedSearchStore';
import {inject, observer} from 'mobx-react';
import {Link} from 'react-router-dom';
import * as styles from './savedSearch.m.scss';
import {observable, action, runInAction} from 'mobx';
import {i18n} from '@unidata/core-app';

interface IInjectedProps {
    savedSearchStore: SavedSearchStore;
}

type IProps = {} & Partial<IInjectedProps>;

@inject('savedSearchStore')
@observer
export class SavedSearch extends React.Component<IProps> {

    @observable
    isEditable: boolean = false;

    @observable
    isRemoveConfirmOpen: boolean = false;

    removedSearch: ILastSearchItem;

    get propsWithInject () {
        return this.props as IProps & IInjectedProps;
    }

    componentDidMount (): void {
        this.propsWithInject.savedSearchStore.getList();
    }

    renderColumn (items: ILastSearchItem[]) {
        return (
            <div className={styles.column}>
                {items.map((searchItem) => {
                    return (
                        <div key={searchItem.searchName} className={styles.item}>
                            {this.isEditable === true ? <Icon name={'delete'} onClick={this.onRemoveClick(searchItem)}/> : null}
                            <LinkWrapper
                                text={searchItem.currentEntity.entityDisplayName}
                                link={
                                    <Link
                                        to={{
                                            pathname: `/search/${searchItem.currentEntity.entityName}`,
                                            state: searchItem
                                        }}
                                    >
                                        {searchItem.searchName}
                                    </Link>
                                }
                            />
                        </div>
                    );
                })}
            </div>
        );
    }

    renderCardBody () {
        const savedSearchStore = this.propsWithInject.savedSearchStore;

        return savedSearchStore.savedSearch.length === 0 ?
            (<div>{i18n.t('module.data>search>widgets>savedSearch>noSearch')}</div>) :
            (
                <div className={styles.columns}>
                    {this.renderColumn(savedSearchStore.savedSearch.slice(0, 10))}
                    {this.renderColumn(savedSearchStore.savedSearch.slice(10))}
                </div>
            );
    }

    @action
    toggleEditable = (e: React.SyntheticEvent<HTMLOrSVGElement>) => {
        e.stopPropagation();
        this.isEditable = !this.isEditable;
    };

    onRemoveClick = (search: ILastSearchItem) => {
        return (e: React.SyntheticEvent<HTMLOrSVGElement>) => {
            e.stopPropagation();
            runInAction(() => {
                this.removedSearch = search;
                this.isRemoveConfirmOpen = true;
            });
        };
    };

    @action
    onRemove = () => {
        this.propsWithInject.savedSearchStore.remove(this.removedSearch);
        this.isRemoveConfirmOpen = false;
    };

    @action
    hideRemoveConfirmOpen = () => {
        this.isRemoveConfirmOpen = false;
    };

    render () {
        const savedSearchStore = this.propsWithInject.savedSearchStore;

        return (
            <CardPanel
                title={i18n.t('module.data>search>widgets>savedSearch>cardTitle')}
                extraButtons={(
                    <Icon
                        name={this.isEditable === true ? 'filledSetting' : 'setting'}
                        onClick={this.toggleEditable}
                        title={i18n.t('module.data>search>widgets>savedSearch>settingsTooltip')}
                    />
                )}
                isCollapsed={false}
                noBodyPadding={true}
            >
                <div className={styles.container}>
                    {savedSearchStore.isLoading ? (<Spinner/>) : this.renderCardBody()}
                </div>
                <Confirm
                    onConfirm={this.onRemove}
                    confirmationMessage={i18n.t('module.data>search>widgets>savedSearch>remove')}
                    isOpen={this.isRemoveConfirmOpen}
                    onClose={this.hideRemoveConfirmOpen}
                />
            </CardPanel>
        );
    }
}
