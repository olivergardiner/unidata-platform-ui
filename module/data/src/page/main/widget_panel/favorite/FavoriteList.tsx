/**
 * List of registries with favorite entries in them and the ability to view their list
 *
 * @author Brauer Ilya
 * @date 2020-06-01
 */

import * as React from 'react';
import {CardPanel, GROUP_TYPE} from '@unidata/uikit';
import {observer} from 'mobx-react';
import {MetaRecordStore} from '../../../search/store/MetaRecordStore/MetaRecordStore';
import {FavoriteStore} from '../../../../store/FavoriteStore';
import {FavoriteTable} from './FavoriteTable';
import {i18n} from '@unidata/core-app';

interface IProps {
    favoriteStore: FavoriteStore;
    metaRecordStore: MetaRecordStore;
}

interface IState {
    expandFavoriteName: string;
}

@observer
export class FavoriteList extends React.Component<IProps, IState> {

    state = {
        expandFavoriteName: ''
    };

    onToggleFavoriteItem = (entityName: string) => (isCollapsed: boolean) => {
        if (isCollapsed === false) {
            this.setState({expandFavoriteName: entityName});
        }
    };

    render () {
        return (
            <CardPanel.Group groupType={GROUP_TYPE.VERTICAL}>
                {this.props.favoriteStore.favoriteEntities.length === 0 && (
                    <div>{i18n.t('module.data>search>widgets>favorite>noFavorite')}</div>
                )}
                {this.props.favoriteStore.favoriteEntities.map((favoriteItem) => {
                    const isCollapsed = this.state.expandFavoriteName === '' ||
                        this.state.expandFavoriteName !== favoriteItem.entityName;

                    return (
                        <CardPanel
                            key={favoriteItem.entityName}
                            title={<div>{favoriteItem.entityDisplayName} ({favoriteItem.count})</div>}
                            isCollapsed={isCollapsed}
                            onToggleCollapse={this.onToggleFavoriteItem(favoriteItem.entityName)}
                            mountOnExpand={true}
                            noBodyPadding={true}
                        >
                            <FavoriteTable
                                favoriteEntity={favoriteItem}
                                favoriteStore={this.props.favoriteStore}
                            />
                        </CardPanel>
                    );
                })}
            </CardPanel.Group>
        );
    }
}
