/**
 * Widget that displays a list of registries with favorite entries in them and the ability to view their list
 *
 * @author Brauer Ilya
 * @date 2020-03-02
 */

import * as React from 'react';
import {CardPanel} from '@unidata/uikit';
import {MetaRecordStore} from '../../../search/store/MetaRecordStore/MetaRecordStore';
import {FavoriteStore} from '../../../../store/FavoriteStore';
import {FavoriteList} from './FavoriteList';
import {i18n} from '@unidata/core-app';

import * as styles from './favoriteWidget.m.scss';

interface IProps {
    favoriteStore: FavoriteStore;
    metaRecordStore: MetaRecordStore;
}

export class FavoriteWidget extends React.Component<IProps, {}> {
    render () {
        return (
            <CardPanel
                title={i18n.t('module.data>search>widgets>favorite>cardTitle')}
                isCollapsed={false}
                noBodyPadding={true}
            >
                <div className={styles.container}>
                   <FavoriteList metaRecordStore={this.props.metaRecordStore} favoriteStore={this.props.favoriteStore}/>
                </div>
            </CardPanel>
        );
    }
}
