/**
 * Table with favorite records
 *
 * @author Brauer Ilya
 * @date 2020-03-02
 */

import * as React from 'react';
import {MetaRecordStore} from '../../../search/store/MetaRecordStore/MetaRecordStore';
import {SearchResultStore} from '../../../search/store/SearchResultStore';
import {FavoriteEntity, FavoriteStore} from '../../../../store/FavoriteStore';
import {setColumn, setFavoriteCheckbox} from '../../../search/search_table/components/table_container/columnSettings';
import {Button, Table, Column} from '@unidata/uikit';
import {makeFavoriteSearchTerms} from '../../../search/store/search_panel_store/favoriteTerms';
import {inject} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {SearchQuery, IDisplayAttr} from '@unidata/meta';

interface IProps {
    favoriteStore: FavoriteStore;
    favoriteEntity: FavoriteEntity;
}

interface IState {
    records: any[];
}

@inject('routing')
export class FavoriteTable extends React.Component<IProps, IState> {
    metaRecordStore = new MetaRecordStore(this.props.favoriteStore);
    searchResultStore = new SearchResultStore(this.metaRecordStore);

    state: IState = {
        records: []
    };

    componentDidMount (): void {
        this.metaRecordStore.setCurrentEntity(this.props.favoriteEntity);

        this.metaRecordStore.getMetaRecord()
            .then(() => {

                const favoriteEtalonsMap = this.props.favoriteStore.getFavoriteForEntity(this.props.favoriteEntity.entityName);
                const {etalonTerm, rangeTerm} = makeFavoriteSearchTerms(favoriteEtalonsMap);

                const searchBuilder = new SearchQuery({
                    entity: this.metaRecordStore.currentEntity.entityName,
                    attributes: this.metaRecordStore.getMainVisibleAttributes()
                });

                searchBuilder.add(etalonTerm.getQuery());
                searchBuilder.add(rangeTerm.getQuery());

                const query = searchBuilder.getSearchQuery();

                this.searchResultStore.setSearchQuery(query);

                return this.searchResultStore.getRecords();
            })
            .then(() => {
                this.setState({records: this.searchResultStore.records.slice()});
            });
    }

    private renderColumns = () => {
        const searchResultStore = this.searchResultStore;
        const metaRecordStore = this.metaRecordStore;
        const favoriteStore = this.props.favoriteStore;

        const columns = metaRecordStore.getMainVisibleAttributes()
            .sort(metaRecordStore.columnsSort)
            .reduce((result: Column[], item) => {
                if (item.isShow) {
                    if (item.children && item.children.length > 0) {
                        result.push({
                            Header: item.displayName,
                            id: item.name,
                            accessor: item.name,
                            columns: item.children
                                .sort(metaRecordStore.columnsSort)
                                .reduce((result: Column[], childItem: IDisplayAttr) => {
                                    if (childItem.isShow) {
                                        result.push(setColumn(metaRecordStore.currentEntity.entityName, childItem, searchResultStore));
                                    }

                                    return result;
                                }, [])
                        });
                    } else {
                        result.push(setColumn(metaRecordStore.currentEntity.entityName, item, searchResultStore));
                    }
                }

                return result;
            }, [
                setFavoriteCheckbox(searchResultStore, metaRecordStore, favoriteStore)
            ]);

        return columns;
    };

    showMore = () => {
        (this.props as any).routing.push(`/search/${this.metaRecordStore.currentEntity.entityName}`);
    };

    render () {
        return (
            <div>
                <Table columns={this.renderColumns()} data={this.state.records} viewRowsCount={0}/>
                {this.searchResultStore.paginationStore.totalRecords > this.searchResultStore.paginationStore.pageSize && (
                    <Button
                        isMinimal={true}
                        isFilled={true}
                        onClick={this.showMore}
                    >
                        {i18n.t('module.data>search.panel>favorite>showMore')}
                    </Button>
                )}
            </div>
        );
    }
}
