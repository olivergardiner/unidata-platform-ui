/**
 * Home page for data - widget container
 *
 * @author Brauer Ilya
 * @date 2020-03-02
 */

import * as React from 'react';
import {SavedSearch} from './saved_search/SavedSearch';
import {FavoriteWidget} from './favorite/FavoriteWidget';
import {LastActions} from './last_actions/LastActions';
import {Menu} from './menu/Menu';
import * as styles from './widgetPanel.m.scss';
import {AppModule, ModuleName, Res, ResourceManager, Right} from '@unidata/core-app';
import {MetaImportExport} from './meta_import_export/MetaImportExport';

export class WidgetPanel extends React.Component<any, any> {
    render () {
        const metaRecordStore = this.props.metaRecordStore;
        const DraftWidgetComponent = AppModule.getModule(ModuleName.DRAFT)?.DraftWidget;

        return (
            <div className={styles.container}>
                <Menu/>
                <div className={styles.columnContainer}>
                    <SavedSearch/>
                    <FavoriteWidget
                        favoriteStore={metaRecordStore.getFavoriteStore()}
                        metaRecordStore={metaRecordStore}
                    />
                </div>
                {ResourceManager.userHasRight(Res.ADMIN_DATA_MANAGEMENT, Right.CREATE) && <MetaImportExport/>}
                <LastActions/>
                {DraftWidgetComponent !== undefined && <DraftWidgetComponent/>}
            </div>
        );
    }
}
