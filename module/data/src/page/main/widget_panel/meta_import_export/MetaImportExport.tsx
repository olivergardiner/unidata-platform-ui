/**
 * Temporary import/export widget on main page
 *
 * @author Brauer Ilya
 * @date 2020-07-28
 */

import * as React from 'react';
import {Button, CardPanel, INTENT, PLACEMENT, SIZE} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import {MetaExportModal, MetaImportBaseModal} from '@unidata/meta';

type ModalType = 'import' | 'export';

interface IState {
    isOpenModal: boolean;
    type: ModalType;
}

export class MetaImportExport extends React.Component<{}, IState> {

    state: IState = {
        isOpenModal: false,
        type: 'import'
    };

    toggleModal = (type: ModalType) => () => {
        this.setState((prevState) => {
            return {
                isOpenModal: !prevState.isOpenModal,
                type: type
            };
        });
    };


    renderButton = (type: 'import' | 'export') => {
        return (
            <Button
                leftIcon={type}
                name={`${type}_meta`}
                title={i18n.t(`module.meta>model_ie>${type}Button`)}
                isRound={true}
                intent={INTENT.PRIMARY}
                isGhost={type === 'export'}
                size={SIZE.LARGE}
                onClick={this.toggleModal(type)}
                tooltipPlacement={PLACEMENT.BOTTOM_END}
            >
                {i18n.t(`module.meta>model_ie>${type}Button`)}
            </Button>
        );
    };

    render () {
        return (
            <CardPanel
                title={i18n.t('module.meta>model_ie>header')}
                isCollapsed={false}
                noBodyPadding={true}
            >
                <div style={{display: 'flex', justifyContent: 'space-around', margin: '20px'}}>
                    {this.renderButton('import')}
                    {this.renderButton('export')}
                </div>

                <MetaImportBaseModal
                    isOpen={this.state.isOpenModal && this.state.type === 'import'}
                    onCloseModal={this.toggleModal('import')}
                />
                <MetaExportModal
                    isOpen={this.state.isOpenModal && this.state.type === 'export'}
                    onCloseModal={this.toggleModal('export')}
                />
            </CardPanel>
        );
    }
}
