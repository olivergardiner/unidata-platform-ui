/**
 * The widget containing the last action
 *
 * @author Brauer Ilya
 * @date 2020-03-09
 */

import * as React from 'react';
import {CardPanel, Icon} from '@unidata/uikit';
import {ueModuleManager, UeModuleReactComponent, UeModuleTypeComponent} from '@unidata/core';
import * as styles from './lastActions.m.scss';
import {i18n} from '@unidata/core-app';

interface IState {
    isEditable: boolean;
}

export class LastActions extends React.Component<{}, IState> {
    state = {
        isEditable: false
    };

    toggleEditable = (e: React.SyntheticEvent<HTMLOrSVGElement>) => {
        e.stopPropagation();
        this.setState((prevState) => {
            return {isEditable: !prevState.isEditable};
        });
    };

    render () {
        const dataPageWidgets = ueModuleManager
            .getModulesByType(UeModuleTypeComponent.DATA_PAGE_WIDGET)
            .reduce<React.ReactNode[]>((result, item: UeModuleReactComponent<UeModuleTypeComponent.DATA_PAGE_WIDGET>) => {
                if ((item.default.resolver === undefined || item.default.resolver() === true) && item.default.component) {
                    const Component: any = item.default.component;

                    result.push(<Component key={item.default.moduleId} dataGrid={item.default.meta.dataGrid}/>);
                }

                return result;
            }, []);

        return (
            <CardPanel
                title={i18n.t('module.data>search>widgets>actions>cardTitle')}
                noBodyPadding={true}
                extraButtons={(
                    <Icon
                        name={this.state.isEditable === true ? 'filledSetting' : 'setting'}
                        onClick={this.toggleEditable}
                        title={i18n.t('module.data>search>widgets>actions>settingsTooltip')}
                    />
                )}
                isCollapsed={false}
            >
                <div className={styles.container}>
                    <CardPanel.Grid
                        isDraggable={this.state.isEditable}
                        isResizable={this.state.isEditable}
                    >
                        {dataPageWidgets}
                    </CardPanel.Grid>
                </div>
            </CardPanel>
        );
    }
}
