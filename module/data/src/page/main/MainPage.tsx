/**
 * Main page for data steward application
 *
 * @author: Ilya Brauer
 * @date: 2020-08-07
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {Layout} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import {MetaRecordStore} from '../search/store/MetaRecordStore/MetaRecordStore';
import {SearchResultStore} from '../search/store/SearchResultStore';
import {Header} from './header/Header';
import * as styles from './mainPage.m.scss';

import {SearchPanel} from '../search/search_panel/SearchPanel';
import {SearchPanelStore} from '../search/store/search_panel_store/SearchPanelStore';
import {FavoriteStore} from '../../store/FavoriteStore';
import {RouteComponentProps} from 'react-router';
import {WidgetPanel} from './widget_panel/WidgetPanel';
import {ILastSearchItem, SavedSearchStore} from '../search/store/SavedSearchStore';

interface IRouteProps {
    entityName?: string;
}

type IProps = RouteComponentProps<IRouteProps, {}, ILastSearchItem>

@observer
export class MainPage extends React.Component<IProps> {
    private metaRecordStore: MetaRecordStore;
    private searchResultStore: SearchResultStore;
    private searchPanelStore: SearchPanelStore;
    private favoriteStore: FavoriteStore;
    private savedSearchStore: SavedSearchStore = new SavedSearchStore();

    searchPanelRef: SearchPanel | null = null;

    constructor (props: IProps) {
        super(props);

        this.searchPanelStore = new SearchPanelStore();
        this.metaRecordStore = this.searchPanelStore.metaRecordStore;
        this.searchResultStore = this.searchPanelStore.searchResultStore;
        this.favoriteStore = this.searchPanelStore.favoriteStore;

        this.metaRecordStore.getEntityList();
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        return i18n.t('wiki:page>datasearch');
    }

    setRefSearchPanel = (el: SearchPanel) => {
        this.searchPanelRef = el;
    };

    render () {
        return (
            <Provider
                searchResultStore={this.searchResultStore}
                metaRecordStore={this.metaRecordStore}
                savedSearchStore={this.savedSearchStore}
                searchPanelStore={this.searchPanelStore}
            >
                <div data-qaid='mainPage'>
                    <Header
                        metaRecordStore={this.metaRecordStore}
                    />

                    <SearchPanel
                        searchResultStore={this.searchResultStore}
                        metaRecordStore={this.metaRecordStore}
                        searchPanelStore={this.searchPanelStore}
                        favoriteStore={this.favoriteStore}
                        ref={this.setRefSearchPanel}
                    />
                    <Layout className={styles.layoutWidget}>
                        <WidgetPanel metaRecordStore={this.metaRecordStore}/>
                    </Layout>
                </div>
            </Provider>
        );
    }
}
