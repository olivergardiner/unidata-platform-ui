import {UeModuleCallback, UeModuleTypeCallBack, UeModuleTypeComponent} from '@unidata/core';
import {SearchPage} from './search/SearchPage';
import {DataViewPage} from './dataview/DataViewPage';
import {IdSearch} from './hotkey/IdSearch';
import {SystemTools} from './hotkey/SystemTools';
import {AppTypeManager} from '@unidata/core-app';
import {MainPage} from './main/MainPage';
import {DataViewLightPage} from './dataview_light/DataViewLightPage';

// todo Brauer Ilya - probably put in core-app, in the General enum of routes
export enum UrlRoutes {
    MainPage = '/', // Main page with widgets
    SearchPage = '/search', // Main route of the search page
    SearchTable = '/search/:entityName?', // A table with the results
    DataView = '/dataview', // Page for editing/viewing an entry
    DataViewLight = '/dataviewlight/:entityName/:etalonId' // Page for viewing data card
}

const hotkeyDefaults = {
    type: UeModuleTypeComponent.GLOBAL_HOTKEY,
    active: true,
    system: false,
    resolver: () => true,
    meta: {
    }
};

export const hotkeys = [
    {
        'default': {
            moduleId: 'idSearch',
            component: IdSearch,
            ...hotkeyDefaults
        }
    },
    {
        'default': {
            moduleId: 'systemTools',
            component: SystemTools,
            ...hotkeyDefaults
        }
    }

];

export const menuItems: Array<UeModuleCallback<UeModuleTypeCallBack.MENU_ITEM>> = [
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'mainMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'main',
                route: UrlRoutes.MainPage,
                icon: 'home',
                menuColor: 'green',
                groupName: 'dataManagement',
                order: 'first'
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeCallBack.MENU_ITEM,
            moduleId: 'searchMenuItem',
            active: true,
            system: false,
            fn: () => {
            },
            resolver: () => true,
            meta: {
                name: 'search',
                route: UrlRoutes.SearchPage,
                icon: 'searchMainMenu',
                menuColor: 'purple',
                groupName: 'dataManagement',
                order: 'last',
                isActiveFor: [UrlRoutes.SearchTable]
            }
        }
    }
];

export const pages = [
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'mainPage',
            active: true,
            system: false,
            component: MainPage,
            resolver: () => {
                return AppTypeManager.isDataSteward();
            },
            meta: {
                route: UrlRoutes.MainPage
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'searchPage',
            active: true,
            system: false,
            component: SearchPage,
            resolver: () => {
                return AppTypeManager.isDataSteward();
            },
            meta: {
                route: UrlRoutes.SearchTable,
                exact: false
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
            moduleId: 'dataview',
            active: true,
            system: false,
            component: DataViewPage,
            resolver: () => {
                return AppTypeManager.isDataSteward();
            },
            meta: {
                route: UrlRoutes.DataView
            }
        }
    },
    {
        'default': {
            type: UeModuleTypeComponent.PAGE,
                moduleId: 'dataviewlight',
            active: true,
            system: false,
            component: DataViewLightPage,
            resolver: () => {
                return AppTypeManager.isDataSteward();
            },
            meta: {
                route: UrlRoutes.DataViewLight
            }
        }
    }
];
