/**
 * Store for the latest open data
 *
 * @author Brauer Ilya
 * @date 2020-04-15
 */
import {UdLogger} from '@unidata/core-app';
import {CurrentEntity} from '../../search/store/MetaRecordStore/MetaRecordStoreTypes';

interface ILastDataItem {
    currentEntity: CurrentEntity;
    timestamp: number;
    etalonId: string;
    mainDisplayable: string[];
    search: string;
}

const STORAGE_KEY = 'lastData';

export class LastDataStore {
    public static get (): ILastDataItem[] {
        const lastSearch = localStorage.getItem(STORAGE_KEY);

        if (lastSearch === null) {
            return [];
        }

        try {
            return JSON.parse(lastSearch);
        } catch (err) {
            UdLogger.error(err);

            return [];
        }
    }

    public static set (lastData: Omit<ILastDataItem, 'timestamp'>) {
        const lastSearch = LastDataStore.get();
        const index = lastSearch.findIndex((item) => item.etalonId === lastData.etalonId);

        if (index === -1) {
            lastSearch.push({
                ...lastData,
                timestamp: new Date().getTime()
            });
        } else {
            lastSearch[index].timestamp = new Date().getTime();
        }

        if (lastSearch.length > 10) {
            lastSearch.shift();
        }

        localStorage.setItem(STORAGE_KEY, JSON.stringify(lastSearch));
    }
}
