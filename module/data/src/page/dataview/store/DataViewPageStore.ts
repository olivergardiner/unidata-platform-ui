/**
 * Store screen in the post editor
 *
 * @author Ivan Marshalkin
 * @date 2020-02-12
 */
import {DataDraftStore} from './DataDraftStore';
import {action, observable} from 'mobx';

export class DataViewPageStore {
    @observable
    public title: string = '';

    @observable
    public entityDisplayName: string = '';

    public dataDraftStore = new DataDraftStore();

    @observable
    public dataRecordDirty: boolean = false;


    @action
    public setDataRecordDirty (dirty: boolean) {
        this.dataRecordDirty = dirty;
        this.dataDraftStore.dataRecordDirty = dirty;
    }

    @action
    public setTitle (title: string) {
        this.title = title;
    }

    @action
    public setEntityDisplayName (entityDisplayName: string) {
        this.entityDisplayName = entityDisplayName;
    }
}
