import {action, computed, observable} from 'mobx';
import {ueModuleManager, UeModuleTypeCallBack} from '@unidata/core';

/**
 *  Store for draft entries
 *
 * @author Sergey Shishigin
 * @date 2020-04-17
 */

export class DataDraftStore {
    @observable
    public draftId: string = '';

    @observable
    public etalonId: string = '';

    @observable
    public displayName: string | null = null;

    @observable
    public isModalOpen: boolean = false;

    @observable
    public confirmIsOpen: boolean = false;

    @observable
    public confirmDraftId: string | null = null;

    @observable
    public dataRecordDirty: boolean = false;

    public onDraftIdChange: (draftId: string) => void;
    public onEtalonIdChange: (draftId: string) => void;

    private loadDisplayNameTimer: number;

    @computed
    public get isDraft () {
        return Boolean(this.draftId);
    }

    constructor () {
        this.onSwitcherChange = this.onSwitcherChange.bind(this);
        this.onDraftModalClose = this.onDraftModalClose.bind(this);
        this.onDraftIdSelect = this.onDraftIdSelect.bind(this);
        this.updateDraftInfo = this.updateDraftInfo.bind(this);
        this.onConfirmClose = this.onConfirmClose.bind(this);
        this.onConfirm = this.onConfirm.bind(this);
        this.onDraftOpen = this.onDraftOpen.bind(this);
    }

    @action
    public setModalOpen (open: boolean) {
        this.isModalOpen = open;
    }

    @action
    public onSwitcherChange () {
        if (this.draftId) {
            if (this.dataRecordDirty) {
                // request confirmation if the draft is changed
                this.setConfirm(true, null);
            } else {
                this.setDraftId('');
            }
        } else if (!this.isModalOpen) {
            this.setModalOpen(true);
        }
    }

    public onDraftModalClose () {
        this.setModalOpen(false);
    }

    /**
     * Managing confirmation window parameters
     */
    @action
    public setConfirm (open: boolean, draftId: string | null) {
        this.confirmIsOpen = open;
        this.confirmDraftId = draftId;
    }

    /**
     * When trying to open a draft
     */
    public onDraftOpen (draftId: string) {
        if (this.dataRecordDirty) {
            // if the entry is changed , we request confirmation
            this.setConfirm(true, draftId.toString());
        } else {
            this.onDraftIdSelect(draftId);
        }
    }

    /**
     * Cancel confirmation
     */
    @action
    public onConfirmClose () {
        this.setConfirm(false, null);
    }

    /**
     * When confirming the exit action from the draft
     */
    @action
    public onConfirm () {
        this.confirmIsOpen = false;

        if (this.confirmDraftId) { // if you selected a new draft
            this.onDraftIdSelect(this.confirmDraftId);
            this.confirmDraftId = null;
        } else { // if we just exit the draft
            this.setDraftId('');
        }
    }

    @action
    public onDraftPublish (etalonId: string) {
        this.setDraftId('');
        this.setEtalonId(etalonId);
        this.loadDisplayNameDelayed();
    }

    private isEmptyDraftId () {
        return this.draftId === '' || this.draftId === 'null';
    }

    private isEmptyEtalonId () {
        return this.etalonId === '' || this.etalonId === 'null';
    }

    public loadDisplayNameDelayed () {
        this.displayName = null;

        clearTimeout(this.loadDisplayNameTimer);
        this.loadDisplayNameTimer = window.setTimeout(this.loadDisplayName.bind(this), 500);
    }

    public loadDisplayName () {
        if (this.isEmptyDraftId()) {
            return;
        }

        let modules = ueModuleManager.getModulesByType(UeModuleTypeCallBack.GET_DRAFT_INFO);
        let module = modules[0];

        if (module) {
            module.default.fn('data', this.etalonId, this.draftId).then(this.updateDraftInfo);
        }
    }

    @action
    public updateDraftInfo (draftInfo: null | {displayName: string}) {
        this.displayName = draftInfo?.displayName ?? null;
    }

    @action
    public setDraftId (draftId: string) {
        this.draftId = draftId.toString();
        this.loadDisplayNameDelayed();

        if (this.onDraftIdChange) {
            this.onDraftIdChange(this.draftId);
        }
    }

    @action
    public setEtalonId (etalonId: string) {
        this.etalonId = etalonId;
        this.loadDisplayNameDelayed();

        if (this.onEtalonIdChange) {
            this.onEtalonIdChange(etalonId);
        }
    }

    public onDraftIdSelect (draftId: string) {
        this.setDraftId(draftId);
        this.setModalOpen(false);
    }
}
