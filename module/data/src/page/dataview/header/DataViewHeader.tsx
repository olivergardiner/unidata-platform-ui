/**
 * Header for the record card
 *
 * @author Sergey Shishigin
 * @date 2020-04-17
 */
import * as React from 'react';
import {Button, INTENT, joinClassNames, PageHeader, Switcher} from '@unidata/uikit';
import {i18n} from '@unidata/core-app';
import * as styles from './DataViewHeader.m.scss';

interface IProps {
    onDraftMenuButtonClick: () => void;
    sectionTitle: string;
    itemTitle: string;
    isDraft: boolean;
    draftSwitcherEnabled: boolean;
    onDraftSwitcherChange: (isDraft: boolean, event: MouseEvent) => void;
}

export class DataViewHeader extends React.Component<IProps> {
    get sectionTitle () {
        return this.props.sectionTitle;
    }

    get itemTitle () {
        // the header text is encoded at the ExtJS level, but when the entry is edited
        // but when the record becomes dirty, span comes with an indication that the record has been changed
        // TODO: insert plain text when the record card is on Reaction
        return <span dangerouslySetInnerHTML={{__html: this.props.itemTitle}}></span>;
    }

    get isDraft () {
        return this.props.isDraft;
    }

    get onDraftSwitcherChange () {
        return this.props.onDraftSwitcherChange;
    }

    get draftSwitcherEnabled () {
        return this.props.draftSwitcherEnabled;
    }

    get switcherTextClassName () {
        return joinClassNames(
            styles.draftSwitcherText,
            [styles.draftSwitcherTextOn, this.isDraft]
        );
    }

    renderDraftSwitcherText () {
        return (
            <div className={this.switcherTextClassName}>
                {i18n.t('module.data>dataview>draft')}
            </div>
        );
    }

    renderExtraButtons () {
        if (!this.draftSwitcherEnabled) {
            return <React.Fragment />;
        }

        return (
            <React.Fragment>
                <Switcher
                    className={styles.draftSwitcher}
                    leftText={this.renderDraftSwitcherText()}
                    size={'small'}
                    checked={this.isDraft}
                    onChange={this.onDraftSwitcherChange}
                />
                <div className={styles.splitter} />
                <Button
                    onClick={this.props.onDraftMenuButtonClick}
                    isGhost={true}
                    isMinimal={true}
                    leftIcon={'menu'}
                    data-qaid={'openDraftList'}
                />
            </React.Fragment>
        );
    }

    render () {
        return <PageHeader
            groupSectionTitle={i18n.t('page.header>dataManagement')}
            sectionTitle={this.sectionTitle}
            itemTitle={this.itemTitle}
            iconType={'star'}
            extraButtons={this.renderExtraButtons()}
        />;
    }
}
