/**
 * Screen with the record editor
 *
 * @author Ivan Marshalkin
 * @date 2020-02-12
 */

import * as React from 'react';
import {observer, Provider} from 'mobx-react';
import {Layout} from 'antd';
import {DataViewPageStore} from './store/DataViewPageStore';
import {Confirm, ExtWidget} from '@unidata/uikit';
import {ReindexDataRecord} from '../hotkey/ReindexDataRecord';
import {DataViewHeader} from './header/DataViewHeader';
import {RouteComponentProps} from 'react-router';
import qs from 'qs';
import {App, i18n, ModuleName} from '@unidata/core-app';
import {LastDataStore} from './store/LastDataStore';
import {SearchPanelStore} from '../search/store/search_panel_store/SearchPanelStore';
import {makeFavoriteSearchTerms} from '../search/store/search_panel_store/favoriteTerms';
import {SearchQuery, IDisplayAttr} from '@unidata/meta';

type IProps = RouteComponentProps<any>;

interface IInjectProps extends IProps {
}

@observer
export class DataViewPage extends React.Component<IProps> {
    store: DataViewPageStore = new DataViewPageStore();
    // todo Brauer Ilya - think about where to put the storages shared by several pages
    searchPanelStore = new SearchPanelStore();
    providerProps: any;
    extWidgetRef: any;
    constructor (props: IProps) {
        super(props);
        let queryParams: {draftId: string; etalonId: string};

        // the same props must be passed to the provider, otherwise mobs throws an exception
        // @see https://stackoverflow.com/questions/43550137/mobx-rerender-after-assign/43562824
        this.providerProps = {
            pageStore: this.store
        };

        this.extWidgetRef = React.createRef();
        queryParams = qs.parse(props.location.search, {
            delimiter: /[?&|]/
        });

        queryParams.draftId = queryParams.draftId || '';
        queryParams.etalonId = queryParams.etalonId !== 'null' ? queryParams.etalonId : '';

        let draftStore = this.draftStore;

        draftStore.setDraftId(queryParams.draftId);
        draftStore.setEtalonId(queryParams.etalonId);

        if (!queryParams.etalonId) {
            this.store.setTitle(i18n.t('module.data>dataview>newRecord'));
        }

        this.onDraftIdChange = this.onDraftIdChange.bind(this);
        this.onDraftMenuButtonClick = this.onDraftMenuButtonClick.bind(this);

        draftStore.onDraftIdChange = this.onDraftIdChange.bind(this);

        this.setToLastData();
    }

    // todo Brauer Ilya - all this method should be reviewed when the card is implemented on the react
    /**
     * Record the fact of viewing information about a record in the store of the last viewed records
     */
    setToLastData () {
        const search = this.props.location.search;
        const searchItems = search.split('|');

        const {entityName, etalonId} = searchItems.reduce((result, item) => {
            const query = qs.parse(item);
            const entity = query['dataSearch?entityName'];
            const etalon = query['etalon?etalonId'];

            if (entity !== undefined) {
                result.entityName = entity as string;

                return result;
            }

            if (etalon !== undefined) {
                result.etalonId = etalon as string;

                return result;
            }

            return result;
        }, {
            entityName: '',
            etalonId: ''
        });

        const metaRecordStore = this.searchPanelStore.metaRecordStore;
        const searchResultStore = this.searchPanelStore.searchResultStore;

        let mainVisibleAttributes: IDisplayAttr[] = [];

        metaRecordStore.getEntityList()
            .then(() => {
                // setting the entity whose card we opened
                return metaRecordStore.setCurrentEntityByName(entityName);
            })
            .then(() => {
                // getting the estimate for the selected entity
                return metaRecordStore.getMetaRecord().then(() => {
                    mainVisibleAttributes = metaRecordStore.getMainVisibleAttributes();
                });
            })
            .then(() => {
                // getting an entry by etalonId
                const {etalonTerm} = makeFavoriteSearchTerms([etalonId]);

                const searchBuilder = new SearchQuery({
                    entity: metaRecordStore.currentEntity.entityName,
                    attributes: mainVisibleAttributes
                });

                searchBuilder.add(etalonTerm.getQuery());

                const query = searchBuilder.getSearchQuery();

                searchResultStore.setSearchQuery(query);

                return searchResultStore.getRecords();
            })
            .then(() => {
                // we record information about viewing the record in the store
                LastDataStore.set({
                    currentEntity: metaRecordStore.currentEntity,
                    etalonId,
                    mainDisplayable: mainVisibleAttributes.reduce<string[]>((result, attribute) => {
                        const cellData = searchResultStore.records[0].preview.find2((item) => {
                            return item.field.getValue() === attribute.name;
                        });

                        if (cellData !== null) {
                            result = result.concat(cellData.values.getValue());
                        }

                        return result;
                    }, []),
                    search
                });
            });
    }

    /**
     * The function returns the url of the help section.
     * Used for integrating features in ExtJS Unidata
     */
    wikiPage () {
        const widget = this.extWidgetRef.current.getWidget();

        if (widget) {
            return widget.wikiPage;
        }

        return null;
    }

    get injected () {
        return this.props as IInjectProps;
    }

    onBeforeUnload (): any {
        const widget = this.extWidgetRef.current.getWidget();

        if (widget) {
            let tab = widget.dataViewPanel.getActiveTab();

            if (tab && tab.getDirty()) {
                return i18n.t('app>leaveConfirmText');
            }
        }
    }

    get draftStore () {
        return this.store.dataDraftStore;
    }

    get etalonId () {
        return this.draftStore.etalonId;
    }

    get isDraft (): boolean {
        return this.draftStore.isDraft;
    }

    get itemTitle (): string {
        let draftDisplayName = this.draftStore.displayName;

        if (this.isDraft && draftDisplayName) {
            return `${this.store.title} (${draftDisplayName})`;
        }

        return this.store.title;
    }

    get onDraftSwitcherChange () {
        return this.draftStore.onSwitcherChange;
    }

    get isDraftModalOpen () {
        return this.draftStore.isModalOpen;
    }

    get draftSwitcherEnabled () {
        return Boolean(this.etalonId);
    }

    onDraftMenuButtonClick () {
        this.draftStore.setModalOpen(true);
    }

    onDraftIdChange (draftId: string) {
        const widget = this.extWidgetRef.current.getWidget();

        widget.setDraftId(draftId);
    }

    get draftModal () {
        const self = this;

        let Component = App.getModule(ModuleName.DRAFT)?.DraftListWnd;

        if (!Component) {
            return null;
        }

        return <Component
            isOpen={this.isDraftModalOpen}
            type={'data'}
            entityId={this.etalonId}
            onDraftOpen={this.draftStore.onDraftOpen}
            onClose={function () {
                self.draftStore.setModalOpen(false);
            }}
        />;
    }

    get confirmDraftSwitch () {
        return (
            <Confirm
                onConfirm={this.draftStore.onConfirm}
                confirmationMessage={i18n.t('module.data>dataview>dirtyConfirm')}
                isOpen={this.draftStore.confirmIsOpen}
                onClose={this.draftStore.onConfirmClose}
            />
        );
    }

    render () {
        let me = this;

        return (
            <Provider {...this.providerProps}>
                <Layout data-qaid='dataViewPage'>
                    <DataViewHeader
                        onDraftMenuButtonClick={this.onDraftMenuButtonClick}
                        sectionTitle={this.store.entityDisplayName}
                        itemTitle={this.itemTitle}
                        isDraft={this.isDraft}
                        draftSwitcherEnabled={this.draftSwitcherEnabled}
                        onDraftSwitcherChange={this.onDraftSwitcherChange}
                    />
                    {this.draftModal}
                    {this.confirmDraftSwitch}
                    <Layout>
                        <Layout.Content>
                            <ExtWidget
                                ref={this.extWidgetRef}
                                autoDestroy={true}
                                createWidget={function () {
                                    let view;

                                    view = window.Ext.widget({
                                        xtype: 'steward.dataview',
                                        promptTabChange: false,
                                        height: '100%',
                                        width: '100%',
                                        listeners: {},
                                        flex: 1,
                                        onDirtyChange: function (dirty: boolean) {
                                            me.store.setDataRecordDirty(dirty);
                                        },
                                        onDraftPublish: function (etalonId: string) {
                                            me.draftStore.onDraftPublish(etalonId);
                                        },
                                        onEtalonIdChange: function (etalonId: string | null) {
                                            me.draftStore.setEtalonId(etalonId ?? '');
                                        },
                                        onDraftIdChange: function (draftId: string | number | null) {
                                            me.draftStore.setDraftId(draftId ? draftId.toString() : '');
                                        },
                                        onTitleChanged: function (title: string) {
                                            me.store.setTitle(title);
                                        },
                                        onEntityDisplayNameChanged: function (title: string) {
                                            me.store.setEntityDisplayName(title);
                                        }
                                    });

                                    return view;
                                }}
                                style={{
                                    width: '100%',
                                    height: '100%'
                                }}
                            >
                            </ExtWidget>

                        </Layout.Content>
                        <ReindexDataRecord />
                    </Layout>
                </Layout>
            </Provider>
        );
    }
}
