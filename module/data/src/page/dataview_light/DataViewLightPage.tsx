/**
 * Screen with lightweight data card editor
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import {RouteComponentProps} from 'react-router';
import {observer, Provider} from 'mobx-react';
import {DataViewHeader} from '../dataview/header/DataViewHeader';
import {Layout} from 'antd';
import {DataViewer} from './dataviewer/DataViewer';
import {DataViewPageStore} from './store/DataViewPageStore';
import {DataViewerProvider} from './provider/DataViewer';

type IProps = RouteComponentProps<{ entityName: string; etalonId: string }>;

@observer
export class DataViewLightPage extends React.Component<IProps> {
    store: DataViewPageStore;

    providerProps: DataViewerProvider;

    constructor (props: IProps) {
        super(props);

        const entityName = this.props.match.params.entityName;

        let etalonId: string | null = this.props.match.params.etalonId;

        if (etalonId === 'new') {
            etalonId = null;
        }

        this.store = new DataViewPageStore(entityName, etalonId);

        this.providerProps = {
            dataAccessorStore: this.store.dataViewStore.dataAccessorStore,
            layoutStore: this.store.dataViewStore.dataCardLayoutStore,
            scrollableItemsStore: this.store.dataViewStore.scrollableItemsStore,
            dataCardActionsStore: this.store.dataViewStore.dataCardActionsStore,
            handlersStore: this.store.dataViewStore.handlersStore
        };
    }

    get entityName () {
        return this.props.match.params.entityName;
    }

    get etalonId () {
        return this.props.match.params.etalonId;
    }

    render () {
        return (
            <Provider {...this.providerProps}>
                <Layout data-qaid='dataViewPage'>
                    <DataViewHeader
                        onDraftMenuButtonClick={() => {
                        }}
                        sectionTitle={this.store.sectionTitle}
                        itemTitle={this.store.itemTitle}
                        isDraft={false}
                        draftSwitcherEnabled={false}
                        onDraftSwitcherChange={() => {
                        }}
                    />
                    <DataViewer/>
                </Layout>
            </Provider>
        );
    }
}
