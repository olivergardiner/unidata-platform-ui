/**
 * Store for working with scrollable items on data card
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import {action, observable, ObservableMap} from 'mobx';
import {ScrollableItem} from '../type/ScrollableItem';
import React from 'react';

export class ScrollableItemsStore {
    @observable
    private scrollableItems: ObservableMap<string, ScrollableItem> = observable.map();

    @action
    public addScrollableItem (id: string, displayName: string, ref: React.RefObject<any>) {
        this.scrollableItems.set(id, {
            id: id,
            displayName: displayName,
            ref: ref
        });
    }

    @action
    public removeScrollableItem (id: string) {
        this.scrollableItems.delete(id);
    }

    public getScrollableItem (id: string) {
        return this.scrollableItems.get(id);
    }

    public getAllScrollableItems () {
        let allItems: ScrollableItem [] = [];

        this.scrollableItems.forEach((value, key) => {
            allItems.push(value);
        });

        return allItems;
    }
}
