/**
 * Main store for dataview page
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {DataViewerStore} from './DataViewerStore';
import {computed} from 'mobx';
import {DataAbstractAttribute, DataArrayAttribute, DataSimpleAttribute} from '../../../model';
import {DefaultArrayAttributeFormatter} from '../../../formatter/DefaultArrayAttributeFormatter';
import {DefaultSimpleAttributeFormatter} from '../../../formatter/DefaultSimpleAttributeFormatter';

export class DataViewPageStore {
    public dataViewStore: DataViewerStore;

    private readonly entityName: string;

    private readonly etalonId: string | null;

    constructor (entityName: string, etalonId: string | null) {
        this.entityName = entityName;
        this.etalonId = etalonId;
        this.dataViewStore = new DataViewerStore(this.entityName, this.etalonId, false);

        this.dataViewStore.loadAll();
    }

    public getTitle () {
        const store = this.dataViewStore.metaRecordStore;

        if (!store) {
            return '';
        }

        return store.metaRecord.displayName.getValue();
    }

    @computed
    public get itemTitle () {
        const metaRecordStore = this.dataViewStore.metaRecordStore;
        const dataRecordStore = this.dataViewStore.dataRecordStore;

        if (!metaRecordStore || !dataRecordStore) {
            return '';
        }

        let displayableMeta = metaRecordStore.getMetaRecord().getMetaAttributesByPropertyValue('displayable', true);
        let displayableData = displayableMeta.map((attr) => dataRecordStore.getDataAttribute(attr.name.getValue()));

        return displayableData
            .map((attr: DataAbstractAttribute) => {
                if (attr instanceof DataArrayAttribute) {
                    return (attr.getFormattedValue(new DefaultArrayAttributeFormatter())).join(', ');
                } else if (attr instanceof DataSimpleAttribute) {
                    return attr.getFormattedValue(new DefaultSimpleAttributeFormatter());
                }

                return '';
            })
            .filter((value) => Boolean(value))
            .join(' | ');

    }

    @computed
    public get sectionTitle () {
        const metaRecordStore = this.dataViewStore.metaRecordStore;

        if (!metaRecordStore) {
            return '';
        }

        return metaRecordStore.getMetaRecord().displayName.getValue();
    }
}
