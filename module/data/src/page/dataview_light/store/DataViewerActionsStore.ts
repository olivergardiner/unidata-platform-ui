/**
 * Store for handling dataRecord actions
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import {IObservableArray, observable, ObservableMap} from 'mobx';
import {DataRecordAction} from '../type/DataRecordAction';
import {DataRecordActionGroup} from '../type/DataRecordActionGroup';
import {DataRecordActionNameEnum} from '../type/DataRecordActionNameEnum';

export class DataViewerActionsStore {
    //ToDo title i18n
    @observable
    public dataCardActions: ObservableMap<DataRecordActionNameEnum, DataRecordAction> = observable.map({
        [DataRecordActionNameEnum.REFRESH]: {
            type: DataRecordActionNameEnum.REFRESH, title: 'Refresh', handler: () => {
            }
        },
        [DataRecordActionNameEnum.PUBLISH_JMS]: {
            type: DataRecordActionNameEnum.PUBLISH_JMS, title: 'Publish JMS', handler: () => {
            }
        },

        [DataRecordActionNameEnum.MERGE]: {
            type: DataRecordActionNameEnum.MERGE, title: 'Merge', handler: () => {
            }
        },
        [DataRecordActionNameEnum.CLONE]: {
            type: DataRecordActionNameEnum.CLONE, title: 'Clone', handler: () => {
            }
        },

        [DataRecordActionNameEnum.HISTORY]: {
            type: DataRecordActionNameEnum.HISTORY, title: 'History', handler: () => {
            }
        },
        [DataRecordActionNameEnum.BACK_REL]: {
            type: DataRecordActionNameEnum.BACK_REL, title: 'Back relations', handler: () => {
            }
        },
        [DataRecordActionNameEnum.ORIGINAL]: {
            type: DataRecordActionNameEnum.ORIGINAL, title: 'Origin', handler: () => {
            }
        }
    });

    @observable
    public dataCardActionGroups: IObservableArray<DataRecordActionGroup> = observable.array([
        {
            name: 'recordActions', icon: 'cog', title: 'Record actions', actions: [
                DataRecordActionNameEnum.REFRESH,
                DataRecordActionNameEnum.PUBLISH_JMS,
                DataRecordActionNameEnum.MERGE,
                DataRecordActionNameEnum.CLONE

            ]
        },
        {
            name: 'etalonActions', icon: 'cog2', title: 'Etalon record', actions: [
                DataRecordActionNameEnum.HISTORY,
                DataRecordActionNameEnum.BACK_REL,
                DataRecordActionNameEnum.ORIGINAL
            ]
        }
    ]);
}
