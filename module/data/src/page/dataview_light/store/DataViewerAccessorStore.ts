/**
 * Store for access to DataViewer global state
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {MetaRecordStore} from './MetaRecordStore';
import {DataRecordStore} from './DataRecordStore';

export class DataViewerAccessorStore {
    private metaRecordStore: MetaRecordStore;
    private dataRecordStore: DataRecordStore;

    constructor (metaRecordStore: MetaRecordStore, dataRecordStore: DataRecordStore) {
        this.metaRecordStore = metaRecordStore;
        this.dataRecordStore = dataRecordStore;
    }

    public getDataAttribute (path: string) {
        return this.dataRecordStore.getDataAttribute(path);
    }

    public getMetaAttribute (path: string) {
        return this.metaRecordStore.getMetaAttribute(path);
    }

    public getAttributeGroupById (id: string) {
        return this.metaRecordStore.getMetaRecord().getAttributeGroupById(id);
    }

    public isDataRecordLoading () {
        return this.dataRecordStore.isLoading;
    }

    public isMetaRecordLoading () {
        return this.metaRecordStore.isLoading;
    }

    public isDataRecordPhantom () {
        return this.dataRecordStore.isPhantom();
    }

    public getCanSaveDataRecord () {
        return this.dataRecordStore.dataRecord.getDirty();
    }

    public getReadOnly () {
        return this.dataRecordStore.isReadOnly;
    }
}
