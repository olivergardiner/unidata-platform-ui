/**
 * Store with global DataViewer handlers
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {DataViewerStore} from './DataViewerStore';
import {RefObject} from 'react';
import {DataAbstractAttribute} from '../../../index';
import {ComplexAttribute} from '@unidata/meta';
import {DataComplexAttribute} from '../../../model/attribute/DataComplexAttribute';
import {NestedRecord} from '../../../model/NestedRecord';

export class DataViewerHandlersStore {
    private dataViewerStore: DataViewerStore;

    constructor (dataViewerStore: DataViewerStore) {
        this.dataViewerStore = dataViewerStore;
    }

    public saveDataRecord () {
        return this.dataViewerStore.saveDataRecord();
    }

    public addScrollableItem (id: string, displayName: string, ref: RefObject<any>) {
        return this.dataViewerStore.scrollableItemsStore.addScrollableItem(id, displayName, ref);
    }

    public addAttributeToDataRecord (attribute: DataAbstractAttribute, dataPath: string) {
        return this.dataViewerStore.dataRecordStore.createAttributeIfNotExists(attribute, dataPath);
    }

    public createComplexAttribute (metaAttribute: ComplexAttribute,
                                   dataAttribute: DataComplexAttribute | null,
                                   dataPath: string) {
        this.dataViewerStore.dataRecordStore.createComplexAttribute(metaAttribute, dataAttribute, dataPath);
    }

    public removeNestedRecord (nestedRecord: NestedRecord,
                               dataAttribute: DataComplexAttribute | null,
                               dataPath: string) {
        this.dataViewerStore.dataRecordStore.removeNestedRecord(nestedRecord, dataAttribute, dataPath);
    }
}
