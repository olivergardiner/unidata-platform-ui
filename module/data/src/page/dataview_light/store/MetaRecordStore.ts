/**
 * Store for working with meteRecord and its attributes
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import {AbstractAttribute, Entity, LookupEntity, MetaRecordService} from '@unidata/meta';
import {action, observable, runInAction} from 'mobx';
import {UPathMetaStore} from './UPathMetaStore';
import {EntityType} from '@unidata/types';

export class MetaRecordStore {
    @observable
    public metaRecord: Entity | LookupEntity;

    public uPathStore: UPathMetaStore;

    @observable
    public isLoading: boolean = false;

    constructor (metaRecord: Entity | LookupEntity) {
        this.metaRecord = metaRecord;
        this.uPathStore = new UPathMetaStore(this.metaRecord);

    }

    public getMetaRecord () {
        return this.metaRecord;
    }

    @action
    public updateMetaRecord (metaRecord: Entity | LookupEntity) {
        this.metaRecord = metaRecord;

        this.uPathStore = new UPathMetaStore(this.metaRecord);
    }

    public getMetaAttribute (attrPath: string): AbstractAttribute | null {
        return this.uPathStore.getAttributeByPath(attrPath) || null;
    }

    public getAllMetaAttributes (): Map<string, AbstractAttribute> {
        return this.uPathStore.getAllMetaAttributes();
    }

    @action
    private setIsLoading (loading: boolean) {
        this.isLoading = loading;
    }

    public loadMetaRecord (entityType: EntityType, entityName: string) {
        this.setIsLoading(true);

        return MetaRecordService.getMetaRecord(entityType, entityName).then((metaRecord: Entity | LookupEntity) => {
            runInAction(() => {
                    this.updateMetaRecord(metaRecord);

                    this.setIsLoading(false);
                }
            );

            return metaRecord;
        });
    }

}
