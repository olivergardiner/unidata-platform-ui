/**
 * Store for working with dataRecord and its attributes
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import {
    DataAbstractAttribute,
    DataArrayAttribute,
    DataRecord,
    DataSimpleAttribute,
    DataCodeAttribute,
    DataComplexAttribute
} from '../../../model';
import {OperationError, ReactiveProp} from '@unidata/core';
import {action, observable, runInAction} from 'mobx';
import {AtomicRecordUtil} from '../../../utils/AtomicRecordUtil';
import {RelationReferenceDiff} from '../../../model/relation/RelationReferenceDiff';
import {Dialog, i18n} from '@unidata/core-app';
import {DataRecordService} from '../../../service/DataRecordService';
import {UPathDataStore} from './UPathDataStore';
import {ComplexAttribute} from '@unidata/meta';
import {NestedRecord} from '../../../model/NestedRecord';
import {UPath} from '../../../utils/UPath';

export class DataRecordStore {
    @observable
    public dataRecord: DataRecord;

    public uPathStore: UPathDataStore;

    @observable
    public isLoading: boolean = true;

    @observable
    public isReadOnly: boolean = false;

    constructor (dataRecord: DataRecord, isReadOnly?: boolean) {
        this.dataRecord = dataRecord;

        if (isReadOnly !== undefined) {
            this.isReadOnly = isReadOnly;
        }

        this.createAttributeIfNotExists = this.createAttributeIfNotExists.bind(this);

        this.uPathStore = new UPathDataStore(this.dataRecord, true);
    }

    @action
    public updateDataRecord (dataRecord: DataRecord) {
        this.dataRecord = dataRecord;

        this.uPathStore.setDataRecord(this.dataRecord);
    }

    public isPhantom () {
        return this.dataRecord.getPhantom();
    }

    public saveDataRecord () {
        const atomicRecord = AtomicRecordUtil.buildAtomicRecord({
            dataRecord: this.dataRecord,
            relationReferenceDiff: new RelationReferenceDiff({}), // ToDo implement relations
            relationManyToManyDiff: new RelationReferenceDiff({})
        });

        const isCreate = this.dataRecord.getPhantom();

        return DataRecordService
            .saveDataRecord(this.dataRecord.etalonId.getValue(), atomicRecord, isCreate)
            .then((dateRecord: DataRecord) => {
                Dialog.showMessage(i18n.t('dataRecord>saveSuccess'));

                return dateRecord;
            })
            .catch((er: Error) => {
                let message = '';

                if (er instanceof OperationError) {
                    let error = er.toJSON();

                    message = error.axiosResponse.data.errors
                        .map((error: any) => error.userMessageDetails || error.userMessage).join(' ');
                } else {
                    message = er.message;
                }

                Dialog.showError(message, i18n.t('dataRecord>saveError'));

                throw er;
            });
    }

    public getDataRecord () {
        return this.dataRecord;
    }

    public getDataAttribute (path: string): DataAbstractAttribute | null {
        return this.uPathStore.getAttributeByPath(path);
    }

    // ToDo Fix for nested attributes
    @action
    public createAttributeIfNotExists (attribute: DataAbstractAttribute, dataPath: string) {

        if (this.getDataAttribute(dataPath)) {
            return;
        }

        if (UPath.isComplexPath(dataPath)) {
            return this.addNestedAttribute(attribute, dataPath);
        }

        DataRecordStore.addAttributeToRecord(this.dataRecord, attribute);

    }

    @action
    public setIsLoading (loading: boolean) {
        this.isLoading = loading;
    }

    public loadDataRecord (etalonId: string) {
        this.setIsLoading(true);

        return DataRecordService.getDataRecord(etalonId).then((dataRecord: DataRecord) => {
            runInAction(() => {
                dataRecord.setReactive([ReactiveProp.DIRTY, ReactiveProp.DEEP_DIRTY]);

                this.updateDataRecord(dataRecord);

                this.setIsLoading(false);
            });
        });
    }

    @action
    public createDataRecord (entityName: string) {
        let dataRecord = new DataRecord({entityName: entityName, etalonId: null});

        dataRecord.setReactive([ReactiveProp.DIRTY, ReactiveProp.DEEP_DIRTY]);

        this.updateDataRecord(dataRecord);
        this.setIsLoading(false);
    }

    public createComplexAttribute (metaAttribute: ComplexAttribute,
                                   dataAttribute: DataComplexAttribute | null,
                                   dataPath: string) {
        let nestedRecord = new NestedRecord({});

        if (dataAttribute) {
            dataAttribute.nestedRecords.add(nestedRecord);
        } else {
            dataAttribute = new DataComplexAttribute({name: dataPath, nestedRecords: [nestedRecord.serialize()]});

            this.createAttributeIfNotExists(dataAttribute, dataPath);
        }

    }

    @action
    public removeNestedRecord (nestedRecord: NestedRecord,
                               dataAttribute: DataComplexAttribute | null,
                               dataPath: string) {

        if (dataAttribute) {
            dataAttribute.nestedRecords.remove(nestedRecord);
        }

        this.uPathStore.buildAttributesCache();
    }

    // ToDo temporary. Rewrite to upath tokens
    private addNestedAttribute (attribute: DataAbstractAttribute, dataPath: string) {
        const dataPathTokens = dataPath.split(/[[\]]+/); // [attr, 0, inner, 1 ...]
        const parentAttrPath = dataPath.substr(0, dataPath.lastIndexOf('['));
        const nestedRecordIndexString = dataPathTokens.reverse().find((el) => !Number.isNaN(Number.parseInt(el)));
        const parentAttribute: DataAbstractAttribute | null = this.getDataAttribute(parentAttrPath);

        if (parentAttribute && parentAttribute instanceof DataComplexAttribute) {
            let nestedRecord = parentAttribute.nestedRecords.get(Number(nestedRecordIndexString));

            DataRecordStore.addAttributeToRecord(nestedRecord, attribute);
            this.uPathStore.buildAttributesCache(); // manually trigger, because reaction doesn't work for nested entities
        }
    }

    private static addAttributeToRecord (record: DataRecord | NestedRecord, attribute: DataAbstractAttribute) {
        if (attribute instanceof DataSimpleAttribute) {
            record.simpleAttributes.add(attribute);
        } else if (attribute instanceof DataArrayAttribute) {
            record.arrayAttributes.add(attribute);
        } else if (attribute instanceof DataCodeAttribute && record instanceof DataRecord) {
            record.codeAttributes.add(attribute);
        } else if (attribute instanceof DataComplexAttribute) {
            record.complexAttributes.add(attribute);
        }
    }
}

