/**
 * Store for UPath attributes cache
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import {AbstractAttribute, Entity, LookupEntity, UPathMetaUtil} from '@unidata/meta';
import {IStringKeyMap} from '@unidata/core';
import {observable, ObservableMap} from 'mobx';

export class UPathMetaStore {
    private readonly metaRecord: Entity | LookupEntity;

    @observable
    private attributesCache: ObservableMap<string, AbstractAttribute> = observable.map({});

    constructor (metaRecord: Entity | LookupEntity) {
        this.metaRecord = metaRecord;

        this.buildAttributesCache();
    }

    private buildAttributesCache () {
        const map: IStringKeyMap<AbstractAttribute> = UPathMetaUtil.buildAttributePathMap(this.metaRecord, []);

        this.attributesCache.replace(map);
    }

    public getAttributeByPath (path: string) {
        return this.attributesCache.get(path) || null;
    }

    public getAllMetaAttributes () {
        return this.attributesCache.toJS();
    }
}
