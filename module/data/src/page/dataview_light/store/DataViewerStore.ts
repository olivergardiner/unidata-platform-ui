/**
 * Dataviewer main store
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import {Catalog, CatalogEntity, Entity, EntityGroupService, EntityGroupUtils} from '@unidata/meta';
import {EntityType} from '@unidata/types';
import {MetaRecordStore} from './MetaRecordStore';
import {DataRecord} from '../../../model';
import {Dialog} from '@unidata/core-app';
import {action, observable, runInAction} from 'mobx';
import {DataRecordStore} from './DataRecordStore';
import {ScrollableItemsStore} from './ScrollableItemsStore';
import {DataViewerActionsStore} from './DataViewerActionsStore';
import {DataCardLayoutStore} from './DataCardLayoutStore';
import {DataViewerAccessorStore} from './DataViewerAccessorStore';
import {DataViewerHandlersStore} from './DataViewerHandlersStore';

export class DataViewerStore {

    public entityName: string;

    public etalonId: string | null;

    @observable
    public readOnly: boolean = false;

    @observable
    public title: string;

    @observable
    public isLoading: boolean = false;

    public dataRecordStore: DataRecordStore = new DataRecordStore(new DataRecord({}));

    public metaRecordStore: MetaRecordStore = new MetaRecordStore(new Entity({}));

    public scrollableItemsStore: ScrollableItemsStore = new ScrollableItemsStore();

    public dataCardActionsStore: DataViewerActionsStore = new DataViewerActionsStore();

    public dataCardLayoutStore: DataCardLayoutStore = new DataCardLayoutStore(this.metaRecordStore, this.dataRecordStore);

    public dataAccessorStore: DataViewerAccessorStore = new DataViewerAccessorStore(this.metaRecordStore, this.dataRecordStore);

    public handlersStore: DataViewerHandlersStore = new DataViewerHandlersStore(this);

    constructor (entityName: string, etalonId: string | null, readOnly: boolean) {
        this.entityName = entityName;
        this.etalonId = etalonId;
        this.readOnly = readOnly;
    }

    public loadAll () {
        this.setIsLoading(true);

        return EntityGroupService.getEntityGroup()
            .then((catalogs: Catalog[]) => {

                let catalogList = EntityGroupUtils.toList(catalogs);

                let catalogEntity = catalogList.find((catalog) => {
                    return catalog.name.getValue() === this.entityName;
                });

                if (!catalogEntity) {
                    throw new Error(`Metarecord ${this.entityName} doesn't exist`);
                }

                let entityType = catalogEntity instanceof CatalogEntity ? EntityType.Entity : EntityType.LookupEntity;

                return this.metaRecordStore.loadMetaRecord(entityType, this.entityName);
            })
            .then(() => {
                this.setIsLoading(false);

                if (!this.etalonId) {
                    return this.dataRecordStore.createDataRecord(this.metaRecordStore.getMetaRecord().name.getValue());
                }

                return this.dataRecordStore.loadDataRecord(this.etalonId);
            })
            .catch((e: Error) => {
                Dialog.showError(e.message);
            });
    }

    public saveDataRecord () {
        let dataRecordStore = this.dataRecordStore;

        this.setIsLoading(true);

        let isNewRecord = dataRecordStore.getDataRecord().getPhantom();

        dataRecordStore.saveDataRecord()
            .then((record: DataRecord) => {
                let etalon = record.etalonId.getValue();

                // Redirect to new record
                // ToDo do it correctly via redirect
               if (isNewRecord) {
                   let index = window.location.href.indexOf('/new');

                   window.location.href = window.location.href.substr(0, index) + '/' + etalon;
               } else {
                   this.dataRecordStore.loadDataRecord(etalon);
               }
            });
    }

    @action
    private setIsLoading (isLoading: boolean) {
        this.isLoading = isLoading;
    }
}
