/**
 * Store for building data card layout
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import {computed} from 'mobx';
import {IStringKeyMap, Nullable} from '@unidata/core';
import {AbstractAttribute, ComplexAttribute, Entity, AttributeGroup} from '@unidata/meta';
import {DataCardLayoutItem} from '../type/DataCardLayoutItem';
import {MetaRecordStore} from './MetaRecordStore';
import {LayoutItemType} from '../type/LayoutItemType';
import {DataRecordStore} from './DataRecordStore';

export class DataCardLayoutStore {

    private metaRecordStore: MetaRecordStore;

    private dataRecordStore: Nullable<DataRecordStore>;

    constructor (metaRecordStore: MetaRecordStore, dataRecordStore: DataRecordStore) {
        this.metaRecordStore = metaRecordStore;
        this.dataRecordStore = dataRecordStore;
    }

    @computed
    public get metaDataLayout () {
        let attributes: Map<string, AbstractAttribute> = this.metaRecordStore.getAllMetaAttributes();
        let attrGroupMapping = this.buildAttributeGroupMapping();
        let result: DataCardLayoutItem[] = [];

        attributes
            .forEach((attr: AbstractAttribute) => {
                let type = LayoutItemType.ATTRIBUTE;
                let attrName = attr.name.getValue();

                if (attr instanceof ComplexAttribute) {
                    type = LayoutItemType.COMPLEX_ATTRIBUTE;
                }

                let groupId = attrGroupMapping[attrName] ? attrGroupMapping[attrName].id : null;

                result.push({
                    name: attr.name.getValue(),
                    displayName: attr.displayName.getValue(),
                    group: groupId,
                    type: type,
                    order: attr.order.getValue()
                });
            });

        return result;
    }

    @computed
    public get attributeGroupsLayout (): IStringKeyMap<DataCardLayoutItem []> {
        return this.metaDataLayout
            .reduce((result: IStringKeyMap, attributeLayoutItem: DataCardLayoutItem) => {
                const group = attributeLayoutItem.group || AttributeGroup.UNNAMED_GROUP;

                if (attributeLayoutItem.type === LayoutItemType.COMPLEX_ATTRIBUTE) {
                    return result;
                }

                if (!result[group]) {
                    result[group] = [];
                }

                result[group].push(attributeLayoutItem);

                return result;
            }, {});
    }

    @computed
    public get complexAttributesLayout (): IStringKeyMap<DataCardLayoutItem []> {
        const metaRecord = this.metaRecordStore.getMetaRecord();

        if (!(metaRecord instanceof Entity)) {
            return {};
        }

        return metaRecord.complexAttributes
            .getRange()
            .reduce((result: IStringKeyMap<DataCardLayoutItem []>,
                     complexAttribute: ComplexAttribute) => {
                if (!result[complexAttribute.name.getValue()]) {
                    result[complexAttribute.name.getValue()] = [];
                }

                result[complexAttribute.name.getValue()].push({
                    displayName: complexAttribute.displayName.getValue(),
                    name: complexAttribute.name.getValue(),
                    metaPath: complexAttribute.name.getValue(), // first level attributes only
                    type: LayoutItemType.COMPLEX_ATTRIBUTE,
                    group: null,
                    order: complexAttribute.order.getValue()
                });

                return result;
            }, {});
    }

    private buildAttributeGroupMapping (): IStringKeyMap<{id: string; title: string}> {
        return this.metaRecordStore.getMetaRecord().attributeGroups
            .getRange()
            .reduce((result: IStringKeyMap, group: AttributeGroup) => {
                group.attributes.getValue().forEach((attr: string) => {
                    result[attr] = {id: group.id, title: group.title.getValue()};
                });

                return result;
            }, {});
    }
}
