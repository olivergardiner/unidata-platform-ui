/**
 * Store for UPath attributes cache
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import {IStringKeyMap} from '@unidata/core';
import {DataAbstractAttribute, DataRecord} from '../../../model';
import {UPathDataUtil} from '../../../utils/UPathDataUtil';
import {IReactionDisposer, observable, ObservableMap, reaction} from 'mobx';

export class UPathDataStore {
    @observable
    private dataRecord: DataRecord;

    private readonly autoUpdate: boolean;

    @observable
    private attributesCache: ObservableMap<string, DataAbstractAttribute> = observable.map({});

    constructor (dataRecord: DataRecord, autoUpdate: boolean = true) {
        this.dataRecord = dataRecord;
        this.autoUpdate = autoUpdate;

        if (this.autoUpdate) {
            this.createDataRecordChangeReaction();
        } else {
            this.buildAttributesCache();
        }
    }

    private dataRecordReactionDisposer: IReactionDisposer;

    public buildAttributesCache () {
        const map: IStringKeyMap<DataAbstractAttribute> = UPathDataUtil.buildAttributesPathMap(this.dataRecord);

        this.attributesCache.replace(map);
    }

    public getAttributeByPath (path: string) {
        return this.attributesCache.get(path) || null;
    }

    public setDataRecord (dataRecord: DataRecord) {
        this.dataRecord = dataRecord;
    }

    private createDataRecordChangeReaction () {
        // watching attributes change, to rebuild cache and trigger rerender
        this.dataRecordReactionDisposer = reaction(() => {
            return [
                this.dataRecord.getAllAttributes()
            ];
        }, () => {
            this.buildAttributesCache();
        });
    }
}
