/**
 * Right side menu for dataviewer
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {DataCardNavigation} from './navigation/DataCardNavigation';
import {inject, observer} from 'mobx-react';
import {DataRecordActionGroup} from '../../type/DataRecordActionGroup';
import {DataViewerActionsPanel} from './actions_panel/DataViewerActionsPanel';
import {dataViewerInject, DataViewerProviderProps} from '../../provider/DataViewer';

type IInjectedProps = DataViewerProviderProps<'dataCardActionsStore'> & IProps;

interface IProps {
}

@inject(dataViewerInject)
@observer
export class DataViewerSideMenu extends React.Component<IProps> {
    get injected () {
        return this.props as IInjectedProps;
    }

    get dataCardActionsStore () {
        return this.injected.dataCardActionsStore;
    }

    render () {
        return (
            <div>
                <DataCardNavigation/>
                {this.dataCardActionsStore.dataCardActionGroups.map((group: DataRecordActionGroup) => {
                    return (<DataViewerActionsPanel key={group.name}
                                                    group={group}
                                                    actions={this.dataCardActionsStore.dataCardActions}/>);
                })}
            </div>
        );
    }
}
