/**
 * Right side menu data card actions
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {DataRecordActionGroup} from '../../../type/DataRecordActionGroup';
import {CardPanel} from '@unidata/uikit';
import {DataRecordAction} from '../../../type/DataRecordAction';
import {computed, ObservableMap} from 'mobx';
import {observer} from 'mobx-react';
import {DataRecordActionNameEnum} from '../../../type/DataRecordActionNameEnum';

import * as styles from './DataViewerActionsPanel.m.scss';

interface IProps {
    group: DataRecordActionGroup;
    actions: ObservableMap<DataRecordActionNameEnum, DataRecordAction>;
}

@observer
export class DataViewerActionsPanel extends React.Component<IProps> {
    @computed
    get recordActionsGroup (): DataRecordAction [] {
        return this.props.group.actions
            .map((actionName) => {
                return this.props.actions.get(actionName);
            })
            .filter(a => a !== undefined) as DataRecordAction [];
    }

    render () {
        const {title} = this.props.group;

        return (
            <CardPanel title={title}
                       internal={true}
                       noBodyPadding={true}>
                <div className={styles.groupItems}>
                    {this.recordActionsGroup.map((groupAction) => {
                        return (<li key={groupAction.type}>{groupAction.title}</li>);
                    })}
                </div>
            </CardPanel>
        );
    }
}
