/**
 * Data card navigation panel
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {findDOMNode} from 'react-dom';
import {Skeleton} from 'antd';
import {SectionTitle} from '../../card/section/title/SectionTitle';
import styles from './DataCardNavigation.m.scss';
import {ScrollableItem} from '../../../type/ScrollableItem';
import {dataViewerInject, DataViewerProviderProps} from '../../../provider/DataViewer';

type IInjectedProps = DataViewerProviderProps<'scrollableItemsStore' | 'dataAccessorStore'> & IProps;

interface IProps {}

@inject(dataViewerInject)
@observer
export class DataCardNavigation extends React.Component<IProps> {
    get injected () {
        return this.props as IInjectedProps;
    }

    private scrollTo (ref: React.RefObject<any>) {

        // eslint-disable-next-line react/no-find-dom-node
        let domNode = findDOMNode(ref.current);

        if (!domNode) {
            return;
        }

        if ('scrollIntoView' in domNode) {
            domNode.scrollIntoView({behavior: 'smooth', block: 'start'});
        }
    }

    render () {
        const scrollableItems: ScrollableItem [] = this.injected.scrollableItemsStore
            .getAllScrollableItems();

        return (
            <section className={styles.navigationItemsContainer}>
                <Skeleton paragraph={{rows: 1}}
                          loading={this.injected.dataAccessorStore.isMetaRecordLoading()}
                          active={true}>
                    {
                        scrollableItems.map((item: ScrollableItem) => {
                            return (<li key={item.id} onClick={() => this.scrollTo(item.ref)}>
                                <SectionTitle text={item.displayName}/>
                            </li>);
                        })
                    }
                </Skeleton>
            </section>
        );
    }
}
