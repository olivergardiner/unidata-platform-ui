/**
 * Read mode field with input-style border and edit button
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import * as styles from '../card/attribute/Attribute.m.scss';
import {Button, Icon, joinClassNames, SIZE} from '@unidata/uikit';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import {AbstractDataAttributeStore} from '../card/attribute/store/AbstractDataAttributeStore';

interface IProps {
    attributeStore: AbstractDataAttributeStore;
}

@observer
export class EditableFieldWrapper extends React.Component<IProps> {
    @observable
    showControls: boolean = false;

    constructor (props: IProps) {
        super(props);
    }

    get store () {
        return this.props.attributeStore;
    }

    @action
    onMouseEnter = () => {
        this.showControls = true;
    };

    @action
    onMouseLeave = () => {
        this.showControls = false;
    };

    @action
    onClick = () => {
        this.showControls = false;
        this.props.attributeStore.getUiStore().setEditModeOn();
    };

    @action
    onEditClick = () => {
        this.enableEditMode();
    };

    @action
    disableEditMode = () => {
        this.showControls = false;
        this.store.getUiStore().setEditModeOff();
    }

    @action
    enableEditMode = () => {
        this.showControls = false;
        this.store.getUiStore().setEditModeOn();
    }

    @action
    onRevertClick = () => {
        this.props.attributeStore.revertAttributeValue();

        this.disableEditMode();
    };

    onKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            this.disableEditMode();
        }
    };

    onBlurCapture = () => {
        this.disableEditMode();
    }

    onFocus = () => {
        this.enableEditMode();
    }

    render () {
        const classNames = joinClassNames(styles.editableField, [styles.showControls, this.showControls]);
        const isEditMode = this.props.attributeStore.getUiStore().isEditMode;
        const readOnly = this.props.attributeStore.getReadOnly();

        if (readOnly) {
            return this.props.children;
        }

        if (isEditMode) {
            return (<span className={styles.editableFieldInput}
                          onKeyPress={this.onKeyPress}>
                {this.props.children}
                <span className={styles.editableFieldButtons}>
                    <Button size={SIZE.SMALL}
                            leftIcon={'check'}
                            isMinimal={true}
                            onMouseDown={this.store.getUiStore().setEditModeOff}
                    />
                    <Button size={SIZE.SMALL}
                            leftIcon={'reload'}
                            isMinimal={true}
                            onMouseDown={this.onRevertClick}
                    />
                </span>
            </span>);
        }

        return (
            <span className={classNames}
                  onMouseEnter={this.onMouseEnter}
                  onClick={this.onEditClick}
                  tabIndex={0}
                  onFocus={this.onFocus}
                  onMouseLeave={this.onMouseLeave}>
                            {this.props.children}
                {
                    this.showControls &&
                    <div className={styles.editIcon}>
                        <Icon name={'pencil-line'}/>
                    </div>
                }
            </span>
        );
    }
}
