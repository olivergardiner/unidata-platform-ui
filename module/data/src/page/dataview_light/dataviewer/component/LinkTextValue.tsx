/**
 * Link attribute text value
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {DataSimpleAttribute} from '../../../../model';
import {EmptyValue} from './EmptyValue';
import {observer} from 'mobx-react';
import * as styles from '../card/attribute/Attribute.m.scss';
import {DefaultSimpleAttributeFormatter} from '../../../../formatter/DefaultSimpleAttributeFormatter';
import {joinClassNames} from '@unidata/uikit';
import {IDataAttributeFormatter} from '../../../../formatter/type/IDataAttributeFormatter';
import {computed} from 'mobx';

interface IProps {
    attribute: DataSimpleAttribute;
    readOnly?: boolean;
    target?: string;
    href: string;
}

@observer
export class LinkTextValue extends React.Component<IProps> {
    formatter: IDataAttributeFormatter = new DefaultSimpleAttributeFormatter();

    @computed
    get value () {
        return this.props.attribute ? this.props.attribute.getFormattedValue(this.formatter) : '';
    }

    render () {
        return (
            <a className={joinClassNames(styles.valueContent, styles.valueLink)}
               target={this.props.target || ''}
               href={this.props.href}>
                {this.value || <EmptyValue/>}
            </a>
        );
    }
}
