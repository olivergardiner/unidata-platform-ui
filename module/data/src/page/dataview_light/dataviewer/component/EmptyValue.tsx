/**
 * Empty attribute value
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import * as styles from '../card/attribute/Attribute.m.scss';
import {i18n} from '@unidata/core-app';

interface IProps {
}

export class EmptyValue extends React.PureComponent<IProps> {
    render () {
        return (
            <span className={styles.valueUnset}>
                {i18n.t('module.data>dataview>valueUnset')}
            </span>
        );
    }
}
