/**
 * Array Attribute select for primitive types
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {Select} from '@unidata/uikit';
import {observer} from 'mobx-react';
import {ISelectableDataStore} from '../../card/attribute/type/ISelectableDataStore';
import {DataArrayAttribute} from '../../../../../index';

interface IProps {
    store: ISelectableDataStore;
    onValueChange?: (attribute: DataArrayAttribute, value: string | number) => void;
    onSelect: (attribute: DataArrayAttribute, value: string | number) => void;
    attribute: DataArrayAttribute;
    onBlur: (e: React.FocusEvent) => void;
}

@observer
export class ArrayAttributeSelect extends React.Component<IProps> {
    constructor (props: IProps) {
        super(props);
    }

    get store () {
        return this.props.store;
    }

    get attribute () {
        return this.props.attribute;
    }

    handleOnSelect = (value: string | number) => {
        this.handleLookupSelect(value);
    };

    onChange = (value: any) => {
        if (this.props.onValueChange) {
            this.props.onValueChange(this.attribute, value);
        }
    };

    handleOnDeselect = (value: string | number) => {
        this.store.removeValue(value);
    };

    handleLookupSelect = (value: string | number) => {
        this.store.addValue(value);

        if (this.props.onSelect) {
            this.props.onSelect(this.attribute, value);
        }
    };

    render () {
        return (
            <div onBlur={this.props.onBlur}>
                <Select
                    options={this.props.store.getOptions()}
                    size={'small'}
                    mode={'tags'}
                    isOpen={true}
                    onSelect={this.handleOnSelect}
                    onChange={this.onChange}
                    onDeselect={this.handleOnDeselect}
                    value={this.props.store.getValue()}
                />
            </div>
        );
    }
}
