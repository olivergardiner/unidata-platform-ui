/**
 * Text value for array attribute
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {DataArrayAttribute} from '../../../../model';
import {DefaultArrayAttributeFormatter} from '../../../../formatter/DefaultArrayAttributeFormatter';
import {EmptyValue} from './EmptyValue';
import {observer} from 'mobx-react';
import * as styles from '../card/attribute/Attribute.m.scss';
import {computed} from 'mobx';
import {IDataArrayAttributeFormatter} from '../../../../formatter/type/IDataArrayAttributeFormatter';

interface IProps {
    attribute: DataArrayAttribute;
    readOnly?: boolean;
}

@observer
export class ArrayTextValue extends React.Component<IProps> {
    formatter: IDataArrayAttributeFormatter = new DefaultArrayAttributeFormatter();

    @computed
    get value () {
        return this.props.attribute ? this.props.attribute.getFormattedValue(this.formatter) : [];
    }

    render () {
        const value = this.value;

        return (
            <div className={styles.valueContent}>
                {value.length === 0 ? <EmptyValue/> : value.join(', ')}
            </div>
        );
    }
}
