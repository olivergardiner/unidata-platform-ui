/**
 * Simple text attribute value
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {DefaultSimpleAttributeFormatter} from '../../../../formatter/DefaultSimpleAttributeFormatter';
import {EmptyValue} from './EmptyValue';
import {observer} from 'mobx-react';
import * as styles from '../card/attribute/Attribute.m.scss';
import {IDataAttributeFormatter} from '../../../../formatter/type/IDataAttributeFormatter';
import {computed} from 'mobx';
import {DataSimpleValuedAttribute} from '../../../../model/attribute/DataSimpleValuedAttribute';

interface IProps {
    attribute: DataSimpleValuedAttribute;
}

@observer
export class SimpleTextValue extends React.Component<IProps> {
    constructor (props: IProps) {
        super(props);
    }

    formatter: IDataAttributeFormatter = new DefaultSimpleAttributeFormatter();

    @computed
    get value () {
        return this.props.attribute ? this.props.attribute.getFormattedValue(this.formatter) : '';
    }

    render () {
        const value = this.value;

        return (<span className={styles.valueContent}>
                {value ? value : <EmptyValue/>}
            </span>);
    }
}
