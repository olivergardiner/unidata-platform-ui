/**
 * Number input for attribute value
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import * as styles from './NumberInput.m.scss';

interface IProps {
    value: number;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    onBlur?: (e: React.FocusEvent) => void;
}

export class NumberInput extends React.Component<IProps> {
    render () {
        return (
            <input value={this.props.value}
                   type={'number'}
                   className={styles.input}
                   autoFocus={true}
                   onChange={this.props.onChange}
                   onBlur={this.props.onBlur}
            />);
    }
}
