/**
 * Text input for attribute value
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import * as styles from './TextInput.m.scss';

interface IProps {
    value: string;
    onChange: (e: React.ChangeEvent) => void;
    onBlur?: () => void;
}

export class TextInput extends React.PureComponent<IProps> {
    render () {
        return <input value={this.props.value}
                      autoFocus={true}
                      className={styles.input}
                      onChange={this.props.onChange}
                      onBlur={this.props.onBlur}
        />;
    }
}
