/**
 * Field for display and edit date / date time attribute
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {ISimpleAttributeProps} from '../card/attribute/type/ISimpleAttributeProps';
import {computed} from 'mobx';
import {DataSimpleAttribute} from '../../../../model';
import * as moment from 'moment';
import {DateInput} from '@unidata/uikit';
import {SimpleTextValue} from './SimpleTextValue';
import {observer} from 'mobx-react';

type IProps = {
    format: string;
    showTime: boolean;
} & ISimpleAttributeProps

@observer
export class DateTimeField extends React.Component<IProps> {
    dataPickerRef: React.RefObject<DateInput>;

    constructor (props: IProps) {
        super(props);

        this.dataPickerRef = React.createRef();
    }

    get store () {
        return this.props.attributeStore;
    }

    get uiStore () {
        return this.store.getUiStore();
    }

    get inputId () {
        return 'dateInput_' + this.dataAttribute.modelId;
    }

    get pickerClassName () {
        return 'datePicker_' + this.dataAttribute.modelId;
    }

    @computed
    get dataAttribute (): DataSimpleAttribute {
        return this.props.attributeStore.getDataAttribute() as DataSimpleAttribute;
    }

    @computed
    get metaAttribute () {
        return this.props.attributeStore.getMetaAttribute();
    }

    onChange = (value: moment.Moment) => {
        const formattedValue = value ? value.format(this.props.format) : '';

        this.dataAttribute.value.setValue(formattedValue);

        this.props.attributeStore.onAttributeChange();
    };

    onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
        let picker = document.getElementsByClassName(this.pickerClassName)[0];
        let input = document.getElementById(this.inputId);
        let relatedTarget = e.relatedTarget;
        let target: Node | undefined = e.target;

        if ((relatedTarget instanceof Element && picker && picker.contains(relatedTarget)) ||
            (target && input && input.contains(target))) {
            e.preventDefault();
        } else {
            this.uiStore.setEditModeOff();
        }
    };

    /*
    * For unknown reason, antd autoFocus doesn't work for date components
    * */
    focus () {
        let dateFieldEl = document.getElementsByClassName(this.pickerClassName)[0];

        if (dateFieldEl) {
            let elements = dateFieldEl.getElementsByClassName('ant-calendar-input'); //ToDo don't rely on antd className
            let input = elements.length > 0 && elements[0];

            if (input) {
                (input as HTMLInputElement).focus();
            }
        }
    }

    componentDidMount () {
        setTimeout(() => this.focus(), 30); // waiting for picker to open
    }

    render () {
        let value: string = this.dataAttribute.value.getValue();
        let formattedValue = value ? moment(new Date(value), this.props.format) : undefined;

        if (this.uiStore.isEditMode) {
            return (
                <div onBlur={this.onBlur}
                >
                    <DateInput value={formattedValue}
                               autoFocus={true}
                               ref={this.dataPickerRef}
                               id={this.inputId}
                               style={{width: '100%'}}
                               open={true}
                               dropdownClassName={this.pickerClassName}
                               size={'small'}
                               showTime={this.props.showTime}
                               allowClear={true}
                               onChange={this.onChange}
                    />
                </div>
            );
        }

        return (
            <SimpleTextValue attribute={this.dataAttribute}/>
        );
    }
}
