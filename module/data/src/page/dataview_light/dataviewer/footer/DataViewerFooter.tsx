/**
 * Data viewer footer with buttons
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import * as styles from './DataViewerFooter.m.scss';
import {Button, INTENT} from '@unidata/uikit';
import {inject, observer} from 'mobx-react';
import {i18n} from '@unidata/core-app';
import {dataViewerInject, DataViewerProviderProps} from '../../provider/DataViewer';

type IInjectedProps = DataViewerProviderProps<'handlersStore' | 'dataAccessorStore'> & IProps;

type IProps = {}

@inject(dataViewerInject)
@observer
export class DataViewerFooter extends React.Component<IProps> {
    get propsWithInject () {
        return this.props as IInjectedProps;
    }

    onSaveClick = () => {
        return this.propsWithInject.handlersStore.saveDataRecord();
    };

    render () {
        const isSavable = this.propsWithInject.dataAccessorStore.getCanSaveDataRecord();

        return (
            <footer className={styles.container}>
                <div className={styles.buttonsGroup}>
                    <Button type={'submit'}
                            isRound={true}
                            intent={INTENT.PRIMARY}
                            isDisabled={!isSavable}
                            onClick={this.onSaveClick}>
                        {i18n.t('data>saveButtonText')}
                    </Button>
                </div>
            </footer>
        );
    }
}
