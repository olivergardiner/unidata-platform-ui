/**
 * Header for data viewer section
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import * as styles from './DataViewerHeader.m.scss';

interface IProps {
}

// ToDo denis.makarov decide whether it is still needed
export class DataViewerHeader extends React.Component<IProps> {
    render () {
        return (
            <header className={styles.header}>
            </header>
        );
    }
}
