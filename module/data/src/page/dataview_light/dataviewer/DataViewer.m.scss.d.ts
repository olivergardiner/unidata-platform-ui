export const container: string;
export const dataViewerContent: string;
export const dataViewerLayout: string;
export const dataViewerLayoutRow: string;
export const dataViewerLayoutCol: string;
export const dataCardContent: string;
export const nestedRecordContainer: string;
export const nestedRecordButtonGroup: string;
