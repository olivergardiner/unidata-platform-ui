/**
 * Section with data record display and edit
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import {InlineMessage, UdInlineMessageType} from '@unidata/uikit';
import {Affix, Col, Row} from 'antd';
import {DataViewerSideMenu} from './side_menu/DataViewerSideMenu';
import {DataViewerFooter} from './footer/DataViewerFooter';
import * as styles from './DataViewer.m.scss';
import {inject, observer} from 'mobx-react';
import {dataViewerInject, DataViewerProviderProps} from '../provider/DataViewer';
import {DataCard} from './card/DataCard';
import {i18n} from '@unidata/core-app';
import {observable, runInAction} from 'mobx';

type IInjectedProps = DataViewerProviderProps<'dataAccessorStore'> & IProps

interface IProps {}

@inject(dataViewerInject)
@observer
export class DataViewer extends React.Component<IProps> {
    @observable
    hasErrors: boolean = false;

    get injected () {
        return this.props as IInjectedProps;
    }

    componentDidCatch (error: Error, errorInfo: React.ErrorInfo) {
        runInAction(() => {
            this.hasErrors = true;
        });
    }

    render () {
        const {dataViewerLayout: layout, dataViewerLayoutCol: col, dataViewerLayoutRow: row} = styles;

        if (this.hasErrors) {
            return (<InlineMessage type={UdInlineMessageType.ERROR}>
                {i18n.t('module.data>dataview>unknownError')}
            </InlineMessage>);
        }

        //ToDo replace CardPanel with div
        return (
            <section className={styles.container}>
                <section className={styles.dataViewerContent} id={'dataViewerContent'}>
                    <div className={layout}>
                        <Row className={row}>
                            <Col className={col} xs={0} md={0} lg={0} xl={3} xxl={6}/>
                            <Col className={col} xs={24} md={16} lg={16} xl={14} xxl={12}>
                                <DataCard/>
                            </Col>
                            <Col className={col} xs={0} md={8} lg={8} xl={7} xxl={6}>
                                <Row className={row}>
                                    <Col className={col} span={1}/>
                                    <Col className={col} span={22}>
                                        {/*ToDo replace with position: sticky*/}
                                        <Affix
                                            offsetTop={12}
                                            target={() => {
                                                return document.getElementById('dataViewerContent');
                                            }}>
                                            <DataViewerSideMenu/>
                                        </Affix>
                                    </Col>
                                    <Col className={col} span={1}/>
                                </Row>
                            </Col>
                        </Row>
                    </div>
                </section>
                <DataViewerFooter/>
            </section>
        );
    }
}
