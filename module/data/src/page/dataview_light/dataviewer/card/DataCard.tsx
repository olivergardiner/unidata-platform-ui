/**
 * Data card
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import {CardPanel, InlineMessage, UdInlineMessageType} from '@unidata/uikit';
import {Skeleton} from 'antd';
import {AttributesPanel} from './attribute/AttributesPanel';
import {inject, observer} from 'mobx-react';
import {dataViewerInject, DataViewerProviderProps} from '../../provider/DataViewer';
import {observable, runInAction} from 'mobx';
import {i18n} from '@unidata/core-app';
import * as styles from '../DataViewer.m.scss';

interface IProps {
}


type IInjectedProps = DataViewerProviderProps<'dataAccessorStore'> & IProps;

@inject(dataViewerInject)
@observer
export class DataCard extends React.Component<IProps> {
    @observable
    hasErrors: boolean = false;

    get injected () {
        return this.props as IInjectedProps;
    }

    componentDidCatch (error: Error, errorInfo: React.ErrorInfo) {
        runInAction(() => {
            this.hasErrors = true;
        });
    }

    render () {
        const loading = this.injected.dataAccessorStore.isMetaRecordLoading();

        if (this.hasErrors) {
            return (<InlineMessage type={UdInlineMessageType.ERROR}>
                {i18n.t('module.data>dataview>unknownError')}
            </InlineMessage>);
        }

        return (
            <section>
                <CardPanel scrollY={false}>
                    <div className={styles.dataCardContent}>
                        <Skeleton loading={loading}
                                  active={true}
                                  paragraph={{rows: 10}}>
                            <AttributesPanel/>
                        </Skeleton>
                    </div>
                </CardPanel>
            </section>
        );
    }
}
