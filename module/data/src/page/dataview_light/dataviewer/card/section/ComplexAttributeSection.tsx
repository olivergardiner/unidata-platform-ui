/**
 * Section with complex attribute
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import {ComplexAttribute, NestedEntity} from '@unidata/meta';
import {Button, CardPanel, INTENT, SIZE} from '@unidata/uikit';
import {SectionTitle} from './title/SectionTitle';
import {SectionContent} from './content/SectionContent';
import {inject, observer} from 'mobx-react';
import {computed} from 'mobx';
import {DataComplexAttribute} from '../../../../../model/attribute/DataComplexAttribute';
import {NestedEntityPanel} from './NestedEntityPanel';
import {Skeleton} from 'antd';
import {NestedRecord} from '../../../../../model/NestedRecord';
import {dataViewerInject, DataViewerProviderProps} from '../../../provider/DataViewer';
import * as styles from '../../DataViewer.m.scss';

type IInjectedProps = DataViewerProviderProps<'scrollableItemsStore' | 'dataAccessorStore' | 'handlersStore'> & IProps;

interface IProps {
    dataPath: string;
    metaPath: string;
    isScrollable: boolean;
    inner?: boolean;
}

@inject(dataViewerInject)
@observer
export class ComplexAttributeSection extends React.Component<IProps> {
    ref: React.RefObject<any>;

    constructor (props: IProps) {
        super(props);

        this.ref = React.createRef();
    }

    get injected () {
        return this.props as IInjectedProps;
    }

    get scrollableItemsStore () {
        return this.injected.scrollableItemsStore;
    }

    @computed
    get dataAttribute () {
        let dataAttribute = this.injected.dataAccessorStore.getDataAttribute(this.props.dataPath);

        if (dataAttribute && !(dataAttribute instanceof DataComplexAttribute)) {
            throw new Error('Data attribute type mismatch, expected DataComplexAttribute');
        }

        return dataAttribute;
    }

    @computed
    get metaAttribute () {
        let metaAttribute = this.injected.dataAccessorStore.getMetaAttribute(this.props.metaPath);

        if (!metaAttribute) {
            throw new Error('MetaAttribute should be present');
        }

        if (!(metaAttribute instanceof ComplexAttribute)) {
            throw new Error('MetaAttribute should be present');
        }

        return metaAttribute;
    }

    extraButtons = () => {
        return <Button isRound={true}
                       intent={INTENT.DEFAULT}
                       size={SIZE.SMALL}
                       leftIcon={'plus'}
                       onClick={this.createAttribute}>
        </Button>;
    };

    createAttribute = (e: React.MouseEvent) => {
        e.stopPropagation();

        this.injected.handlersStore.createComplexAttribute(this.metaAttribute,
            this.dataAttribute,
            this.props.dataPath);
    };

    removeAttribute = (nestedRecord: NestedRecord) => {
        this.injected.handlersStore.removeNestedRecord(nestedRecord, this.dataAttribute, this.props.dataPath);
    };

    componentDidMount () {
        this.addScrollableItem();
    }

    private addScrollableItem () {
        if (this.props.isScrollable && this.scrollableItemsStore.addScrollableItem && this.metaAttribute) {
            this.scrollableItemsStore.addScrollableItem(this.props.metaPath, this.metaAttribute.displayName.getValue(), this.ref);
        }
    }

    private renderNestedRecords (nestedEntity: NestedEntity | null) {
        if (!this.dataAttribute || !nestedEntity) {
            return null;
        }

        return this.dataAttribute.nestedRecords
            .getRange()
            .map((record: NestedRecord, index) => {
                return (
                    <section key={record.modelId}
                             className={styles.nestedRecordContainer}>
                        <div className={styles.nestedRecordButtonGroup}>
                            <Button
                                leftIcon={'trash2'}
                                size={SIZE.MIDDLE}
                                isMinimal={true}
                                onClick={this.removeAttribute.bind(this, record)}/>
                        </div>
                        <NestedEntityPanel record={record}
                                           entity={nestedEntity}
                                           index={index}
                                           metaPath={this.props.metaPath}
                                           dataPath={this.props.dataPath}
                        />
                    </section>
                );
            });
    }

    render () {
        let metaAttribute = this.metaAttribute;

        if (!metaAttribute) {
            return null;
        }

        const nestedEntity = metaAttribute.nestedEntity;

        return (
            <CardPanel
                internal
                isCollapsed={false}
                noBodyPadding={true}
                extraButtons={this.extraButtons()}
                ref={this.ref}
                key={metaAttribute.name.getValue()}
                title={<SectionTitle text={metaAttribute.displayName.getValue()}/>}
            >
                <SectionContent inner={this.props.inner}>
                    <Skeleton loading={this.injected.dataAccessorStore.isDataRecordLoading()} active={true}>
                        {this.renderNestedRecords(nestedEntity)}
                    </Skeleton>
                </SectionContent>
            </CardPanel>
        );
    }
}
