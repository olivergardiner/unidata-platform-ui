/**
 * Panel for nested entity
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {NestedRecord} from '../../../../../model/NestedRecord';
import {AbstractAttribute, ComplexAttribute, NestedEntity} from '@unidata/meta';
import {Attribute} from '../attribute/Attribute';
import {ComplexAttributeSection} from './ComplexAttributeSection';

interface IProps {
    record: NestedRecord;
    entity: NestedEntity;
    index: number;
    dataPath: string;
    metaPath: string;
}

export class NestedEntityPanel extends React.Component<IProps> {
    getAttributes () {
        return this.props.entity
            .getAllAttributes()
            .sort((attr, attr2) => {
                if (attr instanceof ComplexAttribute && !(attr2 instanceof ComplexAttribute)) {
                    return 1;
                }

                if (attr2 instanceof ComplexAttribute && !(attr instanceof ComplexAttribute)) {
                    return -1;
                }

                return attr.order.getValue() > attr2.order.getValue() ? 1 : -1;
            });

    }

    render () {
        let attributes = this.getAttributes();

        return (<>
            {
                attributes.map((attr: AbstractAttribute) => {
                    let attributeName = attr.name.getValue();
                    let dataPath = this.props.dataPath + `[${this.props.index}].` + attr.name.getValue();
                    let metaPath = this.props.metaPath + `.` + attr.name.getValue();

                    if (attr instanceof ComplexAttribute) {
                        return <ComplexAttributeSection dataPath={dataPath}
                                                        key={dataPath}
                                                        metaPath={metaPath}
                                                        isScrollable={false}/>;
                    }

                    return (<Attribute
                        key={dataPath}
                        dataPath={dataPath}
                        metaPath={metaPath}
                        attributeName={attributeName}
                    />);
                })
            }
        </>);

    }
}
