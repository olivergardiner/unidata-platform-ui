/**
 * Section content layout
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import * as styles from './SectionContent.m.scss';
import {joinClassNames} from '@unidata/uikit';

interface IProps {
    inner?: boolean;
}

export class SectionContent extends React.PureComponent<IProps> {
    render () {
        return (
            <div className={joinClassNames(styles.contentBlock, [styles.inner, Boolean(this.props.inner)])}>
                {this.props.children}
            </div>
        );
    }
}
