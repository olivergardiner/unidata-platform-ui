/**
 * Section with attribute group
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import {Attribute} from '../attribute/Attribute';
import {CardPanel} from '@unidata/uikit';
import {inject, observer} from 'mobx-react';
import {DataCardLayoutItem} from '../../../type/DataCardLayoutItem';
import {SectionTitle} from './title/SectionTitle';
import {SectionContent} from './content/SectionContent';
import {dataViewerInject, DataViewerProviderProps} from '../../../provider/DataViewer';

type IInjectedProps = DataViewerProviderProps<'handlersStore' | 'dataAccessorStore'> & IProps;

type IProps = {
    title: string;
    groupId: string;
    isScrollable: boolean;
    inner?: boolean;
    attributes: DataCardLayoutItem[];
}

@inject(dataViewerInject)
@observer
export class AttributesGroupSection extends React.Component<IProps> {
    ref: React.RefObject<any>;

    get injected () {
        return this.props as IInjectedProps;
    }

    constructor (props: IProps) {
        super(props);

        this.ref = React.createRef();
    }

    componentDidMount () {
        if (this.props.isScrollable && this.injected.handlersStore) {
            this.injected.handlersStore.addScrollableItem(this.props.groupId, this.props.title, this.ref);
        }
    }

    render () {
        const title = this.props.title;
        const groupId = this.props.groupId;
        const attributes = this.props.attributes;

        return (
            <CardPanel
                internal={true}
                noBodyPadding={true}
                ref={this.ref}
                isCollapsed={false}
                key={groupId}
                title={<SectionTitle text={title}/>}
            >
                <SectionContent inner={Boolean(this.props.inner)}>
                    {attributes
                        .sort((attr1, attr2) => attr1.order > attr2.order ? 1 : -1)
                        .map((attr) => {
                        return (<Attribute key={attr.name}
                                           dataPath={attr.name}
                                           metaPath={attr.name}
                                           attributeName={attr.name}
                        />);
                    })}
                </SectionContent>
            </CardPanel>
        );
    }
}
