/**
 * Section title
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import * as styles from './SectionTitle.m.scss';

interface IProps {
    text: string;
}

export class SectionTitle extends React.PureComponent<IProps> {
    render () {
        return (
            <div className={styles.title}>
                {this.props.text}
            </div>
        );
    }
}
