/**
 * Array type Attribute Store
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {observable} from 'mobx';
import {Nullable} from '@unidata/core';
import {AbstractDataAttributeStore} from './AbstractDataAttributeStore';
import {DataArrayAttribute} from '../../../../../../index';
import {ArrayAttribute} from '@unidata/meta';

export class ArrayAttributeStore extends AbstractDataAttributeStore {
    @observable
    protected dataAttribute: Nullable<DataArrayAttribute>;

    @observable
    protected metaAttribute: ArrayAttribute;

    @observable
    protected localDataAttribute: DataArrayAttribute;

    public getDataAttribute (): DataArrayAttribute {
        return this.dataAttribute || this.localDataAttribute;
    }

    public getMetaAttribute (): ArrayAttribute {
        return this.metaAttribute;
    }

    public getReadOnly (): boolean {
        return this.accessorStore.getReadOnly();
    }

    public isUsingLocalAttribute (): boolean {
        return Boolean(this.dataAttribute);
    }

    public setDataAttribute (attribute: DataArrayAttribute): void {
        this.dataAttribute = attribute;
    }
}
