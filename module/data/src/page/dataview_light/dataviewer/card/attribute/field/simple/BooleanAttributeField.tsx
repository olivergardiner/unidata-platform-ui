/**
 * Boolean attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {Select, Value} from '@unidata/uikit';
import {DataSimpleAttribute} from '../../../../../../../model';
import {SimpleTextValue} from '../../../../component/SimpleTextValue';
import {ISimpleAttributeProps} from '../../type/ISimpleAttributeProps';
import {i18n} from '@unidata/core-app';

@observer
export class BooleanAttributeField extends React.Component<ISimpleAttributeProps> {
    options = [{
        value: 'true',
        title: i18n.t('common:true')
    }, {
        value: 'false',
        title: i18n.t('common:false')
    }, {
        value: 'unset',
        title: i18n.t('common:unset')
    }];

    get store () {
        return this.props.attributeStore;
    }

    get uiStore () {
        return this.props.attributeStore.getUiStore();
    }

    get attribute () {
        const attribute = this.props.attributeStore.getDataAttribute();

        if ((attribute instanceof DataSimpleAttribute)) {
            return attribute;
        } else {
            throw new Error('Attribute type mismatch');
        }
    }

    handleOnSelect = (value: Value) => {
        let localValue: boolean | null = null;

        if (value === 'true') {
            localValue = true;
        } else if (value === 'false') {
            localValue = false;
        }

        this.attribute.value.setValue(localValue);

        this.props.attributeStore.onAttributeChange();
    };

    render () {
        const attr = this.attribute;
        const value = attr.value.getValue();

        if (this.uiStore.isEditMode) {
            return (
                <div onBlur={this.uiStore.setEditModeOff}>
                    <Select
                        options={this.options}
                        size={'small'}
                        isOpen={true}
                        value={value === null ? undefined : value.toString()}
                        onSelect={this.handleOnSelect}
                    />
                </div>);
        }

        return (
            <SimpleTextValue attribute={attr}/>
        );
    }
}
