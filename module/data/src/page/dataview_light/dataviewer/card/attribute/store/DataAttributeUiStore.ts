/**
 * Store for ui state of the attribute
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {action, observable} from 'mobx';

export class DataAttributeUiStore {
    @observable
    public isEditMode: boolean = false;

    @action
    public setEditModeOn = () => {
        this.isEditMode = true;
    };

    @action
    public setEditModeOff = () => {
        this.isEditMode = false;
    };
}
