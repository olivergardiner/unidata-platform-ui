/**
 * Store for lookup attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {action, observable, runInAction} from 'mobx';
import {LookupDataStore} from './LookupDataStore';
import {SimpleAttribute} from '@unidata/meta';
import {ISelectableDataStore} from '../type/ISelectableDataStore';
import {ILookupOption} from '../type/ILookupOption';
import {DataSimpleAttribute} from '../../../../../../index';

export class LookupAttributeStore implements ISelectableDataStore {
    @observable
    public dataAttribute: DataSimpleAttribute;

    @observable
    public metaAttribute: SimpleAttribute;

    @observable
    public isInitiallyLoaded: boolean = false;

    private lookupDataStore = new LookupDataStore();

    constructor (dataAttribute: DataSimpleAttribute, metaAttribute: SimpleAttribute) {
        this.dataAttribute = dataAttribute;
        this.metaAttribute = metaAttribute;
    }

    @action
    public setLookupValue (value: string | null) {
        this.dataAttribute.value.setValue(value);
    }

    public getValue () {
        return this.dataAttribute.value.getValue();
    }

    public loadData () {
        return this.lookupDataStore
            .loadLookupData(this.metaAttribute.lookupEntityType.getValue())
            .then(() => {
                runInAction(() => {
                    this.isInitiallyLoaded = true;
                });
            });
    }

    public addValue (value: string, displayValue: string) {
        this.dataAttribute.value.setValue(value);
        this.dataAttribute.displayValue.setValue(displayValue);
    }

    public removeValue (value: string) {
        this.dataAttribute.value.setValue(null);
    }

    public getOptions (): ILookupOption[] {
        return this.lookupDataStore.data;
    }

    public getIsLoading (): boolean {
        return this.lookupDataStore.isLoading;
    }

}
