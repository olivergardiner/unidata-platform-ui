/**
 * Integer attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {ISimpleAttributeProps} from '../../type/ISimpleAttributeProps';
import {DataSimpleAttribute} from '../../../../../../../model';
import {SimpleTextValue} from '../../../../component/SimpleTextValue';
import {computed} from 'mobx';

@observer
export class NumberAttributeField extends React.Component<ISimpleAttributeProps> {
    constructor (props: ISimpleAttributeProps) {
        super(props);
    }

    get store () {
        return this.props.attributeStore;
    }

    get uiStore () {
        return this.props.attributeStore.getUiStore();
    }

    @computed
    get dataAttribute (): DataSimpleAttribute {
        return this.props.attributeStore.getDataAttribute() as DataSimpleAttribute;
    }

    @computed
    get metaAttribute () {
        return this.props.attributeStore.getMetaAttribute();
    }

    onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let value = e.target.value;

        this.dataAttribute.value.setValue(Number(value));

        this.props.attributeStore.onAttributeChange();
    };

    render () {
        if (this.uiStore.isEditMode) {
            return <input value={this.dataAttribute.value.getValue()}
                          type={'number'}
                          autoFocus={true}
                          onChange={this.onChange}
                          onBlur={this.uiStore.setEditModeOff}
            />;
        }

        return (
            <SimpleTextValue attribute={this.dataAttribute}/>
        );
    }
}
