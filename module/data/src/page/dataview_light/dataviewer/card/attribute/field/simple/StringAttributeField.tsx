/**
 * String attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {DataSimpleAttribute} from '../../../../../../../model';
import {SimpleTextValue} from '../../../../component/SimpleTextValue';
import {computed} from 'mobx';
import {SimpleAttributeStore} from '../../store/SimpleAttributeStore';
import {TextInput} from '../../../../component/input/text/TextInput';

interface IProps {
    attributeStore: SimpleAttributeStore;
}

@observer
export class StringAttributeField extends React.Component<IProps> {
    constructor (props: IProps) {
        super(props);
    }

    get store () {
        return this.props.attributeStore;
    }

    get uiStore () {
        return this.props.attributeStore.getUiStore();
    }

    @computed
    get attribute (): DataSimpleAttribute {
        return this.props.attributeStore.getDataAttribute() as DataSimpleAttribute;
    }

    onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let value = e.target.value;

        this.attribute.value.setValue(value);

        this.props.attributeStore.onAttributeChange();
    };

    render () {
        if (this.uiStore.isEditMode) {
            return <TextInput value={this.attribute.value.getValue()}
                          onChange={this.onChange}
                          onBlur={this.uiStore.setEditModeOff}
            />;
        }

        return (
            <SimpleTextValue attribute={this.attribute}/>
        );
    }
}
