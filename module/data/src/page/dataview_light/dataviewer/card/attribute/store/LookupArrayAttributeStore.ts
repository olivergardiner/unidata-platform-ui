/**
 * Store for lookup array attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {action, computed, observable, runInAction} from 'mobx';
import {LookupDataStore} from './LookupDataStore';
import {DataArrayAttribute, DataArrayAttributeItem} from '../../../../../../model';
import {ArrayAttribute} from '@unidata/meta';
import {ISelectableDataStore} from '../type/ISelectableDataStore';
import {ILookupOption} from '../type/ILookupOption';

export class LookupArrayAttributeStore implements ISelectableDataStore {
    @observable
    public dataAttribute: DataArrayAttribute;

    @observable
    public metaAttribute: ArrayAttribute;

    @observable
    public isInitiallyLoaded: boolean = false;

    @observable
    public lookupData: ILookupOption[];

    private lookupDataStore = new LookupDataStore();

    constructor (dataAttribute: DataArrayAttribute, metaAttribute: ArrayAttribute) {
        this.dataAttribute = dataAttribute;
        this.metaAttribute = metaAttribute;
    }

    @computed
    private get valuesCache (): Map<any, DataArrayAttributeItem> {
        let cache = this.dataAttribute.value
            .getRange()
            .reduce((result: Map<any, DataArrayAttributeItem>, item: DataArrayAttributeItem) => {
                const value = item.value.getValue();

                result.set(value, item);

                return result;
            }, new Map());

        return cache;
    }

    public getValue () {
        return this.dataAttribute.value
            .getRange()
            .map((attribute) => {
                return attribute.value.getValue();
            });
    }

    public loadData () {
        return this.lookupDataStore
            .loadLookupData(this.metaAttribute.lookupEntityType.getValue())
            .then(() => {
                runInAction(() => {
                    this.isInitiallyLoaded = true;
                });
            });
    }

    @action
    public addValue (value: string | number, displayValue: string): void {
        if (!this.valuesCache.has(value)) {
            this.dataAttribute.value.add(new DataArrayAttributeItem({value: value}));
        }
    }

    @action
    public removeValue (value: string | number): void {
        let item = this.valuesCache.get(value);

        if (item) {
            this.dataAttribute.value.remove(item);
        }
    }

    public getOptions (): ILookupOption[] {
        return this.lookupDataStore.data;
    }

    public getIsLoading (): boolean {
        return this.lookupDataStore.isLoading;
    }

}
