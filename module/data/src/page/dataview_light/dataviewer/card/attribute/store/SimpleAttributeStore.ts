/**
 * Simple data attribute implementation
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {observable} from 'mobx';
import {Nullable} from '@unidata/core';
import {DataSimpleValuedAttribute} from '../../../../../../model/attribute/DataSimpleValuedAttribute';
import {AbstractDataAttributeStore} from './AbstractDataAttributeStore';
import {AbstractSimpleAttribute} from '../../../../../../../../meta/src/model/attribute/AbstractSimpleAttribute';

export class SimpleAttributeStore extends AbstractDataAttributeStore {
    @observable
    protected dataAttribute: Nullable<DataSimpleValuedAttribute>;

    @observable
    protected metaAttribute: AbstractSimpleAttribute;

    @observable
    protected localDataAttribute: DataSimpleValuedAttribute;

    public getDataAttribute (): DataSimpleValuedAttribute {
        return this.dataAttribute || this.localDataAttribute;
    }

    public getMetaAttribute (): AbstractSimpleAttribute {
        return this.metaAttribute;
    }

}
