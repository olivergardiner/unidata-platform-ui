import {IOption} from '@unidata/uikit';

export interface ILookupOption extends IOption {
    displayValue: string;
    etalonId: string;
}
