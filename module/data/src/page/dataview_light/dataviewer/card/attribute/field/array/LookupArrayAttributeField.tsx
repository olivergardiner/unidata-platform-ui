/**
 * Lookup array attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {ArrayTextValue} from '../../../../component/ArrayTextValue';
import {IArrayAttributeProps} from '../../type/IArrayAttributeProps';
import {LookupArrayAttributeStore} from '../../store/LookupArrayAttributeStore';

//ToDo readonly stub for LookupArrayAttribute
@observer
export class LookupArrayAttributeField extends React.Component<IArrayAttributeProps> {
    lookupStore: LookupArrayAttributeStore;

    constructor (props: IArrayAttributeProps) {
        super(props);

        let metaAttribute = this.store.getMetaAttribute();
        let dataAttribute = this.store.getDataAttribute();

        this.lookupStore = new LookupArrayAttributeStore(dataAttribute, metaAttribute);
    }

    get store () {
        return this.props.attributeStore;
    }

    componentDidUpdate () {
        if (!this.lookupStore.isInitiallyLoaded) {
            this.lookupStore.loadData();
        }
    }

    render () {
        return (
            <ArrayTextValue attribute={this.store.getDataAttribute()}/>
        );
    }
}
