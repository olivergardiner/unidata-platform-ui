/**
 * Dictionary array attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {LookupArrayAttributeStore} from '../../store/LookupArrayAttributeStore';
import {DataArrayAttribute} from '../../../../../../../model';
import {ArrayTextValue} from '../../../../component/ArrayTextValue';
import {IArrayAttributeProps} from '../../type/IArrayAttributeProps';


//ToDo readonly stub for DictionaryArrayAttribute
@observer
export class DictionaryArrayAttributeField extends React.Component<IArrayAttributeProps> {
    private store: LookupArrayAttributeStore;

    constructor (props: IArrayAttributeProps) {
        super(props);
    }

    get attributeStore () {
        return this.props.attributeStore;
    }

    get attribute (): DataArrayAttribute {
        return this.props.attributeStore.getDataAttribute();
    }

    render () {
        return (
            <ArrayTextValue attribute={this.attribute}/>
        );
    }
}
