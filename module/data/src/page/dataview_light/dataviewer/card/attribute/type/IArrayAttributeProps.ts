/**
 * Props for array attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {ArrayAttributeStore} from '../store/ArrayAttributeStore';

export interface IArrayAttributeProps {
    attributeStore: ArrayAttributeStore;
}
