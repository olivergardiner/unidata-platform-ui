/**
 * Abstract Data Attribute Store
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {action, computed, observable, reaction} from 'mobx';
import {DataAttributeUtils} from '../../../../../../utils/DataAttributeUtils';
import {DataAbstractAttribute} from '../../../../../../index';
import {AbstractAttribute} from '@unidata/meta';
import {Nullable} from '@unidata/core';
import {DataViewerHandlersStore} from '../../../../store/DataViewerHandlersStore';
import {DataViewerAccessorStore} from '../../../../store/DataViewerAccessorStore';
import {DataAttributeUiStore} from './DataAttributeUiStore';

export abstract class AbstractDataAttributeStore {

    @observable
    protected dataAttribute: Nullable<DataAbstractAttribute>;

    @observable
    protected metaAttribute: AbstractAttribute;

    @observable
    protected localDataAttribute: DataAbstractAttribute;

    protected metaPath: string;

    protected dataPath: string;

    protected uiStore: DataAttributeUiStore = new DataAttributeUiStore();

    protected handlersStore: DataViewerHandlersStore;

    protected accessorStore: DataViewerAccessorStore;


    constructor (metaAttribute: AbstractAttribute,
                 dataAttribute: Nullable<DataAbstractAttribute>,
                 metaPath: string,
                 dataPath: string,
                 handlersStore: DataViewerHandlersStore,
                 accessorStore: DataViewerAccessorStore) {

        this.metaAttribute = metaAttribute;
        this.dataAttribute = dataAttribute;
        this.metaPath = metaPath;
        this.dataPath = dataPath;
        this.handlersStore = handlersStore;
        this.accessorStore = accessorStore;

        this.createLocalDataAttribute();
    }

    public isUsingLocalAttribute () {
        return this.isUsingLocal;
    }

    public getUiStore () {
        return this.uiStore;
    }

    public getReadOnly (): boolean {
        return this.accessorStore.getReadOnly();
    }

    @computed
    private get isUsingLocal () {
        return !this.dataAttribute;
    }

    @action
    private createLocalDataAttribute () {
        this.localDataAttribute = DataAttributeUtils.buildDataAttributeByMetaAttribute(this.metaAttribute, {});
    }

    public onAttributeChange (): void {
        if (this.isUsingLocalAttribute()) {
            this.handlersStore.addAttributeToDataRecord(this.localDataAttribute, this.dataPath);
        }
    }

    public revertAttributeValue (): void {
        this.getDataAttribute().revert();
    }

    @action
    public setDataAttribute (dataAttribute: Nullable<DataAbstractAttribute>) {
        this.dataAttribute = dataAttribute;
    }


    @action
    public getDataAttribute () {
        return this.dataAttribute || this.localDataAttribute;
    }
}
