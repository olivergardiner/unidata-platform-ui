/**
 * Store with logic for attribute component creation
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {Nullable} from '@unidata/core';
import {AttributeTypeCategory, SIMPLE_DATA_TYPE} from '@unidata/types';
import {StringAttributeField} from '../field/simple/StringAttributeField';
import {BooleanAttributeField} from '../field/simple/BooleanAttributeField';
import {IntegerAttributeField} from '../field/simple/IntegerAttributeField';
import {NumberAttributeField} from '../field/simple/NumberAttributeField';
import {LookupAttributeField} from '../field/simple/LookupAttributeField';
import {LinkAttributeField} from '../field/simple/LinkAttributeField';
import {EnumAttributeField} from '../field/simple/EnumAttributeField';
import {StringArrayAttributeField} from '../field/array/StringArrayAttributeField';
import {AbstractAttribute, AliasCodeAttribute, ArrayAttribute, CodeAttribute, SimpleAttribute} from '@unidata/meta';
import {DateAttributeField} from '../field/simple/DateAttributeField';
import {DataViewerHandlersStore} from '../../../../store/DataViewerHandlersStore';
import {DataViewerAccessorStore} from '../../../../store/DataViewerAccessorStore';
import {DataAbstractAttribute} from '../../../../../../index';
import * as React from 'react';
import {ReactElement} from 'react';
import {SimpleAttributeStore} from './SimpleAttributeStore';
import {ArrayAttributeStore} from './ArrayAttributeStore';
import {AbstractDataAttributeStore} from './AbstractDataAttributeStore';
import {DictionaryAttributeField} from '../field/simple/DictionaryAttributeField';
import {DateTimeAttributeField} from '../field/simple/DateTimeAttributeField';

export class AttributeFactoryStore {
    attributeStore: AbstractDataAttributeStore;
    attributeFieldCmp: Nullable<ReactElement>;
    private readonly metaAttribute: AbstractAttribute;
    private readonly dataAttribute: Nullable<DataAbstractAttribute>;
    private readonly handlersStore: DataViewerHandlersStore;
    private readonly accessorStore: DataViewerAccessorStore;
    private readonly metaPath: string;
    private readonly dataPath: string;

    constructor (metaAttribute: AbstractAttribute,
                 dataAttribute: Nullable<DataAbstractAttribute>,
                 metaPath: string,
                 dataPath: string,
                 handlersStore: DataViewerHandlersStore,
                 accessorStore: DataViewerAccessorStore
    ) {
        this.metaAttribute = metaAttribute;
        this.dataAttribute = dataAttribute;
        this.metaPath = metaPath;
        this.dataPath = dataPath;
        this.handlersStore = handlersStore;
        this.accessorStore = accessorStore;
    }

    build () {
        const metaAttribute = this.metaAttribute;

        let fieldCmp;
        let store: SimpleAttributeStore | ArrayAttributeStore;

        if (metaAttribute instanceof SimpleAttribute ||
            metaAttribute instanceof CodeAttribute ||
            metaAttribute instanceof AliasCodeAttribute) {
            store = this.buildSimpleAttributeStore();
            fieldCmp = this.createSimpleField(metaAttribute, store);
        } else if (metaAttribute instanceof ArrayAttribute) {
            store = this.buildArrayAttributeStore();
            fieldCmp = this.createArrayField(metaAttribute, store);
        } else {
            throw new Error(`Cannot create field for attribute "${metaAttribute.name.getValue()}"`);
        }

        this.attributeStore = store;
        this.attributeFieldCmp = fieldCmp;
    }

    private createPrimitiveTypeField (dataType: string, store: SimpleAttributeStore) {
        switch (dataType) {
            case SIMPLE_DATA_TYPE.STRING:
                return (<StringAttributeField attributeStore={store}/>);
            case SIMPLE_DATA_TYPE.BOOLEAN:
                return (<BooleanAttributeField attributeStore={store}/>);
            case SIMPLE_DATA_TYPE.INTEGER:
                return (<IntegerAttributeField attributeStore={store}/>);
            case SIMPLE_DATA_TYPE.NUMBER:
                return (<NumberAttributeField attributeStore={store}/>);
            case SIMPLE_DATA_TYPE.DATE:
                return (<DateAttributeField attributeStore={store}/>);
            case SIMPLE_DATA_TYPE.TIMESTAMP:
                return (<DateTimeAttributeField attributeStore={store}/>);
            default:
                return null;
        }
    }

    private getPrimitiveArrayTypeField (dataType: string, store: ArrayAttributeStore) {
        switch (dataType) {
            case SIMPLE_DATA_TYPE.STRING:
                return (<StringArrayAttributeField attributeStore={store}/>);
            default:
                return null;
        }
    }

    private createSimpleField (metaAttribute: SimpleAttribute | CodeAttribute | AliasCodeAttribute, store: SimpleAttributeStore) {
        const typeCategory = metaAttribute.typeCategory;

        if (typeCategory === AttributeTypeCategory.simpleDataType) {
            return this.createPrimitiveTypeField(metaAttribute.simpleDataType.getValue(), store);
        } else {
            return this.createSimpleDataTypeField(typeCategory, store);
        }
    }

    private createSimpleDataTypeField (typeCategory: string, simpleAttributeStore: SimpleAttributeStore) {
        switch (typeCategory) {
            case AttributeTypeCategory.lookupEntityType:
                return <LookupAttributeField attributeStore={simpleAttributeStore}/>;
            case AttributeTypeCategory.linkDataType:
                return <LinkAttributeField attributeStore={simpleAttributeStore}/>;
            case AttributeTypeCategory.enumDataType:
                return <EnumAttributeField attributeStore={simpleAttributeStore}/>;
            case AttributeTypeCategory.dictionaryDataType:
                return <DictionaryAttributeField attributeStore={simpleAttributeStore}/>;
            default:
                return null;
        }

    }

    private createArrayField (metaAttribute: ArrayAttribute, store: ArrayAttributeStore) {
        const typeCategory = metaAttribute.typeCategory;

        if (typeCategory === AttributeTypeCategory.arrayDataType) {
            return this.getPrimitiveArrayTypeField(metaAttribute.arrayDataType.getValue(), store);
        } else {
            return this.createArrayDataTypeField(typeCategory, store);
        }
    }

    private createArrayDataTypeField (typeCategory: string, arrayAttributeStore: ArrayAttributeStore) {
        // ToDo implement

        return null;
    }

    private buildSimpleAttributeStore (): SimpleAttributeStore {
        return new SimpleAttributeStore(
            this.metaAttribute,
            this.dataAttribute,
            this.metaPath,
            this.dataPath,
            this.handlersStore,
            this.accessorStore);
    }

    private buildArrayAttributeStore () {
        return new ArrayAttributeStore(
            this.metaAttribute,
            this.dataAttribute,
            this.metaPath,
            this.dataPath,
            this.handlersStore,
            this.accessorStore
        );
    }
}
