/**
 * String array attribute store
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {action, computed, observable} from 'mobx';
import {DataArrayAttribute, DataArrayAttributeItem} from '../../../../../../model';
import {ArrayAttribute} from '@unidata/meta';
import {ISelectableDataStore} from '../type/ISelectableDataStore';
import {ILookupOption} from '../type/ILookupOption';

export class StringArrayAttributeStore implements ISelectableDataStore {
    @observable
    public dataAttribute: DataArrayAttribute;

    @observable
    public metaAttribute: ArrayAttribute;

    constructor (dataAttribute: DataArrayAttribute, metaAttribute: ArrayAttribute) {
        this.dataAttribute = dataAttribute;
        this.metaAttribute = metaAttribute;
    }

    @computed
    private get valuesCache (): Map<any, DataArrayAttributeItem> {
        let cache = this.dataAttribute.value
            .getRange()
            .reduce((result: Map<any, DataArrayAttributeItem>, item: DataArrayAttributeItem) => {
                const value = item.value.getValue();

                result.set(value, item);

                return result;

            }, new Map());

        return cache;
    }

    public addValue (value: string, displayValue?: string): void {
        if (!this.valuesCache.has(value)) {
            this.dataAttribute.value.add(new DataArrayAttributeItem({value: value}));
        }
    }

    public getIsLoading (): boolean {
        return false;
    }

    public getOptions (): ILookupOption[] {
        return [];
    }

    public getValue (): string | string[] {
        return this.dataAttribute.value
            .getRange()
            .map((attribute) => {
                return attribute.value.getValue();
            });
    }

    public isInitiallyLoaded: boolean = true;

    public loadData (): void {
    }

    public removeValue (value: string | number | null): void {
        let item = this.valuesCache.get(value);

        if (item) {
            this.dataAttribute.value.remove(item);
        }
    }
}
