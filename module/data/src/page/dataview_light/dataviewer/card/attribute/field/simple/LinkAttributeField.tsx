/**
 * Web link attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {DataSimpleAttribute} from '../../../../../../../model';
import {ISimpleAttributeProps} from '../../type/ISimpleAttributeProps';
import {LinkTextValue} from '../../../../component/LinkTextValue';

@observer
export class LinkAttributeField extends React.Component<ISimpleAttributeProps> {
    constructor (props: ISimpleAttributeProps) {
        super(props);
    }

    get store () {
        return this.props.attributeStore;
    }

    get attribute (): DataSimpleAttribute {
        return this.store.getDataAttribute() as DataSimpleAttribute;
    }

    render () {
        return (
            <LinkTextValue href={this.attribute.value.getValue()}
                           target={'_blank'}
                           attribute={this.attribute}/>
        );
    }
}
