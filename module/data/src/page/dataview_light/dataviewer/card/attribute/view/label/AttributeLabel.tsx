/**
 * Attribute label
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {AbstractAttribute} from '@unidata/meta';
import * as styles from './AttributeLabel.m.scss';
import {observer} from 'mobx-react';
import {joinClassNames} from '@unidata/uikit';

interface IProps {
    attribute: AbstractAttribute;
}

@observer
export class AttributeLabel extends React.Component<IProps> {
    render () {
        const nullableField = (this.props.attribute as any).nullable;

        let required = false;

        if (nullableField) {
            required = !nullableField.getValue();
        }

        return (
            <div className={joinClassNames(styles.label, [styles.attrRequired, required])}>
                <span>
                    {this.props.attribute.displayName.getValue()}
                </span>
            </div>
        );
    }
}
