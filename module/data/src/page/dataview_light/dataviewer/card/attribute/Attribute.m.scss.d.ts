
export const container: string;
export const labelContainer: string;
export const valueContainer: string;
export const loadingIndicator: string;
export const valueUnset: string;
export const editableField: string;
export const editableFieldInput: string;
export const editableFieldButtons: string;
export const showControls: string;
export const valueContent: string;
export const valueLink: string;
export const editIcon: string;

