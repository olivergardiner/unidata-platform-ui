/**
 * Dictionary attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {DataSimpleAttribute} from '../../../../../../../model';
import {ISimpleAttributeProps} from '../../type/ISimpleAttributeProps';
import {SimpleTextValue} from '../../../../component/SimpleTextValue';

@observer
export class DictionaryAttributeField extends React.Component<ISimpleAttributeProps> {
    constructor (props: ISimpleAttributeProps) {
        super(props);
    }

    get store () {
        return this.props.attributeStore;
    }

    get attribute (): DataSimpleAttribute {
        return this.store.getDataAttribute() as DataSimpleAttribute;
    }

    render () {

        return (
            <SimpleTextValue attribute={this.attribute}/>
        );
    }
}
