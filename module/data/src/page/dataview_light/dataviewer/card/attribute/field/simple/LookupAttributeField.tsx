/**
 * Lookup attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {ISimpleAttributeProps} from '../../type/ISimpleAttributeProps';
import {SimpleTextValue} from '../../../../component/SimpleTextValue';
import {LookupSelect} from './lookup/LookupSelect';
import {LookupAttributeStore} from '../../store/LookupAttributeStore';
import {SimpleAttribute} from '@unidata/meta';
import {Nullable} from '@unidata/core';
import {DataSimpleAttribute} from '../../../../../../../index';

@observer
export class LookupAttributeField extends React.Component<ISimpleAttributeProps> {
    lookupStore: Nullable<LookupAttributeStore>;

    constructor (props: ISimpleAttributeProps) {
        super(props);

        let metaAttribute = this.store.getMetaAttribute();
        let dataAttribute = this.store.getDataAttribute();

        if (metaAttribute instanceof SimpleAttribute && dataAttribute instanceof DataSimpleAttribute) {
            this.lookupStore = new LookupAttributeStore(dataAttribute, metaAttribute);
        }
    }

    get store () {
        return this.props.attributeStore;
    }

    get uiStore () {
        return this.store.getUiStore();
    }

    get attribute () {
        return this.store.getDataAttribute();
    }

    render () {
        if (!this.lookupStore) {
            return null;
        }

        if (this.uiStore.isEditMode) {
            return (
                <LookupSelect store={this.lookupStore}
                              onChange={this.onChange}
                              onBlur={this.uiStore.setEditModeOff}
                />
            );
        }

        return (
            <SimpleTextValue attribute={this.attribute}/>
        );
    }

    private onChange = (value: string) => {
        this.props.attributeStore.onAttributeChange();
    };
}
