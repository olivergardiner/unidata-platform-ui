/**
 * Date time attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {ISimpleAttributeProps} from '../../type/ISimpleAttributeProps';
import {DateTimeField} from '../../../../component/DateTimeField';
import {UnidataConfig} from '@unidata/core-app';
import {SimpleTextValue} from '../../../../component/SimpleTextValue';

@observer
export class DateTimeAttributeField extends React.Component<ISimpleAttributeProps> {
    render () {
        if (this.props.attributeStore.getUiStore().isEditMode) {
            return <DateTimeField {...this.props}
                                  format={UnidataConfig.getServerDateTimeFormat()}
                                  showTime={true}/>;
        }

        return <SimpleTextValue attribute={this.props.attributeStore.getDataAttribute()}/>;
    }
}
