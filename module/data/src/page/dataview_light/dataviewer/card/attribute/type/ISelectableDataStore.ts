/**
 * Common interface for selectable data store
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {DataAbstractAttribute} from '../../../../../../model';
import {AbstractAttribute} from '@unidata/meta';
import {ILookupOption} from './ILookupOption';

export interface ISelectableDataStore {
    addValue: (value: string | number, displayValue?: string) => void;
    removeValue: (value: string | number) => void;
    loadData: () => void;
    dataAttribute: DataAbstractAttribute;
    metaAttribute: AbstractAttribute;
    getOptions: () => ILookupOption[];
    getValue: () => string | string [];
    getIsLoading: () => boolean;
    isInitiallyLoaded: boolean;
}
