/**
 * Lookup data store, normally used by attribute stores.
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as moment from 'moment';
import {action, observable, runInAction} from 'mobx';
import {AbstractAttribute, LookupEntity, MetaRecordService} from '@unidata/meta';
import {EntityType} from '@unidata/types';
import {SearchService} from '../../../../../../service/SearchService';
import {Dialog, SearchHit} from '@unidata/core-app';
import {ILookupOption} from '../type/ILookupOption';

export class LookupDataStore {
    @observable
    public isDataLoaded: boolean = false;

    @observable
    public isLoading: boolean = false;

    @observable
    public data: ILookupOption [] = [];

    @action
    private setIsLoading (isLoading: boolean) {
        this.isLoading = isLoading;
    }

    /*
     * Loading lookup data
     * ToDo cache
     * ToDo paging
     */
    public loadLookupData (lookupEntityType: string) {
        this.setIsLoading(true);

        return MetaRecordService.getMetaRecord(EntityType.LookupEntity, lookupEntityType)
            .then((metaRecord: LookupEntity) => {
                let codeAttributeName: string = '';

                if (!metaRecord.codeAttribute) {
                    throw new Error('Code attribute must be set');
                }

                codeAttributeName = metaRecord.codeAttribute.name.getValue();

                let mainDisplayableField = metaRecord
                    .getMetaAttributesByPropertyValue('mainDisplayable', true)
                    .map((attr: AbstractAttribute) => attr.name.getValue())[0];

                let fields: string [] = Array.from(new Set([codeAttributeName, mainDisplayableField]));

                return Promise.all([fields, mainDisplayableField, codeAttributeName]);
            })
            .then(([fields, mainDisplayableAttribute, codeAttribute]) => {
                let searchResultPromise = SearchService.getSearchResult({
                    sortFields: [],
                    asOf: moment(Date.now()).format('YYYY-MM-DD\\THH:mm:ss.SSS'),
                    entity: lookupEntityType,
                    fetchAll: true,
                    searchFields: fields,
                    returnFields: fields,
                    countOnly: false,
                    count: 9999,
                    page: 0,
                    start: 0
                });

                return Promise.all([searchResultPromise, fields, mainDisplayableAttribute, codeAttribute]);
            })
            .then(([response, fields, mainDisplayableAttribute, codeAttribute]) => {
                    const searchHits = response.searchRecords;

                    runInAction(() => {
                        this.data = this.buildLookupData(searchHits, fields, mainDisplayableAttribute, codeAttribute);
                        this.isDataLoaded = true;
                    });
                }
            )
            .catch((error: Error) => {
                Dialog.showError(`Error loading ${lookupEntityType} data`, error.message);

            }).finally(() => {
                this.setIsLoading(false);
            });
    }

    private buildLookupData (data: SearchHit[], fieldNames: string [], mainDisplayableAttribute: string, codeAttribute: string) {
        let options: ILookupOption [] = data
            .map((searchHit: SearchHit) => {
                const plainData = searchHit.getAsObject();

                let values: string [] = [];

                fieldNames.forEach((fieldName) => {
                    values.push(plainData[fieldName]);
                });

                values.filter((val) => Boolean(val));

                const option: ILookupOption = {
                    value: plainData[codeAttribute],
                    title: values.join(' | '),
                    etalonId: plainData['$etalon_id'],
                    displayValue: plainData[mainDisplayableAttribute]
                };

                return option;
            });

        return options;
    }

}
