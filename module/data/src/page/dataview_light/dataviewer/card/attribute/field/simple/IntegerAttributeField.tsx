/**
 * Integer attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {ISimpleAttributeProps} from '../../type/ISimpleAttributeProps';
import {SimpleTextValue} from '../../../../component/SimpleTextValue';
import {computed} from 'mobx';
import {NumberInput} from '../../../../component/input/number/NumberInput';

@observer
export class IntegerAttributeField extends React.Component<ISimpleAttributeProps> {
    constructor (props: ISimpleAttributeProps) {
        super(props);
    }

    get store () {
        return this.props.attributeStore;
    }

    get uiStore () {
        return this.props.attributeStore.getUiStore();
    }

    @computed
    get dataAttribute () {
        return this.props.attributeStore.getDataAttribute();
    }

    @computed
    get metaAttribute () {
        return this.props.attributeStore.getMetaAttribute();
    }

    onChange = (e:  React.ChangeEvent<HTMLInputElement>) => {
        let value = e.target.value;
        // ToDo implement validation

        if (value.includes('.')) {
            value = value.toString().split('.')[0];
        }

        this.dataAttribute.value.setValue(Number(value));

        this.props.attributeStore.onAttributeChange();

    };

    render () {
        if (this.uiStore.isEditMode) {
            return <NumberInput value={this.dataAttribute.value.getValue()}
                          onChange={this.onChange}
                          onBlur={this.uiStore.setEditModeOff}
            />;
        }

        return (
            <SimpleTextValue attribute={this.dataAttribute}/>
        );
    }
}
