/**
 * Lookup select
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {ISelectableDataStore} from '../../../type/ISelectableDataStore';
import {Select} from '@unidata/uikit';
import {computed} from 'mobx';

interface IProps {
    store: ISelectableDataStore;
    onChange?: (value: string) => void;
    onBlur?: () => void;
}

@observer
export class LookupSelect extends React.Component<IProps> {
    constructor (props: IProps) {
        super(props);

        if (!this.props.store.getIsLoading() && !this.props.store.isInitiallyLoaded) {
            this.props.store.loadData();
        }
    }

    get store () {
        return this.props.store;
    }

    @computed
    get lookupData () {
        return this.store.getOptions();
    }

    handleOnSelect = (value: string) => {
        if (this.props.onChange) {
            this.props.onChange(value);
        }

        let option = this.lookupData.find((option) => option.value === value);
        let displayValue = option ? option.displayValue : '';

        this.store.addValue(value, displayValue);
    };

    handleOnDeselect = (value: string) => {
        if (this.props.onChange) {
            this.props.onChange(value);
        }

        this.store.removeValue(value);
    };

    render () {
        const lookupData = this.lookupData;
        const lookupValue = this.store.getValue();
        const isLoading = this.store.getIsLoading();

        return (
            <div onBlur={this.props.onBlur}>
                <Select
                    options={lookupData}
                    size={'small'}
                    isOpen={true}
                    value={lookupValue}
                    onSelect={this.handleOnSelect}
                    onDeselect={this.handleOnDeselect}
                    loading={isLoading}
                />
            </div>

        );
    }
}
