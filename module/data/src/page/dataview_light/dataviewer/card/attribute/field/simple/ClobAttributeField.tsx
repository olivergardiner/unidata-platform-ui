/**
 * Clob attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {ISimpleAttributeProps} from '../../type/ISimpleAttributeProps';
import {DataSimpleAttribute} from '../../../../../../../model';
import {LinkTextValue} from '../../../../component/LinkTextValue';

@observer
export class ClobAttributeField extends React.Component<ISimpleAttributeProps> {
    constructor (props: ISimpleAttributeProps) {
        super(props);
    }

    get store () {
        return this.props.attributeStore;
    }

    get attribute (): DataSimpleAttribute {
        return this.store.getDataAttribute() as DataSimpleAttribute;
    }

    get href () {
        const value = this.attribute.value.getValue();

        return value ? value.filename : '';
    }

    render () {
        return (
            <LinkTextValue href={this.href}
                           attribute={this.attribute}/>
        );
    }
}
