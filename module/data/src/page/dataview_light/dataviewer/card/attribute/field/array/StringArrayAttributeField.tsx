/**
 * String array attribute field
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {observer} from 'mobx-react';
import {StringArrayAttributeStore} from '../../store/StringArrayAttributeStore';
import {IArrayAttributeProps} from '../../type/IArrayAttributeProps';
import {ArrayAttributeSelect} from '../../../../component/select/ArrayAttributeSelect';
import {ArrayTextValue} from '../../../../component/ArrayTextValue';

//ToDo readonly stub for StringArrayAttribute
@observer
export class StringArrayAttributeField extends React.Component<IArrayAttributeProps> {
    stringArraySelectStore: StringArrayAttributeStore;

    constructor (props: IArrayAttributeProps) {
        super(props);

        this.stringArraySelectStore = new StringArrayAttributeStore(
            this.store.getDataAttribute(),
            this.store.getMetaAttribute()
        );
    }

    get store () {
        return this.props.attributeStore;
    }

    get attribute () {
        return this.store.getDataAttribute();
    }

    get uiStore () {
        return this.store.getUiStore();
    }

    onSelect = () => {
        this.store.onAttributeChange();
    };

    render () {
        if (this.uiStore.isEditMode) {
            return <ArrayAttributeSelect
                onSelect={this.onSelect}
                onBlur={this.uiStore.setEditModeOff}
                attribute={this.attribute}
                store={this.stringArraySelectStore}
            />;
        }

        return (
            <ArrayTextValue attribute={this.attribute}/>
        );
    }
}
