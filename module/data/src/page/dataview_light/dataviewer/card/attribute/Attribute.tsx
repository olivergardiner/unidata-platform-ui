/**
 * Attribute with label and value
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {AttributeLabel} from './view/label/AttributeLabel';
import {LoadingIndicator} from '@unidata/uikit';
import * as styles from './Attribute.m.scss';
import {dataViewerInject, DataViewerProviderProps} from '../../../provider/DataViewer';
import {computed} from 'mobx';
import {DataValuedAttribute} from '../../../../../model/attribute/DataValuedAttribute';
import {DataAttributeValueFactory} from './view/value/DataAttributeValueFactory';

type IInjectedProps = DataViewerProviderProps<'handlersStore' | 'dataAccessorStore'> & IProps;

type IProps = {
    dataPath: string;
    metaPath: string;
    attributeName: string;
}

@inject(dataViewerInject)
@observer
export class Attribute extends React.Component<IProps> {
    constructor (props: IProps) {
        super(props);
    }

    get injected () {
        return this.props as IInjectedProps;
    }

    @computed
    get dataAttribute () {
        let dataAttribute = this.injected.dataAccessorStore.getDataAttribute(this.props.dataPath);

        if (dataAttribute && !(dataAttribute instanceof DataValuedAttribute)) {
            throw new Error('Data attribute type mismatch');
        }

        return dataAttribute;
    }

    @computed
    get metaAttribute () {
        return this.injected.dataAccessorStore.getMetaAttribute(this.props.metaPath);
    }

    render () {
        const metaAttribute = this.metaAttribute;
        const isValueLoading = this.injected.dataAccessorStore.isDataRecordLoading();

        if (!metaAttribute) {
            return null;
        }

        return (
            <div key={metaAttribute.name.getValue()} className={styles.container}>
                <div className={styles.labelContainer}>
                    <AttributeLabel attribute={metaAttribute}/>
                </div>
                <div className={styles.valueContainer}>
                    {
                        isValueLoading &&
                        <span className={styles.loadingIndicator}>
                                <LoadingIndicator height={4} width={24}/>
                        </span> ||
                        <DataAttributeValueFactory metaPath={this.props.metaPath}
                                                   dataPath={this.props.dataPath}
                                                   metaAttribute={metaAttribute}
                                                   dataAttribute={this.dataAttribute}
                        />
                    }
                </div>
            </div>
        );
    }

}
