/**
 * Part of datacard where attributes are displayed
 *
 * @author Denis Makarov
 * @date 2020-08-21
 */

import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {AttributesGroupSection} from '../section/AttributesGroupSection';
import {ComplexAttributeSection} from '../section/ComplexAttributeSection';
import {i18n} from '@unidata/core-app';
import {dataViewerInject, DataViewerProviderProps} from '../../../provider/DataViewer';

type IInjectedProps = DataViewerProviderProps<'layoutStore' | 'dataAccessorStore'> & IProps;

interface IProps {}

@inject(dataViewerInject)
@observer
export class AttributesPanel extends React.Component<IProps> {
    get injected () {
        return this.props as IInjectedProps;
    }

    get layoutStore () {
        return this.injected.layoutStore;
    }

    get dataAccessorStore () {
        return this.injected.dataAccessorStore;
    }

    render () {
        const attributeGroupsLayout = this.layoutStore.attributeGroupsLayout;
        const complexAttributesLayout = this.layoutStore.complexAttributesLayout;

        return (
            <>
                {
                    attributeGroupsLayout &&
                    Object.keys(attributeGroupsLayout)
                        .map((groupId) => {
                            const group = this.dataAccessorStore.getAttributeGroupById(groupId);

                            let groupTitle;

                            if (group) {
                                groupTitle = group.title.getValue();
                            } else {
                                groupTitle = i18n.t('data>noGroup');
                            }

                            const attributes = attributeGroupsLayout[groupId];

                            return (
                                <AttributesGroupSection title={groupTitle}
                                                        key={groupId}
                                                        groupId={groupId}
                                                        attributes={attributes}
                                                        isScrollable={true}/>
                            );
                        })
                }
                {
                    complexAttributesLayout &&
                    Object.keys(complexAttributesLayout)
                        .map((attributeName) => {
                            return (
                                <ComplexAttributeSection
                                    key={attributeName}
                                    metaPath={attributeName}
                                    dataPath={attributeName}
                                    isScrollable={true}
                                    inner={false}/>
                            );
                        })
                }
            </>
        );
    }
}
