/**
 * Props for simple attribute
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {SimpleAttributeStore} from '../store/SimpleAttributeStore';

export interface ISimpleAttributeProps {
    attributeStore: SimpleAttributeStore;
}
