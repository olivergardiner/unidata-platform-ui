/**
 * Factory for data attribute value
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import * as React from 'react';
import {AttributeFactoryStore} from '../../store/AttributeFactoryStore';
import {AbstractAttribute} from '@unidata/meta';
import {DataAbstractAttribute} from '../../../../../../../index';
import {Nullable} from '@unidata/core';
import {inject, observer, Provider} from 'mobx-react';
import {dataViewerInject, DataViewerProviderProps} from '../../../../../provider/DataViewer';
import {AbstractDataAttributeStore} from '../../store/AbstractDataAttributeStore';
import {EditableFieldWrapper} from '../../../../component/EditableFieldWrapper';
import {UdLogger} from '@unidata/core-app';

interface IProps {
    dataPath: string;
    metaPath: string;
    metaAttribute: AbstractAttribute;
    dataAttribute: Nullable<DataAbstractAttribute>;
}

type IInjectedProps = DataViewerProviderProps<'handlersStore' | 'dataAccessorStore'> & IProps;

@inject(dataViewerInject)
@observer
export class DataAttributeValueFactory extends React.Component<IProps> {
    factoryStore: AttributeFactoryStore;

    attributeStore: AbstractDataAttributeStore;

    get injected () {
        return this.props as IInjectedProps;
    }

    constructor (props: IProps) {
        super(props);

        this.factoryStore = new AttributeFactoryStore(
            this.props.metaAttribute,
            this.props.dataAttribute,
            this.props.metaPath,
            this.props.dataPath,
            this.injected.handlersStore,
            this.injected.dataAccessorStore);

        this.factoryStore.build();
        this.attributeStore = this.factoryStore.attributeStore;
    }

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<{}>) {
        if (prevProps.dataAttribute !== this.props.dataAttribute) {
            this.attributeStore.setDataAttribute(this.props.dataAttribute);
        }
    }

    render () {
        const field = this.factoryStore.attributeFieldCmp;

        if (!field) {
            UdLogger.warn(`Can't create field for attribute ${this.props.dataPath}`);

            return  null;
        }

        return (
            <Provider {...{attributeStore: this.attributeStore}}>
                <EditableFieldWrapper attributeStore={this.attributeStore}>
                    {field}
                </EditableFieldWrapper>
            </Provider>
        );
    }
}
