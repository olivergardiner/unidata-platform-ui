import {LayoutItemType} from './LayoutItemType';

export type DataCardLayoutItem = {
    name: string;
    displayName: string;
    metaPath?: string;
    group: string | null;
    type: LayoutItemType;
    order: number;
    children?: DataCardLayoutItem [];
}
