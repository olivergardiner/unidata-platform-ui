/**
 * DataRecord action types
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {DataRecordActionNameEnum} from './DataRecordActionNameEnum';

export type DataRecordAction = DataRecordActionBase & (DataRecordActionWithUrl | DataRecordActionWithHandler);

type DataRecordActionBase = {
    type: DataRecordActionNameEnum;
    title: string;
}

type DataRecordActionWithUrl = {
    url: string;
}

type DataRecordActionWithHandler = {
    handler: () => void;
}
