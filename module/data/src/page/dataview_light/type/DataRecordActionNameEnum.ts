/**
 * DataRecord action names enum
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

export enum DataRecordActionNameEnum {
    REFRESH = 'REFRESH',
    MERGE = 'MERGE',
    PUBLISH_JMS = 'PUBLISH_JMS',
    CLONE = 'CLONE',
    HISTORY = 'HISTORY',
    BACK_REL = 'BACK_REL',
    ORIGINAL = 'ORIGINAL'
}
