/**
 * DataRecord action group
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {DataRecordActionNameEnum} from './DataRecordActionNameEnum';

export type DataRecordActionGroup = {
    name: string;
    title: string;
    actions: DataRecordActionNameEnum [];
    icon: string;
}
