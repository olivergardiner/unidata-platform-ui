/**
 * Scrollable item
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import React from 'react';

export type ScrollableItem = {
    id: string;
    displayName: string;
    ref: React.RefObject<any>;
}
