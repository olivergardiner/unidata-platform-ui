/**
 * Inject function for DataViewer provider
 *
 * @author Denis Makarov
 * @date 2020-08-24
 */

import {DataCardLayoutStore} from '../store/DataCardLayoutStore';
import {DataViewerAccessorStore} from '../store/DataViewerAccessorStore';
import {ScrollableItemsStore} from '../store/ScrollableItemsStore';
import {DataViewerActionsStore} from '../store/DataViewerActionsStore';
import {DataViewerHandlersStore} from '../store/DataViewerHandlersStore';

export function dataViewerInject (stores: DataViewerProvider): DataViewerProvider {
    return {
        ...stores
    };
}

export type DataViewerProvider = {
    layoutStore: DataCardLayoutStore;
    dataAccessorStore: DataViewerAccessorStore;
    scrollableItemsStore: ScrollableItemsStore;
    dataCardActionsStore: DataViewerActionsStore;
    handlersStore: DataViewerHandlersStore;
}

export type DataViewerProviderProps<T extends keyof DataViewerProvider> = Pick<ReturnType<typeof dataViewerInject>, T>;
