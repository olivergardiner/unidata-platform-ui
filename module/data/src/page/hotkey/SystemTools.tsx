/**
 * System tools
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-20
 */
import * as React from 'react';
import {DownloadFileService, i18n, UserManager} from '@unidata/core-app';
import {Button, HotKeyModal, INTENT, TabBar, Menu} from '@unidata/uikit';
import * as styles from './SystemTools.m.scss';

export class SystemTools extends React.Component {

    constructor (props: any) {
        super(props);

        this.onClickDownload = this.onClickDownload.bind(this);
    }

    onClickDownload () {
        return DownloadFileService.byUrl('system/system/logs');
    }

    render () {
        if (!UserManager.isUserAdmin()) {
            return null;
        }

        return (
            <HotKeyModal
                header={i18n.t('app.hotkey>systemTools>title')}
                letterKey={'a'}
                ctrlKey={true}
                shiftKey={true}
            >
                <TabBar defaultSelectedKeys={['logs']}>
                    <Menu.Item
                        key={'logs'}
                    >
                        {i18n.t('app.hotkey>systemTools>logs')}
                    </Menu.Item>
                    <Menu.Item
                        key={'system'}
                        disabled
                    >
                        {i18n.t('app.hotkey>systemTools>system')}
                    </Menu.Item>
                </TabBar>
                <div className={styles.contentContainer}>
                    <Button
                        intent={INTENT.PRIMARY}
                        onClick={this.onClickDownload}
                    >
                        {i18n.t('app.hotkey>systemTools>downloadLogs')}
                    </Button>
                </div>
            </HotKeyModal>
        );
    }

}
