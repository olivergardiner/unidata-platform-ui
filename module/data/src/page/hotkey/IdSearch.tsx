/**
 * Search bt etalonId, externalId, originId
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-17
 */
import * as React from 'react';
import {AppTypeManager, i18n} from '@unidata/core-app';
import {Button, HotKeyModal, Input, INTENT, Col, Form, Radio, Row, RadioChangeEvent} from '@unidata/uikit';
import {action, observable} from 'mobx';
import {EntitySelect, EntityStoreManager, EntityTreeType, SourceSystemSelect} from '@unidata/meta';
import {observer} from 'mobx-react';
import * as styles from './IdSearch.m.scss';
import {DataRecordIdSearchService} from '../../service/DataRecordIdSearchService';
import {IdSearchResult} from '../../model/idsearch/IdSearchResult';
import {Redirect} from 'react-router';
import {OperationError, OperationErrorEnum} from '@unidata/core';

interface IProps {
}

interface IFormValues {
    [key: string]: string | undefined;
    searchTarget?: SearchTarget;
    etalonId?: string;
    externalId?: string;
    sourceSystem?: string;
    entityName?: string;
    originId?: string;
}

enum SearchTarget {
    ETALON_ID = 'etalonId',
    EXTERNAL_ID = 'externalId',
    ORIGIN_ID = 'originId'
}

interface IValidationItem {
    validateStatus: undefined | 'error' | 'warning' | 'success';
    help: string | undefined;
}

@observer
export class IdSearch extends React.Component<IProps> {

    @observable
    isOpen: boolean = false;

    @observable
    redirectTo: string | undefined;

    @observable
    formValues: IFormValues = {
        searchTarget: SearchTarget.ETALON_ID
    };

    @observable
    formValidationValues: {[key: string]: IValidationItem} = {}

    formValidationRules: {[key: string]: Array<(name: string) => boolean>} = {
        [SearchTarget.ETALON_ID]: [
            this.checkEmpty,
            this.checkIdValid
        ],
        [SearchTarget.EXTERNAL_ID]: [
            this.checkEmpty
        ],
        [SearchTarget.ORIGIN_ID]: [
            this.checkEmpty,
            this.checkIdValid
        ],
        entityName: [
            this.checkEmpty
        ],
        sourceSystem: [
            this.checkEmpty
        ]
    }

    constructor (props: IProps) {
        super(props);

        this.onSearchTargetChange = this.onSearchTargetChange.bind(this);
        this.onIdChange = this.onIdChange.bind(this);
        this.onEntitySelect = this.onEntitySelect.bind(this);
        this.onSourceSystemChange = this.onSourceSystemChange.bind(this);
        this.setIsOpen = this.setIsOpen.bind(this);
        this.onOpen = this.onOpen.bind(this);
        this.onClose = this.onClose.bind(this);
        this.doSearch = this.doSearch.bind(this);
        this.openRecord = this.openRecord.bind(this);
        this.notFound = this.notFound.bind(this);
        this.renderInputs = this.renderInputs.bind(this);
    }

    @action
    getFormValidationValue (name: string): IValidationItem {
        if (!this.formValidationValues[name]) {
            this.formValidationValues[name] = observable(
                {
                    help: undefined,
                    validateStatus: undefined
                }
            );
        }

        return this.formValidationValues[name];
    }

    @action
    resetFormValidationValue (name: string) {
        this.getFormValidationValue(name).help = undefined;
        this.getFormValidationValue(name).validateStatus = undefined;
    }

    @action
    checkEmpty (name: string): boolean {
        if (!this.formValues[name] || this.formValues[name] === '') {
            this.getFormValidationValue(name).validateStatus = 'error';
            this.getFormValidationValue(name).help = i18n.t('app.hotkey>idSearch>validation>required');

            return false;
        }

        return true;
    }

    @action
    checkIdValid (name: string) {
        let value = this.formValues[name],
            pattern = /^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i,
            valid = false;

        if (value) {
            valid = pattern.test(value);

            if (!valid) {
                this.getFormValidationValue(name).validateStatus = 'error';
                this.getFormValidationValue(name).help = i18n.t('app.hotkey>idSearch>validation>incorrectUUIDPattern');
            }
        }

        return valid;
    }

    validate (name: string): boolean {
        let rules = this.formValidationRules[name];

        if (rules) {
            for (let rule of rules) {
                if (!rule.call(this, name)) {
                    return false;
                }
            }
        }

        return true;
    }

    @action
    onSearchTargetChange (event: RadioChangeEvent) {
        this.formValues.searchTarget = event.target.value;
    }

    @action
    onIdChange (event: React.ChangeEvent<HTMLInputElement>) {
        this.resetFormValidationValue(event.target.name);
        this.formValues[event.target.name] = event.target.value;
    }

    renderRadio () {
        return Object.values(SearchTarget).map(function (target: string) {
            return <Radio key={target} value={target}>{target}</Radio>;
        });
    }

    renderInputs () {
        return Object.values(SearchTarget).map(function (target: string) {
            return (
                <div
                    key={target}
                    hidden={this.formValues.searchTarget !== target}
                >
                    <Form.Item
                        {...this.getFormValidationValue(target)}
                    >
                        <Input
                            name={target}
                            onChange={this.onIdChange}
                            placeholder={target}
                        />
                    </Form.Item>
                </div>
            );
        }.bind(this));
    }

    @action
    onEntitySelect (entity: EntityTreeType, selectedEntities: EntityTreeType[]) {
        this.resetFormValidationValue('entityName');
        this.formValues.entityName = EntityStoreManager.getEntityName(entity);
    }

    @action
    onSourceSystemChange (sourceSystem: string) {
        this.resetFormValidationValue('sourceSystem');
        this.formValues.sourceSystem = sourceSystem;
    }

    onOpen () {
        this.setIsOpen(true);
    }

    onClose () {
        this.setIsOpen(false);
        this.formValidationValues = {};
    }

    @action
    setIsOpen (isOpen: boolean) {
        this.isOpen = isOpen;
    }

    @action
    doSearch () {
        let {
                etalonId,
                externalId,
                originId,
                entityName,
                sourceSystem,
                searchTarget
            } = {...this.formValues},
            promise;

        if (!searchTarget || !this.validate(searchTarget)) {
            return;
        }

        switch (searchTarget) {
            case SearchTarget.ETALON_ID:
                if (!etalonId) {
                    return;
                }

                promise = DataRecordIdSearchService.etalonIdSearch(etalonId);

                break;
            case SearchTarget.EXTERNAL_ID:
                this.validate('entityName');
                this.validate('sourceSystem');

                if (!externalId || !entityName || !sourceSystem) {
                    return;
                }

                promise = DataRecordIdSearchService.externalIdSearch(
                    externalId,
                    entityName,
                    sourceSystem
                );

                break;
            case SearchTarget.ORIGIN_ID:
                if (!originId) {
                    return;
                }

                promise = DataRecordIdSearchService.originIdSearch(originId);

                break;
        }

        if (promise) {
            promise.then(
                this.openRecord,
                this.notFound
            );
        }
    }

    @action
    openRecord (result: IdSearchResult) {
        // TODO: redo a redirect to a record after moving to react
        this.redirectTo = `/dataview?section=dataview|etalon?etalonId=${result.etalonId.getValue()}`;

        let searchTarget = this.formValues.searchTarget;

        if (searchTarget) {
            this.getFormValidationValue(searchTarget).help = i18n.t('app.hotkey>idSearch>validation>found');
            this.getFormValidationValue(searchTarget).validateStatus = 'success';
        }
    }

    @action
    notFound (error: Error) {
        let searchTarget = this.formValues.searchTarget;

        if (error instanceof OperationError &&
            error.type === OperationErrorEnum.FAILURE_FROM_SUCCESS &&
            searchTarget
        ) {
            this.getFormValidationValue(searchTarget).help = i18n.t(
                'app.hotkey>idSearch>validation>notFound',
                {
                    idType: searchTarget
                }
            );
            this.getFormValidationValue(searchTarget).validateStatus = 'warning';
        }
    }

    renderFooter () {
        return (
            <div className={styles.footer}>
                <Button
                    intent={INTENT.PRIMARY}
                    onClick={this.doSearch}
                >
                    {i18n.t('common:ok')}
                </Button>
                <Button
                    intent={INTENT.PRIMARY}
                    onClick={this.onClose}
                >
                    {i18n.t('common:cancel_noun')}
                </Button>
            </div>
        );
    }

    render () {
        if (!AppTypeManager.isDataSteward()) {
            return null;
        }

        return (
            <HotKeyModal
                header={i18n.t('app.hotkey>idSearch>title')}
                letterKey={'f'}
                ctrlKey={true}
                shiftKey={true}
                footer={this.renderFooter()}
                onClose={this.onClose}
                onOpen={this.onOpen}
                isOpen={this.isOpen}
            >
                {this.redirectTo ? <Redirect exact={true} to={this.redirectTo} /> : null}
                {this.isOpen ?
                    <Form>
                        <Form.Item
                            label={i18n.t('app.hotkey>idSearch>searchTarget')}
                            labelCol={{span: 4}}
                            labelAlign={'left'}
                        >
                            <Radio.Group
                                onChange={this.onSearchTargetChange}
                                value={this.formValues.searchTarget}
                            >
                                {this.renderRadio()}
                            </Radio.Group>
                        </Form.Item>

                        <Row>
                            <Col span={24}>
                                {this.renderInputs()}
                            </Col>
                        </Row>
                        <Row
                            hidden={this.formValues.searchTarget !== SearchTarget.EXTERNAL_ID}
                        >
                            <Col span={12}>
                                <Form.Item
                                    {...this.getFormValidationValue('entityName')}
                                >
                                    <EntitySelect
                                        onEntitySelect={this.onEntitySelect}

                                    />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...this.getFormValidationValue('sourceSystem')}
                                >
                                    <SourceSystemSelect
                                        onChange={this.onSourceSystemChange}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form> : null}
            </HotKeyModal>
        );
    }

}
