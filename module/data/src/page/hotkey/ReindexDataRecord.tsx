/**
 * Re-indexing records
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-19
 */
import * as React from 'react';
import {Dialog, i18n, UserManager} from '@unidata/core-app';
import {Button, HotKeyModal, INTENT, SIZE} from '@unidata/uikit';
import * as styles from './ReindexDataRecord.m.scss';
import {DataRecordService} from '../../service/DataRecordService';

interface IProps {
}

interface IState {
    isOpen?: boolean;
}

export class ReindexDataRecord extends React.Component<IProps, IState> {

    constructor (props: IProps) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.onClose = this.onClose.bind(this);
        this.onOpen = this.onOpen.bind(this);
        this.onReindexDone = this.onReindexDone.bind(this);
    }

    onOpen () {
        this.setState({isOpen: true});
    }

    onClose () {
        this.setState({isOpen: false});
    }

    runReindex (etalonId: string) {
        DataRecordService.reindexDataRecord(etalonId).then(this.onReindexDone);
    }

    onReindexDone (success: boolean) {
        if (success === true) {
            this.onClose();
            Dialog.showMessage(i18n.t('app.hotkey>reindexData>success'));
        } else {
            Dialog.showError(i18n.t('app.hotkey>reindexData>failure'));
        }
    }

    renderFooter (etalonId: string) {
        return (
            <div className={styles.footer}>
                <Button
                    intent={INTENT.PRIMARY}
                    onClick={this.runReindex.bind(this, etalonId)}
                >
                    {i18n.t('common:ok')}
                </Button>
                <Button
                    intent={INTENT.PRIMARY}
                    onClick={this.onClose}
                >
                    {i18n.t('common:cancel_noun')}
                </Button>
            </div>
        );
    }

    // TODO: redo to something more normal, after implementing the display of the record on React
    getEtalonId () {
        let match = window.location.hash.match(/etalonId=([a-zA-Z0-9-]*)/),
            etalonId;

        if (match && match[1]) {
            etalonId = match[1];
        }

        return etalonId;
    }

    render () {
        if (!UserManager.isUserAdmin()) {
            return null;
        }

        let etalonId = this.getEtalonId();

        if (!etalonId) {
            return null;
        }

        return (
            <React.Fragment>
                <HotKeyModal
                    header={i18n.t('app.hotkey>reindexData>title')}
                    letterKey={'i'}
                    ctrlKey={true}
                    altKey={true}
                    onClose={this.onClose}
                    onOpen={this.onOpen}
                    isOpen={this.state.isOpen}
                    footer={this.renderFooter(etalonId)}
                    size={SIZE.SMALL}
                >
                    {i18n.t('app.hotkey>reindexData>confirm', {etalonId: etalonId})}
                </HotKeyModal>
            </React.Fragment>
        );
    }

}
