/**
 * Displaying a search Term of the date type
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';

import {PLACEMENT, TRIGGER} from '@unidata/uikit';
import * as inputStyles from '../../search_term.m.scss';

import {CommonTermView} from '../CommonTermView';
import {DateInput} from '../date_input/DateInput';
import {observer} from 'mobx-react';
import {AttributeValueRange} from '../attribute/AttributeValueRange';
import {AsOfST, CreateUpdateST, TimestampFormField} from '@unidata/meta';

import {Dropdown} from 'antd';

type IProps = {
    onDelete: () => void;
    searchTerm: AsOfST | CreateUpdateST;
};

interface IState {
    isVisible: boolean;
}

@observer
export class DateRange extends React.Component<IProps, IState> {
    state = {
        isVisible: false
    };

    onVisibleChange = (isVisible: boolean) => {
        this.setState({isVisible});
    };

    onChange = (value: [string | null, string | null]) => {
        const item = this.props.searchTerm.getFormField();

        this.props.searchTerm.changeItem({
            ...item,
            range: value
        });
    };

    render () {
        const searchTerm = this.props.searchTerm;
        const valueText = AttributeValueRange.valueToView(searchTerm);
        const formField = searchTerm.getFormField();

        return (
            <Dropdown
                visible={this.state.isVisible}
                overlay={(
                    <div className={inputStyles.inputContainer} data-qaid={'modal_' + searchTerm.key}>
                        <DateInput
                            isVisible={this.state.isVisible}
                            value={formField.getValue()}
                            showTime={formField instanceof TimestampFormField}
                            onChange={this.onChange}
                            onOpenChange={this.onVisibleChange}
                        />
                    </div>
                )}
                trigger={[TRIGGER.CLICK]}
                placement={'bottomLeft'}
                onVisibleChange={this.onVisibleChange}
            >
                <div data-qaid={searchTerm.key}>
                    <CommonTermView
                        label={searchTerm.displayName}
                        value={valueText}
                        onDelete={this.props.onDelete}
                        isVisibleDropdown={this.state.isVisible}
                    />
                </div>
            </Dropdown>
        );
    }
}
