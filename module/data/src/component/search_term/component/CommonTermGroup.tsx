/**
 * General display of the searchTerm group. Responsible for the separator in the display
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';

import * as styles from '../search_term.m.scss';
import {AbstractSearchTerm} from '@unidata/meta';

interface IProps {
    searchTerms: AbstractSearchTerm[];
    hasPrev: boolean;
}

export class CommonTermGroup extends React.Component<IProps, any> {
    render () {
        return (
            <React.Fragment>
                {this.props.hasPrev === true && this.props.searchTerms.length > 0 && (
                    <div className={styles.groupSep}/>
                )}
                {this.props.children}
            </React.Fragment>
        );
    }
}
