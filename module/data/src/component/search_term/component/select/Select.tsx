/**
 * Displaying Search Term of the Select type
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {PLACEMENT, TRIGGER, IOption, IGroupOption, DropDown} from '@unidata/uikit';
import {CommonTermView} from '../CommonTermView';
import * as styles from '../../search_term.m.scss';
import {i18n} from '@unidata/core-app';
import {observer} from 'mobx-react';
import {SelectST} from '@unidata/meta';

type IProps = {
    onDelete: () => void;
    searchTerm: SelectST;
};

interface IState {
    isVisible: boolean;
}

@observer
export class Select extends React.Component<IProps, IState> {
    state = {
        isVisible: false
    };

    onChange = (value: string | number, type: boolean) => () => {
        const searchTerm = this.props.searchTerm;
        const values = searchTerm.value;
        const selectedValue = searchTerm.getOptions().find((item) => item.value === value);

        if (selectedValue !== undefined) {
            if (this.props.searchTerm.isMulti === true) {
                if (type === true) {
                    searchTerm.setValue(values.concat(selectedValue.value.toString()));
                } else {
                    const valueIndex = values.findIndex((valueItem) => valueItem === value);

                    if (valueIndex > -1) {
                        searchTerm.setValue([
                            ...values.slice(0, valueIndex),
                            ...values.slice(valueIndex + 1)
                        ]);
                    }
                }
            } else {
                searchTerm.setValue([selectedValue.value.toString()]);
            }
        }
    };

    onVisibleChange = (isVisible: boolean) => {
        this.setState({isVisible});
    };

    renderOptions (selectOptions: IOption[]) {
        const searchTerm = this.props.searchTerm;
        const values = searchTerm.value.map((valItem) => valItem);

        return selectOptions.map((item) => {
            const isCheck = values.includes(item.value.toString());

            return (
                <DropDown.Item
                    key={item.value}
                    data-qaid={'item_' + item.value}
                    onClick={!isCheck || searchTerm.isMulti === true ? this.onChange(item.value, !isCheck) : undefined}
                    isNoHover={isCheck && !searchTerm.isMulti}
                    // Empty div, so that the container under the icon is always visible and there is no change in the width of the drop-down list
                    rightIcon={isCheck ? 'check' : <div/>}
                >
                    {item.title}
                </DropDown.Item>
            );
        });

    }

    render () {
        const searchTerm = this.props.searchTerm;
        const excludeValues: React.ReactText[] = [];
        const values = searchTerm.value;

        const valueList = searchTerm.getOptions().reduce((result: string[], item: IGroupOption) => {
            const itemGroup = item as IGroupOption;

            if (itemGroup.options && itemGroup.options.length > 0) {
                itemGroup.options.forEach((optionItem) => {
                    if (values.includes(optionItem.value.toString()) && !excludeValues.includes(optionItem.value)) {
                        result.push(optionItem.title);
                        excludeValues.push(optionItem.value);
                    }
                });
            } else if (values.includes(item.value.toString()) && !excludeValues.includes(item.value)) {
                result.push(item.title);
                excludeValues.push(item.value);
            }

            return result;
        }, []);

        const valueText = valueList.length === 0 ? (
            <span className={styles.helper}>{i18n.t('module.data>search.panel>attributesMeta>notSelected')}</span>
        ) : <span className={styles.value}>{valueList.join(', ')}</span>;

        return (
            <DropDown
                isOpen={this.state.isVisible}
                onVisibleChange={this.onVisibleChange}
                target={(
                    <div  data-qaid={searchTerm.key}>
                        <CommonTermView
                            label={searchTerm.displayName}
                            value={valueText}
                            onDelete={this.props.onDelete}
                            isVisibleDropdown={this.state.isVisible}
                            counter={valueList.length}
                        />
                    </div>
                )}
                trigger={TRIGGER.CLICK}
                placement={PLACEMENT.BOTTOM_START}
                data-qaid={searchTerm.key + '_dropdown'}
            >
                {
                    searchTerm.getOptions().map((item) => {
                        if ((item as IGroupOption).options) {
                            const itemGroup = item as IGroupOption;

                            if (itemGroup.options.length === 0) {
                                return null;
                            }

                            return (
                                <React.Fragment key={item.value}>
                                    <DropDown.Header>
                                        {itemGroup.title}
                                    </DropDown.Header>
                                    {this.renderOptions(itemGroup.options)}
                                </React.Fragment>
                            );
                        }

                        return this.renderOptions([item as IOption]);
                    })
                }
            </DropDown>
        );
    }
}
