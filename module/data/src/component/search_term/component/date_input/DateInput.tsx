/**
 * Displaying DateInput for SearchTerm of the date type or attribute type (date)
 *
 * @author Brauer Ilya
 * @date 2020-06-01
 */

import * as React from 'react';
import * as moment from 'moment';
import {Moment} from 'moment';

import {DateRangeInput} from '@unidata/uikit';

import {dateFormatToRequest, dateTimeFormatToRequest} from '../../../../page/search/store/search_panel_store/SearchPanelStore';

import {i18n, UserLocale} from '@unidata/core-app';

type IProps = {
    value?: string | [string | null, string | null] | null;
    onChange: (value: [string | null, string | null]) => void;
    showTime?: boolean;
    isVisible?: boolean;
    onOpenChange?: (isOpen: boolean) => void;
    containerRef?: React.RefObject<HTMLElement>; // todo Brauer Ilya: temporary param, remove it when the DatePicker will be in our popover
};

interface IState {
    isVisible: boolean;
}

export class DateInput extends React.Component<IProps, IState> {
    state = {
        isVisible: this.props.isVisible === true ? true : false
    };

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        if (this.props.isVisible !== undefined && this.props.isVisible !== this.state.isVisible) {
            this.setState({isVisible: this.props.isVisible});
        }
    }

    get placeholder (): [string, string] {
        return [
            i18n.t('module.data>search.panel>rangeStart'),
            i18n.t('module.data>search.panel>rangeEnd')
        ];
    }

    get rangeTemplates (): {[key: string]: any} {
        return {
            [i18n.t('module.data>search.panel>today')]: [moment(), moment()],
            [i18n.t('module.data>search.panel>currentMonth')]: [moment().startOf('month'), moment().endOf('month')],
            [i18n.t('module.data>search.panel>currentYear')]: [moment().startOf('year'), moment().endOf('year')]
        };
    }

    onOpenChange = (isVisible: boolean) => {
        this.setState({isVisible});

        if (this.props.onOpenChange) {
            this.props.onOpenChange(isVisible);
        }
    };

    formatValue = (value: string | Moment | undefined | null) => {
        if (value === undefined || value === null) {
            return null;
        }

        const format = this.props.showTime ? dateTimeFormatToRequest : dateFormatToRequest;

        return moment(value).format(format);
    };

    onChange = (value: any) => {
        this.props.onChange([this.formatValue(value[0]), this.formatValue(value[1])]);

        if (this.props.showTime !== true) {
            this.onOpenChange(false);
        }
    };

    onCalendarChange = (value: any) => {
        // todo Brauer Ilya: 'value as any' because DateRange types bad
        // we always return range. if only one date is selected, the same values are used
        const searchTermValue = (value.length === 1 ? [value[0], value[0]] : (value as any)).map((valItem?: Moment) => {
            return (valItem) ? this.formatValue(valItem) : null;
        });

        this.props.onChange(searchTermValue);
    };

    getContainer = () => {
        if (!this.props.containerRef) {
            return document.body;
        }

        return this.props.containerRef.current || document.body;
    };

    render () {
        const value = this.props.value;

        let ranges: any = [undefined, undefined];

        if (Array.isArray(value) || typeof value === 'string') {
            ranges = typeof value === 'string' ?
                [moment(value, dateTimeFormatToRequest), moment(value, dateTimeFormatToRequest)] :
                [
                    value[0] === null ? null : moment(value[0], dateTimeFormatToRequest),
                    value[1] === null ? null : moment(value[1], dateTimeFormatToRequest)
                ];
        }

        return (
            <DateRangeInput
                open={this.state.isVisible}
                onOpenChange={this.onOpenChange}
                value={ranges}
                onChange={this.onChange}
                onCalendarChange={this.onCalendarChange}
                format={this.props.showTime ? UserLocale.getDateTimeFormat() : UserLocale.getDateFormat()}
                placeholder={this.placeholder}
                ranges={this.rangeTemplates}
                showTime={this.props.showTime}
                getPopupContainer={this.getContainer}
            />
        );
    }
}
