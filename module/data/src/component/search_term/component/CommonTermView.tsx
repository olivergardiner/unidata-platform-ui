/**
 * Displaying a single SearchTerm
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {Icon, PLACEMENT, Tooltip, TRIGGER} from '@unidata/uikit';
import * as styles from '../search_term.m.scss';

import {i18n} from '@unidata/core-app';
import {SEARCH_TYPE} from '@unidata/meta';

interface IProps {
    label?: string;
    tooltipOverlay?: React.ReactNode;
    value: React.ReactNode;
    onDelete: () => void;
    isVisibleDropdown: boolean; // whether or not we see the dropdown for entering the search value
    counter?: number; // Counter of selected values in the select (perhaps in attributes in the future?)
}

interface IState {
    hasCounter: boolean;
}

type TermPartProps = {
    helper?: React.ReactNode;
    value?: React.ReactNode;
}

export const RenderTermPart: React.FC<TermPartProps> = (props) => {
    return (
        <>
            {props.helper && (<span className={styles.helper}>{props.helper}</span>)}
            {props.helper && props.value && (<>&nbsp;</>)}
            {props.value && (<span className={styles.value}>{props.value}</span>)}
        </>
    );
};

export function getConcatView (index: number, concatType?: 'OR' | 'AND'): React.ReactNode {
    if (index > 0 && concatType !== undefined) {
        const helperText = i18n.t(`search.panel>attributesMeta>${concatType.toLowerCase()}`);

        return (
            <RenderTermPart
                key={`concat_${index}`}
                helper={` ${helperText} `}
            />
        );
    }

    return '';
}

export function getInvertedView (inverted?: 'true' | 'false'): React.ReactNode {
    return inverted === 'true' ? ` ${i18n.t('module.data>search.panel>attributesMeta>not')} ` : '';
}

export function getFullView (index: number, valueNode: React.ReactNode, searchType: SEARCH_TYPE): React.ReactNode {

    switch (searchType) {
        case SEARCH_TYPE.EXIST: {
            return (
                <RenderTermPart
                    key={`value_${index}`}
                    helper={` ${i18n.t('module.data>search.panel>attributesMeta>exist')}`}
                />
            );
        }
        case SEARCH_TYPE.FUZZY: {
            return (
                <RenderTermPart
                    key={`value_${index}`}
                    helper={` ${i18n.t('module.data>search.panel>attributesMeta>fuzzy')}`}
                    value={valueNode}
                />
            );
        }
        case SEARCH_TYPE.LIKE: {
            return (
                <RenderTermPart
                    key={`value_${index}`}
                    helper={` ${i18n.t('module.data>search.panel>attributesMeta>like')}`}
                    value={valueNode}
                />
            );
        }
        case SEARCH_TYPE.START_WITH: {
            return (
                <RenderTermPart
                    key={`value_${index}`}
                    helper={` ${i18n.t('module.data>search.panel>attributesMeta>startWith')}`}
                    value={valueNode}
                />
            );
        }
        default: {
            return (
                <React.Fragment key={`value_${index}`}>
                    {valueNode}
                </React.Fragment>
            );
        }
    }

}

export class CommonTermView extends React.PureComponent<IProps, IState> {

    valueElement = React.createRef<HTMLDivElement>();

    state = {
        hasCounter: false
    };

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        if (this.valueElement.current !== null && this.props.counter !== undefined) {
            const valueCurrentWidth = this.valueElement.current.offsetWidth;
            // See search_term.m.css for container value "width: calc(100vw - 740px)"
            const maxValueWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) - 740;

            if (valueCurrentWidth === maxValueWidth && prevState.hasCounter === false) {
                this.setState({hasCounter: true});
            } else if (valueCurrentWidth !== maxValueWidth && this.state.hasCounter === true) {
                this.setState({hasCounter: false});
            }
        }
    }

    render () {
        const props = this.props;

        const tooltipProps: any = {
            overlay: props.tooltipOverlay ? props.tooltipOverlay : (
                <React.Fragment>
                    <div>{props.label}</div>
                    <div>
                        {props.value}
                    </div>
                </React.Fragment>
            ),
            trigger: TRIGGER.HOVER_TARGET_ONLY,
            placement: PLACEMENT.RIGHT,
            mouseEnterDelay: 400
        };

        const termItemClass = [styles.tagItem];

        if (props.isVisibleDropdown === true) {
            tooltipProps.visible = false;
            termItemClass.push(styles.tagItemChecked);
        }

        return (
            <Tooltip
                {...tooltipProps}
            >
                <div className={termItemClass.join(' ')}>
                    {props.label !== undefined && (
                        <React.Fragment>
                            <div className={styles.tagItemLabel}>
                                {props.label}
                            </div>
                            <div>:&nbsp;</div>
                        </React.Fragment>
                    )}
                    <div ref={this.valueElement} className={styles.tagItemValue}>
                        {props.value}
                    </div>
                    {this.state.hasCounter && (
                        <div className={styles.counter}>
                            {props.counter}
                        </div>
                    )}
                    <div className={styles.close}>
                        <Icon name={'close'} onClick={props.onDelete}/>
                    </div>
                </div>
            </Tooltip>
        );
    }
}
