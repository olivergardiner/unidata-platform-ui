/**
 * Displaying GroupItem type SearchTerm (e.g. Relation or Classifier) - with nested searchTerm
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {IComponentPropsByUEType, UeModuleTypeComponent} from '@unidata/core';
import {
    joinClassNames
} from '@unidata/uikit';
import * as styles from '../../search_term.m.scss';
import {AbstractSearchTerm} from '@unidata/meta';

type IProps = IComponentPropsByUEType[UeModuleTypeComponent.RENDER_SEARCH_TERM] & {
    isVisible?: boolean;
};

interface IState {
    isVisible: boolean;
}

export class GroupItem extends React.Component<IProps, IState> {
    state: IState = {
        isVisible: false
    };

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        if (this.state.isVisible === true && this.props.isVisible === false) {
            this.setState({isVisible: false});
        }
    }

    toggleVisible = () => {
        this.setState((prevState) => {
            return {
                isVisible: !prevState.isVisible
            };
        });
    };

    render () {
        const searchTerm = this.props.searchTerm;

        const groupTerms = this.props.searchTermList.filter((term: AbstractSearchTerm) => {
            return term.parentId === searchTerm.parentId;
        });

        const classNames = joinClassNames(
            styles.relationTermItem,
            styles.before,
            [styles.after, searchTerm.id !== groupTerms[groupTerms.length - 1].id]
        );

        return (
            <div className={classNames}>
                {this.props.children}
            </div>
        );
    }
}
