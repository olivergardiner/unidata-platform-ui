/**
 * Displaying SearchTerm of the Group type (e.g. Relation or Classifier) - with nested searchTerm
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {IComponentPropsByUEType, UeModuleTypeComponent} from '@unidata/core';
import {CommonTermView} from '../CommonTermView';
import {SearchTerm as UdSearchTerm} from '../../SearchTerm';
import {
    DropDown,
    PLACEMENT,
    TRIGGER
} from '@unidata/uikit';
import * as styles from '../../search_term.m.scss';
import {AbstractAttributeST, AbstractSearchTerm} from '@unidata/meta';
import {GroupItem} from './GroupItem';

interface ITermViewProps {
    value: React.ReactNode;
    label?: string;
    tooltipOverlay?: React.ReactNode;
}

type IProps = IComponentPropsByUEType[UeModuleTypeComponent.RENDER_SEARCH_TERM] & {
    termProps?: ITermViewProps;
    isVisible?: boolean;
};

interface IState {
    isVisible: boolean;
}

export class Group extends React.Component<IProps, IState> {

    state: IState = {
        isVisible: false
    };

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        if (this.state.isVisible === true && this.props.isVisible === false) {
            this.setState({isVisible: false});
        }
    }

    onDeleteGroup = () => {
        this.props.onDelete();
    };

    renderMainInGroup = () => {
        const searchTerm = this.props.searchTerm;

        const groupTerms = this.props.searchTermList.filter((term: AbstractSearchTerm) => {
            return term.parentId === searchTerm.id;
        });

        const termProps: ITermViewProps = this.props.termProps !== undefined ?
            {...this.props.termProps} :
            {value: searchTerm.displayName};

        const overlay = React.Children.count(this.props.children) === 0 ? null : (
            <div data-qaid={searchTerm.key + '_dropdown'} className={styles.ddGroup}>
                {this.props.children}
            </div>
        );

        return (
            <DropDown
                isOpen={this.state.isVisible}
                onVisibleChange={(visible) => this.setState({isVisible: visible})}
                trigger={TRIGGER.CLICK}
                placement={PLACEMENT.BOTTOM_START}
                target={(
                    <div
                        className={`${styles.relationTermItem} ${groupTerms.length >= 1 ? styles.after : ''}`}
                        data-qaid={searchTerm.key}
                    >
                        <CommonTermView
                            {...termProps}
                            onDelete={this.onDeleteGroup}
                            isVisibleDropdown={this.state.isVisible}
                        />
                    </div>
                )}
            >
                {overlay}
            </DropDown>
        );
    };

    render () {
        const searchTerm = this.props.searchTerm;

        if (searchTerm instanceof AbstractAttributeST) {
            return (
                <GroupItem
                    searchTerm={searchTerm}
                    searchTermList={this.props.searchTermList}
                    onDelete={this.props.onDelete}
                >
                    <UdSearchTerm
                        searchTerm={searchTerm}
                        onDelete={this.props.onDelete}
                    />
                </GroupItem>
            );
        }

        return this.renderMainInGroup();
    }
}
