declare const styles: {
  readonly "inputContainer": string;
  readonly "blockContainer": string;
  readonly "blockContainerLast": string;
  readonly "concatContainer": string;
  readonly "sepV": string;
  readonly "sepH": string;
  readonly "invertSelect": string;
  readonly "removeButtonContainer": string;
  readonly "selectType": string;
  readonly "dateAttribute": string;
  readonly "selectInverted": string;
  readonly "valueContainer": string;
  readonly "header": string;
  readonly "plusIconContainer": string;
  readonly "numberRange": string;
  readonly "withMeasurement": string;
  readonly "measurementUnit": string;
  readonly "warning": string;
  readonly "errorContainer": string;
};
export = styles;

