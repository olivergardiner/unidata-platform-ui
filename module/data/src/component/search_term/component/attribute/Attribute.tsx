/**
 * Displaying The searchterm Attribute type (multiple conditions can be set)
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {DropDown, PLACEMENT, POPOVER_BOUNDARIES, TRIGGER} from '@unidata/uikit';
import {SIMPLE_DATA_TYPE} from '@unidata/types';
import {CommonTermView} from '../CommonTermView';
import {Favorite} from './Favorite';
import {observer} from 'mobx-react';

import {AbstractAttributeST, AbstractFormField, systemVariables} from '@unidata/meta';
import {observable, runInAction} from 'mobx';
import {AttributeDropdownContent} from './AttributeDropdownContent';
import {AttributeValueView} from './AttributeValueView';

type IProps<T extends AbstractFormField, S extends SIMPLE_DATA_TYPE> = {
    onDelete: () => void;
    searchTerm: AbstractAttributeST<T, S>;
};

@observer
export class Attribute<T extends AbstractFormField, S extends SIMPLE_DATA_TYPE> extends React.Component<IProps<T, S>, {}> {

    @observable
    isVisible: boolean = false;

    componentWillUnmount (): void {
        document.removeEventListener('keypress', this.onKeyPress);
    }

    onVisibleChange = (isVisible: boolean) => {
        if (isVisible === true) {
            document.addEventListener('keypress', this.onKeyPress);
        } else {
            document.removeEventListener('keypress', this.onKeyPress);
        }

        runInAction(() => this.isVisible = isVisible);
    };


    onKeyPress = (e: KeyboardEvent) => {
        if (e.key === 'Enter') {
            this.onVisibleChange(false);
        }
    };

    render () {
        const searchTerm = this.props.searchTerm;

        if (searchTerm.key === systemVariables.$etalonId) {
            return (
                <Favorite
                    searchTerm={searchTerm}
                    onDelete={this.props.onDelete}
                />
            );
        }

        return (
            <DropDown
                isOpen={this.isVisible}
                trigger={TRIGGER.CLICK}
                placement={PLACEMENT.BOTTOM_START}
                onVisibleChange={this.onVisibleChange}
                target={(
                    <div data-qaid={searchTerm.key}>
                        <CommonTermView
                            label={searchTerm.displayName}
                            value={(
                                <AttributeValueView searchTerm={searchTerm}/>
                            )}
                            onDelete={this.props.onDelete}
                            isVisibleDropdown={this.isVisible}
                        />
                    </div>
                )}
            >
                <AttributeDropdownContent
                    onDelete={this.props.onDelete}
                    searchTerm={searchTerm}
                    isVisible={this.isVisible}
                />
            </DropDown>
        );
    }
}
