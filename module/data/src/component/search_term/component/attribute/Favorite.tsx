/**
 * Displaying Search Term of the Favorite type
 *
 * @author Brauer Ilya
 * @date 2020-03-05
 */

import * as React from 'react';
import {ITermItem} from '../../SearchTerm';
import {CommonTermView} from '../CommonTermView';
import {i18n} from '@unidata/core-app';
import {observer} from 'mobx-react';

type IProps = ITermItem;

@observer
export class Favorite extends React.Component<IProps> {

    render () {
        const searchTerm = this.props.searchTerm;

        return (
            <div data-qaid={searchTerm.key}>
                <CommonTermView
                    label={searchTerm.displayName}
                    value={i18n.t('module.data>search.panel>attributesMeta>yes').toUpperCase()}
                    onDelete={this.props.onDelete}
                    isVisibleDropdown={false}
                />
            </div>
        );
    }
}
