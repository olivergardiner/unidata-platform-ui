/**
 * Display SearchTerm Attribute type input Field (number only)
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {Icon, NumberInput} from '@unidata/uikit';
import * as styles from './attribute.m.css';
import {i18n, UserLocale} from '@unidata/core-app';
import {
    IAbstractFormField, IntegerFormField, NumberAttributeST, NumberFormField,
    SEARCH_TYPE
} from '@unidata/meta';
import {getConcatView, getFullView, getInvertedView, RenderTermPart} from '../CommonTermView';

interface IProps {
    value?: number | [number | null, number | null] | null;
    onChange: (key: keyof IAbstractFormField | 'value' | 'range') => (value: number | [number | null, number | null]) => void;
    searchType: SEARCH_TYPE;
    decimalSeparator?: string;
    isInteger?: boolean;
}

export class AttributeValueNumber extends React.Component<IProps> {
    static typeOptions = () => [
        {
            title: i18n.t('module.data>search.panel>attributesMeta>exist'),
            value: SEARCH_TYPE.EXIST
        },
        {
            title: i18n.t('module.data>search.panel>attributesMeta>value'),
            value: SEARCH_TYPE.EXACT
        },
        {
            title: i18n.t('module.data>search.panel>attributesMeta>range'),
            value: SEARCH_TYPE.RANGE
        }
    ];

    static valueToView = (searchTerm: NumberAttributeST) => {

        const nodes: React.ReactNode[] = [];

        searchTerm.itemsData.forEach((item: NumberFormField | IntegerFormField, index: number) => {
            nodes.push(getConcatView(index, item.concatType));
            nodes.push(getInvertedView(item.inverted));

            const value = item.getValue();

            let valueNode: React.ReactNode = (<RenderTermPart helper={i18n.t('module.data>search.panel>attributesMeta>notSelected')}/>);

            if (value !== null && value !== undefined) {
                const getValue = (value: string | number | null) => {
                    const stringVal = (value !== null ? value : '').toString();

                    return item instanceof NumberFormField ?
                        stringVal.replace('.', UserLocale.getDecimalSeparator()) :
                        stringVal;
                };

                if (typeof value === 'number') {
                    valueNode = (
                        <RenderTermPart
                            value={getValue(value)}
                        />
                    );
                } else {
                    valueNode = (
                        <>
                            <RenderTermPart
                                helper={i18n.t('module.data>search.panel>attributesMeta>fromNumber').toLowerCase()}
                                value={getValue(Array.isArray(value) ? value[0] : null)}
                            />
                            &nbsp;
                            <RenderTermPart
                                helper={i18n.t('module.data>search.panel>attributesMeta>toNumber').toLowerCase()}
                                value={getValue(Array.isArray(value) ? value[1] : null)}
                            />
                        </>
                    );
                }
            }

            nodes.push(getFullView(index, valueNode, item.searchType));
        });

        return nodes;
    };

    onChange = (value: number) => {
        this.props.onChange('value')(value);
    };

    onChangeRange = (index: number) => (value?: number) => {
        const prevValue = this.props.value || null;
        const val = value || null;

        if (Array.isArray(prevValue)) {
            if (index === 0) {
                this.props.onChange('range')([val, prevValue[1]]);
            } else {
                this.props.onChange('range')([prevValue[0], val]);
            }
        } else {
            if (index === 0) {
                this.props.onChange('range')([val, prevValue]);
            } else {
                this.props.onChange('range')([prevValue, val]);
            }
        }
    };

    isRangeWrong (): boolean {
        let border = this.props.value || [];

        if (Array.isArray(border) && border[0] && border[1]) {
            return border[1] < border[0];
        } else {
            return false;
        }
    }

    render () {
        if (this.props.searchType === SEARCH_TYPE.RANGE) {
            let value = this.props.value,
                hasError = this.isRangeWrong();

            if (!Array.isArray(value)) {
                if (value === null || value === undefined) {
                    value = [null, null];
                } else {
                    value = [value, null];
                }
            }

            return (
                <div>
                    <div className={styles.numberRange}>
                        {i18n.t('module.data>search.panel>attributesMeta>fromNumber')}
                        <NumberInput
                            value={value[0] || undefined}
                            onChange={this.onChangeRange(0)}
                            data-qaid='fromInputNumber'
                            decimalSeparator={this.props.decimalSeparator}
                            isInteger={this.props.isInteger}
                            hasError={hasError}
                        />
                        {i18n.t('module.data>search.panel>attributesMeta>toNumber')}
                        <NumberInput
                            value={value[1] || undefined}
                            onChange={this.onChangeRange(1)}
                            data-qaid='toInputNumber'
                            decimalSeparator={this.props.decimalSeparator}
                            isInteger={this.props.isInteger}
                            hasError={hasError}
                        />
                    </div>
                    {hasError ? <div className={styles.errorContainer}>
                        <Icon name='warning'/>
                        <div>{i18n.t('validation>wrongRangeEnd')}</div>
                    </div> : ''}
                </div>
            );
        } else if (this.props.searchType === SEARCH_TYPE.EXACT) {
            const value = this.props.value === null || this.props.value === undefined ? undefined : this.props.value;

            if (typeof value !== 'number' && value !== undefined) {
                console.error('You try to use AttributeValueNumber with SEARCH_TYPE.EXACT and array in value');

                return null;
            }

            return (
                <NumberInput
                    value={value}
                    onChange={this.onChange}
                    decimalSeparator={this.props.decimalSeparator}
                    isInteger={this.props.isInteger}
                    style={{width: '100%'}}
                />
            );
        } else {
            return null;
        }
    }
}
