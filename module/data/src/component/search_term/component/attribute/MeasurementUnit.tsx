import * as React from 'react';
import * as styles from './attribute.m.css';
import {Input, PLACEMENT, Tooltip, TRIGGER} from '@unidata/uikit';

interface IProps {
    measurement: {
        groupName: string;
        displayName: string;
        shortName: string;
    };
}

export class MeasurementUnit extends React.Component<IProps, {}> {

    render () {
        const measurement = this.props.measurement;

        const tooltipProps: any = {
            overlay: (
                <React.Fragment>
                    <span>{measurement.groupName}: </span>
                    <span>
                        {measurement.displayName} ({measurement.shortName})
                    </span>
                </React.Fragment>
            ),
            trigger: TRIGGER.HOVER,
            placement: PLACEMENT.RIGHT,
            mouseEnterDelay: 600
        };

        return (
            <div className={styles.measurementUnit}>
                <Tooltip
                    {...tooltipProps}
                >
                    <Input disabled={true} value={measurement.shortName}/>
                </Tooltip>
            </div>
        );
    }
}
