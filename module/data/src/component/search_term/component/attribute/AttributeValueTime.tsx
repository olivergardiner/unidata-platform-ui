/**
 * Display the Search Term Attribute of type input Field (time)
 *
 * @author Brauer Ilya
 * @date 2020-05-20
 */

import * as React from 'react';
import {Icon, Input} from '@unidata/uikit';
import {DateTimeUtil, i18n} from '@unidata/core-app';
import * as styles from './attribute.m.css';
import {
    IAbstractFormField,
    SEARCH_TYPE, TimeAttributeST, TimeFormField
} from '@unidata/meta';
import {getConcatView, getFullView, getInvertedView, RenderTermPart} from '../CommonTermView';

interface IProps {
    value?: string | [string|null, string|null] | null;
    onChange: (key: keyof IAbstractFormField | 'value' | 'range') => (value: string | [string|null, string|null]) => void;
    searchType: SEARCH_TYPE;
    isVisible: boolean;
}

const inputTimeMask = '__:__:__';

export class AttributeValueTime extends React.Component<IProps> {

    static typeOptions = () => [
        {
            title: i18n.t('module.data>search.panel>attributesMeta>exist'),
            value: SEARCH_TYPE.EXIST
        },
        {
            title: i18n.t('module.data>search.panel>attributesMeta>value'),
            value: SEARCH_TYPE.EXACT
        },
        {
            title: i18n.t('module.data>search.panel>attributesMeta>range'),
            value: SEARCH_TYPE.RANGE
        }
    ];

    static valueToView = (searchTerm: TimeAttributeST) => {
        const nodes: React.ReactNode[] = [];

        searchTerm.itemsData.forEach((item: TimeFormField, index: number) => {
            nodes.push(getConcatView(index, item.concatType));
            nodes.push(getInvertedView(item.inverted));

            const value = item.getValue();

            let valueNode: React.ReactNode = (<RenderTermPart helper={i18n.t('module.data>search.panel>attributesMeta>notSelected')}/>);

            if (value !== null && value !== undefined) {
                if (typeof value === 'string') {
                    valueNode = (
                        <RenderTermPart
                            value={value}
                        />
                    );
                } else {
                    valueNode = (
                        <>
                            <RenderTermPart
                                helper={i18n.t('module.data>search.panel>attributesMeta>fromNumber').toLowerCase()}
                                value={Array.isArray(value) ? value[0] : null}
                            />
                            &nbsp;
                            <RenderTermPart
                                helper={i18n.t('module.data>search.panel>attributesMeta>toNumber').toLowerCase()}
                                value={Array.isArray(value) ? value[1] : null}
                            />
                        </>
                    );
                }
            }

            nodes.push(getFullView(index, valueNode, item.searchType));
        });

        return nodes;
    };

    getValue = (e: React.SyntheticEvent<HTMLInputElement>): string => {
        return e.currentTarget.value === inputTimeMask ? '' : e.currentTarget.value;
    };

    onChange = (event: React.SyntheticEvent<HTMLInputElement>) => {
        const value = this.getValue(event);

        this.props.onChange('value')(value);
    };

    onChangeRange = (index: number) => (event: React.SyntheticEvent<HTMLInputElement>) => {
        const prevValue = this.props.value || null;
        const value = this.getValue(event);
        const propsChange = this.props.onChange;

        if (Array.isArray(prevValue)) {
            if (index === 0) {
                propsChange('range')([value, prevValue[1]]);
            } else {
                propsChange('range')([prevValue[0], value]);
            }
        } else {
            if (index === 0) {
                propsChange('range')([value, prevValue]);
            } else {
                propsChange('range')([prevValue, value]);
            }
        }
    };

    isRangeWrong (): boolean {
        const border = this.props.value || [];

        if (typeof border[0] !== 'string' || typeof border[1] !== 'string') {
            return false;
        }

        return DateTimeUtil.isValidTime(border[0]) &&
            DateTimeUtil.isValidTime(border[1]) && DateTimeUtil.isTimeAfter(border[0], border[1]);
    }

    render () {
        if (this.props.searchType === SEARCH_TYPE.EXIST) {
            return null;
        } else if (this.props.searchType === SEARCH_TYPE.RANGE) {
            let value: any = this.props.value,
                hasError: boolean = this.isRangeWrong();

            if (!Array.isArray(value)) {
                if (value === '') {
                    value = ['', ''];
                } else {
                    value = [value, ''];
                }
            }

            return (
                <>
                    <div className={styles.numberRange}>
                        {i18n.t('module.data>search.panel>attributesMeta>fromNumber')}
                        <Input.Time
                            value={value[0] as string}
                            onChange={this.onChangeRange(0)}
                            hasError={hasError}
                        />
                        {i18n.t('module.data>search.panel>attributesMeta>toNumber')}
                        <Input.Time
                            value={value[1] as string}
                            onChange={this.onChangeRange(1)}
                            hasError={hasError}
                        />
                    </div>
                    {hasError ? <div className={styles.errorContainer}>
                        <Icon name='warning' />
                        <div>{i18n.t('validation>beforeTime')}</div>
                    </div> : ''}
                </>
            );
        } else if (this.props.searchType === SEARCH_TYPE.EXACT) {
            const value = this.props.value;

            return (
                <Input.Time
                    value={value as string}
                    onChange={this.onChange}
                />
            );
        } else {
            return null;
        }
    }
}
