/**
 * Displaying A searchterm attribute of the select type (for searching by enum)
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {IOption, Select, Value} from '@unidata/uikit';
import {i18n, UserLocale} from '@unidata/core-app';
import {
    AbstractFormField, BooleanAttributeST, BooleanFormField, DateFormField, EnumAttributeST, EnumFormField,
    IAbstractFormField, RefAttributeST, RefFormField,
    SEARCH_TYPE, TimestampFormField
} from '@unidata/meta';
import {getConcatView, getFullView, getInvertedView, RenderTermPart} from '../CommonTermView';
import moment from 'moment';

interface IProps {
    value: IOption;
    onChange: (key: keyof IAbstractFormField | 'value') => (value: string | number | null) => void;
    searchType: SEARCH_TYPE;
    isVisible: boolean;
    options: IOption[];
    containerRef: React.RefObject<HTMLElement>;
}

export class AttributeValueSelect extends React.Component<IProps> {
    static typeOptions = () => [
        {
            title: i18n.t('module.data>search.panel>attributesMeta>exist'),
            value: SEARCH_TYPE.EXIST
        },
        {
            title: i18n.t('module.data>search.panel>attributesMeta>value'),
            value: SEARCH_TYPE.EXACT
        }
    ];

    static valueToView = (searchTerm: EnumAttributeST | BooleanAttributeST | RefAttributeST) => {

        const nodes: React.ReactNode[] = [];

        searchTerm.itemsData.forEach((item: EnumFormField | BooleanFormField | RefFormField, index: number) => {
            nodes.push(getConcatView(index, item.concatType));
            nodes.push(getInvertedView(item.inverted));

            const value = item.getValue(searchTerm.getOptions());

            let valueNode: React.ReactNode = (<RenderTermPart helper={i18n.t('module.data>search.panel>attributesMeta>notSelected')}/>);

            if (value !== null && value !== undefined) {
                valueNode = (<RenderTermPart value={value.title}/>);
            }

            nodes.push(getFullView(index, valueNode, item.searchType));
        });

        return nodes;
    };

    onChange = (value: string | number) => {
        this.props.onChange('value')(value);
    };

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<{}>, snapshot?: any): void {
        if (prevProps.searchType !== this.props.searchType && this.props.searchType === SEARCH_TYPE.EXIST) {
            this.props.onChange('value')(null);
        }
    }

    render () {
        const {value} = this.props;

        if (this.props.searchType === SEARCH_TYPE.EXIST) {
            return null;
        }

        return (
            <Select
                value={value.value}
                options={this.props.options}
                onChange={this.onChange}
                containerRef={this.props.containerRef}
            />
        );
    }
}
