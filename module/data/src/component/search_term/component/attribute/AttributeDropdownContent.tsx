/**
 * Displaying the dropdown SearchTerm content of the Attribute type
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {SIZE, Icon, IOption, Tooltip} from '@unidata/uikit';
import {
    SIMPLE_DATA_TYPE
} from '@unidata/types';
import * as inputStyles from '../../search_term.m.scss';
import {AttributeValueRange} from './AttributeValueRange';
import {AttributeValueSelect} from './AttributeValueSelect';
import {IProps as IRowItemProps, RowItem} from './RowItem';
import {AttributeValueInput} from './AttributeValueInput';
import {AttributeValueNumber} from './AttributeValueNumber';
import {AttributeValueTime} from './AttributeValueTime';
import {i18n, UserLocale} from '@unidata/core-app';
import * as styles from './attribute.m.css';
import {MeasurementUnit} from './MeasurementUnit';
import {observer} from 'mobx-react';

import {
    AbstractFormField, IAbstractFormField,
    AbstractAttributeST,
    StringAttributeST,
    BooleanAttributeST,
    DateAttributeST,
    RefAttributeST,
    NumberAttributeST,
    EnumAttributeST,
    TimeAttributeST, BooleanFormField, EnumFormField, RefFormField
} from '@unidata/meta';

type IProps<T extends AbstractFormField, S extends SIMPLE_DATA_TYPE> = {
    onDelete: () => void;
    searchTerm: AbstractAttributeST<T, S>;
    isVisible: boolean;
};

@observer
export class AttributeDropdownContent<T extends AbstractFormField, S extends SIMPLE_DATA_TYPE> extends React.Component<IProps<T, S>, {}> {
    modalContainer = React.createRef<HTMLDivElement>();

    addRow = () => {
        this.props.searchTerm.addItem();
    };

    onChange = (index: number) => (key: keyof IAbstractFormField) => (value: any) => {
        const item = this.props.searchTerm.itemsData[index];

        this.props.searchTerm.changeItem(index, {
            ...item,
            [key]: value
        });
    };

    removeRow = (index: number) => () => {
        this.props.searchTerm.deleteItem(index);
    };

    getCommonRowItemProps = (index: number, item: AbstractFormField): IRowItemProps => {
        return {
            searchType: item.searchType,
            onChange: this.onChange(index),
            index: index,
            concatType: item.concatType,
            inverted: item.inverted,
            removeRow: this.removeRow(index),
            valueComponent: null
        };
    };

    renderInput = (searchTerm: StringAttributeST) => {
        return searchTerm.itemsData.map((item, index: number) => {
            const rowItemProps = this.getCommonRowItemProps(index, item);

            rowItemProps.valueComponent = (
                <AttributeValueInput
                    value={item.getValue() || ''}
                    onChange={this.onChange(index)}
                    searchType={item.searchType}
                    isVisible={this.props.isVisible}
                />
            );

            return (
                <RowItem
                    {...rowItemProps}
                    key={`valueItem_${index}`}
                    data-qaid={searchTerm.key}
                />
            );
        });
    };

    renderSelect = (searchTerm: BooleanAttributeST | EnumAttributeST | RefAttributeST) => {
        let itemsData: Array<BooleanFormField | EnumFormField | RefFormField> = searchTerm.itemsData;

        return itemsData.map((item, index: number) => {
            const rowItemProps = this.getCommonRowItemProps(index, item);
            const value = item.getValue(searchTerm.getOptions()) || {value: '', title: ''} as IOption;

            rowItemProps.valueComponent = (
                <AttributeValueSelect
                    value={value}
                    onChange={this.onChange(index)}
                    options={searchTerm.getOptions()}
                    searchType={item.searchType}
                    isVisible={this.props.isVisible}
                    containerRef={this.modalContainer}
                />
            );

            rowItemProps.typeOptions = AttributeValueSelect.typeOptions();

            return (
                <RowItem
                    {...rowItemProps}
                    key={`valueItem_${index}`}
                    data-qaid={searchTerm.key}
                />
            );
        });
    };

    renderNumber = (searchTerm: NumberAttributeST) => {
        return searchTerm.itemsData.map((item, index: number) => {
            const rowItemProps = this.getCommonRowItemProps(index, item);

            rowItemProps.valueComponent = (
                <div className={styles.withMeasurement}>
                    <AttributeValueNumber
                        value={item.getValue()}
                        onChange={this.onChange(index)}
                        searchType={item.searchType}
                        decimalSeparator={UserLocale.getDecimalSeparator()}
                        isInteger={searchTerm.type === SIMPLE_DATA_TYPE.INTEGER}
                    />
                    {searchTerm.measurement && (
                        <MeasurementUnit measurement={searchTerm.measurement}/>
                    )}
                </div>
            );

            rowItemProps.typeOptions = AttributeValueNumber.typeOptions();

            return (
                <RowItem
                    {...rowItemProps}
                    key={`valueItem_${index}`}
                    data-qaid={searchTerm.key}
                />
            );
        });
    };

    renderDate = (searchTerm: DateAttributeST) => {
        return searchTerm.itemsData.map((item, index: number) => {
            const rowItemProps = this.getCommonRowItemProps(index, item);

            rowItemProps.valueComponent = (
                <AttributeValueRange
                    value={item.getValue()}
                    onChange={this.onChange(index)}
                    searchType={item.searchType}
                    showTime={searchTerm.type === SIMPLE_DATA_TYPE.TIMESTAMP}
                    containerRef={this.modalContainer}
                />
            );
            rowItemProps.invertOptions = AttributeValueRange.invertOptions();
            rowItemProps.typeOptions = AttributeValueRange.typeOptions();
            rowItemProps.attributeType = 'date';

            return (
                <RowItem
                    {...rowItemProps}
                    key={`valueItem_${index}`}
                    data-qaid={searchTerm.key}
                />
            );
        });
    };

    renderTime = (searchTerm: TimeAttributeST) => {
        return searchTerm.itemsData.map((item, index: number) => {
            const rowItemProps = this.getCommonRowItemProps(index, item);

            rowItemProps.valueComponent = (
                <AttributeValueTime
                    value={item.getValue()}
                    onChange={this.onChange(index)}
                    searchType={item.searchType}
                    isVisible={this.props.isVisible}
                />
            );

            rowItemProps.typeOptions = AttributeValueTime.typeOptions();

            return (
                <RowItem
                    {...rowItemProps}
                    key={`valueItem_${index}`}
                    data-qaid={searchTerm.key}
                />
            );
        });
    };

    render () {
        const searchTerm = this.props.searchTerm;
        const itemsLength = searchTerm.itemsData.length;

        return (
            <div className={inputStyles.inputContainer} data-qaid={'modal_' + searchTerm.key}>
                <div className={inputStyles.header}>
                    {searchTerm.displayName}
                </div>
                {searchTerm instanceof StringAttributeST && this.renderInput(searchTerm)}
                {(searchTerm instanceof BooleanAttributeST || searchTerm instanceof EnumAttributeST ||
                    searchTerm instanceof RefAttributeST) && this.renderSelect(searchTerm)}
                {searchTerm instanceof NumberAttributeST && this.renderNumber(searchTerm)}
                {searchTerm instanceof DateAttributeST && this.renderDate(searchTerm)}
                {searchTerm instanceof TimeAttributeST && this.renderTime(searchTerm)}

                <div className={[inputStyles.blockContainer, inputStyles.blockContainerLast].join(' ')}>
                    {itemsLength < 5 && (
                        <Tooltip overlay={i18n.t('module.data>search.panel>addAttributeRowItemTooltip')}>
                            <div
                                className={inputStyles.plusIconContainer}
                                onClick={this.addRow}
                            >
                                <Icon name={'plus'} size={SIZE.SMALL}/>
                            </div>
                        </Tooltip>
                    )}
                </div>

                <div ref={this.modalContainer}/>
            </div>
        );
    }
}
