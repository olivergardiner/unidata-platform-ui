/**
 * Displaying The searchterm value of the Attribute type
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {
    BooleanAttributeST,
    DateAttributeST, EnumAttributeST, NumberAttributeST, RefAttributeST,
    StringAttributeST, TimeAttributeST, AbstractSearchTerm
} from '@unidata/meta';
import {AttributeValueInput} from './AttributeValueInput';
import {AttributeValueRange} from './AttributeValueRange';
import {AttributeValueSelect} from './AttributeValueSelect';
import {AttributeValueNumber} from './AttributeValueNumber';
import {AttributeValueTime} from './AttributeValueTime';

import {observer} from 'mobx-react';

interface IProps {
    searchTerm: AbstractSearchTerm;
}

@observer
export class AttributeValueView extends React.Component<IProps, {}> {

    render () {
        const searchTerm = this.props.searchTerm;

        let valueView: React.ReactNode = '';

        if (searchTerm instanceof StringAttributeST) {
            valueView = AttributeValueInput.valueToView(searchTerm);
        } else if (searchTerm instanceof DateAttributeST) {
            valueView = AttributeValueRange.valueToView(searchTerm);
        } else if (searchTerm instanceof BooleanAttributeST || searchTerm instanceof EnumAttributeST ||
            searchTerm instanceof RefAttributeST) {
            valueView = AttributeValueSelect.valueToView(searchTerm);
        } else if (searchTerm instanceof NumberAttributeST) {
            valueView = AttributeValueNumber.valueToView(searchTerm);
        } else if (searchTerm instanceof TimeAttributeST) {
            valueView = AttributeValueTime.valueToView(searchTerm);
        }

        return valueView;
    }
}
