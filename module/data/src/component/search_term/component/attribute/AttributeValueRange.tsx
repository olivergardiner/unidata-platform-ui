/**
 * Displaying the SearchTerm Attribute of the date type
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';

import {i18n, UserLocale} from '@unidata/core-app';
import {DateInput} from '../date_input/DateInput';
import {
    DateAttributeST, DateFormField,
    IAbstractFormField,
    SEARCH_TYPE, TimestampFormField
} from '@unidata/meta';
import {getConcatView, getFullView, getInvertedView, RenderTermPart} from '../CommonTermView';
import moment from 'moment';

interface IProps {
    value?: string | [string | null, string | null] | null;
    onChange: (key: keyof IAbstractFormField | 'range') => (value: [string | null, string | null]) => void;
    searchType: SEARCH_TYPE;
    showTime?: boolean;
    containerRef: React.RefObject<HTMLElement>;
}

export class AttributeValueRange extends React.Component<IProps> {

    static typeOptions = () => [
        {
            title: i18n.t('module.data>search.panel>attributesMeta>exist'),
            value: SEARCH_TYPE.EXIST
        },
        {
            title: `${i18n.t('module.data>search.panel>attributesMeta>range')} / ${i18n.t('module.data>search.panel>attributesMeta>value')}`,
            value: SEARCH_TYPE.EXACT
        }
    ];

    static invertOptions = () => ({
        [SEARCH_TYPE.EXACT]: [
            {
                title: i18n.t('module.data>search.panel>attributesMeta>includes'),
                value: 'false' as const
            },
            {
                title: `${i18n.t('module.data>search.panel>attributesMeta>not')} ${i18n.t('module.data>search.panel>attributesMeta>includes').toLowerCase()}`,
                value: 'true' as const
            }
        ],
        [SEARCH_TYPE.EXIST]: [
            {
                title: i18n.t('module.data>search.panel>attributesMeta>no'),
                value: 'true' as const
            },
            {
                title: i18n.t('module.data>search.panel>attributesMeta>yes'),
                value: 'false' as const
            }
        ]
    });

    static valueToView = (searchTerm: DateAttributeST) => {

        const nodes: React.ReactNode[] = [];

        searchTerm.itemsData.forEach((item: DateFormField | TimestampFormField, index: number) => {
            nodes.push(getConcatView(index, item.concatType));
            nodes.push(getInvertedView(item.inverted));

            const value = item.getValue();

            let valueNode: React.ReactNode = (<RenderTermPart helper={i18n.t('module.data>search.panel>attributesMeta>notSelected')}/>);

            if (value !== null && value !== undefined) {
                const format = item instanceof DateFormField ? UserLocale.getDateFormat() : UserLocale.getDateTimeFormat();
                const dateFrom = (value[0]) ? moment(value[0]).format(format) : null;
                const dateTo = (value[1]) ? moment(value[1]).format(format) : null;

                if (dateFrom !== null && dateTo !== null && dateFrom === dateTo) {
                    valueNode = (<RenderTermPart value={dateFrom}/>);
                } else if (dateFrom === null && dateTo === null) {
                    valueNode = (<RenderTermPart helper={i18n.t('module.data>search.panel>attributesMeta>anyDate')}/>);
                } else {
                    valueNode = (
                        <>
                            {dateFrom !== null && (
                                <RenderTermPart
                                    helper={i18n.t('module.data>search.panel>attributesMeta>fromDate').toLowerCase()}
                                    value={dateFrom}
                                />
                            )}
                            &nbsp;
                            {dateTo !== null && (
                                <RenderTermPart
                                    helper={i18n.t('module.data>search.panel>attributesMeta>toDate').toLowerCase()}
                                    value={dateTo}
                                />
                            )}
                        </>
                    );
                }
            }

            nodes.push(getFullView(index, valueNode, item.searchType));
        });

        return nodes;
    };

    render () {
        const {value} = this.props;

        if (this.props.searchType === SEARCH_TYPE.EXIST) {
            return null;
        }

        return (
            <DateInput
                value={value}
                showTime={this.props.showTime}
                onChange={this.props.onChange('range')}
                containerRef={this.props.containerRef}
            />
        );
    }
}
