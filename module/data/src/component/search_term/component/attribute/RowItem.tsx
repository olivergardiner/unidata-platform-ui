/**
 * Displaying the Search Term Attribute-an abstract string in the list of conditions
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import * as inputStyles from '../../search_term.m.scss';
import {i18n} from '@unidata/core-app';
import {Button, INTENT, joinClassNames, Select, SIZE, Tooltip} from '@unidata/uikit';

import {
    IAbstractFormField,
    SEARCH_TYPE
} from '@unidata/meta';

type InvertedOptions = {
    [SEARCH_TYPE.EXACT]: Array<{ title: string; value: 'true' | 'false' }>;
    [SEARCH_TYPE.EXIST]: Array<{ title: string; value: 'true' | 'false' }>;
};

export type IProps = {
    index: number;
    concatType?: 'AND' | 'OR';
    searchType: SEARCH_TYPE;
    inverted: 'true' | 'false';
    onChange: <T>(key: keyof IAbstractFormField) => (value: T) => void;
    removeRow: () => void;
    valueComponent: React.ReactNode;
    attributeType?: 'date';
    invertOptions?: InvertedOptions;
    typeOptions?: Array<{
        title: string;
        value: SEARCH_TYPE;
    }>;
};

interface IState {
    uiType: SEARCH_TYPE;
}

const invertOptionList = () => {
    return {
        [SEARCH_TYPE.EXACT]: [
            {
                title: '=',
                value: 'false'
            },
            {
                title: '!=',
                value: 'true'
            }
        ],
        [SEARCH_TYPE.EXIST]: [
            {
                title: i18n.t('module.data>search.panel>attributesMeta>no'),
                value: 'true'
            },
            {
                title: i18n.t('module.data>search.panel>attributesMeta>yes'),
                value: 'false'
            }
        ]
    };
};

const typeOptionList = () => {
    return [
        {
            title: i18n.t('module.data>search.panel>attributesMeta>exist'),
            value: SEARCH_TYPE.EXIST
        },
        {
            title: i18n.t('module.data>search.panel>attributesMeta>exact'),
            value: SEARCH_TYPE.EXACT
        },
        {
            title: i18n.t('module.data>search.panel>attributesMeta>startWith'),
            value: SEARCH_TYPE.START_WITH
        },
        {
            title: i18n.t('module.data>search.panel>attributesMeta>like'),
            value: SEARCH_TYPE.LIKE
        },
        {
            title: i18n.t('module.data>search.panel>attributesMeta>fuzzy'),
            value: SEARCH_TYPE.FUZZY
        }
    ];
};

export class RowItem extends React.Component<IProps, IState> {

    modalContainer = React.createRef<HTMLDivElement>();

    state: IState = {
        uiType: SEARCH_TYPE.EXACT
    };

    clickConcatType = (value: 'AND' | 'OR') => () => {
        this.props.onChange('concatType')(value);
    };

    render () {
        const {index, concatType, searchType, inverted} = this.props;
        const ValueComponent = this.props.valueComponent;
        const classSelectType = [inputStyles.selectType];
        const classInvertedType = [inputStyles.selectInverted];

        if (this.props.attributeType && this.props.attributeType === 'date') {
            classSelectType.push(inputStyles.dateAttribute);
            classInvertedType.push(inputStyles.dateAttribute);
        }

        const typeOptions = this.props.typeOptions ? this.props.typeOptions : typeOptionList();
        const invertOptions = this.props.invertOptions ? this.props.invertOptions : invertOptionList();

        const valueContainerClass = joinClassNames(
            inputStyles.valueContainer,
            [inputStyles.wide, 'date' === this.props.attributeType]
        );

        return (
            <div className={inputStyles.blockContainer} key={`valueItem_${index}`} ref={this.modalContainer}>
                {index > 0 && (
                    <div className={inputStyles.concatContainer}>
                        <Button
                            name={'and'}
                            isRound={true}
                            size={SIZE.SMALL}
                            isGhost={concatType !== 'AND'}
                            intent={concatType === 'AND' ? INTENT.PRIMARY : INTENT.DEFAULT}
                            onClick={this.clickConcatType('AND')}
                            data-qaid='andButton'
                        >
                            {i18n.t('module.data>search.panel>attributesMeta>and').toUpperCase()}
                        </Button>
                        &nbsp;
                        <Button
                            name={'or'}
                            isRound={true}
                            size={SIZE.SMALL}
                            isGhost={concatType !== 'OR'}
                            intent={concatType === 'OR' ? INTENT.PRIMARY : INTENT.DEFAULT}
                            onClick={this.clickConcatType('OR')}
                            data-qaid='orButton'
                        >
                            {i18n.t('module.data>search.panel>attributesMeta>or').toUpperCase()}
                        </Button>
                    </div>
                )}
                <div className={inputStyles.sepV}/>
                <div className={inputStyles.sepH}/>

                <div className={classSelectType.join(' ')} data-qaid='searchType'>
                    <Select
                        onChange={this.props.onChange('searchType')}
                        options={typeOptions}
                        value={searchType}
                        containerRef={this.modalContainer}
                    />
                </div>
                <Tooltip overlay={i18n.t('module.data>search.panel>invertTypeTooltip')}>
                    <div className={classInvertedType.join(' ')} data-qaid='invertType'>
                        <Select
                            onChange={this.props.onChange('inverted')}
                            options={invertOptions[(this.props.searchType === SEARCH_TYPE.EXIST) ?
                                SEARCH_TYPE.EXIST :
                                SEARCH_TYPE.EXACT]}
                            value={inverted}
                            containerRef={this.modalContainer}
                        />
                    </div>
                </Tooltip>
                <div className={valueContainerClass} data-qaid='value'>
                    {ValueComponent}
                </div>
                <div className={inputStyles.removeButtonContainer}>
                    {index > 0 && (
                        <Button
                            name={'removeItem'}
                            leftIcon={'delete'}
                            isMinimal={true}
                            onClick={this.props.removeRow}
                            data-qaid='removeButton'
                            title={i18n.t('module.data>search.panel>removeAttributeRowItemTooltip')}
                        />
                    )}
                </div>
            </div>
        );
    }
}
