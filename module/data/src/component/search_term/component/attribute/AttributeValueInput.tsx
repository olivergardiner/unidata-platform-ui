/**
 * Display the Search Term Attribute of type input Field (string)
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';
import {Input} from '@unidata/uikit';
import {
    IAbstractFormField,
    SEARCH_TYPE, StringAttributeST, StringFormField
} from '@unidata/meta';
import {getConcatView, getFullView, getInvertedView, RenderTermPart} from '../CommonTermView';
import {i18n} from '@unidata/core-app';

interface IProps {
    value: string;
    onChange: (key: keyof IAbstractFormField | 'value') => (value: string | null) => void;
    searchType: SEARCH_TYPE;
    isVisible: boolean;
}

export class AttributeValueInput extends React.Component<IProps> {

    static valueToView = (searchTerm: StringAttributeST) => {
        const nodes: React.ReactNode[] = [];

        searchTerm.itemsData.forEach((item: StringFormField, index: number) => {
            nodes.push(getConcatView(index, item.concatType));
            nodes.push(getInvertedView(item.inverted));

            const value = item.getValue();

            let valueNode: React.ReactNode = (<RenderTermPart helper={i18n.t('module.data>search.panel>attributesMeta>notSelected')}/>);

            if (value !== null && value !== undefined && value !== '') {
                valueNode = (
                    <RenderTermPart
                        value={value}
                    />
                );
            }

            nodes.push(getFullView(index, valueNode, item.searchType));
        });

        return nodes;
    };

    inputElement = React.createRef<HTMLInputElement>();

    // todo Brauer Ilya: the antd DropDown - props isVisible feature comes before the component is mounted.
    // This is the first time the lifecycle is used for focus
    componentDidMount (): void {
        if (this.inputElement.current !== null) {
            this.inputElement.current.focus();
        }
    }

    onChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;

        this.props.onChange('value')(value);
    };

    componentDidUpdate (prevProps: Readonly<IProps>, prevState: Readonly<{}>, snapshot?: any): void {
        if (prevProps.searchType !== this.props.searchType && this.props.searchType === SEARCH_TYPE.EXIST) {
            this.props.onChange('value')(null);
        }

        if (this.props.isVisible === true && prevProps.isVisible === false && this.inputElement.current !== null) {
            this.inputElement.current.focus();
        }
    }

    render () {
        const {value} = this.props;

        if (this.props.searchType === SEARCH_TYPE.EXIST) {
            return null;
        }

        return (
            <Input
                value={value as string}
                onChange={this.onChange}
            />
        );
    }
}
