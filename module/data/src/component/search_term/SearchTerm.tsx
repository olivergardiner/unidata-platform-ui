/**
 * Displaying Search Term
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';

import {Attribute} from './component/attribute/Attribute';
import {CommonTermGroup} from './component/CommonTermGroup';
import {Group} from './component/group/Group';
import {GroupItem} from './component/group/GroupItem';
import {DateRange} from './component/date_range/DateRange';
import {Select} from './component/select/Select';
import {AbstractAttributeST, SelectST, AsOfST, CreateUpdateST, AbstractSearchTerm} from '@unidata/meta';

export type ITermItem = {
    onDelete: () => void;
    searchTerm: AbstractSearchTerm;
}

export class SearchTerm extends React.Component<ITermItem> {

    static Attribute = Attribute;
    static Group = Group;
    static GroupItem = GroupItem;
    // static CommonTermView = CommonTermView;
    static CommonGroup = CommonTermGroup;
    static Select = Select;

    render () {
        const searchTerm = this.props.searchTerm;

        if (searchTerm instanceof AsOfST || searchTerm instanceof CreateUpdateST) {
            return (
                <DateRange
                    searchTerm={searchTerm}
                    onDelete={this.props.onDelete}
                />
            );
        }

        if (searchTerm instanceof SelectST) {
            return (
                <Select
                    searchTerm={searchTerm}
                    onDelete={this.props.onDelete}
                />
            );
        }

        if (searchTerm instanceof AbstractAttributeST) {
            return (
                <Attribute
                    searchTerm={searchTerm}
                    onDelete={this.props.onDelete}
                />
            );
        }

        console.warn(`Could not render SearchTerm`, searchTerm);

        return null;
    }
}

export * from './component/CommonTermView';
