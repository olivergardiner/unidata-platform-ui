/**
 * Display CriteriaItem-component for displaying the selection of a single criterion in the search
 *
 * @author Brauer Ilya
 * @date 2020-02-25
 */

import * as React from 'react';

import * as classes from './criteria_item.m.css';
import {Icon, joinClassNames} from '@unidata/uikit';
import {IconIntent} from '@unidata/icon';

export type TCriteriaItem = React.FC<{
    title: string; // the text of the criterion
    onClick?: (e: React.SyntheticEvent<HTMLDivElement>) => void; // click handler

    isChecked?: boolean; // whether the criteria is selected
    isHeader?: boolean; // is it a header
    hasChildren?: boolean; // does it have descendants (for displaying the correct icon)
    counter?: number; // counter of selected times
    isDisabled?: boolean; // flag for the disabled element
    'data-qaid': string; // ID for tests
    forwardedRef?: React.Ref<HTMLDivElement>;
}>;

const RenderCounter: React.FC<{
    counter: number;
}> = (props) => {
    return (
        <div className={classes.counter}>{props.counter}</div>
    );
};

const RenderIcon: React.FC<{
    hasChildren?: boolean;
    counter?: number;
}> = (props) => {
    if (props.hasChildren !== undefined) {
        return (
            <Icon name={props.hasChildren === false ? 'stop' : 'caret-right'}/>
        );
    } else if (props.counter && props.counter > 0) {
        return RenderCounter({counter: props.counter});
    }

    return (
        <Icon name={'check'} intent={IconIntent.PRIMARY}/>
    );
};

export const CriteriaItem: TCriteriaItem = (props) => {
    const classNames = joinClassNames(
        classes.criteriaItem,
        [classes.isChecked, props.isChecked === true],
        [classes.isHeader, props.isHeader === true],
        [classes.hasChildrenIcon, props.hasChildren !== undefined],
        [classes.isDisabled, props.isDisabled === true || (props.hasChildren === false)],
        [classes.hasCounter, (props.counter !== undefined && props.counter > 0)]
    );

    let onClick: any = () => null;

    if (props.isChecked !== true && props.hasChildren !== false && props.isDisabled !== true) {
        onClick = props.onClick;
    }

    return (
        <div
            className={classNames}
            onClick={onClick}
            data-qaid={props['data-qaid']}
            ref={props.forwardedRef}
        >
            {props.title}

            <div className={classes.iconContainer}>
                {RenderIcon({hasChildren: props.hasChildren, counter: props.counter})}
            </div>
        </div>
    );
};
