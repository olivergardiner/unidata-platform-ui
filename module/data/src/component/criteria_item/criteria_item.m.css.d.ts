declare const styles: {
  readonly "criteriaItem": string;
  readonly "iconContainer": string;
  readonly "isChecked": string;
  readonly "checkHover": string;
  readonly "hasChildrenIcon": string;
  readonly "hasCounter": string;
  readonly "counter": string;
  readonly "isHeader": string;
  readonly "isDisabled": string;
};
export = styles;

