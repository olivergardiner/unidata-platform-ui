import {ArtifactModel} from '../models/ArtifactModel';
import {IModelOpConfig, ModelHttpOp} from '../../src/operation/ModelHttpOp';

export class TestOp extends ModelHttpOp {
    protected config: IModelOpConfig = {
        url: '/test/api',
        method: 'get',
        model: ArtifactModel,
        rootProperty: ''
    };
}
