/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-28
 */

import {observable} from 'mobx';

import {SimpleModel} from './SimpleModel';
import {ArrayField, arrayField, NumberField, numberField, StringField, stringField} from '../../src/data';

export class ExtendedModel extends SimpleModel {
    @observable
    @numberField({primaryKey: true})
    public id: NumberField;

    @observable
    @stringField()
    public country: StringField;

    @observable
    @arrayField()
    public сhildrenNames: ArrayField;
}
