/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-28
 */

import {AbstractModel} from '../../src/data/model/AbstractModel';
import {StringField} from '../../src/data/field/StringField';
// import {observable} from 'mobx';
import {stringField} from '../../src/data/Annotations';

export class PeopleModel extends AbstractModel {

    // @observable
    @stringField()
    public name: StringField;

    // @observable
    @stringField()
    public lastName: StringField;
}
