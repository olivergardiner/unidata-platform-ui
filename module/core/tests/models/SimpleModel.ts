/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-28
 */

import {observable} from 'mobx';

import {PeopleModel} from './PeopleModel';
import {ArtifactModel} from './ArtifactModel';
import {
    AbstractModel, hasMany,
    hasOne,
    IAssociationDescriptorMap,
    IntegerField,
    integerField, ModelCollection, ModelMetaData,
    StringField,
    stringField
} from '../../src/data';

export class SimpleModel extends AbstractModel {

    @observable
    @stringField()
    public name: StringField;

    @observable
    @stringField()
    public lastName: StringField;

    @observable
    @integerField()
    public age: IntegerField;

    @observable
    @hasOne()
    public boss: PeopleModel;

    @observable
    @hasOne()
    public friend: PeopleModel;

    @observable
    @hasMany()
    public artifacts: ModelCollection<ArtifactModel>;

    public initAssociationDescriptors () {
        super.initAssociationDescriptors();

        let map: IAssociationDescriptorMap = {
            hasOne: {
                boss: PeopleModel,
                friend: PeopleModel
            },
            hasMany: {
                artifacts: ArtifactModel
            }
        };

        ModelMetaData.initAssociationDescriptors(this.constructor, map);
    }
}
