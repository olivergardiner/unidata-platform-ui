/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-08-28
 */

import {
    AbstractModel,
    IntegerField,
    integerField,
    numberField,
    NumberField,
    stringField,
    StringField
} from '../../src';
import {observable} from 'mobx';

export class ArtifactModel extends AbstractModel {

    @observable
    @numberField({primaryKey: true})
    public id: NumberField;

    @observable
    @stringField()
    public name: StringField;

    @observable
    @integerField()
    public cost: IntegerField;
}
