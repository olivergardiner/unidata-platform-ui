/**
 * External module Manager
 *
 * @author Ivan Marshalkin
 * @date 2019-09-10
 */

import {UeModuleLoader} from './UeModuleLoader';
import {Nullable} from '../type/Nullable';
import {UeModuleTypeCallBack, UeModuleTypeComponent} from './UeModuleType';
import {UeModuleBase} from './type/UeModuleBase';
import {IComponentPropsByUEType, UeModuleReactComponent} from './type/UeModuleReactComponent';
import {UeModuleCallback} from './type/UeModuleCallback';
import {ComponentType} from 'react';

type ModuleFilterOptions = {
    active?: boolean;
    system?: boolean;
}

export class UeModuleManager {
    private modules: UeModuleBase[] = [];
    private moduleLoader: UeModuleLoader;

    private moduleMap: { [key: string]: UeModuleBase } = {};
    private moduleMapByType: {
        [key in UeModuleTypeCallBack|UeModuleTypeComponent]?: UeModuleBase[];
    } = {};

    constructor (moduleLoader: UeModuleLoader) {
        this.moduleLoader = moduleLoader;
    }

    /**
     * Loads multiple modules
     *
     * @param modulePaths
     */
    public importModules (modulePaths: string[]): Promise<any> {
        const self = this;

        return this.moduleLoader.loadModules(modulePaths)
            .then(function (modules) {
                modules.forEach(function (module) {
                    // we do not process non valid modules
                    if (!self.isValidModule(module)) {
                        return;
                    }

                    self.transformModule(module);

                    let moduleId = module.default.moduleId;

                    if (self.hasModuleById(moduleId)) {
                        throw `duplicate moduleId: ${moduleId}`;
                    } else {
                        self.addModule(module);
                    }
                });

                return Promise.resolve();
            })
            .catch(function () {
                return Promise.reject();
            });
    }

    /**
     * Adds a module to the Manager
     *
     * @param module
     */
    public addModule (module: UeModuleBase) {
        const moduleId = module.default.moduleId;
        const moduleType = module.default.type;

        if (!this.moduleMapByType[moduleType]) {
            this.moduleMapByType[moduleType] = [];
        }

        this.modules.push(module);

        this.moduleMap[moduleId] = module;

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.moduleMapByType[moduleType].push(module);
    }

    public addModules (modules: UeModuleBase[]) {
        modules.forEach(this.addModule, this);
    }

    /**
     * Modifies the loaded module
     *
     * @param module
     */
    private transformModule (module: UeModuleBase) {
        if (!module.default.type) {
            module.default.type = UeModuleTypeCallBack.UNKNOWN;
        }
    }

    /**
     * Checks the validity of the loaded module
     *
     * @param module
     */
    private isValidModule (module: UeModuleBase) {
        if (module && module.default && module.default.moduleId) {
            return true;
        }

        return false;
    }

    /**
     * Returns the module by its type. Additional parameters for filtering by attributes can be specified.
     *
     * @param moduleType
     * @param opt
     */
    public getModulesByType <T extends UeModuleTypeComponent>(moduleType: T, opt?: ModuleFilterOptions): Array<UeModuleReactComponent<T>>;
    public getModulesByType <T extends UeModuleTypeCallBack>(moduleType: T, opt?: ModuleFilterOptions): Array<UeModuleCallback<T>>;
    public getModulesByType (
        moduleType: UeModuleTypeCallBack | UeModuleTypeComponent,
        opt: ModuleFilterOptions = {active: true, system: false}
    ): UeModuleBase[] {
        let modules = this.moduleMapByType[moduleType];

        modules = modules && modules.filter(function (module) {
            const activeFilter = !Object.getOwnPropertyDescriptor(opt, 'active') || module.default.active === opt.active;
            const systemFilter = !Object.getOwnPropertyDescriptor(opt, 'system') || module.default.system === opt.system;

            if (module && activeFilter && systemFilter) {
                return true;
            }

            return false;
        });

        return modules && modules.length ? modules : [];
    }

    /**
     * Returns the module by its ID. Additional parameters for filtering by attributes can be specified.
     *
     * @param moduleId
     */
    public getModuleById (moduleId: string): Nullable<UeModuleBase> {
        const module = this.moduleMap[moduleId];

        return module ? module : null;
    }

    public getComponentById<T extends UeModuleTypeComponent> (moduleId: string, resolverArgs?: any[]):
        Nullable<ComponentType<IComponentPropsByUEType[T]>> {
        const module = this.moduleMap[moduleId] as UeModuleReactComponent<T>;

        if ((module.default.resolver === undefined || module.default.resolver(resolverArgs) === true) && module.default.component) {
            return module.default.component;
        }

        return null;
    }

    public hasModuleById (moduleId: string): boolean {
        const module = this.getModuleById(moduleId);

        return module ? true : false;
    }

    public isComponentModule<T extends UeModuleTypeComponent> (module: UeModuleBase, type: T): module is UeModuleReactComponent<T> {
        return module.default.type === type;
    }

}
