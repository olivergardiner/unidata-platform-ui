/**
 * Interface for an external module of the "react component" type
 *
 * @author Ivan Marshalkin
 * @date 2019-09-10
 */
import {ComponentType, default as React, ReactNode} from 'react';
import {UeModuleBase} from './UeModuleBase';
import {UeModuleTypeComponent} from '../UeModuleType';
import {IStringKeyMap, Nullable} from '../../type';
import {ObservableMap} from 'mobx';

export interface IComponentPropsByUEType {
    [UeModuleTypeComponent.DATA_PROVIDER]: {
        children: {(...args: any[]): ReactNode};
    };

    [UeModuleTypeComponent.PAGE]: {};

    [UeModuleTypeComponent.GLOBAL_HOTKEY]: {};

    [UeModuleTypeComponent.RENDER_CRITERION]: {
        metaRecord: any;
        onAddTerm: (searchTerm: any) => void; // handler for adding criteria
        searchText: string; // search text (for filtering criteria in BM)
        searchTerms: any[]; // list of already selected SearchTerms
        parentIsVisible: boolean; // the visibility flag of the parent
    };
    [UeModuleTypeComponent.RENDER_SEARCH_TERM]: {
        searchTerm: any;
        searchTermList: any[];
        onAddTerm?: () => void;
        onDelete: () => void;
    };
    [UeModuleTypeComponent.ROLE_CUSTOMIZER]: {};
    [UeModuleTypeComponent.RENDER_CELL]: {};
    [UeModuleTypeComponent.ROLE_SECURITY]: {
        readOnly?: boolean;
    };
    [UeModuleTypeComponent.WORKFLOW_ASSIGNMENT]: {
        handleItemAdd: (name: string, defaultAssignmentType?: string, defaultAssignmentProcessType?: any) => void;
        assignmentsHashByEntityName: ObservableMap;
        readOnly?: boolean;
        'data-qaid'?: string;
        assignableNames?: string[];
    };
    [UeModuleTypeComponent.CLASSIFIER_TREE]: {
        clsfName: string;
        onSelectNode: (node: any) => void; // todo Brauer Ilya replace the type with ITreeNode after transferring it to the types module
    };
    [UeModuleTypeComponent.MATCHING_PARAMETER]: {
        treeTableStore: any;
        onBeforeSelect?: (nodes: any []) => boolean;
        onSelect?: (nodes: any []) => {};
    };
    [UeModuleTypeComponent.MATCHING_MAPPING]: {};
    [UeModuleTypeComponent.ENTITY_LIST]: {
        treeTableStore: any;
        onEntitySelect: (catalogEntity: any) => void;
        onBeforeNodeSelect?: (node: any) => boolean;
    };
    [UeModuleTypeComponent.BULK_DATA_WIZARD]: {
        entityName: string;
        entityType: any; // TODO: @marshlkin there should be a type here // import {EntityType} from '@unidata/types';
        etalonIds: any[];
        query: any;
        queryCount: number;
        onFinish: () => void;
        classifiers: string[];
        runOperation: (cfg: string, t: string) => void;
        operationTitle: string;
        recordCount: number;
    };
    [UeModuleTypeComponent.DATA_PAGE_WIDGET]: {};
    [UeModuleTypeComponent.CMP_DRAFT_LIST_WND]: {
        isOpen: boolean;
        type: string;
        entityId: string;
        onDraftOpen: (draftId: string) => void;
        onClose: () => void;
    };
    [UeModuleTypeComponent.ABSTRACT_SEARCH_CRITERIA]: {
        title: string; // the text of the criterion
        onClick?: (e: React.SyntheticEvent<HTMLDivElement>) => void; // click handler

        isChecked?: boolean; // whether the criteria is selected
        isHeader?: boolean; // is it a header
        hasChildren?: boolean; // does it have descendants (for displaying the correct icon)
        counter?: number; // counter of selected times
        isDisabled?: boolean; // flag for the disabled element
        'data-qaid'?: string; // ID for tests
    };
    [UeModuleTypeComponent.ABSTRACT_SEARCH_TERM]: {
        searchTerm: any; // todo Brauer Ilya here will be SearchTerm from @unidata/types
        onChange: (value: any) => void;
        onDelete: () => void;
        onSubmit: () => void;
    };
    [UeModuleTypeComponent.USER_LIST_FILTER]: {
        onFilterChange: (filterId: string, fn: Nullable<(user: any) => boolean>) => void;
    };
    [UeModuleTypeComponent.DRAFT_DATA_LIST]: {
        entityName: string;
    };
}

export interface IComponentMeta {
    [UeModuleTypeComponent.PAGE]: {
        route: string;
    };

    [UeModuleTypeComponent.GLOBAL_HOTKEY]: {};
    [UeModuleTypeComponent.RENDER_CRITERION]: {};
    [UeModuleTypeComponent.RENDER_SEARCH_TERM]: {};
    [UeModuleTypeComponent.ROLE_CUSTOMIZER]: {};
    [UeModuleTypeComponent.RENDER_CELL]: {};
    [UeModuleTypeComponent.ROLE_SECURITY]: {
        menuKey: string;                                // key of the menu item
        menuText: () => string;                         // text of menu item
        headerItems: ReactNode[];                       // ui elements in the header for this UE (buttons, for example)
        load: (roleName: string) => Promise<any>;
        save: (roleName: string) => Promise<any>;
        reset: () => void;
        canSave: () => boolean;
    };
    [UeModuleTypeComponent.DATA_PROVIDER]: {};
    [UeModuleTypeComponent.WORKFLOW_ASSIGNMENT]: {
        order: number;
        menuKey: string;                                // key of the menu item
        menuText: () => string;                         // text of the menu item
        dataQaId: string;
        store: any;
    };
    [UeModuleTypeComponent.CLASSIFIER_TREE]: {};
    [UeModuleTypeComponent.MATCHING_PARAMETER]: {
        menuKey: string;
        menuText: () => string;
        matchingParameterDisplayTypes: IStringKeyMap<() => string>;
        matchingParameterTypes: string [];
        loader: any;
    };
    [UeModuleTypeComponent.MATCHING_MAPPING]: {
        storeCtor: any;
    };
    [UeModuleTypeComponent.ENTITY_LIST]: {
        storeCtor: any;
    };
    [UeModuleTypeComponent.BULK_DATA_WIZARD]: {
        wizardId: string;
    };
    [UeModuleTypeComponent.DATA_PAGE_WIDGET]: {
        // see the documentation for https://github.com/STRML/react-grid-layout
        dataGrid: {
            i: string; // unique identifier of the widget
            x: number; // position on the grill on the x axis
            y: number; // position on the grill on the y axis
            w: number; // width (in grid columns-by default 12)
            h: number; // height (in grid lines)
        };
    };
    [UeModuleTypeComponent.CMP_DRAFT_LIST_WND]: {
    };
    [UeModuleTypeComponent.ABSTRACT_SEARCH_CRITERIA]: {};
    [UeModuleTypeComponent.ABSTRACT_SEARCH_TERM]: {};
    [UeModuleTypeComponent.USER_LIST_FILTER]: {};
    [UeModuleTypeComponent.DRAFT_DATA_LIST]: {};
}

export type UeModuleReactComponent<T extends UeModuleTypeComponent> = UeModuleBase & {
    default: {
        meta: IComponentMeta[T];
        component: ComponentType<IComponentPropsByUEType[T]>;
    };
}
