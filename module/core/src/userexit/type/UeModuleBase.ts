/**
 * Basic interface for an external module
 *
 * @author Ivan Marshalkin
 * @date 2019-09-10
 */
import {UeModuleTypeCallBack, UeModuleTypeComponent} from '../UeModuleType';

export type UeModuleBase = {
    default: {
        type: UeModuleTypeCallBack | UeModuleTypeComponent;                     // module type by place of use
        moduleId: string;                       // unique identifier of the module
        active: boolean;                        // indicates whether the module is active
        system: boolean;                        // indicates the "system module"
        resolver: (...args: any[]) => any;      // the idea defines the applicability to the place where it is called, the criteria for which the decision is made are passed by parameters
        meta: {                                 // additional information specific to the engine
            [key: string]: any;
        };
    };
}
