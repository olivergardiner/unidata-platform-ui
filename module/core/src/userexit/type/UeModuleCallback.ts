/**
 * Interface for an external module of the "callback" type
 *
 * @author Ivan Marshalkin
 * @date 2019-09-10
 */
import {UeModuleBase} from './UeModuleBase';
import {UeModuleTypeCallBack} from '../UeModuleType';
import {CSSProperties} from 'react';
import {Nullable} from '../../type';

export interface ICallbackByUEType {
    [UeModuleTypeCallBack.GET_CLASSIFIERS]: () => Promise<any[]>; //TODO: move to core-app and make the ioptionmanager type
    [UeModuleTypeCallBack.UNKNOWN]: (...args: any[]) => any;
    [UeModuleTypeCallBack.APP_DATA_LOADER]: () => Promise<any>;
    [UeModuleTypeCallBack.GET_DRAFT_INFO]: (
        type: string,
        entityId: Nullable<string>,
        draftId: string
    ) => Promise<null | {displayName: string}>;
    [UeModuleTypeCallBack.TOAST_OPTIONS]: (...args: any[]) => any;
    [UeModuleTypeCallBack.MENU_ITEM]: (...args: any[]) => any;
    [UeModuleTypeCallBack.JOB_PARAMETER]: (...args: any[]) => any;
    [UeModuleTypeCallBack.AUDIT_HEADER]: (...args: any[]) => any;
    [UeModuleTypeCallBack.GROUP_MENU]: (...args: any[]) => any;
    [UeModuleTypeCallBack.PASSWORD_POLICY]: (newPassword: string) => string | undefined;
    [UeModuleTypeCallBack.GET_DQ_NAMES]: (
        req: {
            entityName: string;
            classifiers: null | Array<{
                classifierName: string;
                classifierNodeId: string;
            }>;
        }
    ) => Promise<Array<{value: string; displayValue: string}>>; // todo fix types
    [UeModuleTypeCallBack.GET_DQ_CATEGORIES]: (
        req: {
            entityName: string;
            classifiers: null | Array<{
                classifierName: string;
                classifierNodeId: string;
            }>;
        }
    ) => Promise<Array<{value: string; displayValue: string}>>; // todo fix types
    [UeModuleTypeCallBack.SET_PASSWORD_BACKGROUND]: () => CSSProperties;
    [UeModuleTypeCallBack.SEARCH_TERM_MODEL]: () => void;
    [UeModuleTypeCallBack.GET_META_RECORD]: (
        entity: any
    ) => Promise<any>; // todo fix types
    [UeModuleTypeCallBack.WEBSOCKET_LISTENER]: () => {type: string; listener: any};
    [UeModuleTypeCallBack.GET_STAT_PARAMETER_OPTIONS]: () => Promise<any>;
    [UeModuleTypeCallBack.GET_STAT_ENTITY_OPTIONS]: () => Promise<any>;
}

export interface ICallbackMeta {
    [UeModuleTypeCallBack.UNKNOWN]: {};
    [UeModuleTypeCallBack.APP_DATA_LOADER]: {};
    [UeModuleTypeCallBack.GET_DRAFT_INFO]: {};
    [UeModuleTypeCallBack.TOAST_OPTIONS]: {
        closeDelay?: number;
        disableAutoClose?: boolean;
    };
    [UeModuleTypeCallBack.GET_CLASSIFIERS]: {};
    [UeModuleTypeCallBack.MENU_ITEM]: { // This is the IMenuItem interface from core-app without component
        name: string;
        route: string;
        icon: any; // there must be an IconName type defined in uikit (or MenuIcon as a subset of it)
        menuColor?: string; // see uikit specific/menu_item/menuColors.ts
        activeInDraftMode?: boolean;
        pinned?: boolean;
        isActiveFor?: string[]; // if menu item should be active not only for route, set here all possible routs
    };
    [UeModuleTypeCallBack.JOB_PARAMETER]: {};
    [UeModuleTypeCallBack.AUDIT_HEADER]: {};
    [UeModuleTypeCallBack.GROUP_MENU]: {};
    [UeModuleTypeCallBack.PASSWORD_POLICY]: {};
    [UeModuleTypeCallBack.GET_DQ_NAMES]: {};
    [UeModuleTypeCallBack.GET_DQ_CATEGORIES]: {};
    [UeModuleTypeCallBack.SET_PASSWORD_BACKGROUND]: {};
    [UeModuleTypeCallBack.GET_META_RECORD]: {};
    [UeModuleTypeCallBack.WEBSOCKET_LISTENER]: {};
    [UeModuleTypeCallBack.SEARCH_TERM_MODEL]: {
        modelCtor: any;
    };
    [UeModuleTypeCallBack.GET_STAT_PARAMETER_OPTIONS]: {};
    [UeModuleTypeCallBack.GET_STAT_ENTITY_OPTIONS]: {};
}

export type UeModuleCallback<T extends UeModuleTypeCallBack> = UeModuleBase & {
    default: {
        meta: ICallbackMeta[T];
        fn: ICallbackByUEType[T];
    };
}
