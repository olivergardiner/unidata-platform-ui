﻿/**
 * Types of extension points
 */

export enum UeModuleTypeCallBack {
    UNKNOWN = 'UNKNOWN',
    // App
    MENU_ITEM = 'MENU_ITEM',                                // adds a menu item
    TOAST_OPTIONS = 'TOAST_OPTIONS',                        // settings for the Toast component
    APP_DATA_LOADER = 'APP_DATA_LOADER',                    // loader of various data at the start of the application
    WEBSOCKET_LISTENER = 'WEBSOCKET_LISTENER',              // handler for messages from a websocket
    // Draft
    GET_DRAFT_INFO = 'GET_DRAFT_INFO',                      // obtaining information about the draft
    // Enterprise
    GET_CLASSIFIERS = 'GET_CLASSIFIERS',                    // promise for getting classifiers
    // User
    JOB_PARAMETER = 'JOB_PARAMETER',                        // adds a parameter to the operation
    AUDIT_HEADER = 'AUDIT_HEADER',                          // column in the audit log
    GROUP_MENU = 'GROUP_MENU',                              // transforms groups in the menu
    PASSWORD_POLICY = 'PASSWORD_POLICY',                    // password verification
    GET_DQ_NAMES = 'GET_DQ_NAMES',                          // getting a list of quality rule names by entityName or classifier
    GET_DQ_CATEGORIES = 'GET_DQ_CATEGORIES',                // getting a list of quality rule categories by entityName or classifier
    GET_META_RECORD = 'GET_META_RECORD',                    // getting a MetaRecord
    SET_PASSWORD_BACKGROUND = 'SET_PASSWORD_BACKGROUND',    // changing the background when changing your password

    SEARCH_TERM_MODEL = 'SEARCH_TERM_MODEL',                // search term model (for EE modules DQ and Classifier)

    GET_STAT_PARAMETER_OPTIONS = 'GET_STAT_PARAMETER_OPTIONS',  // get parameters for statistic widget
    GET_STAT_ENTITY_OPTIONS = 'GET_STAT_ENTITY_OPTIONS'         // get entities for statistic widget
}

export enum UeModuleTypeComponent {
    // App
    PAGE = 'PAGE',                                          // route page
    GLOBAL_HOTKEY = 'GLOBAL_HOTKEY',                        // global hotkeys
    ROLE_CUSTOMIZER = 'ROLE_CUSTOMIZER',
    RENDER_CELL = 'RENDER_CELL',                            // drawing a cell in a table in data search
    RENDER_CRITERION = 'RENDER_CRITERION',                  // a component of the search criterion
    DATA_PROVIDER = 'DATA_PROVIDER',                        // wrapper component with data and children as render prop
    BULK_DATA_WIZARD = 'BULK_DATA_WIZARD',                  // wizard for batch data operations
    CMP_DRAFT_LIST_WND = 'CMP_DRAFT_LIST_WND',                      // draft selection window
    // Enterprise
    WORKFLOW_ASSIGNMENT = 'WORKFLOW_ASSIGNMENT',            // configuring the business process for various categories of meta objects (registers, classifiers, etc.)
    RENDER_SEARCH_TERM = 'RENDER_SEARCH_TERM',              // output of the search term
    ROLE_SECURITY = 'ROLE_SECURITY',                        // tab for additional security settings on the roles screen
    CLASSIFIER_TREE = 'CLASSIFIER_TREE',                    // classifier tree
    MATCHING_PARAMETER = 'MATCHING_PARAMETER',              // the choice of the matching parameter
    MATCHING_MAPPING = 'MATCHING_MAPPING',                  // panel with setting up matching fields
    ENTITY_LIST = 'ENTITY_LIST',                            // list of registers and reference lists

    DATA_PAGE_WIDGET = 'DATA_PAGE_WIDGET',                  // Widget on the main data page

    ABSTRACT_SEARCH_CRITERIA = 'ABSTRACT_SEARCH_CRITERIA',  // Abstract view of search criteria
    ABSTRACT_SEARCH_TERM = 'ABSTRACT_SEARCH_TERM',          // Abstract view of search term
    USER_LIST_FILTER = 'USER_LIST_FILTER',                  // Additional filters for the customer's user list
    DRAFT_DATA_LIST = 'DRAFT_DATA_LIST'                    // List of drafts for the registry / directory
}

