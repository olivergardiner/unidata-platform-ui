/**
 * The module loader
 *
 * @author Ivan Marshalkin
 * @date 2019-09-10
 */
import {UeModuleBase} from './type/UeModuleBase';

export class UeModuleLoader {
    private systemJs: any;

    constructor (systemJs: any) {
        this.systemJs = systemJs;
    }

    /**
     * Loads the module
     *
     * @param modulePath
     */
    public loadModule (modulePath: string): Promise<UeModuleBase> {
        return this.systemJs.import(modulePath);
    }

    /**
     * Loads multiple modules
     *
     * @param modulePaths
     */
    public loadModules (modulePaths: string[]): Promise<UeModuleBase[]> {
        const self = this;

        let promises: Array<Promise<any>>;

        promises = modulePaths.map(function (modulePath) {
            return self.loadModule(modulePath);
        });

        return Promise.all(promises);
    }
}
