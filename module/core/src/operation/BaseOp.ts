/**
 * Base class of the operation
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {Sequence} from '../Sequence';

export abstract class BaseOp {
    protected operationId: number;

    protected constructor () {
        this.operationId = Sequence.getId();
    }

    public abstract execute (...args: any[]): Promise<any>;
    protected abstract success (...args: any[]): Promise<any>;
    protected abstract failure (...args: any[]): Promise<any>;
}
