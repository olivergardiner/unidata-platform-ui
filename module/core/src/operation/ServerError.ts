/**
 * Server error interface
 *
 * @author Stebunov Vladimir
 * @date 2019-09-25
 */

export interface IServerError {
    userMessageDetails?: string;
    userMessage?: string;
}
