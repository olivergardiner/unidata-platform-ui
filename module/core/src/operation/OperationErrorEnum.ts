/**
 * Type of operation error
 *
 * @author: Aleksandr Bavin
 * @date: 2020-03-30
 */

export enum OperationErrorEnum {
    FAILURE_FROM_SUCCESS = 'failure from success',
    FAILURE = 'failure'
}
