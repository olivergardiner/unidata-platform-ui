/**
 * An operation that generates a model as a result of execution
 *
 * @author Ivan Marshalkin
 * @date 2019-10-04
 */

import {BaseHttpOp, IOpConfig} from './BaseHttpOp';
import {ClassCtor} from '../type/ClassCtor';
import {AbstractModel} from '../data/model/AbstractModel';
import {AxiosResponse} from 'axios';

export type IModelOpConfig = IOpConfig & {model: ClassCtor<AbstractModel>};

export class ModelHttpOp extends BaseHttpOp {
    protected config: IModelOpConfig;
    protected baseURL: string;

    constructor () {
        super();
    }

    protected processOperationData (data: any): any {
        const classModel = this.config.model;

        return new classModel(data);
    }

    protected responseDataValid (value: AxiosResponse<any>): boolean {
        let data = this.getOperationResponseData(value);

        // if there is no data we assume that the operation cannot be completed successfully
        if (!data) {
            return false;
        }

        return super.responseDataValid(value);
    }
}
