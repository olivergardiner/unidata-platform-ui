/**
 * Base class of operation errors
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {IServerError} from './ServerError';
import {OperationErrorEnum} from './OperationErrorEnum';

export class OperationError extends Error {
    private code: any;
    private axiosResponse: any;
    private isOperationError: boolean;

    public readonly type: OperationErrorEnum;

    constructor (message: OperationErrorEnum, code: any, axiosResponse: any) {
        super(message);

        Object.setPrototypeOf(this, OperationError.prototype);

        if (code) {
            this.code = code;
        }

        this.type = message;

        this.axiosResponse = axiosResponse;
        this.isOperationError = true;
    }

    public toJSON () {
        const me: any = this;

        return {
            // Standard
            message: me.message,
            name: me.name,
            // Microsoft
            description: me.description,
            number: me.number,
            // Mozilla
            fileName: me.fileName,
            lineNumber: me.lineNumber,
            columnNumber: me.columnNumber,
            stack: me.stack,
            // Axios
            config: me.config,
            axiosResponse: me.axiosResponse,
            code: me.code
        };
    }

    public getServerError (): IServerError[] {
        if (!this.axiosResponse.data) {
            return [];
        }

        return this.axiosResponse.data.errors;
    }

}
