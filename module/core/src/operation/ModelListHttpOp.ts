/**
 * An operation that generates a list of models as a result of execution
 *
 * @author Ivan Marshalkin
 * @date 2019-10-04
 */

import {BaseHttpOp} from './BaseHttpOp';
import {IModelOpConfig} from './ModelHttpOp';

export class ModelListHttpOp extends BaseHttpOp {
    protected config: IModelOpConfig;
    protected baseURL: string;

    protected processOperationData (data: any): any {
        const classModel = this.config.model;

        let result: any[];

        // if there is no data we assume that the operation ends with an empty array
        if (!(data instanceof Array)) {
            return [];
        }

        result = data.map(function (item) {
            return new classModel(item);
        });

        return result;
    }
}
