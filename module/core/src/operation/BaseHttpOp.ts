/**
 * Base class for http operations
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import {ApiRequest} from '../network/ApiRequest';
import {AxiosPromise, AxiosRequestConfig, AxiosResponse} from 'axios';
import {Nullable} from '../type/Nullable';
import {BaseOp} from './BaseOp';
import {OperationError} from './OperationError';
import * as Mustache from 'mustache';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';
import {HttpUrl} from '../HttpUrl';
import {OperationErrorEnum} from './OperationErrorEnum';

export type TOpMethod = 'get' | 'post' | 'put' | 'delete';

export type IOpConfig = AxiosRequestConfig & {
    method?: TOpMethod;
    rootProperty?: string; // the path to the data. for more information, see https://lodash.com/docs/4.17.15#get
    successProperty?: string;
    successStatusCode?: number;
    disableErrorHandling?: boolean;
}

// parameters for rendering URLs in Mustache format
type UrlContextType = {
    [key: string]: any;
}

export type AxiosRequestConfigEx = AxiosRequestConfig & {disableErrorHandling: boolean};

export class BaseHttpOp extends BaseOp {
    protected config: IOpConfig;
    protected urlContext?: UrlContextType;
    protected baseURL: string;

    // default values if they are not specified in config
    private defaultConfig: any = {
        method: 'get',
        url: '',
        rootProperty: 'content',
        successProperty: 'success',
        successStatusCode: 200,
        disableErrorHandling: false,
        timeout: 0
    };

    // link to the promise returned by axios
    protected axiosPromise: Nullable<AxiosPromise>;

    constructor () {
        super();
    }

    /**
     * Redefined function query url Builder
     */
    protected buildUrl (): string {
        let url: string,
            baseURL: string = this.baseURL || '';

        const opConfig = this.getConfig(),
              urlContext = this.getUrlContext();

        if (typeof opConfig.url === 'string') {
            url = opConfig.url;

            if (Object.keys(urlContext).length) {
                url = Mustache.render(url, urlContext);
            }

            return HttpUrl.buildUrl(baseURL, url);
        }

        return HttpUrl.buildUrl(baseURL, this.defaultConfig.url);
    }

    protected getConfig (): IOpConfig {
        return this.config;
    }

    protected getUrlContext (): UrlContextType {
        return this.urlContext || {};
    }

    /**
     * Returns the name of the property where the expected data is stored
     */
    protected getRootProperty (): string {
        const opConfig = this.getConfig();

        if (typeof opConfig.rootProperty === 'string') {
            return opConfig.rootProperty;
        }

        return this.defaultConfig.rootProperty;
    }

    /**
     * Returns the name of the property that contains the request success flag
     */
    protected getSuccessProperty (): string {
        const opConfig = this.getConfig();

        if (typeof opConfig.successProperty === 'string') {
            return opConfig.successProperty;
        }

        return this.defaultConfig.successProperty;
    }

    /**
     * Returns the response code that is considered successful
     */
    protected getSuccessStatusCode (): number {
        const opConfig = this.getConfig();

        if (typeof opConfig.successStatusCode === 'number') {
            return opConfig.successStatusCode;
        }

        return this.defaultConfig.successStatusCode;
    }

    /**
     * Returns the prepared config for the request via Axios
     */
    protected getRequestConfig (): AxiosRequestConfig {
        let opConfig: any = cloneDeep(this.getConfig()),
            opDefaultConfig: any = this.defaultConfig;

        // applying default configs
        Object.keys(opDefaultConfig).forEach(function (key) {
            if (opConfig && typeof opConfig[key] === 'undefined') {
                opConfig[key] = opDefaultConfig[key];
            }
        });

        opConfig.url = this.buildUrl();

        return opConfig;
    }

    /**
     * Launches the execution request
     */
    public execute (): Promise<any> {
        let me = this,
            requestConfig,
            promise;

        requestConfig = this.getRequestConfig();

        this.axiosPromise = ApiRequest.request(requestConfig);

        promise = this.axiosPromise
            .then(
                function (value: AxiosResponse) {
                    return me.success(value);
                },
                function (value) {
                    return me.failure(value);
                }
            );

        return promise;
    }

    /**
     * Returns true if the data has a success flag
     *
     * @param data
     */
    protected isSuccessRequestData (data: any): boolean {
        const successProperty = this.getSuccessProperty();

        // if you specified successProperty but there is no data the result is automatically incorrect
        if (!data || typeof data !== 'object') {
            return false;
        }

        // we interpret the absence of successProperty in the data as a successful result because otherwise we will end up in the failure handler
        if (data[successProperty] === false || data[successProperty] === 'false') {
            return false;
        }

        return true;
    }

    /**
     * Handler the successful completion of the request
     *
     * @param value
     */
    protected success (value: AxiosResponse): Promise<any> {
        let data: any;
        let result: any;

        if (this.responseDataValid(value)) {
            data = this.getOperationResponseData(value);
            result = this.processOperationData(data);

            return Promise.resolve(result);
        }

        return Promise.reject(new OperationError(OperationErrorEnum.FAILURE_FROM_SUCCESS, undefined, value));
    }

    /**
     * The handler is NOT request is successfully completed
     *
     * @param value
     */
    protected failure (value: AxiosResponse): Promise<any> {
        return Promise.reject(new OperationError(OperationErrorEnum.FAILURE, undefined, value));
    }

    /**
     * Returns true if the data is valid and applicable to the successful result of the operation
     *
     * @param value
     */
    protected responseDataValid (value: AxiosResponse): boolean {
        const successProperty = this.getSuccessProperty();

        let success: boolean = true;

        if (successProperty) {
            success = this.isSuccessRequestData(value.data);
        }

        if (value && value.status === this.getSuccessStatusCode() && success) {
            return true;
        }

        return false;
    }

    /**
     * Processing data received in the operation
     *
     * @param data
     */
    protected processOperationData (data: any): any {
        return data;
    }

    /**
     * Returns data according to the operation settings from the query result
     *
     * @param value
     */
    protected getOperationResponseData (value: AxiosResponse): any {
        const rootProperty = this.getRootProperty();

        let data: any = null;

        if (value && value.data) {
            data = rootProperty ? get(value.data, rootProperty) : value.data;
        }

        return data;
    }
}
