
/**
 * Tests for BaseHttpOp
 *
 * @author: Denis Makarov
 * @date: 2019-08-23
 */

import {BaseHttpOp} from './BaseHttpOp';
import {ApiRequest} from '../network/ApiRequest';
import {TestOp} from '../../tests/mocks/TestOp';
import {HttpAxiosHelper} from '../HttpAxiosHelper';

let successResponse = {
    data: {
        id: 1,
        name: 'Bow',
        cost: 140
    },
    status: 200,
    statusText: 'success'
};

let failureResponse = {
    status: 500,
    statusText: 'failure'
};

describe('BaseHttpOp', () => {
    describe('#execute() ', () => {

        it('creates model', () => {
            const mockStaticF = jest.fn();

            mockStaticF.mockReturnValue(new Promise((resolve) => {resolve(successResponse);}));
            ApiRequest.request = mockStaticF.bind(ApiRequest);

            let testOp = new TestOp();

            return testOp.execute().then(data => {
                expect(data.name.getValue()).toEqual('Bow');
                expect(data.cost.getValue()).toEqual(140);
                expect(data.id.getValue()).toEqual(1);
            });
        });

        it('rejects Promise', () => {
            const mockStaticF = jest.fn();

            mockStaticF.mockReturnValue(new Promise((resolve, reject) => {reject(successResponse);}));
            ApiRequest.request = mockStaticF.bind(ApiRequest);

            let testOp = new TestOp();

            return expect(testOp.execute()).rejects.toMatchObject({message: 'failure'});
        });

        it('fails in success', () => {
            const mockStaticF = jest.fn();

            mockStaticF.mockReturnValue(new Promise((resolve) => {resolve(failureResponse);}));
            ApiRequest.request = mockStaticF.bind(ApiRequest);

            let testOp = new TestOp();

            return expect(testOp.execute()).rejects.toMatchObject({message: 'failure from success'});
        });

        it('calls transformAxiosRequestConfig', () => {
            const mockStaticF = jest.fn();

            mockStaticF.mockReturnValue(new Promise((resolve) => {resolve(successResponse);}));
            // eslint-disable-next-line no-undef
            let spy = spyOn(HttpAxiosHelper, 'transformAxiosRequestConfig').and.callThrough();

            ApiRequest.request = mockStaticF.bind(ApiRequest);

            let testOp = new TestOp();

            testOp.execute();

            expect(spy).toHaveBeenCalledTimes(1);
        });

    });
});
