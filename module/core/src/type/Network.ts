/**
 * Exporting types from axios that are used in other modules
 *
 * @author Brauer Ilya
 *
 * @date 2020-05-22
 */

import {AxiosError, AxiosResponse} from 'axios';

export {
    AxiosError, AxiosResponse
};
