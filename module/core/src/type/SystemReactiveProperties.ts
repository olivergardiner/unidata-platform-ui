/**
 * Type of system values that can be made reactive
 *
 * @author Ilya Brauer
 * @date 2019-11-20
 */

export enum ReactiveProp {
    DIRTY = 'dirty',
    ORIGIN_VALUE = 'originValue',
    DEFAULT_VALUE = 'defaultValue',
    LAST_VALIDATION_RESULT = 'lastValidationResult',
    DEEP_DIRTY = 'deepDirty',
    MODEL_DIRTY = 'modelDirty',
    COLLECTION_DIRTY = 'collectionDirty'
}
