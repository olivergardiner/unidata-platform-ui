/**
 * Type of map whose keys can only be strings
 *
 * @author Ivan Marshalkin
 * @date 2018-08-20
 */
export interface IStringKeyMap<T = any> {
    [key: string]: T;
}
