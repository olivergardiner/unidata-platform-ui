/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2018-08-20
 */

/**
 * Type of constructor. This makes it easier to describe the type of argument for a function.
 *
 * Example:
 *
 * instead
 * function (ctor: { new (...args: any[]): StringField }) { ... }
 *
 * can be described
 * function (ctor: ClassCtor<StringField>) { ... }
 *
 * @author Ivan Marshalkin
 * @date 2018-08-20
 */
export type ClassCtor<T> = {
    new (...args: any[]): T;
};
