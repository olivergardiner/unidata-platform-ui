﻿/**
 *  Interface to the business module
 *
 * @author Sergey Shishigin
 * @date 2019-12-23
 */
import {UeModuleBase} from '../userexit/type/UeModuleBase';

export interface IModule {
    id: string;
    uemodules: UeModuleBase[];
}
