/**
 * The type of value that can store null
 *
 * Example:
 *
 * let str: string | null = null;
 *
 * can be written as
 *
 * let str: Nullable<string> = null;
 *
 * @author Ivan Marshalkin
 * @date 2018-08-28
 */

export type Nullable<T> = T | null;
