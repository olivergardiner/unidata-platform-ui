/**
 * Auxiliary class for working with URLS
 *
 * @author Ivan Marshalkin
 * @date 2017-12-16
 */

export class HttpUrl {
    /**
     * Removes slashes to the right in the url
     *
     * @param url
     */
    public static removeSlashRight (url: string): string {
        url = url.replace(/(\/|\\)+$/, '');

        return url;
    }

    /**
     * Removes slashes on the left in the url
     *
     * @param url
     */
    public static removeSlashLeft (url: string): string {
        url = url.replace(/^(\/|\\)+/, '');

        return url;
    }

    public static buildUrl (baseUrl: string = '', postUrl: string = ''): string {
        let url: string;

        url = HttpUrl.removeSlashRight(baseUrl) + '/' + HttpUrl.removeSlashLeft(postUrl);

        return url;
    }
}
