/**
 * Interceptors of responses to requests sent using axios
 * Presumably, implementation data should not be used directly, only through the wrapper.
 *
 * @author Ivan Marshalkin
 * @date 2019-08-23
 */

import {AxiosResponse} from 'axios';
import {AxiosInterceptorBase} from './AxiosInterceptorBase';

export class AxiosResponseInterceptor extends AxiosInterceptorBase<AxiosResponse> {
    public initInterceptor () {
        this.cancelInterceptor();

        // Adding an interceptor for responses
        // The handler can change the data that will be passed to subscribers in the future
        // Example:
        // axios.interceptors.response.use(
        //     function (response) {
        //         // Do something with response data
        //         return response
        //     },
        //     function (error) {
        //         // Do something with response error
        //         return Promise.reject(error);
        //     });

        this.interceptorId = this.axiosInstance.interceptors.response.use(
            this.onComplete,    // the handler is called when the request is completed
            this.onReject       // the handler is called if the request failed with an error (for example, the response code 500)
        );
    }

    public cancelInterceptor () {
        const interceptorId = this.interceptorId;

        if (interceptorId !== null) {
            this.axiosInstance.interceptors.response.eject(interceptorId);
            this.interceptorId = null;
        }
    }
}
