/**
 * Basic interceptor class for axios
 *
 * @author Ivan Marshalkin
 * @date 2019-08-23
 */

import Axios, {AxiosInstance, AxiosStatic} from 'axios';
import {Nullable} from '../../type/Nullable';

type Fullfiled<T> = (value: T) => T | Promise<T>;
type Rejected = (error: any) => any;
type onCompleteCallback<T> = Fullfiled<T> | undefined;
type onRejectCallback = Rejected | undefined;

export type AxiosInterceptors<T> = {
    onComplete?: onCompleteCallback<T>;
    onReject?: onRejectCallback;
}

export abstract class AxiosInterceptorBase <T> {
    protected axiosInstance: AxiosStatic | AxiosInstance;

    protected interceptorId: Nullable<number>;

    protected onComplete: onCompleteCallback<T>;
    protected onReject: onRejectCallback;

    constructor (axiosInstance: Nullable<AxiosInstance> = null, interceptors: AxiosInterceptors<T>) {
        this.axiosInstance = axiosInstance ? axiosInstance : Axios;

        if (interceptors.onComplete) {
            this.onComplete = interceptors.onComplete;
        }

        if (interceptors.onReject) {
            this.onReject = interceptors.onReject;
        }

        this.initInterceptor();
    }

    public abstract initInterceptor (): void;
    public abstract cancelInterceptor (): void;
}
