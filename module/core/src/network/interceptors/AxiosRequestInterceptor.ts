/**
 * Interceptors of requests sent using axios
 * Presumably, implementation data should not be used directly, only through the wrapper.
 *
 * @author Ivan Marshalkin
 * @date 2019-08-23
 */

import {AxiosRequestConfig} from 'axios';
import {AxiosInterceptorBase} from './AxiosInterceptorBase';

export class AxiosRequestInterceptor extends AxiosInterceptorBase<AxiosRequestConfig> {
    public initInterceptor () {
        this.cancelInterceptor();

        // Adding an interceptor for requests
        // The handler can change the data that will be passed to subscribers in the future
        // Example:
        // axios.interceptors.request.use(
        //     function (config) {
        //         // Do something before request is sent
        //         return config;
        //     },
        //     function (error) {
        //         // Do something with request error
        //         return Promise.reject(error);
        //     });

        this.interceptorId = this.axiosInstance.interceptors.request.use(
            this.onComplete,    // the handler is called before sending the request, you can modify the config query
            this.onReject       // failed to set when called, presumably only if the request is canceled via token
        );
    }

    public cancelInterceptor () {
        const interceptorId = this.interceptorId;

        if (interceptorId !== null) {
            this.axiosInstance.interceptors.request.eject(interceptorId);
            this.interceptorId = null;
        }
    }
}
