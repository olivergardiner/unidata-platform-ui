/**
 * Auxiliary class for sending ajax requests. The axios library is used
 *
 * @author Ivan Marshalkin
 * @date 2019-08-22
 */

import Axios, {AxiosPromise, AxiosRequestConfig} from 'axios';

export class ApiRequest {
    /**
     * Sends a GET request
     *
     * @param config
     */
    public static get (config: AxiosRequestConfig): AxiosPromise {
        Object.assign({}, config, {
            method: 'get'
        });

        return ApiRequest.request(config);
    }

    /**
     * Sends a PUT request
     *
     * @param config
     */
    public static put (config: AxiosRequestConfig): AxiosPromise {
        Object.assign({}, config, {
            method: 'put'
        });

        return ApiRequest.request(config);
    }

    /**
     * Sends a POST request
     *
     * @param config
     */
    public static post (config: AxiosRequestConfig): AxiosPromise {
        Object.assign({}, config, {
            method: 'post'
        });

        return ApiRequest.request(config);
    }

    /**
     * Sends a DELETE request
     *
     * @param config
     */
    public static delete (config: AxiosRequestConfig): AxiosPromise {
        Object.assign({}, config, {
            method: 'delete'
        });

        return ApiRequest.request(config);
    }

    /**
     * Sends an arbitrary request request
     *
     * @param config
     */
    public static request (config: AxiosRequestConfig): AxiosPromise {
        return Axios(config);
    }
}
