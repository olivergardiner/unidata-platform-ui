import {ISignal, ISignalHandler, ISubscribable, SignalDispatcher} from 'strongly-typed-events';
import {SubscribeMapper} from './SubscribeMapper';
import {ISubscribeFunction} from './ISubscribeFunction';
import each from 'lodash/each';
import some from 'lodash/some';

/**
 * Signal channel. Combines the signal Manager and the signal object
 * from https://github.com/KeesCBakker/Strongly-Typed-Events-for-TypeScript
 *
 * @author Sergey Shishigin
 * @date 2018-08-21
 */
export class SignalChannel implements ISubscribable<ISignalHandler> {
    private readonly dispatcher: SignalDispatcher;
    private subscribeMapper: SubscribeMapper<ISignalHandler>;
    public readonly count: number;

    constructor () {
        this.dispatcher = new SignalDispatcher();
        this.subscribeMapper = new SubscribeMapper();
    }

    public getDispatcher () {
        return this.dispatcher;
    }

    private getEvent (): ISignal {
        return this.dispatcher.asEvent();
    }

    /**
     * The underlying methods are frontend for Event
     */

    /**
     * Subscribe to the event.
     * @param fn The event handler that is called when the event is dispatched.
     * @returns A function that unsubscribes the event handler from the event.
     */
    public subscribe (fn: ISignalHandler, scope?: any): () => void {
        let handleFunction: ISubscribeFunction<ISignalHandler>;

        handleFunction = this.subscribeMapper.buildAndAddSubscribeFunction(fn, scope);

        return this.getEvent().subscribe(handleFunction.bindFn);
    }

    /**
     * Subscribe to the event.
     * @param fn The event handler that is called when the event is dispatched.
     * @returns A function that unsubscribes the event handler from the event.
     */
    public sub (fn: ISignalHandler, scope?: any): () => void {
        return this.subscribe(fn, scope);
    }

    /**
     * Unsubscribe from the event.
     * @param fn The event handler that will be unsubsribed from the event.
     */
    public unsubscribe (fn: ISignalHandler, scope?: any): void {
        let bindFns: ISignalHandler[],
            event = this.getEvent();

        bindFns = this.subscribeMapper.getBindFunctions(fn, scope);

        each(bindFns, (bindFn) => { event.unsubscribe(bindFn); });

        this.subscribeMapper.removeSubscribeFunctions(fn, scope);
    }

    /**
     * Unsubscribe from the event.
     * @param fn The event handler that will be unsubsribed from the event.
     */
    public unsub (fn: ISignalHandler, scope?: any): void {
        return this.unsubscribe(fn, scope);
    }
    /**
     * Subscribes to the event only once.
     * @param fn The event handler that is called when the event is dispatched.
     * @returns A function that unsubscribes the event handler from the event.
     */
    public one (fn: ISignalHandler, scope?: any): () => void {
        return this.getEvent().one(fn.bind(scope));
    }

    /**
     * Checks it the event has a subscription for the specified handler.
     * @param fn The event handler.
     */
    public has (fn: ISignalHandler, scope?: any): boolean {
        let bindFuncs: ISignalHandler[];

        bindFuncs = this.subscribeMapper.getBindFunctions(fn, scope);

        return some(bindFuncs, (bindFn) => {
            return this.getEvent().has(bindFn);
        });
    }

    /**
     * Clears all the subscriptions.
     */
    public clear (): void {
        this.getEvent().clear();
        this.subscribeMapper.clearSubscribeList();
    }
}
