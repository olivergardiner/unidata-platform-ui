import chain from 'lodash/chain';
import remove from 'lodash/remove';
import {ISubscribeFunction} from './ISubscribeFunction';

/**
 * Storage of objects with information about mapping event handlers
 *
 * @author Sergey Shishigin
 * @date 2018-09-04
 */
export class SubscribeMapper<T extends Function> {
    /**
     * List of objects with information about the event handler
     */
    private subscribeList: Array<ISubscribeFunction<T>>;

    constructor () {
        this.subscribeList = [];
    }

    /**
     * Create and add an object with information about the event handler
     *
     * @param {Function} fn
     * @param {any | undefined | null} scope
     * @return {ISubscribeFunction}
     */
    public buildAndAddSubscribeFunction (fn: T, scope: any): ISubscribeFunction<T> {
        let handleFunction: ISubscribeFunction<T>,
            bindFn;

        bindFn = fn.bind(scope);

        handleFunction = {
            fn: fn,
            scope: scope,
            bindFn: bindFn
        };

        this.subscribeList.push(handleFunction);

        return handleFunction;
    }

    /**
     * Get an array of binding handler functions
     *
     * @param {Function} fn
     * @param scope
     * @return {Function[]}
     */
    public getBindFunctions (fn: T, scope: any): T[] {
        return chain(this.subscribeList)
            .filter((handleFunction) => {return handleFunction.fn === fn && handleFunction.scope === scope; })
            .map('bindFn')
            .value();
    }

    /**
     * Delete an array of objects with information about the event handler
     *
     * @param {Function} fn Original function
     * @param scope Context
     */
    public removeSubscribeFunctions (fn: T, scope: any): void {
        remove(this.subscribeList, function (handleFunction) {
           return handleFunction.fn === fn && handleFunction.scope === scope;
        });
    }

    public clearSubscribeList () {
        this.subscribeList.splice(0, this.subscribeList.length);
    }
}
