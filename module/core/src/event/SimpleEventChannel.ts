import {ISimpleEvent, ISimpleEventHandler, ISubscribable, SimpleEventDispatcher} from 'strongly-typed-events';
import {SubscribeMapper} from './SubscribeMapper';
import {ISubscribeFunction} from './ISubscribeFunction';
import each from 'lodash/each';
import some from 'lodash/some';

/**
 * Simple events channel. Combines a simple event and a simple event object
 * from https://github.com/KeesCBakker/Strongly-Typed-Events-for-TypeScript
 *
 * @author Sergey Shishigin
 * @date 2018-08-21
 */
export class SimpleEventChannel<TArgs> implements ISubscribable<ISimpleEventHandler<TArgs>> {
    private readonly dispatcher: SimpleEventDispatcher<TArgs>;
    private subscribeMapper: SubscribeMapper<ISimpleEventHandler<TArgs>>;
    public readonly count: number;

    constructor () {
        this.dispatcher = new SimpleEventDispatcher<TArgs>();
        this.subscribeMapper = new SubscribeMapper();
    }

    public getDispatcher () {
        return this.dispatcher;
    }

    private getEvent (): ISimpleEvent<TArgs> {
        return this.dispatcher.asEvent();
    }

    /**
     * Subscribe to the event.
     * @param fn The event handler that is called when the event is dispatched.
     * @returns A function that unsubscribes the event handler from the event.
     */
    public subscribe (fn: ISimpleEventHandler<TArgs>, scope?: any): () => void {
        let handleFunction: ISubscribeFunction<ISimpleEventHandler<TArgs>>;

        handleFunction = this.subscribeMapper.buildAndAddSubscribeFunction(fn, scope);

        return this.getEvent().subscribe(handleFunction.bindFn);
    }

    /**
     * Subscribe to the event.
     * @param fn The event handler that is called when the event is dispatched.
     * @returns A function that unsubscribes the event handler from the event.
     */
    public sub (fn: ISimpleEventHandler<TArgs>, scope?: any): () => void {
        return this.subscribe(fn, scope);
    }

    /**
     * Unsubscribe from the event.
     * @param fn The event handler that will be unsubsribed from the event.
     */
    public unsubscribe (fn: ISimpleEventHandler<TArgs>, scope?: any): void {
        let bindFns: Array<ISimpleEventHandler<TArgs>>,
            event = this.getEvent();

        bindFns = this.subscribeMapper.getBindFunctions(fn, scope);

        each(bindFns, (bindFn) => { event.unsubscribe(bindFn); });

        this.subscribeMapper.removeSubscribeFunctions(fn, scope);
    }

    /**
     * The underlying methods are frontend for Event
     */

    /**
     * Unsubscribe from the event.
     * @param fn The event handler that will be unsubsribed from the event.
     */
    public unsub (fn: ISimpleEventHandler<TArgs>, scope?: any): void {
        return this.unsubscribe(fn, scope);
    }
    /**
     * Subscribes to the event only once.
     * @param fn The event handler that is called when the event is dispatched.
     * @returns A function that unsubscribes the event handler from the event.
     */
    public one (fn: ISimpleEventHandler<TArgs>, scope?: any): () => void {
        return this.getEvent().one(fn.bind(scope));
    }

    /**
     * Checks it the event has a subscription for the specified handler.
     * @param fn The event handler.
     */
    public has (fn: ISimpleEventHandler<TArgs>, scope?: any): boolean {
        let bindFuncs: Array<ISimpleEventHandler<TArgs>>;

        bindFuncs = this.subscribeMapper.getBindFunctions(fn, scope);

        return some(bindFuncs, (bindFn) => {
            return this.getEvent().has(bindFn);
        });
    }

    /**
     * Clears all the subscriptions.
     */
    public clear (): void {
        this.getEvent().clear();
        this.subscribeMapper.clearSubscribeList();
    }
}
