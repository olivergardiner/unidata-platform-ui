import {EventDispatcher, IEvent, IEventHandler, ISubscribable} from 'strongly-typed-events';
import {SubscribeMapper} from './SubscribeMapper';
import each from 'lodash/each';
import some from 'lodash/some';
import {ISubscribeFunction} from './ISubscribeFunction';

/**
 * Event channel. Combines the event Manager and the event object
 * from https://github.com/KeesCBakker/Strongly-Typed-Events-for-TypeScript
 *
 * @author Sergey Shishigin
 * @date 2018-08-21
 */
export class EventChannel <TSender, TArgs> implements ISubscribable<IEventHandler<TSender, TArgs>> {
    private readonly dispatcher: EventDispatcher<TSender, TArgs>;
    private subscribeMapper: SubscribeMapper<IEventHandler<TSender, TArgs>>;
    public readonly count: number;

    constructor () {
        this.dispatcher = new EventDispatcher<TSender, TArgs>();
        this.subscribeMapper = new SubscribeMapper();
    }

    public getDispatcher () {
        return this.dispatcher;
    }

    private getEvent (): IEvent<TSender, TArgs> {
        return this.dispatcher.asEvent();
    }

    /**
     * The underlying methods are frontend for Event
     */

    /**
     * Subscribe to the event.
     * @param fn The event handler that is called when the event is dispatched.
     * @param scope
     * @returns A function that unsubscribes the event handler from the event.
     */
    public subscribe (fn: IEventHandler<TSender, TArgs>, scope?: any): () => void {
        let handleFunction: ISubscribeFunction<IEventHandler<TSender, TArgs>>;

        handleFunction = this.subscribeMapper.buildAndAddSubscribeFunction(fn, scope);

        return this.getEvent().subscribe(handleFunction.bindFn);
    }

    /**
     * Subscribe to the event.
     * @param fn The event handler that is called when the event is dispatched.
     * @param scope
     * @returns A function that unsubscribes the event handler from the event.
     */
    public sub (fn: IEventHandler<TSender, TArgs>, scope?: any): () => void {
        return this.subscribe(fn, scope);
    }

    /**
     * Unsubscribe from the event.
     * @param fn The event handler that will be unsubsribed from the event.
     * @param scope
     */
    public unsubscribe (fn: IEventHandler<TSender, TArgs>, scope?: any): void {
        let bindFns: Array<IEventHandler<TSender, TArgs>>,
            event = this.getEvent();

        bindFns = this.subscribeMapper.getBindFunctions(fn, scope);

        each(bindFns, (bindFn) => { event.unsubscribe(bindFn); });

        this.subscribeMapper.removeSubscribeFunctions(fn, scope);
    }

    /**
     * Unsubscribe from the event.
     * @param fn The event handler that will be unsubsribed from the event.
     */
    public unsub (fn: IEventHandler<TSender, TArgs>, scope?: any): void {
        return this.unsubscribe(fn, scope);
    }

    /**
     * Subscribes to the event only once.
     * @param fn The event handler that is called when the event is dispatched.
     * @returns A function that unsubscribes the event handler from the event.
     */
    public one (fn: IEventHandler<TSender, TArgs>, scope?: any): () => void {
        return this.getEvent().one(fn.bind(scope));
    }

    /**
     * Checks it the event has a subscription for the specified handler.
     * @param fn The event handler.
     */
    public has (fn: IEventHandler<TSender, TArgs>, scope?: any): boolean {
        let bindFuncs: Array<IEventHandler<TSender, TArgs>>;

        bindFuncs = this.subscribeMapper.getBindFunctions(fn, scope);

        return some(bindFuncs, (bindFn) => {
            return this.getEvent().has(bindFn);
        });
    }

    /**
     * Clears all the subscriptions.
     */
    public clear (): void {
        this.getEvent().clear();
        this.subscribeMapper.clearSubscribeList();
    }
}
