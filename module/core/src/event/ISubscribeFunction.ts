/**
 * Information about the event handler
 *
 * @author Sergey Shishigin
 * @date 2018-09-04
 */
export interface ISubscribeFunction<T> {
    fn: T;
    scope?: any;
    bindFn: T;
}
