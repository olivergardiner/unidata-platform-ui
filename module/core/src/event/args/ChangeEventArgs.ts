/**
 * Arguments for the change event
 *
 * @author Sergey Shishigin
 * @date 2018-08-21
 */
export class ChangeEventArgs<T> {
    private readonly internalOldValue: T;
    private readonly internalValue: T;

    constructor (value: T, oldValue: T) {
        this.internalOldValue = oldValue;
        this.internalValue = value;
    }

    public get oldValue (): T {
        return this.internalOldValue;
    }

    public get value (): T {
        return this.internalValue;
    }
}
