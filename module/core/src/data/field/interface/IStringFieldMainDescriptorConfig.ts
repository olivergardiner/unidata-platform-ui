import {IBaseMainDescriptorConfig} from '../../interface/IBaseMainDescriptorConfig';

/**
 * Config interface for a field with a string type for the main decorator
 *
 * @author Denis Makarov
 * @date 2018-08-14
 */
export interface IStringFieldMainDescriptorConfig extends IBaseMainDescriptorConfig {

}
