import {IBaseMainDescriptorConfig} from '../../interface/IBaseMainDescriptorConfig';

/**
 * Config interface for an integer type field for the main decorator
 *
 * @author Denis Makarov
 * @date 2018-08-14
 */
export interface IIntegerFieldMainDescriptorConfig extends IBaseMainDescriptorConfig {

}
