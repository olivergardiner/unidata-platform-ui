import {IBaseMainDescriptorConfig} from '../../interface/IBaseMainDescriptorConfig';

/**
 * Config interface for a logical type field for the main decorator
 *
 * @author Denis Makarov
 * @date 2018-08-14
 */
export interface IBooleanFieldMainDescriptorConfig extends IBaseMainDescriptorConfig {

}
