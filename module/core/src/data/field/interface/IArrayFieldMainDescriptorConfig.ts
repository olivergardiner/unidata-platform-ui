import {IBaseMainDescriptorConfig} from '../../interface/IBaseMainDescriptorConfig';

/**
 * Config interface for an array type field for the main decorator
 *
 * @author Denis Makarov
 * @date 2018-08-29
 */
export interface IArrayFieldMainDescriptorConfig extends IBaseMainDescriptorConfig {

}
