import {IBaseMainDescriptorConfig} from '../../interface/IBaseMainDescriptorConfig';

/**
 * Config interface for a numeric type field for the main decorator
 *
 * @author Denis Makarov
 * @date 2018-08-14
 */
export interface INumberFieldMainDescriptorConfig extends IBaseMainDescriptorConfig {

}
