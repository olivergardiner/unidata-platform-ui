import {AbstractField} from './AbstractField';

/**
 * Model field of the Date type
 *
 * @author Sergey Shishigin
 * @date 2018-08-20
 */
export class DateField extends AbstractField<Date> {
    /**
     * Comparison function for two dates
     *
     * @param {Date} a
     * @param {Date} b
     * @return {number} Returns one of the values: 0, -1, 1
     */
    public compare (a: Date, b: Date): number {
        let aIsDate: boolean,
            bIsDate: boolean,
            result: number;

        aIsDate = a instanceof Date;
        bIsDate = b instanceof Date;

        if (bIsDate && aIsDate) {
            result = a.getTime() - b.getTime();

            if (result === 0) {
                result = 0;
            } else {
                result = result < 0 ? -1 : 1;
            }
        } else if (aIsDate === bIsDate) {
            result = 0;
        } else {
            result = aIsDate ? 1 : -1;
        }

        return result;
    }
}
