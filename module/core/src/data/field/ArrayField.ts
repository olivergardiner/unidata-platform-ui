/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2018-09-13
 */

import {AbstractField} from './AbstractField';
import cloneDeep from 'lodash/cloneDeep';
import isEqual from 'lodash/isEqual';
import {action, IObservableArray, isObservable, isObservableArray, observable, toJS} from 'mobx';

export class ArrayField extends AbstractField<any | undefined> {

    // ToDo come up with a better way
    public valueEquals (a: IObservableArray<any>, b: any[]): boolean {
        if (a === b) {
            return true;
        }

        if (!a || !isObservableArray(a)) {
            return false;
        }

        if (!b) {
            return false;
        }

        if (isObservableArray(b)) {
            return isEqual(toJS(a).sort(), toJS(b).sort());
        }

        // TODO: figure out how to use sort correctly
        // It is proposed to make ArrayPrimitiveField and Array Object Field
        return isEqual(toJS(a).sort(), b.sort());
    }

    @action /** Don't delete action. The originalValue property can be made reactive in runtime */
    public commit (): void {
        if (this.value === undefined) {
            this.originalValue = undefined;
        }

        if (this.value === null) {
            this.originalValue = null;
        } else if (isObservableArray(this.originalValue)) {
            // TODO: it is not clear why we use deep: false, because the following problem will remain
            // array elements will remain copied only by reference and will always be equal for value and originalValue
            // therefore it will not be possible to track changes in the interior of elements
            this.originalValue = observable.array(cloneDeep(this.value.slice()), {deep: false});
        } else {
            // this.value observableArray, so you can't just clone it
            this.originalValue = this.value.map((valueItem: any) => {
                let item;

                if (isObservable(valueItem)) {
                    item = toJS(valueItem);
                } else {
                    item = valueItem;
                }

                return cloneDeep(item);
            });
        }

        this.updateDirty();
        this.clearValidation();
    }
}
