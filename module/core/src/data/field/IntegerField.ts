import {AbstractField} from './AbstractField';
import {action} from 'mobx';

/**
 * Model field of type integer
 *
 * @author Ivan Marshalkin
 * @date 2018-08-16
 */
export class IntegerField extends AbstractField<number> {
    // @action
    public setValue (value: any, silent?: boolean): void {
        let newValue = Number(value);

        if (!Number.isInteger(newValue)) {
            return;
        }

        super.setValue(newValue, silent);
    }
}
