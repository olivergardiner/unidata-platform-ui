/**
 * Any type of model field
 *
 * Any value can be stored in this field: primitive, Object, Array
 *
 * So far we believe that the array elements of the Object type are not reactive
 *
 * @author Sergey Shishigin
 * @date 2019-08-22
 */
import {AbstractField} from './AbstractField';

export class AnyField<T = any> extends AbstractField<T> {}
