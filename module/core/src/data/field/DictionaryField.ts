/**
 * Dictionary type model field
 *
 * @author Brauer Ilya
 * @date 2020-06-23
 */
import {AbstractField} from './AbstractField';

export class DictionaryField extends AbstractField<string> {
    public delimiter = '=>>';
}
