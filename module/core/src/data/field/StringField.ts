/**
 * String type model field
 *
 * @author Ivan Marshalkin
 * @date 2018-08-16
 */
import {AbstractField} from './AbstractField';

export class StringField extends AbstractField<string> {
}
