import {action, observable, toJS, observe, isObservable} from 'mobx';
import {IStringKeyMap} from '../../type/IStringKeyMap';
import cloneDeep from 'lodash/cloneDeep';
import {IFieldValidator} from '../validation/IFieldValidator';
import {BaseData} from '../BaseData';

/**
 * Abstract data model field
 *
 * @author Ivan Marshalkin
 * @author Denis Makarov
 * @date 2018-08-14
 */
export class AbstractField<T> extends BaseData {
    @observable
    protected value: T;

    protected originalValue: T;

    private defaultValue: T;

    // TODO: implement logic for allowNull
    private allowNull: boolean = false;

    public persist: boolean = true;

    public required: boolean = false;

    public validators: IFieldValidator[] = [];

    public lastValidationResult: string[] = [];

    protected dirty: boolean = false;

    constructor (value: T, onChangeDirty?: () => void, descriptorConfig?: IStringKeyMap) {
        super();

        if (descriptorConfig) {
            if (value === undefined && descriptorConfig.defaultValue !== undefined) {
                value = descriptorConfig.defaultValue;
                this.setDefaultValue(value);
            }

            if (Boolean(descriptorConfig.required)) {
                this.required = true;
            }
        }

        if (onChangeDirty) {
            this.onChangeDirty = onChangeDirty;
        }

        this.setOriginalValue(value, true);
        this.setValue(value, true);
    }

    private onChangeDirty = () => {}; // The method is redefined in the constructor

    public serialize (config?: any): any {
        // We use toJS for serialization. Observable objects and arrays is converted to JS objects and arrays
        return toJS(this.value);
    }

    @action
    public setValue (value: T | null, silent?: boolean): void {
        // ToDo implement normal logic for null
        this.value = value!;

        if (!silent) {
            this.updateDirty();
        }
    }

    @action /** Don't delete action. The originalValue property can be made reactive in runtime */
    public setOriginalValue (value: T, silent?: boolean): void {
        this.originalValue = cloneDeep(value);

        if (!silent) {
            this.updateDirty();
        }
    }

    @action /** Don't delete action. The defaultValue property can be made reactive in runtime */
    public setDefaultValue (value: T): void {
        this.defaultValue = cloneDeep(value);
    }

    public getValue (): T {
        // TODO: return a clone or reference for the object case (date, ...)
        return this.value;
    }

    public getOriginalValue (): T {
        // TODO: return a clone or reference for the object case (date, ...)
        return this.originalValue;
    }

    /**
     * Returns true if the current value is different from the original value
     */
    protected isModified (): boolean {
        return !this.valueEquals(this.value, this.originalValue);
    }

    public valueEquals (a: T, b: T): boolean {
        return this.compare(a, b) === 0;
    }

    @action /** Don't delete action. The lastValidationResult property can be made reactive in runtime */
    public validate (): string[] {
        let validationErrors: string[] = [],
            message,
            me = this;

        this.validators.forEach(function (validator: IFieldValidator) {
            let validateFn = validator.validate || validator.rule.validate;

            if (!validateFn({value: me.getValue(), model: validator.model, args: validator.args})) {
                message = validator.message || validator.rule.defaultMessage;
                validationErrors.push(message);
            }
        });

        this.lastValidationResult = validationErrors;

        return validationErrors;
    }

    public addValidator (validator: IFieldValidator) {
        this.validators.push(validator);
    }

    @action /** Don't delete action. The lastValidationResult property can be made reactive in runtime */
    public clearValidation () {
        this.lastValidationResult.length = 0;
    }

    /**
     * Function for comparing elements of type T
     */
    public compare (a: T, b: T): number {
        let result: number;

        // the first is an equality comparison
        // this is necessary if one of the arguments is undefined: in this case, result = 1
        if (a === b) {
            result = 0;
        } else if (a < b) {
            result = -1;
        } else {
            result = 1;
        }

        return result;
    }

    @action /** Don't delete action. The originalValue property can be made reactive in runtime */
    public commit (): void {
        if (isObservable(this.originalValue)) {
            this.originalValue = cloneDeep(this.value);
        } else {
            this.originalValue = cloneDeep(toJS(this.value));
        }

        this.updateDirty();
        this.clearValidation();
    }

    @action
    public revert (): void {
        this.value =  cloneDeep(this.originalValue);

        this.updateDirty();
        this.clearValidation();
    }

    @action /** Don't delete action. The dirty property can be made reactive in runtime */
    public updateDirty (): void {
        this.dirty = this.isModified();
        this.onChangeDirty();
    }

    public getDirty (): boolean {
        return this.dirty;
    }

    public bind (field: AbstractField<T>) {
        observe(this as any,  'value', function (change) {
            field.setValue(change.newValue);
        });
    }
}
