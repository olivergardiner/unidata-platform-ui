import {AbstractField} from './AbstractField';

/**
 * Boolean model field
 *
 * @author Ivan Marshalkin
 * @date 2018-08-16
 */
export class BooleanField extends AbstractField<boolean> {
}
