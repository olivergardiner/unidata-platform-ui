/**
 * Enum type model field
 *
 * @author Sergey Shishigin
 * @date 2018-09-21
 */
import {AbstractField} from './AbstractField';

export class EnumField<T> extends AbstractField<T> {
}
