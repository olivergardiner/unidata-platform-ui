import {AbstractField} from './AbstractField';

/**
 * Model field of a numeric type
 *
 * @author Ivan Marshalkin
 * @date 2018-08-16
 */
export class NumberField extends AbstractField<number> {
}
