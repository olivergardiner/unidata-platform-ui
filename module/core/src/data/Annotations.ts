/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2018-09-13
 */

import {IHasOneMainDescriptorConfig} from './model/interface/IHasOneMainDescriptorConfig';
import {IHasManyMainDescriptorConfig} from './collection/interface/IHasManyMainDescriptorConfig';
import {IStringFieldMainDescriptorConfig} from './field/interface/IStringFieldMainDescriptorConfig';
import {INumberFieldMainDescriptorConfig} from './field/interface/INumberFieldMainDescriptorConfig';
import {IBooleanFieldMainDescriptorConfig} from './field/interface/IBooleanFieldMainDescriptorConfig';
import {IDateFieldMainDescriptorConfig} from './field/interface/IDateFieldMainDescriptorConfig';
import {ModelMetaData} from './model/ModelMetaData';
import {StringField} from './field/StringField';
import {NumberField} from './field/NumberField';
import {BooleanField} from './field/BooleanField';
import {DateField} from './field/DateField';
import {ModelCollection} from './collection/ModelCollection';
import {IntegerField} from './field/IntegerField';
import {IBaseJsonDescriptorConfig} from './interface/IBaseJsonDescriptorConfig';
import {ArrayField} from './field/ArrayField';
import {AnyField} from './field/AnyField';
import {IAnyFieldMainDescriptorConfig} from './field/interface/IAnyFieldMainDescriptorConfig';

/**
 * A set of annotations (decorators) used to define the data model
 *
 * @author Denis Makarov
 * @date 2018-08-16
 */

/**
 * Decorator for any field
 *
 * @param config
 */
export function anyField (config: IAnyFieldMainDescriptorConfig = {}): (target: any, key: string) => any {
    return ModelMetaData.setFieldMainDescriptor(AnyField, config);
}

/**
 * Decorator for a string field
 *
 * @param config
 */
export function stringField (config: IStringFieldMainDescriptorConfig = {}): (target: any, key: string) => any {
    return ModelMetaData.setFieldMainDescriptor(StringField, config);
}

/**
 * Decorator for a numeric field
 *
 * @param config
 */
export function numberField (config: INumberFieldMainDescriptorConfig = {}): (target: any, key: string) => any {
    return ModelMetaData.setFieldMainDescriptor(NumberField, config);
}

/**
 * Decorator for an integer field
 *
 * @param config
 */
export function integerField (config: INumberFieldMainDescriptorConfig = {}): (target: any, key: string) => any {
    return ModelMetaData.setFieldMainDescriptor(IntegerField, config);
}

/**
 * Decorator for the boolean type
 *
 * @param config
 */
export function booleanField (config: IBooleanFieldMainDescriptorConfig = {}): (target: any, key: string) => any {
    return ModelMetaData.setFieldMainDescriptor(BooleanField, config);
}

/**
 * Decorator for a field with the date type
 *
 * @param config
 */
export function dateField (config: IDateFieldMainDescriptorConfig = {}): (target: any, key: string) => any {
    return ModelMetaData.setFieldMainDescriptor(DateField, config);

}

/**
 * Decorator for a field with the array type
 *
 * @param config
 */
export function arrayField (config: IDateFieldMainDescriptorConfig = {}): (target: any, key: string) => any {
    return ModelMetaData.setFieldMainDescriptor(ArrayField, config);
}

/**
 * Decorator for the model (similar to has One in ExtJS)
 *
 * @param config
 */
export function hasOne (config: IHasOneMainDescriptorConfig = {}): (target: any, key: string) => any {
    return ModelMetaData.setHasOneMainDescriptor(null, config);
}

/**
 * Decorator for a collection (similar to hasMany in ExtJS)
 *
 * @param config
 */
export function hasMany (config: IHasManyMainDescriptorConfig = {}): (target: any, key: string) => any {
    return ModelMetaData.setHasManyMainDescriptor(ModelCollection, config);
}

/**
 * Decorator for setting properties of the model's field serialization / deserialization functional
 *
 * @param config
 */
export function fieldJsonProperty (config: IBaseJsonDescriptorConfig): (target: any, key: string) => any {
    return ModelMetaData.setFieldJsonDescriptor(null, config);
}

/**
 * Decorator for setting properties of the serialization / deserialization functional has One model
 *
 * @param config
 */
export function hasOneJsonProperty (config: IBaseJsonDescriptorConfig): (target: any, key: string) => any {
    return ModelMetaData.setHasOneJsonDescriptor(null, config);
}

/**
 * Decorator for setting properties of the hasMany model serialization / deserialization functionality
 *
 * @param config
 */
export function hasManyJsonProperty (config: IBaseJsonDescriptorConfig): (target: any, key: string) => any {
    return ModelMetaData.setHasManyJsonDescriptor(null, config);
}
