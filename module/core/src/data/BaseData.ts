/**
 * Base class for the model, collection, and field.
 * Contains a method for wrapping the observable system properties and the map with the current observable properties
 *
 * @author Ilya Brauer
 * @date 2019-11-20
 */

import {observable, decorate, extendObservable, isObservable} from 'mobx';
import {Base} from '../Base';
import {ReactiveProp} from '../type/SystemReactiveProperties';

export class BaseData extends Base {

    protected observabilitySet: Set<ReactiveProp> = new Set();

    /**
     * Make some of the system properties reactive in the current model
     * @param props - list of system properties
     */
    public setReactive (props: ReactiveProp[]) {
        const self: any = this;

        props.forEach(function (prop) {
            // eslint-disable-next-line no-prototype-builtins
            if (!self.hasOwnProperty(prop)) {
                console.warn(`Try to create reactive unknown prop '${prop}' in `, self);
            } else {
                if (isObservable(self[prop])) {
                    return;
                }

                const temp = self[prop];

                delete self[prop];
                extendObservable(self, {
                    [prop]: temp
                });
            }
        });
    }
}
