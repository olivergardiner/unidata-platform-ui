import {IBaseMainDescriptorConfig} from '../../interface/IBaseMainDescriptorConfig';

/**
 * Interface of the hasone config for the main decorator
 *
 * @author Denis Makarov
 * @date 2018-08-16
 */
export interface IHasOneMainDescriptorConfig extends IBaseMainDescriptorConfig {

}
