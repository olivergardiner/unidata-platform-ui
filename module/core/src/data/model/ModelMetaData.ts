import {Base} from '../../Base';
import {IDescriptor} from '../interface/IDescriptor';
import {ClassPropertyMeta} from '../ClassPropertyMeta';
import {ModelFieldDataType} from './ModelFieldDataType';
import {ClassPropertyDescriptor} from '../ClassPropertyDescriptor';
import forEach from 'lodash/forEach';
import indexOf from 'lodash/indexOf';
import {Nullable} from '../../type/Nullable';
import {IAssociationDescriptorMap} from '../interface/IAssociationDescriptorMap';

/**
 * Storage for model metadata
 *
 * @author Denis Makarov
 * @date 2018-08-16
 */
export class ModelMetaData {
    private static readonly AnnotationType = {
        fieldMainDescriptor: 'fieldMainDescriptor',
        hasOneMainDescriptor: 'hasOneMainDescriptor',
        hasManyMainDescriptor: 'hasManyMainDescriptor',
        fieldJsonDescriptor: 'fieldJsonDescriptor',
        hasOneJsonDescriptor: 'hasOneJsonDescriptor',
        hasManyJsonDescriptor: 'hasManyJsonDescriptor'
    };

    /**
     * Searches among the array of descriptors of the descriptor by the property name. Returns the first available handle.
     * If the handle is not found, null is returned.
     *
     * @param descriptors
     * @param propertyName
     */
    private static findDescriptorByPropertyName (
        descriptors: ClassPropertyDescriptor[],
        propertyName: string
    ): Nullable<ClassPropertyDescriptor> {
        let resultDescriptor: Nullable<ClassPropertyDescriptor> = null;

        forEach<any>(descriptors, function (descriptor) {
            if (descriptor.propertyName === propertyName) {
                resultDescriptor = descriptor;

                return false; // stopping the iteration
            }

            return;
        });

        return resultDescriptor;
    }

    /**
     * Writes a handle
     *
     * @param annotationKey
     * @param descriptor
     */
    private static setDescriptor (annotationKey: string, descriptor: IDescriptor): (target: any, key: string) => any {
        return (target: any, propertyName: string) => {
            let propertyDescriptor;

            propertyDescriptor = new ClassPropertyDescriptor(descriptor.ctor, propertyName, descriptor.config, descriptor.type);

            ClassPropertyMeta.addDescriptor(target.constructor, annotationKey, propertyDescriptor);
        };
    }

    /**
     * Returns the descriptors
     *
     * @param target
     * @param annotationKey
     */
    private static getDescriptors (target: any, annotationKey: string): ClassPropertyDescriptor[] {
        let descriptors: ClassPropertyDescriptor[] = [],
            names: string[] = [];

        if (!target || target === Base) {
            return descriptors;
        }

        do {
            ClassPropertyMeta.getDescriptors(target, annotationKey).forEach((item) => {
                let propertyName = item.propertyName;

                if (!propertyName) {
                    return;
                }

                // currently, inheritance redefinition is not allowed
                if (indexOf(names, propertyName) !== -1) {
                    throw new Error('Duplicate descriptor for propertyName = ' + propertyName);
                }

                descriptors.push(item);
                names.push(propertyName);
            });

            target = Object.getPrototypeOf(target);
        } while (target && target !== Base);

        return descriptors;
    }

    /**
     * Does the initialization of descriptors for hasOne and hasMany.
     * In fact, this is a "crutch" that helps to get rid of the problem of recursive nesting of one model into another (for example, NestedEntity).
     * In the standard implementation, you can't get a link to the constructor, because the module constructor is created later than it should be
     * used in the annotation. As a result the annotation is stored instead of the undefined constructor
     *
     * @param target
     * @param descriptorMap
     */
    public static initAssociationDescriptors (target: any, descriptorMap: IAssociationDescriptorMap): void {
        // hasOne
        forEach(descriptorMap.hasOne, function (ctor, propertyName) {
            let descriptor = ModelMetaData.getHasOneMainDescriptor(target, propertyName);

            if (!descriptor) {
                throw new Error('hasOne descriptor is undefined');
            }

            // we could already perform initialization, since it is performed at the time of constructing a new record
            if (descriptor.ctor) {
                return;
            }

            descriptor.ctor = ctor;
        });

        // hasMany
        forEach(descriptorMap.hasMany, function (ctor, propertyName) {
            let descriptor = ModelMetaData.getHasManyMainDescriptor(target, propertyName);

            if (!descriptor) {
                throw new Error('hasMany descriptor is undefined');
            }

            // we could already perform initialization, since it is performed at the time of constructing a new record
            if (descriptor.config.ofType) {
                return;
            }

            descriptor.config.ofType = ctor;
        });
    }

    /**
     * Performs the validation of descriptors for hasOne and hasMany. The only purpose of this function is to fall with an error
     * if for some reason the model stores incorrect data in the handle. Works in conjunction with initAssociationDescriptors
     *
     * @param target
     */
    public static validateAssociationDescriptors (target: any): void {
        let descriptors;

        // hasOne
        descriptors = ModelMetaData.getHasOneMainDescriptors(target);

        forEach(descriptors, function (descriptor) {
            if (!descriptor) {
                throw new Error('hasOne descriptor is undefined');
            }

            if (!descriptor.ctor) {
                throw new Error('hasOne descriptor.ctor is undefined');
            }
        });

        // hasMany
        descriptors = ModelMetaData.getHasManyMainDescriptors(target);

        forEach(descriptors, function (descriptor) {
            if (!descriptor) {
                throw new Error('hasMany descriptor is undefined');
            }

            if (!descriptor.config.ofType) {
                throw new Error('hasMany descriptor.config.ofType is undefined');
            }
        });
    }

    /**
     * Validates field descriptors. Only one primary field is allowed
     *
     * @param target
     */
    public static validateFieldDescriptors (target: any): void {
        let primaryKeyDescriptors = ModelMetaData.getPrimaryKeyFieldDescriptors(target);

        // only one primary key field is allowed
        if (primaryKeyDescriptors.length > 1) {
            throw new Error('Only one primary field is allowed');
        }
    }

    /**
     * Returns field descriptors that are the primary key
     * Currently, only one primary key field is allowed. But at the stage of model validation, there may be several of them.
     *
     * @param target
     */
    public static getPrimaryKeyFieldDescriptors (target: any): ClassPropertyDescriptor[] {
        let fieldMainDescriptors = ModelMetaData.getFieldMainDescriptors(target),
            descriptors: ClassPropertyDescriptor[] = [];

        forEach(fieldMainDescriptors, function (descriptor) {
            if (descriptor.config.primaryKey) {
                descriptors.push(descriptor);
            }
        });

        return descriptors;
    }

    /**
     * Returns the field's primary key descriptor.
     * Currently, only one primary key field is allowed.
     *
     * @param target
     */
    public static getPrimaryKeyFieldDescriptor (target: any): Nullable<ClassPropertyDescriptor> {
        let descriptors: ClassPropertyDescriptor[] = ModelMetaData.getPrimaryKeyFieldDescriptors(target),
            descriptor: Nullable<ClassPropertyDescriptor> = null;

        if (descriptors.length) {
            descriptor = descriptors[0];
        }

        return descriptor;
    }

    /**
     * Records the basis of the descriptor field of the model
     *
     * @param ctor
     * @param config
     */
    public static setFieldMainDescriptor (ctor: any, config: any): (target: any, key: string) => any {
        let descriptor: IDescriptor;

        descriptor = {
            ctor: ctor,
            config: config,
            type: ModelFieldDataType.field
        };

        return ModelMetaData.setDescriptor(ModelMetaData.AnnotationType.fieldMainDescriptor, descriptor);
    }

    /**
     * Returns an array of the main descriptors of the model fields. One descriptor corresponds to one field.
     *
     * @param target
     */
    public static getFieldMainDescriptors (target: any): ClassPropertyDescriptor[] {
        return ModelMetaData.getDescriptors(target, ModelMetaData.AnnotationType.fieldMainDescriptor);
    }

    /**
     * Returns the main descriptor of the model field. One descriptor corresponds to one field.
     *
     * @param target
     * @param propertyName
     */
    public static getFieldMainDescriptor (target: any, propertyName: string): ClassPropertyDescriptor {
        let descriptors = ModelMetaData.getFieldMainDescriptors(target),
            descriptor = ModelMetaData.findDescriptorByPropertyName(descriptors, propertyName);

        // we believe that the model should correctly request the main field descriptors and not apply for nonexistent descriptors
        if (!descriptor) {
            throw new Error('Main descriptor must be set [field]');
        }

        return descriptor;
    }

    /**
     * Writes the JSON descriptor of the model field.
     *
     * @param ctor
     * @param config
     */
    public static setFieldJsonDescriptor (ctor: any, config: any): (target: any, key: string) => any {
        let descriptor: IDescriptor;

        descriptor = {
            ctor: ctor,
            config: config,
            type: ModelFieldDataType.field
        };

        return ModelMetaData.setDescriptor(ModelMetaData.AnnotationType.fieldJsonDescriptor, descriptor);
    }

    /**
     * Returns a json field descriptors in the model. One descriptor corresponds to one field.
     *
     * @param target
     */
    public static getFieldJsonDescriptors (target: any): ClassPropertyDescriptor[] {
        return ModelMetaData.getDescriptors(target, ModelMetaData.AnnotationType.fieldJsonDescriptor);
    }

    /**
     * Returns the JSON descriptor of the model field. One descriptor corresponds to one field.
     * The Json descriptor may not be specified, in which case null is returned
     *
     * @param target
     * @param propertyName
     */
    public static getFieldJsonDescriptor (target: any, propertyName: string): Nullable<ClassPropertyDescriptor> {
        let descriptors = ModelMetaData.getFieldJsonDescriptors(target);

        return ModelMetaData.findDescriptorByPropertyName(descriptors, propertyName);
    }

    /**
     * Writes the base hasone model descriptor
     *
     * @param ctor
     * @param config
     */
    public static setHasOneMainDescriptor (ctor: any, config: any): (target: any, key: string) => any {
        let descriptor: IDescriptor;

        descriptor = {
            ctor: ctor,
            config: config,
            type: ModelFieldDataType.model
        };

        return ModelMetaData.setDescriptor(ModelMetaData.AnnotationType.hasOneMainDescriptor, descriptor);
    }

    /**
     * Returns an array of the main hasone descriptors of the model. One descriptor corresponds to one field.
     *
     * @param target
     */
    public static getHasOneMainDescriptors (target: any): ClassPropertyDescriptor[] {
        return ModelMetaData.getDescriptors(target, ModelMetaData.AnnotationType.hasOneMainDescriptor);
    }

    /**
     * Returns the main hasone descriptor of the model. One descriptor corresponds to one field.
     *
     * @param target
     * @param propertyName
     */
    public static getHasOneMainDescriptor (target: any, propertyName: string): ClassPropertyDescriptor {
        let descriptors = ModelMetaData.getHasOneMainDescriptors(target),
            descriptor = ModelMetaData.findDescriptorByPropertyName(descriptors, propertyName);

        if (!descriptor) {
            throw new Error('Main descriptor must be set [hasOne]');
        }

        return descriptor;
    }

    /**
     * Writes the has one model's json descriptor.
     *
     * @param ctor
     * @param config
     */
    public static setHasOneJsonDescriptor (ctor: any, config: any): (target: any, key: string) => any {
        let descriptor: IDescriptor;

        descriptor = {
            ctor: ctor,
            config: config,
            type: ModelFieldDataType.model
        };

        return ModelMetaData.setDescriptor(ModelMetaData.AnnotationType.hasOneJsonDescriptor, descriptor);
    }

    /**
     * Returns JSON handles for the has One model. One descriptor corresponds to one field.
     *
     * @param target
     */
    public static getHasOneJsonDescriptors (target: any): ClassPropertyDescriptor[] {
        return ModelMetaData.getDescriptors(target, ModelMetaData.AnnotationType.hasOneJsonDescriptor);
    }

    /**
     * Returns the model's has One json descriptor. One descriptor corresponds to one field.
     * The Json descriptor may not be specified, in which case null is returned
     *
     * @param target
     * @param propertyName
     */
    public static getHasOneJsonDescriptor (target: any, propertyName: string): Nullable<ClassPropertyDescriptor> {
        let descriptors = ModelMetaData.getHasOneJsonDescriptors(target);

        return ModelMetaData.findDescriptorByPropertyName(descriptors, propertyName);
    }

    /**
     * Writes the base hasMany model descriptor
     *
     * @param ctor
     * @param config
     */
    public static setHasManyMainDescriptor (ctor: any, config: any): (target: any, key: string) => any {
        let descriptor: IDescriptor;

        descriptor = {
            ctor: ctor,
            config: config,
            type: ModelFieldDataType.collection
        };

        return ModelMetaData.setDescriptor(ModelMetaData.AnnotationType.hasManyMainDescriptor, descriptor);
    }

    /**
     * Returns an array of the main hasMany model descriptors. One descriptor corresponds to one field.
     *
     * @param target
     */
    public static getHasManyMainDescriptors (target: any): ClassPropertyDescriptor[] {
        return ModelMetaData.getDescriptors(target, ModelMetaData.AnnotationType.hasManyMainDescriptor);
    }

    /**
     * Returns the main descriptor of the hasMany model. One descriptor corresponds to one field.
     *
     * @param target
     * @param propertyName
     */
    public static getHasManyMainDescriptor (target: any, propertyName: string): ClassPropertyDescriptor {
        let descriptors = ModelMetaData.getHasManyMainDescriptors(target),
            descriptor = ModelMetaData.findDescriptorByPropertyName(descriptors, propertyName);

        if (!descriptor) {
            throw new Error('Main descriptor must be set [hasMany]');
        }

        return descriptor;
    }

    /**
     * Writes the hasMany model's json descriptor.
     *
     * @param ctor
     * @param config
     */
    public static setHasManyJsonDescriptor (ctor: any, config: any): (target: any, key: string) => any {
        let descriptor: IDescriptor;

        descriptor = {
            ctor: ctor,
            config: config,
            type: ModelFieldDataType.collection
        };

        return ModelMetaData.setDescriptor(ModelMetaData.AnnotationType.hasManyJsonDescriptor, descriptor);
    }

    /**
     * Returns json handles for the has Many model. One descriptor corresponds to one field.
     *
     * @param target
     */
    public static getHasManyJsonDescriptors (target: any): ClassPropertyDescriptor[] {
        return ModelMetaData.getDescriptors(target, ModelMetaData.AnnotationType.hasManyJsonDescriptor);
    }

    /**
     * Returns the JSON descriptor of the hasMany model. One descriptor corresponds to one field.
     * The Json descriptor may not be specified, in which case null is returned
     *
     * @param target
     * @param propertyName
     */
    public static getHasManyJsonDescriptor (target: any, propertyName: string): Nullable<ClassPropertyDescriptor> {
        let descriptors = ModelMetaData.getHasManyJsonDescriptors(target);

        return ModelMetaData.findDescriptorByPropertyName(descriptors, propertyName);
    }
}
