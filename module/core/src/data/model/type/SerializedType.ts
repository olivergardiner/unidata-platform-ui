/**
 * Returns the serialized model type
 *
 * @author Aleksandr Bavin
 * @date 2020-05-12
 */
import {AbstractModel} from '../AbstractModel';
import {ArrayField, BooleanField, DateField, NumberField, StringField} from '../../field';
import {ModelCollection} from '../..';
import {AbstractField} from '../../field/AbstractField';

type GetValueType<T> =
    T extends StringField ? string :
    T extends NumberField ? number :
    T extends BooleanField ? boolean :
    T extends DateField ? string :
    T extends ArrayField ? unknown[] : // TODO: type of array values
    T extends AbstractModel ? SerializedType<T> :
    T extends ModelCollection<infer TColl> ? Array<SerializedType<TColl>> :
    never;

type FilterNames<T, P> =
    T extends AbstractField<any> ? P :
    T extends AbstractModel ? P :
    T extends ModelCollection<any> ? P :
    never;

type GetNames<T extends AbstractModel> = {
    [P in keyof T]: FilterNames<T[P], P>;
}[keyof T];

type GetTypes<T extends AbstractModel> = {
    [P in keyof T]: GetValueType<T[P]> | null;
}

// TODO: typing depending on the config
export type SerializedType<T extends AbstractModel> = Partial<Pick<GetTypes<T>, GetNames<T>>>;
