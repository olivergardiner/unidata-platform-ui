/**
 * Returns the names of model fields
 *
 * @author Aleksandr Bavin
 * @date 2020-05-15
 */
import {AbstractModel} from '../AbstractModel';
import {AbstractField} from '../../field/AbstractField';

export type GetFieldNames<T extends AbstractModel> = {
    [P in keyof T]: T[P] extends AbstractField<any> ? P : never;
}[keyof T];
