import {Sequence} from '../../Sequence';
import {AbstractField} from '../field/AbstractField';
import {action, IKeyValueMap, observable, ObservableMap, observe} from 'mobx';
import {ModelCollection} from '../collection/ModelCollection';
import each from 'lodash/each';
import cloneDeep from 'lodash/cloneDeep';
import clone from 'lodash/clone';
import forEach from 'lodash/forEach';
import forOwn from 'lodash/forOwn';
import {ModelMetaData} from './ModelMetaData';
import {IStringKeyMap} from '../../type/IStringKeyMap';
import {ClassPropertyDescriptor} from '../ClassPropertyDescriptor';
import {Nullable} from '../../type/Nullable';
import {IBaseJsonDescriptorConfig} from '../interface/IBaseJsonDescriptorConfig';
import {ReactiveProp} from '../../type/SystemReactiveProperties';
import {BaseData} from '../BaseData';
import {SerializedType} from './type/SerializedType';
import {GetFieldNames} from './type/GetFieldNames';

/**
 * Abstract data model
 *
 * @author Denis Makarov
 * @author Ivan Marshalkin
 * @date 2018-07-30
 */
export class AbstractModel extends BaseData {
    public modelId: number;
    public phantom: boolean = true;
    private building: boolean;

    public dirty: boolean = false;

    private modelDirty: boolean = false;

    private deepDirty: boolean = false;

    private referencesChanged: boolean = false;

    public lastValidationResult: Map<string, string[]> = new Map();

    private hasOneOriginalCollection: IStringKeyMap<AbstractModel> = {};

    public onChangeDirty = () => {}; // the method is redefined in the constructor or in the parent model. In fact it is private

    // TODO: you need to decide whether to pass descriptorConfig for fields / models / collections
    // now it is actually used only for the collection (can it be used in a different way?)
    constructor (data: any, descriptorConfig?: IStringKeyMap, initConfig?: IStringKeyMap, onChangeDirty?: () => void) {
        super();

        initConfig = initConfig || {};

        if (initConfig.isRoot !== false) {
            initConfig.isRoot = true;
        }

        if (onChangeDirty) {
            this.onChangeDirty = onChangeDirty;
        }

        this.modelId = Sequence.getId();

        // initialization should be performed at the very beginning followed by validation because the programmer may simply forget
        // write initialization for hasOne and hasMany
        this.initAssociationDescriptors();
        this.validateAssociationDescriptors();

        this.beforeInit();

        this.initModel(data, initConfig);

        // ToDo think about how to make it more appropriate
        this.initHasOneOriginalCollection();

        this.initAllHasOneObserve();

        this.afterInit();

        this.building = false;
    }

    /**
     * The method calls before the model initialization begins
     */
    public beforeInit (): void {
    }

    /**
     * This method is called at the final stage of model initialization
     */
    public afterInit (): void {
    }

    /**
     * Does the initialization of descriptors for hasOne and hasMany.
     * ModelMetaData.initAssociationDescriptors
     */
    protected initAssociationDescriptors (): void {
    }

    /**
     * performs the validation of descriptors for hasOne and hasMany.
     * ModelMetaData.validateAssociationDescriptors
     */
    public validateAssociationDescriptors (): void {
        let target = this.constructor;

        ModelMetaData.validateAssociationDescriptors(target);
        ModelMetaData.validateFieldDescriptors(target);
    }

    /**
     * Initializes the model and creates " internals"
     *
     * @param data
     */
    private initModel (data: any, initConfig: IStringKeyMap): void {
        this.initModelPhantom(data);

        this.createFields(data);
        this.createHasOne(data, initConfig);
        this.createHasMany(data, initConfig);

        // after creating the model we put the phantom in cascades
        if (initConfig.isRoot === true) {
            this.setPhantomCascade(this.getPhantom());
        }
    }

    private initModelPhantom (data: any) {
        let id = null,
            primaryKeyDescriptor = this.getPrimaryKeyDescriptor(),
            propertyName;

        // if the data has an ID we assume that the model is not a phantom
        // TODO: note that a json descriptor can be specified and fromJson can be specified
        // accordingly, it is necessary to analyze it using fromJson and not descriptor.propertyName
        if (primaryKeyDescriptor) {
            propertyName = primaryKeyDescriptor.propertyName;

            if (data && data[propertyName]) {
                id = data[propertyName];
            }

            if (id || id === 0) {
                this.setPhantom(false);
            }
        }
    }

    /**
     * Creates model fields
     *
     * @param data
     */
    protected createFields (data: any): void {
        let descriptors;

        descriptors = this.getFieldMainDescriptors();

        for (let i = 0; i < descriptors.length; i++) {
            let fieldDescriptor,
                descriptorConfig,
                propertyName,
                value;

            fieldDescriptor = descriptors[i];
            propertyName = fieldDescriptor.propertyName;

            // TODO: note that a json descriptor can be specified and fromJson can be specified
            value = data ? data[propertyName] : null;

            // we perform deep cloning of the object, so that it is not accidentally rubbed
            descriptorConfig = cloneDeep(fieldDescriptor.config);
            (this as any)[propertyName] = new fieldDescriptor.ctor(value, this.updateModelDirty.bind(this), descriptorConfig);
            this.assignPropertyValidators(propertyName);

        }
    }

    /**
     * Throwing the validator into the model property
     * @param propertyName - property name
     */
    private assignPropertyValidators (propertyName: string) {
        let propertyValidators = [],
            validators,
            propertyValidator,
            modelValidators;

        modelValidators = (this as any).constructor.validators;

        if (modelValidators && modelValidators[propertyName]) {
            validators = modelValidators[propertyName];

            for (let validator of validators) {
                propertyValidator = clone(validator);
                propertyValidator.model = this;
                propertyValidators.push(propertyValidator);
            }

            (this as any)[propertyName].validators = propertyValidators;
        }
    }

    /**
     * Creates hasOne models
     *
     * @param data
     */
    protected createHasOne (data: any, initConfig: IStringKeyMap): void {
        let descriptors;

        descriptors = this.getHasOneMainDescriptors();

        for (let i = 0; i < descriptors.length; i++) {
            let modelDescriptor,
                descriptorConfig,
                propertyName,
                value;

            modelDescriptor = descriptors[i];
            propertyName = modelDescriptor.propertyName;

            // TODO: note that a json descriptor can be specified and fromJson can be specified
            value = data ? data[propertyName] : null;

            // we perform deep cloning of the object, so that it is not accidentally rubbed
            descriptorConfig = cloneDeep(modelDescriptor.config);

            // we perform deep cloning of the object, so that it is not accidentally rubbed
            // the models you create are no longer root models
            initConfig = cloneDeep(initConfig);
            initConfig.isRoot = false;

            (this as any)[propertyName] = new modelDescriptor.ctor(
                value,
                descriptorConfig,
                initConfig,
                this.onHasOneEventDirtyChange.bind(this)
            );
            this.assignPropertyValidators(propertyName);
        }
    }

    /**
     * Creates hasMany models
     *
     * @param data
     */
    protected createHasMany (data: any, initConfig: IStringKeyMap): void {
        let descriptors;

        descriptors = this.getHasManyMainDescriptors();

        for (let i = 0; i < descriptors.length; i++) {
            let collectionDescriptor,
                descriptorConfig,
                propertyName,
                value;

            collectionDescriptor = descriptors[i];
            propertyName = collectionDescriptor.propertyName;

            // TODO: note that a json descriptor can be specified and fromJson can be specified
            value = data ? data[propertyName] : null;

            // we perform deep cloning of the object, so that it is not accidentally rubbed
            descriptorConfig = cloneDeep(collectionDescriptor.config);

            // we perform deep cloning of the object, so that it is not accidentally rubbed
            // the models you create are no longer root models
            initConfig = cloneDeep(initConfig);
            initConfig.isRoot = false;

            (this as any)[propertyName] = new collectionDescriptor.ctor(
                value,
                descriptorConfig,
                initConfig,
                this.updateDeepDirty.bind(this)
            );
            this.assignPropertyValidators(propertyName);
        }
    }

    /**
     * Returns the model property (field, has One, has Many) by the main handle, if the property is not set or not found, null is returned
     *
     * @param descriptor
     */
    protected getPropertyByDescriptor<T> (descriptor: ClassPropertyDescriptor): Nullable<T> {
        let me = this,
            property: Nullable<T> = null,
            propertyName = descriptor.propertyName,
            propertyValue = (me as any)[propertyName];

        if (propertyValue) {
            property = propertyValue;
        }

        return property;
    }

    /**
     * Returns an array of model properties based on the passed descriptors
     * Only certain values are returned.
     *
     * @param descriptors
     */
    protected getPropertiesByDescriptors<T> (descriptors: ClassPropertyDescriptor[]): T[] {
        let me = this,
            properties: T[] = [];

        descriptors.forEach(function (descriptor) {
            let property = me.getPropertyByDescriptor<T>(descriptor);

            if (property) {
                properties.push(property);
            }
        });

        return properties;
    }

    /**
     * Returns a map of model properties based on the passed descriptors. The key is the property name, and the value is the property itself.
     * Only certain values are returned.
     *
     * @param descriptors
     */
    protected getPropertiesAsMapByDescriptors<T> (descriptors: ClassPropertyDescriptor[]): IStringKeyMap<T> {
        let me = this,
            properties: IStringKeyMap<T> = {};

        descriptors.forEach(function (descriptor) {
            let property = me.getPropertyByDescriptor<T>(descriptor),
                propertyName = descriptor.propertyName;

            if (property) {
                properties[propertyName] = property;
            }
        });

        return properties;
    }

    public getField<T extends GetFieldNames<this>> (name: T): this[T];
    public getField<T extends string> (name: T): AbstractField<any> | undefined;
    public getField (name: string): any {
        let map = this.getFieldsMap();

        if (!map[name]) {
            return;
        }

        return map[name];
    }

    /**
     * Returns an array of model fields
     */
    public getFields (): Array<AbstractField<any>> {
        let descriptors = this.getFieldMainDescriptors();

        return this.getPropertiesByDescriptors(descriptors);
    }

    // Todo check the getFields method for the property name
    public getFieldsMap (): IKeyValueMap<AbstractField<any>> {
        let descriptors = this.getFieldMainDescriptors();

        return this.getPropertiesAsMapByDescriptors(descriptors);
    }

    public getHasManyMap (): IKeyValueMap<ModelCollection<any>> {
        let descriptors = this.getHasManyMainDescriptors();

        return this.getPropertiesAsMapByDescriptors(descriptors);
    }

    public getHasOneMap (): IKeyValueMap<AbstractModel> {
        let descriptors = this.getHasOneMainDescriptors();

        return this.getPropertiesAsMapByDescriptors(descriptors);
    }

    /**
     * Returns an array of the main descriptors of the model fields. Can include descriptors for which entities have not been created.
     * The main descriptor must exist
     */
    public getFieldMainDescriptors (): ClassPropertyDescriptor[] {
        return ModelMetaData.getFieldMainDescriptors(this.constructor);
    }

    /**
     * returns the main descriptor for the model field by its name.
     * The main descriptor must exist
     */
    public getFieldMainDescriptor (propertyName: string): ClassPropertyDescriptor {
        let descriptor: ClassPropertyDescriptor = ModelMetaData.getFieldMainDescriptor(this.constructor, propertyName);

        return descriptor;
    }

    /**
     * Returns a json descriptor for the model field by its name.
     * The Json handle may not exist
     */
    public getFieldJsonDescriptor (propertyName: string): Nullable<ClassPropertyDescriptor> {
        let descriptor: Nullable<ClassPropertyDescriptor> = ModelMetaData.getFieldJsonDescriptor(this.constructor, propertyName);

        return descriptor;
    }

    /**
     * Returns the model's hasOne array
     */
    public getHasOneCollection (): AbstractModel[] {
        let descriptors = this.getHasOneMainDescriptors();

        return this.getPropertiesByDescriptors(descriptors);
    }

    /**
     * Returns an array of hasone model descriptors. Can include descriptors for which entities have not been created.
     * The main descriptor must exist
     */
    public getHasOneMainDescriptors (): ClassPropertyDescriptor[] {
        return ModelMetaData.getHasOneMainDescriptors(this.constructor);
    }

    /**
     * Returns the main hasone descriptor of the model.
     * The main descriptor must exist
     */
    public getHasOneMainDescriptor (propertyName: string): ClassPropertyDescriptor {
        let descriptor: ClassPropertyDescriptor = ModelMetaData.getHasOneMainDescriptor(this.constructor, propertyName);

        return descriptor;
    }

    /**
     * Returns the model's has One json descriptor.
     * Json handle may not exist
     */
    public getHasOneJsonDescriptor (propertyName: string): Nullable<ClassPropertyDescriptor> {
        let descriptor: Nullable<ClassPropertyDescriptor> = ModelMetaData.getHasOneJsonDescriptor(this.constructor, propertyName);

        return descriptor;
    }

    /**
     * Returns an array of original hasOne
     */
    public getHasOneOriginalCollection (): IStringKeyMap<AbstractModel> {
        return this.hasOneOriginalCollection;
    }

    /**
     * Returns the hasMany array of the model
     */
    public getHasManyCollection (): Array<ModelCollection<AbstractModel>> {
        let descriptors = this.getHasManyMainDescriptors();

        return this.getPropertiesByDescriptors(descriptors);
    }

    /**
     * Returns an array of hasMany model descriptors. Can include descriptors for which entities have not been created.
     * The main descriptor must exist
     */
    public getHasManyMainDescriptors (): ClassPropertyDescriptor[] {
        return ModelMetaData.getHasManyMainDescriptors(this.constructor);
    }

    /**
     * Returns the main descriptor of the hasMany model.
     * The main descriptor must exist
     */
    public getHasManyMainDescriptor (propertyName: string): ClassPropertyDescriptor {
        let descriptor: ClassPropertyDescriptor = ModelMetaData.getHasManyMainDescriptor(this.constructor, propertyName);

        return descriptor;
    }

    /**
     * Returns the JSON descriptor of the hasMany model.
     * The Json descriptor may NOT exist
     */
    public getHasManyJsonDescriptor (propertyName: string): Nullable<ClassPropertyDescriptor> {
        let descriptor: Nullable<ClassPropertyDescriptor> = ModelMetaData.getHasManyJsonDescriptor(this.constructor, propertyName);

        return descriptor;
    }

    /**
     * Returns the property's own value by the property name
     *
     * @param propertyName
     */
    private getOwnProperty (propertyName: string): any {
        let property: any;

        if (Object.prototype.hasOwnProperty.call(this, propertyName)) {
            property = (this as any)[propertyName];
        }

        return property;
    }

    /**
     * Returns true if the value is a field
     *
     * @param property
     */
    private isField (property: any): boolean {
        return property instanceof AbstractField;
    }

    /**
     * Returns true if a property named fieldName is defined on the model and it is a field
     *
     * @param fieldName
     */
    private isFieldByName (fieldName: string): boolean {
        return this.isField(this.getOwnProperty(fieldName));
    }

    /**
     * Returns true if the value is hasOne
     *
     * @param property
     */
    private isHasOne (property: any): boolean {
        return property instanceof AbstractModel;
    }

    /**
     * Returns true if a property named hasOneName is defined on the model and it is hasOne
     *
     * @param fieldName
     */
    private isHasOneByName (hasOneName: string): boolean {
        return this.isHasOne(this.getOwnProperty(hasOneName));
    }

    /**
     * Returns true if the value is hasMany
     *
     * @param property
     */
    private isHasMany (property: any): boolean {
        return property instanceof ModelCollection;
    }

    /**
     * Returns true if a property named hasOneName is defined on the model and it is hasMany
     *
     * @param fieldName
     */
    private isHasManyByName (hasManyName: string): boolean {
        return this.isHasMany(this.getOwnProperty(hasManyName));
    }

    /**
     * Returns true if fields are defined on the model
     */
    private hasAnyField (): boolean {
        let result: boolean = false,
            descriptors = this.getFieldMainDescriptors();

        if (descriptors.length) {
            result = true;
        }

        return result;
    }

    /**
     * Returns true if hasOne is defined on the model
     */
    private hasAnyHasOne (): boolean {
        let result: boolean = false,
            descriptors = this.getHasOneMainDescriptors();

        if (descriptors.length) {
            result = true;
        }

        return result;
    }

    /**
     * Returns true if hasMany is defined on the model
     */
    private hasAnyHasMany (): boolean {
        let result: boolean = false,
            descriptors = this.getHasManyMainDescriptors();

        if (descriptors.length) {
            result = true;
        }

        return result;
    }

    /**
     * Returns the primary key field
     */
    public getPrimaryKeyField (): Nullable<AbstractField<any>> {
        let field: Nullable<AbstractField<any>> = null,
            descriptor = ModelMetaData.getPrimaryKeyFieldDescriptor(this.constructor);

        if (descriptor) {
            field = this.getPropertyByDescriptor(descriptor);
        }

        return field;
    }

    /**
     * Returns the field's primary key descriptor
     */
    public getPrimaryKeyDescriptor (): Nullable<ClassPropertyDescriptor> {
        return ModelMetaData.getPrimaryKeyFieldDescriptor(this.constructor);
    }

    /**
     * Returns a serialized view of the model
     *
     * @param config-serialization parameters
     * @param {boolean} [config.persist=false] When set to true, only stored values are returned.
     * If the value is false, the fields are returned regardless of the value of the persist attribute.
     * The default value is false.
     * @param {boolean} [config.associated=true] If the value is true, hasone and hasMany are serialized.
     * The default value is true.
     * @param {boolean} [config.changes=false] When set to true, only the changed data is serialized.
     * The default value is false.
     */
    public serialize (config?: any): SerializedType<this> {
        let me = this,
            jsonData: any = {},
            persist: boolean = false,
            changes: boolean = false,
            associated: boolean = true,
            fieldMainDescriptors = this.getFieldMainDescriptors(),
            hasOneMainDescriptors = this.getHasOneMainDescriptors(),
            hasManyMainDescriptors = this.getHasManyMainDescriptors();

        config = config || {};

        persist = Boolean(config.persist);
        changes = Boolean(config.changes);

        associated = Boolean(config.associated);

        if (typeof config.associated === 'undefined') {
            associated = true;
        }

        // there were no changes
        if (changes && !this.getModelDirty() && !this.getDeepDirty()) {
            return jsonData;
        }

        // serializing the model fields
        forEach(fieldMainDescriptors, function (mainDescriptor) {
            let propertyName = mainDescriptor.propertyName,
                jsonDescriptor = me.getFieldJsonDescriptor(propertyName),
                toJsonName = propertyName,
                field;

            field = me.getPropertyByDescriptor<AbstractField<any>>(mainDescriptor);

            if (!field) {
                throw new Error('Field must be set');
            }

            // there were no changes and you only need to return the changed data
            if (changes && !field.getDirty()) {
                return;
            }

            if (jsonDescriptor && jsonDescriptor.config && (jsonDescriptor.config as IBaseJsonDescriptorConfig).toJson) {
                toJsonName = jsonDescriptor.config.toJson;
            }

            jsonData[toJsonName] = field.serialize({
                persist: persist,
                changes: changes,
                associated: associated
            });
        });

        if (!associated) {
            return jsonData;
        }

        // serializing hasOne
        forEach(hasOneMainDescriptors, function (hasOneDescriptor) {
            let propertyName = hasOneDescriptor.propertyName,
                jsonDescriptor = me.getHasOneJsonDescriptor(propertyName),
                toJsonName = propertyName,
                hasOne;

            hasOne = me.getPropertyByDescriptor<AbstractModel>(hasOneDescriptor);

            // hasOne may not exist this is the correct behavior of the model
            if (!hasOne) {
                jsonData[toJsonName] = null;

                return;
            }

            // there were no changes and you only need to return the changed data
            if (changes && !hasOne.getDirty()) {
                return;
            }

            if (jsonDescriptor && jsonDescriptor.config && (jsonDescriptor.config as IBaseJsonDescriptorConfig).toJson) {
                toJsonName = jsonDescriptor.config.toJson;
            }

            jsonData[toJsonName] = hasOne.serialize({
                persist: persist,
                changes: changes,
                associated: associated
            });
        });

        // serializing hasMany
        forEach(hasManyMainDescriptors, function (hasManyDescriptor) {
            let propertyName = hasManyDescriptor.propertyName,
                jsonDescriptor = me.getHasManyJsonDescriptor(propertyName),
                toJsonName = propertyName,
                hasMany;

            hasMany = me.getPropertyByDescriptor<ModelCollection<any>>(hasManyDescriptor);

            if (!hasMany) {
                throw new Error('HasMany must be defined');
            }

            // there were no changes and you only need to return the changed data
            if (changes && !hasMany.getDirty()) {
                return;
            }

            if (jsonDescriptor && jsonDescriptor.config && (jsonDescriptor.config as IBaseJsonDescriptorConfig).toJson) {
                toJsonName = jsonDescriptor.config.toJson;
            }

            jsonData[toJsonName] = hasMany.serialize({
                persist: persist,
                changes: changes,
                associated: associated
            });
        });

        return jsonData;
    }

    public commit (): void {
        let fields: Array<AbstractField<any>> = this.getFields(),
            hasManyCollection: Array<ModelCollection<AbstractModel>> = this.getHasManyCollection(),
            hasOneCollection: IStringKeyMap<AbstractModel> = this.getHasOneMap();

        this.setPhantom(false);

        // commit for fields
        each(fields, (field: AbstractField<any>) => {
            field.commit();
        });

        // commit for hasOne collection
        forOwn(hasOneCollection, (hasOne) => {
            if (!hasOne) {
                return;
            }

            hasOne.commit();
        });

        this.hasOneOriginalCollection = clone(hasOneCollection);
        this.setReferencesChanged(false);

        // commit for hasMany collection
        each(hasManyCollection, (hasMany: ModelCollection<AbstractModel>) => {
            hasMany.commit();
        });

        // TODO: should I call events during a commit? or do I make the clearState method?
        this.updateModelDirty();
        this.updateDeepDirty();

        this.clearValidation();
        // TODO: some commit events? silent fn argument?
        // TODO: increment version?
    }

    public revert (): void {
        let self: any = this,
            fields: Array<AbstractField<any>> = this.getFields(),
            hasManyCollection: Array<ModelCollection<AbstractModel>> = this.getHasManyCollection(),
            hasOneOriginalCollection: IStringKeyMap<AbstractModel> = this.getHasOneOriginalCollection(),
            hasOneCollection: AbstractModel[] = this.getHasOneCollection();

        // revert for fields
        each(fields, (field: AbstractField<any>) => {
            field.revert();
        });

        // revert for hasOne collection
        each(hasOneCollection, (hasOne: AbstractModel) => {
            if (!hasOne) {
                return;
            }

            hasOne.revert();
        });

        forOwn(hasOneOriginalCollection, (hasOne: AbstractModel, key) => {
            self[key] = hasOneOriginalCollection[key];
        });

        this.setReferencesChanged(false);

        // revert for hasMany collection
        each(hasManyCollection, (hasMany: ModelCollection<AbstractModel>) => {
            hasMany.revert();
        });

        // TODO: are these the same questions as in commit?
        this.updateModelDirty();
        this.updateDeepDirty();

        this.clearValidation();
    }

    /**
     * Creating an original collection of all hasOne
     */
    private initHasOneOriginalCollection (): void {
        this.hasOneOriginalCollection = clone(this.getHasOneMap());
    }


    private onHasOneEventDirtyChange (): void {
        this.updateDeepDirty();
    }

    /**
     * Subscribing to changes to has One relationships
     */
    private initAllHasOneObserve (): void {
        let self: any = this;

        forOwn(this.getHasOneMap(), function (hasOne: AbstractModel, key: string) {
            if (!hasOne /*|| !hasOne.needObserve*/) {
                return;
            }

            // self.initHasOneObserve(hasOne);
            self.initHasOneReferenceObserve(hasOne, key);
        });
    }

    /**
     * Subscribe to hasone link changes
     * Needed to track a record instead of the current hasOne of a different model, or null.
     */
    private initHasOneReferenceObserve (hasOne: AbstractModel, key: string): void {
        let self: any = this;

        observe(self, key, function (change) {
            let newHasOne: AbstractModel = change.newValue,
                oldHasOne: AbstractModel = change.oldValue,
                originalHasOne: AbstractModel,
                isReferenceChanged: boolean;

            if (newHasOne === oldHasOne) {
                return;
            }

            originalHasOne = self.hasOneOriginalCollection[key];

            if (originalHasOne === newHasOne) {
                isReferenceChanged = false;
            } else {
                isReferenceChanged = true;
            }

            if (newHasOne && self.observabilitySet.size > 0) {
                const observableProps: ReactiveProp[] = [];

                self.observabilitySet.forEach(function (prop: ReactiveProp) {
                    observableProps.push(prop);
                });

                newHasOne.setReactiveCascade(observableProps);
            }

            self.setReferencesChanged(isReferenceChanged);
            self.updateModelDirty();
        });
    }

    /**
     * Make some of the system properties reactive cascading throughout the model
     * @param props - list of system properties
     */
    public setReactiveCascade (props: ReactiveProp[]) {
        const self = this;

        self.setReactive(props);

        self.getFields().forEach(function (field) {
            field.setReactive(props);
        });

        forOwn(self.getHasOneMap(), function (item, key) {
            item.setReactiveCascade(props);
        });

        self.getHasManyCollection().forEach(function (item) {
            item.setReactiveCascade(props);
        });

        props.forEach(function (prop) {
            self.observabilitySet.add(prop);
        });
    }

    // ToDo optimize
    @action /** Don't delete action. The last ValidationResult property can be made reactive in runtime */
    public validate (): ObservableMap<string, string[]> {
        let me = this,
            modelValidators = (this as any).constructor.validators || [],
            validationErrorsMap: ObservableMap = observable.map({}),
            hasOnes,
            hasManys,
            hasOnesKeys,
            hasManyKeys,
            fieldValidationResult;

        for (let fieldKey of Object.keys(modelValidators)) {
            if ((me as any)[fieldKey] && (me as any)[fieldKey].validate) {
                fieldValidationResult = (me as any)[fieldKey].validate();

                if (fieldValidationResult.length > 0) {
                    validationErrorsMap.set(fieldKey, fieldValidationResult);
                }
            } else {
                console.error('Try validate not existed field:', fieldKey);
            }
        }

        hasOnes = me.getHasOneMap();

        hasOnesKeys = Object.keys(hasOnes);

        for (let hasOneKey of hasOnesKeys) {
            // if (hasOnes[hasOneKey].validate) {
                fieldValidationResult = hasOnes[hasOneKey].validate();

                if (fieldValidationResult.size > 0) {
                    validationErrorsMap.set(hasOneKey, fieldValidationResult);
                }
            // }
        }

        hasManys = me.getHasManyMap();

        hasManyKeys = Object.keys(hasManys);

        for (let hasManyKey of hasManyKeys) {
            // if (hasManys[hasManyKey].validate) {
                fieldValidationResult = hasManys[hasManyKey].validate();

                if (fieldValidationResult.length > 0) {
                    validationErrorsMap.set(hasManyKey, fieldValidationResult);
                }
            // }
        }

        if (this.lastValidationResult) {
            this.lastValidationResult.clear();
        }

        this.lastValidationResult = validationErrorsMap;

        return validationErrorsMap;
    }

    @action /** Don't delete action. The last ValidationResult property can be made reactive in runtime */
    public clearValidation (): any {
        this.lastValidationResult.clear();
    }

    public checkFieldDirty (field: AbstractField<any>): boolean {
        let dirty: boolean = false;

        if (field && field.getDirty()) {
            dirty = true;
        }

        return dirty;
    }

    public calculateFieldsDirty (): boolean {
        let self = this,
            dirty: boolean = false;

        this.getFields().forEach(function (field) {
            if (!dirty) {
                dirty = self.checkFieldDirty(field);
            }
        });

        return dirty;
    }

    private checkModelDirty (modelAttribute: AbstractModel): boolean {
        let dirty: boolean = false;

        if (modelAttribute && modelAttribute.getDirty()) {
            dirty = true;
        }

        return dirty;
    }

    private checkModelAttributesDirty (): boolean {
        let self = this,
            dirty: boolean = false,
            hasOneCollection = this.getHasOneMap();

        for (let key in hasOneCollection) {
            dirty = self.checkModelDirty(hasOneCollection[key]);

            if (dirty) {
                return dirty;
            }
        }

        return dirty;
    }

    private checkModelCollectionDirty (modelCollection: ModelCollection<any>): boolean {
        let dirty: boolean = false;

        if (modelCollection && modelCollection.getDirty()) {
            dirty = true;
        }

        return dirty;
    }

    private checkModelCollectionsDirty (): boolean {
        let self = this,
            dirty: boolean = false;

        this.getHasManyCollection().forEach(function (modelCollection) {
            if (!dirty) {
                dirty = self.checkModelCollectionDirty(modelCollection);
            }
        });

        return dirty;
    }

    private checkModelCollectionsDeepDirty (): boolean {
        let self = this,
            dirty: boolean = false;

        this.getHasManyCollection().forEach(function (modelCollection) {
            if (!dirty) {
                dirty = self.checkModelCollectionDeepDirty(modelCollection);
            }
        });

        return dirty;
    }

    private checkModelCollectionDeepDirty (modelCollection: ModelCollection<any>): boolean {
        let dirty: boolean = false;

        if (modelCollection && modelCollection.getDeepDirty()) {
            dirty = true;
        }

        return dirty;
    }

    @action /** Don't delete action. The model Dirty property can be made reactive in runtime */
    public updateModelDirty (): void {
        this.modelDirty = this.calculateModelDirty();

        this.updateDirty();
    }

    public updateDeepDirty (): void {
        this.setDeepDirty(this.calculateDeepDirty());

        this.updateDirty();
    }

    public updateDirty (): void {
        this.setDirty(this.calculateDirty());

        this.onChangeDirty();
    }

    public getDirty (): boolean {
        return this.dirty;
    }

    /**
     * Returns whether a new record is selected
     */
    public getPhantom (): boolean {
        return this.phantom;
    }

    /**
     * Sets the flag for a new record
     */
    private setPhantom (phantom: boolean): void {
        this.phantom = phantom;
    }

    /**
     * Sets the phantom attribute cascadingly for the model
     *
     * @param phantom
     */
    public setPhantomCascade (phantom: boolean): void {
        let hasManyCollection: Array<ModelCollection<AbstractModel>> = this.getHasManyCollection(),
            hasOneCollection: AbstractModel[] = this.getHasOneCollection();

        this.setPhantom(phantom);

        // hasOne
        each(hasOneCollection, (hasOne) => {
            if (!hasOne) {
                return;
            }

            hasOne.setPhantomCascade(phantom);
        });

        // hasMany
        each(hasManyCollection, (hasMany: ModelCollection<AbstractModel>) => {
            hasMany.setPhantomCascade(phantom);
        });
    }

    @action /** Don't delete action. The dirty property can be made reactive in runtime */
    public setDirty (flag: boolean): void {
        this.dirty = Boolean(flag);
    }

    public getReferencesChanged (): boolean {
        return Boolean(this.referencesChanged);
    }

    // @action
    public setReferencesChanged (flag: boolean): void {
        this.referencesChanged = Boolean(flag);
    }

    public getDeepDirty (): boolean {
        return Boolean(this.deepDirty);
    }

    public getModelDirty (): boolean {
        return Boolean(this.modelDirty);
    }

    @action /** Don't delete action. The deep Dirty property can be made reactive in runtime */
    public setDeepDirty (flag: boolean): void {
        this.deepDirty = Boolean(flag);
    }

    public calculateDirty (): boolean {
        return this.modelDirty || this.deepDirty;
    }

    public calculateModelDirty (): boolean {
        return this.getReferencesChanged() || this.calculateFieldsDirty();
    }

    public calculateDeepDirty (): boolean {
        return this.checkModelAttributesDirty() ||
               this.checkModelCollectionsDirty() ||
               this.checkModelCollectionsDeepDirty();
    }

    /**
     * Cloning model data
     *
     * @param config Config of the serialize method
     * @returns {T}
     */
    public clone <T extends AbstractModel> (config?: any): T {
        return new (this as any).constructor(this.serialize(config)) as T;
    }
}
