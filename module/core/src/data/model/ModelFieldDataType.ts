/**
 * Enumeration of model field types
 *
 * @author Denis Makarov
 * @date 2018-08-16
 */
export enum ModelFieldDataType {
    'field',
    'collection',
    'model'
}
