/**
 * A rule that checks whether the value is filled in
 *
 * @author: Denis Makarov
 * @date: 2019-01-27
 */

import {IFieldValidationParams} from '../IFieldValidationParams';

export class Email {
    public static defaultMessage = 'Email invalid';

    public static validate (params: IFieldValidationParams) {
        return Email.validateEmail(params.value);
    }

    public static validateEmail (email: string) {
        let re;

        if (!email) {
            return true;
        }

        // eslint-disable-next-line no-useless-escape
        re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(String(email).toLowerCase());
    }
}
