/**
 * Latin characters only
 *
 * @author: Denis Makarov
 * @date: 2019-03-27
 */

import {IFieldValidationParams} from '../IFieldValidationParams';

export class LatinOnly {
    public static defaultMessage = 'validation>latinOnly';

    public static validate (params: IFieldValidationParams) {
        let re,
            value = params.value;

        if (!value) {
            return true;
        }

        re = /^[a-zA-Z][a-zA-Z0-9_-]*$/i;

        return re.test(String(value).toLowerCase());
    }
}
