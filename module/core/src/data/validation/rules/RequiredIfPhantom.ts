/**
 * A rule that checks whether the value is filled in
 *
 * @author: Denis Makarov
 * @date: 2019-02-02
 */

import {IFieldValidationParams} from '../IFieldValidationParams';
import {Required} from './Required';

export class RequiredIfPhantom {
    public static defaultMessage = 'Required';

    public static validate (params: IFieldValidationParams) {
        if (params.model.phantom) {
            return Required.validate(params);
        }

        return true;
    }
}
