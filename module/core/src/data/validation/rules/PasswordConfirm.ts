/**
 * A rule that checks whether the value is filled in
 *
 * @author: Denis Makarov
 * @date: 2019-02-02
 */

import {IFieldValidationParams} from '../IFieldValidationParams';
import isEmpty from 'lodash/isEmpty';

export class PasswordConfirm {
    public static defaultMessage = 'Passwords don\'t match!';

    public static validate (params: IFieldValidationParams) {
        return PasswordConfirm.isEqual(params.value, params.model, params.args[0]);
    }

    public static isEqual (value: string, model: any, propertyName: string) {
        if (!model) {
            return false;
        }

        if (!model[propertyName]) {
            return false;
        }

        if (isEmpty(value) && isEmpty(model[propertyName].getValue())) {
            return true;
        }

        return model[propertyName].getValue() === value;
    }
}
