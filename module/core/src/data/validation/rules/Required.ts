/**
 * A rule that checks whether the value is filled in
 *
 * @author: Denis Makarov
 * @date: 2018-12-27
 */

import {IFieldValidationParams} from '../IFieldValidationParams';

export class Required {
    public static defaultMessage = 'validation>fieldRequired';

    public static validate (params: IFieldValidationParams) {
        if (params.value) {
            if (params.value.trim) {
                return Boolean(params.value.trim());
            }

            if (params.value.length) {
                return Boolean(params.value.length);
            }
        }

        return Boolean(false);
    }
}
