/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-12-27
 */

import {IFieldValidationParams} from './IFieldValidationParams';

export interface IFieldValidator {
    rule?: any;
    message?: string;
    model?: any;
    args?: any [];
    validate? (params: IFieldValidationParams): boolean;
}
