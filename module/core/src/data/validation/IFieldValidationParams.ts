/**
 * empty comment line Denis Makarov
 *
 * @author: Denis Makarov
 * @date: 2018-12-27
 */

export interface IFieldValidationParams {
    value?: any;
    model?: any;
    args?: any;
}
