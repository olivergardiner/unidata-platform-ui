/**
 * The property descriptor
 *
 * @author Ivan Marshalkin
 * @date 2018-08-23
 */
import {ModelFieldDataType} from './model/ModelFieldDataType';
import {ClassCtor} from '../type/ClassCtor';
import {IStringKeyMap} from '../type/IStringKeyMap';

export class ClassPropertyDescriptor {
    public propertyName: string;

    public ctor: ClassCtor<any>;
    public config: IStringKeyMap;
    public type: ModelFieldDataType;

    constructor (ctor: ClassCtor<any>, propertyName: string, propertyCfg: IStringKeyMap, type: ModelFieldDataType) {
        this.ctor = ctor;
        this.propertyName = propertyName;
        this.config = propertyCfg;
        this.type = type;
    }
}
