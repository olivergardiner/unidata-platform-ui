import {ModelFieldDataType} from '../model/ModelFieldDataType';
import {IStringKeyMap} from '../../type/IStringKeyMap';

/**
 * Handle interface
 *
 * @author Denis Makarov
 * @date 2018-07-30
 */
export interface IDescriptor {
    ctor: any;
    config: IStringKeyMap<any>;
    type: ModelFieldDataType;
}
