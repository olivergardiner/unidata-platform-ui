/**
 * Interface of the basic config for the serialization / deserialization decorator
 *
 * @author Ivan Marshalkin
 * @date 2018-08-22
 */
export interface IBaseJsonDescriptorConfig {
    toJson?: string;
    fromJson: string;
    persist?: boolean;
}
