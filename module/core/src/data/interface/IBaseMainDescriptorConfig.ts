/**
 * Interface of the basic config for the main decorator
 *
 * @author Denis Makarov
 * @date 2018-08-14
 */

export interface IBaseMainDescriptorConfig {
    needObserve?: boolean;
    defaultValue?: any;
    required?: boolean;
    allowNull?: boolean;
    persist?: boolean;
    unique?: boolean;
    primaryKey?: boolean;
}
