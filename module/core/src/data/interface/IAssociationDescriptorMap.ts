/**
 * Interface for creating map constructors for hasOne and hasMany
 *
 * @author Ivan Marshalkin
 * @date 2018-08-30
 */

import {ClassCtor} from '../../type/ClassCtor';

export interface IAssociationDescriptorMap {
    hasOne?: {
        [key: string]: ClassCtor<any>;
    };
    hasMany?: {
        [key: string]: ClassCtor<any>;
    };
}
