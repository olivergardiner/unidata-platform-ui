/**
 * Metadata store
 *
 * @author Denis Makarov
 * @date 2018-08-16
 */

import {ClassPropertyDescriptor} from './ClassPropertyDescriptor';
import {ClassCtor} from '../type/ClassCtor';
import {IStringKeyMap} from '../type/IStringKeyMap';
import {Nullable} from '../type/Nullable';

export class ClassPropertyMeta {
    // map that stores metadata
    private static metamap = new Map();

    /**
     * Writes a handle
     *
     * @param targetCtor
     * @param annotationKey
     * @param propertyDescriptor
     */
    public static addDescriptor (targetCtor: ClassCtor<any>, annotationKey: string, propertyDescriptor: ClassPropertyDescriptor): void {
        let propertyName = propertyDescriptor.propertyName,
            ctorMeta;

        ctorMeta = ClassPropertyMeta.metamap.get(targetCtor);

        if (!ctorMeta) {
            ctorMeta = {};
        }

        if (!ctorMeta[annotationKey]) {
            ctorMeta[annotationKey] = {};
        }

        if (ctorMeta[annotationKey][propertyName]) {
            throw new Error('Duplicate propertyName');
        }

        ctorMeta[annotationKey][propertyName] = propertyDescriptor;

        ClassPropertyMeta.metamap.set(targetCtor, ctorMeta);
    }

    /**
     * Returns the map of descriptors. Where:
     * key - property name
     * value - property descriptor
     *
     * @param targetCtor
     * @param annotationKey
     */
    private static getMapDescriptors (targetCtor: any, annotationKey: string): IStringKeyMap<ClassPropertyDescriptor> {
        let ctorMeta;

        if (!targetCtor) {
            throw new Error('Cannot get descriptor without "targetCtor" param');
        }

        if (!annotationKey) {
            throw new Error('Cannot get descriptor without "annotationKey" param');
        }

        ctorMeta = ClassPropertyMeta.metamap.get(targetCtor);

        if (!ctorMeta) {
            return {};
        }

        if (!ctorMeta[annotationKey]) {
            return {};
        }

        return ctorMeta[annotationKey];
    }

    /**
     * Returns an array of descriptors
     *
     * @param targetCtor
     * @param annotationKey
     */
    public static getDescriptors (targetCtor: any, annotationKey: string): ClassPropertyDescriptor[] {
        let descriptors: ClassPropertyDescriptor[] = [],
            mapDescriptors;

        mapDescriptors = ClassPropertyMeta.getMapDescriptors(targetCtor, annotationKey);

        for (let key in mapDescriptors) {
            descriptors.push(mapDescriptors[key]);
        }

        return descriptors;
    }

    /**
     * Returns the property descriptor or null if it is not specified
     *
     * @param targetCtor
     * @param annotationKey
     * @param propertyName
     */
    public static getDescriptor (targetCtor: any, annotationKey: string, propertyName: string): Nullable<ClassPropertyDescriptor> {
        let descriptor = null,
            mapDescriptors;

        if (!propertyName) {
            throw new Error('Cannot get descriptor without "propertyName" param');
        }

        mapDescriptors = ClassPropertyMeta.getMapDescriptors(targetCtor, annotationKey);

        if (mapDescriptors[propertyName]) {
            descriptor = mapDescriptors[propertyName];
        }

        return descriptor;
    }

}
