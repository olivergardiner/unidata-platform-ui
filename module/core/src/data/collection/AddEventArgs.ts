/**
 * Arguments for the add event for Model Collection
 * @author Sergey Shishigin
 * @date 2018-08-28
 */
export class AddEventArgs<T> {
    public get items (): T[] {
        return this.internalItems;
    }

    private internalItems: T[];

    constructor (items: T[]) {
        this.internalItems = items;
    }
}
