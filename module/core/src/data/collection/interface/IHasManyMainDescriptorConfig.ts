/**
 * empty comment line Ivan Marshalkin
 *
 * @author: Ivan Marshalkin
 * @date: 2018-09-13
 */

import {IBaseMainDescriptorConfig} from '../../interface/IBaseMainDescriptorConfig';

/**
 * Interface of the hasMany config for the main decorator
 *
 * @author Denis Makarov
 * @date 2018-08-16
 */
export interface IHasManyMainDescriptorConfig extends IBaseMainDescriptorConfig {
    ofType?: any;
}
