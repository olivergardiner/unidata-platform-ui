/**
 * Collection of models
 *
 * @author Denis Makarov
 * @date 2018-07-30
 */

import {action, decorate, IObservableArray, observable, ObservableMap, runInAction} from 'mobx';
import {AbstractModel} from '../model/AbstractModel';
import each from 'lodash/each';
import includes from 'lodash/includes';
import difference from 'lodash/difference';
import find from 'lodash/find';
import {IHasManyMainDescriptorConfig} from './interface/IHasManyMainDescriptorConfig';
import {IStringKeyMap} from '../../type/IStringKeyMap';
import {AbstractField} from '../field/AbstractField';
import {IFieldValidator} from '../validation/IFieldValidator';
import {ClassCtor} from '../../type/ClassCtor';
import {Nullable} from '../../type/Nullable';
import {ReactiveProp} from '../../type/SystemReactiveProperties';
import {BaseData} from '../BaseData';

export class ModelCollection<T extends AbstractModel> extends BaseData {

    @observable.shallow
    public items: T[] = [];

    private original: T[] = [];

    public added: T[] = [];

    public removed: T[] = [];

    private dirty: boolean = false;

    private collectionDirty: boolean = false;

    private deepDirty: boolean = false;

    public type: T;

    public validators: IFieldValidator[] = [];

    constructor (data: any [], descriptorConfig: IHasManyMainDescriptorConfig, initConfig: IStringKeyMap, onChangeDirty?: () => void) {
        super();

        let self = this;

        if (data) {
            each(data, function (element: any) {
                self.items.push(new descriptorConfig.ofType(element, undefined, initConfig, self.updateDeepDirty.bind(self)));
            });
        }

        this.original = this.items.slice();
        this.type = descriptorConfig.ofType;

        if (onChangeDirty) {
            this.onChangeDirty = onChangeDirty;
        }
    }

    private onChangeDirty = () => {}; // the method is redefined in the constructor

    /**
     * Make all models in the collection and their properties reactive
     * @param props
     */
    public setReactiveCascade (props: ReactiveProp[]) {
        const self = this;

        self.setReactive(props);

        self.items.forEach(function (model) {
            model.setReactiveCascade(props);
        });

        props.forEach(function (prop) {
            self.observabilitySet.add(prop);
        });
    }

    public validate (): ObservableMap [] {
        let validationErrors: ObservableMap[] = [],
            message;

        this.validators.forEach((validator: IFieldValidator) => {
            let validateFn = validator.validate || validator.rule.validate;

            if (!validateFn({value: this.items, model: validator.model, args: validator.args})) {
                message = validator.message || validator.rule.defaultMessage;
                validationErrors.push(message);
            }
        });

        this.items.forEach(function (item: T) {
            let errors = item.validate();

            if (errors.size > 0) {
                validationErrors.push(errors);
            }
        });

        return validationErrors;
    }

    public add (item: T): ModelCollection<T> {
        this.addItems([item]);

        return this;
    }

    @action
    public addItems (added: T[]) {
        let self = this;

        added.forEach(function (model: T) {
            self.insertLast(model);
        });
    }

    /**
     * Inserts an item in the collection in the index position
     *
     * @param model
     * @param index
     */
    @action
    public insertAt (model: T, index: number) {
        let self = this;

        if (model.getPhantom()) {
            self.added.push(model);
        }

        self.items.splice(index, 0, model);

        model.onChangeDirty = self.updateDeepDirty.bind(self);

        if (self.observabilitySet.size > 0) {
            const observableProps: ReactiveProp[] = [];

            self.observabilitySet.forEach(function (prop) {
                observableProps.push(prop);
            });

            model.setReactiveCascade(observableProps);
        }

        if (self.removed.length > 0) {
            self.removed = difference(self.removed, [model]);
        }

        self.updateCollectionDirty();
    }

    /**
     * Inserts the item first in the collection
     *
     * @param model
     */
    public insertFirst (model: T) {
        this.insertAt(model, 0);
    }

    /**
     * Inserts the item last in the collection
     *
     * @param model
     */
    public insertLast (model: T) {
        this.insertAt(model, this.items.length);
    }

    /**
     * Inserts an element before the element
     *
     * @param model
     * @param before - the element to insert before
     */
    public inserBefore (model: T, before: T) {
        let index = this.indexOf(before);

        if (index === -1) {
            return;
        }

        this.insertAt(model, index);
    }

    /**
     * Inserts an element after an element
     *
     * @param model
     * @param before - the element to insert after
     */
    public inserAfter (model: T, after: T) {
        let index = this.indexOf(after);

        if (index === -1) {
            return;
        }

        this.insertAt(model, index + 1);
    }

    public replaceAll (added: T[]): ModelCollection<T> {
        this.removeItems(0, this.items.length);
        this.addItems(added);

        return this;
    }

    public remove (item: T): ModelCollection<T> {
        this.removeItems(this.indexOf(item), 1);

        return this;
    }

    public removeAt (index: number): ModelCollection<T> {
        this.removeItems(index, 1);

        return this;
    }

    public removeAll (): ModelCollection<T> {
        this.removeItems(0, this.items.length);

        return this;
    }

    /**
     * Returns the index of an item in the collection.
     * If the item is not in the collection -1 is returned
     *
     * @param item
     */
    public indexOf (item: T): number {
        return this.items.indexOf(item);
    }

    /**
     * Swaps items in the collection
     *
     * @param a
     * @param b
     */
    public swapItems (a: T, b: T) {
        const indexA = this.indexOf(a),
            indexB = this.indexOf(b);

        if (indexA === -1 || indexB === -1) {
            return;
        }

        this.items[indexA] = b;
        this.items[indexB] = a;
    }

    @action
    private removeItems (startIndex: number, count: number) {
        let self = this,
            removedOriginalItems,
            toRemove;

        if (startIndex < 0) {
            return;
        }

        toRemove = this.items.slice(startIndex, startIndex + count);

        // change the added and removed collections
        if (self.added.length > 0) {
            removedOriginalItems = difference(toRemove, self.added);
            self.added = difference(self.added, toRemove);
        } else {
            removedOriginalItems = toRemove;
        }

        each(removedOriginalItems, function (model: any) {
            if (model.getPhantom() === false) {
                self.removed.push(model);
            }
        });

        // unsubscribe events for all deleted items
        /*lodash.each(toRemove, function (model: any) {
            model.eventDirtyChange.unsubscribe(self.onDeepDirtyChange, self);
        });*/

        self.items.splice(startIndex, count);

        self.updateCollectionDirty();
    }

    // todo Ilya Brauer there's a double at the bottom - getCount
    public size (): number {
        return this.items.length;
    }

    public get (index: number): T {
        return this.items[index];
    }

    public getByPropertyValues (propName: string, values: any[]): T[] {
        let me: any = this,
            result: T[] = [];

        me.items.forEach(function (item: any) {
            let field = item[propName] as AbstractField<any>;

            if (includes(values, field.getValue())) {
                result.push(item as T);
            }
        });

        return result;

    }

    public getAdded (): T[] {
        return this.added.slice();
    }

    public getRemoved (): T[] {
        return this.removed.slice();
    }

    public getOriginal (): T[] {
        return this.original.slice();
    }

    public commit (): void {
        for (let i = 0; i < this.items.length; i++) {
            this.items[i].commit();
        }

        this.removed = [];
        this.added = [];
        this.original = this.items.slice() as T[];

        this.updateCollectionDirty();
        this.updateDeepDirty();
    }

    public revert (): void {
        for (let i = 0; i < this.added.length; i++) {
            this.added[i].revert();
        }

        for (let i = 0; i < this.removed.length; i++) {
            this.removed[i].revert();
        }

        this.removed = [];
        this.added = [];
        this.items = this.original.slice() as T[];
        this.original = this.items.slice() as T[];

        for (let i = 0; i < this.items.length; i++) {
            this.items[i].revert();
        }

        this.updateCollectionDirty();
        this.updateDeepDirty();
    }

    @action /** Don't delete action. The dirty property can be made reactive in runtime */
    public updateDirty (): void {
        this.dirty = this.calculateDirty();

        this.onChangeDirty();
    }

    @action /** Don't delete action. The collectionDirty property can be made reactive in runtime */
    public updateCollectionDirty (): void {
        this.collectionDirty = this.calculateCollectionDirty();

        this.updateDirty();
        this.updateDeepDirty();
    }

    @action /** Don't delete action. The deepDirty property can be made reactive in runtime */
    public updateDeepDirty (): void {
        this.deepDirty = this.calculateDeepDirty();

        this.updateDirty();
    }

    public getDirty (): boolean {
        return this.dirty;
    }

    public getCollectionDirty (): boolean {
        return this.collectionDirty;
    }

    public getDeepDirty (): boolean {
        return this.deepDirty;
    }

    public calculateDirty (): boolean {
        return this.collectionDirty || this.deepDirty;
    }

    public calculateCollectionDirty (): boolean {
        return (this.added && this.added.length > 0) || (this.removed && this.removed.length > 0);
    }

    public calculateDeepDirty (): boolean {
        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].getDirty() || this.items[i].getDeepDirty()) {
                return true;
            }
        }

        return false;
    }

    public serialize (config?: any): object[] {
        let result: object[] = [],
            item;

        config = config || {};
        // ToDo Boolean(persist)
        config.persist = (typeof config.persist !== 'undefined') ? config.persist : true;
        config.changes = (typeof config.changes !== 'undefined') ? config.changes : false;
        config.associations = (typeof config.associations !== 'undefined') ? config.associations : true;

        for (let i = 0; i < this.items.length; i++) {
            item = this.items[i].serialize(config);

            if (item !== null && Object.keys(item).length > 0) {
                result.push(item);
            }
        }

        return result;
    }

    /**
     * Search by function among items
     * @param {Function} fn
     * @return {T}
     */
    public find (fn: (item: T) => boolean): T {
        return find(this.items, fn) as T;
    }

    /**
     * Get the number of items in the collection
     *
     * @return {number}
     */
    // todo ilya brauer double
    public getCount (): number {
        return this.items.length;
    }

    public setPhantomCascade (phantom: boolean): void {
        for (let i = 0; i < this.items.length; i++) {
            this.items[i].setPhantomCascade(phantom);
        }
    }

    public clone (): ModelCollection<T> {
        return new ModelCollection(this.serialize(), {ofType: this.type}, {}, this.onChangeDirty) as ModelCollection<T>;
    }

    public static create<T extends AbstractModel> (type: ClassCtor<T>, onChangeDirty?: () => void): ModelCollection<T> {
        return new ModelCollection([], {ofType: type}, {}, onChangeDirty) as ModelCollection<T>;
    }

    public clearCollection () {
        this.original = [];
        this.added = [];
        this.removed = [];

        runInAction(() => {
            this.items = [];
        });
    }

    /**
     * Iteratively traverses the collection elements
     *
     * @param callback - search function
     *      @param item - collection item
     *      @param index - sequential number in the collection
     *      @param items - all items in the collection
     */
    public forEach (callback: (item: T, index: number, items: T[]) => void): void {
        return this.items.forEach(callback);
    }

    /**
     * Returns the found collection element or null
     * Iteratively traverses the elements of the collection, and returns the first element of the collection for which callback returns true.
     *
     * @param callback - search function
     *      @param item - collection item
     *      @param index - sequential number in the collection
     *      @param items - all items in the collection
     */
    public find2 (callback: (item: T, index: number, items: T[]) => boolean): Nullable<T> {
        let result = find(this.items, callback);

        // lodash returns undefined if the element is not found
        if (result === undefined) {
            return null;
        }

        return result;
    }

    /**
     * Returns a new array containing all the items in the collection.
     */
    public getRange (): T[] {
        return this.items.slice();
    }
}
