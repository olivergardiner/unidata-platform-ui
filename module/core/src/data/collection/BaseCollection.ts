import {IObservableArray, observable} from 'mobx';

export class BaseCollection<T> {
    @observable private items: IObservableArray<T> = observable.array([], {deep: false});

    public getItems (): T[] {
        return this.items;
    }

    public add (item: T) {
        this.items.push(item);
    }

    public getAt (index: number) {
        return this.items[index];
    }

    public clear () {
        this.items.clear();
    }
}
