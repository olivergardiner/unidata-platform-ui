/**
 * Arguments for the remove event for ModelCollection
 * @author Sergey Shishigin
 * @date 2018-08-28
 */
export class RemoveEventArgs<T> {
    public get items (): T[] {
        return this.internalItems;
    }

    private internalItems: T[];

    constructor (items: T[]) {
        this.internalItems = items;
    }
}
