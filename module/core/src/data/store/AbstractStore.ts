import {Sequence} from '../../Sequence';

export class AbstractStore {
    public storeId: number;

    constructor () {
        this.storeId = Sequence.getId();
    }
}
