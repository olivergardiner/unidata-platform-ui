import {HttpUrl} from './HttpUrl';

describe('HttpUrl', () => {
    describe('#removeSlashRight', () => {
        it('removes slash right', () => {
            expect(HttpUrl.removeSlashRight('http/localhost/')).toEqual('http/localhost');
        });
        it('removes multiple slashes right', () => {
            expect(HttpUrl.removeSlashRight('http/localhost////////////////////')).toEqual('http/localhost');
        });
    });

    describe('#removeSlashLeft', () => {
        it('removes slash left', () => {
            expect(HttpUrl.removeSlashLeft('/http/localhost')).toEqual('http/localhost');
        });
        it('removes multiple slashes left', () => {
            expect(HttpUrl.removeSlashLeft('//////////////////////////http/localhost')).toEqual('http/localhost');
        });
    });
});
