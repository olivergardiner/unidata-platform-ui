/**
 * Internal id generator for models
 *
 * @author Ivan Marshalkin
 * @date 2018-04-23
 */
export class Sequence {
    private static count: number = 0;

    public static getId (): number  {
        return ++Sequence.count;
    }
}
