/**
 * Negative id generator for models
 *
 * @author Denis Makarov
 * @date 2018-10-01
 */

export class NegativeSequence {
    private static count: number = 0;

    public static getId (): number  {
        return --NegativeSequence.count;
    }
}
