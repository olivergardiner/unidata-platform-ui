import 'systemjs/dist/s.js'; // import is required for ueModuleManager

import {UeModuleManager} from './userexit/UeModuleManager';
import {UeModuleLoader} from './userexit/UeModuleLoader';

declare global {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    interface Window {
        System: any;
    }
}

export const ueModuleManager = new UeModuleManager(new UeModuleLoader(window.System));

export * from './module/ModuleManager';

export * from './Sequence';

export * from './NegativeSequence';

export * from './HttpUrl';

export * from './network/interceptors/AxiosRequestInterceptor';

export * from './network/interceptors/AxiosResponseInterceptor';

export * from './event/EventChannel';

export * from './operation';

export * from './data';

export * from './type';

export * from './userexit';

export function assert (condition: boolean, message: string = 'Assert condition error!'): asserts condition {
    if (!condition) {
        throw new Error(message);
    }
}
