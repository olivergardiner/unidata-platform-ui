/**
 *  Manager of business applications
 *
 * @author Sergey Shishigin
 * @date 2019-12-23
 */
import {UeModuleBase} from '../userexit/type/UeModuleBase';
import {IModule} from '../type/IModule';

import {ueModuleManager} from '../index';
import {IStringKeyMap} from '../type/IStringKeyMap';

export class ModuleManager {
    private static modules: IStringKeyMap<IModule> = {};

    public static addModule (module: IModule) {
        const moduleId: string = module.id;

        if (this.hasModule(moduleId)) {
            console.error('Duplicate module id');

            return;
        }

        this.modules[module.id] = module;
        this.addUeModules(module);
    }

    private static addUeModules (module: IModule) {
        module.uemodules.forEach((uemodule: UeModuleBase) => {
            ueModuleManager.addModule(uemodule);
        });
    }

    public static hasModule (moduleId: string): boolean {
        return this.modules[moduleId] ? true : false;
    }
}
